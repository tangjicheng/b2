/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/SessionFactory.h"

#include "qfix/Session.h"
#include "qfix/SessionSettings.h"

namespace FIX {
SessionFactory::~SessionFactory()
{
	Dictionaries::iterator i = m_dictionaries.begin();
	for (; i != m_dictionaries.end(); ++i)
		delete i->second;
}

Session* SessionFactory::create(
	const SessionID& sessionID, const Dictionary& settings)
{
	std::string connectionType = settings.getString(CONNECTION_TYPE);
	if (connectionType != "acceptor" && connectionType != "initiator")
		throw ConfigError("Invalid ConnectionType");

	if (connectionType == "acceptor" && settings.has(SESSION_QUALIFIER))
		throw ConfigError("SessionQualifier cannot be used with acceptor.");

	bool useDataDictionary = true;
	if (settings.has(USE_DATA_DICTIONARY))
		useDataDictionary = settings.getBool(USE_DATA_DICTIONARY);

	DataDictionary dataDictionary;
	if (useDataDictionary) {
		std::string path = settings.getString(DATA_DICTIONARY);
		Dictionaries::iterator i = m_dictionaries.find(path);
		if (i != m_dictionaries.end())
			dataDictionary = *i->second;
		else {
			DataDictionary* p = new DataDictionary(path);
			dataDictionary = *p;
			m_dictionaries[path] = p;
		}
	}

	if (settings.has(VALIDATE_FIELDS_OUT_OF_ORDER)) {
		dataDictionary.checkFieldsOutOfOrder(
			settings.getBool(VALIDATE_FIELDS_OUT_OF_ORDER));
	}
	if (settings.has(VALIDATE_FIELDS_HAVE_VALUES)) {
		dataDictionary.checkFieldsHaveValues(
			settings.getBool(VALIDATE_FIELDS_HAVE_VALUES));
	}
	if (settings.has(VALIDATE_USER_DEFINED_FIELDS)) {
		dataDictionary.checkUserDefinedFields(
			settings.getBool(VALIDATE_USER_DEFINED_FIELDS));
	}

	HeartBtInt heartBtInt(0);
	if (connectionType == "initiator") {
		heartBtInt = HeartBtInt(settings.getLong(HEARTBTINT));
		if (heartBtInt <= 0)
			throw ConfigError("Heartbeat must be greater than zero");
	}

	Session* pSession = 0;
	pSession = new Session(m_application, m_messageStoreFactory, sessionID,
		dataDictionary, heartBtInt.getValue(), m_pLogFactory);

	if (settings.has(SEND_REDUNDANT_RESENDREQUESTS))
		pSession->setSendRedundantResendRequests(
			settings.getBool(SEND_REDUNDANT_RESENDREQUESTS));
	if (settings.has(CHECK_COMPID))
		pSession->setCheckCompId(settings.getBool(CHECK_COMPID));
	if (settings.has(CHECK_LATENCY))
		pSession->setCheckLatency(settings.getBool(CHECK_LATENCY));
	if (settings.has(MAX_LATENCY))
		pSession->setMaxLatency(settings.getLong(MAX_LATENCY));
	if (settings.has(LOGON_TIMEOUT))
		pSession->setLogonTimeout(settings.getLong(LOGON_TIMEOUT));
	if (settings.has(LOGOUT_TIMEOUT))
		pSession->setLogoutTimeout(settings.getLong(LOGOUT_TIMEOUT));
	if (settings.has(RESET_ON_LOGON))
		pSession->setResetOnLogon(settings.getBool(RESET_ON_LOGON));
	if (settings.has(MILLISECONDS_IN_TIMESTAMP))
		pSession->setMillisecondsInTimeStamp(
			settings.getBool(MILLISECONDS_IN_TIMESTAMP));
	if (settings.has(PERSIST_MESSAGES))
		pSession->setPersistMessages(settings.getBool(PERSIST_MESSAGES));
	if (settings.has(REPLICATION_MODE))
		pSession->set_replication_mode(settings.getBool(REPLICATION_MODE));

	return pSession;
}
} // namespace FIX
