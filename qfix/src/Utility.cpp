/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/Utility.h"

#include <limits.h>
#include <math.h>
#include <stdio.h>

#include <algorithm>

namespace FIX {
const char* to_string(Error_code error_code)
{
	switch (error_code) {
	case Error_code::no_error:
		return "No error";
	case Error_code::invalid_address_string:
		return "Invalid address string";
	case Error_code::invalid_address_family:
		return "Invalid address family";
	case Error_code::socket_create_fail:
		return "Socket create fail";
	case Error_code::socket_bind_fail:
		return "Socket bind fail";
	case Error_code::socket_listen_fail:
		break;
	}
	return "Socket listen fail";
}

void string_replace(const std::string& oldValue, const std::string& newValue,
	std::string& value)
{
	for (std::string::size_type pos = value.find(oldValue);
		 pos != std::string::npos; pos = value.find(oldValue, pos)) {
		value.replace(pos, oldValue.size(), newValue);
		pos += newValue.size();
	}
}

std::string string_toUpper(const std::string& value)
{
	std::string copy = value;
	std::transform(copy.begin(), copy.end(), copy.begin(), toupper);
	return copy;
}

std::string string_toLower(const std::string& value)
{
	std::string copy = value;
	std::transform(copy.begin(), copy.end(), copy.begin(), tolower);
	return copy;
}

void socket_init()
{
	struct sigaction sa;
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sigaction(SIGPIPE, &sa, 0);
}

void socket_term() {}

int socket_bind(int socket, const char* hostname, int port)
{
	sockaddr_in address;
	socklen_t socklen;

	address.sin_family = PF_INET;
	address.sin_port = htons(port);
	if (!hostname || !*hostname)
		address.sin_addr.s_addr = INADDR_ANY;
	else
		address.sin_addr.s_addr = inet_addr(hostname);
	socklen = sizeof(address);

	return bind(socket, reinterpret_cast<sockaddr*>(&address), socklen);
}

std::tuple<int, Error_code> socket_createAcceptor(
	int port, bool reuse, const char* host_address)
{
	int socket = ::socket(PF_INET, SOCK_STREAM, 0);
	if (socket < 0)
		return {-1, Error_code::socket_create_fail};

	sockaddr_in address;
	socklen_t socklen;

	address.sin_family = PF_INET;
	address.sin_port = htons(port);
	if (host_address != nullptr) {
		int result = inet_pton(AF_INET, host_address, &(address.sin_addr));
		if (result == 0)
			return {-1, Error_code::invalid_address_string};
		if (result != 1)
			return {-1, Error_code::invalid_address_family};
	}
	else {
		address.sin_addr.s_addr = INADDR_ANY;
	}
	socklen = sizeof(address);
	if (reuse)
		socket_setsockopt(socket, SO_REUSEADDR);

	int result = bind(socket, reinterpret_cast<sockaddr*>(&address), socklen);
	if (result < 0)
		return {-1, Error_code::socket_bind_fail};
	result = listen(socket, SOMAXCONN);
	if (result < 0)
		return {-1, Error_code::socket_listen_fail};
	return {socket, Error_code::no_error};
}

int socket_createConnector()
{
	return ::socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
}

int socket_connect(int socket, const char* address, int port)
{
	const char* hostname = socket_hostname(address);
	if (hostname == 0)
		return -1;

	sockaddr_in addr;
	addr.sin_family = PF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(hostname);

	int result
		= connect(socket, reinterpret_cast<sockaddr*>(&addr), sizeof(addr));

	return result;
}

int socket_accept(int s)
{
	if (!socket_isValid(s))
		return -1;
	return accept(s, 0, 0);
}

ssize_t socket_recv(int s, char* buf, size_t length)
{
	return recv(s, buf, length, 0);
}

int socket_send(int s, const char* msg, int length)
{
	return send(s, msg, length, 0);
}

void socket_close(int s)
{
	shutdown(s, 2);
	close(s);
}

bool socket_fionread(int s, int& bytes)
{
	bytes = 0;
	return ::ioctl(s, FIONREAD, &bytes) == 0;
}

bool socket_disconnected(int s)
{
	char byte;
	return ::recv(s, &byte, sizeof(byte), MSG_PEEK) <= 0;
}

int socket_setsockopt(int s, int opt)
{
	int level = SOL_SOCKET;
	if (opt == TCP_NODELAY)
		level = IPPROTO_TCP;

	int optval = 1;
	return ::setsockopt(s, level, opt, &optval, sizeof(optval));
}

int socket_setsockopt(int s, int opt, int optval)
{
	int level = SOL_SOCKET;
	if (opt == TCP_NODELAY)
		level = IPPROTO_TCP;

	return ::setsockopt(s, level, opt, &optval, sizeof(optval));
}

int socket_getsockopt(int s, int opt, int& optval)
{
	int level = SOL_SOCKET;
	if (opt == TCP_NODELAY)
		level = IPPROTO_TCP;

	socklen_t length = sizeof(socklen_t);

	return ::getsockopt(s, level, opt, (char*)&optval, &length);
}

int socket_fcntl(int s, int opt, int arg)
{
	return ::fcntl(s, opt, arg);
}

int socket_getfcntlflag(int s, int arg)
{
	return socket_fcntl(s, F_GETFL, arg);
}

int socket_setfcntlflag(int s, int arg)
{
	int oldValue = socket_getfcntlflag(s, arg);
	oldValue |= arg;
	return socket_fcntl(s, F_SETFL, arg);
}

int socket_setnonblock(int socket)
{
	return socket_setfcntlflag(socket, O_NONBLOCK);
}
bool socket_isValid(int socket)
{
	return socket >= 0;
}

bool socket_isBad(int s)
{
	struct stat buf;
	fstat(s, &buf);
	return errno == EBADF;
}

void socket_invalidate(int& socket)
{
	socket = -1;
}

short socket_hostport(int socket)
{
	struct sockaddr_in addr;
	socklen_t len = sizeof(addr);
	if (getsockname(socket, (struct sockaddr*)&addr, &len) < 0)
		return 0;

	return ntohs(addr.sin_port);
}

const char* socket_hostname(int socket)
{
	struct sockaddr_in addr;
	socklen_t len = sizeof(addr);
	if (getsockname(socket, (struct sockaddr*)&addr, &len) < 0)
		return 0;

	return inet_ntoa(addr.sin_addr);
}

const char* socket_hostname(const char* name)
{
	struct hostent* host_ptr = 0;
	struct in_addr** paddr;
	struct in_addr saddr;

#if (GETHOSTBYNAME_R_INPUTS_RESULT || GETHOSTBYNAME_R_RETURNS_RESULT)
	hostent host;
	char buf[1024];
	int error;
#endif

	saddr.s_addr = inet_addr(name);
	if (saddr.s_addr != (unsigned)-1)
		return name;

#if GETHOSTBYNAME_R_INPUTS_RESULT
	gethostbyname_r(name, &host, buf, sizeof(buf), &host_ptr, &error);
#elif GETHOSTBYNAME_R_RETURNS_RESULT
	host_ptr = gethostbyname_r(name, &host, buf, sizeof(buf), &error);
#else
	host_ptr = gethostbyname(name);
#endif

	if (host_ptr == 0)
		return 0;

	paddr = (struct in_addr**)host_ptr->h_addr_list;
	return inet_ntoa(**paddr);
}

const char* socket_peername(int socket, int* port)
{
	struct sockaddr_in addr;
	socklen_t len = sizeof(addr);
	if (getpeername(socket, (struct sockaddr*)&addr, &len) < 0)
		return 0;
	if (port)
		*port = addr.sin_port;
	return inet_ntoa(addr.sin_addr);
}

std::tuple<const char*, std::uint16_t> socket_peer_name(std::int32_t socket)
{
	struct sockaddr_in addr;
	socklen_t len = sizeof(addr);
	if (getpeername(socket, (struct sockaddr*)&addr, &len) < 0)
		return {nullptr, 0};
	return {inet_ntoa(addr.sin_addr), addr.sin_port};
}

std::pair<int, int> socket_createpair()
{
	int pair[2];
	socketpair(AF_UNIX, SOCK_STREAM, 0, pair);
	return std::pair<int, int>(pair[0], pair[1]);
}

tm time_gmtime(const time_t* t)
{
	tm result;
	return *gmtime_r(t, &result);
}

tm time_localtime(const time_t* t)
{
	tm result;
	return *localtime_r(t, &result);
}

bool thread_spawn(THREAD_START_ROUTINE func, void* var, thread_id& thread)
{
	thread_id result = 0;
	if (pthread_create(&result, 0, func, var) != 0)
		return false;
	thread = result;
	return true;
}

bool thread_spawn(THREAD_START_ROUTINE func, void* var)
{
	thread_id thread = 0;
	return thread_spawn(func, var, thread);
}

void thread_join(thread_id thread)
{
	pthread_join((pthread_t)thread, 0);
}

void thread_detach(thread_id thread)
{
	pthread_t t = thread;
	pthread_detach(t);
}

thread_id thread_self()
{
	return pthread_self();
}

void process_sleep(double s)
{
	timespec time, remainder;
	double intpart;
	time.tv_nsec = (long)(modf(s, &intpart) * 1e9);
	time.tv_sec = (int)intpart;
	while (nanosleep(&time, &remainder) == -1)
		time = remainder;
}

std::string file_separator()
{
	return "/";
}

void file_mkdir(const char* path)
{
	// use umask to override rwx for all
	mkdir(path, 0777);
}

int recursive_mkdir(const char* path)
{
	const size_t len = strlen(path);
	char tmp_path[PATH_MAX];
	char* p;

	if (len > sizeof(tmp_path) - 1)
		return -1;

	strcpy(tmp_path, path);
	for (p = tmp_path + 1; *p; ++p) {
		if (*p == '/') {
			*p = '\0';
			file_mkdir(tmp_path);
			*p = '/';
		}
	}
	file_mkdir(tmp_path);
	return 0;
}

FILE* file_fopen(const char* path, const char* mode)
{
	return fopen(path, mode);
}

void file_unlink(const char* path)
{
	unlink(path);
}

std::string file_appendpath(const std::string& path, const std::string& file)
{
	const char last = path[path.size() - 1];
	if (last == '/')
		return std::string(path) + file;
	else
		return std::string(path) + file_separator() + file;
}

bool file_atomic_write(
	const std::string& path, std::function<int(FILE*)>&& write_fn)
{
	const auto tmp_path = path + "~";
	file_unlink(tmp_path.c_str());
	auto file = file_fopen(tmp_path.c_str(), "w+");
	if (!file)
		return false;
	if (write_fn(file) < 0) {
		fclose(file);
		return false;
	};
	if (ferror(file)) {
		fclose(file);
		return false;
	}
	if (fflush(file) == EOF) {
		fclose(file);
		return false;
	}
	fclose(file);
	if (rename(tmp_path.c_str(), path.c_str()) == -1)
		return false;
	return true;
}
} // namespace FIX
