#include "qfix/AppLogFactory.h"

#include <string>

#include <misc/Log.h>

#include "qfix/SessionID.h"

namespace FIX {

namespace {

class GlobalAppLog : public Log {
public:
	GlobalAppLog()
		: Log {false, false, true, true}
	{
	}

	void clear() override {}

	void onIncoming(const std::string&) override {}

	void onOutgoing(const std::string&) override {}

	void onEvent(const std::string& value, int log_level) override
	{
		misc::appLog(log_level, "%s", value.c_str());
	}

	void debug(int level, const std::string& value) override
	{
		APP_DEBUG(level, value.c_str());
	}
};

class SessionAppLog : public Log {
public:
	explicit SessionAppLog(const SessionID& sessionId)
		: Log {false, false, true, true}
		, m_sessionId(sessionId.getTargetCompID().getString())
	{
	}

	void clear() override {}

	void onIncoming(const std::string&) override {}

	void onOutgoing(const std::string&) override {}

	void onEvent(
		const std::string& value, int log_level = misc::APP_LOG_INFO) override
	{
		misc::appLog(log_level, "%s: %s", m_sessionId.c_str(), value.c_str());
	}

	void debug(int level, const std::string& value) override
	{
		APP_DEBUG(level, value.c_str());
	}

private:
	std::string m_sessionId;
};

} // namespace

Log* AppLogFactory::create()
{
	return new GlobalAppLog;
}

Log* AppLogFactory::create(const SessionID& sessionId)
{
	return new SessionAppLog(sessionId);
}

void AppLogFactory::destroy(Log* log)
{
	delete log;
}

} // namespace FIX
