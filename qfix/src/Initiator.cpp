/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/Initiator.h"

#include <algorithm>
#include <fstream>
#include <sstream>

#include <misc/thread.h>

#include "qfix/Session.h"
#include "qfix/SessionFactory.h"
#include "qfix/Utility.h"

namespace FIX {
Initiator::Initiator(Application& application,
	MessageStoreFactory& messageStoreFactory, const SessionSettings& settings)
	: m_threadid(0)
	, m_application(application)
	, m_messageStoreFactory(messageStoreFactory)
	, m_settings(settings)
	, m_pLogFactory(0)
	, m_pLog(0)
	, m_stop(false)
{
	initialize();
}

Initiator::Initiator(Application& application,
	MessageStoreFactory& messageStoreFactory, const SessionSettings& settings,
	LogFactory& logFactory)
	: m_threadid(0)
	, m_application(application)
	, m_messageStoreFactory(messageStoreFactory)
	, m_settings(settings)
	, m_pLogFactory(&logFactory)
	, m_pLog(logFactory.create())
	, m_stop(false)
{
	initialize();
}

void Initiator::initialize()
{
	std::set<SessionID> sessions = m_settings.getSessions();
	std::set<SessionID>::iterator i;

	if (!sessions.size())
		throw ConfigError("No sessions defined");

	SessionFactory factory(m_application, m_messageStoreFactory, m_pLogFactory);

	for (i = sessions.begin(); i != sessions.end(); ++i) {
		if (m_settings.get(*i).getString(CONNECTION_TYPE) == "initiator") {
			m_sessionIDs.insert(*i);
			m_sessions[*i] = factory.create(*i, m_settings.get(*i));
			setDisconnected(*i);
		}
	}

	if (!m_sessions.size())
		throw ConfigError("No sessions defined for initiator");
}

Initiator::~Initiator()
{
	Sessions::iterator i;
	for (i = m_sessions.begin(); i != m_sessions.end(); ++i)
		delete i->second;

	if (m_pLogFactory && m_pLog)
		m_pLogFactory->destroy(m_pLog);
}

Session* Initiator::getSession(const SessionID& sessionID, Responder& responder)
{
	Sessions::iterator i = m_sessions.find(sessionID);
	if (i != m_sessions.end()) {
		i->second->setResponder(&responder);
		return i->second;
	}
	return 0;
}

void Initiator::connect()
{
	std::lock_guard lock(m_mutex);

	SessionIDs disconnected = m_disconnected;
	SessionIDs::iterator i = disconnected.begin();
	for (; i != disconnected.end(); ++i) {
		Session* pSession = Session::lookupSession(*i);
		if (pSession->isEnabled())
			doConnect(*i, m_settings.get(*i));
	}
}

void Initiator::setPending(const SessionID& sessionID)
{
	m_pending.insert(sessionID);
	m_connected.erase(sessionID);
	m_disconnected.erase(sessionID);
}

void Initiator::setConnected(const SessionID& sessionID)
{
	std::lock_guard lock(m_mutex);

	m_pending.erase(sessionID);
	m_connected.insert(sessionID);
	m_disconnected.erase(sessionID);
}

void Initiator::setDisconnected(const SessionID& sessionID)
{
	std::lock_guard lock(m_mutex);

	doSetDisconnected(sessionID);
}

void Initiator::doSetDisconnected(const SessionID& sessionID)
{
	m_pending.erase(sessionID);
	m_connected.erase(sessionID);
	m_disconnected.insert(sessionID);
}

void Initiator::start()
{
	m_stop.store(false, std::memory_order_seq_cst);
	onConfigure(m_settings);
	onInitialize(m_settings);

	if (!thread_spawn(&startThread, this, m_threadid))
		throw RuntimeError("Unable to spawn thread");
}

void Initiator::block()
{
	onConfigure(m_settings);
	onInitialize(m_settings);

	startThread(this);
}

bool Initiator::poll()
{
	if (m_firstPoll) {
		onConfigure(m_settings);
		onInitialize(m_settings);
		m_firstPoll = false;
	}

	return onPoll();
}

void Initiator::stop(bool force)
{
	if (isStopped())
		return;
	m_stop.store(true, std::memory_order_seq_cst);

	std::vector<Session*> enabledSessions;

	SessionIDs connected = m_connected;
	SessionIDs::iterator i = connected.begin();
	for (; i != connected.end(); ++i) {
		Session* pSession = Session::lookupSession(*i);
		if (pSession->isEnabled()) {
			enabledSessions.push_back(pSession);
			pSession->logout();
		}
	}

	if (!force) {
		for (int second = 1; second <= 10 && isLoggedOn(); ++second)
			process_sleep(1);
	}

	{
		std::lock_guard lock(m_mutex);
		for (i = connected.begin(); i != connected.end(); ++i)
			doSetDisconnected(Session::lookupSession(*i)->getSessionID());
	}

	onStop();
	if (m_threadid)
		thread_join(m_threadid);
	m_threadid = 0;

	std::vector<Session*>::iterator session = enabledSessions.begin();
	for (; session != enabledSessions.end(); ++session)
		(*session)->logon();
}

bool Initiator::isLoggedOn()
{
	std::lock_guard lock(m_mutex);

	SessionIDs connected = m_connected;
	SessionIDs::iterator i = connected.begin();
	for (; i != connected.end(); ++i) {
		if (Session::lookupSession(*i)->isLoggedOn())
			return true;
	}
	return false;
}

THREAD_PROC Initiator::startThread(void* p)
{
	Initiator* pInitiator = static_cast<Initiator*>(p);

	std::stringstream stream;
	stream << "Initiating connections on TID = " << misc::get_tid();
	pInitiator->getLog()->onEvent(stream.str());

	pInitiator->onStart();
	return 0;
}
} // namespace FIX
