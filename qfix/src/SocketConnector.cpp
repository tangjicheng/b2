/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/SocketConnector.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>

#include <sys/ioctl.h>

#include "qfix/Utility.h"

namespace FIX {
/// Handles events from SocketMonitor for client connections.
class ConnectorWrapper : public SocketMonitor::Strategy {
public:
	ConnectorWrapper(
		SocketConnector& connector, SocketConnector::Strategy& strategy)
		: m_connector(connector)
		, m_strategy(strategy)
	{
	}

private:
	void onConnect(SocketMonitor&, int socket)
	{
		m_strategy.onConnect(m_connector, socket);
	}

	void onWrite(SocketMonitor&, int socket)
	{
		m_strategy.onWrite(m_connector, socket);
	}

	void onEvent(SocketMonitor&, int socket)
	{
		if (!m_strategy.onData(m_connector, socket))
			m_strategy.onDisconnect(m_connector, socket);
	}

	void onError(SocketMonitor&, int socket)
	{
		m_strategy.onDisconnect(m_connector, socket);
	}

	void onError(SocketMonitor&) { m_strategy.onError(m_connector); }

	void onTimeout(SocketMonitor&) { m_strategy.onTimeout(m_connector); };

	SocketConnector& m_connector;
	SocketConnector::Strategy& m_strategy;
};

SocketConnector::SocketConnector(int timeout)
	: m_monitor(timeout)
{
}

int SocketConnector::connect(const std::string& address, int port, bool noDelay)
{
	int socket = socket_createConnector();

	if (socket != -1) {
		if (noDelay)
			socket_setsockopt(socket, TCP_NODELAY);
		m_monitor.addConnect(socket);
		socket_connect(socket, address.c_str(), port);
	}
	return socket;
}

int SocketConnector::connect(
	const std::string& address, int port, bool noDelay, Strategy&)
{
	int socket = connect(address, port, noDelay);
	return socket;
}

void SocketConnector::block(Strategy& strategy, bool poll)
{
	ConnectorWrapper wrapper(*this, strategy);
	m_monitor.block(wrapper, poll);
}
} // namespace FIX
