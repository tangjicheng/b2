/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/SocketConnection.h"

#include "qfix/Session.h"
#include "qfix/SocketAcceptor.h"
#include "qfix/SocketConnector.h"
#include "qfix/SocketInitiator.h"
#include "qfix/Utility.h"

namespace FIX {
SocketConnection::SocketConnection(
	int s, Sessions sessions, SocketMonitor* pMonitor)
	: m_socket(s)
	, m_sendLength(0)
	, m_sessions(sessions)
	, m_pSession(0)
	, m_pMonitor(pMonitor)
{
	initializeEpoll();
}

SocketConnection::SocketConnection(SocketInitiator& i,
	const SessionID& sessionID, int s, SocketMonitor* pMonitor)
	: m_socket(s)
	, m_sendLength(0)
	, m_pSession(i.getSession(sessionID, *this))
	, m_pMonitor(pMonitor)
{
	initializeEpoll();
	m_sessions.insert(sessionID);
}

SocketConnection::~SocketConnection()
{
	if (m_pSession)
		Session::unregisterSession(m_pSession->getSessionID());
}

std::string SocketConnection::getRemoteAddress() const
{
	int port {0};
	const char* address = socket_peername(m_socket, &port);
	std::stringstream stream;
	stream << address << ':' << port;
	return stream.str();
}

bool SocketConnection::send(const std::string& msg)
{
	std::lock_guard lock(m_mutex);

	m_sendQueue.push_back(msg);
	doProcessQueue();
	signal();
	return true;
}

void SocketConnection::schedule_disconnect()
{
	disconnect();
}

bool SocketConnection::processQueue()
{
	std::lock_guard lock(m_mutex);

	return doProcessQueue();
}

bool SocketConnection::doProcessQueue()
{
	if (!m_sendQueue.size())
		return true;

	const auto timeout = 1000;
	const auto maxSize = 1;
	epoll_event events[maxSize];
	auto eventCount = 0;
	do {
		eventCount = epoll_wait(m_epfd, events, maxSize, timeout);
	} while (eventCount < 0 && errno == EINTR);
	if (eventCount <= 0 || (events[0].events & EPOLLOUT) != EPOLLOUT)
		return false;

	const std::string& msg = m_sendQueue.front();

	int result = socket_send(
		m_socket, msg.c_str() + m_sendLength, msg.length() - m_sendLength);

	if (result > 0)
		m_sendLength += result;

	if (m_sendLength == msg.length()) {
		m_sendLength = 0;
		m_sendQueue.pop_front();
	}

	return !m_sendQueue.size();
}

void SocketConnection::disconnect()
{
	socket_close(m_epfd);
	if (m_pMonitor)
		m_pMonitor->drop(m_socket);
}

void SocketConnection::initializeEpoll()
{
	m_epfd = epoll_create1(0);
	if (m_epfd == -1) {
		throw SocketException {};
	}
	epoll_event options;
	options.events = EPOLLOUT;
	options.data.fd = m_socket;
	if (epoll_ctl(m_epfd, EPOLL_CTL_ADD, m_socket, &options) < 0) {
		close(m_epfd);
		throw SocketException {};
	}
}

bool SocketConnection::read(SocketConnector& s)
{
	if (!m_pSession)
		return false;

	try {
		readFromSocket();
		readMessages(s.getMonitor());
	}
	catch (SocketRecvFailed& e) {
		m_pSession->getLog()->onEvent(e.what(), misc::APP_LOG_ERR);
		return false;
	}
	return true;
}

bool SocketConnection::read(SocketAcceptor& a, SocketServer& s)
{
	std::string msg;
	try {
		readFromSocket();

		if (!m_pSession) {
			if (!readMessage(msg))
				return true;
			m_pSession = Session::lookupSession(msg, true);
			if (!isValidSession()) {
				if (m_pSession == 0) {
					a.onEvent("Session not found for incoming message: " + msg,
						misc::APP_LOG_ERR);
					a.onIncoming(msg);
				}
				else
					m_pSession = 0;
			}
			if (m_pSession)
				m_pSession = a.getSession(msg, *this);
			if (m_pSession)
				m_pSession->next(msg);
			if (!m_pSession) {
				s.getMonitor().drop(m_socket);
				return false;
			}

			Session::registerSession(m_pSession->getSessionID());
		}

		readMessages(s.getMonitor());
		return true;
	}
	catch (SocketRecvFailed& e) {
		if (m_pSession)
			m_pSession->getLog()->onEvent(e.what(), misc::APP_LOG_ERR);
		s.getMonitor().drop(m_socket);
	}
	catch (InvalidMessage&) {
		s.getMonitor().drop(m_socket);
	}
	return false;
}

bool SocketConnection::isValidSession()
{
	if (m_pSession == 0)
		return false;
	SessionID sessionID = m_pSession->getSessionID();
	if (Session::isSessionRegistered(sessionID))
		return false;
	return !(m_sessions.find(sessionID) == m_sessions.end());
}

void SocketConnection::readFromSocket()
{
	const auto result = m_parser.read(m_socket);
	if (result != Read_result::success) {
		const auto error_code = (result == Read_result::closed) ? 0 : -1;
		throw SocketRecvFailed {error_code};
	}
}

bool SocketConnection::readMessage(std::string& msg)
{
	try {
		return m_parser.extract_fix_message(msg);
	}
	catch (MessageParseError&) {
	}
	return true;
}

void SocketConnection::readMessages(SocketMonitor& s)
{
	if (!m_pSession)
		return;

	std::string msg;
	while (readMessage(msg)) {
		try {
			m_pSession->next(msg);
		}
		catch (InvalidMessage&) {
			if (!m_pSession->isLoggedOn())
				s.drop(m_socket);
		}
	}
}

void SocketConnection::onTimeout()
{
	if (m_pSession)
		m_pSession->next();
}
} // namespace FIX
