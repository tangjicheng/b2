/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/SocketServer.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <exception>

#include <sys/ioctl.h>

#include "qfix/Exceptions.h"
#include "qfix/FieldConvertors.h"
#include "qfix/Utility.h"

namespace FIX {
/// Handles events from SocketMonitor for server connections.
class ServerWrapper : public SocketMonitor::Strategy {
public:
	ServerWrapper(std::set<int> sockets, SocketServer& server,
		SocketServer::Strategy& strategy)
		: m_sockets(sockets)
		, m_server(server)
		, m_strategy(strategy)
	{
	}

private:
	void onConnect(SocketMonitor&, int) {}

	void onEvent(SocketMonitor& monitor, int socket)
	{
		if (m_sockets.find(socket) != m_sockets.end()) {
			m_strategy.onConnect(m_server, socket, m_server.accept(socket));
		}
		else {
			if (!m_strategy.onData(m_server, socket))
				onError(monitor, socket);
		}
	}

	void onWrite(SocketMonitor&, int socket)
	{
		m_strategy.onWrite(m_server, socket);
	}

	void onError(SocketMonitor& monitor, int socket)
	{
		m_strategy.onDisconnect(m_server, socket);
		monitor.drop(socket);
	}

	void onError(SocketMonitor&) { m_strategy.onError(m_server); }

	void onTimeout(SocketMonitor&) { m_strategy.onTimeout(m_server); };

	typedef std::set<int> Sockets;

	Sockets m_sockets;
	SocketServer& m_server;
	SocketServer::Strategy& m_strategy;
};

SocketServer::SocketServer(int timeout)
	: m_monitor(timeout)
{
}

int SocketServer::add(int port, bool reuse, const char* host, bool noDelay)
{
	if (m_portToInfo.find(port) != m_portToInfo.end())
		return m_portToInfo[port].m_socket;

	auto [socket, error] = socket_createAcceptor(port, reuse, host);
	if (error != Error_code::no_error) {
		SocketException e;
		socket_close(socket);
		throw SocketException(std::string {} + to_string(error) + " for " + host
			+ ":" + IntConvertor::convert((unsigned short)port) + " ("
			+ e.what() + ")");
	}
	if (noDelay)
		socket_setsockopt(socket, TCP_NODELAY);
	m_monitor.addRead(socket);

	SocketInfo info(socket, port, noDelay);
	m_socketToInfo[socket] = info;
	m_portToInfo[port] = info;
	return socket;
}

int SocketServer::accept(int socket)
{
	SocketInfo info = m_socketToInfo[socket];

	int result = socket_accept(socket);
	if (info.m_noDelay)
		socket_setsockopt(result, TCP_NODELAY);
	if (result >= 0)
		m_monitor.addConnect(result);
	return result;
}

void SocketServer::close()
{
	SocketToInfo::iterator i = m_socketToInfo.begin();
	for (; i != m_socketToInfo.end(); ++i) {
		int s = i->first;
		socket_close(s);
		socket_invalidate(s);
	}
}

bool SocketServer::block(Strategy& strategy, bool poll)
{
	std::set<int> sockets;
	SocketToInfo::iterator i = m_socketToInfo.begin();
	for (; i != m_socketToInfo.end(); ++i) {
		if (!socket_isValid(i->first))
			return false;
		sockets.insert(i->first);
	}

	ServerWrapper wrapper(sockets, *this, strategy);
	m_monitor.block(wrapper, poll);
	return true;
}

int SocketServer::socketToPort(int socket)
{
	SocketToInfo::iterator find = m_socketToInfo.find(socket);
	if (find == m_socketToInfo.end())
		return 0;
	return find->second.m_port;
}

int SocketServer::portToSocket(int port)
{
	SocketToInfo::iterator find = m_portToInfo.find(port);
	if (find == m_portToInfo.end())
		return 0;
	return find->second.m_socket;
}
} // namespace FIX
