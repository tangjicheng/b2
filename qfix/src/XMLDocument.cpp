/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/XMLDocument.h"

#include <sstream>

#include <libxml/xpath.h>

namespace FIX {
bool XMLAttributes::get(const std::string& name, std::string& value)
{
	xmlChar* result = xmlGetProp(m_pNode, (const xmlChar*)name.c_str());
	if (result == NULL)
		return false;
	value = (char*)result;
	xmlFree(result);
	return true;
}

std::map<std::string, std::string> XMLAttributes::toMap()
{
	xmlAttr* attr = m_pNode->properties;
	std::map<std::string, std::string> map;
	while (attr != 0) {
		std::string value;
		std::string name;
		if (attr->name)
			name = (char*)attr->name;
		get(name, value);
		map[name] = value;
		attr = attr->next;
	}
	return map;
}

XMLNodePtr XMLNode::getFirstChildNode()
{
	if (!m_pNode->children)
		return XMLNodePtr();
	xmlNodePtr pNode = m_pNode->children;
	if (pNode == NULL)
		return XMLNodePtr();
	return XMLNodePtr(new XMLNode(pNode));
}

XMLNodePtr XMLNode::getNextSiblingNode()
{
	if (!m_pNode->next)
		return XMLNodePtr();
	xmlNodePtr pNode = m_pNode->next;
	if (pNode == NULL)
		return XMLNodePtr();
	return XMLNodePtr(new XMLNode(pNode));
}

XMLAttributesPtr XMLNode::getAttributes()
{
	return XMLAttributesPtr(new XMLAttributes(m_pNode));
}

std::string XMLNode::getName()
{
	return m_pNode->name ? (char*)m_pNode->name : "";
}

std::string XMLNode::getText()
{
	return m_pNode->content ? (char*)m_pNode->content : "";
}

XMLDocument::XMLDocument()
	: m_pDoc(NULL)
{
}

XMLDocument::~XMLDocument()
{
	xmlFreeDoc(m_pDoc);
}

bool XMLDocument::load(std::istream& stream)
{
	try {
		std::stringstream sstream;
		sstream << stream.rdbuf();
		m_pDoc = xmlParseDoc((xmlChar*)sstream.str().c_str());
		return m_pDoc != NULL;
	}
	catch (...) {
		return false;
	}
}

bool XMLDocument::load(const std::string& url)
{
	try {
		m_pDoc = xmlParseFile(url.c_str());
		return m_pDoc != NULL;
	}
	catch (...) {
		return false;
	}
}

bool XMLDocument::xml(std::ostream&)
{
	return false;
}

XMLNodePtr XMLDocument::getNode(const std::string& XPath)
{
	xmlXPathContextPtr context = xmlXPathNewContext(m_pDoc);
	xmlXPathObjectPtr xpathObject
		= xmlXPathEval((xmlChar*)XPath.c_str(), context);

	if (xpathObject == NULL || xpathObject->nodesetval == NULL
		|| xpathObject->nodesetval->nodeNr != 1) {
		xmlXPathFreeContext(context);
		return XMLNodePtr();
	}

	XMLNodePtr result(new XMLNode(xpathObject->nodesetval->nodeTab[0]));
	xmlXPathFreeContext(context);
	xmlXPathFreeObject(xpathObject);
	return result;
}
} // namespace FIX
