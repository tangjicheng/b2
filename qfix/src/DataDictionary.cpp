/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/DataDictionary.h"

#include <fstream>
#include <memory>

#include "qfix/Message.h"

namespace FIX {
DataDictionary::DataDictionary()
	: m_hasVersion(false)
	, m_checkFieldsOutOfOrder(true)
	, m_checkFieldsHaveValues(true)
	, m_checkUserDefinedFields(true)
	, m_orderedFieldsArray(0)
{
}

DataDictionary::DataDictionary(const std::string& url)
	: m_hasVersion(false)
	, m_checkFieldsOutOfOrder(true)
	, m_checkFieldsHaveValues(true)
	, m_checkUserDefinedFields(true)
	, m_orderedFieldsArray(0)
{
	readFromURL(url);
}

DataDictionary::DataDictionary(const DataDictionary& copy)
{
	*this = copy;
}

DataDictionary::~DataDictionary()
{
	FieldToGroup::iterator i;
	for (i = m_groups.begin(); i != m_groups.end(); ++i)
		delete i->second.second;
	if (m_orderedFieldsArray)
		delete[] m_orderedFieldsArray;
}

DataDictionary& DataDictionary::operator=(const DataDictionary& rhs)
{
	m_hasVersion = rhs.m_hasVersion;
	m_checkFieldsOutOfOrder = rhs.m_checkFieldsOutOfOrder;
	m_checkFieldsHaveValues = rhs.m_checkFieldsHaveValues;
	m_checkUserDefinedFields = rhs.m_checkUserDefinedFields;
	m_beginString = rhs.m_beginString;
	m_messageFields = rhs.m_messageFields;
	m_requiredFields = rhs.m_requiredFields;
	m_messages = rhs.m_messages;
	m_fields = rhs.m_fields;
	m_orderedFields = rhs.m_orderedFields;
	m_orderedFieldsArray = 0;
	m_headerFields = rhs.m_headerFields;
	m_trailerFields = rhs.m_trailerFields;
	m_fieldTypes = rhs.m_fieldTypes;
	m_fieldValues = rhs.m_fieldValues;
	m_fieldNames = rhs.m_fieldNames;
	m_names = rhs.m_names;
	m_valueNames = rhs.m_valueNames;

	FieldToGroup::const_iterator i = rhs.m_groups.begin();
	for (; i != rhs.m_groups.end(); ++i) {
		addGroup(i->first.first, i->first.second, i->second.first,
			*i->second.second);
	}
	return *this;
}

void DataDictionary::validate(const Message& message)
{
	const Header& header = message.getHeader();
	const BeginString& beginString = FIELD_GET_REF(header, BeginString);
	const MsgType& msgType = FIELD_GET_REF(header, MsgType);

	if (m_hasVersion && m_beginString != beginString)
		throw UnsupportedVersion();

	int field = 0;
	if (m_checkFieldsOutOfOrder && !message.hasValidStructure(field))
		throw TagOutOfOrder(field);

	if (m_hasVersion) {
		checkMsgType(msgType);
		checkHasRequired(
			message.getHeader(), message, message.getTrailer(), msgType);
	}

	iterate(message.getHeader(), msgType);
	iterate(message.getTrailer(), msgType);
	iterate(message, msgType);
}

void DataDictionary::iterate(const FieldMap& map, const MsgType& msgType)
{
	int lastField = 0;

	FieldMap::iterator i;
	for (i = map.begin(); i != map.end(); ++i) {
		const FieldBase& field = i->second;
		if (i != map.begin() && (field.getField() == lastField))
			throw RepeatedTag(lastField);
		checkHasValue(field);

		if (m_hasVersion) {
			checkValidFormat(field);
			checkValue(field);
		}

		if (m_beginString.getValue().length() && shouldCheckTag(field)) {
			checkValidTagNumber(field);
			if (!Message::isHeaderField(field, this)
				&& !Message::isTrailerField(field, this)) {
				checkIsInMessage(field, msgType);
				checkGroupCount(field, map, msgType);
			}
		}
		lastField = field.getField();
	}
}

void DataDictionary::readFromURL(const std::string& url)
{
	XMLDocumentPtr pDoc = XMLDocumentPtr(new XMLDocument());

	if (!pDoc->load(url))
		throw ConfigError(url + ": Could not parse data dictionary file");

	try {
		readFromDocument(std::move(pDoc));
	}
	catch (ConfigError& e) {
		throw ConfigError(url + ": " + e.what());
	}
}

void DataDictionary::readFromDocument(XMLDocumentPtr pDoc)
{
	// VERSION
	XMLNodePtr pFixNode = pDoc->getNode("/fix");
	if (!pFixNode)
		throw ConfigError("Could not parse data dictionary file"
						  ", or no <fix> node found at root");
	XMLAttributesPtr attrs = pFixNode->getAttributes();
	std::string major;
	if (!attrs->get("major", major))
		throw ConfigError("major attribute not found on <fix>");
	std::string minor;
	if (!attrs->get("minor", minor))
		throw ConfigError("minor attribute not found on <fix>");
	setVersion("FIX." + major + "." + minor);

	// FIELDS
	XMLNodePtr pFieldsNode = pDoc->getNode("/fix/fields");
	if (!pFieldsNode)
		throw ConfigError("<fields> section not found in data dictionary");

	XMLNodePtr pFieldNode = pFieldsNode->getFirstChildNode();
	if (!pFieldNode)
		throw ConfigError("No fields defined");

	while (pFieldNode) {
		if (pFieldNode->getName() == "field") {
			XMLAttributesPtr attrs = pFieldNode->getAttributes();
			std::string name;
			if (!attrs->get("name", name))
				throw ConfigError("<field> does not have a name attribute");
			std::string number;
			if (!attrs->get("number", number))
				throw ConfigError(
					"<field> " + name + " does not have a number attribute");
			int num = atol(number.c_str());
			std::string type;
			if (!attrs->get("type", type))
				throw ConfigError(
					"<field> " + name + " does not have a type attribute");
			addField(num);
			addFieldType(num, XMLTypeToType(type));
			addFieldName(num, name);

			XMLNodePtr pFieldValueNode = pFieldNode->getFirstChildNode();
			while (pFieldValueNode) {
				if (pFieldValueNode->getName() == "value") {
					XMLAttributesPtr attrs = pFieldValueNode->getAttributes();
					std::string enumeration;
					if (!attrs->get("enum", enumeration))
						throw ConfigError(
							"<value> does not have enum attribute in field "
							+ name);
					addFieldValue(num, enumeration);
					std::string description;
					if (attrs->get("description", description))
						addValueName(num, enumeration, description);
				}
				pFieldValueNode = pFieldValueNode->getNextSiblingNode();
			}
		}
		pFieldNode = pFieldNode->getNextSiblingNode();
	}

	// HEADER
	XMLNodePtr pHeaderNode = pDoc->getNode("/fix/header");
	if (!pHeaderNode)
		throw ConfigError("<header> section not found in data dictionary");

	XMLNodePtr pHeaderFieldNode = pHeaderNode->getFirstChildNode();
	if (!pHeaderFieldNode)
		throw ConfigError("No header fields defined");

	while (pHeaderFieldNode) {
		if (pHeaderFieldNode->getName() == "field"
			|| pHeaderFieldNode->getName() == "group") {
			XMLAttributesPtr attrs = pHeaderFieldNode->getAttributes();
			std::string name;
			if (!attrs->get("name", name))
				throw ConfigError("<field> does not have a name attribute");
			std::string required = "false";
			attrs->get("required", required);
			addHeaderField(
				lookupXMLFieldNumber(pDoc.get(), name), required == "true");
		}
		if (pHeaderFieldNode->getName() == "group") {
			XMLAttributesPtr attrs = pHeaderFieldNode->getAttributes();
			std::string required;
			attrs->get("required", required);
			bool isRequired = (required == "Y" || required == "y");
			addXMLGroup(pDoc.get(), pHeaderFieldNode.get(), "_header_", *this,
				isRequired);
		}

		pHeaderFieldNode = pHeaderFieldNode->getNextSiblingNode();
	}

	// TRAILER
	XMLNodePtr pTrailerNode = pDoc->getNode("/fix/trailer");
	if (!pTrailerNode)
		throw ConfigError("<trailer> section not found in data dictionary");

	XMLNodePtr pTrailerFieldNode = pTrailerNode->getFirstChildNode();
	if (!pTrailerFieldNode)
		throw ConfigError("No trailer fields defined");

	while (pTrailerFieldNode) {
		if (pTrailerFieldNode->getName() == "field"
			|| pTrailerFieldNode->getName() == "group") {
			XMLAttributesPtr attrs = pTrailerFieldNode->getAttributes();
			std::string name;
			if (!attrs->get("name", name))
				throw ConfigError("<field> does not have a name attribute");
			std::string required = "false";
			attrs->get("required", required);
			addTrailerField(
				lookupXMLFieldNumber(pDoc.get(), name), required == "true");
		}
		if (pTrailerFieldNode->getName() == "group") {
			XMLAttributesPtr attrs = pTrailerFieldNode->getAttributes();
			std::string required;
			attrs->get("required", required);
			bool isRequired = (required == "Y" || required == "y");
			addXMLGroup(pDoc.get(), pTrailerFieldNode.get(), "_trailer_", *this,
				isRequired);
		}

		pTrailerFieldNode = pTrailerFieldNode->getNextSiblingNode();
	}

	// MSGTYPE
	XMLNodePtr pMessagesNode = pDoc->getNode("/fix/messages");
	if (!pMessagesNode)
		throw ConfigError("<messages> section not found in data dictionary");

	XMLNodePtr pMessageNode = pMessagesNode->getFirstChildNode();
	if (!pMessageNode)
		throw ConfigError("No messages defined");

	while (pMessageNode) {
		if (pMessageNode->getName() == "message") {
			XMLAttributesPtr attrs = pMessageNode->getAttributes();
			std::string msgtype;
			if (!attrs->get("msgtype", msgtype))
				throw ConfigError("<field> does not have a name attribute");
			addMsgType(msgtype);

			std::string name;
			if (attrs->get("name", name))
				addValueName(35, msgtype, name);

			XMLNodePtr pMessageFieldNode = pMessageNode->getFirstChildNode();
			if (!pMessageFieldNode)
				throw ConfigError("<message> contains no fields");
			while (pMessageFieldNode) {
				if (pMessageFieldNode->getName() == "field"
					|| pMessageFieldNode->getName() == "group") {
					XMLAttributesPtr attrs = pMessageFieldNode->getAttributes();
					std::string name;
					if (!attrs->get("name", name))
						throw ConfigError(
							"<field> does not have a name attribute");
					int num = lookupXMLFieldNumber(pDoc.get(), name);
					addMsgField(msgtype, num);

					std::string required;
					if (attrs->get("required", required)
						&& (required == "Y" || required == "y")) {
						addRequiredField(msgtype, num);
					}
				}
				else if (pMessageFieldNode->getName() == "component") {
					XMLAttributesPtr attrs = pMessageFieldNode->getAttributes();
					std::string required;
					attrs->get("required", required);
					bool isRequired = (required == "Y" || required == "y");
					addXMLComponentFields(pDoc.get(), pMessageFieldNode.get(),
						msgtype, *this, isRequired);
				}
				if (pMessageFieldNode->getName() == "group") {
					XMLAttributesPtr attrs = pMessageFieldNode->getAttributes();
					std::string required;
					attrs->get("required", required);
					bool isRequired = (required == "Y" || required == "y");
					addXMLGroup(pDoc.get(), pMessageFieldNode.get(), msgtype,
						*this, isRequired);
				}
				pMessageFieldNode = pMessageFieldNode->getNextSiblingNode();
			}
		}
		pMessageNode = pMessageNode->getNextSiblingNode();
	}
}

int* DataDictionary::getOrderedFields() const
{
	if (m_orderedFieldsArray)
		return m_orderedFieldsArray;
	m_orderedFieldsArray = new int[m_orderedFields.size() + 1];

	int* i = m_orderedFieldsArray;
	OrderedFields::const_iterator iter;
	for (iter = m_orderedFields.begin(); iter != m_orderedFields.end();
		 *(i++) = *(iter++)) {
	}
	*i = 0;
	return m_orderedFieldsArray;
}

int DataDictionary::lookupXMLFieldNumber(
	XMLDocument* pDoc, XMLNode* pNode) const
{
	XMLAttributesPtr attrs = pNode->getAttributes();
	std::string name;
	if (!attrs->get("name", name))
		throw ConfigError("No name given to field");
	return lookupXMLFieldNumber(pDoc, name);
}

int DataDictionary::lookupXMLFieldNumber(
	XMLDocument*, const std::string& name) const
{
	NameToField::const_iterator i = m_names.find(name);
	if (i == m_names.end())
		throw ConfigError("Field " + name + " not defined in fields section");
	return i->second;
}

int DataDictionary::addXMLComponentFields(XMLDocument* pDoc, XMLNode* pNode,
	const std::string& msgtype, DataDictionary& DD, bool componentRequired)
{
	int firstField = 0;

	XMLAttributesPtr attrs = pNode->getAttributes();
	std::string name;
	if (!attrs->get("name", name))
		throw ConfigError("No name given to component");

	XMLNodePtr pComponentNode
		= pDoc->getNode("/fix/components/component[@name='" + name + "']");
	if (!pComponentNode)
		throw ConfigError("Component not found");

	XMLNodePtr pComponentFieldNode = pComponentNode->getFirstChildNode();
	while (pComponentFieldNode) {
		if (pComponentFieldNode->getName() == "field"
			|| pComponentFieldNode->getName() == "group") {
			XMLAttributesPtr attrs = pComponentFieldNode->getAttributes();
			std::string name;
			if (!attrs->get("name", name))
				throw ConfigError("No name given to field");
			int field = lookupXMLFieldNumber(pDoc, name);
			if (firstField == 0)
				firstField = field;

			std::string required;
			if (attrs->get("required", required)
				&& (required == "Y" || required == "y") && componentRequired) {
				addRequiredField(msgtype, field);
			}

			DD.addField(field);
			DD.addMsgField(msgtype, field);
		}
		if (pComponentFieldNode->getName() == "group") {
			XMLAttributesPtr attrs = pComponentFieldNode->getAttributes();
			std::string required;
			attrs->get("required", required);
			bool isRequired = (required == "Y" || required == "y");
			addXMLGroup(
				pDoc, pComponentFieldNode.get(), msgtype, DD, isRequired);
		}
		pComponentFieldNode = pComponentFieldNode->getNextSiblingNode();
	}
	return firstField;
}

void DataDictionary::addXMLGroup(XMLDocument* pDoc, XMLNode* pNode,
	const std::string& msgtype, DataDictionary& DD, bool groupRequired)
{
	XMLAttributesPtr attrs = pNode->getAttributes();
	std::string name;
	if (!attrs->get("name", name))
		throw ConfigError("No name given to group");
	int group = lookupXMLFieldNumber(pDoc, name);
	int delim = 0;
	int field = 0;
	DataDictionary groupDD;
	XMLNodePtr node = pNode->getFirstChildNode();
	while (node) {
		if (node->getName() == "field") {
			field = lookupXMLFieldNumber(pDoc, node.get());
			groupDD.addField(field);

			XMLAttributesPtr attrs = node->getAttributes();
			std::string required;
			if (attrs->get("required", required)
				&& (required == "Y" || required == "y") && groupRequired) {
				groupDD.addRequiredField(msgtype, field);
			}
		}
		else if (node->getName() == "component") {
			field = addXMLComponentFields(
				pDoc, node.get(), msgtype, groupDD, false);
		}
		else if (node->getName() == "group") {
			field = lookupXMLFieldNumber(pDoc, node.get());
			groupDD.addField(field);
			XMLAttributesPtr attrs = node->getAttributes();
			std::string required;
			if (attrs->get("required", required)
				&& (required == "Y" || required == "y") && groupRequired) {
				groupDD.addRequiredField(msgtype, field);
			}
			bool isRequired = false;
			if (attrs->get("required", required))
				isRequired = (required == "Y" || required == "y");
			addXMLGroup(pDoc, node.get(), msgtype, groupDD, isRequired);
		}
		if (delim == 0)
			delim = field;
		node = node->getNextSiblingNode();
	}

	if (delim)
		DD.addGroup(msgtype, group, delim, groupDD);
}

TYPE::Type DataDictionary::XMLTypeToType(const std::string& type) const
{
	if (m_beginString < "FIX.4.2" && type == "CHAR")
		return TYPE::String;

	if (type == "STRING")
		return TYPE::String;
	if (type == "CHAR")
		return TYPE::Char;
	if (type == "PRICE")
		return TYPE::Price;
	if (type == "INT")
		return TYPE::Int;
	if (type == "AMT")
		return TYPE::Amt;
	if (type == "QTY")
		return TYPE::Qty;
	if (type == "CURRENCY")
		return TYPE::Currency;
	if (type == "MULTIPLEVALUESTRING")
		return TYPE::MultipleValueString;
	if (type == "EXCHANGE")
		return TYPE::Exchange;
	if (type == "UTCTIMESTAMP")
		return TYPE::UtcTimeStamp;
	if (type == "BOOLEAN")
		return TYPE::Boolean;
	if (type == "LOCALMKTDATE")
		return TYPE::LocalMktDate;
	if (type == "DATA")
		return TYPE::Data;
	if (type == "FLOAT")
		return TYPE::Float;
	if (type == "PRICEOFFSET")
		return TYPE::PriceOffset;
	if (type == "MONTHYEAR")
		return TYPE::MonthYear;
	if (type == "DAYOFMONTH")
		return TYPE::DayOfMonth;
	if (type == "UTCDATE")
		return TYPE::UtcDate;
	if (type == "UTCDATEONLY")
		return TYPE::UtcDateOnly;
	if (type == "UTCTIMEONLY")
		return TYPE::UtcTimeOnly;
	if (type == "NUMINGROUP")
		return TYPE::NumInGroup;
	if (type == "PERCENTAGE")
		return TYPE::Percentage;
	if (type == "SEQNUM")
		return TYPE::SeqNum;
	if (type == "LENGTH")
		return TYPE::Length;
	if (type == "COUNTRY")
		return TYPE::Country;
	if (type == "TIME")
		return TYPE::UtcTimeStamp;
	return TYPE::Unknown;
}
} // namespace FIX
