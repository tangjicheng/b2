#include "qfix/OEMessageStore.h"

#undef _BSD_SOURCE
#define _BSD_SOURCE
#include <endian.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cerrno>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <system_error>
#include <utility>

#include "qfix/SessionID.h"

namespace FIX {

namespace {

const char READ_ERROR[] = "Cannot read from file: ";
const char WRITE_ERROR[] = "Cannot write to file: ";
const char OPEN_ERROR[] = "Cannot open file: ";
const char CLOSE_ERROR[] = "Cannot close file: ";
const char FLUSH_ERROR[] = "Cannot flush file: ";
const char UNLINK_ERROR[] = "Cannot unlink file: ";
const char SEEK_ERROR[] = "Cannot seek into file: ";
const char TELL_ERROR[] = "Cannot get file position: ";
const char DUP_ERROR[] = "Cannot duplicate file descriptor: ";
const char FD_OPEN_ERROR[] = "Cannot associate stream with file descriptor: ";

void mkdirPath(const char* path)
{
	const char* end = path + strlen(path);
	std::string dir;
	for (const char* pos = path; pos != end; ++pos) {
		dir += *pos;
		if (*pos == '/' || pos + 1 == end)
			mkdir(dir.c_str(), 0777);
	}
}

std::string correctPath(const std::string& path)
{
	if (path.empty())
		return "./";
	else if (*path.rbegin() == '/')
		return path;
	else
		return path + '/';
}

} // namespace

OEMessageStoreFactory::OEMessageStoreFactory(const std::string& path)
	: m_path(path)
{
}

OEMessageStoreFactory::OEMessageStoreFactory(const SessionSettings& settings)
	: m_settings(settings)
{
}

MessageStore* OEMessageStoreFactory::create(const SessionID& sessionID)
{
	if (!m_path.empty())
		return new OEMessageStore(m_path, sessionID);
	else
		return new OEMessageStore(
			m_settings.get(sessionID).getString(FILE_STORE_PATH), sessionID);
}

void OEMessageStoreFactory::destroy(MessageStore* store)
{
	delete store;
}

OEMessageStore::OEMessageStore(
	const std::string& path, const SessionID& sessionID)
try : m_fd(-1), m_aio(2, 256 * 1024), m_nextSenderMsgSeqNum(1),
	m_nextTargetMsgSeqNum(1) {
	mkdirPath(path.c_str());
	std::string basename(correctPath(path));
	basename += sessionID.getBeginString().getString();
	basename += '-';
	basename += sessionID.getSenderCompID().getString();
	basename += '-';
	basename += sessionID.getTargetCompID().getString();
	if (!sessionID.getSessionQualifier().empty()) {
		basename += '-';
		basename += sessionID.getSessionQualifier();
	}
	m_filename = basename + ".dat";
	try {
		open(false);
	}
	catch (const IOException& e) {
		throw ConfigError(e.what());
	}
}
catch (const std::invalid_argument& e) {
	throw ConfigError(e.what());
}

OEMessageStore::~OEMessageStore()
{
	try {
		close();
	}
	catch (...) {
	}
}

void OEMessageStore::open(bool deleteFile)
{
	close();
	if (deleteFile) {
		if (unlink(m_filename.c_str()))
			throw IOException(UNLINK_ERROR + m_filename);
	}
	if ((m_fd = ::open(
			 m_filename.c_str(), O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH))
		== -1) {
		if ((m_fd = ::open(m_filename.c_str(), O_CREAT | O_RDWR,
				 S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH))
			== -1)
			throw IOException(OPEN_ERROR + m_filename);
		try {
			m_aio.attach(m_fd);
			writeCreationTime();
			writeSeqNums();
			writeNullMsg();
		}
		catch (const std::system_error& e) {
			throw IOException(e.code().message());
		}
	}
	else {
		m_aio.attach(m_fd);
		readCreationTime();
		readSeqNums();
		off_t end = lseek(m_fd, 0, SEEK_END);
		if (end == (off_t)-1)
			throw IOException(SEEK_ERROR + m_filename);
		m_aio.seek(end);
	}
}

void OEMessageStore::close()
{
	if (m_fd == -1)
		return;
	try {
		m_aio.seek(0);
		writeCreationTime();
		writeSeqNums();
		m_aio.wait();
	}
	catch (const std::system_error& e) {
		throw IOException(e.code().message());
	}
	while (::close(m_fd) == -1 && errno == EINTR)
		;
	m_fd = -1;
}

void OEMessageStore::writeCreationTime()
{
	uint64_t creTime
		= htobe64(static_cast<uint64_t>(m_creationTime.getTimeT()));
	m_aio.add(&creTime, sizeof(uint64_t));
}

void OEMessageStore::writeSeqNums()
{
	uint64_t senderSeqNum
		= htobe64(static_cast<uint64_t>(m_nextSenderMsgSeqNum));
	m_aio.add(&senderSeqNum, sizeof(uint64_t));
	uint64_t targetSeqNum
		= htobe64(static_cast<uint64_t>(m_nextTargetMsgSeqNum));
	m_aio.add(&targetSeqNum, sizeof(uint64_t));
}

void OEMessageStore::writeNullMsg()
{
	uint64_t seqNum = 0;
	m_aio.add(&seqNum, sizeof(uint64_t));
	uint16_t msgLen = 0;
	m_aio.add(&msgLen, sizeof(uint16_t));
	uint16_t totLen = htobe16(static_cast<uint16_t>(
		sizeof(uint64_t) + sizeof(uint16_t) + sizeof(uint16_t)));
	m_aio.add(&totLen, sizeof(uint16_t));
}

void OEMessageStore::readCreationTime()
{
	uint64_t creTime;
	if (read(m_fd, &creTime, sizeof(uint64_t)) != (ssize_t)sizeof(uint64_t))
		throw IOException(READ_ERROR + m_filename);
	m_creationTime = UtcTimeStamp(static_cast<time_t>(be64toh(creTime)));
}

void OEMessageStore::readSeqNums()
{
	uint64_t senderSeqNum;
	if (read(m_fd, &senderSeqNum, sizeof(uint64_t))
		!= (ssize_t)sizeof(uint64_t))
		throw IOException(READ_ERROR + m_filename);
	m_nextSenderMsgSeqNum = static_cast<int>(be64toh(senderSeqNum));
	uint64_t targetSeqNum;
	if (read(m_fd, &targetSeqNum, sizeof(uint64_t))
		!= (ssize_t)sizeof(uint64_t))
		throw IOException(READ_ERROR + m_filename);
	m_nextTargetMsgSeqNum = static_cast<int>(be64toh(targetSeqNum));
}

bool OEMessageStore::set(int msgSeqNum, const std::string& msg)
{
	try {
		uint64_t seqNum = htobe64(static_cast<uint64_t>(msgSeqNum));
		m_aio.add(&seqNum, sizeof(uint64_t));
		uint16_t msgLen = htobe16(static_cast<uint16_t>(msg.size()));
		m_aio.add(&msgLen, sizeof(uint16_t));
		m_aio.add(msg.c_str(), msg.size());
		uint16_t totLen = htobe16(static_cast<uint16_t>(sizeof(uint64_t)
			+ sizeof(uint16_t) + msg.size() + sizeof(uint16_t)));
		m_aio.add(&totLen, sizeof(uint16_t));
	}
	catch (const std::system_error& e) {
		throw IOException(e.code().message());
	}
	return true;
}

void OEMessageStore::get(int beg, int end, std::vector<std::string>& res) const
{
	try {
		m_aio.wait();
	}
	catch (const std::system_error& e) {
		throw IOException(e.code().message());
	}
	res.clear();
	if (beg < 1 || end < beg)
		return;
	int fd = dup(m_fd);
	if (fd == -1)
		throw IOException(DUP_ERROR + m_filename);
	FILE* file = fdopen(fd, "r");
	if (file == nullptr) {
		while (::close(fd) == -1 && errno == EINTR)
			;
		throw IOException(FD_OPEN_ERROR + m_filename);
	}
	try {
		long start = find(beg, file);
		if (fseek(file, start, SEEK_SET) == -1)
			throw IOException(SEEK_ERROR + m_filename);
		while (true) {
			uint64_t seqNum;
			size_t items = fread(&seqNum, sizeof(uint64_t), 1, file);
			if (feof(file))
				break;
			if (items != (size_t)1 || ferror(file))
				throw IOException(READ_ERROR + m_filename);
			uint16_t msgLen;
			if (fread(&msgLen, sizeof(uint16_t), 1, file) != (size_t)1
				|| ferror(file))
				throw IOException(READ_ERROR + m_filename);
			size_t length = static_cast<size_t>(be16toh(msgLen));
			if (static_cast<int>(be64toh(seqNum)) > end)
				break;
			std::string msg(length, '\0');
			if (fread(&msg[0], sizeof(char), length, file) != length
				|| ferror(file))
				throw IOException(READ_ERROR + m_filename);
			res.push_back(std::move(msg));
			if (fseek(file, sizeof(uint16_t), SEEK_CUR) == -1)
				throw IOException(SEEK_ERROR + m_filename);
		}
	}
	catch (const IOException& e) {
		fclose(file);
		throw e;
	}
	if (fclose(file) == EOF)
		throw IOException(CLOSE_ERROR + m_filename);
}

// pre: beg > 0
long OEMessageStore::find(int beg, FILE* file) const
{
	if (fseek(file, -sizeof(uint16_t), SEEK_END) == -1)
		throw IOException(SEEK_ERROR + m_filename);
	while (true) {
		uint16_t totLen;
		if (fread(&totLen, sizeof(uint16_t), 1, file) != (size_t)1
			|| ferror(file))
			throw IOException(READ_ERROR + m_filename);
		long offset = static_cast<long>(be16toh(totLen));
		if (fseek(file, -offset, SEEK_CUR) == -1)
			throw IOException(SEEK_ERROR + m_filename);
		uint64_t seqNum;
		if (fread(&seqNum, sizeof(uint64_t), 1, file) != (size_t)1
			|| ferror(file))
			throw IOException(READ_ERROR + m_filename);
		if (static_cast<int>(be64toh(seqNum)) < beg) {
			long pos = ftell(file);
			if (pos == (long)-1)
				throw IOException(TELL_ERROR + m_filename);
			return pos - (long)sizeof(uint64_t) + offset;
		}
		if (fseek(file, -(sizeof(uint64_t) + sizeof(uint16_t)), SEEK_CUR) == -1)
			throw IOException(SEEK_ERROR + m_filename);
	}
}

void OEMessageStore::reset()
{
	m_nextSenderMsgSeqNum = 1;
	m_nextTargetMsgSeqNum = 1;
	m_creationTime.setCurrent();
	open(true);
}

void OEMessageStore::refresh() {}

} // namespace FIX
