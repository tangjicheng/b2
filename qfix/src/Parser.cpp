/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/Parser.h"

#include <stdio.h>
#include <string.h>

#include <algorithm>

#include "qfix/FieldConvertors.h"
#include "qfix/Utility.h"

namespace FIX {

constexpr char delimeter {'\001'};
constexpr size_t max_fix_length {1024};

Read_result Parser::read(int socket)
{
	if (begin_ != data_) {
		const auto size = this->size();
		if (size != 0u)
			memmove(data_, begin_, size);
		begin_ = data_;
		end_ = data_ + size;
	}
	if (is_full())
		return Read_result::again;

	const auto n = socket_recv(socket, end_, data_ + max_size() - end_);
	if (n == 0)
		return Read_result::closed;
	if (n == -1) {
		if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
			return Read_result::error;
		else
			return Read_result::again;
	}

	end_ += n;
	return Read_result::success;
}

Read_result Parser::read(const std::string& data)
{
	if (begin_ != data_) {
		const auto size = this->size();
		if (size != 0u)
			memmove(data_, begin_, size);
		begin_ = data_;
		end_ = data_ + size;
	}
	if (data.size() > static_cast<size_t>(data_ + max_size() - end_))
		return Read_result::again;

	memcpy(end_, data.c_str(), data.size());
	end_ += data.size();
	return Read_result::success;
}

bool Parser::extract_length(size_t& length, char*& pos)
{
	for (; pos < end_ - 2; ++pos) {
		if (*pos == delimeter && *(pos + 1) == '9' && *(pos + 2) == '=')
			break;
	}
	if (pos >= (end_ - 2))
		return false;
	pos += 3;
	char* endp = 0;
	length = strtol(pos, &endp, 10);
	if (length <= 0 || *endp != delimeter) {
		if (endp != end_ && *endp != 0)
			throw MessageParseError {};
		return false;
	}
	if (length >= max_fix_length)
		throw MessageParseError {};

	pos = endp + 1;
	return true;
}

bool Parser::extract_fix_message(std::string& str)
{
	char* pos = nullptr;

	if (size() < 2)
		return false;
	for (pos = begin_; pos < end_ - 1; ++pos) {
		if (*pos == '8' && *(pos + 1) == '=')
			break;
	}
	if (pos >= (end_ - 1))
		return false;
	begin_ = pos;
	auto length = size_t {0u};
	try {
		if (extract_length(length, pos)) {
			pos += length;
			if (end_ <= pos)
				return false;

			pos -= 1;
			for (; pos < end_ - 3; ++pos) {
				if (*pos == delimeter && *(pos + 1) == '1' && *(pos + 2) == '0'
					&& *(pos + 3) == '=')
					break;
			}
			if (pos >= (end_ - 3))
				return false;
			pos += 4;

			for (; pos < end_; ++pos) {
				if (*pos == delimeter)
					break;
			}
			if (pos >= end_)
				return false;
			pos += 1;

			str.assign(begin_, pos - begin_);
			begin_ = pos;
			return true;
		}
	}
	catch (MessageParseError& e) {
		if (length > 0)
			begin_ = pos + length;
		else
			clear();

		throw e;
	}

	return false;
}

void Parser::clear()
{
	begin_ = data_;
	end_ = data_;
}

} // namespace FIX
