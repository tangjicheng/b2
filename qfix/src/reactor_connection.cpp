#include "qfix/reactor_connection.h"

#include "qfix/Session.h"
#include "qfix/Utility.h"

namespace FIX {

Reactor_connection::Reactor_connection(std::int32_t socket, Sessions sessions,
	Log* logger, misc::ThreadedReactor& reactor)
	: socket_ {socket}
	, logger_ {logger}
	, sessions_ {sessions}
	, session_ {0}
	, disconnect_ {false}
	, reactor_ {&reactor}
{
	const auto [address, source_port] = socket_peer_name(socket_);
	source_port_ = source_port;
	source_address_.assign(address);
	buffer_string_.reserve(BUFSIZ);
}

std::string Reactor_connection::getRemoteAddress() const
{
	std::stringstream stream;
	stream << source_address_ << ':' << source_port_;
	return stream.str();
}

bool Reactor_connection::send(const std::string& msg)
{
	auto total_sent = std::size_t {0};
	auto retries = 5u;
	while (total_sent < msg.length()) {
		const auto sent = socket_send(
			socket_, msg.c_str() + total_sent, msg.length() - total_sent);
		if (sent == 0) {
			--retries;
			if (retries == 0)
				return false;
			reactor_->idle(false);
			continue;
		}
		if (sent < 0)
			return false;
		total_sent += sent;
		retries = 5u;
	}
	return true;
}

void Reactor_connection::schedule_disconnect()
{
	reactor_->callLater([this]() { session_disconnect(); });
}

bool Reactor_connection::connect()
{
	// do the bind in the thread as name resolution may block
	if (!source_address_.empty() || source_port_)
		socket_bind(socket_, source_address_.c_str(), source_port_);
	return (socket_connect(get_socket(), address_.c_str(), port_) >= 0);
}

void Reactor_connection::disconnect()
{
	if (disconnect_)
		return;

	disconnect_ = true;

	reactor_->removeCall(heartbeat_call_id_);
	reactor_->removeCall(read_call_id_);
	reactor_->removeCall(exception_call_id_);
	if (session_) {
		session_->setResponder(0);
		Session::unregisterSession(session_->getSessionID());
	}
	if (logger_) {
		std::stringstream stream;
		stream << "Connection closed on socket " << get_socket();
		logger_->onEvent(stream.str());
	}
	socket_close(socket_);
}

void Reactor_connection::session_disconnect()
{
	if (disconnect_)
		return;
	if (session_ && session_->isLoggedOn())
		session_->disconnect(); // will call Reactor_connection::disconnect()
	else
		disconnect();
}

void Reactor_connection::start()
{
	if (logger_) {
		std::stringstream stream;
		stream << "Connection open on socket " << get_socket();
		logger_->onEvent(stream.str());
	}
	reactor_->callLater([this]() {
		if (reactor_->supportsIoCalls()) {
			exception_call_id_
				= reactor_->callOnException(get_socket(), [this](int) {
					  session_disconnect();
					  return misc::Reactor::CallStatus::OK;
				  });
			read_call_id_ = reactor_->callOnRead(get_socket(), [this](int) {
				if (disconnect_)
					return misc::Reactor::CallStatus::OK;
				if (read_socket())
					reactor_->callLater([this]() { io_process_read_buffer(); });
				return misc::Reactor::CallStatus::OK;
			});
			heartbeat_call_id_ = reactor_->callEvery(
				std::chrono::milliseconds {1000}, [this]() {
					if (disconnect_)
						return misc::Reactor::CallStatus::OK;
					keep_alive();
					return misc::Reactor::CallStatus::OK;
				});
		}
		else {
			read_call_id_ = reactor_->callInLoop([this]() {
				if (!disconnect_)
					read_socket();
				if (!disconnect_)
					keep_alive();
				if (!disconnect_)
					process_read_buffer();
				return misc::Reactor::CallStatus::OK;
			});
		}
	});
}

void Reactor_connection::shutdown()
{
	reactor_->callNow([this]() { session_disconnect(); });
	reactor_->callNow([] {});
}

bool Reactor_connection::read_socket()
{
	const auto result = parser_.read(socket_);
	if (result == Read_result::error || result == Read_result::closed) {
		session_disconnect();
		return false;
	}
	if (result == Read_result::again)
		return false;
	return true;
}

void Reactor_connection::io_process_read_buffer()
{
	if (disconnect_)
		return;
	if (process_read_buffer())
		reactor_->callLater([this]() { io_process_read_buffer(); });
}

bool Reactor_connection::process_read_buffer()
{
	if (session_ && !session_->ready_to_read())
		return true;
	return process_stream();
}

void Reactor_connection::keep_alive()
{
	if (disconnect_)
		return;
	if (session_ && session_->ready_to_read())
		session_->next();
}

bool Reactor_connection::read_message(std::string& msg)
{
	try {
		return parser_.extract_fix_message(msg);
	}
	catch (MessageParseError&) {
	}
	return true;
}

bool Reactor_connection::process_stream()
{
	buffer_string_.resize(0);
	if (read_message(buffer_string_)) {
		if (!session_) {
			if (!set_session(buffer_string_)) {
				disconnect();
				return false;
			}
		}
		try {
			session_->next(buffer_string_);
		}
		catch (InvalidMessage&) {
			if (!session_->isLoggedOn()) {
				disconnect();
				return false;
			}
		}
		return true;
	}
	return false;
}

bool Reactor_connection::set_session(const std::string& msg)
{
	session_ = Session::lookupSession(msg, true);
	if (!session_) {
		if (logger_) {
			logger_->onEvent("Session not found for incoming message: " + msg,
				misc::APP_LOG_ERR);
			logger_->onIncoming(msg);
		}
		return false;
	}

	const auto& session_id = session_->getSessionID();
	session_ = 0;

	if (!Session::isSessionRegistered(session_id))
		session_ = Session::registerSession(session_id);

	if (!session_)
		return false;
	if (sessions_.find(session_->getSessionID()) == sessions_.end())
		return false;

	session_->setResponder(this);
	return true;
}

} // namespace FIX
