#include "qfix/reactor_acceptor.h"

#include <sstream>

#include <misc/Address.h>
#include <misc/ThreadedReactor.h>
#include <misc/thread.h>

#include "qfix/Settings.h"
#include "qfix/Utility.h"

namespace FIX {

Reactor_acceptor::Reactor_acceptor(Application& application,
	MessageStoreFactory& store_factory, const SessionSettings& settings,
	LogFactory& logger_factory, Acquire_reactor_callback acquire_reactor,
	const std::string& acceptor_reactor_group,
	const std::string& reader_reactor_group)
	: Acceptor {application, store_factory, settings, logger_factory}
	, acquire_reactor_ {acquire_reactor}
	, acceptor_reactor_group_ {acceptor_reactor_group}
	, reader_reactor_group_ {reader_reactor_group}
{
	socket_init();
}

Reactor_acceptor::~Reactor_acceptor()
{
	socket_term();
}

void Reactor_acceptor::onConfigure(const SessionSettings& s)
{
	auto sessions = s.getSessions();
	for (auto i = sessions.begin(); i != sessions.end(); ++i) {
		const auto& settings = s.get(*i);
		settings.getLong(SOCKET_ACCEPT_PORT);
		if (settings.has(SOCKET_REUSE_ADDRESS))
			settings.getBool(SOCKET_REUSE_ADDRESS);
		if (settings.has(SOCKET_NODELAY))
			settings.getBool(SOCKET_NODELAY);
	}
}

void Reactor_acceptor::onInitialize(const SessionSettings& s)
{
	auto ip_ports = std::set<Ip_port> {};
	auto sessions = s.getSessions();
	for (auto i = sessions.begin(); i != sessions.end(); ++i) {
		const Dictionary& settings = s.get(*i);
		const auto port
			= static_cast<std::uint16_t>(settings.getLong(SOCKET_ACCEPT_PORT));
		const auto host = settings.getString(SOCKET_ACCEPT_HOST);
		const auto [host_int, valid_address]
			= misc::io::ip_address_to_int(host);
		if (!valid_address)
			throw RuntimeError {std::string {"FIX. Invalid address on: "}
				+ host.c_str() + ":" + std::to_string(port)};
		const auto ip_port = Ip_port {host_int, port};
		ip_port_to_sessions_[ip_port].insert(*i);
		if (ip_ports.find(ip_port) != ip_ports.end())
			continue;
		ip_ports.insert(ip_port);

		const auto reuse_address = settings.has(SOCKET_REUSE_ADDRESS)
			? settings.getBool(SOCKET_REUSE_ADDRESS)
			: true;

		const auto no_delay = settings.has(SOCKET_NODELAY)
			? settings.getBool(SOCKET_NODELAY)
			: false;

		const auto send_buf_size = settings.has(SOCKET_SEND_BUFFER_SIZE)
			? settings.getInt(SOCKET_SEND_BUFFER_SIZE)
			: 0;

		const auto rcv_buf_size = settings.has(SOCKET_RECEIVE_BUFFER_SIZE)
			? settings.getInt(SOCKET_RECEIVE_BUFFER_SIZE)
			: 0;

		const auto [socket, error]
			= socket_createAcceptor(port, reuse_address, host.c_str());
		if (error != Error_code::no_error) {
			SocketException e;
			socket_close(socket);
			throw RuntimeError {std::string {} + to_string(error) + " for "
				+ host.c_str() + ":" + std::to_string(port) + " (" + e.what()
				+ ")"};
		}
		if (no_delay) {
			if (socket_setsockopt(socket, TCP_NODELAY) == -1)
				print_warning(socket);
		}
		if (send_buf_size) {
			if (socket_setsockopt(socket, SO_SNDBUF, send_buf_size) == -1)
				print_warning(socket);
		}
		if (rcv_buf_size) {
			if (socket_setsockopt(socket, SO_RCVBUF, rcv_buf_size) == -1)
				print_warning(socket);
		}
		if (socket_setnonblock(socket) == -1)
			print_warning(socket);

		socket_to_ip_port_[socket] = ip_port;
		sockets_.insert(socket);
	}
}

void Reactor_acceptor::onStart()
{
	for (auto i = sockets_.begin(); i != sockets_.end(); ++i) {
		const auto socket = *i;
		const auto ip_port = socket_to_ip_port_[socket];
		auto& acceptor_reactor = acquire_reactor_(
			acceptor_reactor_group_, std::to_string(ip_port.second));

		auto settings = Connection_settings {};
		if (socket_getsockopt(socket, TCP_NODELAY, settings.no_delay) == -1)
			print_warning(socket);
		if (socket_getsockopt(socket, SO_SNDBUF, settings.send_buf_size) == -1)
			print_warning(socket);
		if (socket_getsockopt(socket, SO_RCVBUF, settings.rcv_buf_size) == -1)
			print_warning(socket);

		misc::Reactor::CallId call_id;
		if (acceptor_reactor.supportsIoCalls()) {
			call_id = acceptor_reactor.callOnRead(
				*i, [this, socket, ip_port, settings](int) {
					return try_accept(socket, ip_port, settings);
				});
		}
		else {
			call_id = acceptor_reactor.callInLoop(
				[this, socket, ip_port, settings]() {
					return try_accept(socket, ip_port, settings);
				});
		}
		add_acceptor_call_id(*i, call_id);
	}
}

bool Reactor_acceptor::onPoll()
{
	return false;
}

void Reactor_acceptor::onStop()
{
	for (auto [socket, call_id] : acceptor_calls_) {
		const auto ip_port = socket_to_ip_port_[socket];
		auto& acceptor_reactor = acquire_reactor_(
			acceptor_reactor_group_, std::to_string(ip_port.second));
		acceptor_reactor.removeCall(acceptor_calls_[socket]);
		acceptor_reactor.callNow([]() {});
		socket_close(socket);
		std::stringstream stream;
		stream << "Acceptor closed on socket " << socket;
		getLog()->onEvent(stream.str());
	}
	for (auto [socket, connection] : connections_) {
		connection->shutdown();
		delete connection;
	}
}

void Reactor_acceptor::add_acceptor_call_id(
	std::int32_t socket, misc::Reactor::CallId call_id)
{
	acceptor_calls_[socket] = call_id;
	std::stringstream stream;
	stream << "Acceptor open on socket " << socket;
	getLog()->onEvent(stream.str());
}

misc::Reactor::CallStatus Reactor_acceptor::try_accept(
	std::int32_t acceptor_socket, Ip_port ip_port, Connection_settings settings)
{
	if (isStopped())
		return misc::Reactor::CallStatus::OK;

	auto socket = 0;
	if ((socket = socket_accept(acceptor_socket)) != -1) {
		if (settings.no_delay) {
			if (socket_setsockopt(socket, TCP_NODELAY) == -1)
				print_warning(socket);
		}
		if (settings.send_buf_size) {
			if (socket_setsockopt(socket, SO_SNDBUF, settings.send_buf_size)
				== -1)
				print_warning(socket);
		}
		if (settings.rcv_buf_size) {
			if (socket_setsockopt(socket, SO_RCVBUF, settings.rcv_buf_size)
				== -1)
				print_warning(socket);
		}
		if (socket_setnonblock(socket) == -1)
			print_warning(socket);

		Sessions sessions = ip_port_to_sessions_[ip_port];
		auto& connection_reactor = acquire_reactor_(
			reader_reactor_group_, std::to_string(ip_port.second));
		Reactor_connection* connection = nullptr;
		try {
			connection = new Reactor_connection {
				socket, sessions, getLog(), connection_reactor};
		}
		catch (const SocketException& e) {
			if (getLog())
				getLog()->onEvent(e.what());
			socket_close(socket);
			std::stringstream stream;
			stream << "Socket accept exception on port " << ip_port.second
				   << " (fd=" << socket << ")";
			getLog()->onEvent(stream.str(), misc::APP_LOG_ERR);
			return misc::Reactor::CallStatus::OK;
		}
		auto old_connection = connections_.find(socket);
		if (old_connection != connections_.end()) {
			connection_reactor.callNow([]() {});
			delete old_connection->second;
		}
		connections_[socket] = connection;
		connection->start();
	}
	return misc::Reactor::CallStatus::OK;
}

void Reactor_acceptor::print_warning(std::int32_t socket)
{
	const auto log = getLog();
	if (!log)
		return;
	const auto error = errno;
	std::stringstream stream;
	stream << "FIX warning (" << strerror(error) << ") on socket " << socket
		   << "";
	log->onEvent(stream.str(), misc::APP_LOG_WARNING);
}

} // namespace FIX
