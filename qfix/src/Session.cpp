/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/Session.h"

#include <algorithm>
#include <iostream>

#include "qfix/Values.h"

namespace FIX {
Session::Sessions Session::s_sessions;
Session::SessionIDs Session::s_sessionIDs;
Session::Sessions Session::s_registered;
std::mutex Session::s_mutex;

#define LOGEX(method)                                 \
	try {                                             \
		method;                                       \
	}                                                 \
	catch (std::exception & e) {                      \
		m_state.onEvent(e.what(), misc::APP_LOG_ERR); \
	}

Session::Session(Application& application,
	MessageStoreFactory& messageStoreFactory, const SessionID& sessionID,
	const DataDictionary& dataDictionary, int heartBtInt,
	LogFactory* pLogFactory)
	: m_application(application)
	, m_sessionID(sessionID)
	, m_sendRedundantResendRequests(false)
	, m_checkCompId(true)
	, m_checkLatency(true)
	, m_maxLatency(120)
	, m_resetOnLogon(false)
	, m_millisecondsInTimeStamp(true)
	, m_persistMessages(true)
	, m_dataDictionary(dataDictionary)
	, m_messageStoreFactory(messageStoreFactory)
	, m_pLogFactory(pLogFactory)
	, m_pResponder(0)
{
	m_state.heartBtInt(HeartBtInt(heartBtInt));
	m_state.initiate(heartBtInt != 0);
	m_state.store(m_messageStoreFactory.create(m_sessionID));
	if (m_pLogFactory)
		m_state.log(m_pLogFactory->create(m_sessionID));

	addSession(*this);
	m_application.onCreate(m_sessionID); // must set handler_
	if (!handler_)
		throw std::runtime_error("Application must set a handler");
	m_state.onEvent("Created session");
}

Session::~Session()
{
	m_application.onDestroy(m_sessionID);
	removeSession(*this);
	m_messageStoreFactory.destroy(m_state.store());
	if (m_pLogFactory && m_state.log())
		m_pLogFactory->destroy(m_state.log());
}

void Session::insertSendingTime(Header& header, const UtcTimeStamp& now) const
{
	bool showMilliseconds = m_sessionID.getBeginString() >= BeginString_FIX42;
	header.setField(
		SendingTime(now, showMilliseconds && m_millisecondsInTimeStamp));
}

void Session::insertOrigSendingTime(
	Header& header, const UtcTimeStamp& when) const
{
	bool showMilliseconds = m_sessionID.getBeginString() >= BeginString_FIX42;
	header.setField(
		OrigSendingTime(when, showMilliseconds && m_millisecondsInTimeStamp));
}

void Session::fill(Header& header)
{
	UtcTimeStamp now;
	m_state.lastSentTime(now);
	header.setField(m_sessionID.getBeginString());
	header.setField(m_sessionID.getSenderCompID());
	header.setField(m_sessionID.getTargetCompID());
	insertSendingTime(header, now);
}

void Session::next()
{
	try {
		if (!isEnabled()) {
			if (isLoggedOn()) {
				if (!m_state.sentLogout()) {
					m_state.onEvent("Initiated logout request");
					generateLogout(m_state.logoutReason());
				}
			}
			else
				return;
		}

		if (!m_state.receivedLogon()) {
			if (m_state.shouldSendLogon()) {
				generateLogon();
				m_state.onEvent("Initiated logon request");
			}
			else if (m_state.alreadySentLogon() && m_state.logonTimedOut()) {
				m_state.onEvent(
					"Timed out waiting for logon response", misc::APP_LOG_ERR);
				disconnect();
			}
			return;
		}

		if (m_state.heartBtInt() == 0)
			return;

		if (m_state.logoutTimedOut()) {
			m_state.onEvent(
				"Timed out waiting for logout response", misc::APP_LOG_ERR);
			disconnect();
		}

		if (m_state.withinHeartBeat())
			return;

		if (m_state.timedOut()) {
			m_state.onEvent(
				"Timed out waiting for heartbeat", misc::APP_LOG_ERR);
			disconnect();
		}
		else {
			if (m_state.needTestRequest()) {
				generateTestRequest("TEST");
				m_state.testRequest(m_state.testRequest() + 1);
				m_state.onEvent("Sent test request TEST");
			}
			else if (m_state.needHeartbeat()) {
				generateHeartbeat();
			}
		}
	}
	catch (FIX::IOException& e) {
		m_state.onEvent(e.what(), misc::APP_LOG_ERR);
		disconnect();
	}
}

void Session::set_auth_wait()
{
	auth_result_.store(Auth_result::WAITING, std::memory_order_seq_cst);
}

void Session::wait_for_auth_ack() const
{
	while (
		auth_result_.load(std::memory_order_seq_cst) == Auth_result::WAITING) {
		if (!m_state.enabled()) {
			throw GatewayError {"Timeout waiting for ME response."};
		}
		handler_->idle();
	}
}

void Session::ack_auth(Auth_result auth_result)
{
	auth_result_.store(auth_result, std::memory_order_seq_cst);
}

void Session::set_disconnect_wait()
{
	disconnect_result_.store(
		Disconnect_result::WAITING, std::memory_order_seq_cst);
}

void Session::wait_for_disconnect_ack() const
{
	while (disconnect_result_.load(std::memory_order_seq_cst)
		== Disconnect_result::WAITING) {
		if (!m_state.enabled()) {
			throw GatewayError {"Timeout waiting for ME response."};
		}
		handler_->idle();
	}
}

void Session::ack_disconnect(Disconnect_result disconnect_result)
{
	disconnect_result_.store(disconnect_result, std::memory_order_seq_cst);
}

void Session::set_reset_wait()
{
	reset_result_.store(Reset_result::WAITING, std::memory_order_seq_cst);
}

void Session::wait_for_reset_ack() const
{
	while (reset_result_.load(std::memory_order_seq_cst)
		== Reset_result::WAITING) {
		if (!m_state.enabled()) {
			throw GatewayError {"Timeout waiting for ME response."};
		}
		handler_->idle();
	}
}

void Session::ack_reset(Reset_result reset_result)
{
	reset_result_.store(reset_result, std::memory_order_seq_cst);
}

void Session::on_incoming_message()
{
	if (get_replication_mode())
		handler_->on_incoming_message();
	else
		ack_incoming_message();
}

void Session::on_incoming_reset(std::int32_t seq_num)
{
	if (get_replication_mode())
		handler_->on_incoming_reset(seq_num);
	else
		ack_incoming_reset(seq_num);
}

void Session::on_outgoing_message(Message& msg)
{
	if (get_replication_mode())
		handler_->on_outgoing_message(msg);
	else
		ack_outgoing_message(msg);
}

void Session::on_outgoing_reset()
{
	if (get_replication_mode())
		handler_->on_outgoing_reset();
	else
		ack_outgoing_reset();
}

void Session::ack_incoming_message()
{
	if (!isLoggedOn())
		m_state.incrNextTargetMsgSeqNum();
}

void Session::ack_incoming_reset(std::int32_t seq_num)
{
	if (!isLoggedOn())
		m_state.setNextTargetMsgSeqNum(seq_num);
}

void Session::ack_outgoing_message(Message& msg)
{
	sendRaw(msg, 0);
}

void Session::ack_outgoing_reset()
{
	const auto next_target = m_state.getNextTargetMsgSeqNum();
	m_state.reset();
	m_state.setNextTargetMsgSeqNum(next_target);
}

bool Session::ready_to_read() const
{
	return handler_->ready_to_read();
}

void Session::nextLogon(const Message& logon, const UtcTimeStamp& now)
{
	(void)FIELD_GET_REF(logon.getHeader(), SenderCompID);
	(void)FIELD_GET_REF(logon.getHeader(), TargetCompID);

	// This needs to happen before we react to this message in any meaningful
	// way, to prevent unauthorised users from (for example) resetting stores.
	handler_->onAuth(logon);

	wait_for_auth_ack();

	const auto auth_result = auth_result_.load(std::memory_order_seq_cst);
	if (auth_result == Auth_result::REJECTED)
		throw RejectLogon {};
	else if (auth_result == Auth_result::ALREADY_LOGGED_ON)
		throw Already_logged_on {};

	ResetSeqNumFlag resetSeqNumFlag(false);
	if (logon.isSetField(resetSeqNumFlag))
		logon.getField(resetSeqNumFlag);
	m_state.receivedReset(resetSeqNumFlag.getValue());

	if (m_state.receivedReset()) {
		m_state.onEvent(
			"Logon contains ResetSeqNumFlag=Y, reseting sequence numbers to 1");
		if (!m_state.sentReset()) {
			m_state.setNextTargetMsgSeqNum(1);
			on_incoming_reset(1);
			on_outgoing_reset();
			wait_for_reset_ack();
		}
	}

	if (m_state.shouldSendLogon() && !m_state.receivedReset()) {
		m_state.onEvent("Received logon response before sending request",
			misc::APP_LOG_ERR);
		disconnect();
		return;
	}

	if (!verify(logon, now, false, true))
		return;
	m_state.receivedLogon(true);

	if (!m_state.initiate()
		|| (m_state.sentReset() && !m_state.receivedReset())) {
		if (logon.isSetField(m_state.heartBtInt()))
			logon.getField(m_state.heartBtInt());
		m_state.onEvent("Received logon request");
		generateLogon(logon);
		m_state.onEvent("Responding to logon request");
	}
	else
		m_state.onEvent("Received logon response");

	m_state.sentReset(false);
	m_state.receivedReset(false);

	MsgSeqNum msgSeqNum;
	logon.getHeader().getField(msgSeqNum);
	if (isTargetTooHigh(msgSeqNum.getValue()) && !resetSeqNumFlag) {
		doTargetTooHigh(logon);
	}
	else {
		m_state.incrNextTargetMsgSeqNum();
		on_incoming_message();
		nextQueued();
	}

	if (isLoggedOn())
		handler_->onLogon();
}

void Session::nextHeartbeat(const Message& heartbeat, const UtcTimeStamp& now)
{
	if (!verify(heartbeat, now))
		return;
	m_state.incrNextTargetMsgSeqNum();
	on_incoming_message();
	nextQueued();
}

void Session::nextTestRequest(
	const Message& testRequest, const UtcTimeStamp& now)
{
	if (!verify(testRequest, now))
		return;
	generateHeartbeat(testRequest);
	m_state.incrNextTargetMsgSeqNum();
	on_incoming_message();
	nextQueued();
}

void Session::nextLogout(const Message& logout, const UtcTimeStamp& now)
{
	if (!verify(logout, now, false, false))
		return;
	if (!m_state.sentLogout()) {
		m_state.onEvent("Received logout request");
		generateLogout();
		m_state.onEvent("Sending logout response");
	}
	else
		m_state.onEvent("Received logout response");

	if (!m_state.resendRequested()) {
		m_state.incrNextTargetMsgSeqNum();
		on_incoming_message();
	}
	disconnect();
}

void Session::nextReject(const Message& reject, const UtcTimeStamp& now)
{
	if (!verify(reject, now, false, true))
		return;
	m_state.incrNextTargetMsgSeqNum();
	on_incoming_message();
	nextQueued();
}

void Session::nextSequenceReset(
	const Message& sequenceReset, const UtcTimeStamp& now)
{
	bool isGapFill = false;
	GapFillFlag gapFillFlag;
	if (sequenceReset.isSetField(gapFillFlag)) {
		sequenceReset.getField(gapFillFlag);
		isGapFill = gapFillFlag.getValue();
	}

	if (!verify(sequenceReset, now, isGapFill, isGapFill))
		return;

	NewSeqNo newSeqNo;
	if (sequenceReset.isSetField(newSeqNo)) {
		sequenceReset.getField(newSeqNo);

		m_state.onEvent("Received SequenceReset FROM: "
			+ IntConvertor::convert(getExpectedTargetNum())
			+ " TO: " + newSeqNo.getString());

		if (newSeqNo > getExpectedTargetNum()) {
			m_state.setNextTargetMsgSeqNum(newSeqNo.getValue());
			on_incoming_reset(newSeqNo.getValue());
		}
		else if (newSeqNo < getExpectedTargetNum())
			generateReject(
				sequenceReset, SessionRejectReason_VALUE_IS_INCORRECT);
	}
}

void Session::nextResendRequest(
	const Message& resendRequest, const UtcTimeStamp& now)
{
	if (!verify(resendRequest, now, false, false))
		return;

	std::lock_guard lock(m_mutex);

	BeginSeqNo beginSeqNo;
	EndSeqNo endSeqNo;
	resendRequest.getField(beginSeqNo);
	resendRequest.getField(endSeqNo);

	m_state.onEvent("Received ResendRequest FROM: " + beginSeqNo.getString()
		+ " TO: " + endSeqNo.getString());

	std::string beginString = m_sessionID.getBeginString().getValue();
	if ((beginString >= FIX::BeginString_FIX42 && endSeqNo == 0)
		|| (beginString <= FIX::BeginString_FIX42 && endSeqNo == 999999)
		|| endSeqNo >= getExpectedSenderNum()) {
		endSeqNo = EndSeqNo(getExpectedSenderNum() - 1);
	}

	m_state.onEvent("Resending Messages FROM: " + beginSeqNo.getString()
		+ " TO: " + endSeqNo.getString());

	if (!m_persistMessages) {
		endSeqNo = EndSeqNo(endSeqNo.getValue() + 1);
		int next = m_state.getNextSenderMsgSeqNum();
		if (endSeqNo > next)
			endSeqNo = EndSeqNo(next);
		generateSequenceReset(beginSeqNo.getValue(), endSeqNo.getValue());
		return;
	}

	std::vector<std::string> messages;
	m_state.get(beginSeqNo.getValue(), endSeqNo.getValue(), messages);

	std::vector<std::string>::iterator i;
	MsgSeqNum msgSeqNum(0);
	MsgType msgType;
	int count = 0;
	int begin = 0;
	int current = beginSeqNo.getValue();
	std::string messageString;

	for (i = messages.begin(); i != messages.end(); ++i) {
		Message msg(*i, m_dataDictionary);
		msg.getHeader().getField(msgSeqNum);
		msg.getHeader().getField(msgType);

		if ((current != msgSeqNum) && !begin)
			begin = current;

		if (Message::isAdminMsgType(msgType)) {
			if (!begin)
				begin = msgSeqNum.getValue();
		}
		else {
			if (resend(msg)) {
				if (begin)
					generateSequenceReset(begin, msgSeqNum.getValue());
				send(msg.toString(messageString));
				++count;
				begin = 0;
			}
			else {
				if (!begin)
					begin = msgSeqNum.getValue();
			}
		}
		current = msgSeqNum.getValue() + 1;
	}
	if (begin) {
		generateSequenceReset(begin, msgSeqNum.getValue() + 1);
	}

	if (endSeqNo > msgSeqNum) {
		endSeqNo = EndSeqNo(endSeqNo.getValue() + 1);
		int next = m_state.getNextSenderMsgSeqNum();
		if (endSeqNo > next)
			endSeqNo = EndSeqNo(next);
		generateSequenceReset(beginSeqNo.getValue(), endSeqNo.getValue());
	}

	m_state.onEvent("Resent " + std::to_string(count) + " Messages");

	resendRequest.getHeader().getField(msgSeqNum);
	if (!isTargetTooHigh(msgSeqNum.getValue())
		&& !isTargetTooLow(msgSeqNum.getValue())) {
		m_state.incrNextTargetMsgSeqNum();
		on_incoming_message();
	}
}

bool Session::send(Message& message)
{
	message.getHeader().removeField(FIELD::PossDupFlag);
	message.getHeader().removeField(FIELD::OrigSendingTime);
	return sendRaw(message, 0);
}

bool Session::sendRaw(Message& message, int num)
{
	try {
		Header& header = message.getHeader();

		const MsgType& msgType = FIELD_GET_REF(header, MsgType);

		fill(header);
		std::string messageString;

		std::lock_guard lock(m_mutex);

		if (Message::isAdminMsgType(msgType)) {
			handler_->toAdmin(message);

			persist(message, num, messageString);

			if (msgType == "A" || msgType == "5" || msgType == "2"
				|| msgType == "4" || isLoggedOn()) {
				send(messageString);
			}
		}
		else {
			try {
				handler_->toApp(message);

				persist(message, num, messageString);

				if (isLoggedOn())
					send(messageString);
			}
			catch (DoNotSend&) {
				return false;
			}
		}

		return true;
	}
	catch (IOException& e) {
		m_state.onEvent(e.what(), misc::APP_LOG_ERR);
		return false;
	}
}

bool Session::send(const std::string& string)
{
	if (!m_pResponder)
		return false;
	m_state.onOutgoing(string);
	if (!m_pResponder->send(string)) {
		m_state.onEvent("Error on send. Disconnecting", misc::APP_LOG_ERR);
		m_pResponder->schedule_disconnect();
		return false;
	}
	return true;
}

void Session::disconnect()
{
	handler_->on_disconnect();
	try {
		wait_for_disconnect_ack();
	}
	catch (GatewayError&) {
		if (m_pResponder) {
			m_pResponder->disconnect();
			m_pResponder = 0;
		}
		return;
	}

	std::lock_guard lock(m_mutex);
	if (m_pResponder) {
		m_state.onEvent("Disconnecting");

		m_pResponder->disconnect();
		m_pResponder = 0;
	}

	if (m_state.receivedLogon() || m_state.sentLogon()) {
		m_state.receivedLogon(false);
		m_state.sentLogon(false);
		handler_->onLogout();
	}

	m_state.sentLogout(false);
	m_state.receivedReset(false);
	m_state.sentReset(false);
	m_state.clearQueue();
	m_state.logoutReason();

	m_state.resendRange(0, 0);
}

void Session::schedule_disconnect()
{
	if (m_pResponder)
		m_pResponder->schedule_disconnect();
}

void Session::disconnect_on_already_logged_on()
{
	std::lock_guard lock(m_mutex);
	if (m_pResponder) {
		m_state.onEvent("Disconnecting", misc::APP_LOG_ERR);

		m_pResponder->disconnect();
		m_pResponder = 0;
	}
}

bool Session::resend(Message& message)
{
	SendingTime sendingTime;
	MsgSeqNum msgSeqNum;
	Header& header = message.getHeader();
	header.getField(sendingTime);
	header.getField(msgSeqNum);
	insertOrigSendingTime(header, sendingTime.getValue());
	header.setField(PossDupFlag(true));
	insertSendingTime(header, UtcTimeStamp());

	try {
		handler_->onResend(message);
		return true;
	}
	catch (DoNotSend&) {
		return false;
	}
}

void Session::persist(Message& message, int num, std::string& messageString)
{
	Header& header = message.getHeader();
	if (num) {
		header.setField(MsgSeqNum(num));
		message.toString(messageString);
	}
	else {
		int num = getExpectedSenderNum();
		header.setField(MsgSeqNum(num));
		message.toString(messageString);
		if (m_persistMessages)
			m_state.set(num, messageString);
		m_state.incrNextSenderMsgSeqNum();
	}
}

void Session::generateLogon()
{
	Message logon;
	logon.getHeader().setField(MsgType("A"));
	logon.setField(EncryptMethod(0));
	logon.setField(m_state.heartBtInt());

	if (m_resetOnLogon) {
		m_state.setNextTargetMsgSeqNum(1);
		on_incoming_reset(1);
		on_outgoing_reset();
		wait_for_reset_ack();
	}

	std::string beginString = m_sessionID.getBeginString().getValue();
	if (beginString >= FIX::BeginString_FIX41 && (m_resetOnLogon)
		&& (getExpectedSenderNum() == 1) && (getExpectedTargetNum() == 1)) {
		logon.setField(ResetSeqNumFlag(true));
		m_state.sentReset(true);
	}

	UtcTimeStamp now;
	m_state.lastReceivedTime(now);
	m_state.testRequest(0);
	m_state.sentLogon(true);
	m_state.last_outgoing_message_time(now);
	on_outgoing_message(logon);
}

void Session::generateLogon(const Message& aLogon)
{
	Message logon;
	EncryptMethod encryptMethod;
	HeartBtInt heartBtInt;
	logon.setField(EncryptMethod(0));
	if (m_state.receivedReset())
		logon.setField(ResetSeqNumFlag(true));
	aLogon.getField(heartBtInt);
	logon.getHeader().setField(MsgType("A"));
	logon.setField(heartBtInt);
	m_state.last_outgoing_message_time(UtcTimeStamp {});
	on_outgoing_message(logon);
	m_state.sentLogon(true);
}

void Session::generateResendRequest(
	const BeginString& beginString, const MsgSeqNum& msgSeqNum)
{
	Message resendRequest;
	BeginSeqNo beginSeqNo((int)getExpectedTargetNum());
	EndSeqNo endSeqNo(msgSeqNum.getValue() - 1);
	if (beginString >= FIX::BeginString_FIX42)
		endSeqNo = EndSeqNo(0);
	else if (beginString <= FIX::BeginString_FIX41)
		endSeqNo = EndSeqNo(999999);
	resendRequest.getHeader().setField(MsgType("2"));
	resendRequest.setField(beginSeqNo);
	resendRequest.setField(endSeqNo);
	m_state.last_outgoing_message_time(UtcTimeStamp {});
	on_outgoing_message(resendRequest);

	m_state.onEvent("Sent ResendRequest FROM: " + beginSeqNo.getString()
		+ " TO: " + endSeqNo.getString());

	m_state.resendRange(beginSeqNo.getValue(), msgSeqNum.getValue() - 1);
}

void Session::generateSequenceReset(int beginSeqNo, int endSeqNo)
{
	Message sequenceReset;
	NewSeqNo newSeqNo(endSeqNo);
	sequenceReset.getHeader().setField(MsgType("4"));
	sequenceReset.getHeader().setField(PossDupFlag(true));
	sequenceReset.setField(newSeqNo);

	insertOrigSendingTime(sequenceReset.getHeader(),
		UtcTimeStamp()); // We keep it for safety reasons.
	sequenceReset.getHeader().setField(MsgSeqNum(beginSeqNo));
	sequenceReset.setField(GapFillFlag(true));
	m_state.last_outgoing_message_time(UtcTimeStamp {});
	sendRaw(sequenceReset, beginSeqNo);
	m_state.onEvent("Sent SequenceReset TO: " + newSeqNo.getString());
}

void Session::generateHeartbeat()
{
	Message heartbeat;
	heartbeat.getHeader().setField(MsgType("0"));
	m_state.last_outgoing_message_time(UtcTimeStamp {});
	on_outgoing_message(heartbeat);
}

void Session::generateHeartbeat(const Message& testRequest)
{
	Message heartbeat;
	heartbeat.getHeader().setField(MsgType("0"));
	try {
		TestReqID testReqID;
		testRequest.getField(testReqID);
		heartbeat.setField(testReqID);
	}
	catch (FieldNotFound&) {
	}

	m_state.last_outgoing_message_time(UtcTimeStamp {});
	on_outgoing_message(heartbeat);
}

void Session::generateTestRequest(const std::string& id)
{
	Message testRequest;
	testRequest.getHeader().setField(MsgType("1"));
	TestReqID testReqID(id);
	testRequest.setField(testReqID);

	m_state.last_outgoing_message_time(UtcTimeStamp {});
	on_outgoing_message(testRequest);
}

void Session::generateReject(const Message& message, int err)
{
	generateReject(message, err, 0, std::string());
}

void Session::generateReject(
	const Message& message, int err, int field, const std::string& text)
{
	std::string beginString = m_sessionID.getBeginString().getValue();

	Message reject;
	reject.getHeader().setField(MsgType("3"));
	reject.reverseRoute(message.getHeader());

	MsgSeqNum msgSeqNum;
	MsgType msgType;

	message.getHeader().getField(msgType);
	if (message.getHeader().isSetField(msgSeqNum)) {
		message.getHeader().getField(msgSeqNum);
		if (msgSeqNum.getString() != "")
			reject.setField(RefSeqNum(msgSeqNum.getValue()));
	}

	if (beginString >= FIX::BeginString_FIX42) {
		if (msgType.getString() != "")
			reject.setField(RefMsgType(msgType.getValue()));
		if ((beginString == FIX::BeginString_FIX42
				&& err <= SessionRejectReason_INVALID_MSGTYPE)
			|| beginString > FIX::BeginString_FIX42) {
			reject.setField(SessionRejectReason(err));
		}
	}
	if (msgType != MsgType_Logon && msgType != MsgType_SequenceReset
		&& msgSeqNum == getExpectedTargetNum()) {
		m_state.incrNextTargetMsgSeqNum();
		on_incoming_message();
	}

	const char* reason = 0;
	switch (err) {
	case SessionRejectReason_INVALID_TAG_NUMBER:
		reason = SessionRejectReason_INVALID_TAG_NUMBER_TEXT;
		break;
	case SessionRejectReason_REQUIRED_TAG_MISSING:
		reason = SessionRejectReason_REQUIRED_TAG_MISSING_TEXT;
		break;
	case SessionRejectReason_TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE:
		reason = SessionRejectReason_TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE_TEXT;
		break;
	case SessionRejectReason_TAG_SPECIFIED_WITHOUT_A_VALUE:
		reason = SessionRejectReason_TAG_SPECIFIED_WITHOUT_A_VALUE_TEXT;
		break;
	case SessionRejectReason_VALUE_IS_INCORRECT:
		reason = SessionRejectReason_VALUE_IS_INCORRECT_TEXT;
		break;
	case SessionRejectReason_INCORRECT_DATA_FORMAT_FOR_VALUE:
		reason = SessionRejectReason_INCORRECT_DATA_FORMAT_FOR_VALUE_TEXT;
		break;
	case SessionRejectReason_COMPID_PROBLEM:
		reason = SessionRejectReason_COMPID_PROBLEM_TEXT;
		break;
	case SessionRejectReason_SENDINGTIME_ACCURACY_PROBLEM:
		reason = SessionRejectReason_SENDINGTIME_ACCURACY_PROBLEM_TEXT;
		break;
	case SessionRejectReason_INVALID_MSGTYPE:
		reason = SessionRejectReason_INVALID_MSGTYPE_TEXT;
		break;
	case SessionRejectReason_TAG_APPEARS_MORE_THAN_ONCE:
		reason = SessionRejectReason_TAG_APPEARS_MORE_THAN_ONCE_TEXT;
		break;
	case SessionRejectReason_TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER:
		reason = SessionRejectReason_TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER_TEXT;
		break;
	case SessionRejectReason_INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP:
		reason
			= SessionRejectReason_INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP_TEXT;
	};
	if (!text.empty())
		reason = text.c_str();

	if (reason && (field || err == SessionRejectReason_INVALID_TAG_NUMBER)) {
		populateRejectReason(reject, field, reason);
		m_state.onEvent("Message " + msgSeqNum.getString()
				+ " Rejected: " + reason + ":" + IntConvertor::convert(field),
			misc::APP_LOG_ERR);
	}
	else if (reason) {
		populateRejectReason(reject, reason);
		m_state.onEvent(
			"Message " + msgSeqNum.getString() + " Rejected: " + reason,
			misc::APP_LOG_ERR);
	}
	else
		m_state.onEvent("Message " + msgSeqNum.getString() + " Rejected",
			misc::APP_LOG_ERR);

	if (!m_state.receivedLogon())
		throw std::runtime_error("Tried to send a reject while not logged on");

	m_state.last_outgoing_message_time(UtcTimeStamp {});
	on_outgoing_message(reject);
}

void Session::generateReject(const Message& message, const std::string& str)
{
	std::string beginString = m_sessionID.getBeginString().getValue();

	Message reject;
	reject.getHeader().setField(MsgType("3"));
	reject.reverseRoute(message.getHeader());

	MsgType msgType;
	MsgSeqNum msgSeqNum;

	message.getHeader().getField(msgType);
	message.getHeader().getField(msgSeqNum);
	if (beginString >= FIX::BeginString_FIX42)
		reject.setField(RefMsgType(msgType.getValue()));
	reject.setField(RefSeqNum(msgSeqNum.getValue()));

	if (msgType != MsgType_Logon && msgType != MsgType_SequenceReset) {
		m_state.incrNextTargetMsgSeqNum();
		on_incoming_message();
	}

	reject.setField(Text(str));
	m_state.last_outgoing_message_time(UtcTimeStamp {});
	on_outgoing_message(reject);
	m_state.onEvent("Message " + msgSeqNum.getString() + " Rejected: " + str,
		misc::APP_LOG_ERR);
}

void Session::generateBusinessReject(const Message& message, int err, int field)
{
	Message reject;
	reject.getHeader().setField(MsgType(MsgType_BusinessMessageReject));
	MsgType msgType;
	MsgSeqNum msgSeqNum;
	message.getHeader().getField(msgType);
	message.getHeader().getField(msgSeqNum);
	reject.setField(RefMsgType(msgType.getValue()));
	reject.setField(RefSeqNum(msgSeqNum.getValue()));
	reject.setField(BusinessRejectReason(err));
	m_state.incrNextTargetMsgSeqNum();
	on_incoming_message();

	const char* reason = 0;
	switch (err) {
	case BusinessRejectReason_OTHER:
		reason = BusinessRejectReason_OTHER_TEXT;
		break;
	case BusinessRejectReason_UNKOWN_ID:
		reason = BusinessRejectReason_UNKNOWN_ID_TEXT;
		break;
	case BusinessRejectReason_UNKNOWN_SECURITY:
		reason = BusinessRejectReason_UNKNOWN_SECURITY_TEXT;
		break;
	case BusinessRejectReason_UNSUPPORTED_MESSAGE_TYPE:
		reason = BusinessRejectReason_UNSUPPORTED_MESSAGE_TYPE_TEXT;
		break;
	case BusinessRejectReason_APPLICATION_NOT_AVAILABLE:
		reason = BusinessRejectReason_APPLICATION_NOT_AVAILABLE_TEXT;
		break;
	case BusinessRejectReason_CONDITIONALLY_REQUIRED_FIELD_MISSING:
		reason = BusinessRejectReason_CONDITIONALLY_REQUIRED_FIELD_MISSING_TEXT;
		break;
	case BusinessRejectReason_NOT_AUTHORIZED:
		reason = BusinessRejectReason_NOT_AUTHORIZED_TEXT;
		break;
	case BusinessRejectReason_DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME:
		reason
			= BusinessRejectReason_DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME_TEXT;
		break;
	};

	if (reason && field) {
		populateRejectReason(reject, field, reason);
		m_state.onEvent("Message " + msgSeqNum.getString()
				+ " Rejected: " + reason + ":" + IntConvertor::convert(field),
			misc::APP_LOG_ERR);
	}
	else if (reason) {
		populateRejectReason(reject, reason);
		m_state.onEvent(
			"Message " + msgSeqNum.getString() + " Rejected: " + reason,
			misc::APP_LOG_ERR);
	}
	else
		m_state.onEvent("Message " + msgSeqNum.getString() + " Rejected",
			misc::APP_LOG_ERR);

	m_state.last_outgoing_message_time(UtcTimeStamp {});
	on_outgoing_message(reject);
}

void Session::generateLogout(const std::string& text)
{
	Message logout;
	logout.getHeader().setField(MsgType(MsgType_Logout));
	if (text.length())
		logout.setField(Text(text));
	m_state.last_outgoing_message_time(UtcTimeStamp {});
	on_outgoing_message(logout);
	m_state.sentLogout(true);
}

void Session::populateRejectReason(
	Message& reject, int field, const std::string& text)
{
	MsgType msgType;
	reject.getHeader().getField(msgType);

	if (msgType == MsgType_REJECT
		&& m_sessionID.getBeginString() >= FIX::BeginString_FIX42) {
		reject.setField(RefTagID(field));
		reject.setField(Text(text));
	}
	else {
		std::stringstream stream;
		stream << text << " (" << field << ")";
		reject.setField(Text(stream.str()));
	}
}

void Session::populateRejectReason(Message& reject, const std::string& text)
{
	reject.setField(Text(text));
}

bool Session::verify(const Message& msg, const UtcTimeStamp& now,
	bool checkTooHigh, bool checkTooLow)
{
	const MsgType* pMsgType = 0;
	int msgSeqNum = 0;

	try {
		const Header& header = msg.getHeader();

		pMsgType = FIELD_GET_PTR(header, MsgType);
		const SenderCompID& senderCompID = FIELD_GET_REF(header, SenderCompID);
		const TargetCompID& targetCompID = FIELD_GET_REF(header, TargetCompID);
		const SendingTime& sendingTime = FIELD_GET_REF(header, SendingTime);

		if (checkTooHigh || checkTooLow)
			msgSeqNum = FIELD_GET_REF(header, MsgSeqNum).getValue();

		if (!validLogonState(*pMsgType))
			throw std::logic_error("Logon state is not valid for message");

		if (!isGoodTime(sendingTime, now)) {
			doBadTime(msg);
			return false;
		}
		if (!isCorrectCompID(senderCompID, targetCompID)) {
			doBadCompID(msg);
			return false;
		}

		if (checkTooHigh && isTargetTooHigh(msgSeqNum)) {
			doTargetTooHigh(msg);
			return false;
		}
		else if (checkTooLow && isTargetTooLow(msgSeqNum)) {
			doTargetTooLow(msg);
			return false;
		}

		if ((checkTooHigh || checkTooLow) && m_state.resendRequested()) {
			SessionState::ResendRange range = m_state.resendRange();

			if (msgSeqNum >= range.second) {
				m_state.onEvent("ResendRequest for messages FROM: "
					+ IntConvertor::convert(range.first)
					+ " TO: " + IntConvertor::convert(range.second)
					+ " has been satisfied.");
				m_state.resendRange(0, 0);
			}
		}
	}
	catch (std::exception& e) {
		m_state.onEvent(e.what(), misc::APP_LOG_ERR);
		disconnect();
		return false;
	}

	m_state.lastReceivedTime(now);
	m_state.testRequest(0);

	fromCallback(pMsgType ? *pMsgType : MsgType(), msg, m_sessionID);
	return true;
}

bool Session::validLogonState(const MsgType& msgType) const
{
	if ((msgType == MsgType_Logon && m_state.sentReset())
		|| m_state.receivedReset())
		return true;
	if ((msgType == MsgType_Logon && !m_state.receivedLogon())
		|| (msgType != MsgType_Logon && m_state.receivedLogon()))
		return true;
	if (msgType == MsgType_Logout && m_state.sentLogon())
		return true;
	if (msgType != MsgType_Logout && m_state.sentLogout())
		return true;
	if (msgType == MsgType_SequenceReset)
		return true;
	if (msgType == MsgType_Reject)
		return true;

	return false;
}

void Session::fromCallback(
	const MsgType& msgType, const Message& msg, const SessionID&) const
{
	if (Message::isAdminMsgType(msgType))
		handler_->fromAdmin(msg);
	else
		handler_->fromApp(msg);
}

void Session::doReset(bool locked)
{
	if (handler_->onReset(locked))
		m_state.reset();
}

void Session::doBadTime(const Message& msg)
{
	generateReject(msg, SessionRejectReason_SENDINGTIME_ACCURACY_PROBLEM);
	generateLogout();
}

void Session::doBadCompID(const Message& msg)
{
	generateReject(msg, SessionRejectReason_COMPID_PROBLEM);
	generateLogout();
}

bool Session::doPossDup(const Message& msg)
{
	const Header& header = msg.getHeader();
	OrigSendingTime origSendingTime;
	SendingTime sendingTime;
	MsgType msgType;

	header.getField(msgType);
	header.getField(sendingTime);

	if (msgType != MsgType_SequenceReset) {
		if (!header.isSetField(origSendingTime)) {
			generateReject(msg, SessionRejectReason_REQUIRED_TAG_MISSING,
				origSendingTime.getField(), std::string());
			return false;
		}
		header.getField(origSendingTime);

		if (origSendingTime > sendingTime) {
			generateReject(
				msg, SessionRejectReason_SENDINGTIME_ACCURACY_PROBLEM);
			generateLogout();
			return false;
		}
	}
	return true;
}

bool Session::doTargetTooLow(const Message& msg)
{
	const Header& header = msg.getHeader();
	PossDupFlag possDupFlag(false);
	MsgSeqNum msgSeqNum;
	if (header.isSetField(possDupFlag))
		header.getField(possDupFlag);
	header.getField(msgSeqNum);

	if (!possDupFlag) {
		std::stringstream stream;
		stream << "MsgSeqNum too low, expecting " << getExpectedTargetNum()
			   << " but received " << msgSeqNum.getString();
		generateLogout(stream.str());
		throw std::logic_error(stream.str());
	}

	return doPossDup(msg);
}

void Session::doTargetTooHigh(const Message& msg)
{
	const Header& header = msg.getHeader();
	BeginString beginString;
	MsgSeqNum msgSeqNum;
	header.getField(beginString);
	header.getField(msgSeqNum);

	m_state.onEvent("MsgSeqNum too high, expecting "
			+ IntConvertor::convert(getExpectedTargetNum()) + " but received "
			+ msgSeqNum.getString(),
		misc::APP_LOG_ERR);

	m_state.queue(msgSeqNum.getValue(), msg);

	if (m_state.resendRequested()) {
		SessionState::ResendRange range = m_state.resendRange();

		if (!m_sendRedundantResendRequests && msgSeqNum >= range.first) {
			m_state.onEvent("Already sent ResendRequest FROM: "
				+ IntConvertor::convert(range.first)
				+ " TO: " + IntConvertor::convert(range.second)
				+ ".  Not sending another.");
			return;
		}
	}

	generateResendRequest(beginString, msgSeqNum);
}

void Session::nextQueued()
{
	while (nextQueued(getExpectedTargetNum())) {
	}
}

bool Session::nextQueued(int num)
{
	Message msg;
	MsgType msgType;

	if (m_state.retreive(num, msg)) {
		m_state.onEvent(
			"Processing QUEUED message: " + IntConvertor::convert(num));
		msg.getHeader().getField(msgType);
		if (msgType == MsgType_Logon || msgType == MsgType_ResendRequest) {
			m_state.incrNextTargetMsgSeqNum();
			on_incoming_message();
		}
		else {
			std::string msgString;
			next(msg.toString(msgString), true);
		}
		return true;
	}
	return false;
}

void Session::next(const std::string& msg, bool queued)
{
	try {
		m_state.onIncoming(msg);
		handler_->onMessageReceived(msg, queued);
		next(Message(msg, m_dataDictionary), queued);
	}
	catch (InvalidMessage& e) {
		m_state.onEvent(e.what(), misc::APP_LOG_ERR);

		try {
			if (identifyType(msg) == MsgType_Logon) {
				m_state.onEvent(
					"Logon message is not valid", misc::APP_LOG_ERR);
				disconnect();
			}
		}
		catch (MessageParseError&) {
		}
		throw e;
	}
}

void Session::next(const Message& message, bool queued)
{
	UtcTimeStamp now;
	const Header& header = message.getHeader();

	try {
		const MsgType& msgType = FIELD_GET_REF(header, MsgType);
		const BeginString& beginString = FIELD_GET_REF(header, BeginString);
		(void)FIELD_GET_REF(header, SenderCompID);
		(void)FIELD_GET_REF(header, TargetCompID);

		if (beginString != m_sessionID.getBeginString())
			throw UnsupportedVersion();

		m_dataDictionary.validate(message);

		if (msgType == MsgType_Logon)
			nextLogon(message, now);
		else if (msgType == MsgType_Heartbeat)
			nextHeartbeat(message, now);
		else if (msgType == MsgType_TestRequest)
			nextTestRequest(message, now);
		else if (msgType == MsgType_SequenceReset)
			nextSequenceReset(message, now);
		else if (msgType == MsgType_Logout)
			nextLogout(message, now);
		else if (msgType == MsgType_ResendRequest)
			nextResendRequest(message, now);
		else if (msgType == MsgType_Reject)
			nextReject(message, now);
		else {
			if (!verify(message, now))
				return;
			m_state.incrNextTargetMsgSeqNum(); // increment active immediately.
											   // increment passive on direct
											   // reply.
		}
	}
	catch (MessageParseError& e) {
		m_state.onEvent(e.what(), misc::APP_LOG_ERR);
	}
	catch (RequiredTagMissing& e) {
		LOGEX(generateReject(message, SessionRejectReason_REQUIRED_TAG_MISSING,
			e.field, e.text));
	}
	catch (FieldNotFound& e) {
		if (header.getField(FIELD::BeginString) >= FIX::BeginString_FIX42
			&& message.isApp()) {
			LOGEX(generateBusinessReject(message,
				BusinessRejectReason_CONDITIONALLY_REQUIRED_FIELD_MISSING,
				e.field));
		}
		else {
			LOGEX(generateReject(message,
				SessionRejectReason_REQUIRED_TAG_MISSING, e.field,
				std::string()));
			if (header.getField(FIELD::MsgType) == MsgType_Logon) {
				m_state.onEvent(
					"Required field missing from logon", misc::APP_LOG_ERR);
				disconnect();
			}
		}
	}
	catch (InvalidTagNumber& e) {
		LOGEX(generateReject(
			message, SessionRejectReason_INVALID_TAG_NUMBER, e.field, e.text));
	}
	catch (NoTagValue& e) {
		LOGEX(generateReject(message,
			SessionRejectReason_TAG_SPECIFIED_WITHOUT_A_VALUE, e.field,
			e.text));
	}
	catch (TagNotDefinedForMessage& e) {
		LOGEX(generateReject(message,
			SessionRejectReason_TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE, e.field,
			e.text));
	}
	catch (InvalidMessageType& e) {
		LOGEX(generateReject(
			message, SessionRejectReason_INVALID_MSGTYPE, 0, e.text));
	}
	catch (UnsupportedMessageType&) {
		if (header.getField(FIELD::BeginString) >= FIX::BeginString_FIX42) {
			LOGEX(generateBusinessReject(
				message, BusinessRejectReason_UNSUPPORTED_MESSAGE_TYPE));
		}
		else {
			LOGEX(generateReject(message, "Unsupported message type"));
		}
	}
	catch (TagOutOfOrder& e) {
		LOGEX(generateReject(message,
			SessionRejectReason_TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER, e.field,
			e.text));
	}
	catch (IncorrectDataFormat& e) {
		LOGEX(generateReject(message,
			SessionRejectReason_INCORRECT_DATA_FORMAT_FOR_VALUE, e.field,
			e.text));
	}
	catch (IncorrectTagValue& e) {
		LOGEX(generateReject(
			message, SessionRejectReason_VALUE_IS_INCORRECT, e.field, e.text));
	}
	catch (RepeatedTag& e) {
		LOGEX(generateReject(message,
			SessionRejectReason_TAG_APPEARS_MORE_THAN_ONCE, e.field, e.text));
	}
	catch (RepeatingGroupCountMismatch& e) {
		LOGEX(generateReject(message,
			SessionRejectReason_INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP,
			e.field, e.text));
	}
	catch (InvalidMessage& e) {
		m_state.onEvent(e.what(), misc::APP_LOG_ERR);
	}
	catch (RejectLogon& e) {
		m_state.onEvent(e.what(), misc::APP_LOG_ERR);
		generateLogout(e.what());
		disconnect();
	}
	catch (Already_logged_on& e) {
		m_state.onEvent(
			"Already logged on. Ignoring logon request.", misc::APP_LOG_ERR);
		disconnect_on_already_logged_on();
	}
	catch (UnsupportedVersion&) {
		if (header.getField(FIELD::MsgType) == MsgType_Logout)
			nextLogout(message, now);
		else {
			generateLogout("Incorrect BeginString");
			m_state.incrNextTargetMsgSeqNum();
			on_incoming_message();
		}
	}
	catch (IOException& e) {
		m_state.onEvent(e.what(), misc::APP_LOG_ERR);
		disconnect();
	}
	catch (GatewayError&) {
		LOGEX(generateBusinessReject(message, BusinessRejectReason_OTHER));
	}

	if (!queued)
		nextQueued();

	if (isLoggedOn())
		next();
}

bool Session::sendToTarget(Message& message, const std::string& qualifier)
{
	try {
		SessionID sessionID = message.getSessionID(qualifier);
		return sendToTarget(message, sessionID);
	}
	catch (FieldNotFound&) {
		throw SessionNotFound();
	}
}

bool Session::sendToTarget(Message& message, const SessionID& sessionID)
{
	message.setSessionID(sessionID);
	Session* pSession = lookupSession(sessionID);
	if (!pSession)
		throw SessionNotFound();
	return pSession->send(message);
}

bool Session::sendToTarget(Message& message, const SenderCompID& senderCompID,
	const TargetCompID& targetCompID, const std::string& qualifier)
{
	message.getHeader().setField(senderCompID);
	message.getHeader().setField(targetCompID);
	return sendToTarget(message, qualifier);
}

bool Session::sendToTarget(Message& message, const std::string& sender,
	const std::string& target, const std::string& qualifier)
{
	return sendToTarget(
		message, SenderCompID(sender), TargetCompID(target), qualifier);
}

std::set<SessionID> Session::getSessions()
{
	std::lock_guard lock(s_mutex);
	return s_sessionIDs;
}

bool Session::doesSessionExist(const SessionID& sessionID)
{
	std::lock_guard lock(s_mutex);
	return s_sessions.end() != s_sessions.find(sessionID);
}

Session* Session::lookupSession(const SessionID& sessionID)
{
	std::lock_guard lock(s_mutex);
	Sessions::iterator find = s_sessions.find(sessionID);
	if (find != s_sessions.end())
		return find->second;
	else
		return 0;
}

Session* Session::lookupSession(const std::string& string, bool reverse)
{
	Message message;
	if (!message.setStringHeader(string))
		return 0;

	try {
		const Header& header = message.getHeader();
		const BeginString& beginString = FIELD_GET_REF(header, BeginString);
		const SenderCompID& senderCompID = FIELD_GET_REF(header, SenderCompID);
		const TargetCompID& targetCompID = FIELD_GET_REF(header, TargetCompID);

		if (reverse) {
			return lookupSession(
				SessionID(beginString, SenderCompID(targetCompID.getValue()),
					TargetCompID(senderCompID.getValue())));
		}

		return lookupSession(
			SessionID(beginString, senderCompID, targetCompID));
	}
	catch (FieldNotFound&) {
		return 0;
	}
}

bool Session::isSessionRegistered(const SessionID& sessionID)
{
	std::lock_guard lock(s_mutex);
	return s_registered.end() != s_registered.find(sessionID);
}

Session* Session::registerSession(const SessionID& sessionID)
{
	std::lock_guard lock(s_mutex);
	Sessions::iterator find = s_sessions.find(sessionID);
	if (find == s_sessions.end())
		return 0;
	Session* pSession = find->second;
	if (s_registered.end() != s_registered.find(sessionID))
		return 0;
	s_registered[sessionID] = pSession;
	return pSession;
}

void Session::unregisterSession(const SessionID& sessionID)
{
	std::lock_guard lock(s_mutex);
	s_registered.erase(sessionID);
}

int Session::numSessions()
{
	std::lock_guard lock(s_mutex);
	return s_sessions.size();
}

bool Session::addSession(Session& s)
{
	std::lock_guard lock(s_mutex);
	Sessions::iterator it = s_sessions.find(s.m_sessionID);
	if (it == s_sessions.end()) {
		s_sessions[s.m_sessionID] = &s;
		s_sessionIDs.insert(s.m_sessionID);
		return true;
	}
	else
		return false;
}

void Session::removeSession(Session& s)
{
	std::lock_guard lock(s_mutex);
	s_sessions.erase(s.m_sessionID);
	s_sessionIDs.erase(s.m_sessionID);
	s_registered.erase(s.m_sessionID);
}
} // namespace FIX
