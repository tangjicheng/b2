#include "qfix/Settings.h"

#include "qfix/Exceptions.h"

namespace FIX {

Settings::Settings(std::istream& is)
{
	while (is) {
		std::string line;
		getline(is, line);
		if (line.empty() || line.front() == '#')
			continue;
		if (line.front() == '[' && line.back() == ']') {
			const auto name = line.substr(1, line.size() - 2);
			if (name.empty())
				throw ConfigError {"Empty section name"};
			sections_.push_back(Dictionary {name});
			continue;
		}
		const auto pos = line.find('=');
		if (pos == std::string::npos)
			throw ConfigError {"No separator"};
		if (pos == 0)
			throw ConfigError {"Empty key"};
		if (pos == line.size() - 1)
			throw ConfigError {"Empty value"};
		const auto key = line.substr(0, pos);
		const auto value = line.substr(pos + 1);
		if (sections_.empty())
			throw ConfigError {"No section defined"};
		sections_.back().setString(key, value);
	}
}

Settings::Sections Settings::get(const std::string& name) const noexcept
{
	Sections sections;
	for (auto& section : sections_) {
		if (name == section.getName())
			sections.push_back(section);
	}
	return sections;
}

} // namespace FIX
