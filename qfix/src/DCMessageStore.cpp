#include "qfix/DCMessageStore.h"

#include <unistd.h>

#include <cstddef>
#include <cstring>
#include <utility>

#include "qfix/Exceptions.h"
#include "qfix/FieldConvertors.h"
#include "qfix/SessionID.h"

namespace FIX {

namespace {

constexpr char READ_ERROR[] = "Cannot read from file: ";
constexpr char WRITE_ERROR[] = "Cannot write to file: ";
constexpr char OPEN_ERROR[] = "Cannot open file: ";
constexpr char CLOSE_ERROR[] = "Cannot close file: ";
constexpr char FLUSH_ERROR[] = "Cannot flush file: ";
constexpr char UNLINK_ERROR[] = "Cannot unlink file: ";
constexpr char SEEK_ERROR[] = "Cannot seek into file: ";
constexpr char TELL_ERROR[] = "Cannot get file position: ";
constexpr char FORMAT_ERROR[] = "Incorrect file format: ";

void mkdirPath(const char* path)
{
	const char* end = path + strlen(path);
	std::string dir;
	for (const char* pos = path; pos != end; ++pos) {
		dir += *pos;
		if (*pos == '/' || pos + 1 == end)
			mkdir(dir.c_str(), 0777);
	}
}

std::string correctPath(const std::string& path)
{
	if (path.empty())
		return "./";
	else if (*path.rbegin() == '/')
		return path;
	else
		return path + '/';
}

} // namespace

DCMessageStoreFactory::DCMessageStoreFactory(const std::string& path)
	: m_path(path)
{
}

DCMessageStoreFactory::DCMessageStoreFactory(const SessionSettings& settings)
	: m_settings(settings)
{
}

MessageStore* DCMessageStoreFactory::create(const SessionID& s)
{
	if (!m_path.empty())
		return new DCMessageStore(m_path, s);
	else
		return new DCMessageStore(
			m_settings.get(s).getString(FILE_STORE_PATH), s);
}

void DCMessageStoreFactory::destroy(MessageStore* store)
{
	delete store;
}

DCMessageStore::DCMessageStore(const std::string& path, const SessionID& s)
	: m_messageFile(nullptr)
	, m_seqnumsFile(nullptr)
	, m_sessionFile(nullptr)
{
	mkdirPath(path.c_str());

	std::string basename = correctPath(path);
	basename += s.getBeginString().getString();
	basename += '-';
	basename += s.getSenderCompID().getString();
	basename += '-';
	basename += s.getTargetCompID().getString();
	if (!s.getSessionQualifier().empty()) {
		basename += '-';
		basename += s.getSessionQualifier();
	}

	m_messageFilename = basename + ".message";
	m_seqnumsFilename = basename + ".seqnums";
	m_sessionFilename = basename + ".session";

	try {
		open(false);
	}
	catch (IOException& e) {
		throw ConfigError(e.what());
	}
}

DCMessageStore::~DCMessageStore()
{
	fclose(m_messageFile);
	fclose(m_seqnumsFile);
	fclose(m_sessionFile);
}

void DCMessageStore::open(bool deleteFile)
{
	if (m_messageFile) {
		if (fclose(m_messageFile))
			throw IOException(CLOSE_ERROR + m_messageFilename);
	}
	if (m_seqnumsFile) {
		if (fclose(m_seqnumsFile))
			throw IOException(CLOSE_ERROR + m_seqnumsFilename);
	}
	if (m_sessionFile) {
		if (fclose(m_sessionFile))
			throw IOException(CLOSE_ERROR + m_sessionFilename);
	}

	if (deleteFile) {
		if (unlink(m_messageFilename.c_str()))
			throw IOException(UNLINK_ERROR + m_messageFilename);
		if (unlink(m_seqnumsFilename.c_str()))
			throw IOException(UNLINK_ERROR + m_seqnumsFilename);
		if (unlink(m_sessionFilename.c_str()))
			throw IOException(UNLINK_ERROR + m_sessionFilename);
	}

	if ((m_messageFile = fopen(m_messageFilename.c_str(), "r+")) == nullptr) {
		if ((m_messageFile = fopen(m_messageFilename.c_str(), "w+")) == nullptr)
			throw ConfigError(OPEN_ERROR + m_messageFilename);
		setNullMsg();
	}

	if ((m_seqnumsFile = fopen(m_seqnumsFilename.c_str(), "r+")) == nullptr) {
		if ((m_seqnumsFile = fopen(m_seqnumsFilename.c_str(), "w+")) == nullptr)
			throw ConfigError(OPEN_ERROR + m_seqnumsFilename);
		setSeqNums();
	}
	else {
		getSeqNums();
	}

	if ((m_sessionFile = fopen(m_sessionFilename.c_str(), "r+")) == nullptr) {
		if ((m_sessionFile = fopen(m_sessionFilename.c_str(), "w+")) == nullptr)
			throw ConfigError(OPEN_ERROR + m_sessionFilename);
		setSession();
	}
	else {
		getSession();
	}
}

bool DCMessageStore::set(int msgSeqNum, const std::string& msg)
{
	if (fseek(m_messageFile, 0, SEEK_END))
		throw IOException(SEEK_ERROR + m_messageFilename);
	int size = msg.size();
	fprintf(m_messageFile, "%010d%04d", msgSeqNum, size);
	if (ferror(m_messageFile))
		throw IOException(WRITE_ERROR + m_messageFilename);
	fwrite(msg.c_str(), sizeof(char), size, m_messageFile);
	if (ferror(m_messageFile))
		throw IOException(WRITE_ERROR + m_messageFilename);
	fprintf(m_messageFile, "%04d", 10 + 4 + size + 4);
	if (ferror(m_messageFile))
		throw IOException(WRITE_ERROR + m_messageFilename);
	if (fflush(m_messageFile))
		throw IOException(FLUSH_ERROR + m_messageFilename);
	return true;
}

void DCMessageStore::get(int beg, int end, std::vector<std::string>& res) const
{
	res.clear();
	if (beg < 1 || end < beg)
		return;
	long start = find(beg);

	if (fseek(m_messageFile, start, SEEK_SET))
		throw IOException(SEEK_ERROR + m_messageFilename);
	while (true) {
		int msgSeqNum = 0;
		int size = 0;
		int num = fscanf(m_messageFile, "%10d%4d", &msgSeqNum, &size);
		if (ferror(m_messageFile))
			throw IOException(READ_ERROR + m_messageFilename);
		if (num == EOF)
			return;
		if (num != 2)
			throw IOException(FORMAT_ERROR + m_messageFilename);
		if (msgSeqNum > end)
			return;
		std::string msg(size, '\0');
		size_t items = fread(&msg[0], sizeof(char), size, m_messageFile);
		if (ferror(m_messageFile))
			throw IOException(READ_ERROR + m_messageFilename);
		if (static_cast<int>(items) != size)
			throw IOException(FORMAT_ERROR + m_messageFilename);
		res.push_back(std::move(msg));
		if (fseek(m_messageFile, 4, SEEK_CUR))
			throw IOException(SEEK_ERROR + m_messageFilename);
	}
}

// Pre: beg > 0
long DCMessageStore::find(int beg) const
{
	if (fseek(m_messageFile, -4, SEEK_END))
		throw IOException(SEEK_ERROR + m_messageFilename);
	while (true) {
		int offset = 0;
		int numOff = fscanf(m_messageFile, "%4d", &offset);
		if (ferror(m_messageFile))
			throw IOException(READ_ERROR + m_messageFilename);
		if (numOff != 1)
			throw IOException(FORMAT_ERROR + m_messageFilename);
		if (fseek(m_messageFile, -offset, SEEK_CUR))
			throw IOException(SEEK_ERROR + m_messageFilename);
		int msgSeqNum = 0;
		int numSeq = fscanf(m_messageFile, "%10d", &msgSeqNum);
		if (ferror(m_messageFile))
			throw IOException(READ_ERROR + m_messageFilename);
		if (numSeq != 1)
			throw IOException(FORMAT_ERROR + m_messageFilename);
		if (msgSeqNum < beg) {
			long pos = ftell(m_messageFile);
			if (pos == -1L)
				throw IOException(TELL_ERROR + m_messageFilename);
			return pos - 10 + offset;
		}
		if (fseek(m_messageFile, -(10 + 4), SEEK_CUR))
			throw IOException(SEEK_ERROR + m_messageFilename);
	}
}

int DCMessageStore::getNextSenderMsgSeqNum() const
{
	return m_cache.getNextSenderMsgSeqNum();
}

int DCMessageStore::getNextTargetMsgSeqNum() const
{
	return m_cache.getNextTargetMsgSeqNum();
}

void DCMessageStore::setNextSenderMsgSeqNum(int value)
{
	m_cache.setNextSenderMsgSeqNum(value);
	setSeqNums();
}

void DCMessageStore::setNextTargetMsgSeqNum(int value)
{
	m_cache.setNextTargetMsgSeqNum(value);
	setSeqNums();
}

void DCMessageStore::incrNextSenderMsgSeqNum()
{
	m_cache.incrNextSenderMsgSeqNum();
	setSeqNums();
}

void DCMessageStore::incrNextTargetMsgSeqNum()
{
	m_cache.incrNextTargetMsgSeqNum();
	setSeqNums();
}

UtcTimeStamp DCMessageStore::getCreationTime() const
{
	return m_cache.getCreationTime();
}

void DCMessageStore::reset()
{
	m_cache.reset();
	open(true);
}

void DCMessageStore::refresh()
{
	m_cache.reset();
	open(false);
}

void DCMessageStore::getSeqNums()
{
	rewind(m_seqnumsFile);
	int sender;
	int target;
	int num = fscanf(m_seqnumsFile, "%10d : %10d", &sender, &target);
	if (ferror(m_seqnumsFile))
		throw IOException(READ_ERROR + m_seqnumsFilename);
	if (num != 2)
		throw IOException(FORMAT_ERROR + m_seqnumsFilename);
	m_cache.setNextSenderMsgSeqNum(sender);
	m_cache.setNextTargetMsgSeqNum(target);
}

void DCMessageStore::getSession()
{
	rewind(m_sessionFile);
	char time[22];
	int num = fscanf(m_sessionFile, "%s", time);
	if (ferror(m_sessionFile))
		throw IOException(READ_ERROR + m_sessionFilename);
	if (num != 1)
		throw IOException(FORMAT_ERROR + m_sessionFilename);
	m_cache.setCreationTime(UtcTimeStampConvertor::convert(time, true));
}

void DCMessageStore::setSeqNums()
{
	rewind(m_seqnumsFile);
	fprintf(m_seqnumsFile, "%010d : %010d", m_cache.getNextSenderMsgSeqNum(),
		m_cache.getNextTargetMsgSeqNum());
	if (ferror(m_seqnumsFile))
		throw IOException(WRITE_ERROR + m_seqnumsFilename);
	if (fflush(m_seqnumsFile))
		throw IOException(FLUSH_ERROR + m_seqnumsFilename);
}

void DCMessageStore::setSession()
{
	rewind(m_sessionFile);
	fprintf(m_sessionFile, "%s",
		UtcTimeStampConvertor::convert(m_cache.getCreationTime()).c_str());
	if (ferror(m_sessionFile))
		throw IOException(WRITE_ERROR + m_sessionFilename);
	if (fflush(m_sessionFile))
		throw IOException(FLUSH_ERROR + m_sessionFilename);
}

void DCMessageStore::setNullMsg()
{
	rewind(m_messageFile);
	fprintf(m_messageFile, "%018d", 10 + 4 + 4);
	if (ferror(m_messageFile))
		throw IOException(WRITE_ERROR + m_messageFilename);
	if (fflush(m_messageFile))
		throw IOException(FLUSH_ERROR + m_messageFilename);
}

} // namespace FIX
