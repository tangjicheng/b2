/****************************************************************************
** Copyright (c) 2001-2014
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/ThreadedSocketConnection.h"

#include "qfix/Session.h"
#include "qfix/ThreadedSocketAcceptor.h"
#include "qfix/ThreadedSocketInitiator.h"
#include "qfix/Utility.h"

namespace FIX {
ThreadedSocketConnection::ThreadedSocketConnection(
	int s, Sessions sessions, Log* pLog)
	: m_socket(s)
	, m_pLog(pLog)
	, m_sessions(sessions)
	, m_pSession(0)
	, m_disconnect(false)
{
	initializeEpoll();
	const char* address = socket_peername(m_socket, &m_sourcePort);
	m_sourceAddress.assign(address);
}

ThreadedSocketConnection::ThreadedSocketConnection(const SessionID& sessionID,
	int s, const std::string& address, short port, Log* pLog,
	const std::string& sourceAddress, short sourcePort)
	: m_socket(s)
	, m_address(address)
	, m_port(port)
	, m_sourceAddress(sourceAddress)
	, m_sourcePort(sourcePort)
	, m_pLog(pLog)
	, m_pSession(Session::lookupSession(sessionID))
	, m_disconnect(false)
{
	initializeEpoll();
	if (m_pSession)
		m_pSession->setResponder(this);
}

ThreadedSocketConnection::~ThreadedSocketConnection()
{
	if (m_pSession) {
		m_pSession->setResponder(0);
		Session::unregisterSession(m_pSession->getSessionID());
	}
}

std::string ThreadedSocketConnection::getRemoteAddress() const
{
	std::stringstream stream;
	stream << m_sourceAddress << ':' << m_sourcePort;
	return stream.str();
}

bool ThreadedSocketConnection::send(const std::string& msg)
{
	int totalSent = 0;
	while (totalSent < (int)msg.length()) {
		ssize_t sent = socket_send(
			m_socket, msg.c_str() + totalSent, msg.length() - totalSent);
		if (sent < 0)
			return false;
		totalSent += sent;
	}

	return true;
}

void ThreadedSocketConnection::schedule_disconnect()
{
	if (m_pSession)
		m_pSession->disconnect();
	else
		disconnect();
}

bool ThreadedSocketConnection::connect()
{
	// do the bind in the thread as name resolution may block
	if (!m_sourceAddress.empty() || m_sourcePort)
		socket_bind(m_socket, m_sourceAddress.c_str(), m_sourcePort);

	return socket_connect(getSocket(), m_address.c_str(), m_port) >= 0;
}

void ThreadedSocketConnection::disconnect()
{
	m_disconnect = true;
	socket_close(m_epfd);
	socket_close(m_socket);
}

bool ThreadedSocketConnection::read()
{
	const auto timeout = 1000; // 1sec
	try {
		const auto maxSize = 1;
		epoll_event events[maxSize];
		auto result = 0;
		do {
			result = epoll_wait(m_epfd, events, maxSize, timeout);
		} while (result < 0 && errno == EINTR);
		if (result > 0) // Something to read
		{
			if ((events[0].events & EPOLLIN) != EPOLLIN)
				throw SocketRecvFailed {-1};

			const auto result = m_parser.read(m_socket);
			if (result != Read_result::success) {
				const auto error_code
					= (result == Read_result::closed) ? 0 : -1;
				throw SocketRecvFailed {error_code};
			}
		}
		else if (result == 0 && m_pSession) // Timeout
		{
			m_pSession->next();
		}
		else if (result < 0) // Error
		{
			throw SocketRecvFailed(result);
		}

		processStream();
		return true;
	}
	catch (SocketRecvFailed& e) {
		if (m_disconnect)
			return false;

		if (m_pSession) {
			m_pSession->getLog()->onEvent(e.what(), misc::APP_LOG_ERR);
			m_pSession->disconnect();
		}
		else {
			disconnect();
		}

		return false;
	}
}

bool ThreadedSocketConnection::readMessage(std::string& msg)
{
	try {
		return m_parser.extract_fix_message(msg);
	}
	catch (MessageParseError&) {
	}
	return true;
}

void ThreadedSocketConnection::processStream()
{
	std::string msg;
	while (readMessage(msg)) {
		if (!m_pSession) {
			if (!setSession(msg)) {
				disconnect();
				continue;
			}
		}
		try {
			m_pSession->next(msg);
		}
		catch (InvalidMessage&) {
			if (!m_pSession->isLoggedOn()) {
				disconnect();
				return;
			}
		}
	}
}

bool ThreadedSocketConnection::setSession(const std::string& msg)
{
	m_pSession = Session::lookupSession(msg, true);
	if (!m_pSession) {
		if (m_pLog) {
			m_pLog->onEvent("Session not found for incoming message: " + msg,
				misc::APP_LOG_ERR);
			m_pLog->onIncoming(msg);
		}
		return false;
	}

	SessionID sessionID = m_pSession->getSessionID();
	m_pSession = 0;

	// see if the session frees up within 5 seconds
	for (int i = 1; i <= 5; i++) {
		if (!Session::isSessionRegistered(sessionID))
			m_pSession = Session::registerSession(sessionID);
		if (m_pSession)
			break;
		process_sleep(1);
	}

	if (!m_pSession)
		return false;
	if (m_sessions.find(m_pSession->getSessionID()) == m_sessions.end())
		return false;

	m_pSession->setResponder(this);
	return true;
}

void ThreadedSocketConnection::initializeEpoll()
{
	m_epfd = epoll_create1(0);
	if (m_epfd == -1) {
		throw SocketException {};
	}
	epoll_event options;
	options.events = EPOLLIN;
	options.data.fd = m_socket;
	if (epoll_ctl(m_epfd, EPOLL_CTL_ADD, m_socket, &options) < 0) {
		close(m_epfd);
		throw SocketException {};
	}
}

} // namespace FIX
