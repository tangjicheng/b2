/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "qfix/SocketMonitor.h"

#include <algorithm>
#include <exception>
#include <iostream>
#include <set>

#include <sys/epoll.h>

#include "qfix/Exceptions.h"
#include "qfix/Utility.h"

namespace FIX {

SocketMonitor::SocketMonitor(int timeout)
	: m_timeout(timeout)
{
	socket_init();

	std::pair<int, int> sockets = socket_createpair();
	m_signal = sockets.first;
	m_interrupt = sockets.second;
	m_readSockets.insert(m_interrupt);
	m_epfd = epoll_create1(0);
	if (m_epfd == -1)
		throw SocketException {};
	subscribeSocket(m_interrupt, EPOLLIN);

	m_timeval = 0;
#ifndef SELECT_DECREMENTS_TIME
	m_ticks = clock();
#endif
}

SocketMonitor::~SocketMonitor()
{
	socket_close(m_epfd);
	Sockets::iterator i;
	for (i = m_readSockets.begin(); i != m_readSockets.end(); ++i)
		socket_close(*i);
	socket_close(m_signal);
	socket_close(m_interrupt);
	socket_term();
}

bool SocketMonitor::addConnect(int s)
{
	socket_setnonblock(s);
	Sockets::iterator i = m_connectSockets.find(s);
	if (i != m_connectSockets.end())
		return false;

	try {
		m_connectSockets.insert(s);
		subscribeSocket(s, EPOLLOUT | EPOLLPRI);
		return true;
	}
	catch (const SocketException&) {
		return false;
	}
}

bool SocketMonitor::addRead(int s)
{
	socket_setnonblock(s);
	Sockets::iterator i = m_readSockets.find(s);
	if (i != m_readSockets.end())
		return false;

	try {
		m_readSockets.insert(s);
		subscribeSocket(s, EPOLLIN);
		return true;
	}
	catch (const SocketException&) {
		return false;
	}
}

bool SocketMonitor::addWrite(int s)
{
	if (m_readSockets.find(s) == m_readSockets.end())
		return false;

	socket_setnonblock(s);
	Sockets::iterator i = m_writeSockets.find(s);
	if (i != m_writeSockets.end())
		return false;

	try {
		m_writeSockets.insert(s);
		modifySocket(s, EPOLLIN | EPOLLOUT);
		return true;
	}
	catch (const SocketException&) {
		return false;
	}
}

bool SocketMonitor::drop(int s)
{
	Sockets::iterator i = m_readSockets.find(s);
	Sockets::iterator j = m_writeSockets.find(s);
	Sockets::iterator k = m_connectSockets.find(s);

	if (i != m_readSockets.end() || j != m_writeSockets.end()
		|| k != m_connectSockets.end()) {
		try {
			unsubscribeSocket(s);
		}
		catch (const SocketException&) {
			return false;
		}
		socket_close(s);
		m_readSockets.erase(s);
		m_writeSockets.erase(s);
		m_connectSockets.erase(s);
		m_dropped.push(s);
		return true;
	}
	return false;
}

inline int SocketMonitor::getTimeval(bool poll)
{
	if (poll)
		return 0;

	if (!m_timeout)
		return 0;

#ifdef SELECT_MODIFIES_TIMEVAL
	if (!m_timeval && m_timeout)
		m_timeval = m_timeout;
	return m_timeval;
#else
	double elapsed = (double)(clock() - m_ticks) / (double)CLOCKS_PER_SEC;
	if (elapsed >= m_timeout || elapsed == 0.0) {
		m_ticks = clock();
		m_timeval = 1000;
	}
	else {
		m_timeval = (int)((m_timeout - elapsed) * 1000);
	}
	return m_timeval;
#endif
}

bool SocketMonitor::sleepIfEmpty(bool poll)
{
	if (poll)
		return false;

	if (m_readSockets.empty() && m_writeSockets.empty()
		&& m_connectSockets.empty()) {
		process_sleep(m_timeout);
		return true;
	}
	else
		return false;
}

void SocketMonitor::signal(int socket)
{
	socket_send(m_signal, (char*)&socket, sizeof(socket));
}

void SocketMonitor::unsignal(int s)
{
	Sockets::iterator i = m_writeSockets.find(s);
	if (i == m_writeSockets.end())
		return;

	try {
		m_writeSockets.erase(s);
		unsubscribeSocket(s);
	}
	catch (const SocketException&) {
		return;
	}
}

void SocketMonitor::block(Strategy& strategy, bool poll)
{
	while (m_dropped.size()) {
		strategy.onError(*this, m_dropped.front());
		m_dropped.pop();
		if (m_dropped.size() == 0)
			return;
	}

	if (sleepIfEmpty(poll)) {
		strategy.onTimeout(*this);
		return;
	}

	const auto maxSize = 10;
	epoll_event events[maxSize];
	auto eventCount = 0;
	do {
		eventCount = epoll_wait(m_epfd, events, maxSize, getTimeval(poll));
	} while (eventCount < 0 && errno == EINTR);

	if (eventCount == 0) {
		strategy.onTimeout(*this);
		return;
	}
	if (eventCount < 0) {
		strategy.onError(*this);
		return;
	}

	try {
		for (auto i = 0; i < eventCount; ++i)
			processEvent(strategy, events[i].data.fd, events[i].events);
	}
	catch (const SocketException& e) {
		strategy.onError(*this);
	}
}

void SocketMonitor::processEvent(
	Strategy& strategy, int socket, unsigned int flag)
{
	auto processed = false;

	if ((flag & EPOLLPRI) == EPOLLPRI || (flag & EPOLLERR) == EPOLLERR) {
		if (const auto it = m_connectSockets.find(socket);
			it != m_connectSockets.end()) {
			processExceptEvent(strategy, *it);
			processed = true;
		}
	}

	if ((flag & EPOLLOUT) == EPOLLOUT) {
		if (const auto it = m_connectSockets.find(socket);
			it != m_connectSockets.end()) {
			processConnectEvent(strategy, *it);
			processed = true;
		}

		if (const auto it = m_writeSockets.find(socket);
			it != m_writeSockets.end()) {
			processWriteEvent(strategy, *it);
			processed = true;
		}
	}

	if ((flag & EPOLLIN) == EPOLLIN) {
		if (const auto it = m_readSockets.find(socket);
			it != m_readSockets.end()) {
			processReadEvent(strategy, *it);
			processed = true;
		}
	}

	if (!processed)
		processExceptEvent(strategy, socket);
}

void SocketMonitor::processReadEvent(Strategy& strategy, int socket)
{
	if (socket == m_interrupt) {
		int socketToWrite = 0;
		recv(socket, (char*)&socketToWrite, sizeof(socketToWrite), 0);
		addWrite(socketToWrite);
	}
	else {
		strategy.onEvent(*this, socket);
	}
}

void SocketMonitor::processConnectEvent(Strategy& strategy, int socket)
{
	m_connectSockets.erase(socket);
	m_readSockets.insert(socket);
	modifySocket(socket, EPOLLIN);
	strategy.onConnect(*this, socket);
}

void SocketMonitor::processWriteEvent(Strategy& strategy, int socket)
{
	strategy.onWrite(*this, socket);
}

void SocketMonitor::processExceptEvent(Strategy& strategy, int socket)
{
	strategy.onError(*this, socket);
}

void SocketMonitor::subscribeSocket(int s, unsigned int flag)
{
	controlSocket(s, EPOLL_CTL_ADD, flag);
}

void SocketMonitor::unsubscribeSocket(int s)
{
	controlSocket(s, EPOLL_CTL_DEL, 0);
}

void SocketMonitor::modifySocket(int s, unsigned int flag)
{
	controlSocket(s, EPOLL_CTL_MOD, flag);
}

void SocketMonitor::controlSocket(int s, int op, unsigned int flag)
{
	epoll_event options;
	options.events = flag;
	options.data.fd = s;
	const auto err = epoll_ctl(m_epfd, op, s, &options);
	if (err < 0)
		throw SocketException {};
}

} // namespace FIX
