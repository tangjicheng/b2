PROJECT_NAME = qfix

AR = ar
ARFLAGS = crv

CC = gcc
CXX = g++
LANGSTD = -std=c++20
OPTFLAGS = -O3 -flto
DEBUG = -gdwarf-4
WARNINGS = -Wall -Wextra -Werror -pedantic-errors

GIT = git
SED = sed

TOPDIR := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

EXTLIBDIR = $(realpath $(TOPDIR)/..)
LIBMISCDIR = $(EXTLIBDIR)/libmisc
QFIXDIR = $(TOPDIR)
GOOGLETESTDIR = $(EXTLIBDIR)/googletest/build

LIBMISC_CPPFLAGS = -I$(LIBMISCDIR)/include
LIBMISC_LDLIBS = -L$(LIBMISCDIR)/lib -lmisc -lexpat

QFIX_CPPFLAGS = -I$(QFIXDIR)/include -I/usr/include/libxml2
QFIX_LDLIBS = -L$(QFIXDIR)/lib -lqfix -lxml2

GOOGLETEST_CPPFLAGS = -I$(GOOGLETESTDIR)/include
GOOGLETEST_LDLIBS = -L$(GOOGLETESTDIR)/lib64 -lgtest_main -lgtest
