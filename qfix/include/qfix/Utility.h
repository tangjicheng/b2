/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_UTILITY_H
#define FIX_UTILITY_H

#include <cstdint>

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <functional>
#include <string>
#include <tuple>

#include <sys/ioctl.h>

namespace FIX {
enum class Error_code {
	no_error,
	invalid_address_string,
	invalid_address_family,
	socket_create_fail,
	socket_bind_fail,
	socket_listen_fail
};

const char* to_string(Error_code);

void string_replace(const std::string& oldValue, const std::string& newValue,
	std::string& value);
std::string string_toLower(const std::string& value);
std::string string_toUpper(const std::string& value);

void socket_init();
void socket_term();
int socket_bind(int socket, const char* hostname, int port);
std::tuple<int, Error_code> socket_createAcceptor(
	int port, bool reuse = false, const char* host_address = nullptr);
int socket_createConnector();
int socket_connect(int s, const char* address, int port);
int socket_accept(int s);
ssize_t socket_recv(int s, char* buf, size_t length);
int socket_send(int s, const char* msg, int length);
void socket_close(int s);
bool socket_fionread(int s, int& bytes);
bool socket_disconnected(int s);
int socket_setsockopt(int s, int opt);
int socket_getsockopt(int s, int opt, int& optval);
int socket_setsockopt(int s, int opt, int optval);
int socket_fcntl(int s, int opt, int arg);
int socket_getfcntlflag(int s, int arg);
int socket_setfcntlflag(int s, int arg);
int socket_setnonblock(int s);
bool socket_isValid(int socket);
bool socket_isBad(int s);
void socket_invalidate(int& socket);
short socket_hostport(int socket);
const char* socket_hostname(int socket);
const char* socket_hostname(const char* name);
const char* socket_peername(int socket, int* = 0);
std::tuple<const char*, std::uint16_t> socket_peer_name(std::int32_t socket);
std::pair<int, int> socket_createpair();

tm time_gmtime(const time_t* t);
tm time_localtime(const time_t* t);

extern "C" {
typedef void*(THREAD_START_ROUTINE)(void*);
}
#define THREAD_PROC void*

typedef pthread_t thread_id;

bool thread_spawn(THREAD_START_ROUTINE func, void* var, thread_id& thread);
bool thread_spawn(THREAD_START_ROUTINE func, void* var);
void thread_join(thread_id thread);
void thread_detach(thread_id thread);
thread_id thread_self();

void process_sleep(double s);

std::string file_separator();
void file_mkdir(const char* path);
int recursive_mkdir(const char* path);
FILE* file_fopen(const char* path, const char* mode);
void file_unlink(const char* path);
std::string file_appendpath(const std::string& path, const std::string& file);
bool file_atomic_write(
	const std::string& path, std::function<int(FILE*)>&& write_fn);
} // namespace FIX

#define FILE_FSCANF fscanf

#define STRING_SPRINTF sprintf

#endif
