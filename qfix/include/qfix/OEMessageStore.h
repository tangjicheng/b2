#ifndef FIX_OE_MESSAGE_STORE_H
#define FIX_OE_MESSAGE_STORE_H

#include <cstdio>
#include <string>
#include <vector>

#include <misc/aio_writer.h>

#include "Exceptions.h"
#include "FieldTypes.h"
#include "MessageStore.h"
#include "SessionSettings.h"

namespace FIX {

class SessionID;

class OEMessageStoreFactory : public MessageStoreFactory {
public:
	explicit OEMessageStoreFactory(const std::string&);
	explicit OEMessageStoreFactory(const SessionSettings&);

	MessageStore* create(const SessionID&) override;
	void destroy(MessageStore*) override;

private:
	std::string m_path;
	SessionSettings m_settings;
};

class OEMessageStore : public MessageStore {
public:
	OEMessageStore(const std::string&, const SessionID&);
	~OEMessageStore() override;

	bool set(int, const std::string&) override;
	void get(int, int, std::vector<std::string>&) const override;

	int getNextSenderMsgSeqNum() const override
	{
		return m_nextSenderMsgSeqNum;
	}
	int getNextTargetMsgSeqNum() const override
	{
		return m_nextTargetMsgSeqNum;
	}
	void setNextSenderMsgSeqNum(int seqNum) override
	{
		m_nextSenderMsgSeqNum = seqNum;
	}
	void setNextTargetMsgSeqNum(int seqNum) override
	{
		m_nextTargetMsgSeqNum = seqNum;
	}
	void incrNextSenderMsgSeqNum() override { ++m_nextSenderMsgSeqNum; }
	void incrNextTargetMsgSeqNum() override { ++m_nextTargetMsgSeqNum; }

	UtcTimeStamp getCreationTime() const override { return m_creationTime; }

	void reset() override;
	void refresh() override;

private:
	std::string m_filename;
	int m_fd;
	mutable misc::Aio_writer m_aio;

	int m_nextSenderMsgSeqNum;
	int m_nextTargetMsgSeqNum;
	UtcTimeStamp m_creationTime;

	void open(bool);
	void close();
	void writeCreationTime();
	void writeSeqNums();
	void writeNullMsg();
	void readCreationTime();
	void readSeqNums();
	long find(int, FILE*) const;
};

} // namespace FIX

#endif // FIX_OE_MESSAGE_STORE_H
