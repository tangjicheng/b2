#ifndef FIX_EXTENDED_VALUES_H
#define FIX_EXTENDED_VALUES_H

namespace FIX {
constexpr char ExecInst_IGNORE_NOTIONAL_VALUE_CHECKS = 'x';
constexpr int ExecRestatementReason_TRADE_PREVENTION = 100;
constexpr int ExecRestatementReason_TRADE_CANCELED = 101;
constexpr int ExecRestatementReason_MARGIN_RESTRICTION = 102;
constexpr int ExecRestatementReason_GOOD_FOR_TIME_ORDER_EXPIRED = 103;
constexpr int ExposureDurationUnit_SECONDS = 0;
constexpr int ExposureDurationUnit_TENTHS_OF_A_SECOND = 1;
constexpr int ExposureDurationUnit_HUNDREDTHS_OF_A_SECOND = 2;
constexpr int ExposureDurationUnit_MILLISECONDS = 3;
constexpr int ExposureDurationUnit_MICROSECONDS = 4;
constexpr int ExposureDurationUnit_NANOSECONDS = 5;
constexpr int ExposureDurationUnit_MINUTES = 10;
constexpr int ExposureDurationUnit_HOURS = 11;
constexpr int ExposureDurationUnit_DAYS = 12;
constexpr int ExposureDurationUnit_WEEKS = 13;
constexpr int ExposureDurationUnit_MONTHS = 14;
constexpr int ExposureDurationUnit_YEARS = 15;
constexpr char OrderClassification_NON_HFT = '1';
constexpr char OrderClassification_HFT_MARKET_MAKING_STRATEGY = '3';
constexpr char OrderClassification_HFT_ARBITRAGE_STRATEGY = '4';
constexpr char OrderClassification_HFT_DIRECTIONAL_STRATEGY = '5';
constexpr char OrderClassification_HFT_OTHER_STRATEGY = '6';
constexpr char MarginTransactionType_NEGOTIABLE = '1';
constexpr char MarginTransactionType_STANDARDIZED = '2';
} // namespace FIX

#endif // FIX_EXTENDED_VALUES_H
