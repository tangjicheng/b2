/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_SESSIONSETTINGS_H
#define FIX_SESSIONSETTINGS_H

#include <map>
#include <set>

#include "Dictionary.h"
#include "Exceptions.h"
#include "SessionID.h"

namespace FIX {
const char BEGINSTRING[] = "BeginString";
const char SENDERCOMPID[] = "SenderCompID";
const char TARGETCOMPID[] = "TargetCompID";
const char SESSION_QUALIFIER[] = "SessionQualifier";
const char CONNECTION_TYPE[] = "ConnectionType";
const char USE_DATA_DICTIONARY[] = "UseDataDictionary";
const char SEND_RESETSEQNUMFLAG[] = "SendResetSeqNumFlag";
const char SEND_REDUNDANT_RESENDREQUESTS[] = "SendRedundantResendRequests";
const char DATA_DICTIONARY[] = "DataDictionary";
const char CHECK_COMPID[] = "CheckCompID";
const char CHECK_LATENCY[] = "CheckLatency";
const char MAX_LATENCY[] = "MaxLatency";
const char HEARTBTINT[] = "HeartBtInt";
const char SOCKET_ACCEPT_HOST[] = "SocketAcceptHost";
const char SOCKET_ACCEPT_PORT[] = "SocketAcceptPort";
const char SOCKET_REUSE_ADDRESS[] = "SocketReuseAddress";
const char SOCKET_CONNECT_HOST[] = "SocketConnectHost";
const char SOCKET_CONNECT_PORT[] = "SocketConnectPort";
const char SOCKET_CONNECT_SOURCE_HOST[] = "SocketConnectSourceHost";
const char SOCKET_CONNECT_SOURCE_PORT[] = "SocketConnectSourcePort";
const char SOCKET_NODELAY[] = "SocketNodelay";
const char SOCKET_SEND_BUFFER_SIZE[] = "SocketSendBufferSize";
const char SOCKET_RECEIVE_BUFFER_SIZE[] = "SocketReceiveBufferSize";
const char RECONNECT_INTERVAL[] = "ReconnectInterval";
const char VALIDATE_FIELDS_OUT_OF_ORDER[] = "ValidateFieldsOutOfOrder";
const char VALIDATE_FIELDS_HAVE_VALUES[] = "ValidateFieldsHaveValues";
const char VALIDATE_USER_DEFINED_FIELDS[] = "ValidateUserDefinedFields";
const char LOGON_TIMEOUT[] = "LogonTimeout";
const char LOGOUT_TIMEOUT[] = "LogoutTimeout";
const char FILE_STORE_PATH[] = "FileStorePath";
const char FILE_LOG_PATH[] = "FileLogPath";
const char SCREEN_LOG_SHOW_INCOMING[] = "ScreenLogShowIncoming";
const char SCREEN_LOG_SHOW_OUTGOING[] = "ScreenLogShowOutgoing";
const char SCREEN_LOG_SHOW_EVENTS[] = "ScreenLogShowEvents";
const char SCREEN_LOG_SHOW_DEBUG[] = "ScreenLogShowDebug";
const char RESET_ON_LOGON[] = "ResetOnLogon";
const char RESET_ON_LOGOUT[] = "ResetOnLogout";
const char RESET_ON_DISCONNECT[] = "ResetOnDisconnect";
const char REFRESH_ON_LOGON[] = "RefreshOnLogon";
const char MILLISECONDS_IN_TIMESTAMP[] = "MillisecondsInTimeStamp";
const char PERSIST_MESSAGES[] = "PersistMessages";
const char RESET_ON_STARTUP[] = "ResetOnStartup";
/// In replication mode, all changes to incoming and outgoing sequence numbers
/// are replicated through application and must be acknowledged.
const char REPLICATION_MODE[] = "ReplicationMode";

/// Container for setting dictionaries mapped to sessions.
class SessionSettings {
public:
	SessionSettings() {}
	SessionSettings(std::istream& stream);
	SessionSettings(const std::string& file);

	/// Check if session setings are present
	bool has(const SessionID&) const;

	/// Get a dictionary for a session.
	const Dictionary& get(const SessionID&) const;
	/// Set a dictionary for a session
	void set(const SessionID&, Dictionary);

	/// Get global default settings
	const Dictionary& get() const { return m_defaults; }
	/// Set global default settings
	void set(const Dictionary& defaults);

	/// Number of session settings
	int size() { return m_settings.size(); }

	typedef std::map<SessionID, Dictionary> Dictionaries;
	std::set<SessionID> getSessions() const;

private:
	void validate(const Dictionary&) const;

	Dictionaries m_settings;
	Dictionary m_defaults;

	friend std::ostream& operator<<(std::ostream&, const SessionSettings&);
};
/*! @} */

std::istream& operator>>(std::istream&, SessionSettings&);
std::ostream& operator<<(std::ostream&, const SessionSettings&);
} // namespace FIX

#endif // FIX_SESSIONSETTINGS_H
