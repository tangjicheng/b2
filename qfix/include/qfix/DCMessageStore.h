#ifndef FIX_DC_MESSAGE_STORE_H
#define FIX_DC_MESSAGE_STORE_H

#include <cstdio>
#include <string>
#include <vector>

#include "FieldTypes.h"
#include "MessageStore.h"
#include "SessionSettings.h"

namespace FIX {

class SessionID;

class DCMessageStoreFactory : public MessageStoreFactory {
public:
	explicit DCMessageStoreFactory(const std::string&);
	explicit DCMessageStoreFactory(const SessionSettings&);

	MessageStore* create(const SessionID&) override;
	void destroy(MessageStore*) override;

private:
	std::string m_path;
	SessionSettings m_settings;
};

class DCMessageStoreCache {
public:
	DCMessageStoreCache()
		: m_nextSenderMsgSeqNum(1)
		, m_nextTargetMsgSeqNum(1)
	{
	}

	int getNextSenderMsgSeqNum() const { return m_nextSenderMsgSeqNum; }
	int getNextTargetMsgSeqNum() const { return m_nextTargetMsgSeqNum; }
	void setNextSenderMsgSeqNum(int value) { m_nextSenderMsgSeqNum = value; }
	void setNextTargetMsgSeqNum(int value) { m_nextTargetMsgSeqNum = value; }
	void incrNextSenderMsgSeqNum() { ++m_nextSenderMsgSeqNum; }
	void incrNextTargetMsgSeqNum() { ++m_nextTargetMsgSeqNum; }

	UtcTimeStamp getCreationTime() const { return m_creationTime; }
	void setCreationTime(const UtcTimeStamp& value) { m_creationTime = value; }

	void reset()
	{
		m_nextSenderMsgSeqNum = 1;
		m_nextTargetMsgSeqNum = 1;
		m_creationTime.setCurrent();
	}

private:
	int m_nextSenderMsgSeqNum;
	int m_nextTargetMsgSeqNum;
	UtcTimeStamp m_creationTime;
};

class DCMessageStore : public MessageStore {
public:
	DCMessageStore(const std::string&, const SessionID&);
	~DCMessageStore() override;

	bool set(int, const std::string&) override;
	void get(int, int, std::vector<std::string>&) const override;

	int getNextSenderMsgSeqNum() const override;
	int getNextTargetMsgSeqNum() const override;
	void setNextSenderMsgSeqNum(int) override;
	void setNextTargetMsgSeqNum(int) override;
	void incrNextSenderMsgSeqNum() override;
	void incrNextTargetMsgSeqNum() override;

	UtcTimeStamp getCreationTime() const override;

	void reset() override;
	void refresh() override;

private:
	DCMessageStoreCache m_cache;

	std::string m_messageFilename;
	std::string m_seqnumsFilename;
	std::string m_sessionFilename;

	FILE* m_messageFile;
	FILE* m_seqnumsFile;
	FILE* m_sessionFile;

	void open(bool);
	long find(int) const;
	void getSeqNums();
	void getSession();
	void setSeqNums();
	void setSession();
	void setNullMsg();
};

} // namespace FIX

#endif // FIX_DC_MESSAGE_STORE_H
