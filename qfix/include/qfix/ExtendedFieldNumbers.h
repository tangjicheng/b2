#ifndef FIX_EXTENDED_FIELD_NUMBERS_H
#define FIX_EXTENDED_FIELD_NUMBERS_H

namespace FIX { namespace FIELD {
enum ExtendedField {
	ExposureDuration = 1629,
	ExposureDurationUnit = 1916,
	SecondaryTransactTime = 5164,
	OrderClassification = 8060,
	SecondaryTrdMatchID = 8175,
	MarginTransactionType = 8214
};
}} // namespace FIX::FIELD

#endif // FIX_EXTENDED_FIELD_NUMBERS_H
