/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_SOCKETMONITOR_H
#define FIX_SOCKETMONITOR_H

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>

#include <queue>
#include <set>

namespace FIX {
/// Monitors events on a collection of sockets.
class SocketMonitor {
public:
	class Strategy;

	SocketMonitor(int timeout = 0);
	virtual ~SocketMonitor();

	bool addConnect(int socket);
	bool addRead(int socket);
	bool addWrite(int socket);
	bool drop(int socket);
	void signal(int socket);
	void unsignal(int socket);
	void block(Strategy& strategy, bool poll = 0);

	int numSockets() { return m_readSockets.size() - 1; }

private:
	typedef std::set<int> Sockets;
	typedef std::queue<int> Queue;

	void setsockopt();
	bool bind();
	bool listen();
	inline int getTimeval(bool poll);
	inline bool sleepIfEmpty(bool poll);

	void processEvent(Strategy&, int socket, unsigned int flag);
	void processReadEvent(Strategy&, int socket);
	void processWriteEvent(Strategy&, int socket);
	void processConnectEvent(Strategy&, int socket);
	void processExceptEvent(Strategy&, int socket);
	void subscribeSocket(int s, unsigned int flag);
	void unsubscribeSocket(int s);
	void modifySocket(int s, unsigned int flag);
	void controlSocket(int s, int op, unsigned int flag);

	int m_timeout;
	int m_timeval; // ms
#ifndef SELECT_DECREMENTS_TIME
	clock_t m_ticks;
#endif

	int m_signal;
	int m_interrupt;
	Sockets m_connectSockets;
	Sockets m_readSockets;
	Sockets m_writeSockets;
	Queue m_dropped;
	int m_epfd;

public:
	class Strategy {
	public:
		virtual ~Strategy() {}
		virtual void onConnect(SocketMonitor&, int socket) = 0;
		virtual void onEvent(SocketMonitor&, int socket) = 0;
		virtual void onWrite(SocketMonitor&, int socket) = 0;
		virtual void onError(SocketMonitor&, int socket) = 0;
		virtual void onError(SocketMonitor&) = 0;
		virtual void onTimeout(SocketMonitor&) {}
	};
};
} // namespace FIX

#endif // FIX_SOCKETMONITOR_H
