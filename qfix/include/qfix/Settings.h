#ifndef FIX_SETTINGS_H
#define FIX_SETTINGS_H

#include <istream>
#include <string>
#include <vector>

#include "Dictionary.h"

namespace FIX {

class Settings {
public:
	using Sections = std::vector<Dictionary>;

	Settings() = default;

	// Throws: ConfigError for invalid input.
	explicit Settings(std::istream&);

	Sections get(const std::string&) const noexcept;

private:
	Sections sections_;
};

} // namespace FIX

#endif // FIX_SETTINGS_H
