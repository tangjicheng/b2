#ifndef FIX_REACTOR_ACCEPTOR_H
#define FIX_REACTOR_ACCEPTOR_H

#include <map>
#include <set>
#include <string>

#include <misc/ThreadedReactor.h>

#include "Acceptor.h"
#include "reactor_connection.h"

namespace FIX {

using Acquire_reactor_callback = std::function<misc::ThreadedReactor&(
	const std::string&, const std::string&)>;

class Reactor_acceptor : public Acceptor {
public:
	Reactor_acceptor(Application&, MessageStoreFactory&, const SessionSettings&,
		LogFactory&, Acquire_reactor_callback,
		const std::string& acceptor_reactor_group,
		const std::string& reader_reactor_group);

	virtual ~Reactor_acceptor();

private:
	typedef std::set<std::int32_t> Sockets;
	typedef std::set<SessionID> Sessions;
	typedef std::pair<std::uint32_t, std::uint16_t> Ip_port;
	typedef std::map<Ip_port, Sessions> Ip_port_to_sessions;
	typedef std::map<std::int32_t, Ip_port> Socket_to_ip_port;

	struct Connection_settings {
		std::int32_t no_delay;
		std::int32_t send_buf_size;
		std::int32_t rcv_buf_size;
	};

	// Acceptor
	void onConfigure(const SessionSettings&) override;
	void onInitialize(const SessionSettings&) override;
	void onStart() override;
	bool onPoll() override;
	void onStop() override;
	bool start_in_thread() const override { return false; }

	void add_acceptor_call_id(int socket, misc::Reactor::CallId);
	misc::Reactor::CallStatus try_accept(
		std::int32_t socket, Ip_port, Connection_settings);
	void print_warning(std::int32_t socket);

	Sockets sockets_;
	Ip_port_to_sessions ip_port_to_sessions_;
	Socket_to_ip_port socket_to_ip_port_;
	std::map<std::int32_t, misc::Reactor::CallId> acceptor_calls_;
	std::map<std::int32_t, Reactor_connection*> connections_;
	Acquire_reactor_callback acquire_reactor_;
	std::string acceptor_reactor_group_;
	std::string reader_reactor_group_;
};

} // namespace FIX

#endif // FIX_REACTOR_ACCEPTOR_H
