/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_HANDLER_H
#define FIX_HANDLER_H

#include "Message.h"

namespace FIX {

class Handler {
public:
	virtual ~Handler() {};
	/// Notification of a session begin created
	virtual void onAuth(const Message&) {}
	/// Notification of a session successfully logging on
	virtual void onLogon() = 0;
	/// Notification of a session logging off or disconnecting
	virtual void onLogout() = 0;
	virtual bool onReset(bool) { return true; }
	virtual void onMessageReceived(const std::string&, bool) {}
	virtual void onResend(Message& msg) { toApp(msg); }
	/// Notification of admin message being sent to target
	virtual void toAdmin(Message&) = 0;
	/// Notification of app message being sent to target
	virtual void toApp(Message&) = 0;
	/// Notification of admin message being received from target
	virtual void fromAdmin(const Message&) = 0;
	/// Notification of app message being received from target
	virtual void fromApp(const Message&) = 0;

	/// Notification that session is about to disconnect.
	virtual void on_disconnect() = 0;

	/// Notification that target message sequence number will be incremented.
	virtual void on_incoming_message() = 0;

	/// Notification that target message sequence number will be set to seq_num.
	virtual void on_incoming_reset(std::int32_t seq_num) = 0;

	/// Request that the message should be scheduled for sending.
	virtual void on_outgoing_message(const Message&) = 0;

	/// Request that sender message sequence number should be scheduled to be
	/// set to seq_num.
	virtual void on_outgoing_reset() = 0;

	virtual bool ready_to_read() { return true; }

	virtual void idle() {}
};

} // namespace FIX

#endif // FIX_HANDLER_H
