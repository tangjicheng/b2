#ifndef FIX_EXTENDED_FIELDS_H
#define FIX_EXTENDED_FIELDS_H

#include "Field.h"

namespace FIX {
DEFINE_INT(ExposureDuration);
DEFINE_INT(ExposureDurationUnit);
DEFINE_UTCTIMESTAMP(SecondaryTransactTime);
DEFINE_INT(OrderClassification);
DEFINE_STRING(SecondaryTrdMatchID);
DEFINE_CHAR(MarginTransactionType);
} // namespace FIX

#endif // FIX_EXTENDED_FIELDS_H
