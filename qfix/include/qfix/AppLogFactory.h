#ifndef FIX_APP_LOG_FACTORY_H
#define FIX_APP_LOG_FACTORY_H

#include "Log.h"

namespace FIX {

class AppLogFactory : public LogFactory {
public:
	Log* create() override;
	Log* create(const SessionID&) override;
	void destroy(Log*) override;
};

} // namespace FIX

#endif // FIX_APP_LOG_FACTORY_H
