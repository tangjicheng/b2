/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_LOG_H
#define FIX_LOG_H

#include <map>
#include <mutex>
#include <vector>

#include <misc/Log.h>

#include "Message.h"
#include "SessionSettings.h"

namespace FIX {
class Log;

/**
 * This interface must be implemented to create a Log.
 */
class LogFactory {
public:
	virtual ~LogFactory() {}
	virtual Log* create() = 0;
	virtual Log* create(const SessionID&) = 0;
	virtual void destroy(Log*) = 0;
};

/**
 * Creates a screen based implementation of Log.
 *
 * This displays all log events onto the standard output
 */
class ScreenLogFactory : public LogFactory {
public:
	ScreenLogFactory(const SessionSettings& settings)
		: m_useSettings(true)
		, m_settings(settings) {};
	ScreenLogFactory(bool incoming, bool outgoing, bool event, bool debug)
		: m_incoming(incoming)
		, m_outgoing(outgoing)
		, m_event(event)
		, m_debug(debug)
		, m_useSettings(false)
	{
	}

	Log* create();
	Log* create(const SessionID&);
	void destroy(Log* log);

private:
	void init(const Dictionary& settings, bool& incoming, bool& outgoing,
		bool& event, bool& debug);

	bool m_incoming;
	bool m_outgoing;
	bool m_event;
	bool m_debug;
	bool m_useSettings;
	SessionSettings m_settings;
};

/**
 * This interface must be implemented to log messages and events
 */
class Log {
public:
	Log(bool supports_incoming = true, bool supports_outgoing = true,
		bool supports_event = true, bool supports_debug = true)
		: supports_incoming_ {supports_incoming}
		, supports_outgoing_ {supports_outgoing}
		, supports_event_ {supports_event}
		, supports_debug_ {supports_debug}
	{
	}

	virtual ~Log() {}

	virtual void clear() = 0;
	virtual void onIncoming(const std::string&) = 0;
	virtual void onOutgoing(const std::string&) = 0;
	virtual void onEvent(const std::string&, int log_level = misc::APP_LOG_INFO)
		= 0;
	virtual void debug(int level, const std::string&) = 0;

	bool supports_incoming() const { return supports_incoming_; }
	bool supports_outgoing() const { return supports_outgoing_; }
	bool supports_event() const { return supports_event_; }
	bool supports_debug() const { return supports_debug_; }

private:
	bool supports_incoming_ {true};
	bool supports_outgoing_ {true};
	bool supports_event_ {true};
	bool supports_debug_ {true};
};
/*! @} */

/**
 * Null implementation of Log
 *
 * This is only for internal use. Used when no log factory is
 * passed to the initiator or acceptor.
 */
class NullLog : public Log {
public:
	NullLog()
		: Log {false, false, false, false}
	{
	}

	void clear() {}
	void onIncoming(const std::string&) {}
	void onOutgoing(const std::string&) {}
	void onEvent(const std::string&, int) {}
	void debug(int, const std::string&) override {}
};

/**
 * Screen based implementation of Log.
 *
 * This will display all log information onto the standard output
 */
class ScreenLog : public Log {
public:
	ScreenLog(bool incoming, bool outgoing, bool event, bool debug)
		: Log {incoming, outgoing, event, debug}
		, m_prefix("GLOBAL")
	{
	}

	ScreenLog(const SessionID& sessionID, bool incoming, bool outgoing,
		bool event, bool debug)
		: Log {incoming, outgoing, event, debug}
		, m_prefix(sessionID.toString())
	{
	}

	void clear() {}

	void onIncoming(const std::string& value)
	{
		if (!supports_incoming())
			return;
		std::lock_guard lock(s_mutex);
		m_time.setCurrent();
		std::cout << "<" << UtcTimeStampConvertor::convert(m_time) << ", "
				  << m_prefix << ", "
				  << "incoming>" << std::endl
				  << "  (" << value << ")" << std::endl;
	}

	void onOutgoing(const std::string& value)
	{
		if (!supports_outgoing())
			return;
		std::lock_guard lock(s_mutex);
		m_time.setCurrent();
		std::cout << "<" << UtcTimeStampConvertor::convert(m_time) << ", "
				  << m_prefix << ", "
				  << "outgoing>" << std::endl
				  << "  (" << value << ")" << std::endl;
	}

	void onEvent(const std::string& value, int)
	{
		if (!supports_event())
			return;
		std::lock_guard lock(s_mutex);
		m_time.setCurrent();
		std::cout << "<" << UtcTimeStampConvertor::convert(m_time) << ", "
				  << m_prefix << ", "
				  << "event>" << std::endl
				  << "  (" << value << ")" << std::endl;
	}

	void debug(int level, const std::string& value) override
	{
		if (!supports_debug() || misc::debug < level)
			return;
		const std::lock_guard lock(s_mutex);
		m_time.setCurrent();
		std::cout << "<" << UtcTimeStampConvertor::convert(m_time) << ", "
				  << m_prefix << ", "
				  << "debug>" << std::endl
				  << "  (" << value << ")" << std::endl;
	}

private:
	std::string m_prefix;
	UtcTimeStamp m_time;
	static std::mutex s_mutex;
};
} // namespace FIX

#endif // FIX_LOG_H
