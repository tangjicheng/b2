/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_APPLICATION_H
#define FIX_APPLICATION_H

#include "Handler.h"
#include "SessionID.h"

namespace FIX {
/**
 * This interface must be implemented to define what your %FIX application
 * does.
 *
 * These methods notify your application about events that happen on
 * active %FIX sessions. There is no guarantee how many threads will be calling
 * these functions. If the application is sharing resources among multiple
 * sessions, you must synchronize those resources. You can also use the
 * SynchronizedApplication class to automatically synchronize all function calls
 * into your application. The various MessageCracker classes can be used to
 * parse the generic message structure into specific %FIX messages.
 */
class Application {
public:
	virtual ~Application() {};
	/// Notification of a session begin created
	virtual void onCreate(const SessionID&) = 0;
	virtual void onDestroy(const SessionID&) {}
};
/*! @} */

} // namespace FIX

#endif // FIX_APPLICATION_H
