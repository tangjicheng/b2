/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_XMLDOCUMENT_H
#define FIX_XMLDOCUMENT_H

#include <istream>
#include <map>
#include <memory>
#include <ostream>
#include <string>

#include <libxml/parser.h>
#include <libxml/xmlmemory.h>

#include "Exceptions.h"

namespace FIX {
/// XML attribute as represented by libxml.
class XMLAttributes {
public:
	XMLAttributes(xmlNodePtr pNode)
		: m_pNode(pNode)
	{
	}

	bool get(const std::string&, std::string&);
	std::map<std::string, std::string> toMap();

private:
	xmlNodePtr m_pNode;
};
typedef std::unique_ptr<XMLAttributes> XMLAttributesPtr;

/// XML node as represented by libxml.
class XMLNode {
public:
	XMLNode(xmlNodePtr pNode)
		: m_pNode(pNode)
	{
	}
	~XMLNode() {}

	std::unique_ptr<XMLNode> getFirstChildNode();
	std::unique_ptr<XMLNode> getNextSiblingNode();
	XMLAttributesPtr getAttributes();
	std::string getName();
	std::string getText();

private:
	xmlNodePtr m_pNode;
};
typedef std::unique_ptr<XMLNode> XMLNodePtr;

/// XML document as represented by libxml.
class XMLDocument {
public:
	XMLDocument();
	~XMLDocument();

	bool load(std::istream&);
	bool load(const std::string&);
	bool xml(std::ostream&);

	XMLNodePtr getNode(const std::string&);

private:
	xmlDocPtr m_pDoc;
};
typedef std::unique_ptr<XMLDocument> XMLDocumentPtr;
} // namespace FIX

#endif
