/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_SESSION_H
#define FIX_SESSION_H

#include <atomic>
#include <map>
#include <mutex>
#include <queue>
#include <utility>

#include "Application.h"
#include "DataDictionary.h"
#include "Fields.h"
#include "Log.h"
#include "Responder.h"
#include "SessionID.h"
#include "SessionState.h"

namespace FIX {

enum class Auth_result { NO_ERROR, WAITING, REJECTED, ALREADY_LOGGED_ON };

enum class Disconnect_result { NO_ERROR, WAITING };

enum class Reset_result { NO_ERROR, WAITING };

/// Maintains the state and implements the logic of a %FIX %session.
class Session {
public:
	Session(Application&, MessageStoreFactory&, const SessionID&,
		const DataDictionary&, int heartBtInt, LogFactory* pLogFactory);
	~Session();

	void logon()
	{
		m_state.enabled(true);
		m_state.logoutReason("");
	}
	void logout(const std::string& reason = "")
	{
		m_state.enabled(false);
		m_state.logoutReason(reason);
	}
	bool isEnabled() const { return m_state.enabled(); }

	bool sentLogon() const { return m_state.sentLogon(); }
	bool sentLogout() const { return m_state.sentLogout(); }
	bool receivedLogon() const { return m_state.receivedLogon(); }
	bool isLoggedOn() const { return receivedLogon() && sentLogon(); }
	void reset()
	{
		generateLogout();
		disconnect();
		doReset();
	}
	void refresh() { m_state.refresh(); }
	void setNextSenderMsgSeqNum(int num)
	{
		m_state.setNextSenderMsgSeqNum(num);
	}
	void setNextTargetMsgSeqNum(int num)
	{
		m_state.setNextTargetMsgSeqNum(num);
	}
	void resetStore() { m_state.reset(); }

	const SessionID& getSessionID() const { return m_sessionID; }
	void setDataDictionary(const DataDictionary& dataDictionary)
	{
		m_dataDictionary = dataDictionary;
	}
	const DataDictionary& getDataDictionary() const { return m_dataDictionary; }

	static bool sendToTarget(
		Message& message, const std::string& qualifier = "");
	static bool sendToTarget(Message& message, const SessionID& sessionID);
	static bool sendToTarget(Message&, const SenderCompID& senderCompID,
		const TargetCompID& targetCompID, const std::string& qualifier = "");
	static bool sendToTarget(Message& message, const std::string& senderCompID,
		const std::string& targetCompID, const std::string& qualifier = "");

	static std::set<SessionID> getSessions();
	static bool doesSessionExist(const SessionID&);
	static Session* lookupSession(const SessionID&);
	static Session* lookupSession(const std::string&, bool reverse = false);
	static bool isSessionRegistered(const SessionID&);
	static Session* registerSession(const SessionID&);
	static void unregisterSession(const SessionID&);
	static int numSessions();

	bool isInitiator() const { return m_state.initiate(); }
	bool isAcceptor() const { return !m_state.initiate(); }

	bool getSendRedundantResendRequests() const
	{
		return m_sendRedundantResendRequests;
	}
	void setSendRedundantResendRequests(bool value)
	{
		m_sendRedundantResendRequests = value;
	}

	bool getCheckCompId() const { return m_checkCompId; }
	void setCheckCompId(bool value) { m_checkCompId = value; }

	bool getCheckLatency() const { return m_checkLatency; }
	void setCheckLatency(bool value) { m_checkLatency = value; }

	int getMaxLatency() const { return m_maxLatency; }
	void setMaxLatency(int value) { m_maxLatency = value; }

	int getLogonTimeout() const { return m_state.logonTimeout(); }
	void setLogonTimeout(int value) { m_state.logonTimeout(value); }

	int getLogoutTimeout() const { return m_state.logoutTimeout(); }
	void setLogoutTimeout(int value) { m_state.logoutTimeout(value); }

	bool getResetOnLogon() const { return m_resetOnLogon; }
	void setResetOnLogon(bool value) { m_resetOnLogon = value; }

	bool getMillisecondsInTimeStamp() const
	{
		return m_millisecondsInTimeStamp;
	}
	void setMillisecondsInTimeStamp(bool value)
	{
		m_millisecondsInTimeStamp = value;
	}

	bool getPersistMessages() const { return m_persistMessages; }
	void setPersistMessages(bool value) { m_persistMessages = value; }

	bool get_replication_mode() const { return replication_mode_; }
	void set_replication_mode(bool value) { replication_mode_ = value; }

	std::string getRemoteAddress() const
	{
		return m_pResponder ? m_pResponder->getRemoteAddress() : std::string();
	}

	void setResponder(Responder* pR) { m_pResponder = pR; }

	void set_handler(Handler& handler) { handler_ = &handler; }

	void destroy_handler() { handler_ = nullptr; }

	bool send(Message&);
	void next();
	void next(const std::string&, bool queued = false);
	void next(const Message&, bool queued = false);
	void disconnect();
	void schedule_disconnect();

	long getExpectedSenderNum() const
	{
		return m_state.getNextSenderMsgSeqNum();
	}
	long getExpectedTargetNum() const
	{
		return m_state.getNextTargetMsgSeqNum();
	}

	Log* getLog() { return &m_state; }
	const MessageStore* getStore() const { return &m_state; }

	void lock() { m_mutex.lock(); }
	void unlock() { m_mutex.unlock(); }

	void set_auth_wait();
	void wait_for_auth_ack() const;
	void ack_auth(Auth_result);

	void set_disconnect_wait();
	void wait_for_disconnect_ack() const;
	void ack_disconnect(Disconnect_result);

	void set_reset_wait();
	void wait_for_reset_ack() const;
	void ack_reset(Reset_result);

	void on_incoming_message();
	void on_incoming_reset(std::int32_t seq_num);
	void on_outgoing_message(Message&);
	void on_outgoing_reset();

	void ack_incoming_message();
	void ack_incoming_reset(std::int32_t);
	void ack_outgoing_message(Message&);
	void ack_outgoing_reset();

	bool ready_to_read() const;

private:
	typedef std::map<SessionID, Session*> Sessions;
	typedef std::set<SessionID> SessionIDs;

	static bool addSession(Session&);
	static void removeSession(Session&);

	bool send(const std::string&);
	bool sendRaw(Message&, int msgSeqNum = 0);
	bool resend(Message& message);
	void persist(Message&, int, std::string&);

	void insertSendingTime(Header&, const UtcTimeStamp&) const;
	void insertOrigSendingTime(Header&, const UtcTimeStamp&) const;
	void fill(Header&);

	void disconnect_on_already_logged_on();

	bool isGoodTime(
		const SendingTime& sendingTime, const UtcTimeStamp& now) const
	{
		if (!m_checkLatency)
			return true;
		return labs(now - sendingTime.getValue()) <= m_maxLatency;
	}
	bool isTargetTooHigh(int msgSeqNum) const
	{
		return msgSeqNum > (m_state.getNextTargetMsgSeqNum());
	}
	bool isTargetTooLow(int msgSeqNum) const
	{
		return msgSeqNum < (m_state.getNextTargetMsgSeqNum());
	}
	bool isCorrectCompID(const SenderCompID& senderCompID,
		const TargetCompID& targetCompID) const
	{
		if (!m_checkCompId)
			return true;

		return m_sessionID.getSenderCompID().getValue()
			== targetCompID.getValue()
			&& m_sessionID.getTargetCompID().getValue()
			== senderCompID.getValue();
	}

	bool validLogonState(const MsgType& msgType) const;
	void fromCallback(const MsgType& msgType, const Message& msg,
		const SessionID& sessionID) const;

	void doReset(bool locked = false);

	void doBadTime(const Message& msg);
	void doBadCompID(const Message& msg);
	bool doPossDup(const Message& msg);
	bool doTargetTooLow(const Message& msg);
	void doTargetTooHigh(const Message& msg);
	void nextQueued();
	bool nextQueued(int num);

	void nextLogon(const Message&, const UtcTimeStamp&);
	void nextHeartbeat(const Message&, const UtcTimeStamp&);
	void nextTestRequest(const Message&, const UtcTimeStamp&);
	void nextLogout(const Message&, const UtcTimeStamp&);
	void nextReject(const Message&, const UtcTimeStamp&);
	void nextSequenceReset(const Message&, const UtcTimeStamp&);
	void nextResendRequest(const Message&, const UtcTimeStamp&);

	void generateLogon();
	void generateLogon(const Message&);
	void generateResendRequest(const BeginString&, const MsgSeqNum&);
	void generateSequenceReset(int, int);
	void generateHeartbeat();
	void generateHeartbeat(const Message&);
	void generateTestRequest(const std::string&);
	void generateReject(const Message&, int err);
	void generateReject(
		const Message&, int err, int field, const std::string& text);
	void generateReject(const Message&, const std::string&);
	void generateBusinessReject(const Message&, int err, int field = 0);
	void generateLogout(const std::string& text = "");

	void populateRejectReason(Message&, int field, const std::string&);
	void populateRejectReason(Message&, const std::string&);

	bool verify(const Message& msg, const UtcTimeStamp& now,
		bool checkTooHigh = true, bool checkTooLow = true);

	bool set(int s, const Message& m);
	bool get(int s, Message& m) const;

	Application& m_application;
	Handler* handler_;
	SessionID m_sessionID;

	bool m_sendRedundantResendRequests;
	bool m_checkCompId;
	bool m_checkLatency;
	int m_maxLatency;
	bool m_resetOnLogon;
	bool m_millisecondsInTimeStamp;
	bool m_persistMessages;
	bool replication_mode_ {false};

	SessionState m_state;
	DataDictionary m_dataDictionary;
	MessageStoreFactory& m_messageStoreFactory;
	LogFactory* m_pLogFactory;
	Responder* m_pResponder;
	std::recursive_mutex m_mutex;
	std::atomic<Auth_result> auth_result_ {Auth_result::NO_ERROR};
	std::atomic<Disconnect_result> disconnect_result_ {
		Disconnect_result::NO_ERROR};
	std::atomic<Reset_result> reset_result_ {Reset_result::NO_ERROR};

	static Sessions s_sessions;
	static SessionIDs s_sessionIDs;
	static Sessions s_registered;
	static std::mutex s_mutex;
};
} // namespace FIX

#endif // FIX_SESSION_H
