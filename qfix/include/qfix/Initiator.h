/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_INITIATOR_H
#define FIX_INITIATOR_H

#include <atomic>
#include <map>
#include <mutex>
#include <set>
#include <string>

#include "Application.h"
#include "Exceptions.h"
#include "Log.h"
#include "MessageStore.h"
#include "Responder.h"
#include "Session.h"
#include "SessionSettings.h"

namespace FIX {
class Client;

/**
 * Base for classes which act as an initiator for establishing connections.
 *
 * Most users will not need to implement one of these.  The default
 * SocketInitiator implementation will be used in most cases.
 */
class Initiator : public Log {
public:
	Initiator(Application&, MessageStoreFactory&, const SessionSettings&);
	Initiator(Application&, MessageStoreFactory&, const SessionSettings&,
		LogFactory&);

	virtual ~Initiator();

	/// Start initiator.
	void start();
	/// Block on the initiator
	void block();
	/// Poll the initiator
	bool poll();

	/// Stop initiator.
	void stop(bool force = false);

	/// Check to see if any sessions are currently logged on
	bool isLoggedOn();

	Session* getSession(const SessionID& sessionID, Responder&);
	const std::set<SessionID> getSessions() const { return m_sessionIDs; }

	bool has(const SessionID& id)
	{
		return m_sessions.find(id) != m_sessions.end();
	}

	bool isStopped() { return m_stop.load(std::memory_order_relaxed); }

public:
	Application& getApplication() { return m_application; }
	MessageStoreFactory& getMessageStoreFactory()
	{
		return m_messageStoreFactory;
	}

	Log* getLog()
	{
		if (m_pLog)
			return m_pLog;
		return &m_nullLog;
	}

protected:
	void setPending(const SessionID&);
	void setConnected(const SessionID&);
	void setDisconnected(const SessionID&);

	void connect();

public:
	void onEvent(const std::string& text, int log_level = misc::APP_LOG_INFO)
	{
		if (m_pLog && m_pLog->supports_event())
			m_pLog->onEvent(text, log_level);
	}
	void onIncoming(const std::string& text)
	{
		if (m_pLog && m_pLog->supports_incoming())
			m_pLog->onIncoming(text);
	}
	void onOutgoing(const std::string& text)
	{
		if (m_pLog && m_pLog->supports_outgoing())
			m_pLog->onOutgoing(text);
	}
	void debug(int level, const std::string& text) override
	{
		if (m_pLog && m_pLog->supports_debug())
			m_pLog->debug(level, text);
	}
	void clear()
	{
		if (m_pLog)
			m_pLog->clear();
	}

private:
	void initialize();
	void doSetDisconnected(const SessionID&);

	/// Implemented to configure acceptor
	virtual void onConfigure(const SessionSettings&) {};
	/// Implemented to initialize initiator
	virtual void onInitialize(const SessionSettings&) {};
	/// Implemented to start connecting to targets.
	virtual void onStart() = 0;
	/// Implemented to connect and poll for events.
	virtual bool onPoll() = 0;
	/// Implemented to stop a running initiator.
	virtual void onStop() = 0;
	/// Implemented to connect a session to its target.
	virtual bool doConnect(const SessionID&, const Dictionary&) = 0;

	static THREAD_PROC startThread(void* p);

	typedef std::set<SessionID> SessionIDs;
	typedef std::map<SessionID, int> SessionState;
	typedef std::map<SessionID, Session*> Sessions;

	Sessions m_sessions;
	SessionIDs m_sessionIDs;
	SessionIDs m_pending;
	SessionIDs m_connected;
	SessionIDs m_disconnected;
	SessionState m_sessionState;

	thread_id m_threadid;
	Application& m_application;
	MessageStoreFactory& m_messageStoreFactory;
	SessionSettings m_settings;
	LogFactory* m_pLogFactory;
	Log* m_pLog;
	NullLog m_nullLog;
	bool m_firstPoll;
	std::atomic<bool> m_stop {false};
	std::mutex m_mutex;
};
/*! @} */
} // namespace FIX

#endif // FIX_INITIATOR_H
