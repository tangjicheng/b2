/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_SOCKETCONNECTION_H
#define FIX_SOCKETCONNECTION_H

#include <mutex>
#include <set>

#include <sys/epoll.h>

#include "Parser.h"
#include "Responder.h"
#include "SessionID.h"
#include "SocketMonitor.h"
#include "Utility.h"

namespace FIX {
class SocketAcceptor;
class SocketServer;
class SocketConnector;
class SocketInitiator;
class Session;

/// Encapsulates a socket file descriptor (single-threaded).
class SocketConnection : Responder {
public:
	typedef std::set<SessionID> Sessions;

	SocketConnection(int s, Sessions sessions, SocketMonitor* pMonitor);
	SocketConnection(SocketInitiator&, const SessionID&, int, SocketMonitor*);
	virtual ~SocketConnection();

	int getSocket() const { return m_socket; }
	Session* getSession() const { return m_pSession; }

	bool read(SocketConnector& s);
	bool read(SocketAcceptor&, SocketServer&);
	bool processQueue();

	void unsignal()
	{
		std::lock_guard lock(m_mutex);
		if (m_sendQueue.size() == 0)
			m_pMonitor->unsignal(m_socket);
	}

	void onTimeout();

private:
	typedef std::deque<std::string> Queue;

	bool doProcessQueue();

	void signal()
	{
		if (m_sendQueue.size() == 1)
			m_pMonitor->signal(m_socket);
	}

	bool isValidSession();
	void readFromSocket();
	bool readMessage(std::string& msg);
	void readMessages(SocketMonitor& s);
	std::string getRemoteAddress() const;
	bool send(const std::string&);
	void schedule_disconnect() override;
	void disconnect();
	void initializeEpoll();

	int m_socket;

	Parser m_parser;
	Queue m_sendQueue;
	unsigned m_sendLength;
	Sessions m_sessions;
	Session* m_pSession;
	SocketMonitor* m_pMonitor;
	std::mutex m_mutex;
	int m_epfd;
};
} // namespace FIX

#endif // FIX_SOCKETCONNECTION_H
