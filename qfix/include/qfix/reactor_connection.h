#ifndef FIX_REACTOR_CONNECTION_H
#define FIX_REACTOR_CONNECTION_H

#include <map>
#include <set>
#include <string>

#include <misc/ThreadedReactor.h>

#include <sys/epoll.h>

#include "Parser.h"
#include "Responder.h"
#include "SessionID.h"

namespace FIX {
class Application;
class Log;
class Reactor_acceptor;
class Session;

class Reactor_connection : Responder {
public:
	typedef std::set<SessionID> Sessions;

	Reactor_connection(
		std::int32_t socket, Sessions, Log*, misc::ThreadedReactor&);

	// Responder
	std::string getRemoteAddress() const override;
	void disconnect() override;

	Session* get_session() const { return session_; }
	std::int32_t get_socket() const { return socket_; }
	bool connect();

	void start();
	void shutdown();

	bool read_socket();
	void io_process_read_buffer();
	bool process_read_buffer();
	void keep_alive();

private:
	// Responder
	bool send(const std::string&) override;
	void schedule_disconnect() override;

	bool read_message(std::string& msg);
	bool process_stream();
	bool set_session(const std::string& msg);
	void session_disconnect();

	std::int32_t socket_;
	std::string buffer_string_;
	std::string address_;
	std::uint16_t port_;
	std::string source_address_;
	std::uint16_t source_port_;
	Log* logger_;
	Parser parser_;
	Sessions sessions_;
	Session* session_;
	bool disconnect_;
	misc::ThreadedReactor* reactor_;
	misc::Reactor::CallId read_call_id_;
	misc::Reactor::CallId exception_call_id_;
	misc::Reactor::CallId heartbeat_call_id_;
};

} // namespace FIX

#endif // FIX_REACTOR_CONNECTION_H
