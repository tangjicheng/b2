/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_ACCEPTOR_H
#define FIX_ACCEPTOR_H

#include <atomic>
#include <map>
#include <string>

#include "Application.h"
#include "Exceptions.h"
#include "Log.h"
#include "MessageStore.h"
#include "Responder.h"
#include "SessionSettings.h"

namespace FIX {
class Client;
class Session;

/**
 * Base for classes which act as an acceptor for incoming connections.
 *
 * Most users will not need to implement one of these.  The default
 * SocketAcceptor implementation will be used in most cases.
 */
class Acceptor : public Log {
public:
	Acceptor(Application&, MessageStoreFactory&, const SessionSettings&);
	Acceptor(Application&, MessageStoreFactory&, const SessionSettings&,
		LogFactory&);

	virtual ~Acceptor();

	Log* getLog()
	{
		if (m_pLog)
			return m_pLog;
		return &m_nullLog;
	}

	/// Start acceptor.
	void start();
	/// Block on the acceptor
	void block();
	/// Poll the acceptor
	bool poll();

	/// Stop acceptor.
	void stop(bool force = false);

	/// Check to see if any sessions are currently logged on
	bool isLoggedOn();

	Session* getSession(const std::string& msg, Responder&);
	const std::set<SessionID> getSessions() const { return m_sessionIDs; }

	bool has(const SessionID& id)
	{
		return m_sessions.find(id) != m_sessions.end();
	}

	bool isStopped() { return m_stop.load(std::memory_order_relaxed); }

	Application& getApplication() { return m_application; }
	MessageStoreFactory& getMessageStoreFactory()
	{
		return m_messageStoreFactory;
	}

public:
	void onEvent(const std::string& text, int log_level = misc::APP_LOG_INFO)
	{
		if (m_pLog && m_pLog->supports_event())
			m_pLog->onEvent(text, log_level);
	}
	void onIncoming(const std::string& text)
	{
		if (m_pLog && m_pLog->supports_incoming())
			m_pLog->onIncoming(text);
	}
	void onOutgoing(const std::string& text)
	{
		if (m_pLog && m_pLog->supports_outgoing())
			m_pLog->onOutgoing(text);
	}
	void debug(int level, const std::string& text) override
	{
		if (m_pLog && m_pLog->supports_debug())
			m_pLog->debug(level, text);
	}
	void clear()
	{
		if (m_pLog)
			m_pLog->clear();
	}

protected:
	virtual bool start_in_thread() const { return true; }

private:
	void initialize();

	/// Implemented to configure acceptor
	virtual void onConfigure(const SessionSettings&) {};
	/// Implemented to initialize acceptor
	virtual void onInitialize(const SessionSettings&) {};
	/// Implemented to start listening for connections.
	virtual void onStart() = 0;
	/// Implemented to connect and poll for events.
	virtual bool onPoll() = 0;
	/// Implemented to stop a running acceptor.
	virtual void onStop() = 0;

	static THREAD_PROC startThread(void* p);

	typedef std::set<SessionID> SessionIDs;
	typedef std::map<SessionID, Session*> Sessions;

	thread_id m_threadid;
	Sessions m_sessions;
	SessionIDs m_sessionIDs;
	Application& m_application;
	MessageStoreFactory& m_messageStoreFactory;
	SessionSettings m_settings;
	LogFactory* m_pLogFactory;
	Log* m_pLog;
	NullLog m_nullLog;
	bool m_firstPoll;
	std::atomic<bool> m_stop {false};
};
/*! @} */
} // namespace FIX

#endif // FIX_ACCEPTOR_H
