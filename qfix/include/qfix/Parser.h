/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_PARSER_H
#define FIX_PARSER_H

#include <iostream>
#include <string>

#include "Exceptions.h"

namespace FIX {

enum class Read_result { success, again, closed, error };

/// Parses %FIX messages off an input stream.
class Parser {
public:
	Parser() {}

	Read_result read(int socket);
	Read_result read(const std::string&);
	bool extract_fix_message(std::string& str);

	void clear();

private:
	char data_[BUFSIZ];
	char* begin_ {data_};
	char* end_ {data_};

	size_t size() const { return end_ - begin_; }
	bool is_full() const { return size() == max_size(); }
	static constexpr size_t max_size() { return sizeof(data_); }
	bool extract_length(size_t& length, char*& pos);
};
} // namespace FIX
#endif // FIX_PARSER_H
