/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_FIELDMAP
#define FIX_FIELDMAP

#include <algorithm>
#include <map>
#include <sstream>
#include <vector>

#include "Exceptions.h"
#include "Field.h"
#include "MessageSorters.h"
#include "Utility.h"

namespace FIX {
/**
 * Stores and organizes a collection of Fields.
 *
 * This is the basis for a message, header, and trailer.  This collection
 * class uses a sorter to keep the fields in a particular order.
 */
class FieldMap {
public:
	typedef std::multimap<int, FieldBase, message_order> Fields;
	typedef std::map<int, std::vector<FieldMap*>, std::less<int>> Groups;

	typedef Fields::const_iterator iterator;
	typedef iterator const_iterator;
	typedef Groups::const_iterator g_iterator;
	typedef Groups::const_iterator g_const_iterator;

	FieldMap(const message_order& order = message_order(message_order::normal))
		: m_fields(order)
	{
	}

	FieldMap(const int order[])
		: m_fields(message_order(order))
	{
	}

	FieldMap(const FieldMap& copy) { *this = copy; }

	virtual ~FieldMap();

	FieldMap& operator=(const FieldMap& rhs);

	/// Set a field without type checking
	FieldBase& setField(FieldBase&& field, bool overwrite = true)
	{
		Fields::iterator i;
		if (!overwrite)
			i = m_fields.emplace(field.getField(), std::move(field));
		else {
			i = m_fields.lower_bound(field.getField());
			if (i == m_fields.end()
				|| m_fields.key_comp()(field.getField(), i->first))
				i = m_fields.emplace_hint(
					i, field.getField(), std::move(field));
			else
				i->second = std::move(field);
		}
		return i->second;
	}
	/// Set a field without type checking
	FieldBase& setField(const FieldBase& field, bool overwrite = true)
	{
		Fields::iterator i;
		if (!overwrite)
			i = m_fields.emplace(field.getField(), field);
		else {
			i = m_fields.lower_bound(field.getField());
			if (i == m_fields.end()
				|| m_fields.key_comp()(field.getField(), i->first))
				i = m_fields.emplace_hint(i, field.getField(), field);
			else
				i->second = field;
		}
		return i->second;
	}
	/// Set a field without a field class
	FieldBase& setField(int field, const std::string& value)
	{
		FieldBase fieldBase(field, value);
		return setField(std::move(fieldBase));
	}

	/// Get a field without type checking
	FieldBase& getField(FieldBase& field) const
	{
		Fields::const_iterator iter = m_fields.find(field.getField());
		if (iter == m_fields.end())
			throw FieldNotFound(field.getField());
		field = iter->second;
		return field;
	}

	/// Get a field without a field class
	const std::string& getField(int field) const
	{
		return getFieldRef(field).getString();
	}

	/// Get direct access to a field through a referencce
	const FieldBase& getFieldRef(int field) const
	{
		Fields::const_iterator iter = m_fields.find(field);
		if (iter == m_fields.end())
			throw FieldNotFound(field);
		return iter->second;
	}

	/// Get direct access to a field through a pointer
	const FieldBase* getFieldPtr(int field) const
	{
		return &getFieldRef(field);
	}

	/// Get direct access to a field through a pointer (non-throw version of
	/// getFieldPtr)
	const FieldBase* findField(int field) const
	{
		Fields::const_iterator iter = m_fields.find(field);
		if (iter == m_fields.end())
			return NULL;
		return &iter->second;
	}

	/// Check to see if a field is set
	bool isSetField(const FieldBase& field) const
	{
		return m_fields.find(field.getField()) != m_fields.end();
	}
	/// Check to see if a field is set by referencing its number
	bool isSetField(int field) const
	{
		return m_fields.find(field) != m_fields.end();
	}

	/// Remove a field. If field is not present, this is a no-op.
	void removeField(int field);

	/// Add a group.
	void addGroup(int field, const FieldMap& group, bool setCount = true);

	/// Replace a specific instanct of a group.
	void replaceGroup(int num, int field, FieldMap& group);

	/// Get a specific instance of a group.
	FieldMap& getGroup(int num, int field, FieldMap& group) const;

	/// Remove a specific instance of a group.
	void removeGroup(int num, int field);
	/// Remove all instances of a group.
	void removeGroup(int field);

	/// Check to see any instance of a group exists
	bool hasGroup(int field) const;
	/// Check to see if a specific instance of a group exists
	bool hasGroup(int num, int field) const;
	/// Count the number of instance of a group
	int groupCount(int field) const;

	/// Clear all fields from the map
	void clear();
	/// Check if map contains any fields
	bool isEmpty() const;

	int totalFields() const;

	std::string& calculateString(std::string&, bool clear = true) const;

	int calculateLength(int beginStringField = FIELD::BeginString,
		int bodyLengthField = FIELD::BodyLength,
		int checkSumField = FIELD::CheckSum) const;

	int calculateTotal(int checkSumField = FIELD::CheckSum) const;

	iterator begin() const { return m_fields.begin(); }
	iterator end() const { return m_fields.end(); }
	g_iterator g_begin() const { return m_groups.begin(); }
	g_iterator g_end() const { return m_groups.end(); }

private:
	Fields m_fields;
	Groups m_groups;
};
/*! @} */
} // namespace FIX

#define FIELD_SET(MAP, FIELD)                                                \
	bool isSet(const FIELD& field) const { return (MAP).isSetField(field); } \
	FIELD& set(const FIELD& field) { return (FIELD&)(MAP).setField(field); } \
	FIELD& get(FIELD& field) const { return (FIELD&)(MAP).getField(field); }

#define FIELD_GET_PTR(MAP, FLD) \
	((const FIX::FLD*)MAP.getFieldPtr(FIX::FIELD::FLD))
#define FIELD_GET_REF(MAP, FLD) \
	((const FIX::FLD&)MAP.getFieldRef(FIX::FIELD::FLD))
#define FIELD_FIND_PTR(MAP, FLD) \
	((const FIX::FLD*)MAP.findField(FIX::FIELD::FLD))

#endif // FIX_FIELDMAP
