#include <cstdint>
#include <iomanip>
#include <ios>
#include <limits>
#include <sstream>
#include <string>

#include <gtest/gtest.h>

#include <qfix/FileStore.h>
#include <qfix/MessageStore.h>
#include <qfix/SessionID.h>

namespace {

constexpr uint64_t message_size = 1024;
const FIX::SessionID session_id {"FIX.4.2", "sender", "target"};

std::string create_message(uint64_t seq_num)
{
	constexpr auto hex_digits = sizeof(seq_num) * 2;
	constexpr auto count = message_size / hex_digits;
	auto oss = std::ostringstream {};
	oss << std::hex << std::setfill('0');
	for (uint64_t i = 0; i != count; ++i)
		oss << std::setw(hex_digits) << seq_num;
	return oss.str();
}

void set_messages(FIX::MessageStore& store, uint64_t count)
{
	store.reset();
	for (uint64_t i = 0; i != count; ++i)
		ASSERT_NO_THROW(store.set(i, create_message(i)));
	ASSERT_NO_THROW(store.set(count, create_message(count)));
}

void two_gig_limit(FIX::MessageStore& store)
{
	constexpr auto count
		= (static_cast<uint64_t>(std::numeric_limits<int32_t>::max()) + 1)
		/ message_size;
	set_messages(store, count);
}

void thousand_limit(FIX::MessageStore& store)
{
	set_messages(store, 1000);
}

} // namespace

TEST(MemoryStore, two_gig_limit)
{
	auto store = FIX::MemoryStore {};
	two_gig_limit(store);
}

TEST(FileStore, thousand_limit)
{
	auto store = FIX::FileStore {"store", session_id, false};
	thousand_limit(store);
}
