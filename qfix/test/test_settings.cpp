#include <sstream>

#include <gtest/gtest.h>

#include <qfix/Settings.h>

TEST(Settings, constructor)
{
	std::istringstream iss {"[DEFAULT]\n"
							"ConnectionType=acceptor\n"
							"SocketReuseAddress=Y\n"
							"SocketNodelay=Y\n"
							"BeginString=FIX.4.2\n"
							"[SESSION]\n"
							"TargetCompID=User1\n"
							"SocketAcceptPort=5002\n"
							"SocketAcceptHost=127.0.0.1\n"
							"CancelOnDisconnect=Y\n"
							"[SESSION]\n"
							"TargetCompID=User2\n"
							"SocketAcceptPort=5002\n"
							"SocketAcceptHost=127.0.0.1\n"
							"ThrottleRate=25"};
	FIX::Settings settings {iss};
	{
		auto section = settings.get("NONE");
		ASSERT_EQ(0u, section.size());
	}
	{
		auto section = settings.get("DEFAULT");
		ASSERT_EQ(1u, section.size());
		{
			auto& dictionary = section[0];
			ASSERT_EQ("DEFAULT", dictionary.getName());
			ASSERT_EQ(4, dictionary.size());

			ASSERT_EQ("acceptor", dictionary.getString("ConnectionType"));
			ASSERT_EQ("Y", dictionary.getString("SocketReuseAddress"));
			ASSERT_EQ("Y", dictionary.getString("SocketNodelay"));
			ASSERT_EQ("FIX.4.2", dictionary.getString("BeginString"));
		}
	}
	{
		auto section = settings.get("SESSION");
		ASSERT_EQ(2u, section.size());
		{
			auto& dictionary = section[0];
			ASSERT_EQ("SESSION", dictionary.getName());
			ASSERT_EQ(4, dictionary.size());

			ASSERT_EQ("User1", dictionary.getString("TargetCompID"));
			ASSERT_EQ("5002", dictionary.getString("SocketAcceptPort"));
			ASSERT_EQ("127.0.0.1", dictionary.getString("SocketAcceptHost"));
			ASSERT_EQ("Y", dictionary.getString("CancelOnDisconnect"));
		}
		{
			auto& dictionary = section[1];
			ASSERT_EQ("SESSION", dictionary.getName());
			ASSERT_EQ(4, dictionary.size());

			ASSERT_EQ("User2", dictionary.getString("TargetCompID"));
			ASSERT_EQ("5002", dictionary.getString("SocketAcceptPort"));
			ASSERT_EQ("127.0.0.1", dictionary.getString("SocketAcceptHost"));
			ASSERT_EQ("25", dictionary.getString("ThrottleRate"));
		}
	}
}

TEST(Settings, whitespace)
{
	std::istringstream iss {
		"[WHITESPACE]\n"
		"  leading key=leading key spaces not stripped\n"
		"trailing key  =trailing key spaces not stripped\n"
		"leading value=  leading value spaces not stripped\n"
		"trailing value=trailing value spaces not stripped  \n"
		"  spaced  =  out  "};
	FIX::Settings settings {iss};

	auto section = settings.get("WHITESPACE");
	ASSERT_EQ(1u, section.size());

	auto& dictionary = section[0];
	ASSERT_EQ("WHITESPACE", dictionary.getName());
	ASSERT_EQ(5, dictionary.size());

	ASSERT_EQ("leading key spaces not stripped",
		dictionary.getString("  leading key"));
	ASSERT_EQ("trailing key spaces not stripped",
		dictionary.getString("trailing key  "));
	ASSERT_EQ("  leading value spaces not stripped",
		dictionary.getString("leading value"));
	ASSERT_EQ("trailing value spaces not stripped  ",
		dictionary.getString("trailing value"));
	ASSERT_EQ("  out  ", dictionary.getString("  spaced  "));
}

TEST(Settings, blank)
{
	std::istringstream iss {"[ ]\n"
							" = "};
	FIX::Settings settings {iss};

	auto section = settings.get(" ");
	ASSERT_EQ(1u, section.size());

	auto& dictionary = section[0];
	ASSERT_EQ(" ", dictionary.getName());
	ASSERT_EQ(1, dictionary.size());

	ASSERT_EQ(" ", dictionary.getString(" "));
}

TEST(Settings, newline)
{
	std::istringstream iss {"[NEWLINE]\n"
							"\n"
							"# above is a newline\n"
							"# below is a newline\n"
							"\n"
							"new=line"};
	FIX::Settings settings {iss};

	auto section = settings.get("NEWLINE");
	ASSERT_EQ(1u, section.size());

	auto& dictionary = section[0];
	ASSERT_EQ("NEWLINE", dictionary.getName());
	ASSERT_EQ(1, dictionary.size());

	ASSERT_EQ("line", dictionary.getString("new"));
}

TEST(Settings, comment)
{
	std::istringstream iss {"[COMMENT]\n"
							"# this is a comment\n"
							" # this is = not a comment"};
	FIX::Settings settings {iss};

	auto section = settings.get("COMMENT");
	ASSERT_EQ(1u, section.size());

	auto& dictionary = section[0];
	ASSERT_EQ("COMMENT", dictionary.getName());
	ASSERT_EQ(1, dictionary.size());

	ASSERT_EQ(" not a comment", dictionary.getString(" # this is "));
}

TEST(Settings, valid)
{
	std::istringstream iss {"[VALID]\n"
							"k=v\n"
							"key=value"};
	FIX::Settings settings {iss};

	auto section = settings.get("VALID");
	ASSERT_EQ(1u, section.size());

	auto& dictionary = section[0];
	ASSERT_EQ("VALID", dictionary.getName());
	ASSERT_EQ(2, dictionary.size());

	ASSERT_EQ("v", dictionary.getString("k"));
	ASSERT_EQ("value", dictionary.getString("key"));
}

TEST(Settings, empty_section_name)
{
	try {
		std::istringstream iss {"[DEFAULT]\n"
								"[]"};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: Empty section name", e.what());
	}
}

TEST(Settings, no_separator)
{
	try {
		std::istringstream iss {"[DEFAULT]\n"
								"1"};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: No separator", e.what());
	}
	try {
		std::istringstream iss {"[DEFAULT]\n"
								"22"};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: No separator", e.what());
	}
	try {
		std::istringstream iss {"[DEFAULT]\n"
								"key value"};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: No separator", e.what());
	}
}

TEST(Settings, empty_key)
{
	try {
		std::istringstream iss {"[DEFAULT]\n"
								"="};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: Empty key", e.what());
	}
	try {
		std::istringstream iss {"[DEFAULT]\n"
								"=2"};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: Empty key", e.what());
	}
	try {
		std::istringstream iss {"[DEFAULT]\n"
								"=value"};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: Empty key", e.what());
	}
}

TEST(Settings, empty_value)
{
	try {
		std::istringstream iss {"[DEFAULT]\n"
								"2="};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: Empty value", e.what());
	}
	try {
		std::istringstream iss {"[DEFAULT]\n"
								"key="};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: Empty value", e.what());
	}
}

TEST(Settings, no_section_defined)
{
	try {
		std::istringstream iss {"key=value\n"
								"[SESSION]"};
		FIX::Settings settings {iss};
		ASSERT_TRUE(false);
	}
	catch (const FIX::ConfigError& e) {
		ASSERT_STREQ("Configuration failed: No section defined", e.what());
	}
}
