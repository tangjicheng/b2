#include <string>

#include <gtest/gtest.h>

#include <getopt.h>
#include <qfix/XMLDocument.h>

std::string FIX42 = "lib/qfix/spec/FIX42.xml";

class TestXMLDocument : public testing::Test {
protected:
	void SetUp() override { d.load(FIX42); }

	FIX::XMLDocument d;
};

TEST_F(TestXMLDocument, load)
{
	auto b = d.load(FIX42);
	;
	ASSERT_TRUE(b);
}

TEST_F(TestXMLDocument, getNode)
{
	ASSERT_TRUE(static_cast<bool>(d.getNode("/fix")));
	ASSERT_FALSE(static_cast<bool>(d.getNode("/repair")));
}

TEST_F(TestXMLDocument, getName)
{
	auto n = d.getNode("/fix");

	ASSERT_EQ("fix", n->getName());
}

TEST_F(TestXMLDocument, getAttributes)
{
	auto n = d.getNode("/fix");

	auto a = n->getAttributes();
	std::string major;
	ASSERT_TRUE(a->get("major", major));
	ASSERT_EQ("4", major);
	std::string minor;
	ASSERT_TRUE(a->get("minor", minor));
	ASSERT_EQ("2", minor);
	std::string version;
	ASSERT_FALSE(a->get("version", version));
	ASSERT_TRUE(version.empty());
}

TEST_F(TestXMLDocument, childNodes)
{
	auto n = d.getNode("/fix/fields");
	ASSERT_TRUE(static_cast<bool>(n));
	ASSERT_EQ("fields", n->getName());

	auto c = n->getFirstChildNode();
	ASSERT_TRUE(static_cast<bool>(c));
	ASSERT_EQ("text", c->getName());

	c = c->getNextSiblingNode();
	ASSERT_TRUE(static_cast<bool>(c));
	ASSERT_EQ("field", c->getName());
	{
		auto a = c->getAttributes();
		std::string number;
		ASSERT_TRUE(a->get("number", number));
		ASSERT_EQ("1", number);
		std::string name;
		ASSERT_TRUE(a->get("name", name));
		ASSERT_EQ("Account", name);
		std::string type;
		ASSERT_TRUE(a->get("type", type));
		ASSERT_EQ("STRING", type);
	}

	c = c->getNextSiblingNode();
	ASSERT_TRUE(static_cast<bool>(c));
	ASSERT_EQ("text", c->getName());

	c = c->getNextSiblingNode();
	ASSERT_TRUE(static_cast<bool>(c));
	ASSERT_EQ("field", c->getName());
	{
		auto a = c->getAttributes();
		std::string number;
		ASSERT_TRUE(a->get("number", number));
		ASSERT_EQ("2", number);
		std::string name;
		ASSERT_TRUE(a->get("name", name));
		ASSERT_EQ("AdvId", name);
		std::string type;
		ASSERT_TRUE(a->get("type", type));
		ASSERT_EQ("STRING", type);
	}
}

using ::testing::InitGoogleTest;

int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);

	int option;
	while ((option = getopt(argc, argv, "f:")) != -1) {
		switch (option) {
		case 'f':
			FIX42 = optarg;
			break;
		}
	}

	return RUN_ALL_TESTS();
}
