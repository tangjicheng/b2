/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_FILESTOREFACTORYTESTCASE_H
#define FIX_FILESTOREFACTORYTESTCASE_H

#include <CPPTest/TestCase.h>
#include <qfix/Application.h>
#include <qfix/FileStore.h>
#include <qfix/Responder.h>
#include <qfix/Session.h>

namespace FIX {

class CallCreateTestHandler : public Handler {
public:
	void onLogon() override {}
	void onLogout() override {}
	void toAdmin(Message&) override {}
	void toApp(Message&) override {}
	void fromAdmin(const Message&) override {}
	void fromApp(const Message&) override {}
	void onRun() {}

	void on_disconnect() override {}
	void on_incoming_message() override {}
	void on_incoming_reset(std::int32_t) override {}
	void on_outgoing_message(const Message&) override {}
	void on_outgoing_reset() override {}
};

class FileStoreFactoryTestCase : public CPPTest::TestCase<FileStoreFactory> {
public:
	FileStoreFactoryTestCase() { add(&m_callCreate); }

private:
	typedef CPPTest::Test<FileStoreFactory> Test;

	class callCreate : public Test, Application, FIX::Responder {
	public:
		callCreate()
			: m_object("store")
		{
		}
		bool onSetup(FileStoreFactory*& pObject) override
		{
			pObject = &m_object;
			return true;
		}
		void onRun(FileStoreFactory& object) override;
		void onTeardown(FileStoreFactory* pObject) override;

		void onCreate(const SessionID& sessionId) override
		{
			auto session = FIX::Session::lookupSession(sessionId);
			session->set_handler(m_pHandler);
		}

		bool send(const std::string&) override { return true; }
		void schedule_disconnect() override {}
		void disconnect() override {}

	private:
		CallCreateTestHandler m_pHandler;
		FileStoreFactory m_object;

	} m_callCreate;
};

} // namespace FIX

#endif // FIX_FILESTOREFACTORYTESTCASE_H
