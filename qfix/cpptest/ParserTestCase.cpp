/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include "ParserTestCase.h"

#include <sstream>
#include <string>

namespace FIX {

bool ParserTestCase::readFixMessage::onSetup(Parser*& pObject)
{
	m_fixMsg1 = "8=FIX.4.2\0019=12\00135=A\001108=30\00110=31\001";
	m_fixMsg2 = "8=FIX.4.2\0019=17\00135=4\00136=88\001123=Y\00110=34\001";
	m_fixMsg3 = "8=FIX.4.2\0019=19\00135=A\001108=30\0019710=8\00110=31\001";

	pObject = new Parser();
	pObject->read(m_fixMsg1 + m_fixMsg2 + m_fixMsg3);
	return true;
}

void ParserTestCase::readFixMessage::onRun(Parser& object)
{
	std::string fixMsg1;

	assert(object.extract_fix_message(fixMsg1));
	assert(fixMsg1 == m_fixMsg1);

	std::string fixMsg2;
	assert(object.extract_fix_message(fixMsg2));
	assert(fixMsg2 == m_fixMsg2);

	std::string fixMsg3;
	assert(object.extract_fix_message(fixMsg3));
	assert(fixMsg3 == m_fixMsg3);
}

bool ParserTestCase::readPartialFixMessage::onSetup(Parser*& pObject)
{
	m_partFixMsg1 = "8=FIX.4.2\0019=17\00135=4\00136=";
	m_partFixMsg2 = "88\001123=Y\00110=34\001";

	pObject = new Parser();
	pObject->read(m_partFixMsg1);
	return true;
}

void ParserTestCase::readPartialFixMessage::onRun(Parser& object)
{
	std::string partFixMsg;
	assert(!object.extract_fix_message(partFixMsg));
	object.read(m_partFixMsg2);
	assert(object.extract_fix_message(partFixMsg));
	assert(partFixMsg == (m_partFixMsg1 + m_partFixMsg2));
}

bool ParserTestCase::readMessageWithBadLength::onSetup(Parser*& pObject)
{
	m_fixMsg = "8=TEST\0019=TEST\00135=TEST\00149=SS1\00156=RORE\00134=3\00152="
			   "20050222-16:45:53\00110=TEST\001";

	pObject = new Parser();
	pObject->read(m_fixMsg);
	return true;
}

void ParserTestCase::readMessageWithBadLength::onRun(Parser& object)
{
	std::string fixMsg;

	try {
		object.extract_fix_message(fixMsg);
		assert(false);
	}
	catch (MessageParseError&) {
	}

	try {
		assert(!object.extract_fix_message(fixMsg));
	}
	catch (MessageParseError&) {
		assert(false);
	}
}

} // namespace FIX
