/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#include <unistd.h>

#include <CPPTest/TestStandardOutputDisplay.h>
#include <CPPTest/TestXMLFileOutputDisplay.h>
#include <qfix/SessionSettings.h>
#include <qfix/Utility.h>

#include "TestSuite.h"

int main(int argc, char** argv)
{
	short port = 0;
	std::unique_ptr<FIX::SessionSettings> sessionSettingsPtr;

	int opt;
	while ((opt = getopt(argc, argv, "+p:+f:")) != -1) {
		switch (opt) {
		case 'p':
			port = (short)atol(optarg);
			break;
		case 'f':
			sessionSettingsPtr.reset(new FIX::SessionSettings(optarg));
			break;
		default:
			std::cout << "usage: " << argv[0] << " -p port -f file"
					  << std::endl;
			return 1;
		}
	}
	std::unique_ptr<CPPTest::TestDisplay> display(
		new CPPTest::TestXMLFileOutputDisplay());

	TestSuite suite(*display, port, *sessionSettingsPtr);
	suite.run();

	return suite.getExceptions().size() > 0;
}
