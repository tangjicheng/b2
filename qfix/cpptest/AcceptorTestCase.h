/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifndef FIX_ACCEPTORTESTCASE_H
#define FIX_ACCEPTORTESTCASE_H

#include <CPPTest/TestCase.h>
#include <qfix/Acceptor.h>
#include <qfix/Session.h>

namespace FIX {
class TestApplication : public Application {
public:
	void onCreate(const SessionID& sessionId) override
	{
		auto session = FIX::Session::lookupSession(sessionId);
		session->set_handler(handler_);
	}
	class TestHandler : public Handler {
	public:
		void onLogon() override {}
		void onLogout() override {}
		void toAdmin(Message&) override {}
		void toApp(Message&) override {}
		void fromAdmin(const Message&) override {}
		void fromApp(const Message&) override {}
		void onRun() {}

		void on_disconnect() override {}
		void on_incoming_message() override {}
		void on_incoming_reset(std::int32_t) override {}
		void on_outgoing_message(const Message&) override {}
		void on_outgoing_reset() override {}
	} handler_;
};

class TestAcceptor : public Acceptor {
public:
	TestAcceptor(Application& application, MemoryStoreFactory& factory,
		const SessionSettings& settings)
		: Acceptor(application, factory, settings)
	{
	}
	void onInitialize(const SessionSettings&) {}
	void onStart() {}
	bool onPoll() { return false; }
	void onStop() {}
};

class AcceptorTestCase : public CPPTest::TestCase<Acceptor> {
public:
	AcceptorTestCase() { add(&m_parseStream); }

private:
	typedef CPPTest::Test<Acceptor> Test;

	class parseStream : public Test {
		bool onSetup(Acceptor*& pObject);
		void onRun(Acceptor& object);
		void onTeardown(Acceptor* pObject)
		{
			delete pObject;
			delete m_pFactory;
			delete m_pApplication;
		}
		TestApplication* m_pApplication;
		MemoryStoreFactory* m_pFactory;
	} m_parseStream;
};
} // namespace FIX

#endif // FIX_ACCEPTORTESTCASE_H
