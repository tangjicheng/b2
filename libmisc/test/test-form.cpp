#include <gtest/gtest.h>

#include <misc/form.h>

TEST(TestForm, noFormat)
{
	auto output = misc::form("Let there be string");
	auto expected = "Let there be string";
	EXPECT_EQ(expected, output);
}

TEST(TestForm, formattedWithInt)
{
	auto output = misc::form("Let there be integer %d", 42);
	auto expected = "Let there be integer 42";
	EXPECT_EQ(expected, output);
}

TEST(TestForm, formattedWithCstr)
{
	auto output = misc::form("Let there be %s", "c-string");
	auto expected = "Let there be c-string";
	EXPECT_EQ(expected, output);
}

TEST(TestForm, longString)
{
	auto output
		= misc::form("The master programmer knows when to apply structure"
					 " and when to leave things in their simple form."
					 " Her programs are like clay, solid yet malleable.");
	auto expected = "The master programmer knows when to apply structure"
					" and when to leave things in their simple form."
					" Her programs are like clay, solid yet malleable.";
	EXPECT_EQ(expected, output);
}
