#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <misc/inject.h>

#include "unittest.h"

namespace {

constexpr int UNUSED_VALUE = 99;
constexpr const char* UNUSED_STRING = "";

template <typename T, size_t Length, misc::ErrorPolicy Policy>
void testInject(T input, const char* expected)
{
	char output[Length + 1];
	output[Length] = '\0';
	misc::inject<Length, T, '.', Policy>(output, input);
	EXPECT_STREQ(expected, output)
		<< "Inject returned unexpected output.\nInput is " << input;
}

template <typename T, size_t Length>
void testInjectNoThrow(T input)
{
	char output[Length];
	EXPECT_NO_THROW(
		(misc::inject<Length, T, '.', misc::ErrorPolicy::NONE>(output, input)))
		<< "Inject threw on no policy.\nInput is " << input;
}

template <typename T, size_t Length>
void testInjectFail(T input)
{
	char output[Length + 1];
	char expected[Length + 1];
	memset(output, '_', Length);
	output[Length] = '\0';
	memcpy(expected, output, Length + 1);
	misc::inject<Length, T, '.', misc::ErrorPolicy::FAIL>(output, input);
	EXPECT_STREQ(expected, output)
		<< "Inject returned unexpected output.\nInput is " << input;
}

} // namespace

TEST(TestInject, noPolicy)
{
	constexpr misc::ErrorPolicy policy = misc::ErrorPolicy::NONE;
	testInject<int, 4, policy>(0, "...0");
	testInject<int, 4, policy>(1, "...1");
	testInject<int, 4, policy>(9999, "9999");
	testInjectNoThrow<int, 4>(10000);
	testInjectNoThrow<int, 4>(-1);
	testInject<unsigned int, 4, policy>(0, "...0");
	testInject<unsigned int, 4, policy>(1, "...1");
	testInject<unsigned int, 4, policy>(9999, "9999");
	testInjectNoThrow<unsigned int, 4>(10000);
	testInject<int, 10, policy>(2147483647, "2147483647");
	testInject<unsigned int, 10, policy>(4294967295, "4294967295");
}

TEST(TestInject, policyFail)
{
	constexpr misc::ErrorPolicy policy = misc::ErrorPolicy::FAIL;
	testInject<int, 4, policy>(0, "...0");
	testInject<int, 4, policy>(1, "...1");
	testInject<int, 4, policy>(9999, "9999");
	testInjectFail<int, 4>(10000);
	testInjectFail<int, 4>(-1);
	testInject<unsigned int, 4, policy>(0, "...0");
	testInject<unsigned int, 4, policy>(1, "...1");
	testInject<unsigned int, 4, policy>(9999, "9999");
	testInjectFail<unsigned int, 4>(10000);
	testInject<int, 10, policy>(2147483647, "2147483647");
	testInject<unsigned int, 10, policy>(4294967295, "4294967295");
}

TEST(TestInject, policyCap)
{
	constexpr misc::ErrorPolicy policy = misc::ErrorPolicy::CAP;
	testInject<int, 4, policy>(0, "...0");
	testInject<int, 4, policy>(1, "...1");
	testInject<int, 4, policy>(9999, "9999");
	testInject<int, 4, policy>(10000, "9999");
	testInject<int, 4, policy>(-1, "...0");
	testInject<unsigned int, 4, policy>(0, "...0");
	testInject<unsigned int, 4, policy>(1, "...1");
	testInject<unsigned int, 4, policy>(9999, "9999");
	testInject<unsigned int, 4, policy>(10000, "9999");
	testInject<int, 10, policy>(2147483647, "2147483647");
	testInject<unsigned int, 10, policy>(4294967295, "4294967295");
}

TEST(TestInject, policyThrow)
{
	constexpr misc::ErrorPolicy policy = misc::ErrorPolicy::THROW;
	testInject<int, 4, policy>(0, "...0");
	testInject<int, 4, policy>(1, "...1");
	testInject<int, 4, policy>(9999, "9999");
	EXPECT_THROW_WHAT((testInject<int, 4, policy>(10000, UNUSED_STRING)),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((testInject<int, 4, policy>(-1, UNUSED_STRING)),
		std::invalid_argument, "Out of range");
	testInject<unsigned int, 4, policy>(0, "...0");
	testInject<unsigned int, 4, policy>(1, "...1");
	testInject<unsigned int, 4, policy>(9999, "9999");
	EXPECT_THROW_WHAT(
		(testInject<unsigned int, 4, policy>(10000, UNUSED_STRING)),
		std::invalid_argument, "Out of range");
	testInject<int, 10, policy>(2147483647, "2147483647");
	testInject<unsigned int, 10, policy>(4294967295, "4294967295");
}

namespace {

template <size_t Length, misc::ErrorPolicy Policy, typename T, char Pad = '.'>
void testExtract(const char* input, T expected)
{
	T output = expected + 1;
	misc::extract<Length, T, Pad, Policy>(input, output);
	EXPECT_EQ(expected, output)
		<< "Extract returned unexpected output.\nInput is " << input;
}

template <size_t Length, typename T>
void testExtractNoThrow(const char* input)
{
	T output;
	EXPECT_NO_THROW((
		misc::extract<Length, T, '.', misc::ErrorPolicy::NONE>(input, output)));
}

template <size_t Length, typename T>
void testExtractFail(const char* input)
{
	T output = UNUSED_VALUE;
	T expected = UNUSED_VALUE;
	misc::extract<Length, T, '.', misc::ErrorPolicy::FAIL>(input, output);
	EXPECT_EQ(expected, output)
		<< "Extract returned unexpected output.\nInput is " << input;
}

} // namespace

TEST(TestExtract, noPolicy)
{
	constexpr misc::ErrorPolicy policy = misc::ErrorPolicy::NONE;
	testExtract<4, policy, short>("-999", -999);
	testExtract<4, policy, short>("+999", 999);
	testExtract<5, policy, short>("....0", 0);
	testExtract<5, policy, short>("....1", 1);
	testExtract<5, policy, short>("...01", 1);
	testExtract<5, policy, short>("32767", 32767);
	testExtract<5, policy, short>("...-1", -1);
	testExtract<5, policy, short>("-0999", -999);
	testExtract<5, policy, short>(".+999", 999);
	testExtract<5, policy, short>("+0999", 999);
	testExtract<6, policy, short>("-32768", -32768);
	testExtract<6, policy, short>("+32767", 32767);
	testExtract<6, policy, short>("032767", 32767);
	testExtract<2, policy, unsigned short>(".1", 1);
	testExtract<2, policy, unsigned short>("00", 0);
	testExtract<2, policy, unsigned short>("99", 99);
	testExtract<5, policy, unsigned short>("....0", 0);
	testExtract<5, policy, unsigned short>("....1", 1);
	testExtract<5, policy, unsigned short>("...01", 1);
	testExtract<5, policy, unsigned short>("65535", 65535);
	testExtractNoThrow<5, short>("99999");
	EXPECT_THROW_WHAT((testExtract<5, policy, short>("xxx-1", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short>(".-999", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short>(".+999", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
}

TEST(TestExtract, policyFail)
{
	constexpr misc::ErrorPolicy policy = misc::ErrorPolicy::FAIL;
	testExtract<4, policy, short>("-999", -999);
	testExtract<4, policy, short>("+999", 999);
	testExtract<5, policy, short>("....0", 0);
	testExtract<5, policy, short>("....1", 1);
	testExtract<5, policy, short>("...01", 1);
	testExtract<5, policy, short>("32767", 32767);
	testExtract<5, policy, short>("...-1", -1);
	testExtract<5, policy, short>("-0999", -999);
	testExtract<5, policy, short>(".+999", 999);
	testExtract<5, policy, short>("+0999", 999);
	testExtract<6, policy, short>("-32768", -32768);
	testExtract<6, policy, short>("+32767", 32767);
	testExtract<6, policy, short>("032767", 32767);
	testExtract<2, policy, unsigned short>(".1", 1);
	testExtract<2, policy, unsigned short>("00", 0);
	testExtract<2, policy, unsigned short>("99", 99);
	testExtract<5, policy, unsigned short>("....0", 0);
	testExtract<5, policy, unsigned short>("....1", 1);
	testExtract<5, policy, unsigned short>("...01", 1);
	testExtract<5, policy, unsigned short>("65535", 65535);
	testExtractFail<5, short>("99999");
	testExtractFail<6, short>("-99999");
	testExtractFail<5, unsigned short>("89999");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short>(".-999", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT((testExtract<5, policy, short>("xxx11", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT((testExtract<5, policy, short>("xxx-1", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
}

TEST(TestExtract, policyCap)
{
	constexpr misc::ErrorPolicy policy = misc::ErrorPolicy::CAP;
	testExtract<4, policy, short>("-999", -999);
	testExtract<4, policy, short>("+999", 999);
	testExtract<5, policy, short>("....0", 0);
	testExtract<5, policy, short>("....1", 1);
	testExtract<5, policy, short>("...01", 1);
	testExtract<5, policy, short>("32767", 32767);
	testExtract<5, policy, short>("...-1", -1);
	testExtract<5, policy, short>("..-01", -1);
	testExtract<5, policy, short>(".-001", -1);
	testExtract<5, policy, short>("-0001", -1);
	testExtract<5, policy, short>("-0999", -999);
	testExtract<5, policy, short>(".+999", 999);
	testExtract<5, policy, short>("+0999", 999);
	testExtract<6, policy, short>("-32768", -32768);
	testExtract<6, policy, short>("+32767", 32767);
	testExtract<6, policy, short>("032767", 32767);
	testExtract<2, policy, unsigned short>(".1", 1);
	testExtract<2, policy, unsigned short>("00", 0);
	testExtract<2, policy, unsigned short>("99", 99);
	testExtract<5, policy, unsigned short>("....0", 0);
	testExtract<5, policy, unsigned short>("....1", 1);
	testExtract<5, policy, unsigned short>("...01", 1);
	testExtract<5, policy, unsigned short>("65535", 65535);
	testExtract<5, policy, short>("99099", 32767);
	testExtract<6, policy, short>("+44444", 32767);
	testExtract<6, policy, short>("-44444", -32768);
	testExtract<6, policy, short>("-32769", -32768);
	testExtract<6, policy, short>("-32768", -32768);
	testExtract<6, policy, short>("-32767", -32767);
	testExtract<5, policy, unsigned short>("70000", 65535);
	testExtract<5, policy, unsigned short>("65536", 65535);
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short>(".-999", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT((testExtract<5, policy>("xxx11", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT((testExtract<5, policy>("xxx-1", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
}

TEST(TestExtract, policyThrow)
{
	constexpr misc::ErrorPolicy policy = misc::ErrorPolicy::THROW;
	testExtract<2, policy, unsigned short>(".1", 1);
	testExtract<4, policy, short>("-999", -999);
	testExtract<4, policy, short>("+999", 999);
	testExtract<5, policy, short>("....0", 0);
	testExtract<5, policy, short>("....1", 1);
	testExtract<5, policy, short>("...01", 1);
	testExtract<5, policy, short>("32767", 32767);
	testExtract<5, policy, short>("...-1", -1);
	testExtract<5, policy, short>("..-01", -1);
	testExtract<5, policy, short>(".-001", -1);
	testExtract<5, policy, short>("-0001", -1);
	testExtract<5, policy, short>("-0999", -999);
	testExtract<5, policy, short>(".+999", 999);
	testExtract<5, policy, short>("+0999", 999);
	testExtract<6, policy, short>("-32768", -32768);
	testExtract<6, policy, short>("+32767", 32767);
	testExtract<6, policy, short>("032767", 32767);
	testExtract<2, policy, unsigned short>(".1", 1);
	testExtract<2, policy, unsigned short>("00", 0);
	testExtract<2, policy, unsigned short>("99", 99);
	testExtract<5, policy, unsigned short>("....0", 0);
	testExtract<5, policy, unsigned short>("....1", 1);
	testExtract<5, policy, unsigned short>("...01", 1);
	testExtract<5, policy, unsigned short>("65535", 65535);
	EXPECT_THROW_WHAT((testExtract<5, policy, short>("xxx-1", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT((testExtract<5, policy, short>("55555", UNUSED_VALUE)),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((testExtract<6, policy, short>("-55555", UNUSED_VALUE)),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short>(".-999", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short>(".+999", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short>("99999", UNUSED_VALUE)),
		std::invalid_argument, "Out of range");
}

TEST(TestExtract, policyThrowPad0)
{
	constexpr misc::ErrorPolicy policy = misc::ErrorPolicy::THROW;
	testExtract<4, policy, short, '0'>("-999", -999);
	testExtract<4, policy, short, '0'>("+999", 999);
	testExtract<5, policy, short, '0'>("00000", 0);
	testExtract<5, policy, short, '0'>("00001", 1);
	testExtract<5, policy, short, '0'>("32767", 32767);
	testExtract<5, policy, short, '0'>("-0001", -1);
	testExtract<5, policy, short, '0'>("-0999", -999);
	testExtract<5, policy, short, '0'>("+0999", 999);
	testExtract<5, policy, short, '0'>("+0999", 999);
	testExtract<6, policy, short, '0'>("-32768", -32768);
	testExtract<6, policy, short, '0'>("+32767", 32767);
	testExtract<6, policy, short, '0'>("032767", 32767);
	testExtract<2, policy, unsigned short, '0'>("01", 1);
	testExtract<2, policy, unsigned short, '0'>("00", 0);
	testExtract<2, policy, unsigned short, '0'>("99", 99);
	testExtract<5, policy, unsigned short, '0'>("00000", 0);
	testExtract<5, policy, unsigned short, '0'>("00001", 1);
	testExtract<5, policy, unsigned short, '0'>("65535", 65535);
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, short, '0'>("xxx-1", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, short, '0'>("55555", UNUSED_VALUE)),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(testExtract<6, policy, short, '0'>("-55555", UNUSED_VALUE)),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short, '0'>("0-999", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short, '0'>("+x999", UNUSED_VALUE)),
		std::invalid_argument, "Not a digit");
	EXPECT_THROW_WHAT(
		(testExtract<5, policy, unsigned short, '0'>("99999", UNUSED_VALUE)),
		std::invalid_argument, "Out of range");
}

TEST(TestExtract, roundTrip)
{
	constexpr auto len = std::numeric_limits<uint16_t>::digits10 + 1;
	for (uint32_t input = std::numeric_limits<uint16_t>::min();
		 input <= std::numeric_limits<uint16_t>::max(); ++input) {
		char buffer[len + 2];
		memset(buffer, '$', len + 2);
		void* p = buffer + 1;
		ASSERT_NO_THROW(
			(misc::inject<len, uint16_t, '.', misc::ErrorPolicy::THROW>(
				p, input)));
		ASSERT_EQ('$', buffer[0]);
		ASSERT_EQ('$', buffer[len + 1]);
		uint16_t output = input - 1;
		ASSERT_NO_THROW(
			(misc::extract<len, uint16_t, '.', misc::ErrorPolicy::THROW>(
				p, output)));
		ASSERT_EQ(input, output);
	}
}
