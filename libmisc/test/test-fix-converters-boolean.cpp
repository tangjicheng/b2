#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <misc/fix/Converters.h>

#include "unittest.h"

TEST(fixBooleanConvertFromString, valid)
{
	EXPECT_TRUE(misc::fix::booleanConvert(std::string(1, 'Y')));
	EXPECT_FALSE(misc::fix::booleanConvert(std::string(1, 'N')));
}

TEST(fixBooleanConvertFromString, badFormat)
{
	for (unsigned int count = 0; count != 10; ++count) {
		if (count == 1)
			continue;
		EXPECT_THROW_WHAT(misc::fix::booleanConvert(std::string(count, 'Y')),
			std::invalid_argument, "Bad format");
		EXPECT_THROW_WHAT(misc::fix::booleanConvert(std::string(count, 'N')),
			std::invalid_argument, "Bad format");
	}
}

TEST(fixBooleanConvertFromString, invalidCharacter)
{
	for (unsigned int c = 0; c != 256; ++c) {
		if (c == 'Y' || c == 'N')
			continue;
		EXPECT_THROW_WHAT(misc::fix::booleanConvert(std::string(1, c)),
			std::invalid_argument, "Invalid character");
	}
}

TEST(fixBooleanConvertToString, valid)
{
	EXPECT_EQ(std::string(1, 'Y'), misc::fix::booleanConvert(true));
	EXPECT_EQ(std::string(1, 'N'), misc::fix::booleanConvert(false));
}
