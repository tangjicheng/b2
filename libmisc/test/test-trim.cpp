#include <string>

#include <gtest/gtest.h>

#include <misc/trim.h>

class TestTrim : public ::testing::Test {
protected:
	const char input_array_trailing[8] {'h', 'e', 'l', 'l', 'o', ' ', ' ', ' '};
	const char input_array_leading[8] {' ', ' ', ' ', 'w', 'o', 'r', 'l', 'd'};
	const char input_array_blank[8] {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
	const char input_array_separated[11] {
		' ', 'h', ' ', 'e', ' ', 'l', ' ', 'l', ' ', 'o', ' '};
	const char input_array_none[5] {'w', 'o', 'r', 'l', 'd'};
	const char input_array_empty[1] {' '};

	const char* input_cstr_trailing {"hello   "};
	const char* input_cstr_leading {"   world"};
	const char* input_cstr_blank {"        "};
	const char* input_cstr_separated {" h e l l o "};
	const char* input_cstr_none {"world"};
	const char* input_cstr_empty {""};

	std::string input_string_trailing {input_cstr_trailing};
	std::string input_string_leading {input_cstr_leading};
	std::string input_string_blank {input_cstr_blank};
	std::string input_string_separated {input_cstr_separated};
	std::string input_string_none {input_cstr_none};
	std::string input_string_empty {input_cstr_empty};

	std::string expected_left_trailing {"hello   "};
	std::string expected_left_leading {"world"};
	std::string expected_left_blank {""};
	std::string expected_left_separated {"h e l l o "};
	std::string expected_left_none {"world"};
	std::string expected_left_empty {""};

	std::string expected_right_trailing {"hello"};
	std::string expected_right_leading {"   world"};
	std::string expected_right_blank {""};
	std::string expected_right_separated {" h e l l o"};
	std::string expected_right_none {"world"};
	std::string expected_right_empty {""};

	std::string expected_trailing {"hello"};
	std::string expected_leading {"world"};
	std::string expected_blank {""};
	std::string expected_separated {"h e l l o"};
	std::string expected_none {"world"};
	std::string expected_empty {""};
};

TEST_F(TestTrim, ltrim_array)
{
	EXPECT_EQ(expected_left_trailing,
		misc::ltrim(input_array_trailing, sizeof(input_array_trailing)));
	EXPECT_EQ(expected_left_leading,
		misc::ltrim(input_array_leading, sizeof(input_array_leading)));
	EXPECT_EQ(expected_left_blank,
		misc::ltrim(input_array_blank, sizeof(input_array_blank)));
	EXPECT_EQ(expected_left_separated,
		misc::ltrim(input_array_separated, sizeof(input_array_separated)));
	EXPECT_EQ(expected_left_none,
		misc::ltrim(input_array_none, sizeof(input_array_none)));
	EXPECT_EQ(expected_left_empty, misc::ltrim(input_array_empty, 0));
}

TEST_F(TestTrim, ltrim_cstr)
{
	EXPECT_EQ(expected_left_trailing, misc::ltrim(input_cstr_trailing));
	EXPECT_EQ(expected_left_leading, misc::ltrim(input_cstr_leading));
	EXPECT_EQ(expected_left_blank, misc::ltrim(input_cstr_blank));
	EXPECT_EQ(expected_left_separated, misc::ltrim(input_cstr_separated));
	EXPECT_EQ(expected_left_none, misc::ltrim(input_cstr_none));
	EXPECT_EQ(expected_left_empty, misc::ltrim(input_cstr_empty));
}

TEST_F(TestTrim, ltrim_string)
{
	EXPECT_EQ(expected_left_trailing, misc::ltrim(input_string_trailing));
	EXPECT_EQ(expected_left_leading, misc::ltrim(input_string_leading));
	EXPECT_EQ(expected_left_blank, misc::ltrim(input_string_blank));
	EXPECT_EQ(expected_left_separated, misc::ltrim(input_string_separated));
	EXPECT_EQ(expected_left_none, misc::ltrim(input_string_none));
	EXPECT_EQ(expected_left_empty, misc::ltrim(input_string_empty));
}

TEST_F(TestTrim, rtrim_array)
{
	EXPECT_EQ(expected_right_trailing,
		misc::rtrim(input_array_trailing, sizeof(input_array_trailing)));
	EXPECT_EQ(expected_right_leading,
		misc::rtrim(input_array_leading, sizeof(input_array_leading)));
	EXPECT_EQ(expected_right_blank,
		misc::rtrim(input_array_blank, sizeof(input_array_blank)));
	EXPECT_EQ(expected_right_separated,
		misc::rtrim(input_array_separated, sizeof(input_array_separated)));
	EXPECT_EQ(expected_right_none,
		misc::rtrim(input_array_none, sizeof(input_array_none)));
	EXPECT_EQ(expected_right_empty, misc::rtrim(input_array_empty, 0));
}

TEST_F(TestTrim, rtrim_cstr)
{
	EXPECT_EQ(expected_right_trailing, misc::rtrim(input_cstr_trailing));
	EXPECT_EQ(expected_right_leading, misc::rtrim(input_cstr_leading));
	EXPECT_EQ(expected_right_blank, misc::rtrim(input_cstr_blank));
	EXPECT_EQ(expected_right_separated, misc::rtrim(input_cstr_separated));
	EXPECT_EQ(expected_right_none, misc::rtrim(input_cstr_none));
	EXPECT_EQ(expected_right_empty, misc::rtrim(input_cstr_empty));
}

TEST_F(TestTrim, rtrim_string)
{
	EXPECT_EQ(expected_right_trailing, misc::rtrim(input_string_trailing));
	EXPECT_EQ(expected_right_leading, misc::rtrim(input_string_leading));
	EXPECT_EQ(expected_right_blank, misc::rtrim(input_string_blank));
	EXPECT_EQ(expected_right_separated, misc::rtrim(input_string_separated));
	EXPECT_EQ(expected_right_none, misc::rtrim(input_string_none));
	EXPECT_EQ(expected_right_empty, misc::rtrim(input_string_empty));
}

TEST_F(TestTrim, trim_array)
{
	EXPECT_EQ(expected_trailing,
		misc::trim(input_array_trailing, sizeof(input_array_trailing)));
	EXPECT_EQ(expected_leading,
		misc::trim(input_array_leading, sizeof(input_array_leading)));
	EXPECT_EQ(expected_blank,
		misc::trim(input_array_blank, sizeof(input_array_blank)));
	EXPECT_EQ(expected_separated,
		misc::trim(input_array_separated, sizeof(input_array_separated)));
	EXPECT_EQ(
		expected_none, misc::trim(input_array_none, sizeof(input_array_none)));
	EXPECT_EQ(expected_empty, misc::trim(input_array_empty, 0));
}

TEST_F(TestTrim, trim_cstr)
{
	EXPECT_EQ(expected_trailing, misc::trim(input_cstr_trailing));
	EXPECT_EQ(expected_leading, misc::trim(input_cstr_leading));
	EXPECT_EQ(expected_blank, misc::trim(input_cstr_blank));
	EXPECT_EQ(expected_separated, misc::trim(input_cstr_separated));
	EXPECT_EQ(expected_none, misc::trim(input_cstr_none));
	EXPECT_EQ(expected_empty, misc::trim(input_cstr_empty));
}

TEST_F(TestTrim, trim_string)
{
	EXPECT_EQ(expected_trailing, misc::trim(input_string_trailing));
	EXPECT_EQ(expected_leading, misc::trim(input_string_leading));
	EXPECT_EQ(expected_blank, misc::trim(input_string_blank));
	EXPECT_EQ(expected_separated, misc::trim(input_string_separated));
	EXPECT_EQ(expected_none, misc::trim(input_string_none));
	EXPECT_EQ(expected_empty, misc::trim(input_string_empty));
}
