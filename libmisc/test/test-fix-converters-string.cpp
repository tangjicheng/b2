#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <misc/fix/Converters.h>

#include "unittest.h"

TEST(fixStringConvert, valid)
{
	for (unsigned int c = 32; c != 128; ++c) {
		if (c == ' ' || c == 127)
			continue;
		for (unsigned int count = 1; count != 10; ++count)
			EXPECT_EQ(std::string(count, c),
				misc::fix::stringConvert(std::string(count, c)));
	}
}

TEST(fixStringConvert, badFormat)
{
	EXPECT_THROW_WHAT(misc::fix::stringConvert(std::string()),
		std::invalid_argument, "Bad format");
}

TEST(fixStringConvert, invalidCharacter)
{
	for (unsigned int c = 0; c != 32; ++c) {
		EXPECT_THROW_WHAT(misc::fix::stringConvert(std::string({'A', 'B', 'C',
							  static_cast<char>(c), 'a', 'b', 'c'})),
			std::invalid_argument, "Invalid character");
	}
	EXPECT_THROW_WHAT(misc::fix::stringConvert(
						  std::string({'A', 'B', 'C', ' ', 'a', 'b', 'c'})),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::stringConvert(std::string({'A', 'B', 'C',
						  static_cast<char>(127), 'a', 'b', 'c'})),
		std::invalid_argument, "Invalid character");
	for (unsigned int c = 128; c != 256; ++c) {
		EXPECT_THROW_WHAT(misc::fix::stringConvert(std::string({'A', 'B', 'C',
							  static_cast<char>(c), 'a', 'b', 'c'})),
			std::invalid_argument, "Invalid character");
	}
}
