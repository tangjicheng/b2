#include <gtest/gtest.h>

#include <misc/fix/UTCTimeDate.h>

#include "unittest.h"

TEST(fixUTCDateOnly, defaultConstructor)
{
	{
		misc::fix::UTCDateOnly date;
		EXPECT_EQ(19700101U, date.yyyymmdd());
		EXPECT_EQ(1970U, date.year());
		EXPECT_EQ(1U, date.month());
		EXPECT_EQ(1U, date.day());
	}
}

TEST(fixUTCDateOnly, oneArgumentConstructor)
{
	{
		misc::fix::UTCDateOnly date(101);
		EXPECT_EQ(101U, date.yyyymmdd());
		EXPECT_EQ(0U, date.year());
		EXPECT_EQ(1U, date.month());
		EXPECT_EQ(1U, date.day());
	}
	{
		misc::fix::UTCDateOnly date(10101);
		EXPECT_EQ(10101U, date.yyyymmdd());
		EXPECT_EQ(1U, date.year());
		EXPECT_EQ(1U, date.month());
		EXPECT_EQ(1U, date.day());
	}
	{
		misc::fix::UTCDateOnly date(20170530);
		EXPECT_EQ(20170530U, date.yyyymmdd());
		EXPECT_EQ(2017U, date.year());
		EXPECT_EQ(5U, date.month());
		EXPECT_EQ(30U, date.day());
	}
	{
		misc::fix::UTCDateOnly date(99991231);
		EXPECT_EQ(99991231U, date.yyyymmdd());
		EXPECT_EQ(9999U, date.year());
		EXPECT_EQ(12U, date.month());
		EXPECT_EQ(31U, date.day());
	}
}

TEST(fixUTCDateOnly, threeArgumentConstructor)
{
	{
		misc::fix::UTCDateOnly date(0, 1, 1);
		EXPECT_EQ(101U, date.yyyymmdd());
		EXPECT_EQ(0U, date.year());
		EXPECT_EQ(1U, date.month());
		EXPECT_EQ(1U, date.day());
	}
	{
		misc::fix::UTCDateOnly date(1, 1, 1);
		EXPECT_EQ(10101U, date.yyyymmdd());
		EXPECT_EQ(1U, date.year());
		EXPECT_EQ(1U, date.month());
		EXPECT_EQ(1U, date.day());
	}
	{
		misc::fix::UTCDateOnly date(2017, 5, 30);
		EXPECT_EQ(20170530U, date.yyyymmdd());
		EXPECT_EQ(2017U, date.year());
		EXPECT_EQ(5U, date.month());
		EXPECT_EQ(30U, date.day());
	}
	{
		misc::fix::UTCDateOnly date(9999, 12, 31);
		EXPECT_EQ(99991231U, date.yyyymmdd());
		EXPECT_EQ(9999U, date.year());
		EXPECT_EQ(12U, date.month());
		EXPECT_EQ(31U, date.day());
	}
}

TEST(fixUTCTimeOnly, defaultConstructor)
{
	{
		misc::fix::UTCTimeOnly time;
		EXPECT_EQ(0U, time.hhmmssSSS());
		EXPECT_EQ(0U, time.hhmmss());
		EXPECT_EQ(0U, time.hour());
		EXPECT_EQ(0U, time.minute());
		EXPECT_EQ(0U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_EQ(true, time.hasMilliseconds());
	}
}

TEST(fixUTCTimeOnly, oneArgumentConstructor)
{
	{
		misc::fix::UTCTimeOnly time(0);
		EXPECT_EQ(0U, time.hhmmssSSS());
		EXPECT_EQ(0U, time.hhmmss());
		EXPECT_EQ(0U, time.hour());
		EXPECT_EQ(0U, time.minute());
		EXPECT_EQ(0U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_FALSE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(10101);
		EXPECT_EQ(10101000U, time.hhmmssSSS());
		EXPECT_EQ(10101U, time.hhmmss());
		EXPECT_EQ(1U, time.hour());
		EXPECT_EQ(1U, time.minute());
		EXPECT_EQ(1U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_FALSE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(190342);
		EXPECT_EQ(190342000U, time.hhmmssSSS());
		EXPECT_EQ(190342U, time.hhmmss());
		EXPECT_EQ(19U, time.hour());
		EXPECT_EQ(3U, time.minute());
		EXPECT_EQ(42U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_FALSE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(235960);
		EXPECT_EQ(235960000U, time.hhmmssSSS());
		EXPECT_EQ(235960U, time.hhmmss());
		EXPECT_EQ(23U, time.hour());
		EXPECT_EQ(59U, time.minute());
		EXPECT_EQ(60U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_FALSE(time.hasMilliseconds());
	}
}

TEST(fixUTCTimeOnly, twoArgumentConstructor)
{
	{
		misc::fix::UTCTimeOnly time(0, 0);
		EXPECT_EQ(0U, time.hhmmssSSS());
		EXPECT_EQ(0U, time.hhmmss());
		EXPECT_EQ(0U, time.hour());
		EXPECT_EQ(0U, time.minute());
		EXPECT_EQ(0U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_TRUE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(10101, 1);
		EXPECT_EQ(10101001U, time.hhmmssSSS());
		EXPECT_EQ(10101U, time.hhmmss());
		EXPECT_EQ(1U, time.hour());
		EXPECT_EQ(1U, time.minute());
		EXPECT_EQ(1U, time.second());
		EXPECT_EQ(1U, time.msec());
		EXPECT_TRUE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(190342, 59);
		EXPECT_EQ(190342059U, time.hhmmssSSS());
		EXPECT_EQ(190342U, time.hhmmss());
		EXPECT_EQ(19U, time.hour());
		EXPECT_EQ(3U, time.minute());
		EXPECT_EQ(42U, time.second());
		EXPECT_EQ(59U, time.msec());
		EXPECT_TRUE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(235960, 999);
		EXPECT_EQ(235960999U, time.hhmmssSSS());
		EXPECT_EQ(235960U, time.hhmmss());
		EXPECT_EQ(23U, time.hour());
		EXPECT_EQ(59U, time.minute());
		EXPECT_EQ(60U, time.second());
		EXPECT_EQ(999U, time.msec());
		EXPECT_TRUE(time.hasMilliseconds());
	}
}

TEST(fixUTCTimeOnly, threeArgumentConstructor)
{
	{
		misc::fix::UTCTimeOnly time(0, 0, 0);
		EXPECT_EQ(0U, time.hhmmssSSS());
		EXPECT_EQ(0U, time.hhmmss());
		EXPECT_EQ(0U, time.hour());
		EXPECT_EQ(0U, time.minute());
		EXPECT_EQ(0U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_FALSE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(1, 1, 1);
		EXPECT_EQ(10101000U, time.hhmmssSSS());
		EXPECT_EQ(10101U, time.hhmmss());
		EXPECT_EQ(1U, time.hour());
		EXPECT_EQ(1U, time.minute());
		EXPECT_EQ(1U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_FALSE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(19, 3, 42);
		EXPECT_EQ(190342000U, time.hhmmssSSS());
		EXPECT_EQ(190342U, time.hhmmss());
		EXPECT_EQ(19U, time.hour());
		EXPECT_EQ(3U, time.minute());
		EXPECT_EQ(42U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_FALSE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(23, 59, 60);
		EXPECT_EQ(235960000U, time.hhmmssSSS());
		EXPECT_EQ(235960U, time.hhmmss());
		EXPECT_EQ(23U, time.hour());
		EXPECT_EQ(59U, time.minute());
		EXPECT_EQ(60U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_FALSE(time.hasMilliseconds());
	}
}

TEST(fixUTCTimeOnly, fourArgumentConstructor)
{
	{
		misc::fix::UTCTimeOnly time(0, 0, 0, 0);
		EXPECT_EQ(0U, time.hhmmssSSS());
		EXPECT_EQ(0U, time.hhmmss());
		EXPECT_EQ(0U, time.hour());
		EXPECT_EQ(0U, time.minute());
		EXPECT_EQ(0U, time.second());
		EXPECT_EQ(0U, time.msec());
		EXPECT_TRUE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(1, 1, 1, 1);
		EXPECT_EQ(10101001U, time.hhmmssSSS());
		EXPECT_EQ(10101U, time.hhmmss());
		EXPECT_EQ(1U, time.hour());
		EXPECT_EQ(1U, time.minute());
		EXPECT_EQ(1U, time.second());
		EXPECT_EQ(1U, time.msec());
		EXPECT_TRUE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(19, 3, 42, 59);
		EXPECT_EQ(190342059U, time.hhmmssSSS());
		EXPECT_EQ(190342U, time.hhmmss());
		EXPECT_EQ(19U, time.hour());
		EXPECT_EQ(3U, time.minute());
		EXPECT_EQ(42U, time.second());
		EXPECT_EQ(59U, time.msec());
		EXPECT_TRUE(time.hasMilliseconds());
	}
	{
		misc::fix::UTCTimeOnly time(23, 59, 60, 999);
		EXPECT_EQ(235960999U, time.hhmmssSSS());
		EXPECT_EQ(235960U, time.hhmmss());
		EXPECT_EQ(23U, time.hour());
		EXPECT_EQ(59U, time.minute());
		EXPECT_EQ(60U, time.second());
		EXPECT_EQ(999U, time.msec());
		EXPECT_TRUE(time.hasMilliseconds());
	}
}

TEST(fixUTCTimestamp, defaultConstructor)
{
	{
		misc::fix::UTCTimestamp timestamp;

		EXPECT_EQ(19700101U, timestamp.yyyymmdd());
		EXPECT_EQ(1970U, timestamp.year());
		EXPECT_EQ(1U, timestamp.month());
		EXPECT_EQ(1U, timestamp.day());

		EXPECT_EQ(0U, timestamp.hhmmssSSS());
		EXPECT_EQ(0U, timestamp.hhmmss());
		EXPECT_EQ(0U, timestamp.hour());
		EXPECT_EQ(0U, timestamp.minute());
		EXPECT_EQ(0U, timestamp.second());
		EXPECT_EQ(0U, timestamp.msec());
		EXPECT_TRUE(timestamp.hasMilliseconds());
	}
}

TEST(fixUTCTimestamp, twoArgumentConstructor)
{
	{
		misc::fix::UTCTimestamp timestamp(misc::fix::UTCDateOnly(2017, 5, 30),
			misc::fix::UTCTimeOnly(19, 3, 42, 59));

		EXPECT_EQ(20170530U, timestamp.yyyymmdd());
		EXPECT_EQ(2017U, timestamp.year());
		EXPECT_EQ(5U, timestamp.month());
		EXPECT_EQ(30U, timestamp.day());

		EXPECT_EQ(190342059U, timestamp.hhmmssSSS());
		EXPECT_EQ(190342U, timestamp.hhmmss());
		EXPECT_EQ(19U, timestamp.hour());
		EXPECT_EQ(3U, timestamp.minute());
		EXPECT_EQ(42U, timestamp.second());
		EXPECT_EQ(59U, timestamp.msec());
		EXPECT_TRUE(timestamp.hasMilliseconds());
	}
	{
		misc::fix::UTCTimestamp timestamp(20170530, 190342);

		EXPECT_EQ(20170530U, timestamp.yyyymmdd());
		EXPECT_EQ(2017U, timestamp.year());
		EXPECT_EQ(5U, timestamp.month());
		EXPECT_EQ(30U, timestamp.day());

		EXPECT_EQ(190342000U, timestamp.hhmmssSSS());
		EXPECT_EQ(190342U, timestamp.hhmmss());
		EXPECT_EQ(19U, timestamp.hour());
		EXPECT_EQ(3U, timestamp.minute());
		EXPECT_EQ(42U, timestamp.second());
		EXPECT_EQ(0U, timestamp.msec());
		EXPECT_FALSE(timestamp.hasMilliseconds());
	}
}

TEST(fixUTCTimestamp, threeArgumentConstructor)
{
	{
		misc::fix::UTCTimestamp timestamp(20170530, 190342, 59);

		EXPECT_EQ(20170530U, timestamp.yyyymmdd());
		EXPECT_EQ(2017U, timestamp.year());
		EXPECT_EQ(5U, timestamp.month());
		EXPECT_EQ(30U, timestamp.day());

		EXPECT_EQ(190342059U, timestamp.hhmmssSSS());
		EXPECT_EQ(190342U, timestamp.hhmmss());
		EXPECT_EQ(19U, timestamp.hour());
		EXPECT_EQ(3U, timestamp.minute());
		EXPECT_EQ(42U, timestamp.second());
		EXPECT_EQ(59U, timestamp.msec());
		EXPECT_TRUE(timestamp.hasMilliseconds());
	}
}

TEST(fixUTCTimestamp, sixArgumentConstructor)
{
	{
		misc::fix::UTCTimestamp timestamp(2017, 5, 30, 19, 3, 42);

		EXPECT_EQ(20170530U, timestamp.yyyymmdd());
		EXPECT_EQ(2017U, timestamp.year());
		EXPECT_EQ(5U, timestamp.month());
		EXPECT_EQ(30U, timestamp.day());

		EXPECT_EQ(190342000U, timestamp.hhmmssSSS());
		EXPECT_EQ(190342U, timestamp.hhmmss());
		EXPECT_EQ(19U, timestamp.hour());
		EXPECT_EQ(3U, timestamp.minute());
		EXPECT_EQ(42U, timestamp.second());
		EXPECT_EQ(0U, timestamp.msec());
		EXPECT_FALSE(timestamp.hasMilliseconds());
	}
}

TEST(fixUTCTimestamp, sevenArgumentConstructor)
{
	{
		misc::fix::UTCTimestamp timestamp(2017, 5, 30, 19, 3, 42, 59);

		EXPECT_EQ(20170530U, timestamp.yyyymmdd());
		EXPECT_EQ(2017U, timestamp.year());
		EXPECT_EQ(5U, timestamp.month());
		EXPECT_EQ(30U, timestamp.day());

		EXPECT_EQ(190342059U, timestamp.hhmmssSSS());
		EXPECT_EQ(190342U, timestamp.hhmmss());
		EXPECT_EQ(19U, timestamp.hour());
		EXPECT_EQ(3U, timestamp.minute());
		EXPECT_EQ(42U, timestamp.second());
		EXPECT_EQ(59U, timestamp.msec());
		EXPECT_TRUE(timestamp.hasMilliseconds());
	}
}

TEST(fixUTCTimestamp, toTimeT)
{
	{
		misc::fix::UTCTimestamp timestamp(2017, 5, 30, 19, 3, 42);
		EXPECT_EQ(1496171022, timestamp.toTimeT());
	}
	{
		misc::fix::UTCTimestamp timestamp(2017, 5, 30, 19, 3, 42, 1);
		EXPECT_EQ(1496171022, timestamp.toTimeT());
	}
	{
		misc::fix::UTCTimestamp timestamp(2017, 5, 30, 19, 3, 42, 999);
		EXPECT_EQ(1496171022, timestamp.toTimeT());
	}
}

TEST(fixUTCTimestamp, fromTimeT)
{
	{
		auto timestamp = misc::fix::UTCTimestamp::fromTimeT(1496171022);
		EXPECT_EQ(misc::fix::UTCTimestamp(2017, 5, 30, 19, 3, 42), timestamp);
		EXPECT_FALSE(timestamp.hasMilliseconds());
	}
	{
		auto timestamp = misc::fix::UTCTimestamp::fromTimeT(1496171022, 59);
		EXPECT_EQ(
			misc::fix::UTCTimestamp(2017, 5, 30, 19, 3, 42, 59), timestamp);
		EXPECT_TRUE(timestamp.hasMilliseconds());
	}
}
