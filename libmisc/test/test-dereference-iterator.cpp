#include <memory>
#include <string>
#include <type_traits>
#include <vector>

#include <gtest/gtest.h>

#include <misc/dereference_iterator.h>

#include "unittest.h"

class TestDereferenceIterator : public ::testing::Test {
protected:
	std::vector<std::string> expected {"one", "two", "three", "four", "five"};
	std::vector<std::string*> data {
		&expected[0], &expected[1], &expected[2], &expected[3], &expected[4]};

	using iterator = misc::dereference_iterator<decltype(data)::iterator>;
	iterator iterator_to_front() { return iterator(data.begin()); }
	iterator iterator_to_back() { return iterator(--data.end()); }

	using const_iterator
		= misc::dereference_iterator<decltype(data)::const_iterator>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(data.cbegin());
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(--data.cend());
	}

	iterator iter {iterator_to_front()};
};

TEST_F(TestDereferenceIterator, copy)
{
	iterator iter1(iter);
	EXPECT_EQ(iter.base(), iter1.base());
}

TEST_F(TestDereferenceIterator, assignment)
{
	iterator iter1;
	iter1 = iter;
	EXPECT_EQ(iter.base(), iter1.base());
}

TEST_F(TestDereferenceIterator, conversion)
{
	const_iterator citer1(iter);
	EXPECT_EQ(iter.base(), citer1.base());

	const_iterator citer2;
	citer2 = iter;
	EXPECT_EQ(iter.base(), citer2.base());
}

TEST_F(TestDereferenceIterator, dereference)
{
	EXPECT_EQ(expected[0], *iter);
}

TEST_F(TestDereferenceIterator, arrow)
{
	EXPECT_EQ(expected[0], iter->substr());
}

TEST_F(TestDereferenceIterator, index)
{
	EXPECT_EQ(expected[0], iter[0]);
	EXPECT_EQ(expected[4], iter[4]);
	EXPECT_EQ(expected[0], iterator_to_back()[-4]);
}

TEST_F(TestDereferenceIterator, increment)
{
	EXPECT_EQ(expected[0], *iter);
	EXPECT_EQ(expected[1], *++iter);
	EXPECT_EQ(expected[1], *iter);
	EXPECT_EQ(expected[1], *iter++);
	EXPECT_EQ(expected[2], *iter);
}

TEST_F(TestDereferenceIterator, decrement)
{
	iter = iterator_to_back();
	EXPECT_EQ(expected[4], *iter);
	EXPECT_EQ(expected[3], *--iter);
	EXPECT_EQ(expected[3], *iter);
	EXPECT_EQ(expected[3], *iter--);
	EXPECT_EQ(expected[2], *iter);
}

TEST_F(TestDereferenceIterator, addition)
{
	EXPECT_EQ(expected[0], *iter);
	EXPECT_EQ(expected[2], *(iter += 2));
	EXPECT_EQ(expected[2], *iter);
	EXPECT_EQ(expected[4], *(iter + 2));
	EXPECT_EQ(expected[2], *iter);
	EXPECT_EQ(expected[4], *(2 + iter));
	EXPECT_EQ(expected[2], *iter);
}

namespace {

template <typename IteratorFront, typename IteratorBack>
void testSubtraction(const IteratorFront& front, const IteratorBack& back)
{
	EXPECT_EQ(0, front - front);
	EXPECT_EQ(4, back - front);
	EXPECT_EQ(-4, front - back);
}

} // namespace

TEST_F(TestDereferenceIterator, subtraction)
{
	iter = iterator_to_back();
	EXPECT_EQ(expected[4], *iter);
	EXPECT_EQ(expected[2], *(iter -= 2));
	EXPECT_EQ(expected[2], *iter);
	EXPECT_EQ(expected[0], *(iter - 2));
	EXPECT_EQ(expected[2], *iter);

	testSubtraction(iterator_to_front(), iterator_to_back());
	testSubtraction(iterator_to_front(), const_iterator_to_back());
	testSubtraction(const_iterator_to_front(), iterator_to_back());
	testSubtraction(const_iterator_to_front(), const_iterator_to_back());
}

TEST_F(TestDereferenceIterator, relational)
{
	testRelational(iterator_to_front(), iterator_to_back());
	testRelational(iterator_to_front(), const_iterator_to_back());
	testRelational(const_iterator_to_front(), iterator_to_back());
	testRelational(const_iterator_to_front(), const_iterator_to_back());
}

TEST_F(TestDereferenceIterator, make)
{
	auto iter = misc::make_dereference_iterator(data.begin());
	EXPECT_EQ(data.begin(), iter.base());
	EXPECT_TRUE((std::is_same_v<iterator, decltype(iter)>));

	auto citer = misc::make_dereference_iterator(data.cbegin());
	EXPECT_EQ(data.cbegin(), citer.base());
	EXPECT_TRUE((std::is_same_v<const_iterator, decltype(citer)>));

	auto range = misc::deref(data.begin(), data.end());
	EXPECT_EQ(data.begin(), range.begin().base());
	EXPECT_EQ(data.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	auto crange = misc::deref(data.cbegin(), data.cend());
	EXPECT_EQ(data.cbegin(), crange.begin().base());
	EXPECT_EQ(data.cend(), crange.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestDereferenceIterator, range)
{
	auto range = misc::deref(data);
	EXPECT_EQ(data.begin(), range.begin().base());
	EXPECT_EQ(data.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(data)& cdata = data;
	auto crange = misc::deref(cdata);
	EXPECT_EQ(cdata.begin(), crange.begin().base());
	EXPECT_EQ(cdata.end(), crange.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST(TestDereferenceIteratorPointer, type)
{
	std::vector<std::string*> data;

	auto iter = misc::make_dereference_iterator(data.begin());
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::iterator_category,
		decltype(iter)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::difference_type,
		decltype(iter)::difference_type>));
	EXPECT_TRUE((std::is_same_v<std::string, decltype(iter)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(iter)::pointer>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(iter)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(*iter)>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(iter.operator->())>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(iter[0])>));

	auto citer = misc::make_dereference_iterator(data.cbegin());
	EXPECT_TRUE(
		(std::is_same_v<decltype(data)::const_iterator::iterator_category,
			decltype(citer)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::const_iterator::difference_type,
		decltype(citer)::difference_type>));
	EXPECT_TRUE((std::is_same_v<std::string, decltype(citer)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(citer)::pointer>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(citer)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(*citer)>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(citer.operator->())>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(citer[0])>));
}

TEST(TestDereferenceIteratorPointerToConst, type)
{
	std::vector<const std::string*> data;

	auto iter = misc::make_dereference_iterator(data.begin());
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::iterator_category,
		decltype(iter)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::difference_type,
		decltype(iter)::difference_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string, decltype(iter)::value_type>));
	EXPECT_TRUE((std::is_same_v<const std::string*, decltype(iter)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(iter)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*iter)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(iter.operator->())>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(iter[0])>));

	auto citer = misc::make_dereference_iterator(data.cbegin());
	EXPECT_TRUE(
		(std::is_same_v<decltype(data)::const_iterator::iterator_category,
			decltype(citer)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::const_iterator::difference_type,
		decltype(citer)::difference_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string, decltype(citer)::value_type>));
	EXPECT_TRUE((std::is_same_v<const std::string*, decltype(citer)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(citer)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*citer)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(citer.operator->())>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(citer[0])>));
}

TEST(TestDereferenceIteratorUniquePtr, type)
{
	std::vector<std::unique_ptr<std::string>> data;

	auto iter = misc::make_dereference_iterator(data.begin());
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::iterator_category,
		decltype(iter)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::difference_type,
		decltype(iter)::difference_type>));
	EXPECT_TRUE((std::is_same_v<std::string, decltype(iter)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(iter)::pointer>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(iter)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(*iter)>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(iter.operator->())>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(iter[0])>));

	auto citer = misc::make_dereference_iterator(data.cbegin());
	EXPECT_TRUE(
		(std::is_same_v<decltype(data)::const_iterator::iterator_category,
			decltype(citer)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::const_iterator::difference_type,
		decltype(citer)::difference_type>));
	EXPECT_TRUE((std::is_same_v<std::string, decltype(citer)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(citer)::pointer>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(citer)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(*citer)>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(citer.operator->())>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(citer[0])>));
}

TEST(TestDereferenceIteratorUniquePtrToConst, type)
{
	std::vector<std::unique_ptr<const std::string>> data;

	auto iter = misc::make_dereference_iterator(data.begin());
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::iterator_category,
		decltype(iter)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::difference_type,
		decltype(iter)::difference_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string, decltype(iter)::value_type>));
	EXPECT_TRUE((std::is_same_v<const std::string*, decltype(iter)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(iter)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*iter)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(iter.operator->())>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(iter[0])>));

	auto citer = misc::make_dereference_iterator(data.cbegin());
	EXPECT_TRUE(
		(std::is_same_v<decltype(data)::const_iterator::iterator_category,
			decltype(citer)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::const_iterator::difference_type,
		decltype(citer)::difference_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string, decltype(citer)::value_type>));
	EXPECT_TRUE((std::is_same_v<const std::string*, decltype(citer)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(citer)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*citer)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(citer.operator->())>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(citer[0])>));
}
