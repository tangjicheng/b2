#include <chrono>
#include <cstdlib>
#include <iostream>
#include <ostream>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

#include <misc/ThreadManager.h>

namespace {

void verifySyntaxError(const std::string& spec,
	const std::set<std::string>& names = std::set<std::string>())
{
	auto& tm = misc::ThreadManager::instance();
	try {
		std::cout << "\"" << spec << "\" ";
		tm.defineAffinities(spec, names);
		throw std::logic_error("statement succeeded");
	}
	catch (const misc::ThreadAffinitySyntaxError& e) {
		std::cout << "passed (" << e.what() << ')' << std::endl;
	}
	catch (...) {
		std::cout << "failed" << std::endl;
	}
}

void verifyThreadNameError(const std::string& spec,
	const std::set<std::string>& names = std::set<std::string>())
{
	auto& tm = misc::ThreadManager::instance();
	try {
		std::cout << "\"" << spec << "\" ";
		tm.defineAffinities(spec, names);
		throw std::logic_error("statement succeeded");
	}
	catch (const misc::ThreadNameError& e) {
		std::cout << "passed (" << e.what() << ')' << std::endl;
	}
	catch (...) {
		std::cout << "failed" << std::endl;
	}
}

void func(const char* name, unsigned int delay)
{
	std::this_thread::sleep_for(std::chrono::milliseconds(
		delay * 20)); // try to get print outs happening in order
	std::ostringstream os;

	os << name << ": ";
	auto mask = misc::getThreadAffinity();
	for (int i = 0; i < CPU_SETSIZE; ++i) {
		if (mask.isSet(i))
			os << i << ' ';
	}
	os << '\n';

	std::cout << os.str()
			  << std::flush; // one-shot print, threads won't stomp each other
}

} // namespace

int main()
{
	auto& tm = misc::ThreadManager::instance();
	std::vector<std::thread> threads;

	// PITA 1: round-robin affinity in order of thread creation:
	tm.defineAffinity("foo", misc::CPUAffinityMask(1));
	tm.defineAffinity("foo", misc::CPUAffinityMask(2));
	tm.defineAffinity("foo", misc::CPUAffinityMask(3));

	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("foo");
		func("foo1", threads.size());
	}));
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("foo");
		func("foo2", threads.size());
	}));
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("foo");
		func("foo3", threads.size());
	}));

	// PITA 2: single-thread multi-cpu affinity mask:
	misc::CPUAffinityMask mask;
	mask.set(1);
	mask.set(2);
	mask.set(3);
	tm.defineAffinity("bar", mask);

	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("bar");
		func("bar1", threads.size());
	}));
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("bar");
		func("bar2", threads.size());
	}));
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("bar");
		func("bar3", threads.size());
	}));

	// Recommended way:
	// Command-line mixed affinity spec: (same as: "beef=0xd/beef=0x7/beef=0xd")
	tm.defineAffinities(
		"beef=0,0xc beef=0-2 beef=0-3:2,3", {std::string("beef")});

	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("beef");
		func("beef1", threads.size());
	}));
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("beef");
		func("beef2", threads.size());
	}));
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("beef");
		func("beef3", threads.size());
	}));
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("beef");
		func("beef4", threads.size());
	}));
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("beef");
		func("beef5", threads.size());
	}));
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("beef");
		func("beef6", threads.size());
	}));

	// Undefined affinity, effectively runs anywhere (ie default):
	threads.push_back(std::thread([&] {
		tm.setThreadAffinity("other");
		func("other", threads.size());
	}));

	for (auto t = threads.begin(); t != threads.end(); ++t)
		t->join();

	// Verify some parsing behaviour:
	verifySyntaxError("");
	verifySyntaxError("nocpu=");
	verifySyntaxError("pork=fish");
	verifySyntaxError("pork=-1");
	verifySyntaxError("pork=64");
	verifySyntaxError("pork=3-1");
	verifySyntaxError("pork=1-3:0");
	verifySyntaxError("pork=1abc");
	verifySyntaxError("pork=0xcoffeebabe");
	verifySyntaxError("supercore!");
	verifyThreadNameError("pork=1", {std::string("fish")});

	return EXIT_SUCCESS;
}
