#include <map>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include <gtest/gtest.h>

#include <misc/const_iterator.h>
#include <misc/dereference_iterator.h>
#include <misc/first_iterator.h>
#include <misc/second_iterator.h>

#include "unittest.h"

class TestMapDataIteratorValue : public ::testing::Test {
protected:
	std::vector<std::string> expected {"apple", "bubble", "cookie"};
	std::map<char, std::string> m {std::make_pair('a', expected[0]),
		std::make_pair('b', expected[1]), std::make_pair('c', expected[2])};

	using iterator = misc::second_iterator<decltype(m)::iterator>;
	iterator iterator_to_front() { return iterator(m.begin()); }
	iterator iterator_to_back() { return iterator(--m.end()); }

	using const_iterator = misc::second_iterator<decltype(m)::const_iterator>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(m.cbegin());
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(--m.cend());
	}

	iterator begin {iterator_to_front()};
	const_iterator cbegin {const_iterator_to_front()};
};

TEST_F(TestMapDataIteratorValue, conversion)
{
	const_iterator cbegin1(begin);
	EXPECT_EQ(begin.base(), cbegin1.base());

	const_iterator cbegin2;
	cbegin2 = begin;
	EXPECT_EQ(begin.base(), cbegin2.base());
}

TEST_F(TestMapDataIteratorValue, dereference)
{
	EXPECT_EQ(expected[0], *begin);

	EXPECT_EQ(expected[0], *cbegin);
}

TEST_F(TestMapDataIteratorValue, arrow)
{
	EXPECT_EQ(expected[0], begin->substr());

	EXPECT_EQ(expected[0], cbegin->substr());
}

TEST_F(TestMapDataIteratorValue, increment)
{
	EXPECT_EQ(expected[0], *begin);
	EXPECT_EQ(expected[1], *++begin);
	EXPECT_EQ(expected[1], *begin);
	EXPECT_EQ(expected[1], *begin++);
	EXPECT_EQ(expected[2], *begin);

	EXPECT_EQ(expected[0], *cbegin);
	EXPECT_EQ(expected[1], *++cbegin);
	EXPECT_EQ(expected[1], *cbegin);
	EXPECT_EQ(expected[1], *cbegin++);
	EXPECT_EQ(expected[2], *cbegin);
}

TEST_F(TestMapDataIteratorValue, decrement)
{
	auto end = iterator_to_back();
	auto cend = const_iterator_to_back();

	EXPECT_EQ(expected[2], *end);
	EXPECT_EQ(expected[1], *--end);
	EXPECT_EQ(expected[1], *end);
	EXPECT_EQ(expected[1], *end--);
	EXPECT_EQ(expected[0], *end);

	EXPECT_EQ(expected[2], *cend);
	EXPECT_EQ(expected[1], *--cend);
	EXPECT_EQ(expected[1], *cend);
	EXPECT_EQ(expected[1], *cend--);
	EXPECT_EQ(expected[0], *cend);
}

TEST_F(TestMapDataIteratorValue, relational)
{
	testEquality(iterator_to_front(), iterator_to_back());
	testEquality(iterator_to_front(), const_iterator_to_back());
	testEquality(const_iterator_to_front(), iterator_to_back());
	testEquality(const_iterator_to_front(), const_iterator_to_back());
}

TEST_F(TestMapDataIteratorValue, range)
{
	auto range = misc::second(m);
	EXPECT_EQ(m.begin(), range.begin().base());
	EXPECT_EQ(m.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(m)& cm = m;
	auto crange = misc::second(cm);
	EXPECT_EQ(cm.begin(), crange.begin().base());
	EXPECT_EQ(cm.end(), crange.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestMapDataIteratorValue, type)
{
	EXPECT_TRUE((std::is_same_v<std::string, decltype(begin)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin)::pointer>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(begin)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(*begin)>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin.operator->())>));

	EXPECT_TRUE((std::is_same_v<std::string, decltype(cbegin)::value_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(cbegin)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*cbegin)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin.operator->())>));
}

class TestMapDataIteratorPointer : public ::testing::Test {
protected:
	std::vector<std::string> expected {"apple", "bubble", "cookie"};
	std::map<char, std::string*> m {std::make_pair('a', &expected[0]),
		std::make_pair('b', &expected[1]), std::make_pair('c', &expected[2])};

	using iterator = misc::dereference_iterator<
		misc::second_iterator<decltype(m)::iterator>>;
	iterator iterator_to_front()
	{
		return iterator(iterator::iterator_type(m.begin()));
	}
	iterator iterator_to_back()
	{
		return iterator(iterator::iterator_type(--m.end()));
	}

	using const_iterator = misc::const_iterator<misc::dereference_iterator<
		misc::second_iterator<decltype(m)::const_iterator>>>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(const_iterator::iterator_type(
			const_iterator::iterator_type::iterator_type(m.cbegin())));
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(const_iterator::iterator_type(
			const_iterator::iterator_type::iterator_type(--m.cend())));
	}

	iterator begin {iterator_to_front()};
	const_iterator cbegin {const_iterator_to_front()};
};

TEST_F(TestMapDataIteratorPointer, conversion)
{
	const_iterator cbegin1(begin);
	EXPECT_EQ(begin, cbegin1.base());

	const_iterator cbegin2;
	cbegin2 = begin;
	EXPECT_EQ(begin, cbegin2.base());
}

TEST_F(TestMapDataIteratorPointer, dereference)
{
	EXPECT_EQ(expected[0], *begin);

	EXPECT_EQ(expected[0], *cbegin);
}

TEST_F(TestMapDataIteratorPointer, arrow)
{
	EXPECT_EQ(expected[0], begin->substr());

	EXPECT_EQ(expected[0], cbegin->substr());
}

TEST_F(TestMapDataIteratorPointer, increment)
{
	EXPECT_EQ(expected[0], *begin);
	EXPECT_EQ(expected[1], *++begin);
	EXPECT_EQ(expected[1], *begin);
	EXPECT_EQ(expected[1], *begin++);
	EXPECT_EQ(expected[2], *begin);

	EXPECT_EQ(expected[0], *cbegin);
	EXPECT_EQ(expected[1], *++cbegin);
	EXPECT_EQ(expected[1], *cbegin);
	EXPECT_EQ(expected[1], *cbegin++);
	EXPECT_EQ(expected[2], *cbegin);
}

TEST_F(TestMapDataIteratorPointer, decrement)
{
	auto end = iterator_to_back();
	auto cend = const_iterator_to_back();

	EXPECT_EQ(expected[2], *end);
	EXPECT_EQ(expected[1], *--end);
	EXPECT_EQ(expected[1], *end);
	EXPECT_EQ(expected[1], *end--);
	EXPECT_EQ(expected[0], *end);

	EXPECT_EQ(expected[2], *cend);
	EXPECT_EQ(expected[1], *--cend);
	EXPECT_EQ(expected[1], *cend);
	EXPECT_EQ(expected[1], *cend--);
	EXPECT_EQ(expected[0], *cend);
}

TEST_F(TestMapDataIteratorPointer, relational)
{
	testEquality(iterator_to_front(), iterator_to_back());
	testEquality(iterator_to_front(), const_iterator_to_back());
	testEquality(const_iterator_to_front(), iterator_to_back());
	testEquality(const_iterator_to_front(), const_iterator_to_back());
}

TEST_F(TestMapDataIteratorPointer, range)
{
	auto range = misc::deref(misc::second(m));
	EXPECT_EQ(m.begin(), range.begin().base().base());
	EXPECT_EQ(m.end(), range.end().base().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(m)& cm = m;
	auto crange = misc::constify(misc::deref(misc::second(cm)));
	EXPECT_EQ(cm.begin(), crange.begin().base().base().base());
	EXPECT_EQ(cm.end(), crange.end().base().base().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestMapDataIteratorPointer, type)
{
	EXPECT_TRUE((std::is_same_v<std::string, decltype(begin)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin)::pointer>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(begin)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(*begin)>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin.operator->())>));

	EXPECT_TRUE((std::is_same_v<std::string, decltype(cbegin)::value_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(cbegin)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*cbegin)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin.operator->())>));
}

class TestMapDataIteratorUniquePtr : public ::testing::Test {
protected:
	std::vector<std::string> expected {"apple", "bubble", "cookie"};
	std::map<char, std::unique_ptr<std::string>> m;

	TestMapDataIteratorUniquePtr()
	{
		m.emplace('a', decltype(m)::mapped_type(new std::string(expected[0])));
		m.emplace('b', decltype(m)::mapped_type(new std::string(expected[1])));
		m.emplace('c', decltype(m)::mapped_type(new std::string(expected[2])));

		begin = iterator_to_front();
		cbegin = const_iterator_to_front();
	}

	using iterator = misc::dereference_iterator<
		misc::second_iterator<decltype(m)::iterator>>;
	iterator iterator_to_front()
	{
		return iterator(iterator::iterator_type(m.begin()));
	}
	iterator iterator_to_back()
	{
		return iterator(iterator::iterator_type(--m.end()));
	}

	using const_iterator = misc::const_iterator<misc::dereference_iterator<
		misc::second_iterator<decltype(m)::const_iterator>>>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(const_iterator::iterator_type(
			const_iterator::iterator_type::iterator_type(m.cbegin())));
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(const_iterator::iterator_type(
			const_iterator::iterator_type::iterator_type(--m.cend())));
	}

	iterator begin;
	const_iterator cbegin;
};

TEST_F(TestMapDataIteratorUniquePtr, conversion)
{
	const_iterator cbegin1(begin);
	EXPECT_EQ(begin, cbegin1);

	const_iterator cbegin2;
	cbegin2 = begin;
	EXPECT_EQ(begin, cbegin2);
}

TEST_F(TestMapDataIteratorUniquePtr, dereference)
{
	EXPECT_EQ(expected[0], *begin);

	EXPECT_EQ(expected[0], *cbegin);
}

TEST_F(TestMapDataIteratorUniquePtr, arrow)
{
	EXPECT_EQ(expected[0], begin->substr());

	EXPECT_EQ(expected[0], cbegin->substr());
}

TEST_F(TestMapDataIteratorUniquePtr, increment)
{
	EXPECT_EQ(expected[0], *begin);
	EXPECT_EQ(expected[1], *++begin);
	EXPECT_EQ(expected[1], *begin);
	EXPECT_EQ(expected[1], *begin++);
	EXPECT_EQ(expected[2], *begin);

	EXPECT_EQ(expected[0], *cbegin);
	EXPECT_EQ(expected[1], *++cbegin);
	EXPECT_EQ(expected[1], *cbegin);
	EXPECT_EQ(expected[1], *cbegin++);
	EXPECT_EQ(expected[2], *cbegin);
}

TEST_F(TestMapDataIteratorUniquePtr, decrement)
{
	auto end = iterator_to_back();
	auto cend = const_iterator_to_back();

	EXPECT_EQ(expected[2], *end);
	EXPECT_EQ(expected[1], *--end);
	EXPECT_EQ(expected[1], *end);
	EXPECT_EQ(expected[1], *end--);
	EXPECT_EQ(expected[0], *end);

	EXPECT_EQ(expected[2], *cend);
	EXPECT_EQ(expected[1], *--cend);
	EXPECT_EQ(expected[1], *cend);
	EXPECT_EQ(expected[1], *cend--);
	EXPECT_EQ(expected[0], *cend);
}

TEST_F(TestMapDataIteratorUniquePtr, relational)
{
	testEquality(iterator_to_front(), iterator_to_back());
	testEquality(iterator_to_front(), const_iterator_to_back());
	testEquality(const_iterator_to_front(), iterator_to_back());
	testEquality(const_iterator_to_front(), const_iterator_to_back());
}

TEST_F(TestMapDataIteratorUniquePtr, range)
{
	auto range = misc::deref(misc::second(m));
	EXPECT_EQ(m.begin(), range.begin().base().base());
	EXPECT_EQ(m.end(), range.end().base().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(m)& cm = m;
	auto crange = misc::constify(misc::deref(misc::second(cm)));
	EXPECT_EQ(cm.begin(), crange.begin().base().base().base());
	EXPECT_EQ(cm.end(), crange.end().base().base().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestMapDataIteratorUniquePtr, type)
{
	EXPECT_TRUE((std::is_same_v<std::string, decltype(begin)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin)::pointer>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(begin)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(*begin)>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin.operator->())>));

	EXPECT_TRUE((std::is_same_v<std::string, decltype(cbegin)::value_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(cbegin)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*cbegin)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin.operator->())>));
}

class TestMapKeyIteratorValue : public ::testing::Test {
protected:
	std::vector<std::string> expected {"apple", "bubble", "cookie"};
	std::map<std::string, char> m {std::make_pair(expected[0], 'a'),
		std::make_pair(expected[1], 'b'), std::make_pair(expected[2], 'c')};

	using iterator = misc::first_iterator<decltype(m)::iterator>;
	iterator iterator_to_front() { return iterator(m.begin()); }
	iterator iterator_to_back() { return iterator(--m.end()); }

	using const_iterator = misc::first_iterator<decltype(m)::const_iterator>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(m.cbegin());
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(--m.cend());
	}

	iterator begin {iterator_to_front()};
	const_iterator cbegin {const_iterator_to_front()};
};

TEST_F(TestMapKeyIteratorValue, conversion)
{
	const_iterator cbegin1(begin);
	EXPECT_EQ(begin.base(), cbegin1.base());

	const_iterator cbegin2;
	cbegin2 = begin;
	EXPECT_EQ(begin.base(), cbegin2.base());
}

TEST_F(TestMapKeyIteratorValue, dereference)
{
	EXPECT_EQ(expected[0], *cbegin);
}

TEST_F(TestMapKeyIteratorValue, arrow)
{
	EXPECT_EQ(expected[0], cbegin->substr());
}

TEST_F(TestMapKeyIteratorValue, increment)
{
	EXPECT_EQ(expected[0], *cbegin);
	EXPECT_EQ(expected[1], *++cbegin);
	EXPECT_EQ(expected[1], *cbegin);
	EXPECT_EQ(expected[1], *cbegin++);
	EXPECT_EQ(expected[2], *cbegin);
}

TEST_F(TestMapKeyIteratorValue, decrement)
{
	auto cend = const_iterator_to_back();

	EXPECT_EQ(expected[2], *cend);
	EXPECT_EQ(expected[1], *--cend);
	EXPECT_EQ(expected[1], *cend);
	EXPECT_EQ(expected[1], *cend--);
	EXPECT_EQ(expected[0], *cend);
}

TEST_F(TestMapKeyIteratorValue, relational)
{
	testEquality(iterator_to_front(), iterator_to_back());
	testEquality(iterator_to_front(), const_iterator_to_back());
	testEquality(const_iterator_to_front(), iterator_to_back());
	testEquality(const_iterator_to_front(), const_iterator_to_back());
}

TEST_F(TestMapKeyIteratorValue, range)
{
	auto range = misc::first(m);
	EXPECT_EQ(m.begin(), range.begin().base());
	EXPECT_EQ(m.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(m)& cm = m;
	auto crange = misc::first(cm);
	EXPECT_EQ(cm.begin(), range.begin().base());
	EXPECT_EQ(cm.end(), range.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestMapKeyIteratorValue, type)
{
	EXPECT_TRUE(
		(std::is_same_v<const std::string, decltype(begin)::value_type>));
	EXPECT_TRUE((std::is_same_v<const std::string*, decltype(begin)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(begin)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*begin)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(begin.operator->())>));

	EXPECT_TRUE(
		(std::is_same_v<const std::string, decltype(cbegin)::value_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(cbegin)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*cbegin)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin.operator->())>));
}

class TestMapKeyIteratorPointer : public ::testing::Test {
protected:
	struct Compare {
		template <typename T>
		bool operator()(const T& lhs, const T& rhs) const
		{
			return *lhs < *rhs;
		}
	};

	std::vector<std::string> expected {"apple", "bubble", "cookie"};
	std::map<std::string*, char, Compare> m {std::make_pair(&expected[0], 'a'),
		std::make_pair(&expected[1], 'b'), std::make_pair(&expected[2], 'c')};

	using iterator = misc::dereference_iterator<
		misc::first_iterator<decltype(m)::iterator>>;
	iterator iterator_to_front()
	{
		return iterator(iterator::iterator_type(m.begin()));
	}
	iterator iterator_to_back()
	{
		return iterator(iterator::iterator_type(--m.end()));
	}

	using const_iterator = misc::const_iterator<misc::dereference_iterator<
		misc::first_iterator<decltype(m)::const_iterator>>>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(const_iterator::iterator_type(
			const_iterator::iterator_type::iterator_type(m.cbegin())));
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(const_iterator::iterator_type(
			const_iterator::iterator_type::iterator_type(--m.cend())));
	}

	iterator begin {iterator_to_front()};
	const_iterator cbegin {const_iterator_to_front()};
};

TEST_F(TestMapKeyIteratorPointer, dereference)
{
	EXPECT_EQ(expected[0], *cbegin);
}

TEST_F(TestMapKeyIteratorPointer, arrow)
{
	EXPECT_EQ(expected[0], cbegin->substr());
}

TEST_F(TestMapKeyIteratorPointer, increment)
{
	EXPECT_EQ(expected[0], *cbegin);
	EXPECT_EQ(expected[1], *++cbegin);
	EXPECT_EQ(expected[1], *cbegin);
	EXPECT_EQ(expected[1], *cbegin++);
	EXPECT_EQ(expected[2], *cbegin);
}

TEST_F(TestMapKeyIteratorPointer, decrement)
{
	auto cend = const_iterator_to_back();

	EXPECT_EQ(expected[2], *cend);
	EXPECT_EQ(expected[1], *--cend);
	EXPECT_EQ(expected[1], *cend);
	EXPECT_EQ(expected[1], *cend--);
	EXPECT_EQ(expected[0], *cend);
}

TEST_F(TestMapKeyIteratorPointer, relational)
{
	testEquality(iterator_to_front(), iterator_to_back());
	testEquality(iterator_to_front(), const_iterator_to_back());
	testEquality(const_iterator_to_front(), iterator_to_back());
	testEquality(const_iterator_to_front(), const_iterator_to_back());
}

TEST_F(TestMapKeyIteratorPointer, range)
{
	auto range = misc::deref(misc::first(m));
	EXPECT_EQ(m.begin(), range.begin().base().base());
	EXPECT_EQ(m.end(), range.end().base().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(m)& cm = m;
	auto crange = misc::constify(misc::deref(misc::first(cm)));
	EXPECT_EQ(cm.begin(), crange.begin().base().base().base());
	EXPECT_EQ(cm.end(), crange.end().base().base().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestMapKeyIteratorPointer, type)
{
	EXPECT_TRUE((std::is_same_v<std::string, decltype(begin)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin)::pointer>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(begin)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(*begin)>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin.operator->())>));

	EXPECT_TRUE((std::is_same_v<std::string, decltype(cbegin)::value_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(cbegin)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*cbegin)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin.operator->())>));
}

class TestMapKeyIteratorUniquePtr : public ::testing::Test {
protected:
	struct Compare {
		template <typename T>
		bool operator()(const T& lhs, const T& rhs) const
		{
			return *lhs < *rhs;
		}
	};

	std::vector<std::string> expected {"apple", "bubble", "cookie"};
	std::map<std::unique_ptr<std::string>, char, Compare> m;

	TestMapKeyIteratorUniquePtr()
	{
		m.emplace(decltype(m)::key_type(new std::string(expected[0])), 'a');
		m.emplace(decltype(m)::key_type(new std::string(expected[1])), 'b');
		m.emplace(decltype(m)::key_type(new std::string(expected[2])), 'c');

		begin = iterator_to_front();
		cbegin = const_iterator_to_front();
	}

	using iterator = misc::dereference_iterator<
		misc::first_iterator<decltype(m)::iterator>>;
	iterator iterator_to_front()
	{
		return iterator(iterator::iterator_type(m.begin()));
	}
	iterator iterator_to_back()
	{
		return iterator(iterator::iterator_type(--m.end()));
	}

	using const_iterator = misc::const_iterator<misc::dereference_iterator<
		misc::first_iterator<decltype(m)::const_iterator>>>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(const_iterator::iterator_type(
			const_iterator::iterator_type::iterator_type(m.cbegin())));
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(const_iterator::iterator_type(
			const_iterator::iterator_type::iterator_type(--m.cend())));
	}

	iterator begin;
	const_iterator cbegin;
};

TEST_F(TestMapKeyIteratorUniquePtr, dereference)
{
	EXPECT_EQ(expected[0], *cbegin);
}

TEST_F(TestMapKeyIteratorUniquePtr, arrow)
{
	EXPECT_EQ(expected[0], cbegin->substr());
}

TEST_F(TestMapKeyIteratorUniquePtr, increment)
{
	EXPECT_EQ(expected[0], *cbegin);
	EXPECT_EQ(expected[1], *++cbegin);
	EXPECT_EQ(expected[1], *cbegin);
	EXPECT_EQ(expected[1], *cbegin++);
	EXPECT_EQ(expected[2], *cbegin);
}

TEST_F(TestMapKeyIteratorUniquePtr, decrement)
{
	auto cend = const_iterator_to_back();

	EXPECT_EQ(expected[2], *cend);
	EXPECT_EQ(expected[1], *--cend);
	EXPECT_EQ(expected[1], *cend);
	EXPECT_EQ(expected[1], *cend--);
	EXPECT_EQ(expected[0], *cend);
}

TEST_F(TestMapKeyIteratorUniquePtr, relational)
{
	testEquality(iterator_to_front(), iterator_to_back());
	testEquality(iterator_to_front(), const_iterator_to_back());
	testEquality(const_iterator_to_front(), iterator_to_back());
	testEquality(const_iterator_to_front(), const_iterator_to_back());
}

TEST_F(TestMapKeyIteratorUniquePtr, range)
{
	auto range = misc::deref(misc::first(m));
	EXPECT_EQ(m.begin(), range.begin().base().base());
	EXPECT_EQ(m.end(), range.end().base().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(m)& cm = m;
	auto crange = misc::constify(misc::deref(misc::first(cm)));
	EXPECT_EQ(cm.begin(), crange.begin().base().base().base());
	EXPECT_EQ(cm.end(), crange.end().base().base().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestMapKeyIteratorUniquePtr, type)
{
	EXPECT_TRUE((std::is_same_v<std::string, decltype(begin)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin)::pointer>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(begin)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string&, decltype(*begin)>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(begin.operator->())>));

	EXPECT_TRUE((std::is_same_v<std::string, decltype(cbegin)::value_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(cbegin)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*cbegin)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(cbegin.operator->())>));
}
