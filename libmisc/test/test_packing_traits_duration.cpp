#include <chrono>
#include <cstddef>
#include <cstdint>
#include <ratio>

#include <gtest/gtest.h>

#include <misc/Record.h>
#include <misc/dump.h>
#include <misc/packing_traits_duration.h>

TEST(Packing_traits, duration)
{
	using Nanoseconds = std::chrono::duration<int64_t, std::nano>;
	using Seconds = std::chrono::duration<uint64_t>;
	using Minutes = std::chrono::duration<int32_t, std::ratio<60>>;
	using Hours = std::chrono::duration<uint32_t, std::ratio<3600>>;
	misc::Record<Nanoseconds, Seconds, Minutes, Hours> r;
	constexpr size_t expectedSize = sizeof(Nanoseconds::rep)
		+ sizeof(Seconds::rep) + sizeof(Minutes::rep) + sizeof(Hours::rep);

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	const Nanoseconds a {0x11};
	const Seconds b {0x22};
	const Minutes c {0x33};
	const Hours d {0x44};
	const char expectedData[] = "\x00\x00\x00\x00\x00\x00\x00\x11"
								"\x00\x00\x00\x00\x00\x00\x00\x22"
								"\x00\x00\x00\x33"
								"\x00\x00\x00\x44";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	Nanoseconds A {};
	Seconds B {};
	Minutes C {};
	Hours D {};
	ASSERT_EQ(
		expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B, C, D));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
}
