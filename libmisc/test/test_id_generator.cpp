#include <gtest/gtest.h>

#include <misc/id_generator.h>
#include <misc/object_id.h>

MISC_OBJECT_ID_DECL(Test_id, uint64_t)
MISC_OBJECT_ID_DECL(Test_no, uint32_t)

using Generator = misc::Id_generator<Test_id, Test_no>;

class Test_id_generator : public ::testing::Test {
protected:
	Generator generator;

	void SetUp() override { generator.reset(misc::Date {20121107}); }
};

TEST_F(Test_id_generator, peek)
{
	EXPECT_EQ(Test_id {201211070000000001ul}, generator.peek());
	EXPECT_EQ(Test_id {201211070000000001ul}, generator.peek());
	EXPECT_EQ(Test_id {201211070000000001ul}, generator.peek());

	generator.reset(misc::Date {20121108});

	EXPECT_EQ(Test_id {201211080000000001ul}, generator.peek());
	EXPECT_EQ(Test_id {201211080000000001ul}, generator.peek());
	EXPECT_EQ(Test_id {201211080000000001ul}, generator.peek());

	generator.reset(misc::Date {20121109});

	EXPECT_EQ(Test_id {201211090000000001ul}, generator.peek());
	EXPECT_EQ(Test_id {201211090000000001ul}, generator.peek());
	EXPECT_EQ(Test_id {201211090000000001ul}, generator.peek());
}

TEST_F(Test_id_generator, next)
{
	EXPECT_EQ(Test_id {201211070000000001ul}, generator.next());
	EXPECT_EQ(Test_id {201211070000000002ul}, generator.next());
	EXPECT_EQ(Test_id {201211070000000003ul}, generator.next());

	generator.reset(misc::Date {20121108});

	EXPECT_EQ(Test_id {201211080000000001ul}, generator.next());
	EXPECT_EQ(Test_id {201211080000000002ul}, generator.next());
	EXPECT_EQ(Test_id {201211080000000003ul}, generator.next());

	generator.reset(misc::Date {20121109});

	EXPECT_EQ(Test_id {201211090000000001ul}, generator.next());
	EXPECT_EQ(Test_id {201211090000000002ul}, generator.next());
	EXPECT_EQ(Test_id {201211090000000003ul}, generator.next());
}

TEST_F(Test_id_generator, date_and_no)
{
	EXPECT_EQ(misc::Date {20121107}, Generator::date(generator.next()));
	EXPECT_EQ(Test_no {2}, Generator::no(generator.next()));

	generator.reset(misc::Date {20121108});

	EXPECT_EQ(misc::Date {20121108}, Generator::date(generator.next()));
	EXPECT_EQ(Test_no {2}, Generator::no(generator.next()));

	generator.reset(misc::Date {20121109});

	EXPECT_EQ(misc::Date {20121109}, Generator::date(generator.next()));
	EXPECT_EQ(Test_no {2}, Generator::no(generator.next()));
}

TEST_F(Test_id_generator, static_id)
{
	const auto date = misc::Date {20121109};
	const auto no = Test_no {2};
	EXPECT_EQ(Test_id {201211090000000002ul}, Generator::id(date, no));
}
