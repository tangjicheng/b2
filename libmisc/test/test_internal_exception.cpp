#include <exception>
#include <string>

#include <gtest/gtest.h>

#include <misc/internal_exception.h>

#include "unittest.h"

TEST(TestInternalError, constructors)
{
	{
		misc::InternalError e {"what", "file", 10};
		EXPECT_STREQ("what", e.what());
		EXPECT_STREQ("file", e.file());
		EXPECT_EQ(10, e.line());
	}
	{
		std::string s {"what"};
		misc::InternalError e {s, "file", 10};
		EXPECT_STREQ("what", e.what());
		EXPECT_STREQ("file", e.file());
		EXPECT_EQ(10, e.line());
	}
	{
		misc::InternalError e {std::string {"what"}, "file", 10};
		EXPECT_STREQ("what", e.what());
		EXPECT_STREQ("file", e.file());
		EXPECT_EQ(10, e.line());
	}
}

TEST(TestInternalError, inheritance)
{
	EXPECT_THROW_WHAT((throw misc::InternalError {"what", "file", 10}),
		std::exception, "what");
}
