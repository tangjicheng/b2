#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include <gtest/gtest.h>

#include <misc/first_iterator.h>

class TestFirstIterator : public ::testing::Test {
protected:
	std::vector<std::string> expected {"one", "two", "three", "four", "five"};
	std::vector<std::pair<std::string, std::string>> data {
		std::make_pair(expected[0], std::string()),
		std::make_pair(expected[1], std::string()),
		std::make_pair(expected[2], std::string()),
		std::make_pair(expected[3], std::string()),
		std::make_pair(expected[4], std::string())};

	using iterator = misc::first_iterator<decltype(data)::iterator>;
	iterator iterator_to_front() { return iterator(data.begin()); }
	iterator iterator_to_back() { return iterator(--data.end()); }

	using const_iterator = misc::first_iterator<decltype(data)::const_iterator>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(data.cbegin());
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(--data.cend());
	}

	iterator iter {iterator_to_front()};
};

TEST_F(TestFirstIterator, dereference)
{
	EXPECT_EQ(expected[0], *iter);
}

TEST_F(TestFirstIterator, arrow)
{
	EXPECT_EQ(expected[0], iter->substr());
}

TEST_F(TestFirstIterator, index)
{
	EXPECT_EQ(expected[0], iter[0]);
	EXPECT_EQ(expected[4], iter[4]);
	EXPECT_EQ(expected[0], iterator_to_back()[-4]);
}

TEST_F(TestFirstIterator, make)
{
	auto iter = misc::make_first_iterator(data.begin());
	EXPECT_EQ(data.begin(), iter.base());
	EXPECT_TRUE((std::is_same_v<iterator, decltype(iter)>));

	auto citer = misc::make_first_iterator(data.cbegin());
	EXPECT_EQ(data.cbegin(), citer.base());
	EXPECT_TRUE((std::is_same_v<const_iterator, decltype(citer)>));

	auto range = misc::first(data.begin(), data.end());
	EXPECT_EQ(data.begin(), range.begin().base());
	EXPECT_EQ(data.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	auto crange = misc::first(data.cbegin(), data.cend());
	EXPECT_EQ(data.cbegin(), crange.begin().base());
	EXPECT_EQ(data.cend(), crange.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestFirstIterator, range)
{
	auto range = misc::first(data);
	EXPECT_EQ(data.begin(), range.begin().base());
	EXPECT_EQ(data.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(data)& cdata = data;
	auto crange = misc::first(cdata);
	EXPECT_EQ(cdata.begin(), crange.begin().base());
	EXPECT_EQ(cdata.end(), crange.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}
