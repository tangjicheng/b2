#include <map>
#include <utility>

#include <gtest/gtest.h>

#include <misc/find_insert_hint.h>

namespace {

struct ComparatorItem {
	int thing;
};

struct Comparator {
	bool operator()(const ComparatorItem& lhs, const ComparatorItem& rhs) const
	{
		return lhs.thing < rhs.thing;
	}
};

struct OperatorItem {
	int thing;
};

bool operator<(const OperatorItem& lhs, const OperatorItem& rhs)
{
	return lhs.thing < rhs.thing;
}

template <typename Map>
void testMap()
{
	Map m {{OperatorItem {1}, 0}, {OperatorItem {3}, 0}, {OperatorItem {5}, 0}};
	auto i = m.begin();
	auto middle = ++i;
	auto last = ++i;
	EXPECT_EQ(std::make_pair(m.begin(), misc::fih_result::found),
		misc::find_insert_hint(m, OperatorItem {0}));
	EXPECT_EQ(std::make_pair(m.begin(), misc::fih_result::collision),
		misc::find_insert_hint(m, OperatorItem {1}));
	EXPECT_EQ(std::make_pair(middle, misc::fih_result::found),
		misc::find_insert_hint(m, OperatorItem {2}));
	EXPECT_EQ(std::make_pair(middle, misc::fih_result::collision),
		misc::find_insert_hint(m, OperatorItem {3}));
	EXPECT_EQ(std::make_pair(last, misc::fih_result::found),
		misc::find_insert_hint(m, OperatorItem {4}));
	EXPECT_EQ(std::make_pair(last, misc::fih_result::collision),
		misc::find_insert_hint(m, OperatorItem {5}));
	EXPECT_EQ(std::make_pair(m.end(), misc::fih_result::found),
		misc::find_insert_hint(m, OperatorItem {6}));
}

} // namespace

TEST(TestFindInsertHint, map)
{
	testMap<std::map<OperatorItem, int>>();
}

TEST(TestFindInsertHint, constMap)
{
	testMap<const std::map<OperatorItem, int>>();
}

namespace {

template <typename Vector>
void testVectorOfOperatorItem()
{
	Vector v {OperatorItem {1}, OperatorItem {3}, OperatorItem {5}};
	auto i = v.begin();
	auto middle = ++i;
	auto last = ++i;
	EXPECT_EQ(std::make_pair(v.begin(), misc::fih_result::found),
		misc::find_insert_hint(v.begin(), v.end(), OperatorItem {0}));
	EXPECT_EQ(std::make_pair(v.begin(), misc::fih_result::collision),
		misc::find_insert_hint(v.begin(), v.end(), OperatorItem {1}));
	EXPECT_EQ(std::make_pair(middle, misc::fih_result::found),
		misc::find_insert_hint(v.begin(), v.end(), OperatorItem {2}));
	EXPECT_EQ(std::make_pair(middle, misc::fih_result::collision),
		misc::find_insert_hint(v.begin(), v.end(), OperatorItem {3}));
	EXPECT_EQ(std::make_pair(last, misc::fih_result::found),
		misc::find_insert_hint(v.begin(), v.end(), OperatorItem {4}));
	EXPECT_EQ(std::make_pair(last, misc::fih_result::collision),
		misc::find_insert_hint(v.begin(), v.end(), OperatorItem {5}));
	EXPECT_EQ(std::make_pair(v.end(), misc::fih_result::found),
		misc::find_insert_hint(v.begin(), v.end(), OperatorItem {6}));
}

} // namespace

TEST(TestFindInsertHint, vectorOfOperatorItem)
{
	testVectorOfOperatorItem<std::vector<OperatorItem>>();
}

TEST(TestFindInsertHint, constVectorOfOperatorItem)
{
	testVectorOfOperatorItem<const std::vector<OperatorItem>>();
}

namespace {

template <typename Vector>
void testVectorOfComparatorItem()
{
	Vector v {ComparatorItem {1}, ComparatorItem {3}, ComparatorItem {5}};
	auto i = v.begin();
	auto middle = ++i;
	auto last = ++i;
	EXPECT_EQ(std::make_pair(v.begin(), misc::fih_result::found),
		misc::find_insert_hint(
			v.begin(), v.end(), ComparatorItem {0}, Comparator()));
	EXPECT_EQ(std::make_pair(v.begin(), misc::fih_result::collision),
		misc::find_insert_hint(
			v.begin(), v.end(), ComparatorItem {1}, Comparator()));
	EXPECT_EQ(std::make_pair(middle, misc::fih_result::found),
		misc::find_insert_hint(
			v.begin(), v.end(), ComparatorItem {2}, Comparator()));
	EXPECT_EQ(std::make_pair(middle, misc::fih_result::collision),
		misc::find_insert_hint(
			v.begin(), v.end(), ComparatorItem {3}, Comparator()));
	EXPECT_EQ(std::make_pair(last, misc::fih_result::found),
		misc::find_insert_hint(
			v.begin(), v.end(), ComparatorItem {4}, Comparator()));
	EXPECT_EQ(std::make_pair(last, misc::fih_result::collision),
		misc::find_insert_hint(
			v.begin(), v.end(), ComparatorItem {5}, Comparator()));
	EXPECT_EQ(std::make_pair(v.end(), misc::fih_result::found),
		misc::find_insert_hint(
			v.begin(), v.end(), ComparatorItem {6}, Comparator()));
}

} // namespace

TEST(TestFindInsertHint, vectorOfComparatorItem)
{
	testVectorOfComparatorItem<std::vector<ComparatorItem>>();
}

TEST(TestFindInsertHint, constVectorOfComparatorItem)
{
	testVectorOfComparatorItem<const std::vector<ComparatorItem>>();
}
