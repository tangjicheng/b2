#include <chrono>
#include <cstdlib>
#include <iostream>
#include <ostream>
#include <thread>

#include <misc/LockedQueue.h>
#include <misc/ThreadedTaskQueue.h>

namespace {

const bool STOP_PUSHING = false;
const bool DRAIN_TASK = false;

void g();

bool keepGoing = true;
misc::LockedQueue<int> q;

misc::ThreadedTaskQueue tq;
misc::Task t(&g);

void f()
{
	for (int i = 0; keepGoing; ++i) {
		std::this_thread::sleep_for(std::chrono::seconds(1));
		if (STOP_PUSHING
			&& i > 3) { // This will prevent the task from being in the queue
						// when it gets drained (see below).
			std::cout << "Not pushing " << i << std::endl;
			continue;
		}
		q.push(i);
		tq.enqueue(t);
		std::cout << "Pushed to data queue and enqueued task " << i
				  << std::endl;
	}
}

void g()
{
	std::cout << "Task function called" << std::endl;
	while (true) {
		auto i = q.pop();
		if (!i.has_value())
			break;
		std::cout << "Popped from data queue " << *i << std::endl;
	}
	std::cout << "Data queue now empty" << std::endl;
}

} // namespace

int main()
{
	std::cout << "Starting pushing thread" << std::endl;
	std::thread push(&f);
	std::cout << "Starting threaded task queue" << std::endl;
	tq.start();
	std::this_thread::sleep_for(std::chrono::seconds(5));
	std::cout << "Stopping threaded task queue" << std::endl;
	tq.stop();
	std::cout << "Stopping pushing thread" << std::endl;
	keepGoing = false;
	push.join();
	if (DRAIN_TASK) {
		std::cout << "Draining the task..."
				  << std::endl; // This will block if the task is in the queue
								// (see above).
		tq.drain(t);
	}
	std::cout << "Size of data queue " << q.size() << " (";
	while (!q.empty())
		std::cout << ' ' << *q.pop();
	std::cout << " )" << std::endl;
	return EXIT_SUCCESS;
}
