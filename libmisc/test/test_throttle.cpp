#include <gtest/gtest.h>

#include <misc/throttle.h>

TEST(Test_throttle, throttling_works)
{
	auto throttle = misc::Throttle<std::uint64_t> {3, 10};
	ASSERT_TRUE(throttle.insert(5));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_TRUE(throttle.insert(6, true));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(2));

	ASSERT_TRUE(throttle.insert(7));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_FALSE(throttle.insert(8));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_TRUE(throttle.insert(8, true));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_FALSE(throttle.insert(15));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_FALSE(throttle.insert(16));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_TRUE(throttle.insert(17));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_TRUE(throttle.insert(18));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_TRUE(throttle.insert(19));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_FALSE(throttle.insert(20));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_FALSE(throttle.insert(27));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_TRUE(throttle.insert(57));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_TRUE(throttle.insert(97));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_TRUE(throttle.insert(197));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_TRUE(throttle.insert(198));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(2));

	ASSERT_TRUE(throttle.insert(198));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_FALSE(throttle.insert(198));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_TRUE(throttle.insert(250));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_FALSE(throttle.insert(249));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_FALSE(throttle.insert(249, true));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_TRUE(throttle.insert(350));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_TRUE(throttle.insert(351));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(2));

	ASSERT_TRUE(throttle.insert(352));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_FALSE(throttle.insert(353));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_TRUE(throttle.insert(353, true));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_TRUE(throttle.insert(364));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_TRUE(throttle.insert(365));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(2));
}

TEST(Test_throttle, nanoseconds_work)
{
	auto throttle = misc::Throttle<std::int64_t> {2, 1'000'000'000};
	ASSERT_TRUE(throttle.insert(1633583737659158500));
	ASSERT_TRUE(throttle.insert(1633583739182474600));
	ASSERT_TRUE(throttle.insert(1633583739185848500));
	ASSERT_FALSE(throttle.insert(1633583739193067200));
	ASSERT_FALSE(throttle.insert(1633583739195567600));
	ASSERT_FALSE(throttle.insert(1633583739199021400));
	ASSERT_FALSE(throttle.insert(1633583739201178000));
	ASSERT_FALSE(throttle.insert(1633583739202392100));
	ASSERT_FALSE(throttle.insert(1633583739726301200));
	ASSERT_FALSE(throttle.insert(1633583739729311600));
	ASSERT_FALSE(throttle.insert(1633583739733387300));
	ASSERT_TRUE(throttle.insert(1633583740650835600));
}

TEST(Test_throttle, rate_works)
{
	auto throttle = misc::Throttle<std::uint64_t> {3, 10};
	ASSERT_EQ(throttle.rate(1), static_cast<size_t>(0));

	ASSERT_TRUE(throttle.insert(5));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));
	ASSERT_EQ(throttle.rate(6), static_cast<size_t>(1));

	ASSERT_TRUE(throttle.insert(6));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(2));
	ASSERT_EQ(throttle.rate(6), static_cast<size_t>(2));

	ASSERT_TRUE(throttle.insert(7));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(3));

	ASSERT_EQ(throttle.rate(15), static_cast<size_t>(3));

	ASSERT_EQ(throttle.rate(16), static_cast<size_t>(2));

	ASSERT_EQ(throttle.rate(17), static_cast<size_t>(1));

	ASSERT_EQ(throttle.rate(18), static_cast<size_t>(0));

	ASSERT_TRUE(throttle.insert(20, true));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(1));

	ASSERT_EQ(throttle.rate(21), static_cast<size_t>(1));

	ASSERT_TRUE(throttle.insert(25, true));
	ASSERT_EQ(throttle.size(), static_cast<size_t>(2));

	ASSERT_EQ(throttle.rate(32), static_cast<size_t>(1));

	ASSERT_EQ(throttle.rate(40), static_cast<size_t>(0));
}
