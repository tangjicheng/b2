#include <gtest/gtest.h>

#include <misc/FlagSet.h>

namespace {

enum class Flags { ENGLISH, PIRATE, RAINBOW };

MISC_FLAGSET_DECL(Flags, FlagBunch)

enum class Flags_64 { ENGLISH = 0, PIRATE = 31, RAINBOW = 63 };

MISC_FLAGSET_TYPE_DECL(Flags_64, Flag_bunch_64, std::uint64_t)

} // namespace

class TestFlagSet : public ::testing::Test {
protected:
	FlagBunch f0, f1, f2, f3;

	void SetUp() override
	{
		f1 = Flags::ENGLISH;
		f2 = Flags::RAINBOW | Flags::PIRATE;
		f3 = Flags::RAINBOW | Flags::PIRATE | Flags::ENGLISH;
	}
};

TEST_F(TestFlagSet, empty)
{
	EXPECT_TRUE(f0.empty());
}

TEST_F(TestFlagSet, defaultConstructor)
{
	struct Ship {
		int id;
		FlagBunch flags;
	};
	const auto ship = Ship {1, {}};
	EXPECT_TRUE(ship.flags.empty());
}

TEST_F(TestFlagSet, includes)
{
	EXPECT_TRUE(f1.includes(Flags::ENGLISH));
	EXPECT_FALSE(f1.includes(Flags::PIRATE));
	EXPECT_FALSE(f1.includes(Flags::RAINBOW));
	EXPECT_TRUE(f3.includes(f2));
	EXPECT_FALSE(f2.includes(f3));
	EXPECT_FALSE(f0.includes(f1));
	EXPECT_TRUE(f1.includes(f0));
}

TEST_F(TestFlagSet, intersects)
{
	EXPECT_TRUE(f1.intersects(Flags::ENGLISH));
	EXPECT_FALSE(f1.intersects(Flags::PIRATE));
	EXPECT_FALSE(f1.intersects(Flags::RAINBOW));
	EXPECT_FALSE(f1.intersects(f2));
	EXPECT_FALSE(f2.intersects(f1));
	EXPECT_FALSE(f0.intersects(f1));
	EXPECT_FALSE(f1.intersects(f0));
	EXPECT_TRUE(f3.intersects(f1));
	EXPECT_TRUE(f1.intersects(f3));
}

TEST_F(TestFlagSet, value)
{
	EXPECT_EQ(0u, f0.value());
	EXPECT_EQ(1u, f1.value());
	EXPECT_EQ(6u, f2.value());
	EXPECT_EQ(7u, f3.value());
}

TEST_F(TestFlagSet, equality)
{
	EXPECT_TRUE(f1 == f1);
	EXPECT_FALSE(f1 == f2);
}

TEST_F(TestFlagSet, inequality)
{
	EXPECT_FALSE(f1 != f1);
	EXPECT_TRUE(f1 != f2);
}

TEST_F(TestFlagSet, intersectAssign)
{
	f2 &= f1;
	EXPECT_EQ(f0, f2);
	f3 &= f1;
	EXPECT_EQ(f1, f3);
}

TEST_F(TestFlagSet, unionAssign)
{
	f2 |= f1;
	EXPECT_EQ(f3, f2);
	f0 |= f1;
	EXPECT_EQ(f1, f0);
}

TEST_F(TestFlagSet, xorAssign)
{
	f3 ^= f1;
	EXPECT_EQ(f2, f3);
	f0 ^= f1;
	EXPECT_EQ(f1, f0);
	f1 ^= f1;
	EXPECT_EQ(FlagBunch(), f1);
}

TEST_F(TestFlagSet, intersectionOperator)
{
	EXPECT_EQ(f0, f1 & f2);
	EXPECT_EQ(f2, f2 & f2);
	EXPECT_EQ(f1, f1 & f3);
	EXPECT_EQ(f1, f3 & f1);
}

TEST_F(TestFlagSet, unionOperator)
{
	EXPECT_EQ(f2, f2 | f0);
	EXPECT_EQ(f2, f2 | f2);
	EXPECT_EQ(f3, f1 | f2);
	EXPECT_EQ(f3, f1 | f3);
	EXPECT_EQ(f3, f3 | f1);
}

TEST_F(TestFlagSet, unionOperatorEnum)
{
	EXPECT_EQ(f3, f2 | Flags::ENGLISH);
}

TEST_F(TestFlagSet, xorOperator)
{
	EXPECT_EQ(f0, f0 ^ f0);
	EXPECT_EQ(f0, f3 ^ f3);
	EXPECT_EQ(f1, f3 ^ f2);
	EXPECT_EQ(f1, f2 ^ f3);
}

TEST_F(TestFlagSet, support_64)
{
	auto flags = Flag_bunch_64 {};
	EXPECT_TRUE(flags.empty());
	flags |= Flags_64::ENGLISH;
	EXPECT_TRUE(flags.includes(Flags_64::ENGLISH));
	EXPECT_FALSE(flags.includes(Flags_64::PIRATE));
	EXPECT_FALSE(flags.includes(Flags_64::RAINBOW));
	flags |= Flags_64::PIRATE;
	EXPECT_TRUE(flags.includes(Flags_64::ENGLISH));
	EXPECT_TRUE(flags.includes(Flags_64::PIRATE));
	EXPECT_FALSE(flags.includes(Flags_64::RAINBOW));
	flags |= Flags_64::RAINBOW;
	EXPECT_TRUE(flags.includes(Flags_64::ENGLISH));
	EXPECT_TRUE(flags.includes(Flags_64::PIRATE));
	EXPECT_TRUE(flags.includes(Flags_64::RAINBOW));
}
