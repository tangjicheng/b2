#include <map>
#include <sstream>
#include <type_traits>
#include <unordered_map>

#include <gtest/gtest.h>

#include <misc/object_id.h>

namespace {

const auto sentinel_value = 42u;
MISC_OBJECT_ID_DECL(Thing_id, unsigned int)
MISC_OBJECT_ID_DECL(Int_id, int)
MISC_OBJECT_ID_HAS_SENTINEL_DECL(Sentinel_id, unsigned int, sentinel_value)

} // namespace

namespace space {

MISC_OBJECT_ID_DECL(Stuff_id, unsigned long)

} // namespace space

TEST(Object_id, parameters_helper)
{
	EXPECT_TRUE((std::is_same_v<Thing_id_tag,
		misc::details::Object_id_parameters<Thing_id>::Tag>));
	EXPECT_TRUE((std::is_same_v<Int_id_tag,
		misc::details::Object_id_parameters<Int_id>::Tag>));

	EXPECT_TRUE((std::is_same_v<unsigned int,
		misc::details::Object_id_parameters<Thing_id>::Underlying_type>));
	EXPECT_TRUE((std::is_same_v<int,
		misc::details::Object_id_parameters<Int_id>::Underlying_type>));
}

TEST(Object_id, default_constructor)
{
	{
		Thing_id id;
		EXPECT_EQ(0u, id.value());
	}
	{
		constexpr Thing_id id;
		EXPECT_EQ(0u, id.value());
	}
	{
		Sentinel_id id;
		EXPECT_EQ(sentinel_value, id.value());
	}
}

TEST(Object_id, value_constructor)
{
	{
		Thing_id id(10);
		EXPECT_EQ(10u, id.value());
	}
	{
		constexpr Thing_id id(20);
		EXPECT_EQ(20u, id.value());
	}
	{
		Sentinel_id id(30);
		EXPECT_EQ(30u, id.value());
	}
}

TEST(Object_id, value)
{
	{
		Thing_id id;
		auto value = id.value();
		EXPECT_EQ(0u, value);
	}
	{
		Thing_id id(10);
		auto value = id.value();
		EXPECT_EQ(10u, value);
	}
	{
		constexpr Thing_id id;
		constexpr auto value = id.value();
		EXPECT_EQ(0u, value);
	}
	{
		constexpr Thing_id id(20);
		constexpr auto value = id.value();
		EXPECT_EQ(20u, value);
	}
	{
		Sentinel_id id;
		auto value = id.value();
		EXPECT_EQ(sentinel_value, value);
	}
	{
		Sentinel_id id(30);
		auto value = id.value();
		EXPECT_EQ(30u, value);
	}
}

TEST(Object_id, is_valid)
{
	{
		Sentinel_id id;
		EXPECT_FALSE(id.is_valid());
	}
	{
		Sentinel_id id(30);
		EXPECT_TRUE(id.is_valid());
	}
	{
		Sentinel_id id(sentinel_value);
		EXPECT_FALSE(id.is_valid());
	}
}

namespace {

template <typename T>
void test_comparisons(const T& low, const T& high)
{
	EXPECT_TRUE(low == low);
	EXPECT_FALSE(low == high);
	EXPECT_FALSE(high == low);
	EXPECT_TRUE(high == high);

	EXPECT_FALSE(low != low);
	EXPECT_TRUE(low != high);
	EXPECT_TRUE(high != low);
	EXPECT_FALSE(high != high);

	EXPECT_FALSE(low < low);
	EXPECT_TRUE(low < high);
	EXPECT_FALSE(high < low);
	EXPECT_FALSE(high < high);

	EXPECT_FALSE(low > low);
	EXPECT_FALSE(low > high);
	EXPECT_TRUE(high > low);
	EXPECT_FALSE(high > high);

	EXPECT_TRUE(low <= low);
	EXPECT_TRUE(low <= high);
	EXPECT_FALSE(high <= low);
	EXPECT_TRUE(high <= high);

	EXPECT_TRUE(low >= low);
	EXPECT_FALSE(low >= high);
	EXPECT_TRUE(high >= low);
	EXPECT_TRUE(high >= high);
}

} // namespace

TEST(Object_id, comparison_operators)
{
	test_comparisons(space::Stuff_id(100), space::Stuff_id(200));
	test_comparisons(Int_id(-100), Int_id(300));
	test_comparisons(Sentinel_id(400), Sentinel_id(500));
	{
		constexpr bool b1 = Thing_id(400) == Thing_id(400);
		EXPECT_TRUE(b1);
		constexpr bool b2 = Thing_id(400) != Thing_id(500);
		EXPECT_TRUE(b2);
		constexpr bool b3 = Thing_id(400) < Thing_id(500);
		EXPECT_TRUE(b3);
		constexpr bool b4 = Thing_id(500) > Thing_id(400);
		EXPECT_TRUE(b4);
		constexpr bool b5 = Thing_id(400) <= Thing_id(400);
		EXPECT_TRUE(b5);
		constexpr bool b6 = Thing_id(500) >= Thing_id(400);
		EXPECT_TRUE(b6);
	}
}

TEST(Object_id, to_string)
{
	EXPECT_EQ("0", to_string(Thing_id()));
	EXPECT_EQ("10", to_string(Thing_id(10)));

	EXPECT_EQ("0", to_string(Int_id()));
	EXPECT_EQ("20", to_string(Int_id(20)));
	EXPECT_EQ("-30", to_string(Int_id(-30)));

	EXPECT_EQ(std::to_string(sentinel_value), to_string(Sentinel_id()));
	EXPECT_EQ("10", to_string(Sentinel_id(10)));
}

TEST(Object_id, output_stream_operator)
{
	{
		space::Stuff_id id(10);
		std::ostringstream oss;
		oss << id;
		EXPECT_EQ("10", oss.str());
	}
	{
		Sentinel_id id(20);
		std::ostringstream oss;
		oss << id;
		EXPECT_EQ("20", oss.str());
	}
}

TEST(Object_id, input_stream_operator)
{
	{
		std::istringstream iss("20");
		space::Stuff_id id;
		iss >> id;
		EXPECT_EQ(20u, id.value());
	}
	{
		std::istringstream iss("30");
		Sentinel_id id;
		iss >> id;
		EXPECT_EQ(30u, id.value());
	}
}

namespace {

template <typename Map>
void test_map(Map& m)
{
	m.emplace(Thing_id(1), 10);
	m.emplace(Thing_id(3), 30);
	m.emplace(Thing_id(2), 20);

	auto i = m.find(Thing_id(1));
	ASSERT_NE(m.end(), i);
	EXPECT_EQ(10, i->second);

	i = m.find(Thing_id(2));
	ASSERT_NE(m.end(), i);
	EXPECT_EQ(20, i->second);

	i = m.find(Thing_id(3));
	ASSERT_NE(m.end(), i);
	EXPECT_EQ(30, i->second);
}

} // namespace

TEST(Object_id, use_in_std_map)
{
	std::map<Thing_id, int> m;
	test_map(m);
}

TEST(Object_id, use_in_std_unordered_map)
{
	std::unordered_map<Thing_id, int> m;
	test_map(m);
}
