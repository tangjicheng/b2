#include <algorithm>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

#include <gtest/gtest.h>

#include <misc/preallocator.h>

namespace misc {

struct Element {
	int a;
	double b;

	void assign(int a, double b)
	{
		this->a = a;
		this->b = b;
	}
};

void acquire(Preallocator<Element>& preallocator,
	std::vector<Element*>& elements, int preallocated_count, double offset,
	int begin = 0)
{
	preallocator.log_info();
	std::cout << "Acquired: ";
	for (auto i = begin; i < begin + preallocated_count; ++i) {
		auto& element = preallocator.acquire();
		element.assign(i, i + offset);
		elements.push_back(&element);
		std::cout << i << ' ';
	}
	std::cout << '\n';
	preallocator.log_info();
}

int release_randomly(
	Preallocator<Element>& preallocator, std::vector<Element*>& elements)
{
	auto random_device = std::random_device {};
	auto generator = std::mt19937 {random_device()};
	auto distribution
		= std::uniform_int_distribution<std::mt19937::result_type> {0, 1};

	std::cout << "Released: ";
	const auto element_count = elements.size();
	auto released_count = 0;
	for (auto i = 0u; i < element_count; ++i) {
		if (distribution(generator) == 1) {
			preallocator.release(*elements[i]);
			++released_count;
			std::cout << i << ' ';
		}
	}
	std::cout << '\n';
	preallocator.log_info();
	return released_count;
}

void reacquire(Preallocator<Element>& preallocator,
	const std::vector<Element*>& elements,
	std::vector<Element*>& reacquired_elements, int released_count,
	double offset)
{
	std::cout << "Reacquired: ";
	for (auto i = 0; i < released_count; ++i) {
		auto& element = preallocator.acquire();
		element.assign(i * i, i + i + offset);
		reacquired_elements.push_back(&element);

		const auto iter_find
			= std::find(elements.begin(), elements.end(), &element);
		EXPECT_NE(iter_find, elements.end());
		std::cout << std::distance(elements.begin(), iter_find) << ' ';
	}
	std::cout << '\n';
	preallocator.log_info();
}

void expect(const std::vector<Element*>& elements, double offset,
	const std::vector<Element*>& reacquired_elements = {})
{
	const auto element_count = static_cast<int>(elements.size());
	for (auto i = 0; i < element_count; ++i) {
		const auto& element = *elements[i];
		const auto iter_find = std::find(
			reacquired_elements.begin(), reacquired_elements.end(), &element);
		if (iter_find == reacquired_elements.end()) {
			EXPECT_EQ(i, element.a);
			EXPECT_DOUBLE_EQ(i + offset, element.b);
		}
		else {
			const auto reacquired_i
				= std::distance(reacquired_elements.begin(), iter_find);
			EXPECT_EQ(reacquired_i * reacquired_i, element.a);
			EXPECT_DOUBLE_EQ(reacquired_i + reacquired_i + offset, element.b);
		}
	}
}

void release(
	Preallocator<Element>& preallocator, std::vector<Element*>& elements)
{
	std::cout << "Releasing " << elements.size() << " elements\n";
	for (auto element : elements)
		preallocator.release(*element);
	preallocator.log_info();
}

void test_fill_and_release(Preallocator<Element>& preallocator)
{
	const auto capacity = preallocator.capacity();
	auto elements = std::vector<Element*> {};
	elements.reserve(capacity);

	constexpr auto offset = 0.1;
	acquire(preallocator, elements, capacity, offset);

	expect(elements, offset);

	release(preallocator, elements);
}

void test_over_acquire(Preallocator<Element>& preallocator)
{
	auto elements = std::vector<Element*> {};
	const auto size = preallocator.capacity();
	try {
		acquire(preallocator, elements, size + 1, 0.1);
	}
	catch (const Preallocator_memory_exception&) {
		return;
	}
	ASSERT_TRUE(false);
}

void test_fill_release_reacquire(Preallocator<Element>& preallocator)
{
	const auto capacity = preallocator.capacity();
	auto elements = std::vector<Element*> {};
	elements.reserve(capacity);

	constexpr auto offset = 0.2;
	acquire(preallocator, elements, capacity, offset);

	const auto released_count = release_randomly(preallocator, elements);

	auto reacquired_elements = std::vector<Element*> {};
	reacquired_elements.reserve(released_count);
	reacquire(
		preallocator, elements, reacquired_elements, released_count, offset);

	expect(elements, offset, reacquired_elements);

	release(preallocator, elements);
}

void test_fill_prefer_reuse(Preallocator<Element>& preallocator)
{
	const auto capacity = preallocator.capacity();
	auto elements = std::vector<Element*> {};
	elements.reserve(capacity);

	const auto half_capacity = capacity / 2;
	constexpr auto offset = 0.3;
	acquire(preallocator, elements, half_capacity, offset);

	const auto released_count = release_randomly(preallocator, elements);

	auto reacquired_elements = std::vector<Element*> {};
	reacquired_elements.reserve(released_count);
	reacquire(
		preallocator, elements, reacquired_elements, released_count, offset);

	acquire(preallocator, elements, capacity - half_capacity, offset,
		half_capacity);

	expect(elements, offset, reacquired_elements);

	release(preallocator, elements);
}

TEST(Test_preallocator, invalid_capacity)
{
	try {
		auto preallocator = Preallocator<Element> {Preallocator_settings {-1}};
	}
	catch (const Preallocator_memory_exception&) {
		return;
	}
	EXPECT_TRUE(false);
}

TEST(Test_preallocator, non_dynamic)
{
	auto preallocator = Preallocator<Element> {Preallocator_settings {50}};
	test_fill_and_release(preallocator);
}

TEST(Test_preallocator, non_dynamic_over_acquire)
{
	constexpr auto size = 100;
	auto preallocator = Preallocator<Element> {Preallocator_settings {size}};
	test_over_acquire(preallocator);
}

TEST(Test_preallocator, non_dynamic_reuse)
{
	auto preallocator = Preallocator<Element> {Preallocator_settings {42}};
	test_fill_release_reacquire(preallocator);
}

TEST(Test_preallocator, non_dynamic_prefer_reuse)
{
	auto preallocator = Preallocator<Element> {Preallocator_settings {18}};
	test_fill_prefer_reuse(preallocator);
}

} // namespace misc
