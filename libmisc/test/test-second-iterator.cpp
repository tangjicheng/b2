#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include <gtest/gtest.h>

#include <misc/second_iterator.h>

class TestSecondIterator : public ::testing::Test {
protected:
	std::vector<std::string> expected {"one", "two", "three", "four", "five"};
	std::vector<std::pair<std::string, std::string>> data {
		std::make_pair(std::string(), expected[0]),
		std::make_pair(std::string(), expected[1]),
		std::make_pair(std::string(), expected[2]),
		std::make_pair(std::string(), expected[3]),
		std::make_pair(std::string(), expected[4])};

	using iterator = misc::second_iterator<decltype(data)::iterator>;
	iterator iterator_to_front() { return iterator(data.begin()); }
	iterator iterator_to_back() { return iterator(--data.end()); }

	using const_iterator
		= misc::second_iterator<decltype(data)::const_iterator>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(data.cbegin());
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(--data.cend());
	}

	iterator iter {iterator_to_front()};
};

TEST_F(TestSecondIterator, dereference)
{
	EXPECT_EQ(expected[0], *iter);
}

TEST_F(TestSecondIterator, arrow)
{
	EXPECT_EQ(expected[0], iter->substr());
}

TEST_F(TestSecondIterator, index)
{
	EXPECT_EQ(expected[0], iter[0]);
	EXPECT_EQ(expected[4], iter[4]);
	EXPECT_EQ(expected[0], iterator_to_back()[-4]);
}

TEST_F(TestSecondIterator, make)
{
	auto iter = misc::make_second_iterator(data.begin());
	EXPECT_EQ(data.begin(), iter.base());
	EXPECT_TRUE((std::is_same_v<iterator, decltype(iter)>));

	auto citer = misc::make_second_iterator(data.cbegin());
	EXPECT_EQ(data.cbegin(), citer.base());
	EXPECT_TRUE((std::is_same_v<const_iterator, decltype(citer)>));

	auto range = misc::second(data.begin(), data.end());
	EXPECT_EQ(data.begin(), range.begin().base());
	EXPECT_EQ(data.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	auto crange = misc::second(data.cbegin(), data.cend());
	EXPECT_EQ(data.cbegin(), crange.begin().base());
	EXPECT_EQ(data.cend(), crange.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestSecondIterator, range)
{
	auto range = misc::second(data);
	EXPECT_EQ(data.begin(), range.begin().base());
	EXPECT_EQ(data.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(data)& cdata = data;
	auto crange = misc::second(cdata);
	EXPECT_EQ(cdata.begin(), crange.begin().base());
	EXPECT_EQ(cdata.end(), crange.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}
