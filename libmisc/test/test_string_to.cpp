#include <cstddef>
#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <misc/string_to.h>

#include "unittest.h"

template <typename T>
class Test_string_to : public ::testing::Test {
protected:
	std::string m_input;
	T m_expectedResult;
	size_t m_expectedEndIndex;

	void testValid(const char*, T, size_t);
	void testValid(const char*, T, size_t, int);
	void testInvalidSign(const char*);
	void testNoConversion(const char*);
	void testUnsupportedBase(const char*, int);
	void testUnderflow(const char*);
	void testOverflow(const char*);
	void testInvalidCharacter(const char*, T, size_t);

	void load(const char*);
	void load(const char*, T, size_t);
	void testWithEndIndex();
	void testWithoutEndIndex();
	void testWithBase(int);
};

template <typename T>
void Test_string_to<T>::testValid(
	const char* input, T expectedResult, size_t expectedEndIndex)
{
	load(input, expectedResult, expectedEndIndex);
	testWithEndIndex();
	testWithoutEndIndex();
}

template <typename T>
void Test_string_to<T>::testValid(
	const char* input, T expectedResult, size_t expectedEndIndex, int base)
{
	load(input, expectedResult, expectedEndIndex);
	testWithBase(base);
}

template <typename T>
void Test_string_to<T>::testInvalidSign(const char* input)
{
	load(input);
	EXPECT_THROW_WHAT(
		testWithEndIndex(), std::invalid_argument, "Invalid sign");
	EXPECT_THROW_WHAT(
		testWithoutEndIndex(), std::invalid_argument, "Invalid sign");
}

template <typename T>
void Test_string_to<T>::testNoConversion(const char* input)
{
	load(input);
	EXPECT_THROW_WHAT(
		testWithEndIndex(), std::invalid_argument, "No conversion");
	EXPECT_THROW_WHAT(
		testWithoutEndIndex(), std::invalid_argument, "No conversion");
}

template <typename T>
void Test_string_to<T>::testUnsupportedBase(const char* input, int base)
{
	load(input);
	EXPECT_THROW_WHAT(
		testWithBase(base), std::invalid_argument, "Unsupported base");
}

template <typename T>
void Test_string_to<T>::testUnderflow(const char* input)
{
	load(input);
	EXPECT_THROW_WHAT(testWithEndIndex(), std::out_of_range, "Underflow");
	EXPECT_THROW_WHAT(testWithoutEndIndex(), std::out_of_range, "Underflow");
}

template <typename T>
void Test_string_to<T>::testOverflow(const char* input)
{
	load(input);
	EXPECT_THROW_WHAT(testWithEndIndex(), std::out_of_range, "Overflow");
	EXPECT_THROW_WHAT(testWithoutEndIndex(), std::out_of_range, "Overflow");
}

template <typename T>
void Test_string_to<T>::testInvalidCharacter(
	const char* input, T expectedResult, size_t expectedEndIndex)
{
	load(input, expectedResult, expectedEndIndex);
	testWithEndIndex();
	EXPECT_THROW_WHAT(
		testWithoutEndIndex(), std::invalid_argument, "Invalid character");
}

template <typename T>
void Test_string_to<T>::load(const char* input)
{
	m_input = input;
}

template <typename T>
void Test_string_to<T>::load(
	const char* input, T expectedResult, size_t expectedEndIndex)
{
	load(input);
	m_expectedResult = expectedResult;
	m_expectedEndIndex = expectedEndIndex;
}

template <typename T>
void Test_string_to<T>::testWithEndIndex()
{
	size_t endIndex = -1;
	EXPECT_EQ(m_expectedResult, misc::string_to<T>(m_input, &endIndex))
		<< "Result of conversion with end index not equal to expected value";
	EXPECT_EQ(m_expectedEndIndex, endIndex)
		<< "End index not equal to expected value";
}

template <typename T>
void Test_string_to<T>::testWithoutEndIndex()
{
	EXPECT_EQ(m_expectedResult, misc::string_to<T>(m_input))
		<< "Result of conversion without end index not equal to expected value";
}

template <typename T>
void Test_string_to<T>::testWithBase(int base)
{
	size_t endIndex = -1;
	EXPECT_EQ(m_expectedResult, misc::string_to<T>(m_input, &endIndex, base))
		<< "Result of conversion with base not equal to expected value";
	EXPECT_EQ(m_expectedEndIndex, endIndex)
		<< "End index not equal to expected value";
}

class TestConvertShort : public Test_string_to<short> {
};
class TestConvertUnsignedShort : public Test_string_to<unsigned short> {
};
class TestConvertInt : public Test_string_to<int> {
};
class TestConvertUnsignedInt : public Test_string_to<unsigned int> {
};
class TestConvertLong : public Test_string_to<long> {
};
class TestConvertUnsignedLong : public Test_string_to<unsigned long> {
};
class TestConvertFloat : public Test_string_to<float> {
};
class TestConvertDouble : public Test_string_to<double> {
};

TEST_F(TestConvertShort, empty)
{
	testNoConversion("");
}

TEST_F(TestConvertUnsignedShort, empty)
{
	testNoConversion("");
}

TEST_F(TestConvertInt, empty)
{
	testNoConversion("");
}

TEST_F(TestConvertUnsignedInt, empty)
{
	testNoConversion("");
}

TEST_F(TestConvertLong, empty)
{
	testNoConversion("");
}

TEST_F(TestConvertUnsignedLong, empty)
{
	testNoConversion("");
}

TEST_F(TestConvertFloat, empty)
{
	testNoConversion("");
}

TEST_F(TestConvertDouble, empty)
{
	testNoConversion("");
}

TEST_F(TestConvertShort, whitespace)
{
	testNoConversion("   ");
}

TEST_F(TestConvertUnsignedShort, whitespace)
{
	testNoConversion("   ");
}

TEST_F(TestConvertInt, whitespace)
{
	testNoConversion("   ");
}

TEST_F(TestConvertUnsignedInt, whitespace)
{
	testNoConversion("   ");
}

TEST_F(TestConvertLong, whitespace)
{
	testNoConversion("   ");
}

TEST_F(TestConvertUnsignedLong, whitespace)
{
	testNoConversion("   ");
}

TEST_F(TestConvertFloat, whitespace)
{
	testNoConversion("   ");
}

TEST_F(TestConvertDouble, whitespace)
{
	testNoConversion("   ");
}

TEST_F(TestConvertShort, valid)
{
	testValid("3", 3, 1);
}

TEST_F(TestConvertUnsignedShort, valid)
{
	testValid("6", 6u, 1);
}

TEST_F(TestConvertInt, valid)
{
	testValid("11", 11, 2);
}

TEST_F(TestConvertUnsignedInt, valid)
{
	testValid("21", 21u, 2);
}

TEST_F(TestConvertLong, valid)
{
	testValid("42", 42l, 2);
}

TEST_F(TestConvertUnsignedLong, valid)
{
	testValid("84", 84ul, 2);
}

TEST_F(TestConvertFloat, valid)
{
	testValid("1.57", 1.57f, 4);
}

TEST_F(TestConvertDouble, valid)
{
	testValid("3.14", 3.14, 4);
}

TEST_F(TestConvertShort, invalidTrailingCharacters)
{
	testInvalidCharacter("3abc", 3, 1);
}

TEST_F(TestConvertUnsignedShort, invalidTrailingCharacters)
{
	testInvalidCharacter("6def", 6u, 1);
}

TEST_F(TestConvertInt, invalidTrailingCharacters)
{
	testInvalidCharacter("11abc", 11, 2);
}

TEST_F(TestConvertUnsignedInt, invalidTrailingCharacters)
{
	testInvalidCharacter("21def", 21u, 2);
}

TEST_F(TestConvertLong, invalidTrailingCharacters)
{
	testInvalidCharacter("42abc", 42l, 2);
}

TEST_F(TestConvertUnsignedLong, invalidTrailingCharacters)
{
	testInvalidCharacter("84def", 84ul, 2);
}

TEST_F(TestConvertFloat, invalidTrailingCharacters)
{
	testInvalidCharacter("1.57xyz", 1.57f, 4);
}

TEST_F(TestConvertDouble, invalidTrailingCharacters)
{
	testInvalidCharacter("3.14xyz", 3.14, 4);
}

TEST_F(TestConvertShort, invalidLeadingCharacters)
{
	testNoConversion("abc3");
}

TEST_F(TestConvertUnsignedShort, invalidLeadingCharacters)
{
	testNoConversion("def6");
}

TEST_F(TestConvertInt, invalidLeadingCharacters)
{
	testNoConversion("abc11");
}

TEST_F(TestConvertUnsignedInt, invalidLeadingCharacters)
{
	testNoConversion("def21");
}

TEST_F(TestConvertLong, invalidLeadingCharacters)
{
	testNoConversion("abc42");
}

TEST_F(TestConvertUnsignedLong, invalidLeadingCharacters)
{
	testNoConversion("def84");
}

TEST_F(TestConvertFloat, invalidLeadingCharacters)
{
	testNoConversion("xyz1.57");
}

TEST_F(TestConvertDouble, invalidLeadingCharacters)
{
	testNoConversion("xyz3.14");
}

TEST_F(TestConvertShort, leadingSpaceOrSign)
{
	testValid(" 3", 3, 2);
	testValid("+3", 3, 2);
	testValid(" +3", 3, 3);
	testValid("-3", -3, 2);
	testValid(" -3", -3, 3);
}

TEST_F(TestConvertUnsignedShort, leadingSpaceOrSign)
{
	testValid(" 6", 6u, 2);

	testInvalidSign("+6");
	testInvalidSign(" +6");
	testInvalidSign("-6");
	testInvalidSign(" -6");
}

TEST_F(TestConvertInt, leadingSpaceOrSign)
{
	testValid(" 11", 11, 3);
	testValid("+11", 11, 3);
	testValid(" +11", 11, 4);
	testValid("-11", -11, 3);
	testValid(" -11", -11, 4);
}

TEST_F(TestConvertUnsignedInt, leadingSpaceOrSign)
{
	testValid(" 21", 21u, 3);

	testInvalidSign("+21");
	testInvalidSign(" +21");
	testInvalidSign("-21");
	testInvalidSign(" -21");
}

TEST_F(TestConvertLong, leadingSpaceOrSign)
{
	testValid(" 42", 42l, 3);
	testValid("+42", 42l, 3);
	testValid(" +42", 42l, 4);
	testValid("-42", -42l, 3);
	testValid(" -42", -42l, 4);
}

TEST_F(TestConvertUnsignedLong, leadingSpaceOrSign)
{
	testValid(" 84", 84ul, 3);

	testInvalidSign("+84");
	testInvalidSign(" +84");
	testInvalidSign("-84");
	testInvalidSign(" -84");
}

TEST_F(TestConvertFloat, leadingSpaceOrSign)
{
	testValid(" 1.57", 1.57f, 5);
	testValid("+1.57", 1.57f, 5);
	testValid(" +1.57", 1.57f, 6);
	testValid("-1.57", -1.57f, 5);
	testValid(" -1.57", -1.57f, 6);
}

TEST_F(TestConvertDouble, leadingSpaceOrSign)
{
	testValid(" 3.14", 3.14, 5);
	testValid("+3.14", 3.14, 5);
	testValid(" +3.14", 3.14, 6);
	testValid("-3.14", -3.14, 5);
	testValid(" -3.14", -3.14, 6);
}

TEST_F(TestConvertShort, overflow)
{
	testOverflow("32768");
}

TEST_F(TestConvertUnsignedShort, overflow)
{
	testOverflow("65536");
}

TEST_F(TestConvertInt, overflow)
{
	testOverflow("2147483648");
}

TEST_F(TestConvertUnsignedInt, overflow)
{
	testOverflow("4294967296");
}

TEST_F(TestConvertLong, overflow)
{
	testOverflow("9223372036854775808");
}

TEST_F(TestConvertUnsignedLong, overflow)
{
	testOverflow("18446744073709551616");
}

TEST_F(TestConvertFloat, overflow)
{
	testOverflow("3.40283e+38");
	testOverflow("-3.40283e+38");
}

TEST_F(TestConvertDouble, overflow)
{
	testOverflow("1.79770e+308");
	testOverflow("-1.79770e+308");
}

TEST_F(TestConvertShort, underflow)
{
	testUnderflow("-32769");
}

TEST_F(TestConvertInt, underflow)
{
	testUnderflow("-2147483649");
}

TEST_F(TestConvertLong, underflow)
{
	testUnderflow("-9223372036854775809");
}

TEST_F(TestConvertFloat, underflow)
{
	testUnderflow("1.17549e-38");
}

TEST_F(TestConvertDouble, underflow)
{
	testUnderflow("2.22507e-308");
}

TEST_F(TestConvertShort, validBase)
{
	testValid("3", 3, 1, 10);
	testValid("A", 10, 1, 16);
	testValid("Z", 35, 1, 36);
}

TEST_F(TestConvertUnsignedShort, validBase)
{
	testValid("6", 6u, 1, 10);
	testValid("AB", 171u, 2, 16);
	testValid("ZY", 1294u, 2, 36);
}

TEST_F(TestConvertInt, validBase)
{
	testValid("11", 11, 2, 10);
	testValid("ABC", 2748, 3, 16);
	testValid("ZYX", 46617, 3, 36);
}

TEST_F(TestConvertUnsignedInt, validBase)
{
	testValid("21", 21u, 2, 10);
	testValid("ABCD", 43981u, 4, 16);
	testValid("ZYXW", 1678244u, 4, 36);
}

TEST_F(TestConvertLong, validBase)
{
	testValid("42", 42l, 2, 10);
	testValid("ABCDE", 703710l, 5, 16);
	testValid("ZYXWV", 60416815l, 5, 36);
}

TEST_F(TestConvertUnsignedLong, validBase)
{
	testValid("84", 84ul, 2, 10);
	testValid("ABCDEF", 11259375ul, 6, 16);
	testValid("ZYXWVU", 2175005370ul, 6, 36);
}

TEST_F(TestConvertShort, unsupportedBase)
{
	testUnsupportedBase("1", -1);
	testUnsupportedBase("1", 1);
	testUnsupportedBase("1", 37);
}

TEST_F(TestConvertUnsignedShort, unsupportedBase)
{
	testUnsupportedBase("1", -1);
	testUnsupportedBase("1", 1);
	testUnsupportedBase("1", 37);
}

TEST_F(TestConvertInt, unsupportedBase)
{
	testUnsupportedBase("1", -1);
	testUnsupportedBase("1", 1);
	testUnsupportedBase("1", 37);
}

TEST_F(TestConvertUnsignedInt, unsupportedBase)
{
	testUnsupportedBase("1", -1);
	testUnsupportedBase("1", 1);
	testUnsupportedBase("1", 37);
}

TEST_F(TestConvertLong, unsupportedBase)
{
	testUnsupportedBase("1", -1);
	testUnsupportedBase("1", 1);
	testUnsupportedBase("1", 37);
}

TEST_F(TestConvertUnsignedLong, unsupportedBase)
{
	testUnsupportedBase("1", -1);
	testUnsupportedBase("1", 1);
	testUnsupportedBase("1", 37);
}

TEST_F(TestConvertShort, range)
{
	testOverflow("18446744073709551616");
	testOverflow("18446744073709551615");
	testOverflow("9223372036854775809");
	testOverflow("9223372036854775808");
	testOverflow("9223372036854775807");
	testOverflow("4294967296");
	testOverflow("4294967295");
	testOverflow("2147483649");
	testOverflow("2147483648");
	testOverflow("2147483647");
	testOverflow("65536");
	testOverflow("65535");
	testOverflow("32769");
	testOverflow("32768");

	testValid("32767", 32767, 5);
	testValid("1", 1, 1);
	testValid("0", 0, 1);
	testValid("-0", 0, 2);
	testValid("-1", -1, 2);
	testValid("-32767", -32767, 6);
	testValid("-32768", -32768, 6);

	testUnderflow("-32769");
	testUnderflow("-65535");
	testUnderflow("-65536");
	testUnderflow("-2147483647");
	testUnderflow("-2147483648");
	testUnderflow("-2147483649");
	testUnderflow("-4294967295");
	testUnderflow("-4294967296");
	testUnderflow("-9223372036854775807");
	testUnderflow("-9223372036854775808");
	testUnderflow("-9223372036854775809");
	testUnderflow("-18446744073709551615");
	testUnderflow("-18446744073709551616");
}

TEST_F(TestConvertUnsignedShort, range)
{
	testOverflow("18446744073709551616");
	testOverflow("18446744073709551615");
	testOverflow("9223372036854775809");
	testOverflow("9223372036854775808");
	testOverflow("9223372036854775807");
	testOverflow("4294967296");
	testOverflow("4294967295");
	testOverflow("2147483649");
	testOverflow("2147483648");
	testOverflow("2147483647");
	testOverflow("65536");

	testValid("65535", 65535, 5);
	testValid("32769", 32769, 5);
	testValid("32768", 32768, 5);
	testValid("32767", 32767, 5);
	testValid("1", 1, 1);
	testValid("0", 0, 1);

	testInvalidSign("-0");
	testInvalidSign("-1");
	testInvalidSign("-32767");
	testInvalidSign("-32768");
	testInvalidSign("-32769");
	testInvalidSign("-65535");
	testInvalidSign("-65536");
	testInvalidSign("-2147483647");
	testInvalidSign("-2147483648");
	testInvalidSign("-2147483649");
	testInvalidSign("-4294967295");
	testInvalidSign("-4294967296");
	testInvalidSign("-9223372036854775807");
	testInvalidSign("-9223372036854775808");
	testInvalidSign("-9223372036854775809");
	testInvalidSign("-18446744073709551615");
	testInvalidSign("-18446744073709551616");
}

TEST_F(TestConvertInt, range)
{
	testOverflow("18446744073709551616");
	testOverflow("18446744073709551615");
	testOverflow("9223372036854775809");
	testOverflow("9223372036854775808");
	testOverflow("9223372036854775807");
	testOverflow("4294967296");
	testOverflow("4294967295");
	testOverflow("2147483649");
	testOverflow("2147483648");

	testValid("2147483647", 2147483647, 10);
	testValid("1", 1, 1);
	testValid("0", 0, 1);
	testValid("-0", 0, 2);
	testValid("-1", -1, 2);
	testValid("-2147483647", -2147483647, 11);
	testValid("-2147483648", -2147483648, 11);

	testUnderflow("-2147483649");
	testUnderflow("-4294967295");
	testUnderflow("-4294967296");
	testUnderflow("-9223372036854775807");
	testUnderflow("-9223372036854775808");
	testUnderflow("-9223372036854775809");
	testUnderflow("-18446744073709551615");
	testUnderflow("-18446744073709551616");
}

TEST_F(TestConvertUnsignedInt, range)
{
	testOverflow("18446744073709551616");
	testOverflow("18446744073709551615");
	testOverflow("9223372036854775809");
	testOverflow("9223372036854775808");
	testOverflow("9223372036854775807");
	testOverflow("4294967296");

	testValid("4294967295", 4294967295, 10);
	testValid("2147483649", 2147483649, 10);
	testValid("2147483648", 2147483648, 10);
	testValid("2147483647", 2147483647, 10);
	testValid("1", 1, 1);
	testValid("0", 0, 1);

	testInvalidSign("-0");
	testInvalidSign("-1");
	testInvalidSign("-2147483647");
	testInvalidSign("-2147483648");
	testInvalidSign("-2147483649");
	testInvalidSign("-4294967295");
	testInvalidSign("-4294967296");
	testInvalidSign("-9223372036854775807");
	testInvalidSign("-9223372036854775808");
	testInvalidSign("-9223372036854775809");
	testInvalidSign("-18446744073709551615");
	testInvalidSign("-18446744073709551616");
}

TEST_F(TestConvertLong, range)
{
	testOverflow("18446744073709551616");
	testOverflow("18446744073709551615");
	testOverflow("9223372036854775809");
	testOverflow("9223372036854775808");

	testValid("9223372036854775807", 9223372036854775807, 19);
	testValid("4294967296", 4294967296, 10);
	testValid("4294967295", 4294967295, 10);
	testValid("2147483649", 2147483649, 10);
	testValid("2147483648", 2147483648, 10);
	testValid("2147483647", 2147483647, 10);
	testValid("1", 1, 1);
	testValid("0", 0, 1);
	testValid("-0", 0, 2);
	testValid("-1", -1, 2);
	testValid("-2147483647", -2147483647, 11);
	testValid("-2147483648", -2147483648, 11);
	testValid("-2147483649", -2147483649, 11);
	testValid("-4294967295", -4294967295, 11);
	testValid("-4294967296", -4294967296, 11);
	testValid("-9223372036854775807", -9223372036854775807, 20);
	testValid("-9223372036854775808", -9223372036854775808u, 20);

	testUnderflow("-9223372036854775809");
	testUnderflow("-18446744073709551615");
	testUnderflow("-18446744073709551616");
}

TEST_F(TestConvertUnsignedLong, range)
{
	testOverflow("18446744073709551616");

	testValid("18446744073709551615", 18446744073709551615u, 20);
	testValid("9223372036854775809", 9223372036854775809u, 19);
	testValid("9223372036854775808", 9223372036854775808u, 19);
	testValid("9223372036854775807", 9223372036854775807, 19);
	testValid("4294967296", 4294967296, 10);
	testValid("4294967295", 4294967295, 10);
	testValid("2147483649", 2147483649, 10);
	testValid("2147483648", 2147483648, 10);
	testValid("2147483647", 2147483647, 10);
	testValid("1", 1, 1);
	testValid("0", 0, 1);

	testInvalidSign("-0");
	testInvalidSign("-1");
	testInvalidSign("-2147483647");
	testInvalidSign("-2147483648");
	testInvalidSign("-2147483649");
	testInvalidSign("-4294967295");
	testInvalidSign("-4294967296");
	testInvalidSign("-9223372036854775807");
	testInvalidSign("-9223372036854775808");
	testInvalidSign("-9223372036854775809");
	testInvalidSign("-18446744073709551615");
	testInvalidSign("-18446744073709551616");
}
