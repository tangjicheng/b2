#include <limits>
#include <new>

#include <gtest/gtest.h>

#include <misc/ForwardMemoryPool.h>
#include <misc/PoolAllocator.h>

namespace {

class X {
public:
	explicit X(int x) { m_x += x; }

	~X() { m_x -= m_x; }

	static int x() { return m_x; }

private:
	static int m_x;
};

int X::m_x = 0;

} // namespace

class TestPoolAllocator : public ::testing::Test {
protected:
	char data[12 * sizeof(int)];
	misc::ForwardMemoryPool pool {data, sizeof(data)};
	misc::PoolAllocator<int, misc::ForwardMemoryPool> alloc {pool};
	int* ptr {nullptr};

	void SetUp() { ptr = alloc.allocate(6); }
};

TEST_F(TestPoolAllocator, copyConstructor)
{
	misc::PoolAllocator<int, misc::ForwardMemoryPool> other(alloc);

	EXPECT_EQ(ptr + 6, other.allocate(2));
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(8 * sizeof(int), pool.used());
	EXPECT_EQ(8 * (int)sizeof(int), pool.balance());

	EXPECT_EQ(ptr + 8, alloc.allocate(2));
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(10 * sizeof(int), pool.used());
	EXPECT_EQ(10 * (int)sizeof(int), pool.balance());

	EXPECT_EQ(ptr + 10, other.allocate(2));
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(12 * (int)sizeof(int), pool.balance());

	alloc.deallocate(ptr, 6);
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(6 * (int)sizeof(int), pool.balance());

	other.deallocate(ptr + 6, 2);
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(4 * (int)sizeof(int), pool.balance());

	alloc.deallocate(ptr + 8, 2);
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(2 * (int)sizeof(int), pool.balance());

	other.deallocate(ptr + 10, 2);
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(0 * (int)sizeof(int), pool.balance());
}

TEST_F(TestPoolAllocator, address)
{
	int i = 1;
	EXPECT_EQ(&i, alloc.address(i));

	const int ci = 2;
	EXPECT_EQ(&ci, alloc.address(ci));
}

TEST_F(TestPoolAllocator, allocate)
{
	EXPECT_EQ(ptr, reinterpret_cast<int*>(data));
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(6 * sizeof(int), pool.used());
	EXPECT_EQ(6 * (int)sizeof(int), pool.balance());

	EXPECT_EQ(ptr + 6, alloc.allocate(4));
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(10 * sizeof(int), pool.used());
	EXPECT_EQ(10 * (int)sizeof(int), pool.balance());

	EXPECT_EQ(ptr + 10, alloc.allocate(2));
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(12 * (int)sizeof(int), pool.balance());

	EXPECT_THROW(alloc.allocate(1), std::bad_alloc);
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(12 * (int)sizeof(int), pool.balance());
}

TEST_F(TestPoolAllocator, deallocate)
{
	EXPECT_EQ(ptr + 6, alloc.allocate(2));
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(8 * sizeof(int), pool.used());
	EXPECT_EQ(8 * (int)sizeof(int), pool.balance());

	alloc.deallocate(ptr, 6);
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(8 * sizeof(int), pool.used());
	EXPECT_EQ(2 * (int)sizeof(int), pool.balance());

	alloc.deallocate(ptr + 6, 2);
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(8 * sizeof(int), pool.used());
	EXPECT_EQ(0 * (int)sizeof(int), pool.balance());

	EXPECT_EQ(ptr + 8, alloc.allocate(4));
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(4 * (int)sizeof(int), pool.balance());

	alloc.deallocate(ptr + 8, 4);
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(0 * (int)sizeof(int), pool.balance());

	EXPECT_THROW(alloc.allocate(1), std::bad_alloc);
	EXPECT_EQ(12 * sizeof(int), pool.size());
	EXPECT_EQ(12 * sizeof(int), pool.used());
	EXPECT_EQ(0 * (int)sizeof(int), pool.balance());
}

TEST_F(TestPoolAllocator, maxSize)
{
	EXPECT_EQ(std::numeric_limits<decltype(alloc)::size_type>::max(),
		alloc.max_size());
}

TEST_F(TestPoolAllocator, constructDestroy)
{
	auto x = reinterpret_cast<X*>(ptr);

	EXPECT_EQ(0, X::x());
	alloc.construct(x, 5);
	EXPECT_EQ(5, X::x());
	alloc.destroy(x);
	EXPECT_EQ(0, X::x());
}

TEST_F(TestPoolAllocator, relational)
{
	EXPECT_TRUE(alloc == alloc);
	EXPECT_FALSE(alloc != alloc);

	misc::PoolAllocator<long, misc::ForwardMemoryPool> samePoolAlloc {pool};
	EXPECT_TRUE(alloc == samePoolAlloc);
	EXPECT_FALSE(alloc != samePoolAlloc);

	char otherData[12];
	misc::ForwardMemoryPool otherPool {otherData, sizeof(otherData)};
	misc::PoolAllocator<int, misc::ForwardMemoryPool> otherPoolAlloc {
		otherPool};
	EXPECT_FALSE(alloc == otherPoolAlloc);
	EXPECT_TRUE(alloc != otherPoolAlloc);

	misc::PoolAllocator<int, misc::ForwardMemoryPool> copiedAlloc(alloc);
	EXPECT_TRUE(alloc == copiedAlloc);
	EXPECT_FALSE(alloc != copiedAlloc);

	misc::PoolAllocator<long, misc::ForwardMemoryPool> copiedAllocLong(alloc);
	EXPECT_TRUE(alloc == copiedAllocLong);
	EXPECT_FALSE(alloc != copiedAllocLong);
}
