#include <utility>

#include <gtest/gtest.h>

#include <misc/BinaryMessage.h>

namespace {

using BinaryMessage = misc::BinaryMessage<32>;

} // namespace

TEST(BinaryMessageTest, default_constructor)
{
	BinaryMessage m;

	EXPECT_NE(nullptr, m.data());
	EXPECT_EQ(32u, m.size());
	EXPECT_EQ(false, m.empty());
	EXPECT_EQ(32u, m.maxSize());
}

TEST(BinaryMessageTest, move_constructor)
{
	BinaryMessage m(16);

	EXPECT_NE(nullptr, m.data());
	EXPECT_EQ(16u, m.size());
	EXPECT_EQ(false, m.empty());
	EXPECT_EQ(32u, m.maxSize());

	BinaryMessage n(std::move(m));

	EXPECT_NE(nullptr, n.data());
	EXPECT_EQ(16u, n.size());
	EXPECT_EQ(false, n.empty());
	EXPECT_EQ(32u, n.maxSize());

	EXPECT_EQ(nullptr, m.data());
	EXPECT_EQ(0u, m.size());
	EXPECT_EQ(true, m.empty());
	EXPECT_EQ(32u, m.maxSize());
}

TEST(BinaryMessageTest, move_assignment)
{
	BinaryMessage m(16);

	EXPECT_NE(nullptr, m.data());
	EXPECT_EQ(16u, m.size());
	EXPECT_EQ(false, m.empty());
	EXPECT_EQ(32u, m.maxSize());

	BinaryMessage n;

	EXPECT_NE(nullptr, n.data());
	EXPECT_EQ(32u, n.size());
	EXPECT_EQ(false, n.empty());
	EXPECT_EQ(32u, n.maxSize());

	n = std::move(m);

	EXPECT_NE(nullptr, n.data());
	EXPECT_EQ(16u, n.size());
	EXPECT_EQ(false, n.empty());
	EXPECT_EQ(32u, n.maxSize());

	EXPECT_EQ(nullptr, m.data());
	EXPECT_EQ(0u, m.size());
	EXPECT_EQ(true, m.empty());
	EXPECT_EQ(32u, m.maxSize());
}

TEST(BinaryMessageTest, size_constructor)
{
	BinaryMessage m1(0);

	EXPECT_NE(nullptr, m1.data());
	EXPECT_EQ(0u, m1.size());
	EXPECT_EQ(true, m1.empty());
	EXPECT_EQ(32u, m1.maxSize());

	BinaryMessage m2(16);

	EXPECT_NE(nullptr, m2.data());
	EXPECT_EQ(16u, m2.size());
	EXPECT_EQ(false, m2.empty());
	EXPECT_EQ(32u, m2.maxSize());

	BinaryMessage m3(32);

	EXPECT_NE(nullptr, m3.data());
	EXPECT_EQ(32u, m3.size());
	EXPECT_EQ(false, m3.empty());
	EXPECT_EQ(32u, m3.maxSize());
}

TEST(BinaryMessageTest, resize)
{
	BinaryMessage m;

	EXPECT_NE(nullptr, m.data());
	EXPECT_EQ(32u, m.size());
	EXPECT_EQ(false, m.empty());
	EXPECT_EQ(32u, m.maxSize());

	m.resize(0);

	EXPECT_NE(nullptr, m.data());
	EXPECT_EQ(0u, m.size());
	EXPECT_EQ(true, m.empty());
	EXPECT_EQ(32u, m.maxSize());

	m.resize(16);

	EXPECT_NE(nullptr, m.data());
	EXPECT_EQ(16u, m.size());
	EXPECT_EQ(false, m.empty());
	EXPECT_EQ(32u, m.maxSize());

	m.resize(32);

	EXPECT_NE(nullptr, m.data());
	EXPECT_EQ(32u, m.size());
	EXPECT_EQ(false, m.empty());
	EXPECT_EQ(32u, m.maxSize());
}
