#include <gtest/gtest.h>

#include <misc/pow.h>

TEST(pow, struct_implementation)
{
	// Base 0
	{
		constexpr auto value = misc::Pow_v<unsigned int, 0, 0>;
		EXPECT_EQ(1u, value);
	}
	{
		constexpr auto value = misc::Pow_v<unsigned int, 0, 1>;
		EXPECT_EQ(0u, value);
	}
	{
		constexpr auto value = misc::Pow_v<unsigned int, 0, 2>;
		EXPECT_EQ(0u, value);
	}
	// Base 1
	{
		constexpr auto value = misc::Pow_v<unsigned int, 1, 0>;
		EXPECT_EQ(1u, value);
	}
	{
		constexpr auto value = misc::Pow_v<unsigned int, 1, 1>;
		EXPECT_EQ(1u, value);
	}
	{
		constexpr auto value = misc::Pow_v<unsigned int, 1, 2>;
		EXPECT_EQ(1u, value);
	}
	// Base 2
	{
		constexpr auto value = misc::Pow_v<unsigned int, 2, 0>;
		EXPECT_EQ(1u, value);
	}
	{
		constexpr auto value = misc::Pow_v<unsigned int, 2, 1>;
		EXPECT_EQ(2u, value);
	}
	{
		constexpr auto value = misc::Pow_v<unsigned int, 2, 2>;
		EXPECT_EQ(4u, value);
	}
	{
		constexpr auto value = misc::Pow_v<unsigned int, 2, 3>;
		EXPECT_EQ(8u, value);
	}
	{
		constexpr auto value = misc::Pow_v<unsigned int, 2, 4>;
		EXPECT_EQ(16u, value);
	}
	{
		constexpr auto value = misc::Pow_v<unsigned int, 2, 5>;
		EXPECT_EQ(32u, value);
	}
	// Base 8
	{
		constexpr auto value = misc::Pow_v<unsigned int, 8, 3>;
		EXPECT_EQ(512u, value);
	}
	// Base 10
	{
		constexpr auto value = misc::Pow_v<unsigned int, 10, 3>;
		EXPECT_EQ(1000u, value);
	}
	// Base 16
	{
		constexpr auto value = misc::Pow_v<unsigned int, 16, 3>;
		EXPECT_EQ(4096u, value);
	}
	// Signed base 10
	{
		constexpr auto value = misc::Pow_v<int, 10, 0>;
		EXPECT_EQ(1, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, 10, 1>;
		EXPECT_EQ(10, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, 10, 2>;
		EXPECT_EQ(100, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, 10, 3>;
		EXPECT_EQ(1000, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, 10, 4>;
		EXPECT_EQ(10000, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, 10, 5>;
		EXPECT_EQ(100000, value);
	}
	// Signed base -10
	{
		constexpr auto value = misc::Pow_v<int, -10, 0>;
		EXPECT_EQ(1, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, -10, 1>;
		EXPECT_EQ(-10, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, -10, 2>;
		EXPECT_EQ(100, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, -10, 3>;
		EXPECT_EQ(-1000, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, -10, 4>;
		EXPECT_EQ(10000, value);
	}
	{
		constexpr auto value = misc::Pow_v<int, -10, 5>;
		EXPECT_EQ(-100000, value);
	}
}

TEST(pow, function_implementation)
{
	// Base 0
	{
		constexpr auto value = misc::pow(0u, 0);
		EXPECT_EQ(1u, value);
	}
	{
		constexpr auto value = misc::pow(0u, 1);
		EXPECT_EQ(0u, value);
	}
	{
		constexpr auto value = misc::pow(0u, 2);
		EXPECT_EQ(0u, value);
	}
	// Base 1
	{
		constexpr auto value = misc::pow(1u, 0);
		EXPECT_EQ(1u, value);
	}
	{
		constexpr auto value = misc::pow(1u, 1);
		EXPECT_EQ(1u, value);
	}
	{
		constexpr auto value = misc::pow(1u, 2);
		EXPECT_EQ(1u, value);
	}
	// Base 2
	{
		constexpr auto value = misc::pow(2u, 0);
		EXPECT_EQ(1u, value);
	}
	{
		constexpr auto value = misc::pow(2u, 1);
		EXPECT_EQ(2u, value);
	}
	{
		constexpr auto value = misc::pow(2u, 2);
		EXPECT_EQ(4u, value);
	}
	{
		constexpr auto value = misc::pow(2u, 3);
		EXPECT_EQ(8u, value);
	}
	{
		constexpr auto value = misc::pow(2u, 4);
		EXPECT_EQ(16u, value);
	}
	{
		constexpr auto value = misc::pow(2u, 5);
		EXPECT_EQ(32u, value);
	}
	// Base 8
	{
		constexpr auto value = misc::pow(8u, 3);
		EXPECT_EQ(512u, value);
	}
	// Base 10
	{
		constexpr auto value = misc::pow(10u, 3);
		EXPECT_EQ(1000u, value);
	}
	// Base 16
	{
		constexpr auto value = misc::pow(16u, 3);
		EXPECT_EQ(4096u, value);
	}
	// Signed base 10
	{
		constexpr auto value = misc::pow(10, 0);
		EXPECT_EQ(1, value);
	}
	{
		constexpr auto value = misc::pow(10, 1);
		EXPECT_EQ(10, value);
	}
	{
		constexpr auto value = misc::pow(10, 2);
		EXPECT_EQ(100, value);
	}
	{
		constexpr auto value = misc::pow(10, 3);
		EXPECT_EQ(1000, value);
	}
	{
		constexpr auto value = misc::pow(10, 4);
		EXPECT_EQ(10000, value);
	}
	{
		constexpr auto value = misc::pow(10, 5);
		EXPECT_EQ(100000, value);
	}
	// Signed base -10
	{
		constexpr auto value = misc::pow(-10, 0);
		EXPECT_EQ(1, value);
	}
	{
		constexpr auto value = misc::pow(-10, 1);
		EXPECT_EQ(-10, value);
	}
	{
		constexpr auto value = misc::pow(-10, 2);
		EXPECT_EQ(100, value);
	}
	{
		constexpr auto value = misc::pow(-10, 3);
		EXPECT_EQ(-1000, value);
	}
	{
		constexpr auto value = misc::pow(-10, 4);
		EXPECT_EQ(10000, value);
	}
	{
		constexpr auto value = misc::pow(-10, 5);
		EXPECT_EQ(-100000, value);
	}
}
