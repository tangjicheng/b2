#include <sstream>

#include <gtest/gtest.h>

#include <misc/quote.h>

TEST(TestQuote, empty)
{
	std::ostringstream output;
	output << misc::quote;
	EXPECT_EQ("", output.str());
}

TEST(TestQuote, cstr)
{
	const char* input = "Hello";
	std::ostringstream output;
	output << misc::quote << input;
	EXPECT_EQ("\"Hello\"", output.str());
}

TEST(TestQuote, characterArray)
{
	char input[] = "Hello World";
	std::ostringstream output;
	output << misc::quote << input;
	EXPECT_EQ("\"Hello World\"", output.str());
}

TEST(TestQuote, string)
{
	std::string input = "World";
	std::ostringstream output;
	output << misc::quote << input;
	EXPECT_EQ("\"World\"", output.str());
}

TEST(TestQuote, character)
{
	char input = 'a';
	std::ostringstream output;
	output << misc::quote << input;
	EXPECT_EQ("a", output.str());
}

TEST(TestQuote, integer)
{
	int input = -42;
	std::ostringstream output;
	output << misc::quote << input;
	EXPECT_EQ("-42", output.str());
}

TEST(TestQuote, unsignedInteger)
{
	unsigned int input = 42;
	std::ostringstream output;
	output << misc::quote << input;
	EXPECT_EQ("42", output.str());
}

TEST(TestQuote, negativeFloat)
{
	float input = -3.14;
	std::ostringstream output;
	output << misc::quote << input;
	EXPECT_EQ("-3.14", output.str());
}

TEST(TestQuote, positiveDouble)
{
	double input = 3.14;
	std::ostringstream output;
	output << misc::quote << input;
	EXPECT_EQ("3.14", output.str());
}

TEST(TestQuote, separateStatements)
{
	std::ostringstream output;
	output << misc::quote;
	output << "The End";
	EXPECT_EQ("The End", output.str());
}
