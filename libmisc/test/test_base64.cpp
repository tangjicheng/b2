#include <cstdint>
#include <cstring>
#include <iostream>
#include <string>
#include <tuple>

#include <gtest/gtest.h>

#include <misc/base64.h>

struct Struct {
	uint64_t bigNumber;
	uint32_t smallNumber1;
	uint32_t smallNumber2;
};

bool operator==(const Struct& lhs, const Struct& rhs)
{
	return std::tie(lhs.bigNumber, lhs.smallNumber1, lhs.smallNumber2)
		== std::tie(rhs.bigNumber, rhs.smallNumber1, rhs.smallNumber2);
}

template <typename Object>
void showEncoding(const char* label, const std::string& encoding)
{
	std::cout << label << " (" << sizeof(Object) << " bytes): '" << encoding
			  << "' (" << encoding.size() << " bytes)" << std::endl;
}

template <typename Object>
std::string helpEncode(const Object& obj)
{
	char encodingBuffer[misc::base64::encodedBufferLength(sizeof(obj))];
	size_t encodedLength
		= misc::base64::encode(encodingBuffer, &obj, sizeof(obj));
	EXPECT_TRUE(encodedLength == sizeof(encodingBuffer));
	return std::string(encodingBuffer, encodedLength);
}

template <typename Object>
size_t helpDecode(const std::string& encoding, Object& obj)
{
	EXPECT_TRUE(sizeof(obj)
		== misc::base64::decodedBufferLength(
			&encoding.front(), encoding.size()));
	return misc::base64::decode(&obj, encoding.data(), encoding.size());
}

TEST(Base64, NoTrailingEquals)
{
	// 12 = (3 x 4) bytes => (4 x 4) = 16 bytes (no trailing equals)
	constexpr char inText[] = "EXAMPLETEXT"; // includes termination
	std::string textEncoding(helpEncode(inText));
	//	showEncoding<decltype(inText)>("text", textEncoding);
	char outText[misc::base64::decodedBufferLengthUpperBound(
		misc::base64::encodedBufferLength(sizeof(inText)))];
	try {
		auto bytes = helpDecode(textEncoding, outText);
		ASSERT_EQ(sizeof(inText), bytes);
		ASSERT_TRUE(memcmp(inText, outText, sizeof(inText)) == 0);
	}
	catch (const misc::base64::DecodeCharacterError& e) {
		FAIL() << e.what() << ": " << e.offset() << " (" << e.character()
			   << ")";
	}
}

TEST(Base64, OneTrailingEquals)
{
	// 8 = (3 x 2) + 2 bytes => (4 x 2) + 4 = 12 bytes (1 trailing equals)
	uint64_t inNumber = 0x1234567812345678;
	std::string numberEncoding(helpEncode(inNumber));
	//	showEncoding<uint64_t>("number", numberEncoding);
	uint64_t outNumber;
	try {
		helpDecode(numberEncoding, outNumber);
		ASSERT_TRUE(inNumber == outNumber);
	}
	catch (const misc::base64::DecodeCharacterError& e) {
		FAIL() << e.what() << ": " << e.offset() << " (" << e.character()
			   << ")";
	}
}

TEST(Base64, TwoTrailingEquals)
{
	// 16 = (3 * 5) + 1 bytes => (4 x 5) + 4 = 24 bytes (2 trailing equals)
	Struct inStruct {0x0123456789012345, 0x43218765, 0x87654321};
	std::string structEncoding(helpEncode(inStruct));
	//	showEncoding<Struct>("struct", structEncoding);
	Struct outStruct;
	try {
		helpDecode(structEncoding, outStruct);
		ASSERT_TRUE(inStruct == outStruct);
	}
	catch (const misc::base64::DecodeCharacterError& e) {
		FAIL() << e.what() << ": " << e.offset() << " (" << e.character()
			   << ")";
	}
}
