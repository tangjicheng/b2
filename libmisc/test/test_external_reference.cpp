#include <cstddef>
#include <cstring>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <unordered_map>

#include <gtest/gtest.h>

#include <misc/external_reference.h>

namespace {

constexpr size_t length = 10;

MISC_EXTERNAL_REFERENCE_DECL(External_ref, length)

constexpr char blank[] = "          ";

constexpr char data0[] = "";
constexpr char data1[] = "1";
constexpr char data2[] = "12";
constexpr char data5[] = "12345";
constexpr char data8[] = "12345678";
constexpr char data9[] = "123456789";
constexpr char data10[] = "1234567890";
constexpr char data11[] = "12345678901";
constexpr char data12[] = "123456789012";

constexpr size_t data0_len = sizeof(data0) - 1;
constexpr size_t data1_len = sizeof(data1) - 1;
constexpr size_t data2_len = sizeof(data2) - 1;
constexpr size_t data5_len = sizeof(data5) - 1;
constexpr size_t data8_len = sizeof(data8) - 1;
constexpr size_t data9_len = sizeof(data9) - 1;
constexpr size_t data10_len = sizeof(data10) - 1;
constexpr size_t data11_len = sizeof(data11) - 1;
constexpr size_t data12_len = sizeof(data12) - 1;

constexpr const char* data0_end = data0 + data0_len;
constexpr const char* data1_end = data1 + data1_len;
constexpr const char* data2_end = data2 + data2_len;
constexpr const char* data5_end = data5 + data5_len;
constexpr const char* data8_end = data8 + data8_len;
constexpr const char* data9_end = data9 + data9_len;
constexpr const char* data10_end = data10 + data10_len;
constexpr const char* data11_end = data11 + data11_len;
constexpr const char* data12_end = data12 + data12_len;

constexpr char expected0[length] = {};
constexpr char expected1[length] = {'1'};
constexpr char expected2[length] = {'1', '2'};
constexpr char expected5[length] = {'1', '2', '3', '4', '5'};
constexpr char expected8[length] = {'1', '2', '3', '4', '5', '6', '7', '8'};
constexpr char expected9[length]
	= {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
constexpr char expected10[length]
	= {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};

constexpr size_t expected0_len = 0;
constexpr size_t expected1_len = 1;
constexpr size_t expected2_len = 2;
constexpr size_t expected5_len = 5;
constexpr size_t expected8_len = 8;
constexpr size_t expected9_len = 9;
constexpr size_t expected10_len = 10;

constexpr char blob_empty[length]
	= {'\0', '2', '3', '4', '\0', '6', '7', '\0', '9', '0'};
constexpr char blob_non_empty[length]
	= {'1', '2', '3', '4', '\0', '6', '7', '\0', '9', '0'};

} // namespace

#define EXPECT_EXT_REF_EQ(expected, ref) \
	EXPECT_EQ(0, memcmp(expected, ref.data(), length))

TEST(External_reference, parameters_helper)
{
	EXPECT_TRUE((std::is_same_v<External_ref_tag,
		misc::details::External_reference_parameters<External_ref>::Tag>));

	EXPECT_EQ(10u,
		misc::details::External_reference_parameters<External_ref>::length);
}

TEST(External_reference, default_constructor)
{
	EXPECT_EXT_REF_EQ(expected0, External_ref {});

	constexpr External_ref ref;
	EXPECT_EXT_REF_EQ(expected0, ref);
}

TEST(External_reference, view_constructor)
{
	EXPECT_EXT_REF_EQ(expected0, (External_ref {data0, data0_len}));
	EXPECT_EXT_REF_EQ(expected1, (External_ref {data1, data1_len}));
	EXPECT_EXT_REF_EQ(expected2, (External_ref {data2, data2_len}));
	EXPECT_EXT_REF_EQ(expected5, (External_ref {data5, data5_len}));
	EXPECT_EXT_REF_EQ(expected8, (External_ref {data8, data8_len}));
	EXPECT_EXT_REF_EQ(expected9, (External_ref {data9, data9_len}));
	EXPECT_EXT_REF_EQ(expected10, (External_ref {data10, data10_len}));

	EXPECT_THROW((External_ref {data11, data11_len}), std::length_error);
	EXPECT_THROW((External_ref {data12, data12_len}), std::length_error);
}

TEST(External_reference, range_constructor)
{
	EXPECT_EXT_REF_EQ(expected0, (External_ref {data0, data0_end}));
	EXPECT_EXT_REF_EQ(expected1, (External_ref {data1, data1_end}));
	EXPECT_EXT_REF_EQ(expected2, (External_ref {data2, data2_end}));
	EXPECT_EXT_REF_EQ(expected5, (External_ref {data5, data5_end}));
	EXPECT_EXT_REF_EQ(expected8, (External_ref {data8, data8_end}));
	EXPECT_EXT_REF_EQ(expected9, (External_ref {data9, data9_end}));
	EXPECT_EXT_REF_EQ(expected10, (External_ref {data10, data10_end}));

	EXPECT_THROW((External_ref {data11, data11_end}), std::length_error);
	EXPECT_THROW((External_ref {data12, data12_end}), std::length_error);
}

TEST(External_reference, c_string_constructor)
{
	EXPECT_EXT_REF_EQ(expected0, External_ref {data0});
	EXPECT_EXT_REF_EQ(expected1, External_ref {data1});
	EXPECT_EXT_REF_EQ(expected2, External_ref {data2});
	EXPECT_EXT_REF_EQ(expected5, External_ref {data5});
	EXPECT_EXT_REF_EQ(expected8, External_ref {data8});
	EXPECT_EXT_REF_EQ(expected9, External_ref {data9});
	EXPECT_EXT_REF_EQ(expected10, External_ref {data10});

	EXPECT_THROW(External_ref {data11}, std::length_error);
	EXPECT_THROW(External_ref {data12}, std::length_error);
}

TEST(External_reference, string_constructor)
{
	EXPECT_EXT_REF_EQ(expected0, External_ref {std::string {data0}});
	EXPECT_EXT_REF_EQ(expected1, External_ref {std::string {data1}});
	EXPECT_EXT_REF_EQ(expected2, External_ref {std::string {data2}});
	EXPECT_EXT_REF_EQ(expected5, External_ref {std::string {data5}});
	EXPECT_EXT_REF_EQ(expected8, External_ref {std::string {data8}});
	EXPECT_EXT_REF_EQ(expected9, External_ref {std::string {data9}});
	EXPECT_EXT_REF_EQ(expected10, External_ref {std::string {data10}});

	EXPECT_THROW(External_ref {std::string {data11}}, std::length_error);
	EXPECT_THROW(External_ref {std::string {data12}}, std::length_error);
}

TEST(External_reference, view_assign)
{
	{
		External_ref ref {blank};
		ref.assign(data0, data0_len);
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data1, data1_len);
		EXPECT_EXT_REF_EQ(expected1, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data2, data2_len);
		EXPECT_EXT_REF_EQ(expected2, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data5, data5_len);
		EXPECT_EXT_REF_EQ(expected5, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data8, data8_len);
		EXPECT_EXT_REF_EQ(expected8, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data9, data9_len);
		EXPECT_EXT_REF_EQ(expected9, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data10, data10_len);
		EXPECT_EXT_REF_EQ(expected10, ref);
	}
	{
		External_ref ref {blank};
		EXPECT_THROW(ref.assign(data11, data11_len), std::length_error);
	}
	{
		External_ref ref {blank};
		EXPECT_THROW(ref.assign(data12, data12_len), std::length_error);
	}
}

TEST(External_reference, range_assign)
{
	{
		External_ref ref {blank};
		ref.assign(data0, data0_end);
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data1, data1_end);
		EXPECT_EXT_REF_EQ(expected1, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data2, data2_end);
		EXPECT_EXT_REF_EQ(expected2, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data5, data5_end);
		EXPECT_EXT_REF_EQ(expected5, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data8, data8_end);
		EXPECT_EXT_REF_EQ(expected8, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data9, data9_end);
		EXPECT_EXT_REF_EQ(expected9, ref);
	}
	{
		External_ref ref {blank};
		ref.assign(data10, data10_end);
		EXPECT_EXT_REF_EQ(expected10, ref);
	}
	{
		External_ref ref {blank};
		EXPECT_THROW(ref.assign(data11, data11_end), std::length_error);
	}
	{
		External_ref ref {blank};
		EXPECT_THROW(ref.assign(data12, data12_end), std::length_error);
	}
}

TEST(External_reference, c_string_assignment)
{
	{
		External_ref ref {blank};
		ref = data0;
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {blank};
		ref = data1;
		EXPECT_EXT_REF_EQ(expected1, ref);
	}
	{
		External_ref ref {blank};
		ref = data2;
		EXPECT_EXT_REF_EQ(expected2, ref);
	}
	{
		External_ref ref {blank};
		ref = data5;
		EXPECT_EXT_REF_EQ(expected5, ref);
	}
	{
		External_ref ref {blank};
		ref = data8;
		EXPECT_EXT_REF_EQ(expected8, ref);
	}
	{
		External_ref ref {blank};
		ref = data9;
		EXPECT_EXT_REF_EQ(expected9, ref);
	}
	{
		External_ref ref {blank};
		ref = data10;
		EXPECT_EXT_REF_EQ(expected10, ref);
	}
	{
		External_ref ref {blank};
		EXPECT_THROW(ref = data11, std::length_error);
	}
	{
		External_ref ref {blank};
		EXPECT_THROW(ref = data12, std::length_error);
	}
}

TEST(External_reference, string_assignment)
{
	{
		External_ref ref {blank};
		ref = std::string {data0};
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {blank};
		ref = std::string {data1};
		EXPECT_EXT_REF_EQ(expected1, ref);
	}
	{
		External_ref ref {blank};
		ref = std::string {data2};
		EXPECT_EXT_REF_EQ(expected2, ref);
	}
	{
		External_ref ref {blank};
		ref = std::string {data5};
		EXPECT_EXT_REF_EQ(expected5, ref);
	}
	{
		External_ref ref {blank};
		ref = std::string {data8};
		EXPECT_EXT_REF_EQ(expected8, ref);
	}
	{
		External_ref ref {blank};
		ref = std::string {data9};
		EXPECT_EXT_REF_EQ(expected9, ref);
	}
	{
		External_ref ref {blank};
		ref = std::string {data10};
		EXPECT_EXT_REF_EQ(expected10, ref);
	}
	{
		External_ref ref {blank};
		EXPECT_THROW(ref = std::string {data11}, std::length_error);
	}
	{
		External_ref ref {blank};
		EXPECT_THROW(ref = std::string {data12}, std::length_error);
	}
}

TEST(External_reference, subscript_operator)
{
	{
		External_ref ref {data0};
		for (size_t i = 0; i != ref.max_size() && ref[i] != 0; ++i)
			EXPECT_EQ(expected0[i], ref[i]);
	}
	{
		External_ref ref {data1};
		for (size_t i = 0; i != ref.max_size() && ref[i] != 0; ++i)
			EXPECT_EQ(expected1[i], ref[i]);
	}
	{
		External_ref ref {data2};
		for (size_t i = 0; i != ref.max_size() && ref[i] != 0; ++i)
			EXPECT_EQ(expected2[i], ref[i]);
	}
	{
		External_ref ref {data5};
		for (size_t i = 0; i != ref.max_size() && ref[i] != 0; ++i)
			EXPECT_EQ(expected5[i], ref[i]);
	}
	{
		External_ref ref {data8};
		for (size_t i = 0; i != ref.max_size() && ref[i] != 0; ++i)
			EXPECT_EQ(expected8[i], ref[i]);
	}
	{
		External_ref ref {data9};
		for (size_t i = 0; i != ref.max_size() && ref[i] != 0; ++i)
			EXPECT_EQ(expected9[i], ref[i]);
	}
	{
		External_ref ref {data10};
		for (size_t i = 0; i != ref.max_size() && ref[i] != 0; ++i)
			EXPECT_EQ(expected10[i], ref[i]);
	}
}

TEST(External_reference, non_const_subscript_operator)
{
	External_ref ref {data5};
	for (size_t i = 0; i != 5; ++i)
		ref[i] = ' ';
	EXPECT_EQ(0, memcmp(blank, ref.data(), 5));
	EXPECT_EQ(0, memcmp(expected5 + 5, ref.data() + 5, 5));
}

TEST(External_reference, non_const_data)
{
	External_ref ref {data5};
	memset(ref.data(), ' ', 5);
	EXPECT_EQ(0, memcmp(blank, ref.data(), 5));
	EXPECT_EQ(0, memcmp(expected5 + 5, ref.data() + 5, 5));
}

TEST(External_reference, size)
{
	EXPECT_EQ(0u, External_ref {}.size());

	EXPECT_EQ(0u, External_ref {data0}.size());
	EXPECT_EQ(1u, External_ref {data1}.size());
	EXPECT_EQ(2u, External_ref {data2}.size());
	EXPECT_EQ(5u, External_ref {data5}.size());
	EXPECT_EQ(8u, External_ref {data8}.size());
	EXPECT_EQ(9u, External_ref {data9}.size());
	EXPECT_EQ(10u, External_ref {data10}.size());

	EXPECT_EQ(0u, (External_ref {blob_empty, length}.size()));
	EXPECT_EQ(4u, (External_ref {blob_non_empty, length}.size()));
}

TEST(External_reference, max_size)
{
	EXPECT_EQ(length, External_ref::max_size());
	constexpr auto max_size = External_ref::max_size();
	EXPECT_EQ(length, max_size);

	EXPECT_EQ(length, sizeof(External_ref));
}

TEST(External_reference, empty)
{
	EXPECT_TRUE(External_ref {}.empty());

	EXPECT_TRUE(External_ref {data0}.empty());
	EXPECT_FALSE(External_ref {data1}.empty());
	EXPECT_FALSE(External_ref {data2}.empty());
	EXPECT_FALSE(External_ref {data5}.empty());
	EXPECT_FALSE(External_ref {data8}.empty());
	EXPECT_FALSE(External_ref {data9}.empty());
	EXPECT_FALSE(External_ref {data10}.empty());

	EXPECT_TRUE((External_ref {blob_empty, length}.empty()));
	EXPECT_FALSE((External_ref {blob_non_empty, length}.empty()));
}

TEST(External_reference, clear)
{
	{
		External_ref ref;
		ref.clear();
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {data0};
		ref.clear();
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {data1};
		ref.clear();
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {data2};
		ref.clear();
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {data5};
		ref.clear();
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {data8};
		ref.clear();
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {data9};
		ref.clear();
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
	{
		External_ref ref {data10};
		ref.clear();
		EXPECT_EXT_REF_EQ(expected0, ref);
	}
}

namespace {

template <typename T>
void test_comparisons(const T& low, const T& high)
{
	EXPECT_TRUE(low == low);
	EXPECT_FALSE(low == high);
	EXPECT_FALSE(high == low);
	EXPECT_TRUE(high == high);

	EXPECT_FALSE(low != low);
	EXPECT_TRUE(low != high);
	EXPECT_TRUE(high != low);
	EXPECT_FALSE(high != high);

	EXPECT_FALSE(low < low);
	EXPECT_TRUE(low < high);
	EXPECT_FALSE(high < low);
	EXPECT_FALSE(high < high);

	EXPECT_FALSE(low > low);
	EXPECT_FALSE(low > high);
	EXPECT_TRUE(high > low);
	EXPECT_FALSE(high > high);

	EXPECT_TRUE(low <= low);
	EXPECT_TRUE(low <= high);
	EXPECT_FALSE(high <= low);
	EXPECT_TRUE(high <= high);

	EXPECT_TRUE(low >= low);
	EXPECT_FALSE(low >= high);
	EXPECT_TRUE(high >= low);
	EXPECT_TRUE(high >= high);
}

} // namespace

TEST(External_reference, comparison_operators)
{
	// Strings of same length, only last character differs (is greater than).
	test_comparisons(External_ref {"1"}, External_ref {"A"});
	test_comparisons(External_ref {"12"}, External_ref {"1A"});
	test_comparisons(External_ref {"123"}, External_ref {"12A"});
	test_comparisons(External_ref {"1234"}, External_ref {"123A"});
	test_comparisons(External_ref {"12345"}, External_ref {"1234A"});
	test_comparisons(External_ref {"123456"}, External_ref {"12345A"});
	test_comparisons(External_ref {"1234567"}, External_ref {"123456A"});
	test_comparisons(External_ref {"12345678"}, External_ref {"1234567A"});
	test_comparisons(External_ref {"123456789"}, External_ref {"12345678A"});
	test_comparisons(External_ref {"1234567890"}, External_ref {"123456789A"});

	// Strings of different lengths, equal characters in overlapping range.
	test_comparisons(External_ref {""}, External_ref {"1"});
	test_comparisons(External_ref {"1"}, External_ref {"12"});
	test_comparisons(External_ref {"12"}, External_ref {"123"});
	test_comparisons(External_ref {"123"}, External_ref {"1234"});
	test_comparisons(External_ref {"1234"}, External_ref {"12345"});
	test_comparisons(External_ref {"12345"}, External_ref {"123456"});
	test_comparisons(External_ref {"123456"}, External_ref {"1234567"});
	test_comparisons(External_ref {"1234567"}, External_ref {"12345678"});
	test_comparisons(External_ref {"12345678"}, External_ref {"123456789"});
	test_comparisons(External_ref {"123456789"}, External_ref {"1234567890"});

	EXPECT_TRUE((External_ref {} == External_ref {blob_empty, length}));
	EXPECT_TRUE(
		(External_ref {"1234"} == External_ref {blob_non_empty, length}));
}

namespace {

template <typename T, typename U>
void test_equal_to(const char* low, const char* high)
{
	EXPECT_TRUE(T {low} == U {low});
	EXPECT_FALSE(T {low} == U {high});
	EXPECT_FALSE(T {high} == U {low});
	EXPECT_TRUE(T {high} == U {high});

	EXPECT_FALSE(T {low} != U {low});
	EXPECT_TRUE(T {low} != U {high});
	EXPECT_TRUE(T {high} != U {low});
	EXPECT_FALSE(T {high} != U {high});
}

template <typename T, typename U>
void test_mixed_comparisons()
{
	// Strings of same length, only last character differs (is greater than).
	test_equal_to<T, U>("1", "A");
	test_equal_to<T, U>("12", "1A");
	test_equal_to<T, U>("123", "12A");
	test_equal_to<T, U>("1234", "123A");
	test_equal_to<T, U>("12345", "1234A");
	test_equal_to<T, U>("123456", "12345A");
	test_equal_to<T, U>("1234567", "123456A");
	test_equal_to<T, U>("12345678", "1234567A");
	test_equal_to<T, U>("123456789", "12345678A");
	test_equal_to<T, U>("1234567890", "123456789A");

	// Strings of different lengths, equal characters in overlapping range.
	test_equal_to<T, U>("", "1");
	test_equal_to<T, U>("1", "12");
	test_equal_to<T, U>("12", "123");
	test_equal_to<T, U>("123", "1234");
	test_equal_to<T, U>("1234", "12345");
	test_equal_to<T, U>("12345", "123456");
	test_equal_to<T, U>("123456", "1234567");
	test_equal_to<T, U>("1234567", "12345678");
	test_equal_to<T, U>("12345678", "123456789");
	test_equal_to<T, U>("123456789", "1234567890");
}

} // namespace

TEST(External_reference, mixed_comparison_operators)
{
	test_mixed_comparisons<External_ref, const char*>();
	test_mixed_comparisons<const char*, External_ref>();

	// Strings of different lengths, equal characters in overlapping range.
	EXPECT_FALSE(External_ref {"1234567890"} == "12345678901");
	EXPECT_TRUE(External_ref {"1234567890"} != "12345678901");

	test_mixed_comparisons<External_ref, std::string>();
	test_mixed_comparisons<std::string, External_ref>();

	// Strings of different lengths, equal characters in overlapping range.
	EXPECT_FALSE(External_ref {"1234567890"} == std::string {"12345678901"});
	EXPECT_TRUE(External_ref {"1234567890"} != std::string {"12345678901"});

	EXPECT_TRUE(("" == External_ref {blob_empty, length}));
	EXPECT_TRUE(("1234" == External_ref {blob_non_empty, length}));

	EXPECT_TRUE((std::string {} == External_ref {blob_empty, length}));
	EXPECT_TRUE(
		(std::string {"1234"} == External_ref {blob_non_empty, length}));
}

TEST(External_reference, to_string)
{
	EXPECT_EQ(std::string {}, to_string(External_ref {}));

	EXPECT_EQ((std::string {expected0, expected0_len}),
		to_string(External_ref {data0}));
	EXPECT_EQ((std::string {expected1, expected1_len}),
		to_string(External_ref {data1}));
	EXPECT_EQ((std::string {expected2, expected2_len}),
		to_string(External_ref {data2}));
	EXPECT_EQ((std::string {expected5, expected5_len}),
		to_string(External_ref {data5}));
	EXPECT_EQ((std::string {expected8, expected8_len}),
		to_string(External_ref {data8}));
	EXPECT_EQ((std::string {expected9, expected9_len}),
		to_string(External_ref {data9}));
	EXPECT_EQ((std::string {expected10, expected10_len}),
		to_string(External_ref {data10}));

	EXPECT_EQ(std::string {}, to_string(External_ref {blob_empty, length}));
	EXPECT_EQ(
		std::string {"1234"}, to_string(External_ref {blob_non_empty, length}));
}

TEST(External_reference, output_stream_operator)
{
	{
		std::ostringstream oss;
		oss << External_ref {};
		EXPECT_EQ(std::string {}, oss.str());
	}
	{
		std::ostringstream oss;
		oss << External_ref {data0};
		EXPECT_EQ((std::string {expected0, expected0_len}), oss.str());
	}
	{
		std::ostringstream oss;
		oss << External_ref {data1};
		EXPECT_EQ((std::string {expected1, expected1_len}), oss.str());
	}
	{
		std::ostringstream oss;
		oss << External_ref {data2};
		EXPECT_EQ((std::string {expected2, expected2_len}), oss.str());
	}
	{
		std::ostringstream oss;
		oss << External_ref {data5};
		EXPECT_EQ((std::string {expected5, expected5_len}), oss.str());
	}
	{
		std::ostringstream oss;
		oss << External_ref {data8};
		EXPECT_EQ((std::string {expected8, expected8_len}), oss.str());
	}
	{
		std::ostringstream oss;
		oss << External_ref {data9};
		EXPECT_EQ((std::string {expected9, expected9_len}), oss.str());
	}
	{
		std::ostringstream oss;
		oss << External_ref {data10};
		EXPECT_EQ((std::string {expected10, expected10_len}), oss.str());
	}
	{
		std::ostringstream oss;
		oss << External_ref {blob_empty, length};
		EXPECT_EQ(std::string {}, oss.str());
	}
	{
		std::ostringstream oss;
		oss << External_ref {blob_non_empty, length};
		EXPECT_EQ(std::string {"1234"}, oss.str());
	}
}

TEST(External_reference, hash)
{
	EXPECT_EQ(std::hash<std::string> {}(std::string {}),
		std::hash<External_ref> {}(External_ref {}));

	EXPECT_EQ(std::hash<std::string> {}(data0),
		std::hash<External_ref> {}(External_ref {data0}));
	EXPECT_EQ(std::hash<std::string> {}(data1),
		std::hash<External_ref> {}(External_ref {data1}));
	EXPECT_EQ(std::hash<std::string> {}(data2),
		std::hash<External_ref> {}(External_ref {data2}));
	EXPECT_EQ(std::hash<std::string> {}(data5),
		std::hash<External_ref> {}(External_ref {data5}));
	EXPECT_EQ(std::hash<std::string> {}(data8),
		std::hash<External_ref> {}(External_ref {data8}));
	EXPECT_EQ(std::hash<std::string> {}(data9),
		std::hash<External_ref> {}(External_ref {data9}));
	EXPECT_EQ(std::hash<std::string> {}(data10),
		std::hash<External_ref> {}(External_ref {data10}));

	EXPECT_EQ(std::hash<std::string> {}(std::string {}),
		std::hash<External_ref> {}(External_ref {blob_empty, length}));
	EXPECT_EQ(std::hash<std::string> {}("1234"),
		std::hash<External_ref> {}(External_ref {blob_non_empty, length}));
}

namespace {

template <typename Map>
void test_map(Map& m)
{
	m.emplace(External_ref {data1}, 10);
	m.emplace(External_ref {data9}, 90);
	m.emplace(External_ref {data5}, 50);

	auto i = m.find(External_ref {data1});
	ASSERT_NE(m.end(), i);
	EXPECT_EQ(10, i->second);

	i = m.find(External_ref {data5});
	ASSERT_NE(m.end(), i);
	EXPECT_EQ(50, i->second);

	i = m.find(External_ref {data9});
	ASSERT_NE(m.end(), i);
	EXPECT_EQ(90, i->second);
}

} // namespace

TEST(External_reference, use_in_std_map)
{
	std::map<External_ref, int> m;
	test_map(m);
}

TEST(External_reference, use_in_std_unordered_map)
{
	std::unordered_map<External_ref, int> m;
	test_map(m);
}
