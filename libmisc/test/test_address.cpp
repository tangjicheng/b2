#include <cstdint>
#include <string>

#include <gtest/gtest.h>

#include <misc/Address.h>

TEST(address, conversions_work)
{
	{
		const auto address = std::string {"127.0.0.1"};
		const auto int_address = std::uint32_t {2130706433u};
		const auto [int_result, valid] = misc::io::ip_address_to_int(address);
		EXPECT_TRUE(valid);
		EXPECT_EQ(int_result, int_address);
		EXPECT_EQ(misc::io::int_to_ip_address(int_address), address);
	}
	{
		const auto address = std::string {"127.0.0.1"};
		const auto port = std::string {"12345"};
		const auto [int_address, valid_address]
			= misc::io::ip_address_to_int(address);
		const auto [int_port, valid_port] = misc::io::ip_port_to_int(port);
		EXPECT_TRUE(valid_address);
		EXPECT_TRUE(valid_port);
		EXPECT_EQ(misc::io::int_to_ip_address(int_address), address);
		EXPECT_EQ(misc::io::int_to_ip_port(int_port), port);
	}
	{
		const auto address = std::string {"256.0.0.1"};
		const auto [int_result, valid] = misc::io::ip_address_to_int(address);
		EXPECT_FALSE(valid);
	}
	{
		const auto address = std::string {"255.0.0.1.0"};
		const auto [int_result, valid] = misc::io::ip_address_to_int(address);
		EXPECT_FALSE(valid);
	}
	{
		const auto address = std::string {"ip_address"};
		const auto [int_result, valid] = misc::io::ip_address_to_int(address);
		EXPECT_FALSE(valid);
	}
	{
		const auto port = std::string {"65536"};
		const auto [int_port, valid] = misc::io::ip_port_to_int(port);
		EXPECT_FALSE(valid);
	}
	{
		const auto port = std::string {"ip_port"};
		const auto [int_port, valid] = misc::io::ip_port_to_int(port);
		EXPECT_FALSE(valid);
	}
}

TEST(address, valid_address)
{
	{
		std::string address {"127.0.0.1"};
		std::string port {"12345"};
		auto result = misc::io::to_address(address + ":" + port);
		EXPECT_TRUE(result);
		EXPECT_EQ(result.value().host(), address);
		EXPECT_EQ(result.value().service(), port);
	}

	{
		std::string address {"abcde"};
		std::string port {"12345"};
		auto result = misc::io::to_address(address + ":" + port);
		EXPECT_TRUE(result);
		EXPECT_EQ(result.value().host(), address);
		EXPECT_EQ(result.value().service(), port);
	}
}

TEST(address, invalid_delimiter)
{
	std::string address {"abcde"};
	std::string port {"12345"};
	auto result = misc::io::to_address(address + port);
	EXPECT_FALSE(result);
}

TEST(address, non_numerical_port)
{
	std::string address {"abcde"};
	std::string port {"fghi"};
	auto result = misc::io::to_address(address + ":" + port);
	EXPECT_TRUE(result);
	EXPECT_EQ(result.value().host(), address);
	EXPECT_EQ(result.value().service(), port);
}
