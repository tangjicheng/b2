#include <cctype>
#include <cstddef>
#include <cstring>
#include <iterator>
#include <vector>

#include <gtest/gtest.h>

#include <misc/range.h>

class TestRange : public ::testing::Test {
protected:
	template <typename Range>
	void read(const Range& output, const char* expected)
	{
		auto len = std::distance(output.begin(), output.end());
		decltype(len) expectedLen = strlen(expected);
		ASSERT_EQ(len, expectedLen);
		size_t idx = 0;
		for (auto i = output.begin(), end = output.end(); i != end; ++i, ++idx)
			EXPECT_EQ(expected[idx], *i);
	}

	template <typename Range>
	void write(const Range& output, const char* expected)
	{
		for (auto& o : output)
			o = toupper(o);
		read(output, expected);
	}

	template <typename T>
	class Iterator : public std::iterator<std::forward_iterator_tag, T> {
	public:
		Iterator()
			: m_ptr(nullptr)
		{
		}
		explicit Iterator(T* ptr)
			: m_ptr(ptr)
		{
		}

		T& operator*() const { return *m_ptr; }
		T* operator->() const { return m_ptr; }

		Iterator& operator++()
		{
			++m_ptr;
			return *this;
		}
		Iterator operator++(int)
		{
			auto tmp = *this;
			++m_ptr;
			return tmp;
		}

		bool operator==(const Iterator& rhs) const
		{
			return m_ptr == rhs.m_ptr;
		}
		bool operator!=(const Iterator& rhs) const
		{
			return m_ptr != rhs.m_ptr;
		}

	private:
		T* m_ptr;
	};
};

TEST_F(TestRange, defaultConstructor)
{
	misc::range<char*> ar;
	EXPECT_EQ(ar.begin(), ar.end());

	misc::range<Iterator<char>> air;
	EXPECT_EQ(air.begin(), air.end());

	misc::range<std::vector<char>::iterator> vr;
	EXPECT_EQ(vr.begin(), vr.end());
}

TEST_F(TestRange, arrayRange)
{
	char a[] = {'a', 'b', 'c', 'd', 'e'};

	misc::range<const char*> cr(a, a + sizeof(a) / sizeof(a[0]));
	read(cr, "abcde");

	misc::range<char*> r(a, a + sizeof(a) / sizeof(a[0]));
	read(r, "abcde");
	write(r, "ABCDE");
}

TEST_F(TestRange, arrayIteratorRange)
{
	char a[] = {'a', 'b', 'c', 'd', 'e'};

	misc::range<Iterator<const char>> cr(a, a + sizeof(a) / sizeof(a[0]));
	read(cr, "abcde");

	misc::range<Iterator<char>> r(a, a + sizeof(a) / sizeof(a[0]));
	read(r, "abcde");
	write(r, "ABCDE");
}

TEST_F(TestRange, vectorRange)
{
	std::vector<char> v = {'a', 'b', 'c', 'd', 'e'};

	misc::range<std::vector<char>::const_iterator> cr(v.begin(), v.end());
	read(cr, "abcde");

	misc::range<std::vector<char>::iterator> r(v.begin(), v.end());
	read(r, "abcde");
	write(r, "ABCDE");
}

TEST_F(TestRange, arrayMakeRange)
{
	char a[] = {'a', 'b', 'c', 'd', 'e'};

	auto cr = misc::make_range(const_cast<const char*>(a),
		const_cast<const char*>(a + sizeof(a) / sizeof(a[0])));
	read(cr, "abcde");

	auto r = misc::make_range(a, a + sizeof(a) / sizeof(a[0]));
	read(r, "abcde");
	write(r, "ABCDE");
}

TEST_F(TestRange, arrayIteratorMakeRange)
{
	char a[] = {'a', 'b', 'c', 'd', 'e'};

	auto cr = misc::make_range(Iterator<const char>(a),
		Iterator<const char>(a + sizeof(a) / sizeof(a[0])));
	read(cr, "abcde");

	auto r = misc::make_range(
		Iterator<char>(a), Iterator<char>(a + sizeof(a) / sizeof(a[0])));
	read(r, "abcde");
	write(r, "ABCDE");
}

TEST_F(TestRange, vectorMakeRange)
{
	std::vector<char> v = {'a', 'b', 'c', 'd', 'e'};

	auto cr = misc::make_range(v.cbegin(), v.cend());
	read(cr, "abcde");

	auto r = misc::make_range(v.begin(), v.end());
	read(r, "abcde");
	write(r, "ABCDE");
}

TEST_F(TestRange, arrayMakeRangeFromContainer)
{
	char a[] = {'a', 'b', 'c', 'd', 'e'};
	const decltype(a)& ca = a;

	auto cr = misc::make_range(ca);
	read(cr, "abcde");

	auto r = misc::make_range(a);
	read(r, "abcde");
	write(r, "ABCDE");
}

TEST_F(TestRange, vectorMakeRangeHelperFromContainer)
{
	std::vector<char> v = {'a', 'b', 'c', 'd', 'e'};
	const decltype(v)& cv = v;

	auto cr = misc::make_range(cv);
	read(cr, "abcde");

	auto r = misc::make_range(v);
	read(r, "abcde");
	write(r, "ABCDE");
}

TEST_F(TestRange, vectorMakeRangeHelperFromTemporary)
{
	read(
		misc::make_range(std::vector<char> {'a', 'b', 'c', 'd', 'e'}), "abcde");
}
