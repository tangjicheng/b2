#include <map>
#include <sstream>
#include <string>
#include <vector>

#include <gtest/gtest.h>

#include <misc/json.h>

TEST(Json, details)
{
	using misc::json::details::IsCharacter;
	ASSERT_TRUE((IsCharacter<char> {}));
	ASSERT_FALSE((IsCharacter<unsigned char> {}));
	ASSERT_FALSE((IsCharacter<signed char> {}));
	ASSERT_FALSE((IsCharacter<int> {}));

	using misc::json::details::IsCString;
	ASSERT_TRUE((IsCString<const char*> {}));
	ASSERT_TRUE((IsCString<char*> {}));
	ASSERT_FALSE((IsCString<char> {}));

	using misc::json::details::IsPair;
	ASSERT_TRUE((IsPair<std::pair<int, int>> {}));
	ASSERT_TRUE((IsPair<std::pair<int, int>&> {}));
	ASSERT_TRUE((IsPair<const std::pair<int, int>> {}));
	ASSERT_TRUE((IsPair<const std::pair<int, int>&> {}));
	ASSERT_FALSE((IsPair<std::string> {}));

	using misc::json::details::IsSequence;
	ASSERT_TRUE((IsSequence<std::vector<int>> {}));
	ASSERT_TRUE((IsSequence<std::string> {}));
	ASSERT_TRUE((IsSequence<const std::string> {}));
	ASSERT_TRUE((IsSequence<int[6]> {}));
	ASSERT_FALSE((IsSequence<std::pair<int, int>> {}));

	using misc::json::details::IsCharacterSequence;
	ASSERT_TRUE((IsCharacterSequence<std::string> {}));
	ASSERT_TRUE((IsCharacterSequence<std::vector<char>> {}));
	ASSERT_TRUE((IsCharacterSequence<char[6]> {}));
	ASSERT_FALSE((IsCharacterSequence<const char*> {}));

	using misc::json::details::IsQuotable;
	ASSERT_TRUE((IsQuotable<std::string> {}));
	ASSERT_TRUE((IsQuotable<char> {}));
	ASSERT_FALSE((IsQuotable<int> {}));
	ASSERT_FALSE((IsQuotable<std::map<std::string, int>> {}));

	using misc::json::ArrayTraits;
	using misc::json::ObjectTraits;
	using misc::json::details::HasContainerTraitsT;
	ASSERT_TRUE(
		(HasContainerTraitsT<std::map<std::string, int>, ObjectTraits> {}));
	ASSERT_TRUE((HasContainerTraitsT<std::vector<std::pair<std::string, int>>,
		ObjectTraits> {}));
	ASSERT_FALSE((HasContainerTraitsT<std::vector<int>, ObjectTraits> {}));
	ASSERT_TRUE((HasContainerTraitsT<std::vector<char>, ArrayTraits> {}));
	ASSERT_TRUE((HasContainerTraitsT<char[6], ArrayTraits> {}));
	ASSERT_FALSE((HasContainerTraitsT<std::map<int, int>, ArrayTraits> {}));
}

TEST(JsonQuote, charArray)
{
	std::ostringstream output;
	char a[] = "Hello";
	output << misc::json::quote << a;
	EXPECT_EQ("\"Hello\"", output.str());
}

TEST(JsonQuote, stringLiteral)
{
	std::ostringstream output;
	output << misc::json::quote << "Hello";
	EXPECT_EQ("\"Hello\"", output.str());
}

TEST(JsonQuote, stdString)
{
	std::ostringstream output;
	output << misc::json::quote << std::string("World");
	EXPECT_EQ("\"World\"", output.str());
}

TEST(JsonQuote, charVector)
{
	std::ostringstream output;
	output << misc::json::quote << std::vector<char>({'1', '2', '3'});
	EXPECT_EQ("\"123\"", output.str());
}

TEST(JsonQuote, charLiteral)
{
	std::ostringstream output;
	output << misc::json::quote << 'a';
	EXPECT_EQ("\"a\"", output.str());
}

TEST(JsonQuote, allASCIIChars)
{
	std::string input;
	for (int i = 0; i != 128; ++i)
		input += char(i);
	std::ostringstream output;
	output << misc::json::quote << input;
	EXPECT_EQ("\"\\u0000\\u0001\\u0002\\u0003\\u0004\\u0005\\u0006\\u0007"
			  "\\b\\t\\n\\u000B\\f\\r\\u000E\\u000F\\u0010\\u0011\\u0012\\u0013"
			  "\\u0014\\u0015\\u0016\\u0017\\u0018\\u0019\\u001A\\u001B\\u001C"
			  "\\u001D\\u001E\\u001F"
			  " !\\\"#$%&'()*+,-./0123456789:;<=>?@"
			  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			  "[\\\\]^_`"
			  "abcdefghijklmnopqrstuvwxyz"
			  "{|}~\\u007F\"",
		output.str());
}

TEST(JsonValue, quotable)
{
	std::ostringstream output;
	{
		misc::json::Array array(output);
		array.entry('c');
		array.entry((unsigned char)117);
		array.entry((signed char)115);
		array.entry("cc"); // ie char[3]
		const char* cc = "cc";
		array.entry(cc);
		array.entry(std::string("ccc")); // ie std::string&&
		const std::string cccc("cccc");
		array.entry(cccc);
		const char& c = cccc[0];
		array.entry(c);
		char ccccc[] = "ccccc";
		array.entry(ccccc);
	}
	EXPECT_EQ("[\n\t\"c\",\n\t117,\n\t115,\n\t\"cc\",\n\t\"cc\",\n\t\"ccc\","
			  "\n\t\"cccc\",\n\t\"c\",\n\t\"ccccc\"\n]",
		output.str());
}

TEST(JsonValue, integral)
{
	std::ostringstream output;
	{
		misc::json::Array array(output);
		array.entry(1);
	}
	EXPECT_EQ("[\n\t1\n]", output.str());
}

TEST(JsonValue, boolean)
{
	std::ostringstream output;
	{
		misc::json::Array array(output);
		array.entry(true);
		array.entry(false);
	}
	EXPECT_EQ("[\n\ttrue,\n\tfalse\n]", output.str());
}

TEST(JsonValue, null)
{
	std::ostringstream output;
	{
		misc::json::Array array(output);
		array.entry(misc::json::null);
	}
	EXPECT_EQ("[\n\tnull\n]", output.str());
}

TEST(JsonValue, stringsInArray)
{
	std::ostringstream output;
	{
		misc::json::Array array(output);
		array.entry(std::string("123"));
		array.entry(std::vector<char>({'1', '2', '3'}));
		array.entry("123");
		array.inject(std::string("123"));
		array.inject(std::vector<char>({'1', '2', '3'}));
		array.inject("12");
	}
	EXPECT_EQ(
		"[\n\t\"123\",\n\t\"123\",\n\t\"123\",\n\t\"1\",\n\t\"2\",\n\t\"3\","
		"\n\t\"1\",\n\t\"2\",\n\t\"3\",\n\t\"1\",\n\t\"2\",\n\t\"\\u0000\"\n]",
		output.str());
}

TEST(JsonValue, objectInArray)
{
	std::ostringstream output;
	{
		misc::json::Array array(output);
		array.entry(std::map<std::string, int> {});
	}
	EXPECT_EQ("[\n\t{}\n]", output.str());
}

TEST(JsonValue, arrayInArray)
{
	std::ostringstream output;
	{
		misc::json::Array array(output);
		array.entry(std::vector<int> {});
	}
	EXPECT_EQ("[\n\t[]\n]", output.str());
}

TEST(JsonContainer, indent)
{
	std::ostringstream output;
	{
		misc::json::Array array(output);
		array.entry(std::vector<std::vector<std::vector<int>>> {{}});
	}
	EXPECT_EQ("[\n\t[\n\t\t[]\n\t]\n]", output.str());
}

TEST(JsonArray, empty)
{
	std::ostringstream output;
	misc::json::Array(output).inject(std::vector<int> {});
	EXPECT_EQ("[]", output.str());
}

TEST(JsonArray, oneElement)
{
	std::ostringstream output;
	misc::json::Array(output).inject(std::vector<int> {1});
	EXPECT_EQ("[\n\t1\n]", output.str());
}

TEST(JsonArray, threeElements)
{
	std::ostringstream output;
	misc::json::Array(output).inject(std::vector<int> {1, 2, 3});
	EXPECT_EQ("[\n\t1,\n\t2,\n\t3\n]", output.str());
}

TEST(JsonArray, nesting)
{
	std::ostringstream output;
	misc::json::Array(output).inject(
		std::vector<std::vector<int>> {{}, {}, {}});
	EXPECT_EQ("[\n\t[],\n\t[],\n\t[]\n]", output.str());
}

TEST(JsonObject, empty)
{
	std::ostringstream output;
	misc::json::Object(output).inject(std::map<std::string, int> {});
	EXPECT_EQ("{}", output.str());
}

TEST(JsonObject, oneElement)
{
	std::ostringstream output;
	misc::json::Object(output).inject(std::map<std::string, int> {{"1", 1}});
	EXPECT_EQ("{\n\t\"1\": 1\n}", output.str());
}

TEST(JsonObject, threeElements)
{
	std::ostringstream output;
	misc::json::Object(output).inject(
		std::map<std::string, int> {{"1", 1}, {"2", 2}, {"3", 3}});
	EXPECT_EQ("{\n\t\"1\": 1,\n\t\"2\": 2,\n\t\"3\": 3\n}", output.str());
}

TEST(JsonObject, pairVector)
{
	std::ostringstream output;
	misc::json::Object(output).inject(std::vector<std::pair<std::string, int>> {
		{"1", 1}, {"2", 2}, {"3", 3}});
	EXPECT_EQ("{\n\t\"1\": 1,\n\t\"2\": 2,\n\t\"3\": 3\n}", output.str());
}

TEST(JsonObject, nesting)
{
	std::ostringstream output;
	misc::json::Object(output).inject(
		std::map<std::string, std::map<std::string, int>> {
			{"1", {}}, {"2", {}}, {"3", {}}});
	EXPECT_EQ("{\n\t\"1\": {},\n\t\"2\": {},\n\t\"3\": {}\n}", output.str());
}

namespace {

using Custom = std::tuple<int, std::string, std::vector<char>>;

} // namespace

namespace misc { namespace json {

template <>
struct ArrayTraits<Custom> {
	static void inject(Array& a, const Custom& custom)
	{
		a.entry(std::get<0>(custom));
		a.entry(std::get<1>(custom));
		a.inject(std::get<2>(custom));
	}
};

}} // namespace misc::json

TEST(JsonArray, customise)
{
	std::ostringstream output;
	{
		misc::json::Array array(output);
		array.entry(Custom {1, "1", {'1', '1'}});
		array.inject(Custom {2, "2", {'2', '2'}});
	}
	EXPECT_EQ("[\n\t[\n\t\t1,\n\t\t\"1\",\n\t\t\"1\",\n\t\t\"1\"\n\t],\n\t2,"
			  "\n\t\"2\",\n\t\"2\",\n\t\"2\"\n]",
		output.str());
}
