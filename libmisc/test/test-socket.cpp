#include <gtest/gtest.h>

#include <misc/Socket.h>

TEST(TestSocket, Address_family)
{
	using misc::io::Address_family;

	constexpr int unspec = AF_UNSPEC;
	constexpr int inet = AF_INET;
	constexpr int inet6 = AF_INET6;

	EXPECT_EQ(unspec, static_cast<int>(Address_family::unspecified));
	EXPECT_EQ(inet, static_cast<int>(Address_family::ipv4));
	EXPECT_EQ(inet6, static_cast<int>(Address_family::ipv6));
}

TEST(TestSocket, Socket_type)
{
	using misc::io::Socket_type;

	constexpr int unspec = 0;
	constexpr int stream = SOCK_STREAM;
	constexpr int dgram = SOCK_DGRAM;

	EXPECT_EQ(unspec, static_cast<int>(Socket_type::unspecified));
	EXPECT_EQ(stream, static_cast<int>(Socket_type::stream));
	EXPECT_EQ(dgram, static_cast<int>(Socket_type::datagram));
}

TEST(TestSocket, Address_info_flags)
{
	using misc::io::Address_info_flag;
	using misc::io::Address_info_flags;

	constexpr int none = 0;
	constexpr int passive = AI_PASSIVE;
	constexpr int host = AI_NUMERICHOST;
	constexpr int serv = AI_NUMERICSERV;
	constexpr int host_serv = AI_NUMERICHOST | AI_NUMERICSERV;

	EXPECT_EQ(none, static_cast<int>(Address_info_flag::none));
	EXPECT_EQ(passive, static_cast<int>(Address_info_flag::passive));
	EXPECT_EQ(host, static_cast<int>(Address_info_flag::numeric_host));
	EXPECT_EQ(serv, static_cast<int>(Address_info_flag::numeric_service));

	EXPECT_EQ(none, Address_info_flags(Address_info_flag::none).value());
	EXPECT_EQ(passive, Address_info_flags(Address_info_flag::passive).value());
	EXPECT_EQ(
		host, Address_info_flags(Address_info_flag::numeric_host).value());
	EXPECT_EQ(
		serv, Address_info_flags(Address_info_flag::numeric_service).value());

	EXPECT_EQ(host_serv,
		(Address_info_flag::numeric_host | Address_info_flag::numeric_service)
			.value());
	EXPECT_EQ(host_serv,
		(Address_info_flag::numeric_service | Address_info_flag::numeric_host)
			.value());

	EXPECT_EQ(host_serv,
		(Address_info_flag::numeric_host
			| Address_info_flags(Address_info_flag::numeric_service))
			.value());
	EXPECT_EQ(host_serv,
		(Address_info_flag::numeric_service
			| Address_info_flags(Address_info_flag::numeric_host))
			.value());

	EXPECT_EQ(host_serv,
		(Address_info_flags(Address_info_flag::numeric_host)
			| Address_info_flag::numeric_service)
			.value());
	EXPECT_EQ(host_serv,
		(Address_info_flags(Address_info_flag::numeric_service)
			| Address_info_flag::numeric_host)
			.value());

	EXPECT_EQ(host_serv,
		(Address_info_flags(Address_info_flag::numeric_host)
			| Address_info_flags(Address_info_flag::numeric_service))
			.value());
	EXPECT_EQ(host_serv,
		(Address_info_flags(Address_info_flag::numeric_service)
			| Address_info_flags(Address_info_flag::numeric_host))
			.value());
}

TEST(TestSocket, Name_info_flags)
{
	using misc::io::Name_info_flag;
	using misc::io::Name_info_flags;

	constexpr int none = 0;
	constexpr int host = NI_NUMERICHOST;
	constexpr int serv = NI_NUMERICSERV;
	constexpr int host_serv = NI_NUMERICHOST | NI_NUMERICSERV;

	EXPECT_EQ(none, static_cast<int>(Name_info_flag::none));
	EXPECT_EQ(host, static_cast<int>(Name_info_flag::numeric_host));
	EXPECT_EQ(serv, static_cast<int>(Name_info_flag::numeric_service));

	EXPECT_EQ(none, Name_info_flags(Name_info_flag::none).value());
	EXPECT_EQ(host, Name_info_flags(Name_info_flag::numeric_host).value());
	EXPECT_EQ(serv, Name_info_flags(Name_info_flag::numeric_service).value());

	EXPECT_EQ(host_serv,
		(Name_info_flag::numeric_host | Name_info_flag::numeric_service)
			.value());
	EXPECT_EQ(host_serv,
		(Name_info_flag::numeric_service | Name_info_flag::numeric_host)
			.value());

	EXPECT_EQ(host_serv,
		(Name_info_flag::numeric_host
			| Name_info_flags(Name_info_flag::numeric_service))
			.value());
	EXPECT_EQ(host_serv,
		(Name_info_flag::numeric_service
			| Name_info_flags(Name_info_flag::numeric_host))
			.value());

	EXPECT_EQ(host_serv,
		(Name_info_flags(Name_info_flag::numeric_host)
			| Name_info_flag::numeric_service)
			.value());
	EXPECT_EQ(host_serv,
		(Name_info_flags(Name_info_flag::numeric_service)
			| Name_info_flag::numeric_host)
			.value());

	EXPECT_EQ(host_serv,
		(Name_info_flags(Name_info_flag::numeric_host)
			| Name_info_flags(Name_info_flag::numeric_service))
			.value());
	EXPECT_EQ(host_serv,
		(Name_info_flags(Name_info_flag::numeric_service)
			| Name_info_flags(Name_info_flag::numeric_host))
			.value());
}
