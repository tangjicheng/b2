#include <cstdlib>
#include <iostream>
#include <map>
#include <ostream>
#include <stdexcept>

#include <misc/Random.h>

namespace {

void test(misc::UniformRandom& r, int n)
{
	std::map<int, int> m;
	for (int i = 0; i != n; ++i) {
		int j = r();
		++m[j];
		std::cout << j << ' ';
	}
	std::cout << std::endl;
	for (auto i = m.begin(); i != m.end(); ++i)
		std::cout << i->first << ' ' << i->second << std::endl;
	std::cout << r.min() << ' ' << r.max() << ' ' << (r.max() - r.min() + 1)
			  << ' ' << m.size() << std::endl;
}

} // namespace

int main()
{
	try {
		const int n = 1000;
		misc::UniformRandom r(0, 9);
		test(r, n);
		r.seed(5489UL);
		test(r, n);
		r.seed();
		test(r, n);
	}
	catch (const std::invalid_argument& e) {
		std::cout << "Invalid argument: " << e.what() << std::endl;
	}
	return EXIT_SUCCESS;
}
