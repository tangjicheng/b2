#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <misc/fix/Converters.h>

#include "unittest.h"

TEST(fixPositiveFloatConvertFromString, valid)
{
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<0, int>(std::string("0"))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<1, int>(std::string("0"))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<2, int>(std::string("0"))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<3, int>(std::string("0"))));

	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<0, int>(std::string("0.0"))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<1, int>(std::string("0.0"))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<2, int>(std::string("0.0"))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<3, int>(std::string("0.0"))));

	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<0, int>(std::string("0."))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<1, int>(std::string("0."))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<2, int>(std::string("0."))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<3, int>(std::string("0."))));

	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<0, int>(std::string(".0"))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<1, int>(std::string(".0"))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<2, int>(std::string(".0"))));
	EXPECT_EQ(0, (misc::fix::positiveFloatConvert<3, int>(std::string(".0"))));

	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<0, int>(std::string("123"))));
	EXPECT_EQ(
		1230, (misc::fix::positiveFloatConvert<1, int>(std::string("123"))));
	EXPECT_EQ(
		12300, (misc::fix::positiveFloatConvert<2, int>(std::string("123"))));
	EXPECT_EQ(
		123000, (misc::fix::positiveFloatConvert<3, int>(std::string("123"))));

	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<0, int>(std::string("123."))));
	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<1, int>(std::string("12.3"))));
	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<2, int>(std::string("1.23"))));
	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<3, int>(std::string(".123"))));

	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<0, int>(std::string("000123."))));
	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<1, int>(std::string("00012.3"))));
	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<2, int>(std::string("0001.23"))));
	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<3, int>(std::string("000.123"))));

	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<0, int>(std::string("123.000"))));
	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<1, int>(std::string("12.3000"))));
	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<2, int>(std::string("1.23000"))));
	EXPECT_EQ(
		123, (misc::fix::positiveFloatConvert<3, int>(std::string(".123000"))));

	EXPECT_EQ(
		123000, (misc::fix::positiveFloatConvert<6, int>(std::string(".123"))));
	EXPECT_EQ(123000,
		(misc::fix::positiveFloatConvert<6, int>(std::string(".123000"))));
	EXPECT_EQ(123000,
		(misc::fix::positiveFloatConvert<6, int>(std::string(".123000000"))));

	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<0, int>(std::string("999999999."))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<1, int>(std::string("99999999.9"))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<2, int>(std::string("9999999.99"))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<3, int>(std::string("999999.999"))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<4, int>(std::string("99999.9999"))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<5, int>(std::string("9999.99999"))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<6, int>(std::string("999.999999"))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<7, int>(std::string("99.9999999"))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<8, int>(std::string("9.99999999"))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveFloatConvert<9, int>(std::string(".999999999"))));

	EXPECT_EQ(999999999U,
		(misc::fix::positiveFloatConvert<0, unsigned int>(
			std::string("999999999."))));
	EXPECT_EQ(999999999U,
		(misc::fix::positiveFloatConvert<9, unsigned int>(
			std::string(".999999999"))));
	EXPECT_EQ(999999999999999999L,
		(misc::fix::positiveFloatConvert<0, long>(
			std::string("999999999999999999."))));
	EXPECT_EQ(999999999999999999L,
		(misc::fix::positiveFloatConvert<18, long>(
			std::string(".999999999999999999"))));
	EXPECT_EQ(9999999999999999999UL,
		(misc::fix::positiveFloatConvert<0, unsigned long>(
			std::string("9999999999999999999."))));
	EXPECT_EQ(9999999999999999999UL,
		(misc::fix::positiveFloatConvert<19, unsigned long>(
			std::string(".9999999999999999999"))));
}

TEST(fixPositiveFloatConvertFromString, badFormat)
{
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<0, int>(std::string())),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string("."))),
		std::invalid_argument, "Bad format");
}

TEST(fixPositiveFloatConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string("+123"))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string("-123"))),
		std::invalid_argument, "Invalid character");

	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string(" 123"))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string("123 "))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<3, int>(std::string("123 456"))),
		std::invalid_argument, "Invalid character");

	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<3, int>(std::string("123. "))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<3, int>(std::string("123.456 "))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<3, int>(std::string("123.456000 "))),
		std::invalid_argument, "Invalid character");
}

TEST(fixPositiveFloatConvertFromString, outOfRange)
{
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string(".1"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<1, int>(std::string(".01"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<2, int>(std::string(".001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<3, int>(std::string(".0001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<4, int>(std::string(".00001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<5, int>(std::string(".000001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<6, int>(std::string(".0000001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<7, int>(std::string(".00000001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<8, int>(std::string(".000000001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<9, int>(std::string(".0000000001"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string("1000000000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<1, int>(std::string("100000000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<2, int>(std::string("10000000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<3, int>(std::string("1000000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<4, int>(std::string("100000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<5, int>(std::string("10000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<6, int>(std::string("1000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<7, int>(std::string("100."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<8, int>(std::string("10."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<9, int>(std::string("1."))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string("2147483647."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string("2147483648."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<0, int>(std::string("2147483649."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<9, int>(std::string(".2147483647"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<9, int>(std::string(".2147483648"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveFloatConvert<9, int>(std::string(".2147483649"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<0, unsigned int>(
						  std::string("4294967295."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<0, unsigned int>(
						  std::string("4294967296."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<9, unsigned int>(
						  std::string(".4294967295"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<9, unsigned int>(
						  std::string(".4294967296"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<0, long>(
						  std::string("9223372036854775807."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<0, long>(
						  std::string("9223372036854775808."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<0, long>(
						  std::string("9223372036854775809."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<18, long>(
						  std::string(".9223372036854775807"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<18, long>(
						  std::string(".9223372036854775808"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<18, long>(
						  std::string(".9223372036854775809"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<0, unsigned long>(
						  std::string("18446744073709551615."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<0, unsigned long>(
						  std::string("18446744073709551616."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<19, unsigned long>(
						  std::string(".18446744073709551615"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveFloatConvert<19, unsigned long>(
						  std::string(".18446744073709551616"))),
		std::invalid_argument, "Out of range");
}

TEST(fixFloatConvertFromString, validPositive)
{
	EXPECT_EQ(0, (misc::fix::floatConvert<0, int>(std::string("0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<1, int>(std::string("0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<2, int>(std::string("0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<3, int>(std::string("0"))));

	EXPECT_EQ(0, (misc::fix::floatConvert<0, int>(std::string("0.0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<1, int>(std::string("0.0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<2, int>(std::string("0.0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<3, int>(std::string("0.0"))));

	EXPECT_EQ(0, (misc::fix::floatConvert<0, int>(std::string("0."))));
	EXPECT_EQ(0, (misc::fix::floatConvert<1, int>(std::string("0."))));
	EXPECT_EQ(0, (misc::fix::floatConvert<2, int>(std::string("0."))));
	EXPECT_EQ(0, (misc::fix::floatConvert<3, int>(std::string("0."))));

	EXPECT_EQ(0, (misc::fix::floatConvert<0, int>(std::string(".0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<1, int>(std::string(".0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<2, int>(std::string(".0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<3, int>(std::string(".0"))));

	EXPECT_EQ(123, (misc::fix::floatConvert<0, int>(std::string("123"))));
	EXPECT_EQ(1230, (misc::fix::floatConvert<1, int>(std::string("123"))));
	EXPECT_EQ(12300, (misc::fix::floatConvert<2, int>(std::string("123"))));
	EXPECT_EQ(123000, (misc::fix::floatConvert<3, int>(std::string("123"))));

	EXPECT_EQ(123, (misc::fix::floatConvert<0, int>(std::string("123."))));
	EXPECT_EQ(123, (misc::fix::floatConvert<1, int>(std::string("12.3"))));
	EXPECT_EQ(123, (misc::fix::floatConvert<2, int>(std::string("1.23"))));
	EXPECT_EQ(123, (misc::fix::floatConvert<3, int>(std::string(".123"))));

	EXPECT_EQ(123, (misc::fix::floatConvert<0, int>(std::string("000123."))));
	EXPECT_EQ(123, (misc::fix::floatConvert<1, int>(std::string("00012.3"))));
	EXPECT_EQ(123, (misc::fix::floatConvert<2, int>(std::string("0001.23"))));
	EXPECT_EQ(123, (misc::fix::floatConvert<3, int>(std::string("000.123"))));

	EXPECT_EQ(123, (misc::fix::floatConvert<0, int>(std::string("123.000"))));
	EXPECT_EQ(123, (misc::fix::floatConvert<1, int>(std::string("12.3000"))));
	EXPECT_EQ(123, (misc::fix::floatConvert<2, int>(std::string("1.23000"))));
	EXPECT_EQ(123, (misc::fix::floatConvert<3, int>(std::string(".123000"))));

	EXPECT_EQ(123000, (misc::fix::floatConvert<6, int>(std::string(".123"))));
	EXPECT_EQ(
		123000, (misc::fix::floatConvert<6, int>(std::string(".123000"))));
	EXPECT_EQ(
		123000, (misc::fix::floatConvert<6, int>(std::string(".123000000"))));

	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<0, int>(std::string("999999999."))));
	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<1, int>(std::string("99999999.9"))));
	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<2, int>(std::string("9999999.99"))));
	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<3, int>(std::string("999999.999"))));
	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<4, int>(std::string("99999.9999"))));
	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<5, int>(std::string("9999.99999"))));
	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<6, int>(std::string("999.999999"))));
	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<7, int>(std::string("99.9999999"))));
	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<8, int>(std::string("9.99999999"))));
	EXPECT_EQ(999999999,
		(misc::fix::floatConvert<9, int>(std::string(".999999999"))));

	EXPECT_EQ(999999999999999999L,
		(misc::fix::floatConvert<0, long>(std::string("999999999999999999."))));
	EXPECT_EQ(999999999999999999L,
		(misc::fix::floatConvert<18, long>(
			std::string(".999999999999999999"))));
}

TEST(fixFloatConvertFromString, validNegative)
{
	EXPECT_EQ(0, (misc::fix::floatConvert<0, int>(std::string("-0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<1, int>(std::string("-0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<2, int>(std::string("-0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<3, int>(std::string("-0"))));

	EXPECT_EQ(0, (misc::fix::floatConvert<0, int>(std::string("-0.0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<1, int>(std::string("-0.0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<2, int>(std::string("-0.0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<3, int>(std::string("-0.0"))));

	EXPECT_EQ(0, (misc::fix::floatConvert<0, int>(std::string("-0."))));
	EXPECT_EQ(0, (misc::fix::floatConvert<1, int>(std::string("-0."))));
	EXPECT_EQ(0, (misc::fix::floatConvert<2, int>(std::string("-0."))));
	EXPECT_EQ(0, (misc::fix::floatConvert<3, int>(std::string("-0."))));

	EXPECT_EQ(0, (misc::fix::floatConvert<0, int>(std::string("-.0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<1, int>(std::string("-.0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<2, int>(std::string("-.0"))));
	EXPECT_EQ(0, (misc::fix::floatConvert<3, int>(std::string("-.0"))));

	EXPECT_EQ(-123, (misc::fix::floatConvert<0, int>(std::string("-123"))));
	EXPECT_EQ(-1230, (misc::fix::floatConvert<1, int>(std::string("-123"))));
	EXPECT_EQ(-12300, (misc::fix::floatConvert<2, int>(std::string("-123"))));
	EXPECT_EQ(-123000, (misc::fix::floatConvert<3, int>(std::string("-123"))));

	EXPECT_EQ(-123, (misc::fix::floatConvert<0, int>(std::string("-123."))));
	EXPECT_EQ(-123, (misc::fix::floatConvert<1, int>(std::string("-12.3"))));
	EXPECT_EQ(-123, (misc::fix::floatConvert<2, int>(std::string("-1.23"))));
	EXPECT_EQ(-123, (misc::fix::floatConvert<3, int>(std::string("-.123"))));

	EXPECT_EQ(-123, (misc::fix::floatConvert<0, int>(std::string("-000123."))));
	EXPECT_EQ(-123, (misc::fix::floatConvert<1, int>(std::string("-00012.3"))));
	EXPECT_EQ(-123, (misc::fix::floatConvert<2, int>(std::string("-0001.23"))));
	EXPECT_EQ(-123, (misc::fix::floatConvert<3, int>(std::string("-000.123"))));

	EXPECT_EQ(-123, (misc::fix::floatConvert<0, int>(std::string("-123.000"))));
	EXPECT_EQ(-123, (misc::fix::floatConvert<1, int>(std::string("-12.3000"))));
	EXPECT_EQ(-123, (misc::fix::floatConvert<2, int>(std::string("-1.23000"))));
	EXPECT_EQ(-123, (misc::fix::floatConvert<3, int>(std::string("-.123000"))));

	EXPECT_EQ(-123000, (misc::fix::floatConvert<6, int>(std::string("-.123"))));
	EXPECT_EQ(
		-123000, (misc::fix::floatConvert<6, int>(std::string("-.123000"))));
	EXPECT_EQ(
		-123000, (misc::fix::floatConvert<6, int>(std::string("-.123000000"))));

	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<0, int>(std::string("-999999999."))));
	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<1, int>(std::string("-99999999.9"))));
	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<2, int>(std::string("-9999999.99"))));
	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<3, int>(std::string("-999999.999"))));
	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<4, int>(std::string("-99999.9999"))));
	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<5, int>(std::string("-9999.99999"))));
	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<6, int>(std::string("-999.999999"))));
	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<7, int>(std::string("-99.9999999"))));
	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<8, int>(std::string("-9.99999999"))));
	EXPECT_EQ(-999999999,
		(misc::fix::floatConvert<9, int>(std::string("-.999999999"))));

	EXPECT_EQ(-999999999999999999L,
		(misc::fix::floatConvert<0, long>(
			std::string("-999999999999999999."))));
	EXPECT_EQ(-999999999999999999L,
		(misc::fix::floatConvert<18, long>(
			std::string("-.999999999999999999"))));
}

TEST(fixFloatConvertFromString, badFormat)
{
	EXPECT_THROW_WHAT((misc::fix::floatConvert<0, int>(std::string("-"))),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<0, int>(std::string("-."))),
		std::invalid_argument, "Bad format");
}

TEST(fixFloatConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT((misc::fix::floatConvert<0, int>(std::string("+123"))),
		std::invalid_argument, "Invalid character");

	EXPECT_THROW_WHAT((misc::fix::floatConvert<0, int>(std::string(" -123"))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<0, int>(std::string("-123 "))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<3, int>(std::string("-123 456"))),
		std::invalid_argument, "Invalid character");

	EXPECT_THROW_WHAT((misc::fix::floatConvert<3, int>(std::string("-123. "))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<3, int>(std::string("-123.456 "))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<3, int>(std::string("-123.456000 "))),
		std::invalid_argument, "Invalid character");
}

TEST(fixFloatConvertFromString, outOfRange)
{
	EXPECT_THROW_WHAT((misc::fix::floatConvert<0, int>(std::string("-.1"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<1, int>(std::string("-.01"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<2, int>(std::string("-.001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<3, int>(std::string("-.0001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<4, int>(std::string("-.00001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<5, int>(std::string("-.000001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<6, int>(std::string("-.0000001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<7, int>(std::string("-.00000001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<8, int>(std::string("-.000000001"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<9, int>(std::string("-.0000000001"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<0, int>(std::string("-1000000000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<1, int>(std::string("-100000000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<2, int>(std::string("-10000000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<3, int>(std::string("-1000000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<4, int>(std::string("-100000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<5, int>(std::string("-10000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<6, int>(std::string("-1000."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<7, int>(std::string("-100."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<8, int>(std::string("-10."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<9, int>(std::string("-1."))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<0, int>(std::string("-2147483647."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<0, int>(std::string("-2147483648."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<0, int>(std::string("-2147483649."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<9, int>(std::string("-.2147483647"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<9, int>(std::string("-.2147483648"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::floatConvert<9, int>(std::string("-.2147483649"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT((misc::fix::floatConvert<0, long>(
						  std::string("-9223372036854775807."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<0, long>(
						  std::string("-9223372036854775808."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<0, long>(
						  std::string("-9223372036854775809."))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<18, long>(
						  std::string("-.9223372036854775807"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<18, long>(
						  std::string("-.9223372036854775808"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::floatConvert<18, long>(
						  std::string("-.9223372036854775809"))),
		std::invalid_argument, "Out of range");
}

TEST(fixPositiveFloatConvertFromStringToDouble, valid)
{
	EXPECT_EQ(0.0,
		(misc::fix::positiveFloatConvert<1, int, double>(std::string("0"))));
	EXPECT_EQ(0.0,
		(misc::fix::positiveFloatConvert<1, int, double>(std::string("0.0"))));
	EXPECT_EQ(0.0,
		(misc::fix::positiveFloatConvert<1, int, double>(std::string("0."))));
	EXPECT_EQ(0.0,
		(misc::fix::positiveFloatConvert<1, int, double>(std::string(".0"))));

	EXPECT_EQ(123.0,
		(misc::fix::positiveFloatConvert<0, int, double>(std::string("123."))));
	EXPECT_EQ(12.3,
		(misc::fix::positiveFloatConvert<1, int, double>(std::string("12.3"))));
	EXPECT_EQ(1.23,
		(misc::fix::positiveFloatConvert<2, int, double>(std::string("1.23"))));
	EXPECT_EQ(0.123,
		(misc::fix::positiveFloatConvert<3, int, double>(std::string(".123"))));

	EXPECT_EQ(999999999.0,
		(misc::fix::positiveFloatConvert<0, int, double>(
			std::string("999999999."))));
	EXPECT_EQ(0.999999999,
		(misc::fix::positiveFloatConvert<9, int, double>(
			std::string(".999999999"))));
}

TEST(fixFloatConvertFromStringToDouble, validPositive)
{
	EXPECT_EQ(0.0, (misc::fix::floatConvert<1, int, double>(std::string("0"))));
	EXPECT_EQ(
		0.0, (misc::fix::floatConvert<1, int, double>(std::string("0.0"))));
	EXPECT_EQ(
		0.0, (misc::fix::floatConvert<1, int, double>(std::string("0."))));
	EXPECT_EQ(
		0.0, (misc::fix::floatConvert<1, int, double>(std::string(".0"))));

	EXPECT_EQ(
		123.0, (misc::fix::floatConvert<0, int, double>(std::string("123."))));
	EXPECT_EQ(
		12.3, (misc::fix::floatConvert<1, int, double>(std::string("12.3"))));
	EXPECT_EQ(
		1.23, (misc::fix::floatConvert<2, int, double>(std::string("1.23"))));
	EXPECT_EQ(
		0.123, (misc::fix::floatConvert<3, int, double>(std::string(".123"))));

	EXPECT_EQ(999999999.0,
		(misc::fix::floatConvert<0, int, double>(std::string("999999999."))));
	EXPECT_EQ(0.999999999,
		(misc::fix::floatConvert<9, int, double>(std::string(".999999999"))));
}

TEST(fixFloatConvertFromStringToDouble, validNegative)
{
	EXPECT_EQ(
		0.0, (misc::fix::floatConvert<1, int, double>(std::string("-0"))));
	EXPECT_EQ(
		0.0, (misc::fix::floatConvert<1, int, double>(std::string("-0.0"))));
	EXPECT_EQ(
		0.0, (misc::fix::floatConvert<1, int, double>(std::string("-0."))));
	EXPECT_EQ(
		0.0, (misc::fix::floatConvert<1, int, double>(std::string("-.0"))));

	EXPECT_EQ(-123.0,
		(misc::fix::floatConvert<0, int, double>(std::string("-123."))));
	EXPECT_EQ(
		-12.3, (misc::fix::floatConvert<1, int, double>(std::string("-12.3"))));
	EXPECT_EQ(
		-1.23, (misc::fix::floatConvert<2, int, double>(std::string("-1.23"))));
	EXPECT_EQ(-0.123,
		(misc::fix::floatConvert<3, int, double>(std::string("-.123"))));

	EXPECT_EQ(-999999999.0,
		(misc::fix::floatConvert<0, int, double>(std::string("-999999999."))));
	EXPECT_EQ(-0.999999999,
		(misc::fix::floatConvert<9, int, double>(std::string("-.999999999"))));
}

TEST(fixFloatConvertUnsignedToString, valid)
{
	EXPECT_EQ(
		std::string("0.00000"), (misc::fix::floatConvert<5, unsigned int>(0)));
	EXPECT_EQ(
		std::string("0.00002"), (misc::fix::floatConvert<5, unsigned int>(2)));
	EXPECT_EQ(std::string("0.00200"),
		(misc::fix::floatConvert<5, unsigned int>(200)));
	EXPECT_EQ(std::string("0.20000"),
		(misc::fix::floatConvert<5, unsigned int>(20000)));
	EXPECT_EQ(std::string("0.22222"),
		(misc::fix::floatConvert<5, unsigned int>(22222)));

	EXPECT_EQ(std::string("1.00000"),
		(misc::fix::floatConvert<5, unsigned int>(100000)));
	EXPECT_EQ(std::string("1.00002"),
		(misc::fix::floatConvert<5, unsigned int>(100002)));
	EXPECT_EQ(std::string("1.00200"),
		(misc::fix::floatConvert<5, unsigned int>(100200)));
	EXPECT_EQ(std::string("1.20000"),
		(misc::fix::floatConvert<5, unsigned int>(120000)));
	EXPECT_EQ(std::string("1.22222"),
		(misc::fix::floatConvert<5, unsigned int>(122222)));

	EXPECT_EQ(std::string("1001.00000"),
		(misc::fix::floatConvert<5, unsigned int>(100100000)));
	EXPECT_EQ(std::string("1001.00002"),
		(misc::fix::floatConvert<5, unsigned int>(100100002)));
	EXPECT_EQ(std::string("1001.00200"),
		(misc::fix::floatConvert<5, unsigned int>(100100200)));
	EXPECT_EQ(std::string("1001.20000"),
		(misc::fix::floatConvert<5, unsigned int>(100120000)));
	EXPECT_EQ(std::string("1001.22222"),
		(misc::fix::floatConvert<5, unsigned int>(100122222)));

	EXPECT_EQ(std::string("0"), (misc::fix::floatConvert<0, unsigned int>(0)));
	EXPECT_EQ(
		std::string("123"), (misc::fix::floatConvert<0, unsigned int>(123)));
	EXPECT_EQ(std::string("999999999"),
		(misc::fix::floatConvert<0, unsigned int>(999999999)));
	EXPECT_EQ(std::string("1000000000"),
		(misc::fix::floatConvert<0, unsigned int>(1000000000)));
	EXPECT_EQ(std::string("4294967295"),
		(misc::fix::floatConvert<0, unsigned int>(4294967295)));

	EXPECT_EQ(std::string("0"), (misc::fix::floatConvert<0, unsigned long>(0)));
	EXPECT_EQ(
		std::string("123"), (misc::fix::floatConvert<0, unsigned long>(123)));
	EXPECT_EQ(std::string("9999999999999999999"),
		(misc::fix::floatConvert<0, unsigned long>(9999999999999999999UL)));
	EXPECT_EQ(std::string("10000000000000000000"),
		(misc::fix::floatConvert<0, unsigned long>(10000000000000000000UL)));
	EXPECT_EQ(std::string("18446744073709551615"),
		(misc::fix::floatConvert<0, unsigned long>(18446744073709551615UL)));

	EXPECT_EQ(
		std::string("0.0"), (misc::fix::floatConvert<1, unsigned int>(0)));
	EXPECT_EQ(
		std::string("12.3"), (misc::fix::floatConvert<1, unsigned int>(123)));
	EXPECT_EQ(std::string("99999999.9"),
		(misc::fix::floatConvert<1, unsigned int>(999999999)));
	EXPECT_EQ(std::string("100000000.0"),
		(misc::fix::floatConvert<1, unsigned int>(1000000000)));
	EXPECT_EQ(std::string("429496729.5"),
		(misc::fix::floatConvert<1, unsigned int>(4294967295)));

	EXPECT_EQ(
		std::string("0.0"), (misc::fix::floatConvert<1, unsigned long>(0)));
	EXPECT_EQ(
		std::string("12.3"), (misc::fix::floatConvert<1, unsigned long>(123)));
	EXPECT_EQ(std::string("999999999999999999.9"),
		(misc::fix::floatConvert<1, unsigned long>(9999999999999999999UL)));
	EXPECT_EQ(std::string("1000000000000000000.0"),
		(misc::fix::floatConvert<1, unsigned long>(10000000000000000000UL)));
	EXPECT_EQ(std::string("1844674407370955161.5"),
		(misc::fix::floatConvert<1, unsigned long>(18446744073709551615UL)));

	EXPECT_EQ(
		std::string("0.00"), (misc::fix::floatConvert<2, unsigned int>(0)));
	EXPECT_EQ(
		std::string("1.23"), (misc::fix::floatConvert<2, unsigned int>(123)));
	EXPECT_EQ(std::string("9999999.99"),
		(misc::fix::floatConvert<2, unsigned int>(999999999)));
	EXPECT_EQ(std::string("10000000.00"),
		(misc::fix::floatConvert<2, unsigned int>(1000000000)));
	EXPECT_EQ(std::string("42949672.95"),
		(misc::fix::floatConvert<2, unsigned int>(4294967295)));

	EXPECT_EQ(
		std::string("0.00"), (misc::fix::floatConvert<2, unsigned long>(0)));
	EXPECT_EQ(
		std::string("1.23"), (misc::fix::floatConvert<2, unsigned long>(123)));
	EXPECT_EQ(std::string("99999999999999999.99"),
		(misc::fix::floatConvert<2, unsigned long>(9999999999999999999UL)));
	EXPECT_EQ(std::string("100000000000000000.00"),
		(misc::fix::floatConvert<2, unsigned long>(10000000000000000000UL)));
	EXPECT_EQ(std::string("184467440737095516.15"),
		(misc::fix::floatConvert<2, unsigned long>(18446744073709551615UL)));

	EXPECT_EQ(
		std::string("0.000"), (misc::fix::floatConvert<3, unsigned int>(0)));
	EXPECT_EQ(
		std::string("0.123"), (misc::fix::floatConvert<3, unsigned int>(123)));
	EXPECT_EQ(std::string("999999.999"),
		(misc::fix::floatConvert<3, unsigned int>(999999999)));
	EXPECT_EQ(std::string("1000000.000"),
		(misc::fix::floatConvert<3, unsigned int>(1000000000)));
	EXPECT_EQ(std::string("4294967.295"),
		(misc::fix::floatConvert<3, unsigned int>(4294967295)));

	EXPECT_EQ(
		std::string("0.000"), (misc::fix::floatConvert<3, unsigned long>(0)));
	EXPECT_EQ(
		std::string("0.123"), (misc::fix::floatConvert<3, unsigned long>(123)));
	EXPECT_EQ(std::string("9999999999999999.999"),
		(misc::fix::floatConvert<3, unsigned long>(9999999999999999999UL)));
	EXPECT_EQ(std::string("10000000000000000.000"),
		(misc::fix::floatConvert<3, unsigned long>(10000000000000000000UL)));
	EXPECT_EQ(std::string("18446744073709551.615"),
		(misc::fix::floatConvert<3, unsigned long>(18446744073709551615UL)));
}

TEST(fixFloatConvertSignedToString, valid)
{
	EXPECT_EQ(std::string("0.00000"), (misc::fix::floatConvert<5, int>(0)));
	EXPECT_EQ(std::string("-0.00002"), (misc::fix::floatConvert<5, int>(-2)));
	EXPECT_EQ(std::string("-0.00200"), (misc::fix::floatConvert<5, int>(-200)));
	EXPECT_EQ(
		std::string("-0.20000"), (misc::fix::floatConvert<5, int>(-20000)));
	EXPECT_EQ(
		std::string("-0.22222"), (misc::fix::floatConvert<5, int>(-22222)));

	EXPECT_EQ(
		std::string("-1.00000"), (misc::fix::floatConvert<5, int>(-100000)));
	EXPECT_EQ(
		std::string("-1.00002"), (misc::fix::floatConvert<5, int>(-100002)));
	EXPECT_EQ(
		std::string("-1.00200"), (misc::fix::floatConvert<5, int>(-100200)));
	EXPECT_EQ(
		std::string("-1.20000"), (misc::fix::floatConvert<5, int>(-120000)));
	EXPECT_EQ(
		std::string("-1.22222"), (misc::fix::floatConvert<5, int>(-122222)));

	EXPECT_EQ(std::string("-1001.00000"),
		(misc::fix::floatConvert<5, int>(-100100000)));
	EXPECT_EQ(std::string("-1001.00002"),
		(misc::fix::floatConvert<5, int>(-100100002)));
	EXPECT_EQ(std::string("-1001.00200"),
		(misc::fix::floatConvert<5, int>(-100100200)));
	EXPECT_EQ(std::string("-1001.20000"),
		(misc::fix::floatConvert<5, int>(-100120000)));
	EXPECT_EQ(std::string("-1001.22222"),
		(misc::fix::floatConvert<5, int>(-100122222)));

	EXPECT_EQ(std::string("-2147483648"),
		(misc::fix::floatConvert<0, int>(-2147483648)));
	EXPECT_EQ(std::string("-2147483647"),
		(misc::fix::floatConvert<0, int>(-2147483647)));
	EXPECT_EQ(std::string("-1000000000"),
		(misc::fix::floatConvert<0, int>(-1000000000)));
	EXPECT_EQ(std::string("-999999999"),
		(misc::fix::floatConvert<0, int>(-999999999)));
	EXPECT_EQ(std::string("-123"), (misc::fix::floatConvert<0, int>(-123)));
	EXPECT_EQ(std::string("0"), (misc::fix::floatConvert<0, int>(0)));
	EXPECT_EQ(std::string("123"), (misc::fix::floatConvert<0, int>(123)));
	EXPECT_EQ(
		std::string("999999999"), (misc::fix::floatConvert<0, int>(999999999)));
	EXPECT_EQ(std::string("1000000000"),
		(misc::fix::floatConvert<0, int>(1000000000)));
	EXPECT_EQ(std::string("2147483647"),
		(misc::fix::floatConvert<0, int>(2147483647)));

	EXPECT_EQ(std::string("-9223372036854775808"),
		(misc::fix::floatConvert<0, long>(-9223372036854775808UL)));
	EXPECT_EQ(std::string("-9223372036854775807"),
		(misc::fix::floatConvert<0, long>(-9223372036854775807L)));
	EXPECT_EQ(std::string("-1000000000000000000"),
		(misc::fix::floatConvert<0, long>(-1000000000000000000L)));
	EXPECT_EQ(std::string("-999999999999999999"),
		(misc::fix::floatConvert<0, long>(-999999999999999999L)));
	EXPECT_EQ(std::string("-123"), (misc::fix::floatConvert<0, long>(-123)));
	EXPECT_EQ(std::string("0"), (misc::fix::floatConvert<0, long>(0)));
	EXPECT_EQ(std::string("123"), (misc::fix::floatConvert<0, long>(123)));
	EXPECT_EQ(std::string("999999999999999999"),
		(misc::fix::floatConvert<0, long>(999999999999999999L)));
	EXPECT_EQ(std::string("1000000000000000000"),
		(misc::fix::floatConvert<0, long>(1000000000000000000L)));
	EXPECT_EQ(std::string("9223372036854775807"),
		(misc::fix::floatConvert<0, long>(9223372036854775807L)));

	EXPECT_EQ(std::string("-214748364.8"),
		(misc::fix::floatConvert<1, int>(-2147483648)));
	EXPECT_EQ(std::string("-214748364.7"),
		(misc::fix::floatConvert<1, int>(-2147483647)));
	EXPECT_EQ(std::string("-100000000.0"),
		(misc::fix::floatConvert<1, int>(-1000000000)));
	EXPECT_EQ(std::string("-99999999.9"),
		(misc::fix::floatConvert<1, int>(-999999999)));
	EXPECT_EQ(std::string("-12.3"), (misc::fix::floatConvert<1, int>(-123)));
	EXPECT_EQ(std::string("0.0"), (misc::fix::floatConvert<1, int>(0)));
	EXPECT_EQ(std::string("12.3"), (misc::fix::floatConvert<1, int>(123)));
	EXPECT_EQ(std::string("99999999.9"),
		(misc::fix::floatConvert<1, int>(999999999)));
	EXPECT_EQ(std::string("100000000.0"),
		(misc::fix::floatConvert<1, int>(1000000000)));
	EXPECT_EQ(std::string("214748364.7"),
		(misc::fix::floatConvert<1, int>(2147483647)));

	EXPECT_EQ(std::string("-922337203685477580.8"),
		(misc::fix::floatConvert<1, long>(-9223372036854775808UL)));
	EXPECT_EQ(std::string("-922337203685477580.7"),
		(misc::fix::floatConvert<1, long>(-9223372036854775807L)));
	EXPECT_EQ(std::string("-100000000000000000.0"),
		(misc::fix::floatConvert<1, long>(-1000000000000000000L)));
	EXPECT_EQ(std::string("-99999999999999999.9"),
		(misc::fix::floatConvert<1, long>(-999999999999999999L)));
	EXPECT_EQ(std::string("-12.3"), (misc::fix::floatConvert<1, long>(-123)));
	EXPECT_EQ(std::string("0.0"), (misc::fix::floatConvert<1, long>(0)));
	EXPECT_EQ(std::string("12.3"), (misc::fix::floatConvert<1, long>(123)));
	EXPECT_EQ(std::string("99999999999999999.9"),
		(misc::fix::floatConvert<1, long>(999999999999999999L)));
	EXPECT_EQ(std::string("100000000000000000.0"),
		(misc::fix::floatConvert<1, long>(1000000000000000000L)));
	EXPECT_EQ(std::string("922337203685477580.7"),
		(misc::fix::floatConvert<1, long>(9223372036854775807L)));

	EXPECT_EQ(std::string("-21474836.48"),
		(misc::fix::floatConvert<2, int>(-2147483648)));
	EXPECT_EQ(std::string("-21474836.47"),
		(misc::fix::floatConvert<2, int>(-2147483647)));
	EXPECT_EQ(std::string("-10000000.00"),
		(misc::fix::floatConvert<2, int>(-1000000000)));
	EXPECT_EQ(std::string("-9999999.99"),
		(misc::fix::floatConvert<2, int>(-999999999)));
	EXPECT_EQ(std::string("-1.23"), (misc::fix::floatConvert<2, int>(-123)));
	EXPECT_EQ(std::string("0.00"), (misc::fix::floatConvert<2, int>(0)));
	EXPECT_EQ(std::string("1.23"), (misc::fix::floatConvert<2, int>(123)));
	EXPECT_EQ(std::string("9999999.99"),
		(misc::fix::floatConvert<2, int>(999999999)));
	EXPECT_EQ(std::string("10000000.00"),
		(misc::fix::floatConvert<2, int>(1000000000)));
	EXPECT_EQ(std::string("21474836.47"),
		(misc::fix::floatConvert<2, int>(2147483647)));

	EXPECT_EQ(std::string("-92233720368547758.08"),
		(misc::fix::floatConvert<2, long>(-9223372036854775808UL)));
	EXPECT_EQ(std::string("-92233720368547758.07"),
		(misc::fix::floatConvert<2, long>(-9223372036854775807L)));
	EXPECT_EQ(std::string("-10000000000000000.00"),
		(misc::fix::floatConvert<2, long>(-1000000000000000000L)));
	EXPECT_EQ(std::string("-9999999999999999.99"),
		(misc::fix::floatConvert<2, long>(-999999999999999999L)));
	EXPECT_EQ(std::string("-1.23"), (misc::fix::floatConvert<2, long>(-123)));
	EXPECT_EQ(std::string("0.00"), (misc::fix::floatConvert<2, long>(0)));
	EXPECT_EQ(std::string("1.23"), (misc::fix::floatConvert<2, long>(123)));
	EXPECT_EQ(std::string("9999999999999999.99"),
		(misc::fix::floatConvert<2, long>(999999999999999999L)));
	EXPECT_EQ(std::string("10000000000000000.00"),
		(misc::fix::floatConvert<2, long>(1000000000000000000L)));
	EXPECT_EQ(std::string("92233720368547758.07"),
		(misc::fix::floatConvert<2, long>(9223372036854775807L)));

	EXPECT_EQ(std::string("-2147483.648"),
		(misc::fix::floatConvert<3, int>(-2147483648)));
	EXPECT_EQ(std::string("-2147483.647"),
		(misc::fix::floatConvert<3, int>(-2147483647)));
	EXPECT_EQ(std::string("-1000000.000"),
		(misc::fix::floatConvert<3, int>(-1000000000)));
	EXPECT_EQ(std::string("-999999.999"),
		(misc::fix::floatConvert<3, int>(-999999999)));
	EXPECT_EQ(std::string("-0.123"), (misc::fix::floatConvert<3, int>(-123)));
	EXPECT_EQ(std::string("0.000"), (misc::fix::floatConvert<3, int>(0)));
	EXPECT_EQ(std::string("0.123"), (misc::fix::floatConvert<3, int>(123)));
	EXPECT_EQ(std::string("999999.999"),
		(misc::fix::floatConvert<3, int>(999999999)));
	EXPECT_EQ(std::string("1000000.000"),
		(misc::fix::floatConvert<3, int>(1000000000)));
	EXPECT_EQ(std::string("2147483.647"),
		(misc::fix::floatConvert<3, int>(2147483647)));

	EXPECT_EQ(std::string("-9223372036854775.808"),
		(misc::fix::floatConvert<3, long>(-9223372036854775808UL)));
	EXPECT_EQ(std::string("-9223372036854775.807"),
		(misc::fix::floatConvert<3, long>(-9223372036854775807L)));
	EXPECT_EQ(std::string("-1000000000000000.000"),
		(misc::fix::floatConvert<3, long>(-1000000000000000000L)));
	EXPECT_EQ(std::string("-999999999999999.999"),
		(misc::fix::floatConvert<3, long>(-999999999999999999L)));
	EXPECT_EQ(std::string("-0.123"), (misc::fix::floatConvert<3, long>(-123)));
	EXPECT_EQ(std::string("0.000"), (misc::fix::floatConvert<3, long>(0)));
	EXPECT_EQ(std::string("0.123"), (misc::fix::floatConvert<3, long>(123)));
	EXPECT_EQ(std::string("999999999999999.999"),
		(misc::fix::floatConvert<3, long>(999999999999999999L)));
	EXPECT_EQ(std::string("1000000000000000.000"),
		(misc::fix::floatConvert<3, long>(1000000000000000000L)));
	EXPECT_EQ(std::string("9223372036854775.807"),
		(misc::fix::floatConvert<3, long>(9223372036854775807L)));
}

TEST(fixFloatConvertDoubleToString, valid)
{
	EXPECT_EQ(std::string("0.0"), (misc::fix::floatConvert<1, int, double>(0)));
	EXPECT_EQ(
		std::string("0.0"), (misc::fix::floatConvert<1, int, double>(0.0)));
	EXPECT_EQ(
		std::string("0.0"), (misc::fix::floatConvert<1, int, double>(0.)));
	EXPECT_EQ(
		std::string("0.0"), (misc::fix::floatConvert<1, int, double>(.0)));

	EXPECT_EQ(
		std::string("123"), (misc::fix::floatConvert<0, int, double>(123.0)));
	EXPECT_EQ(
		std::string("12.3"), (misc::fix::floatConvert<1, int, double>(12.3)));
	EXPECT_EQ(
		std::string("1.23"), (misc::fix::floatConvert<2, int, double>(1.23)));
	EXPECT_EQ(
		std::string("0.123"), (misc::fix::floatConvert<3, int, double>(0.123)));

	EXPECT_EQ(std::string("999999999"),
		(misc::fix::floatConvert<0, int, double>(999999999.0)));
	EXPECT_EQ(std::string("0.999999999"),
		(misc::fix::floatConvert<9, int, double>(0.999999999)));

	EXPECT_EQ(
		std::string("0.0"), (misc::fix::floatConvert<1, int, double>(-0)));
	EXPECT_EQ(
		std::string("0.0"), (misc::fix::floatConvert<1, int, double>(-0.0)));
	EXPECT_EQ(
		std::string("0.0"), (misc::fix::floatConvert<1, int, double>(-0.)));
	EXPECT_EQ(
		std::string("0.0"), (misc::fix::floatConvert<1, int, double>(-.0)));

	EXPECT_EQ(
		std::string("-123"), (misc::fix::floatConvert<0, int, double>(-123.0)));
	EXPECT_EQ(
		std::string("-12.3"), (misc::fix::floatConvert<1, int, double>(-12.3)));
	EXPECT_EQ(
		std::string("-1.23"), (misc::fix::floatConvert<2, int, double>(-1.23)));
	EXPECT_EQ(std::string("-0.123"),
		(misc::fix::floatConvert<3, int, double>(-0.123)));

	EXPECT_EQ(std::string("-999999999"),
		(misc::fix::floatConvert<0, int, double>(-999999999.0)));
	EXPECT_EQ(std::string("-0.999999999"),
		(misc::fix::floatConvert<9, int, double>(-0.999999999)));

	EXPECT_EQ(
		std::string("9.123"), (misc::fix::floatConvert<3, int, double>(9.123)));
	EXPECT_EQ(std::string("9.123"),
		(misc::fix::floatConvert<3, int, double>(9.1231)));
	EXPECT_EQ(std::string("9.123"),
		(misc::fix::floatConvert<3, int, double>(9.1229)));
	EXPECT_EQ(std::string("9.123"),
		(misc::fix::floatConvert<3, int, double>(9.12301)));
	EXPECT_EQ(std::string("9.123"),
		(misc::fix::floatConvert<3, int, double>(9.12299)));
	EXPECT_EQ(std::string("9.123"),
		(misc::fix::floatConvert<3, int, double>(9.123001)));
	EXPECT_EQ(std::string("9.123"),
		(misc::fix::floatConvert<3, int, double>(9.122999)));

	EXPECT_EQ(std::string("-9.123"),
		(misc::fix::floatConvert<3, int, double>(-9.123)));
	EXPECT_EQ(std::string("-9.123"),
		(misc::fix::floatConvert<3, int, double>(-9.1231)));
	EXPECT_EQ(std::string("-9.123"),
		(misc::fix::floatConvert<3, int, double>(-9.1229)));
	EXPECT_EQ(std::string("-9.123"),
		(misc::fix::floatConvert<3, int, double>(-9.12301)));
	EXPECT_EQ(std::string("-9.123"),
		(misc::fix::floatConvert<3, int, double>(-9.12299)));
	EXPECT_EQ(std::string("-9.123"),
		(misc::fix::floatConvert<3, int, double>(-9.123001)));
	EXPECT_EQ(std::string("-9.123"),
		(misc::fix::floatConvert<3, int, double>(-9.122999)));
}
