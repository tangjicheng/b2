#include <array>
#include <cstring>
#include <string>

#include <gtest/gtest.h>

#include <getopt.h>
#include <misc/plist.h>

std::string PLIST_IN = "test-plist-in.plist";

namespace {

class PList {
protected:
	PList()
		: pl(plist::PlistParser().parseFile(PLIST_IN))
	{
	}

	plist::Plist pl;
	std::array<std::string, 3> expected_read {
		{"Element One", "Element Two", "Element Three"}};
	std::array<std::string, 3> expected_write {
		{"Element 1", "Element 2", "Element 3"}};
};

template <typename Dict>
class PListHomogeneousDict : public PList {
protected:
	PListHomogeneousDict()
		: dict(pl["Homogeneous Dictionary"].asDict())
	{
	}

	void readByKey(const std::array<std::string, 3>& expected)
	{
		for (size_t i = 0; i != dict.size(); ++i)
			EXPECT_EQ("Dictionary " + expected[i], dict[key[i]].asString());
	}

	void write(const std::array<std::string, 3>& expected)
	{
		for (size_t i = 0; i != dict.size(); ++i)
			ASSERT_NO_THROW(
				dict[key[i]].asString() = "Dictionary " + expected[i]);
		EXPECT_EQ(3u, dict.size());
	}

	Dict& dict;
	std::array<std::string, 3> key {{"Key A", "Key B", "Key C"}};
};

} // namespace

class TestPListHomogeneousDict : public ::testing::Test,
								 public PListHomogeneousDict<plist::Dict> {
};

TEST_F(TestPListHomogeneousDict, size)
{
	EXPECT_EQ(3u, dict.size());
}

TEST_F(TestPListHomogeneousDict, read)
{
	readByKey(expected_read);
}

TEST_F(TestPListHomogeneousDict, write)
{
	write(expected_write);
	readByKey(expected_write);
}

class TestPListHomogeneousConstDict
	: public ::testing::Test,
	  public PListHomogeneousDict<const plist::Dict> {
};

TEST_F(TestPListHomogeneousConstDict, size)
{
	EXPECT_EQ(3u, dict.size());
}

TEST_F(TestPListHomogeneousConstDict, read)
{
	readByKey(expected_read);
}

namespace {

template <typename Array>
class PListHomogeneousArray : public PList {
protected:
	PListHomogeneousArray()
		: array(pl["Homogeneous Array"].asArray())
	{
	}

	void readByIndex(const std::array<std::string, 3>& expected)
	{
		for (size_t i = 0; i != array.size(); ++i)
			EXPECT_EQ("Array " + expected[i], array[i].asString());
	}

	void readByIterator(const std::array<std::string, 3>& expected)
	{
		size_t idx = 0;
		for (auto& element : array) {
			EXPECT_EQ("Array " + expected[idx], element.asString());
			++idx;
		}
	}

	void write(const std::array<std::string, 3>& expected)
	{
		for (size_t i = 0; i != array.size(); ++i)
			ASSERT_NO_THROW(array[i].asString() = "Array " + expected[i]);
		EXPECT_EQ(3u, array.size());
	}

	Array& array;
};

} // namespace

class TestPListHomogeneousArray : public ::testing::Test,
								  public PListHomogeneousArray<plist::Array> {
};

TEST_F(TestPListHomogeneousArray, size)
{
	EXPECT_EQ(3u, array.size());
}

TEST_F(TestPListHomogeneousArray, read)
{
	readByIndex(expected_read);
	readByIterator(expected_read);
}

TEST_F(TestPListHomogeneousArray, write)
{
	write(expected_write);
	readByIndex(expected_write);
	readByIterator(expected_write);
}

class TestPListHomogeneousConstArray
	: public ::testing::Test,
	  public PListHomogeneousArray<const plist::Array> {
};

TEST_F(TestPListHomogeneousConstArray, size)
{
	EXPECT_EQ(3u, array.size());
}

TEST_F(TestPListHomogeneousConstArray, read)
{
	readByIndex(expected_read);
	readByIterator(expected_read);
}

namespace {

template <typename Dict>
class PListHeterogeneousDict : public PList {
protected:
	PListHeterogeneousDict()
		: dict(pl["Heterogeneous Dictionary"].asDict())
	{
	}

	void read()
	{
		EXPECT_EQ("Hello World", dict["Key A"].asString());
		EXPECT_TRUE(dict["Key B"].asBool());
		EXPECT_EQ(42, dict["Key C"].asInteger());
		EXPECT_EQ(3.14, dict["Key D"].asReal());
	}

	void write()
	{
		ASSERT_NO_THROW(dict["Key A"].asString() = "Goodbye");
		ASSERT_NO_THROW(dict["Key B"].asBool() = false);
		ASSERT_NO_THROW(dict["Key C"].asInteger() = 227);
		ASSERT_NO_THROW(dict["Key D"].asReal() = 2.72);

		EXPECT_EQ(4u, dict.size());

		EXPECT_EQ("Goodbye", dict["Key A"].asString());
		EXPECT_FALSE(dict["Key B"].asBool());
		EXPECT_EQ(227, dict["Key C"].asInteger());
		EXPECT_EQ(2.72, dict["Key D"].asReal());
	}

	void getDefault()
	{
		EXPECT_EQ("", dict.getString("Key W", std::string()));
		EXPECT_FALSE(dict.getBool("Key X", false));
		EXPECT_EQ(0, dict.getInteger("Key Y", 0));
		EXPECT_EQ(0, dict.getReal("Key Z", 0.0));

		EXPECT_EQ("Hello World", dict.getString("Key W", "Hello World"));
		EXPECT_TRUE(dict.getBool("Key X", true));
		EXPECT_EQ(42, dict.getInteger("Key Y", 42));
		EXPECT_EQ(3.14, dict.getReal("Key Z", 3.14));
	}

	Dict& dict;
};

} // namespace

class TestPListHeterogeneousDict : public ::testing::Test,
								   public PListHeterogeneousDict<plist::Dict> {
};

TEST_F(TestPListHeterogeneousDict, size)
{
	EXPECT_EQ(4u, dict.size());
}

TEST_F(TestPListHeterogeneousDict, read)
{
	read();
}

TEST_F(TestPListHeterogeneousDict, write)
{
	write();
}

TEST_F(TestPListHeterogeneousDict, getDefault)
{
	getDefault();
}

class TestPListHeterogeneousConstDict
	: public ::testing::Test,
	  public PListHeterogeneousDict<const plist::Dict> {
};

TEST_F(TestPListHeterogeneousConstDict, size)
{
	EXPECT_EQ(4u, dict.size());
}

TEST_F(TestPListHeterogeneousConstDict, read)
{
	read();
}

TEST_F(TestPListHeterogeneousConstDict, getDefault)
{
	getDefault();
}

namespace {

template <typename Array>
class PListHeterogeneousArray : public PList {
protected:
	PListHeterogeneousArray()
		: array(pl["Heterogeneous Array"].asArray())
	{
	}

	void read()
	{
		EXPECT_EQ("Goodbye", array[0].asString());
		EXPECT_FALSE(array[1].asBool());
		EXPECT_EQ(227, array[2].asInteger());
		EXPECT_EQ(2.72, array[3].asReal());
	}

	void write()
	{
		ASSERT_NO_THROW(array[0].asString() = "Hello World");
		ASSERT_NO_THROW(array[1].asBool() = true);
		ASSERT_NO_THROW(array[2].asInteger() = 42);
		ASSERT_NO_THROW(array[3].asReal() = 3.14);

		EXPECT_EQ(4u, array.size());

		EXPECT_EQ("Hello World", array[0].asString());
		EXPECT_TRUE(array[1].asBool());
		EXPECT_EQ(42, array[2].asInteger());
		EXPECT_EQ(3.14, array[3].asReal());
	}

	void getDefault()
	{
		EXPECT_EQ("", array.getString(6, std::string()));
		EXPECT_FALSE(array.getBool(7, false));
		EXPECT_EQ(0, array.getInteger(8, 0));
		EXPECT_EQ(0.0, array.getReal(9, 0.0));

		EXPECT_EQ("Goodbye", array.getString(6, "Goodbye"));
		EXPECT_TRUE(array.getBool(7, true));
		EXPECT_EQ(227, array.getInteger(8, 227));
		EXPECT_EQ(2.72, array.getReal(9, 2.72));
	}

	Array& array;
};

} // namespace

class TestPListHeterogeneousArray
	: public ::testing::Test,
	  public PListHeterogeneousArray<plist::Array> {
};

TEST_F(TestPListHeterogeneousArray, size)
{
	EXPECT_EQ(4u, array.size());
}

TEST_F(TestPListHeterogeneousArray, read)
{
	read();
}

TEST_F(TestPListHeterogeneousArray, write)
{
	write();
}

TEST_F(TestPListHeterogeneousArray, getDefault)
{
	getDefault();
}

class TestPListHeterogeneousConstArray
	: public ::testing::Test,
	  public PListHeterogeneousArray<const plist::Array> {
};

TEST_F(TestPListHeterogeneousConstArray, size)
{
	EXPECT_EQ(4u, array.size());
}

TEST_F(TestPListHeterogeneousConstArray, read)
{
	read();
}

TEST_F(TestPListHeterogeneousConstArray, getDefault)
{
	getDefault();
}

class TestPListWriter : public ::testing::Test {
protected:
	void SetUp()
	{
		auto dict = new plist::Dict;
		dict->insert("Key X", new plist::StringValue("Foo"));
		dict->insert("Key Y", new plist::StringValue("Bar"));
		dict->insert("Key Z", new plist::BoolValue(true));

		auto array = new plist::Array;
		array->append(new plist::IntegerValue(123));
		array->append(new plist::RealValue("456.789"));
		array->append(new plist::BoolValue(false));

		auto root = new plist::Dict;
		root->insert("Dictionary", dict);
		root->insert("Array", array);

		plist::PlistWriter().writeFile(
			plist::Plist(root), "test-plist-out.plist");
		pl = plist::PlistParser().parseFile("test-plist-out.plist");
	}

	plist::Plist pl;
};

TEST_F(TestPListWriter, size)
{
	EXPECT_EQ(3u, pl["Dictionary"].asDict().size());
	EXPECT_EQ(3u, pl["Array"].asArray().size());
}

TEST_F(TestPListWriter, read)
{
	auto& dict = pl["Dictionary"].asDict();
	EXPECT_EQ("Foo", dict["Key X"].asString());
	EXPECT_EQ("Bar", dict["Key Y"].asString());
	EXPECT_TRUE(dict["Key Z"].asBool());

	auto& array = pl["Array"].asArray();
	EXPECT_EQ(123, array[0].asInteger());
	EXPECT_EQ(456.789, array[1].asReal());
	EXPECT_FALSE(array[2].asBool());
}

int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);

	int option;
	while ((option = getopt(argc, argv, "f:")) != -1) {
		switch (option) {
		case 'f':
			PLIST_IN = optarg;
			break;
		}
	}

	return RUN_ALL_TESTS();
}
