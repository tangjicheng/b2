#include <exception>
#include <string>

#include <gtest/gtest.h>

#include <misc/application_exception.h>

#include "unittest.h"

TEST(TestConfigurationError, constructors)
{
	{
		misc::ConfigurationError e {"what"};
		EXPECT_STREQ("what", e.what());
	}
	{
		std::string s {"what"};
		misc::ConfigurationError e {s};
		EXPECT_STREQ("what", e.what());
	}
	{
		misc::ConfigurationError e {std::string {"what"}};
		EXPECT_STREQ("what", e.what());
	}
}

TEST(TestConfigurationError, inheritance)
{
	EXPECT_THROW_WHAT(
		(throw misc::ConfigurationError {"what"}), std::exception, "what");
}

TEST(TestCriticalError, constructors)
{
	{
		misc::CriticalError e {"what"};
		EXPECT_STREQ("what", e.what());
	}
	{
		std::string s {"what"};
		misc::CriticalError e {s};
		EXPECT_STREQ("what", e.what());
	}
	{
		misc::CriticalError e {std::string {"what"}};
		EXPECT_STREQ("what", e.what());
	}
}

TEST(TestCriticalError, inheritance)
{
	EXPECT_THROW_WHAT(
		(throw misc::CriticalError {"what"}), std::exception, "what");
}
