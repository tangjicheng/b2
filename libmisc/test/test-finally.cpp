#include <gtest/gtest.h>

#include <misc/Finally.h>

TEST(TestFinally, called)
{
	bool called = false;
	{
		auto y = misc::finally([&] { called = true; });
	}
	EXPECT_TRUE(called);
}

TEST(TestFinally, copy)
{
	bool called = false;
	{
		const std::function<void()> action([&] { called = true; });
		auto y = misc::finally(action);
	}
	EXPECT_TRUE(called);
}

TEST(TestFinally, move)
{
	bool called = false;
	{
		std::function<void()> action([&] { called = true; });
		auto y = misc::finally(std::move(action));
	}
	EXPECT_TRUE(called);
}

TEST(TestFinally, clear)
{
	bool called = false;
	{
		auto y = misc::finally([&] { called = true; });
		y.clear();
	}
	EXPECT_FALSE(called);
}
