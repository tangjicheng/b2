#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <ostream>
#include <string>
#include <thread>

#include <misc/ThreadedReactor.h>

namespace {

std::chrono::system_clock::duration time()
{
	static std::chrono::system_clock::time_point start
		= std::chrono::system_clock::now();
	return std::chrono::system_clock::now() - start;
}

std::chrono::seconds::rep timeInSeconds()
{
	return std::chrono::duration_cast<std::chrono::seconds>(time()).count();
}

int pipefd[2];

misc::Reactor::CallStatus io(int)
{
	char buf[11];
	if (read(pipefd[0], buf, sizeof(buf)) == -1) {
		perror("Could not read from a pipe");
		exit(EXIT_FAILURE);
	}
	std::cout << timeInSeconds() << '\t' << "IO    call \"" << buf << '\"'
			  << std::endl;
	return misc::Reactor::CallStatus::OK;
}

void single(const std::string& s)
{
	std::cout << timeInSeconds() << '\t' << "Timed call \"" << s << '\"'
			  << std::endl;
}

misc::Reactor::CallStatus recurring(const std::string& s)
{
	single(s);
	return s.find("repeat") != std::string::npos
		? misc::Reactor::CallStatus::OK
		: misc::Reactor::CallStatus::REMOVE;
}

void test_io_call(misc::Reactor& reactor)
{
	auto a = reactor.callOnRead(pipefd[0], [](int fd) { return io(fd); });
	while (true) {
		auto t = time();
		if (t > std::chrono::seconds(10))
			break;
		char buf[11] = "HelloWorld";
		if (write(pipefd[1], buf, sizeof(buf)) == -1) {
			perror("Could not write to a pipe");
			exit(EXIT_FAILURE);
		}
		std::this_thread::sleep_for(std::chrono::seconds(2));
	}
	reactor.removeCall(a);
	std::this_thread::sleep_for(std::chrono::seconds(5));
}

void test_timed_call(misc::Reactor& reactor)
{
	reactor.callEvery(
		std::chrono::seconds(-7), [] { return recurring("T=-7 bee-4"); });
	auto b = reactor.callEvery(
		std::chrono::seconds(1), [] { return recurring("T=1 repeat"); });
	auto c = reactor.callEvery(
		std::chrono::seconds(3), [] { return recurring("T=3 repeat"); });
	reactor.callEvery(
		std::chrono::seconds(5), [] { return recurring("T=5 1-shot"); });
	reactor.callEvery(
		std::chrono::seconds(9), [] { return recurring("T=9 1-shot"); });
	bool removed = false;
	bool called = false;
	while (true) {
		auto t = time();
		if (t > std::chrono::seconds(10))
			break;
		if (!removed && t > std::chrono::seconds(4)) {
			reactor.removeCall(b);
			removed = true;
		}
		if (!called && t > std::chrono::seconds(8)) {
			reactor.callNow([] { single("  Call now"); });
			called = true;
		}
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
	reactor.removeCall(c);
	std::this_thread::sleep_for(std::chrono::seconds(5));
}

} // namespace

int main()
{
	if (pipe(pipefd) == -1) {
		perror("Could not create a pipe");
		return EXIT_FAILURE;
	}

	misc::ThreadedReactor reactor;
	reactor.start();
	if (reactor.get_tid() < 0) {
		perror("Unexpected thread ID");
		return EXIT_FAILURE;
	}

	std::thread io_thread([&] { test_io_call(reactor.get()); });
	std::thread timed_thread([&] { test_timed_call(reactor.get()); });

	io_thread.join();
	timed_thread.join();

	reactor.stop();
	if (reactor.get_tid() >= 0) {
		perror("Unexpected thread ID");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
