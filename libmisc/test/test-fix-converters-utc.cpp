#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <misc/fix/Converters.h>
#include <misc/fix/UTCTimeDate.h>

#include "unittest.h"

TEST(fixUTCDateOnlyConvertFromString, valid)
{
	EXPECT_EQ(misc::fix::UTCDateOnly(0, 1, 1),
		misc::fix::utcDateOnlyConvert(std::string("00000101")));
	EXPECT_EQ(misc::fix::UTCDateOnly(1, 1, 1),
		misc::fix::utcDateOnlyConvert(std::string("00010101")));
	EXPECT_EQ(misc::fix::UTCDateOnly(2017, 5, 30),
		misc::fix::utcDateOnlyConvert(std::string("20170530")));
	EXPECT_EQ(misc::fix::UTCDateOnly(9999, 12, 31),
		misc::fix::utcDateOnlyConvert(std::string("99991231")));
}

TEST(fixUTCDateOnlyConvertFromString, badFormat)
{
	EXPECT_THROW_WHAT(misc::fix::utcDateOnlyConvert(std::string()),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::utcDateOnlyConvert(std::string("30")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::utcDateOnlyConvert(std::string("0530")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::utcDateOnlyConvert(std::string("2017-05-30")),
		std::invalid_argument, "Bad format");
}

TEST(fixUTCDateOnlyConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT(misc::fix::utcDateOnlyConvert(std::string("2017 530")),
		std::invalid_argument, "Invalid character");
}

TEST(fixUTCDateOnlyConvertFromString, outOfRange)
{
	EXPECT_THROW_WHAT(misc::fix::utcDateOnlyConvert(std::string("20170500")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(misc::fix::utcDateOnlyConvert(std::string("20170532")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(misc::fix::utcDateOnlyConvert(std::string("20170030")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(misc::fix::utcDateOnlyConvert(std::string("20171330")),
		std::invalid_argument, "Out of range");
}

TEST(fixUTCDateOnlyConvertToString, valid)
{
	EXPECT_EQ(std::string("00000101"),
		misc::fix::utcDateOnlyConvert(misc::fix::UTCDateOnly(0, 1, 1)));
	EXPECT_EQ(std::string("00010101"),
		misc::fix::utcDateOnlyConvert(misc::fix::UTCDateOnly(1, 1, 1)));
	EXPECT_EQ(std::string("20170530"),
		misc::fix::utcDateOnlyConvert(misc::fix::UTCDateOnly(2017, 5, 30)));
	EXPECT_EQ(std::string("99991231"),
		misc::fix::utcDateOnlyConvert(misc::fix::UTCDateOnly(9999, 12, 31)));
}

TEST(fixUTCTimeOnlyConvertFromString, valid)
{
	EXPECT_EQ(misc::fix::UTCTimeOnly(0, 0, 0),
		misc::fix::utcTimeOnlyConvert(std::string("00:00:00")));
	EXPECT_EQ(misc::fix::UTCTimeOnly(1, 1, 1),
		misc::fix::utcTimeOnlyConvert(std::string("01:01:01")));
	EXPECT_EQ(misc::fix::UTCTimeOnly(19, 3, 42),
		misc::fix::utcTimeOnlyConvert(std::string("19:03:42")));
	EXPECT_EQ(misc::fix::UTCTimeOnly(23, 59, 60),
		misc::fix::utcTimeOnlyConvert(std::string("23:59:60")));

	EXPECT_EQ(misc::fix::UTCTimeOnly(0, 0, 0, 0),
		misc::fix::utcTimeOnlyConvert(std::string("00:00:00.000")));
	EXPECT_EQ(misc::fix::UTCTimeOnly(1, 1, 1, 1),
		misc::fix::utcTimeOnlyConvert(std::string("01:01:01.001")));
	EXPECT_EQ(misc::fix::UTCTimeOnly(19, 3, 42, 59),
		misc::fix::utcTimeOnlyConvert(std::string("19:03:42.059")));
	EXPECT_EQ(misc::fix::UTCTimeOnly(23, 59, 60, 999),
		misc::fix::utcTimeOnlyConvert(std::string("23:59:60.999")));
}

TEST(fixUTCTimeOnlyConvertFromString, badFormat)
{
	EXPECT_THROW_WHAT(misc::fix::utcTimeOnlyConvert(std::string()),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::utcTimeOnlyConvert(std::string(":42")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::utcTimeOnlyConvert(std::string(":03:42")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::utcTimeOnlyConvert(std::string("19:03:42.0")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimeOnlyConvert(std::string("19:03:42.059038")),
		std::invalid_argument, "Bad format");

	EXPECT_THROW_WHAT(
		misc::fix::utcTimeOnlyConvert(std::string("19-03:42.059")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimeOnlyConvert(std::string("19:03-42.059")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimeOnlyConvert(std::string("19:03:42-059")),
		std::invalid_argument, "Bad format");
}

TEST(fixUTCTimeOnlyConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT(
		misc::fix::utcTimeOnlyConvert(std::string("19: 3:42.059")),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimeOnlyConvert(std::string("19:03:42. 59")),
		std::invalid_argument, "Invalid character");
}

TEST(fixUTCTimeOnlyConvertFromString, outOfRange)
{
	EXPECT_THROW_WHAT(
		misc::fix::utcTimeOnlyConvert(std::string("19:03:61.059")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimeOnlyConvert(std::string("19:60:42.059")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimeOnlyConvert(std::string("24:03:42.059")),
		std::invalid_argument, "Out of range");
}

TEST(fixUTCTimeOnlyConvertToString, valid)
{
	EXPECT_EQ(std::string("00:00:00"),
		misc::fix::utcTimeOnlyConvert(misc::fix::UTCTimeOnly(0, 0, 0)));
	EXPECT_EQ(std::string("01:01:01"),
		misc::fix::utcTimeOnlyConvert(misc::fix::UTCTimeOnly(1, 1, 1)));
	EXPECT_EQ(std::string("19:03:42"),
		misc::fix::utcTimeOnlyConvert(misc::fix::UTCTimeOnly(19, 3, 42)));
	EXPECT_EQ(std::string("23:59:60"),
		misc::fix::utcTimeOnlyConvert(misc::fix::UTCTimeOnly(23, 59, 60)));

	EXPECT_EQ(std::string("00:00:00.000"),
		misc::fix::utcTimeOnlyConvert(misc::fix::UTCTimeOnly(0, 0, 0, 0)));
	EXPECT_EQ(std::string("01:01:01.001"),
		misc::fix::utcTimeOnlyConvert(misc::fix::UTCTimeOnly(1, 1, 1, 1)));
	EXPECT_EQ(std::string("19:03:42.059"),
		misc::fix::utcTimeOnlyConvert(misc::fix::UTCTimeOnly(19, 3, 42, 59)));
	EXPECT_EQ(std::string("23:59:60.999"),
		misc::fix::utcTimeOnlyConvert(misc::fix::UTCTimeOnly(23, 59, 60, 999)));
}

TEST(fixUTCTimestampConvertFromString, valid)
{
	EXPECT_EQ(misc::fix::UTCTimestamp(0, 1, 1, 0, 0, 0),
		misc::fix::utcTimestampConvert(std::string("00000101-00:00:00")));
	EXPECT_EQ(misc::fix::UTCTimestamp(1, 1, 1, 1, 1, 1),
		misc::fix::utcTimestampConvert(std::string("00010101-01:01:01")));
	EXPECT_EQ(misc::fix::UTCTimestamp(2017, 5, 30, 19, 3, 42),
		misc::fix::utcTimestampConvert(std::string("20170530-19:03:42")));
	EXPECT_EQ(misc::fix::UTCTimestamp(9999, 12, 31, 23, 59, 60),
		misc::fix::utcTimestampConvert(std::string("99991231-23:59:60")));

	EXPECT_EQ(misc::fix::UTCTimestamp(0, 1, 1, 0, 0, 0, 0),
		misc::fix::utcTimestampConvert(std::string("00000101-00:00:00.000")));
	EXPECT_EQ(misc::fix::UTCTimestamp(1, 1, 1, 1, 1, 1, 1),
		misc::fix::utcTimestampConvert(std::string("00010101-01:01:01.001")));
	EXPECT_EQ(misc::fix::UTCTimestamp(2017, 5, 30, 19, 3, 42, 59),
		misc::fix::utcTimestampConvert(std::string("20170530-19:03:42.059")));
	EXPECT_EQ(misc::fix::UTCTimestamp(9999, 12, 31, 23, 59, 60, 999),
		misc::fix::utcTimestampConvert(std::string("99991231-23:59:60.999")));
}

TEST(fixUTCTimestampConvertFromString, badFormat)
{
	EXPECT_THROW_WHAT(misc::fix::utcTimestampConvert(std::string()),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::utcTimestampConvert(std::string("20170530")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("19:03:42.059")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("2017-05-30-19:03:42.059")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-19:03:42.0")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-19:03:42.059038")),
		std::invalid_argument, "Bad format");

	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530+19:03:42.059")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-19-03:42.059")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-19:03-42.059")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-19:03:42-059")),
		std::invalid_argument, "Bad format");
}

TEST(fixUTCTimestampConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("2017 530-19:03:42.059")),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-19: 3:42.059")),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-19:03:42. 59")),
		std::invalid_argument, "Invalid character");
}

TEST(fixUTCTimestampConvertFromString, outOfRange)
{
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170500-19:03:42.059")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170532-19:03:42.059")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170030-19:03:42.059")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20171330-19:03:42.059")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-19:03:61.059")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-19:60:42.059")),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		misc::fix::utcTimestampConvert(std::string("20170530-24:03:42.059")),
		std::invalid_argument, "Out of range");
}

TEST(fixUTCTimestampConvertToString, valid)
{
	EXPECT_EQ(std::string("00000101-00:00:00"),
		misc::fix::utcTimestampConvert(
			misc::fix::UTCTimestamp(0, 1, 1, 0, 0, 0)));
	EXPECT_EQ(std::string("00010101-01:01:01"),
		misc::fix::utcTimestampConvert(
			misc::fix::UTCTimestamp(1, 1, 1, 1, 1, 1)));
	EXPECT_EQ(std::string("20170530-19:03:42"),
		misc::fix::utcTimestampConvert(
			misc::fix::UTCTimestamp(2017, 5, 30, 19, 3, 42)));
	EXPECT_EQ(std::string("99991231-23:59:60"),
		misc::fix::utcTimestampConvert(
			misc::fix::UTCTimestamp(9999, 12, 31, 23, 59, 60)));

	EXPECT_EQ(std::string("00000101-00:00:00.000"),
		misc::fix::utcTimestampConvert(
			misc::fix::UTCTimestamp(0, 1, 1, 0, 0, 0, 0)));
	EXPECT_EQ(std::string("00010101-01:01:01.001"),
		misc::fix::utcTimestampConvert(
			misc::fix::UTCTimestamp(1, 1, 1, 1, 1, 1, 1)));
	EXPECT_EQ(std::string("20170530-19:03:42.059"),
		misc::fix::utcTimestampConvert(
			misc::fix::UTCTimestamp(2017, 5, 30, 19, 3, 42, 59)));
	EXPECT_EQ(std::string("99991231-23:59:60.999"),
		misc::fix::utcTimestampConvert(
			misc::fix::UTCTimestamp(9999, 12, 31, 23, 59, 60, 999)));
}
