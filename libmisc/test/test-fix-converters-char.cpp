#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <misc/fix/Converters.h>

#include "unittest.h"

TEST(fixCharConvertFromString, valid)
{
	for (unsigned int c = 32; c != 128; ++c) {
		if (c == ' ' || c == 127)
			continue;
		EXPECT_EQ(
			static_cast<char>(c), misc::fix::charConvert(std::string(1, c)));
	}
}

TEST(fixCharConvertFromString, badFormat)
{
	for (unsigned int count = 0; count != 10; ++count) {
		if (count == 1)
			continue;
		EXPECT_THROW_WHAT(misc::fix::charConvert(std::string(count, 'A')),
			std::invalid_argument, "Bad format");
	}
}

TEST(fixCharConvertFromString, invalidCharacter)
{
	for (unsigned int c = 0; c != 32; ++c) {
		EXPECT_THROW_WHAT(misc::fix::charConvert(std::string(1, c)),
			std::invalid_argument, "Invalid character");
	}
	EXPECT_THROW_WHAT(misc::fix::charConvert(std::string(1, ' ')),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::charConvert(std::string(1, 127)),
		std::invalid_argument, "Invalid character");
	for (unsigned int c = 128; c != 256; ++c) {
		EXPECT_THROW_WHAT(misc::fix::charConvert(std::string(1, c)),
			std::invalid_argument, "Invalid character");
	}
}

TEST(fixCharConvertToString, valid)
{
	for (unsigned int c = 32; c != 128; ++c) {
		if (c == ' ' || c == 127)
			continue;
		EXPECT_EQ(std::string(1, c), misc::fix::charConvert(c));
	}
}

TEST(fixCharConvertToString, invalidCharacter)
{
	for (unsigned int c = 0; c != 32; ++c) {
		EXPECT_THROW_WHAT(misc::fix::charConvert(c), std::invalid_argument,
			"Invalid character");
	}
	EXPECT_THROW_WHAT(misc::fix::charConvert(' '), std::invalid_argument,
		"Invalid character");
	EXPECT_THROW_WHAT(misc::fix::charConvert(127), std::invalid_argument,
		"Invalid character");
	for (unsigned int c = 128; c != 256; ++c) {
		EXPECT_THROW_WHAT(misc::fix::charConvert(c), std::invalid_argument,
			"Invalid character");
	}
}
