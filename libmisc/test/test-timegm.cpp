#include <cstring>
#include <ctime>

#include <gtest/gtest.h>

#include <misc/timegm.h>

TEST(timegm, convert)
{
	tm tm;
	memset(&tm, 0, sizeof(tm));
	tm.tm_sec = 5;
	tm.tm_min = 14;
	tm.tm_hour = 6;
	tm.tm_mday = 7;
	tm.tm_mon = 9 - 1;
	tm.tm_year = 2017 - 1900;
	time_t t = misc::timegm(&tm);
	EXPECT_EQ(1504764845, t);
}

TEST(mktime, convert)
{
	tm tm;
	memset(&tm, 0, sizeof(tm));
	tm.tm_sec = 5;
	tm.tm_min = 14;
	tm.tm_hour = 15;
	tm.tm_mday = 7;
	tm.tm_mon = 9 - 1;
	tm.tm_year = 2017 - 1900;
	time_t t = mktime(&tm);
	EXPECT_EQ(1504764845, t);
}

TEST(localtime, convert)
{
	time_t t = 1504764845;
	tm tm;
	memset(&tm, 0, sizeof(tm));
	localtime_r(&t, &tm);
	EXPECT_EQ(5, tm.tm_sec);
	EXPECT_EQ(14, tm.tm_min);
	EXPECT_EQ(15, tm.tm_hour);
	EXPECT_EQ(7, tm.tm_mday);
	EXPECT_EQ(9, tm.tm_mon + 1);
	EXPECT_EQ(2017, tm.tm_year + 1900);
}

TEST(gmtime, convert)
{
	time_t t = 1504764845;
	tm tm;
	memset(&tm, 0, sizeof(tm));
	gmtime_r(&t, &tm);
	EXPECT_EQ(5, tm.tm_sec);
	EXPECT_EQ(14, tm.tm_min);
	EXPECT_EQ(6, tm.tm_hour);
	EXPECT_EQ(7, tm.tm_mday);
	EXPECT_EQ(9, tm.tm_mon + 1);
	EXPECT_EQ(2017, tm.tm_year + 1900);
}
