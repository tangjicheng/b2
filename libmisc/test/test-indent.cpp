#include <sstream>

#include <gtest/gtest.h>

#include <misc/indent.h>

class TestIndent : public ::testing::Test {
protected:
	std::ostringstream output;
};

TEST_F(TestIndent, zeroTabs)
{
	output << misc::indent(0) << "Hello";
	EXPECT_EQ("Hello", output.str());
}

TEST_F(TestIndent, oneTab)
{
	output << misc::indent(1) << "Hello";
	EXPECT_EQ("\tHello", output.str());
}

TEST_F(TestIndent, twoTabs)
{
	output << misc::indent(2) << "Hello";
	EXPECT_EQ("\t\tHello", output.str());
}

TEST_F(TestIndent, nonDefaultIndentCharacter)
{
	output << misc::indent(5, '*') << "Hello";
	EXPECT_EQ("*****Hello", output.str());
}

TEST_F(TestIndent, callOperator)
{
	misc::indent(5, '*')(output);
	output << "Hello";
	EXPECT_EQ("*****Hello", output.str());
}
