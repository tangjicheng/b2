#include <stdexcept>
#include <string>
#include <vector>

#include <gtest/gtest.h>

#include <misc/fix/Converters.h>

#include "unittest.h"

TEST(fixMultipleCharValueConvertFromString, valid)
{
	EXPECT_EQ((std::vector<char> {'2'}),
		misc::fix::multipleCharValueConvert(std::string("2")));
	EXPECT_EQ((std::vector<char> {'2', 'A'}),
		misc::fix::multipleCharValueConvert(std::string("2 A")));
	EXPECT_EQ((std::vector<char> {'2', 'A', 'F'}),
		misc::fix::multipleCharValueConvert(std::string("2 A F")));
}

TEST(fixMultipleCharValueConvertFromString, badFormat)
{
	EXPECT_THROW_WHAT(misc::fix::multipleCharValueConvert(std::string("")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::multipleCharValueConvert(std::string("2A")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::multipleCharValueConvert(std::string("2 ")),
		std::invalid_argument, "Bad format");
}

TEST(fixMultipleCharValueConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT(misc::fix::multipleCharValueConvert(std::string(" ")),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::multipleCharValueConvert(std::string(" 2")),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::multipleCharValueConvert(std::string("2  A")),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		misc::fix::multipleCharValueConvert(std::string("2 A \x0F")),
		std::invalid_argument, "Invalid character");
}

TEST(fixMultipleCharValueConvertToString, valid)
{
	EXPECT_EQ(std::string("2"),
		misc::fix::multipleCharValueConvert(std::vector<char> {'2'}));
	EXPECT_EQ(std::string("2 A"),
		misc::fix::multipleCharValueConvert(std::vector<char> {'2', 'A'}));
	EXPECT_EQ(std::string("2 A F"),
		misc::fix::multipleCharValueConvert(std::vector<char> {'2', 'A', 'F'}));
}

TEST(fixMultipleCharValueConvertToString, badFormat)
{
	EXPECT_THROW_WHAT(misc::fix::multipleCharValueConvert(std::vector<char>()),
		std::invalid_argument, "Bad format");
}

TEST(fixMultipleCharValueConvertToString, invalidCharacter)
{
	EXPECT_THROW_WHAT(
		misc::fix::multipleCharValueConvert(std::vector<char> {'2', 'A', ' '}),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::multipleCharValueConvert(
						  std::vector<char> {'2', 'A', '\x0F'}),
		std::invalid_argument, "Invalid character");
}
