#include <cstdarg>
#include <cstdio>
#include <cstdlib>

#include <misc/Log.h>

namespace {

void customLogger(int priority, const char* fmt, va_list ap) noexcept
{
	fprintf(stderr, "Custom logger (%d) ", priority);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
}

void f()
{
	misc::appLog(misc::APP_LOG_EMERG, "Emergency");
	misc::appLog(misc::APP_LOG_ALERT, "Alert");
	misc::appLog(misc::APP_LOG_CRIT, "Critical");
	misc::appLog(misc::APP_LOG_ERR, "Error");
	misc::appLog(misc::APP_LOG_WARNING, "Warning");
	misc::appLog(misc::APP_LOG_NOTICE, "Notice");
	misc::appLog(misc::APP_LOG_INFO, "Informational");
	misc::appLog(misc::APP_LOG_DEBUG, "Debug");
	fputc('\n', stderr);
}

} // namespace

int main()
{
	f();
	auto oldLogger = misc::appSetLogger(&customLogger);
	f();
	misc::appSetLogger(oldLogger);
	f();
	misc::appSetLogger(&misc::appScreenLoggerNoTime);
	f();
	misc::appSetLogger(&misc::appScreenLogger);
	f();

	return EXIT_SUCCESS;
}
