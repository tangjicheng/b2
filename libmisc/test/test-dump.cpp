#include <sstream>
#include <string>

#include <gtest/gtest.h>

#include <misc/dump.h>

class TestDump : public ::testing::Test {
protected:
	static const char input_str[];
	static const size_t input_len;
	static const std::string input_string;
	std::ostringstream output;
	static const char expected[];

	void clearOutput()
	{
		output.clear();
		output.str(std::string());
	}
};

const char TestDump::input_str[] = "*\nHello\aWorld\n*";
const size_t TestDump::input_len = sizeof(TestDump::input_str) - 1;
const std::string TestDump::input_string = TestDump::input_str;
const char TestDump::expected[] = "*\\x0aHello\\x07World\\x0a*";

TEST_F(TestDump, writeToStream)
{
	misc::dump(input_str, input_len, output);
	EXPECT_EQ(expected, output.str());

	clearOutput();

	misc::dump(input_string, output);
	EXPECT_EQ(expected, output.str());
}

TEST_F(TestDump, returnAString)
{
	EXPECT_EQ(expected, misc::dump(input_str, input_len));
	EXPECT_EQ(expected, misc::dump(input_string));
}

TEST_F(TestDump, ostreamChaining)
{
	output << misc::Dump(input_str, input_len);
	EXPECT_EQ(expected, output.str());

	clearOutput();

	output << misc::Dump(input_string);
	EXPECT_EQ(expected, output.str());
}
