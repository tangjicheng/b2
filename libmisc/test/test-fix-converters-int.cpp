#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <misc/fix/Converters.h>

#include "unittest.h"

TEST(fixPositiveIntConvertFromString, valid)
{
	EXPECT_EQ(0, (misc::fix::positiveIntConvert<int>(std::string("0"))));
	EXPECT_EQ(0, (misc::fix::positiveIntConvert<int>(std::string("0000"))));

	EXPECT_EQ(123, (misc::fix::positiveIntConvert<int>(std::string("123"))));
	EXPECT_EQ(123, (misc::fix::positiveIntConvert<int>(std::string("000123"))));

	EXPECT_EQ(999999999,
		(misc::fix::positiveIntConvert<int>(std::string("999999999"))));
	EXPECT_EQ(999999999,
		(misc::fix::positiveIntConvert<int>(std::string("000999999999"))));

	EXPECT_EQ(999999999U,
		(misc::fix::positiveIntConvert<unsigned int>(
			std::string("999999999"))));
	EXPECT_EQ(999999999999999999L,
		(misc::fix::positiveIntConvert<long>(
			std::string("999999999999999999"))));
	EXPECT_EQ(9999999999999999999UL,
		(misc::fix::positiveIntConvert<unsigned long>(
			std::string("9999999999999999999"))));
}

TEST(fixPositiveIntConvertFromString, badFormat)
{
	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<int>(std::string())),
		std::invalid_argument, "Bad format");
}

TEST(fixPositiveIntConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<int>(std::string("+123"))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<int>(std::string("-123"))),
		std::invalid_argument, "Invalid character");

	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<int>(std::string(" 123"))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<int>(std::string("123 "))),
		std::invalid_argument, "Invalid character");
}

TEST(fixPositiveIntConvertFromString, outOfRange)
{
	EXPECT_THROW_WHAT(
		(misc::fix::positiveIntConvert<int>(std::string("1000000000"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT(
		(misc::fix::positiveIntConvert<int>(std::string("2147483647"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveIntConvert<int>(std::string("2147483648"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::positiveIntConvert<int>(std::string("2147483649"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<unsigned int>(
						  std::string("4294967295"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<unsigned int>(
						  std::string("4294967296"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<long>(
						  std::string("9223372036854775807"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<long>(
						  std::string("9223372036854775808"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<long>(
						  std::string("9223372036854775809"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<unsigned long>(
						  std::string("18446744073709551615"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::positiveIntConvert<unsigned long>(
						  std::string("18446744073709551616"))),
		std::invalid_argument, "Out of range");
}

TEST(fixIntConvertFromString, validPositive)
{
	EXPECT_EQ(0, (misc::fix::intConvert<int>(std::string("0"))));
	EXPECT_EQ(0, (misc::fix::intConvert<int>(std::string("0000"))));

	EXPECT_EQ(123, (misc::fix::intConvert<int>(std::string("123"))));
	EXPECT_EQ(123, (misc::fix::intConvert<int>(std::string("000123"))));

	EXPECT_EQ(
		999999999, (misc::fix::intConvert<int>(std::string("999999999"))));
	EXPECT_EQ(
		999999999, (misc::fix::intConvert<int>(std::string("000999999999"))));

	EXPECT_EQ(999999999999999999L,
		(misc::fix::intConvert<long>(std::string("999999999999999999"))));
}

TEST(fixIntConvertFromString, validNegative)
{
	EXPECT_EQ(0, (misc::fix::intConvert<int>(std::string("-0"))));
	EXPECT_EQ(0, (misc::fix::intConvert<int>(std::string("-0000"))));

	EXPECT_EQ(-123, (misc::fix::intConvert<int>(std::string("-123"))));
	EXPECT_EQ(-123, (misc::fix::intConvert<int>(std::string("-000123"))));

	EXPECT_EQ(
		-999999999, (misc::fix::intConvert<int>(std::string("-999999999"))));
	EXPECT_EQ(
		-999999999, (misc::fix::intConvert<int>(std::string("-000999999999"))));

	EXPECT_EQ(-999999999999999999L,
		(misc::fix::intConvert<long>(std::string("-999999999999999999"))));
}

TEST(fixIntConvertFromString, badFormat)
{
	EXPECT_THROW_WHAT((misc::fix::intConvert<int>(std::string("-"))),
		std::invalid_argument, "Bad format");
}

TEST(fixIntConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT((misc::fix::intConvert<int>(std::string("+123"))),
		std::invalid_argument, "Invalid character");

	EXPECT_THROW_WHAT((misc::fix::intConvert<int>(std::string(" 123"))),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT((misc::fix::intConvert<int>(std::string("123 "))),
		std::invalid_argument, "Invalid character");
}

TEST(fixIntConvertFromString, outOfRange)
{
	EXPECT_THROW_WHAT((misc::fix::intConvert<int>(std::string("-1000000000"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT((misc::fix::intConvert<int>(std::string("-2147483647"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::intConvert<int>(std::string("-2147483648"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT((misc::fix::intConvert<int>(std::string("-2147483649"))),
		std::invalid_argument, "Out of range");

	EXPECT_THROW_WHAT(
		(misc::fix::intConvert<long>(std::string("-9223372036854775807"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::intConvert<long>(std::string("-9223372036854775808"))),
		std::invalid_argument, "Out of range");
	EXPECT_THROW_WHAT(
		(misc::fix::intConvert<long>(std::string("-9223372036854775809"))),
		std::invalid_argument, "Out of range");
}

TEST(fixIntConvertUnsignedToString, valid)
{
	EXPECT_EQ(std::string("0"), (misc::fix::intConvert<unsigned int>(0)));
	EXPECT_EQ(std::string("123"), (misc::fix::intConvert<unsigned int>(123)));
	EXPECT_EQ(std::string("999999999"),
		(misc::fix::intConvert<unsigned int>(999999999)));
	EXPECT_EQ(std::string("1000000000"),
		(misc::fix::intConvert<unsigned int>(1000000000)));
	EXPECT_EQ(std::string("4294967295"),
		(misc::fix::intConvert<unsigned int>(4294967295)));

	EXPECT_EQ(std::string("0"), (misc::fix::intConvert<unsigned long>(0)));
	EXPECT_EQ(std::string("123"), (misc::fix::intConvert<unsigned long>(123)));
	EXPECT_EQ(std::string("9999999999999999999"),
		(misc::fix::intConvert<unsigned long>(9999999999999999999UL)));
	EXPECT_EQ(std::string("10000000000000000000"),
		(misc::fix::intConvert<unsigned long>(10000000000000000000UL)));
	EXPECT_EQ(std::string("18446744073709551615"),
		(misc::fix::intConvert<unsigned long>(18446744073709551615UL)));
}

TEST(fixIntConvertSignedToString, valid)
{
	EXPECT_EQ(
		std::string("-2147483648"), (misc::fix::intConvert<int>(-2147483648)));
	EXPECT_EQ(
		std::string("-2147483647"), (misc::fix::intConvert<int>(-2147483647)));
	EXPECT_EQ(
		std::string("-1000000000"), (misc::fix::intConvert<int>(-1000000000)));
	EXPECT_EQ(
		std::string("-999999999"), (misc::fix::intConvert<int>(-999999999)));
	EXPECT_EQ(std::string("-123"), (misc::fix::intConvert<int>(-123)));
	EXPECT_EQ(std::string("0"), (misc::fix::intConvert<int>(0)));
	EXPECT_EQ(std::string("123"), (misc::fix::intConvert<int>(123)));
	EXPECT_EQ(
		std::string("999999999"), (misc::fix::intConvert<int>(999999999)));
	EXPECT_EQ(
		std::string("1000000000"), (misc::fix::intConvert<int>(1000000000)));
	EXPECT_EQ(
		std::string("2147483647"), (misc::fix::intConvert<int>(2147483647)));

	EXPECT_EQ(std::string("-9223372036854775808"),
		(misc::fix::intConvert<long>(-9223372036854775808UL)));
	EXPECT_EQ(std::string("-9223372036854775807"),
		(misc::fix::intConvert<long>(-9223372036854775807L)));
	EXPECT_EQ(std::string("-1000000000000000000"),
		(misc::fix::intConvert<long>(-1000000000000000000L)));
	EXPECT_EQ(std::string("-999999999999999999"),
		(misc::fix::intConvert<long>(-999999999999999999L)));
	EXPECT_EQ(std::string("-123"), (misc::fix::intConvert<long>(-123)));
	EXPECT_EQ(std::string("0"), (misc::fix::intConvert<long>(0)));
	EXPECT_EQ(std::string("123"), (misc::fix::intConvert<long>(123)));
	EXPECT_EQ(std::string("999999999999999999"),
		(misc::fix::intConvert<long>(999999999999999999L)));
	EXPECT_EQ(std::string("1000000000000000000"),
		(misc::fix::intConvert<long>(1000000000000000000L)));
	EXPECT_EQ(std::string("9223372036854775807"),
		(misc::fix::intConvert<long>(9223372036854775807L)));
}

TEST(fixPaddedIntConvertToString, valid)
{
	EXPECT_EQ(std::string("0000000000"),
		(misc::fix::paddedIntConvert<10, unsigned int>(0)));
	EXPECT_EQ(std::string("0000000123"),
		(misc::fix::paddedIntConvert<10, unsigned int>(123)));
	EXPECT_EQ(std::string("0999999999"),
		(misc::fix::paddedIntConvert<10, unsigned int>(999999999)));
	EXPECT_EQ(std::string("1000000000"),
		(misc::fix::paddedIntConvert<10, unsigned int>(1000000000)));
	EXPECT_EQ(std::string("4294967295"),
		(misc::fix::paddedIntConvert<10, unsigned int>(4294967295)));

	EXPECT_EQ(std::string("00000000000000000000"),
		(misc::fix::paddedIntConvert<20, unsigned int>(0)));
	EXPECT_EQ(std::string("00000000000000000123"),
		(misc::fix::paddedIntConvert<20, unsigned int>(123)));
	EXPECT_EQ(std::string("00000000000999999999"),
		(misc::fix::paddedIntConvert<20, unsigned int>(999999999)));
	EXPECT_EQ(std::string("00000000001000000000"),
		(misc::fix::paddedIntConvert<20, unsigned int>(1000000000)));
	EXPECT_EQ(std::string("00000000004294967295"),
		(misc::fix::paddedIntConvert<20, unsigned int>(4294967295)));

	EXPECT_EQ(std::string("00000000000000000000"),
		(misc::fix::paddedIntConvert<20, unsigned long>(0)));
	EXPECT_EQ(std::string("00000000000000000123"),
		(misc::fix::paddedIntConvert<20, unsigned long>(123)));
	EXPECT_EQ(std::string("09999999999999999999"),
		(misc::fix::paddedIntConvert<20, unsigned long>(
			9999999999999999999UL)));
	EXPECT_EQ(std::string("10000000000000000000"),
		(misc::fix::paddedIntConvert<20, unsigned long>(
			10000000000000000000UL)));
	EXPECT_EQ(std::string("18446744073709551615"),
		(misc::fix::paddedIntConvert<20, unsigned long>(
			18446744073709551615UL)));

	EXPECT_EQ(std::string("0000000000"),
		(misc::fix::paddedIntConvert<10, unsigned long>(0)));
	EXPECT_EQ(std::string("0000000123"),
		(misc::fix::paddedIntConvert<10, unsigned long>(123)));
	EXPECT_EQ(std::string("9999999999"),
		(misc::fix::paddedIntConvert<10, unsigned long>(
			9999999999999999999UL)));
	EXPECT_EQ(std::string("0000000000"),
		(misc::fix::paddedIntConvert<10, unsigned long>(
			10000000000000000000UL)));
	EXPECT_EQ(std::string("3709551615"),
		(misc::fix::paddedIntConvert<10, unsigned long>(
			18446744073709551615UL)));

	EXPECT_EQ(std::string("3"), (misc::fix::paddedIntConvert<1>(123u)));
	EXPECT_EQ(std::string("23"), (misc::fix::paddedIntConvert<2>(123u)));
	EXPECT_EQ(std::string("123"), (misc::fix::paddedIntConvert<3>(123u)));
	EXPECT_EQ(std::string("0123"), (misc::fix::paddedIntConvert<4>(123u)));
	EXPECT_EQ(std::string("00123"), (misc::fix::paddedIntConvert<5>(123u)));
	EXPECT_EQ(std::string("000123"), (misc::fix::paddedIntConvert<6>(123u)));
}
