#include <stdexcept>
#include <vector>

#include <gtest/gtest.h>

#include <misc/OneTimeAllocator.h>

namespace {

constexpr size_t ALLOCATE_SMALL {1};
constexpr size_t ALLOCATE_MEDIUM {2};
constexpr size_t ALLOCATE_LARGE {3};

using Allocator = misc::OneTimeAllocator<int>;
using Vector = std::vector<int, Allocator>;

} // namespace

TEST(TestOneTimeAllocatorConstructor, default)
{
	EXPECT_NO_THROW(Vector());
}

TEST(TestOneTimeAllocatorConstructor, size)
{
	EXPECT_NO_THROW((Vector(ALLOCATE_SMALL)));
}

class TestOneTimeAllocator : public ::testing::Test {
protected:
	Vector empty;
	Vector small = Vector(ALLOCATE_SMALL);
	Vector medium = Vector(ALLOCATE_MEDIUM);
	Vector large = Vector(ALLOCATE_LARGE);
};

TEST_F(TestOneTimeAllocator, resizeAfterDefaultConstructor)
{
	EXPECT_NO_THROW(empty.resize(ALLOCATE_SMALL));
}

TEST_F(TestOneTimeAllocator, resizeAfterSizeConstructor)
{
	EXPECT_THROW(small.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, copyConstructorEmptySource)
{
	EXPECT_NO_THROW((Vector(empty)));
}

TEST_F(TestOneTimeAllocator, resizeAfterCopyConstructorEmptySource)
{
	Vector copyConstructed(empty);
	ASSERT_NO_THROW(copyConstructed.resize(ALLOCATE_SMALL));
	EXPECT_THROW(copyConstructed.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, copyConstructor)
{
	EXPECT_NO_THROW((Vector(small)));
}

TEST_F(TestOneTimeAllocator, resizeAfterCopyConstructor)
{
	Vector copyConstructed(small);
	EXPECT_THROW(copyConstructed.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, copyAssignEmptySourceAndDest)
{
	Vector copyAssigned;
	copyAssigned = empty;
	ASSERT_NO_THROW(copyAssigned.resize(ALLOCATE_SMALL));
	EXPECT_THROW(copyAssigned.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, copyAssignEmptyDest)
{
	ASSERT_NO_THROW(empty = small);
	EXPECT_THROW(empty.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, copyAssignSmallerAndBigger)
{
	EXPECT_NO_THROW(medium = small);
	EXPECT_THROW(medium = large, std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, moveConstructorEmptySource)
{
	EXPECT_NO_THROW(Vector(std::move(empty)));
}

TEST_F(TestOneTimeAllocator, resizeAfterMoveConstructorEmptySource)
{
	Vector moveConstructed(std::move(empty));
	EXPECT_NO_THROW(moveConstructed.resize(ALLOCATE_SMALL));
	EXPECT_NO_THROW(empty.resize(ALLOCATE_SMALL));
	EXPECT_THROW(moveConstructed.resize(ALLOCATE_LARGE), std::bad_alloc);
	EXPECT_THROW(empty.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, moveConstructor)
{
	EXPECT_NO_THROW(Vector(std::move(small)));
}

TEST_F(TestOneTimeAllocator, resizeAfterMoveConstructor)
{
	Vector moveConstructed(std::move(small));
	EXPECT_THROW(moveConstructed.resize(ALLOCATE_LARGE), std::bad_alloc);
	EXPECT_THROW(small.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, resizeAfterMoveAssignEmptySourceAndDest)
{
	Vector moveAssigned;
	moveAssigned = std::move(empty);
	ASSERT_NO_THROW(moveAssigned.resize(ALLOCATE_SMALL));
	ASSERT_NO_THROW(empty.resize(ALLOCATE_SMALL));
	EXPECT_THROW(moveAssigned.resize(ALLOCATE_LARGE), std::bad_alloc);
	EXPECT_THROW(empty.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, moveAssignEmptyDest)
{
	ASSERT_NO_THROW(empty = std::move(small));
	EXPECT_THROW(empty.resize(ALLOCATE_LARGE), std::bad_alloc);
	EXPECT_THROW(small.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, moveAssignSmaller)
{
	EXPECT_NO_THROW(medium = std::move(small));
	EXPECT_THROW(small.resize(ALLOCATE_LARGE), std::bad_alloc);
	EXPECT_THROW(medium.resize(ALLOCATE_LARGE), std::bad_alloc);
}

TEST_F(TestOneTimeAllocator, moveAssignBigger)
{
	EXPECT_NO_THROW(small = std::move(medium));
	EXPECT_THROW(small.resize(ALLOCATE_LARGE), std::bad_alloc);
	EXPECT_THROW(medium.resize(ALLOCATE_LARGE), std::bad_alloc);
}
