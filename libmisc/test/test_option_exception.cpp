#include <exception>
#include <string>

#include <gtest/gtest.h>

#include <misc/option_exception.h>

#include "unittest.h"

TEST(TestInvalidOption, constructor)
{
	misc::InvalidOption e {'a'};
	EXPECT_STREQ("Invalid option -a", e.what());
}

TEST(TestInvalidOption, inheritance)
{
	EXPECT_THROW_WHAT(
		(throw misc::InvalidOption {'a'}), std::exception, "Invalid option -a");
	EXPECT_THROW_WHAT((throw misc::InvalidOption {'a'}), misc::OptionError,
		"Invalid option -a");
}

TEST(TestMissingOptionArgument, constructor)
{
	misc::MissingOptionArgument e {'a'};
	EXPECT_STREQ("Option -a requires an argument", e.what());
}

TEST(TestMissingOptionArgument, inheritance)
{
	EXPECT_THROW_WHAT((throw misc::MissingOptionArgument {'a'}), std::exception,
		"Option -a requires an argument");
	EXPECT_THROW_WHAT((throw misc::MissingOptionArgument {'a'}),
		misc::OptionError, "Option -a requires an argument");
}

TEST(TestInvalidOptionArgument, cstrConstructor)
{
	misc::InvalidOptionArgument e {'a', "what"};
	EXPECT_STREQ("Option -a has an invalid argument (what)", e.what());
}

TEST(TestInvalidOptionArgument, stringConstructor)
{
	misc::InvalidOptionArgument e {'a', std::string {"what"}};
	EXPECT_STREQ("Option -a has an invalid argument (what)", e.what());
}

TEST(TestInvalidOptionArgument, inheritance)
{
	EXPECT_THROW_WHAT((throw misc::InvalidOptionArgument {'a', "what"}),
		std::exception, "Option -a has an invalid argument (what)");
	EXPECT_THROW_WHAT((throw misc::InvalidOptionArgument {'a', "what"}),
		misc::OptionError, "Option -a has an invalid argument (what)");
}

TEST(TestRequiredOptionMissing, constructor)
{
	misc::RequiredOptionMissing e {'a'};
	EXPECT_STREQ("Option -a is required", e.what());
}

TEST(TestRequiredOptionMissing, inheritance)
{
	EXPECT_THROW_WHAT((throw misc::RequiredOptionMissing {'a'}), std::exception,
		"Option -a is required");
	EXPECT_THROW_WHAT((throw misc::RequiredOptionMissing {'a'}),
		misc::OptionError, "Option -a is required");
}
