#include <chrono>

#include <gtest/gtest.h>

#include <misc/delay_sequence.h>

using Delays = misc::Delay_sequence<std::chrono::seconds, 3, 1, 7, 1, 5, 9>;

TEST(Delay_sequence, next)
{
	Delays delays;

	ASSERT_EQ(std::chrono::seconds {3}, delays.next());
	ASSERT_EQ(std::chrono::seconds {1}, delays.next());
	ASSERT_EQ(std::chrono::seconds {7}, delays.next());
	ASSERT_EQ(std::chrono::seconds {1}, delays.next());
	ASSERT_EQ(std::chrono::seconds {5}, delays.next());
	ASSERT_EQ(std::chrono::seconds {9}, delays.next());

	ASSERT_EQ(std::chrono::seconds {9}, delays.next());
	ASSERT_EQ(std::chrono::seconds {9}, delays.next());
	ASSERT_EQ(std::chrono::seconds {9}, delays.next());
	ASSERT_EQ(std::chrono::seconds {9}, delays.next());
	ASSERT_EQ(std::chrono::seconds {9}, delays.next());
	ASSERT_EQ(std::chrono::seconds {9}, delays.next());
}

TEST(Delay_sequence, reset)
{
	Delays delays;

	delays.reset();
	ASSERT_EQ(std::chrono::seconds {3}, delays.next());

	delays.next();
	delays.reset();
	ASSERT_EQ(std::chrono::seconds {3}, delays.next());

	delays.next();
	delays.next();
	delays.reset();
	ASSERT_EQ(std::chrono::seconds {3}, delays.next());

	delays.next();
	delays.next();
	delays.next();
	delays.reset();
	ASSERT_EQ(std::chrono::seconds {3}, delays.next());

	delays.next();
	delays.next();
	delays.next();
	delays.next();
	delays.reset();
	ASSERT_EQ(std::chrono::seconds {3}, delays.next());

	delays.next();
	delays.next();
	delays.next();
	delays.next();
	delays.next();
	delays.reset();
	ASSERT_EQ(std::chrono::seconds {3}, delays.next());

	delays.next();
	delays.next();
	delays.next();
	delays.next();
	delays.next();
	delays.next();
	delays.reset();
	ASSERT_EQ(std::chrono::seconds {3}, delays.next());
}
