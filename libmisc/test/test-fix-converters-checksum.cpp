#include <cstdint>
#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <misc/fix/Converters.h>

#include "unittest.h"

TEST(fixChecksumConvertFromString, valid)
{
	for (uint32_t i = 0; i != 256; ++i) {
		auto s = std::to_string(i);
		if (i < 10)
			s = '0' + s;
		if (i < 100)
			s = '0' + s;
		EXPECT_EQ(i, misc::fix::checksumConvert(s));
	}
}

TEST(fixChecksumConvertFromString, badFormat)
{
	for (uint32_t count = 0; count != 10; ++count) {
		if (count == 3)
			continue;
		EXPECT_THROW_WHAT(misc::fix::checksumConvert(std::string(count, '0')),
			std::invalid_argument, "Bad format");
	}
}

TEST(fixChecksumConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT(misc::fix::checksumConvert(std::string({'-', '0', '0'})),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::checksumConvert(std::string({'0', '+', '0'})),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::checksumConvert(std::string({'0', '0', '.'})),
		std::invalid_argument, "Invalid character");

	EXPECT_THROW_WHAT(misc::fix::checksumConvert(std::string({'A', '0', '0'})),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::checksumConvert(std::string({'0', 'B', '0'})),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::checksumConvert(std::string({'0', '0', 'C'})),
		std::invalid_argument, "Invalid character");
}

TEST(fixChecksumConvertFromString, outOfRange)
{
	for (uint32_t i = 256; i != 512; ++i) {
		auto s = std::to_string(i);
		EXPECT_THROW_WHAT(misc::fix::checksumConvert(s), std::invalid_argument,
			"Out of range");
	}
}

TEST(fixChecksumConvertToString, valid)
{
	for (uint32_t i = 0; i != 256; ++i) {
		auto s = std::to_string(i);
		if (i < 10)
			s = '0' + s;
		if (i < 100)
			s = '0' + s;
		EXPECT_EQ(s, misc::fix::checksumConvert(i));
	}
}

TEST(fixChecksumConvertToString, outOfRange)
{
	for (uint32_t i = 256; i != 512; ++i) {
		EXPECT_THROW_WHAT(misc::fix::checksumConvert(i), std::invalid_argument,
			"Out of range");
	}
}
