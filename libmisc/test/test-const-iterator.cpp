#include <string>
#include <type_traits>
#include <vector>

#include <gtest/gtest.h>

#include <misc/const_iterator.h>

#include "unittest.h"

class TestConstIterator : public ::testing::Test {
protected:
	std::vector<std::string> expected {"one", "two", "three", "four", "five"};
	std::vector<std::string> data {expected};

	using iterator = misc::const_iterator<decltype(data)::iterator>;
	iterator iterator_to_front() { return iterator(data.begin()); }
	iterator iterator_to_back() { return iterator(--data.end()); }

	using const_iterator = misc::const_iterator<decltype(data)::const_iterator>;
	const_iterator const_iterator_to_front()
	{
		return const_iterator(data.cbegin());
	}
	const_iterator const_iterator_to_back()
	{
		return const_iterator(--data.cend());
	}

	iterator iter {iterator_to_front()};

	using iterator_base = decltype(data)::iterator;
	iterator_base iterator_base_to_front() { return data.begin(); }
	iterator_base iterator_base_to_back() { return --data.end(); }

	using const_iterator_base = decltype(data)::const_iterator;
	const_iterator_base const_iterator_base_to_front() { return data.cbegin(); }
	const_iterator_base const_iterator_base_to_back() { return --data.cend(); }
};

TEST_F(TestConstIterator, copy)
{
	iterator iter1(iter);
	EXPECT_EQ(iter.base(), iter1.base());
}

TEST_F(TestConstIterator, assignment)
{
	iterator iter1;
	iter1 = iter;
	EXPECT_EQ(iter.base(), iter1.base());
}

TEST_F(TestConstIterator, conversion)
{
	const_iterator citer1(iter);
	EXPECT_EQ(iter.base(), citer1.base());

	const_iterator citer2;
	citer2 = iter;
	EXPECT_EQ(iter.base(), citer2.base());

	auto iter_base = iterator_base_to_front();

	iterator iter3(iter_base);
	EXPECT_EQ(iter_base.base(), iter3.base().base());

	iterator iter4;
	iter4 = iter_base;
	EXPECT_EQ(iter_base.base(), iter4.base().base());

	const_iterator citer5(iter_base);
	EXPECT_EQ(iter_base.base(), citer5.base().base());

	const_iterator citer6;
	citer6 = iter_base;
	EXPECT_EQ(iter_base.base(), citer6.base().base());

	auto citer_base = const_iterator_base_to_front();

	const_iterator citer7(citer_base);
	EXPECT_EQ(citer_base.base(), citer7.base().base());

	const_iterator citer8;
	citer8 = citer_base;
	EXPECT_EQ(citer_base.base(), citer8.base().base());
}

TEST_F(TestConstIterator, dereference)
{
	EXPECT_EQ(expected[0], *iter);
}

TEST_F(TestConstIterator, arrow)
{
	EXPECT_EQ(expected[0], iter->substr());
}

TEST_F(TestConstIterator, index)
{
	EXPECT_EQ(expected[0], iter[0]);
	EXPECT_EQ(expected[4], iter[4]);
	EXPECT_EQ(expected[0], iterator_to_back()[-4]);
}

TEST_F(TestConstIterator, increment)
{
	EXPECT_EQ(expected[0], *iter);
	EXPECT_EQ(expected[1], *++iter);
	EXPECT_EQ(expected[1], *iter);
	EXPECT_EQ(expected[1], *iter++);
	EXPECT_EQ(expected[2], *iter);
}

TEST_F(TestConstIterator, decrement)
{
	iter = iterator_to_back();
	EXPECT_EQ(expected[4], *iter);
	EXPECT_EQ(expected[3], *--iter);
	EXPECT_EQ(expected[3], *iter);
	EXPECT_EQ(expected[3], *iter--);
	EXPECT_EQ(expected[2], *iter);
}

TEST_F(TestConstIterator, addition)
{
	EXPECT_EQ(expected[0], *iter);
	EXPECT_EQ(expected[2], *(iter += 2));
	EXPECT_EQ(expected[2], *iter);
	EXPECT_EQ(expected[4], *(iter + 2));
	EXPECT_EQ(expected[2], *iter);
	EXPECT_EQ(expected[4], *(2 + iter));
	EXPECT_EQ(expected[2], *iter);
}

namespace {

template <typename IteratorFront, typename IteratorBack>
void testSubtraction(const IteratorFront& front, const IteratorBack& back)
{
	EXPECT_EQ(0, front - front);
	EXPECT_EQ(4, back - front);
	EXPECT_EQ(-4, front - back);
}

} // namespace

TEST_F(TestConstIterator, subtraction)
{
	iter = iterator_to_back();
	EXPECT_EQ(expected[4], *iter);
	EXPECT_EQ(expected[2], *(iter -= 2));
	EXPECT_EQ(expected[2], *iter);
	EXPECT_EQ(expected[0], *(iter - 2));
	EXPECT_EQ(expected[2], *iter);

	testSubtraction(iterator_to_front(), iterator_to_back());
	testSubtraction(iterator_to_front(), const_iterator_to_back());
	testSubtraction(const_iterator_to_front(), iterator_to_back());
	testSubtraction(const_iterator_to_front(), const_iterator_to_back());

	testSubtraction(iterator_base_to_front(), iterator_to_back());
	testSubtraction(iterator_base_to_front(), const_iterator_to_back());
	testSubtraction(const_iterator_base_to_front(), iterator_to_back());
	testSubtraction(const_iterator_base_to_front(), const_iterator_to_back());

	testSubtraction(iterator_to_front(), iterator_base_to_back());
	testSubtraction(iterator_to_front(), const_iterator_base_to_back());
	testSubtraction(const_iterator_to_front(), iterator_base_to_back());
	testSubtraction(const_iterator_to_front(), const_iterator_base_to_back());
}

TEST_F(TestConstIterator, relational)
{
	testRelational(iterator_to_front(), iterator_to_back());
	testRelational(iterator_to_front(), const_iterator_to_back());
	testRelational(const_iterator_to_front(), iterator_to_back());
	testRelational(const_iterator_to_front(), const_iterator_to_back());

	testRelational(iterator_base_to_front(), iterator_to_back());
	testRelational(iterator_base_to_front(), const_iterator_to_back());
	testRelational(const_iterator_base_to_front(), iterator_to_back());
	testRelational(const_iterator_base_to_front(), const_iterator_to_back());

	testRelational(iterator_to_front(), iterator_base_to_back());
	testRelational(iterator_to_front(), const_iterator_base_to_back());
	testRelational(const_iterator_to_front(), iterator_base_to_back());
	testRelational(const_iterator_to_front(), const_iterator_base_to_back());
}

TEST_F(TestConstIterator, make)
{
	auto iter = misc::make_const_iterator(data.begin());
	EXPECT_EQ(data.begin(), iter.base());
	EXPECT_TRUE((std::is_same_v<iterator, decltype(iter)>));

	auto citer = misc::make_const_iterator(data.cbegin());
	EXPECT_EQ(data.cbegin(), citer.base());
	EXPECT_TRUE((std::is_same_v<const_iterator, decltype(citer)>));

	auto range = misc::constify(data.begin(), data.end());
	EXPECT_EQ(data.begin(), range.begin().base());
	EXPECT_EQ(data.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	auto crange = misc::constify(data.cbegin(), data.cend());
	EXPECT_EQ(data.cbegin(), crange.begin().base());
	EXPECT_EQ(data.cend(), crange.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST_F(TestConstIterator, range)
{
	auto range = misc::constify(data);
	EXPECT_EQ(data.begin(), range.begin().base());
	EXPECT_EQ(data.end(), range.end().base());
	EXPECT_TRUE((std::is_same_v<misc::range<iterator>, decltype(range)>));

	const decltype(data)& cdata = data;
	auto crange = misc::constify(cdata);
	EXPECT_EQ(cdata.begin(), crange.begin().base());
	EXPECT_EQ(cdata.end(), crange.end().base());
	EXPECT_TRUE(
		(std::is_same_v<misc::range<const_iterator>, decltype(crange)>));
}

TEST(TestConstIteratorValue, type)
{
	std::vector<std::string> data;

	auto iter = misc::make_const_iterator(data.begin());
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::iterator_category,
		decltype(iter)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::difference_type,
		decltype(iter)::difference_type>));
	EXPECT_TRUE((std::is_same_v<std::string, decltype(iter)::value_type>));
	EXPECT_TRUE((std::is_same_v<const std::string*, decltype(iter)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(iter)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*iter)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(iter.operator->())>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(iter[0])>));

	auto citer = misc::make_const_iterator(data.cbegin());
	EXPECT_TRUE(
		(std::is_same_v<decltype(data)::const_iterator::iterator_category,
			decltype(citer)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::const_iterator::difference_type,
		decltype(citer)::difference_type>));
	EXPECT_TRUE((std::is_same_v<std::string, decltype(citer)::value_type>));
	EXPECT_TRUE((std::is_same_v<const std::string*, decltype(citer)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string&, decltype(citer)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(*citer)>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(citer.operator->())>));
	EXPECT_TRUE((std::is_same_v<const std::string&, decltype(citer[0])>));
}

TEST(TestConstIteratorPointer, type)
{
	std::vector<std::string*> data;

	auto iter = misc::make_const_iterator(data.begin());
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::iterator_category,
		decltype(iter)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::difference_type,
		decltype(iter)::difference_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(iter)::value_type>));
	EXPECT_TRUE((std::is_same_v<std::string* const*, decltype(iter)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<std::string* const&, decltype(iter)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string* const&, decltype(*iter)>));
	EXPECT_TRUE(
		(std::is_same_v<std::string* const*, decltype(iter.operator->())>));
	EXPECT_TRUE((std::is_same_v<std::string* const&, decltype(iter[0])>));

	auto citer = misc::make_const_iterator(data.cbegin());
	EXPECT_TRUE(
		(std::is_same_v<decltype(data)::const_iterator::iterator_category,
			decltype(citer)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::const_iterator::difference_type,
		decltype(citer)::difference_type>));
	EXPECT_TRUE((std::is_same_v<std::string*, decltype(citer)::value_type>));
	EXPECT_TRUE(
		(std::is_same_v<std::string* const*, decltype(citer)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<std::string* const&, decltype(citer)::reference>));
	EXPECT_TRUE((std::is_same_v<std::string* const&, decltype(*citer)>));
	EXPECT_TRUE(
		(std::is_same_v<std::string* const*, decltype(citer.operator->())>));
	EXPECT_TRUE((std::is_same_v<std::string* const&, decltype(citer[0])>));
}

TEST(TestConstIteratorPointerToConst, type)
{
	std::vector<const std::string*> data;

	auto iter = misc::make_const_iterator(data.begin());
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::iterator_category,
		decltype(iter)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::iterator::difference_type,
		decltype(iter)::difference_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(iter)::value_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string* const*, decltype(iter)::pointer>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string* const&, decltype(iter)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string* const&, decltype(*iter)>));
	EXPECT_TRUE((std::is_same_v<const std::string* const*,
		decltype(iter.operator->())>));
	EXPECT_TRUE((std::is_same_v<const std::string* const&, decltype(iter[0])>));

	auto citer = misc::make_const_iterator(data.cbegin());
	EXPECT_TRUE(
		(std::is_same_v<decltype(data)::const_iterator::iterator_category,
			decltype(citer)::iterator_category>));
	EXPECT_TRUE((std::is_same_v<decltype(data)::const_iterator::difference_type,
		decltype(citer)::difference_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string*, decltype(citer)::value_type>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string* const*, decltype(citer)::pointer>));
	EXPECT_TRUE((
		std::is_same_v<const std::string* const&, decltype(citer)::reference>));
	EXPECT_TRUE((std::is_same_v<const std::string* const&, decltype(*citer)>));
	EXPECT_TRUE((std::is_same_v<const std::string* const*,
		decltype(citer.operator->())>));
	EXPECT_TRUE(
		(std::is_same_v<const std::string* const&, decltype(citer[0])>));
}
