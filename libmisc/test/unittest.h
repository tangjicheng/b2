#ifndef UNITTEST_H
#define UNITTEST_H

#include <sstream>
#include <string>

#define EXPECT_THROW_WHAT(statement, expected_exception, expected_what) \
	do {                                                                \
		try {                                                           \
			statement;                                                  \
			ADD_FAILURE()                                               \
				<< "Expected: " #statement                              \
				   " throws an exception of type " #expected_exception  \
				   " with what " #expected_what                         \
				   ".\n  Actual: it throws nothing.";                   \
		}                                                               \
		catch (const expected_exception& e) {                           \
			EXPECT_STREQ(expected_what, e.what())                       \
				<< "Expected: " #statement                              \
				   " throws an exception of type " #expected_exception  \
				   " with what " #expected_what                         \
				   ".\n  Actual: the what is different.";               \
		}                                                               \
		catch (...) {                                                   \
			ADD_FAILURE()                                               \
				<< "Expected: " #statement                              \
				   " throws an exception of type " #expected_exception  \
				   " with what " #expected_what                         \
				   ".\n  Actual: it throws a different type.";          \
		}                                                               \
	} while (false)

#define ASSERT_THROW_WHAT(statement, expected_exception, expected_what)   \
	do {                                                                  \
		try {                                                             \
			statement;                                                    \
			FAIL() << "Expected: " #statement                             \
					  " throws an exception of type " #expected_exception \
					  " with what " #expected_what                        \
					  ".\n  Actual: it throws nothing.";                  \
		}                                                                 \
		catch (const expected_exception& e) {                             \
			ASSERT_STREQ(expected_what, e.what())                         \
				<< "Expected: " #statement                                \
				   " throws an exception of type " #expected_exception    \
				   " with what " #expected_what                           \
				   ".\n  Actual: the what is different.";                 \
		}                                                                 \
		catch (...) {                                                     \
			FAIL() << "Expected: " #statement                             \
					  " throws an exception of type " #expected_exception \
					  " with what " #expected_what                        \
					  ".\n  Actual: it throws a different type.";         \
		}                                                                 \
	} while (false)

#define EXPECT_OSTREAM(expected, object) \
	EXPECT_PRED_FORMAT2(verifyOstream, expected, object)

template <typename T>
::testing::AssertionResult verifyOstream(const char*, const char* objectExpr,
	const std::string& expected, const T& object)
{
	std::ostringstream os;
	os << object;
	if (os.str() == expected)
		return ::testing::AssertionSuccess();
	return ::testing::AssertionFailure()
		<< "Stream output of " << objectExpr << " does not match"
		<< "\n  Expected: " << expected << "\n  Actual: " << os.str();
}

template <typename T, typename U>
void testEquality(const T& low, const U& high)
{
	EXPECT_TRUE(low == low);
	EXPECT_FALSE(low == high);
	EXPECT_FALSE(high == low);

	EXPECT_FALSE(low != low);
	EXPECT_TRUE(low != high);
	EXPECT_TRUE(high != low);
}

template <typename T, typename U>
void testRelational(const T& low, const U& high)
{
	testEquality(low, high);

	EXPECT_FALSE(low < low);
	EXPECT_TRUE(low < high);
	EXPECT_FALSE(high < low);

	EXPECT_FALSE(low > low);
	EXPECT_FALSE(low > high);
	EXPECT_TRUE(high > low);

	EXPECT_TRUE(low <= low);
	EXPECT_TRUE(low <= high);
	EXPECT_FALSE(high <= low);

	EXPECT_TRUE(low >= low);
	EXPECT_FALSE(low >= high);
	EXPECT_TRUE(high >= low);
}

#endif // UNITTEST_H
