#include <chrono>
#include <memory>
#include <optional>
#include <string>
#include <thread>
#include <utility>

#include <gtest/gtest.h>

#include <misc/TimedQueue.h>

#include "unittest.h"

TEST(TestTimedQueue, constructor)
{
	misc::TimedQueue<std::string> q;

	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

TEST(TestTimedQueue, push)
{
	misc::TimedQueue<std::string> q;

	std::string s1("one");
	ASSERT_NO_THROW(q.push(s1));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.push(std::string("two")));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	std::string s3("three");
	ASSERT_NO_THROW(q.push(std::move(s3)));
	EXPECT_EQ(3u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("one", q.try_pop());
	EXPECT_EQ("two", q.try_pop());
	EXPECT_EQ("three", q.try_pop());
}

TEST(TestTimedQueue, emplace)
{
	misc::TimedQueue<std::string> q;

	ASSERT_NO_THROW(q.emplace("one"));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace(3, '2'));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace("threeTHREE", 5));
	EXPECT_EQ(3u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace(std::string("FOURfourFOUR"), 4, 4));
	EXPECT_EQ(4u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace(std::string("five")));
	EXPECT_EQ(5u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("one", q.try_pop());
	EXPECT_EQ("222", q.try_pop());
	EXPECT_EQ("three", q.try_pop());
	EXPECT_EQ("four", q.try_pop());
	EXPECT_EQ("five", q.try_pop());
}

namespace {

void unblockPopByPush(misc::TimedQueue<std::string>& q)
{
	std::thread t([&] {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		ASSERT_NO_THROW(q.push("unblock"));
	});
	EXPECT_EQ("unblock", q.pop());
	t.join();
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

void unblockPopByUnblock(misc::TimedQueue<std::string>& q)
{
	std::thread t([&] {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		ASSERT_NO_THROW(q.unblock());
	});
	EXPECT_EQ(std::nullopt, q.pop());
	t.join();
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

} // namespace

TEST(TestTimedQueue, pop)
{
	misc::TimedQueue<std::string> q;

	unblockPopByPush(q);
	unblockPopByUnblock(q);

	ASSERT_NO_THROW(q.push("one"));
	ASSERT_NO_THROW(q.push("two"));
	ASSERT_NO_THROW(q.push("three"));

	EXPECT_EQ(3u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("one", q.pop());
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("two", q.pop());
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("three", q.pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	unblockPopByPush(q);
	unblockPopByUnblock(q);
}

TEST(TestTimedQueue, tryPop)
{
	misc::TimedQueue<std::string> q;

	EXPECT_EQ(std::nullopt, q.try_pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	ASSERT_NO_THROW(q.push("one"));
	ASSERT_NO_THROW(q.push("two"));
	ASSERT_NO_THROW(q.push("three"));

	EXPECT_EQ(3u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("one", q.try_pop());
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("two", q.try_pop());
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("three", q.try_pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	EXPECT_EQ(std::nullopt, q.try_pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

namespace {

template <typename Duration>
std::chrono::system_clock::time_point after(Duration relTime)
{
	return std::chrono::system_clock::now() + relTime;
}

void unblockTryPopUntilByPush(misc::TimedQueue<std::string>& q)
{
	std::thread t([&] {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		ASSERT_NO_THROW(q.push("unblock"));
	});
	EXPECT_EQ("unblock", q.try_pop_until(after(std::chrono::seconds(10))));
	t.join();
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

void unblockTryPopUntilByUnblock(misc::TimedQueue<std::string>& q)
{
	std::thread t([&] {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		ASSERT_NO_THROW(q.unblock());
	});
	EXPECT_EQ(std::nullopt, q.try_pop_until(after(std::chrono::seconds(10))));
	t.join();
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

void tryPopUntilTimeout(misc::TimedQueue<std::string>& q)
{
	EXPECT_EQ(
		std::nullopt, q.try_pop_until(after(std::chrono::milliseconds(100))));
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

} // namespace

TEST(TestTimedQueue, tryPopUntil)
{
	misc::TimedQueue<std::string> q;

	unblockTryPopUntilByPush(q);
	unblockTryPopUntilByUnblock(q);
	tryPopUntilTimeout(q);

	ASSERT_NO_THROW(q.push("one"));
	ASSERT_NO_THROW(q.push("two"));
	ASSERT_NO_THROW(q.push("three"));

	EXPECT_EQ(3u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("one", q.try_pop_until(after(std::chrono::seconds(1))));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("two", q.try_pop_until(after(std::chrono::seconds(1))));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("three", q.try_pop_until(after(std::chrono::seconds(1))));
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	unblockTryPopUntilByPush(q);
	unblockTryPopUntilByUnblock(q);
	tryPopUntilTimeout(q);
}

namespace {

void unblockTryPopForByPush(misc::TimedQueue<std::string>& q)
{
	std::thread t([&] {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		ASSERT_NO_THROW(q.push("unblock"));
	});
	EXPECT_EQ("unblock", q.try_pop_for(std::chrono::seconds(10)));
	t.join();
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

void unblockTryPopForByUnblock(misc::TimedQueue<std::string>& q)
{
	std::thread t([&] {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		ASSERT_NO_THROW(q.unblock());
	});
	EXPECT_EQ(std::nullopt, q.try_pop_for(std::chrono::seconds(10)));
	t.join();
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

void tryPopForTimeout(misc::TimedQueue<std::string>& q)
{
	EXPECT_EQ(std::nullopt, q.try_pop_for(std::chrono::milliseconds(100)));
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

} // namespace

TEST(TestTimedQueue, tryPopFor)
{
	misc::TimedQueue<std::string> q;

	unblockTryPopForByPush(q);
	unblockTryPopForByUnblock(q);
	tryPopForTimeout(q);

	ASSERT_NO_THROW(q.push("one"));
	ASSERT_NO_THROW(q.push("two"));
	ASSERT_NO_THROW(q.push("three"));

	EXPECT_EQ(3u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("one", q.try_pop_for(std::chrono::seconds(1)));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("two", q.try_pop_for(std::chrono::seconds(1)));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("three", q.try_pop_for(std::chrono::seconds(1)));
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	unblockTryPopForByPush(q);
	unblockTryPopForByUnblock(q);
	tryPopForTimeout(q);
}

namespace {

void push(misc::TimedQueue<std::string>& q, size_t count)
{
	for (size_t i = 0; i != count; ++i) {
		ASSERT_NO_THROW(q.push(std::to_string(i)));
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

void pop(misc::TimedQueue<std::string>& q, size_t count)
{
	for (size_t i = 0; i != count;) {
		std::optional<std::string> value;
		switch (i % 4) {
		case 0:
			value = q.pop();
			break;
		case 1:
			value = q.try_pop();
			break;
		case 2:
			value = q.try_pop_until(after(std::chrono::milliseconds(10)));
			break;
		case 3:
			value = q.try_pop_for(std::chrono::milliseconds(10));
			break;
		default:
			break;
		}
		if (!value.has_value())
			continue;
		EXPECT_EQ(std::to_string(i), value);
		++i;
	}
}

} // namespace

TEST(TestTimedQueue, multiThreaded)
{
	misc::TimedQueue<std::string> q;

	std::thread pushThread([&] { push(q, 1000); });
	std::thread popThread([&] { pop(q, 1000); });

	pushThread.join();
	popThread.join();
}

TEST(TestTimedQueue, move_only)
{
	misc::TimedQueue<std::unique_ptr<int>> q;

	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	ASSERT_NO_THROW(q.push(std::make_unique<int>(1)));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	auto i2 = std::make_unique<int>(2);
	ASSERT_NO_THROW(q.push(std::move(i2)));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ(1, **q.pop());
	EXPECT_EQ(2, **q.pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	ASSERT_NO_THROW(q.emplace(new int(3)));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace(std::make_unique<int>(4)));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ(3, **q.try_pop());
	EXPECT_EQ(4, **q.try_pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	auto p5 = new int(5);
	auto p6 = new int(6);
	ASSERT_NO_THROW(q.emplace(p5));
	ASSERT_NO_THROW(q.emplace(p6));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	auto i5 = q.try_pop_until(after(std::chrono::milliseconds(10)));
	EXPECT_EQ(5, **i5);
	EXPECT_EQ(p5, i5->get());
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	auto i6 = q.try_pop_for(std::chrono::milliseconds(10));
	EXPECT_EQ(6, **i6);
	EXPECT_EQ(p6, i6->get());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	EXPECT_EQ(std::nullopt, q.try_pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}
