#include <array>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <future>
#include <thread>
#include <vector>

#include <gtest/gtest.h>

#include <misc/BinaryLog.h>
#include <misc/ThreadedReactor.h>
#include <misc/range.h>

namespace {

using BinaryMessage = std::vector<uint8_t>;

constexpr size_t BINARY_MESSAGE_MAX_SIZE = 256;

void fillRandom(BinaryMessage& message)
{
	constexpr char alnum[]
		= "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	auto len = std::max<size_t>(10, rand() % BINARY_MESSAGE_MAX_SIZE);
	message.resize(len);
	for (auto& ch : misc::make_range(message.data(), message.data() + len))
		ch = alnum[rand() % (sizeof(alnum) - 1)];
}

misc::range<BinaryMessage*> makeMessageRange(BinaryMessage& message)
{
	return misc::make_range(&message, &message + 1);
}

template <typename Data>
void writeAll(misc::BinaryLogWriter& logWriter, const Data& data)
{
	logWriter.write(misc::make_range(data.begin(), data.end()),
		[&](size_t messagesWritten, int error) {
			ASSERT_EQ(data.size(), messagesWritten);
			ASSERT_EQ(0, error);
		});
}

template <typename Data>
void writeLog(misc::Reactor& reactor, const std::string& logFilename,
	misc::BinaryLogIndex& index, bool reuse, const Data& data)
{
	misc::BinaryLogWriter logWriter(reactor);
	logWriter.open(logFilename, index, reuse);
	writeAll(logWriter, data);
	logWriter.close();
}

template <typename Data>
struct VerifyCtx {
	const Data& data;
	size_t i {0};
	BinaryMessage message;
	std::promise<size_t> nMessage;

	explicit VerifyCtx(const Data& data)
		: data(data)
	{
	}
};

template <typename Data>
void verifyNext(
	misc::BinaryLogReader& logReader, std::shared_ptr<VerifyCtx<Data>> ctx)
{
	logReader.read(makeMessageRange(ctx->message),
		[&, ctx](size_t messagesRead, int error) {
			ASSERT_EQ(0, error);
			if (messagesRead == 0) {
				ctx->nMessage.set_value(ctx->i);
				return;
			}
			auto i = ctx->i++ % ctx->data.size();
			ASSERT_EQ(1u, messagesRead);
			ASSERT_EQ(ctx->data[i], ctx->message);
			ctx->message.resize(BINARY_MESSAGE_MAX_SIZE);
			verifyNext(logReader, ctx);
		});
}

template <typename Data>
std::future<size_t> verifyAll(
	misc::BinaryLogReader& logReader, const Data& data)
{
	auto ctx = std::make_shared<VerifyCtx<Data>>(data);
	ctx->message.resize(BINARY_MESSAGE_MAX_SIZE);
	verifyNext(logReader, ctx);
	return ctx->nMessage.get_future();
}

class BinaryLogTest : public ::testing::Test {
protected:
	static misc::ThreadedReactor reactor;
	std::string m_logFilename;
	std::array<BinaryMessage, 1000> m_data;

	static void SetUpTestCase() { reactor.start(); }

	static void TearDownTestCase() { reactor.stop(); }

	void SetUp() override
	{
		char templ[] = "test-binary-log.XXXXXX";
		mkstemp(templ);
		m_logFilename = templ;
		for (auto& message : m_data)
			fillRandom(message);
	}

	void TearDown() override { remove(m_logFilename.c_str()); }
};

misc::ThreadedReactor BinaryLogTest::reactor;

} // namespace

TEST_F(BinaryLogTest, creation)
{
	misc::BinaryLogIndex index(100, 10);

	writeLog(reactor.get(), m_logFilename, index, false, m_data);

	misc::BinaryLogReader logReader(reactor.get());
	logReader.open(m_logFilename, index);
	auto nMessage = verifyAll(logReader, m_data);
	ASSERT_EQ(m_data.size(), nMessage.get());

	logReader.close();
}

TEST_F(BinaryLogTest, reuse)
{
	misc::BinaryLogIndex index(100, 10);

	writeLog(reactor.get(), m_logFilename, index, false, m_data);
	writeLog(reactor.get(), m_logFilename, index, true, m_data);

	misc::BinaryLogReader logReader(reactor.get());
	logReader.open(m_logFilename, index);
	auto nMessage = verifyAll(logReader, m_data);
	ASSERT_EQ(2 * m_data.size(), nMessage.get());

	logReader.close();
}

TEST_F(BinaryLogTest, write_corner_cases)
{
	misc::BinaryLogIndex index(100, 10);

	misc::BinaryLogWriter logWriter(reactor.get());
	logWriter.open(m_logFilename, index);

	BinaryMessage message(65535, 'a');
	logWriter.write(
		makeMessageRange(message), [&](size_t messagesWritten, int error) {
			ASSERT_EQ(0, error);
			ASSERT_EQ(1u, messagesWritten);
			message.resize(65536);
		});
	// Too big
	logWriter.write(
		makeMessageRange(message), [&](size_t messagesWritten, int error) {
			ASSERT_EQ(EIO, error);
			ASSERT_EQ(0u, messagesWritten);
		});

	logWriter.close();
}

TEST_F(BinaryLogTest, read_corner_cases)
{
	misc::BinaryLogIndex index(100, 10);

	writeLog(reactor.get(), m_logFilename, index, false, m_data);

	misc::BinaryLogReader logReader(reactor.get());
	logReader.open(m_logFilename, index);
	auto nMessage = verifyAll(logReader, m_data);
	ASSERT_EQ(m_data.size(), nMessage.get());
	BinaryMessage message;
	message.resize(BINARY_MESSAGE_MAX_SIZE);
	logReader.read(
		0, makeMessageRange(message), [&](size_t messagesRead, int error) {
			ASSERT_EQ(0, error);
			ASSERT_EQ(1u, messagesRead);
			ASSERT_EQ(m_data[0], message);
			message.resize(BINARY_MESSAGE_MAX_SIZE);
		});
	logReader.read(
		makeMessageRange(message), [&](size_t messagesRead, int error) {
			ASSERT_EQ(0, error);
			ASSERT_EQ(1u, messagesRead);
			ASSERT_EQ(m_data[1], message);
			message.resize(BINARY_MESSAGE_MAX_SIZE);
		});
	// Out of range
	logReader.read(
		1000, makeMessageRange(message), [&](size_t messagesRead, int error) {
			ASSERT_EQ(EIO, error);
			ASSERT_EQ(0u, messagesRead);
		});
	logReader.read(
		999, makeMessageRange(message), [&](size_t messagesRead, int error) {
			ASSERT_EQ(0, error);
			ASSERT_EQ(1u, messagesRead);
			ASSERT_EQ(m_data[999], message);
			message.resize(BINARY_MESSAGE_MAX_SIZE);
		});
	// End of file
	logReader.read(
		makeMessageRange(message), [&](size_t messagesRead, int error) {
			ASSERT_EQ(0, error);
			ASSERT_EQ(0u, messagesRead);
		});

	reactor.callNow([] {});
	logReader.close();
}

TEST_F(BinaryLogTest, random_reads)
{
	misc::BinaryLogIndex index(100, 10);

	writeLog(reactor.get(), m_logFilename, index, false, m_data);

	misc::BinaryLogReader logReader(reactor.get());
	logReader.open(m_logFilename, index);
	constexpr auto READ_SIZE = 50;
	constexpr auto NREADS = 10000;
	std::array<BinaryMessage, READ_SIZE> messages;
	for (auto& message : messages)
		message.resize(BINARY_MESSAGE_MAX_SIZE);
	for (auto count = NREADS; count != 0; --count) {
		auto start = rand() % m_data.size();
		logReader.read(start,
			misc::make_range(messages.begin(), messages.end()),
			[&, this, start](size_t messagesRead, int error) {
				ASSERT_EQ(0, error);
				auto expectedMessagesRead
					= std::min(start + messages.size(), m_data.size()) - start;
				ASSERT_EQ(expectedMessagesRead, messagesRead);
				for (size_t i = 0; i != messagesRead; ++i)
					ASSERT_EQ(m_data[i + start], messages[i]);
				for (auto& message : messages)
					message.resize(BINARY_MESSAGE_MAX_SIZE);
			});
	}

	reactor.callNow([] {});
	logReader.close();
}
