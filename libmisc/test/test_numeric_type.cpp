#include <cstdint>
#include <map>
#include <sstream>
#include <type_traits>
#include <unordered_map>

#include <gtest/gtest.h>

#include <misc/numeric_type.h>

namespace {

MISC_NUMERIC_TYPE_DECL(UInt32_0, uint32_t, 0)
MISC_NUMERIC_TYPE_DECL(UInt32_1, uint32_t, 1)
MISC_NUMERIC_TYPE_DECL(UInt32_2, uint32_t, 2)
MISC_NUMERIC_TYPE_DECL(UInt32_3, uint32_t, 3)
MISC_NUMERIC_TYPE_DECL(UInt32_4, uint32_t, 4)
MISC_NUMERIC_TYPE_DECL(UInt32_5, uint32_t, 5)
MISC_NUMERIC_TYPE_DECL(UInt32_6, uint32_t, 6)
MISC_NUMERIC_TYPE_DECL(UInt32_7, uint32_t, 7)
MISC_NUMERIC_TYPE_DECL(UInt32_8, uint32_t, 8)
MISC_NUMERIC_TYPE_DECL(UInt32_9, uint32_t, 9)
MISC_NUMERIC_TYPE_DECL(UInt32_10, uint32_t, 10)

MISC_NUMERIC_TYPE_DECL(Int32_0, int32_t, 0)
MISC_NUMERIC_TYPE_DECL(Int32_1, int32_t, 1)
MISC_NUMERIC_TYPE_DECL(Int32_2, int32_t, 2)
MISC_NUMERIC_TYPE_DECL(Int32_3, int32_t, 3)
MISC_NUMERIC_TYPE_DECL(Int32_4, int32_t, 4)
MISC_NUMERIC_TYPE_DECL(Int32_5, int32_t, 5)
MISC_NUMERIC_TYPE_DECL(Int32_6, int32_t, 6)
MISC_NUMERIC_TYPE_DECL(Int32_7, int32_t, 7)
MISC_NUMERIC_TYPE_DECL(Int32_8, int32_t, 8)
MISC_NUMERIC_TYPE_DECL(Int32_9, int32_t, 9)
MISC_NUMERIC_TYPE_DECL(Int32_10, int32_t, 10)

MISC_NUMERIC_TYPE_DECL(UInt64_0, uint64_t, 0)
MISC_NUMERIC_TYPE_DECL(UInt64_1, uint64_t, 1)
MISC_NUMERIC_TYPE_DECL(UInt64_2, uint64_t, 2)
MISC_NUMERIC_TYPE_DECL(UInt64_3, uint64_t, 3)
MISC_NUMERIC_TYPE_DECL(UInt64_4, uint64_t, 4)
MISC_NUMERIC_TYPE_DECL(UInt64_5, uint64_t, 5)
MISC_NUMERIC_TYPE_DECL(UInt64_6, uint64_t, 6)
MISC_NUMERIC_TYPE_DECL(UInt64_7, uint64_t, 7)
MISC_NUMERIC_TYPE_DECL(UInt64_8, uint64_t, 8)
MISC_NUMERIC_TYPE_DECL(UInt64_9, uint64_t, 9)
MISC_NUMERIC_TYPE_DECL(UInt64_10, uint64_t, 10)

MISC_NUMERIC_TYPE_DECL(Int64_0, int64_t, 0)
MISC_NUMERIC_TYPE_DECL(Int64_1, int64_t, 1)
MISC_NUMERIC_TYPE_DECL(Int64_2, int64_t, 2)
MISC_NUMERIC_TYPE_DECL(Int64_3, int64_t, 3)
MISC_NUMERIC_TYPE_DECL(Int64_4, int64_t, 4)
MISC_NUMERIC_TYPE_DECL(Int64_5, int64_t, 5)
MISC_NUMERIC_TYPE_DECL(Int64_6, int64_t, 6)
MISC_NUMERIC_TYPE_DECL(Int64_7, int64_t, 7)
MISC_NUMERIC_TYPE_DECL(Int64_8, int64_t, 8)
MISC_NUMERIC_TYPE_DECL(Int64_9, int64_t, 9)
MISC_NUMERIC_TYPE_DECL(Int64_10, int64_t, 10)

MISC_NUMERIC_TYPE_DECIMALS_UNSPECIFIED_DECL(UInt32_x, uint32_t)
MISC_NUMERIC_TYPE_DECIMALS_UNSPECIFIED_DECL(Int32_x, int32_t)

MISC_NUMERIC_TYPE_DECIMALS_UNSPECIFIED_DECL(UInt64_x, uint64_t)
MISC_NUMERIC_TYPE_DECIMALS_UNSPECIFIED_DECL(Int64_x, int64_t)

MISC_NUMERIC_TYPE_DECL(
	Int32_5_twin, Int32_5::Underlying_type, Int32_5::decimals())
MISC_NUMERIC_TYPE_DECIMALS_UNSPECIFIED_DECL(
	Int64_x_twin, Int64_x::Underlying_type)

MISC_NUMERIC_TYPE_BASED_ON_DECL(Int32_5_clone, Int32_5)
MISC_NUMERIC_TYPE_BASED_ON_DECL(Int64_x_clone, Int64_x)

constexpr UInt64_4 operator*(UInt32_4 lhs, UInt32_0 rhs)
{
	return UInt64_4(static_cast<uint64_t>(lhs.value()) * rhs.value());
}

} // namespace

namespace space {

MISC_NUMERIC_TYPE_DECL(Ship, int32_t, 0)
MISC_NUMERIC_TYPE_DECL(Alien, int32_t, 0)

inline bool operator*(Ship, Alien)
{
	return true;
}

} // namespace space

TEST(Numeric_type, parameters_helper)
{
	EXPECT_TRUE((std::is_same_v<Int32_5_tag,
		misc::details::Numeric_type_parameters<Int32_5>::Tag>));
	EXPECT_TRUE((std::is_same_v<Int64_x_tag,
		misc::details::Numeric_type_parameters<Int64_x>::Tag>));

	EXPECT_TRUE((std::is_same_v<int32_t,
		misc::details::Numeric_type_parameters<Int32_5>::Underlying_type>));
	EXPECT_TRUE((std::is_same_v<int64_t,
		misc::details::Numeric_type_parameters<Int64_x>::Underlying_type>));

	EXPECT_EQ(5u, misc::details::Numeric_type_parameters<Int32_5>::decimals);
	EXPECT_EQ(
		4294967295u, misc::details::Numeric_type_parameters<Int64_x>::decimals);
}

TEST(Numeric_type, macros)
{
	EXPECT_TRUE((std::is_same_v<int32_t, Int32_5::Underlying_type>));
	EXPECT_TRUE(Int32_5::decimals_specified());
	EXPECT_EQ(5u, Int32_5::decimals());

	EXPECT_TRUE((std::is_same_v<int64_t, Int64_x::Underlying_type>));
	EXPECT_FALSE(Int64_x::decimals_specified());

	EXPECT_FALSE((std::is_same_v<Int32_5, Int32_5_twin>));
	EXPECT_TRUE((std::is_same_v<Int32_5::Underlying_type,
		Int32_5_twin::Underlying_type>));
	EXPECT_EQ(
		Int32_5::decimals_specified(), Int32_5_twin::decimals_specified());
	EXPECT_EQ(Int32_5::decimals(), Int32_5_twin::decimals());

	EXPECT_FALSE((std::is_same_v<Int64_x, Int64_x_twin>));
	EXPECT_TRUE((std::is_same_v<Int64_x::Underlying_type,
		Int64_x_twin::Underlying_type>));
	EXPECT_EQ(
		Int64_x::decimals_specified(), Int64_x_twin::decimals_specified());

	EXPECT_FALSE((std::is_same_v<Int32_5, Int32_5_clone>));
	EXPECT_TRUE((std::is_same_v<Int32_5::Underlying_type,
		Int32_5_clone::Underlying_type>));
	EXPECT_EQ(
		Int32_5::decimals_specified(), Int32_5_clone::decimals_specified());
	EXPECT_EQ(Int32_5::decimals(), Int32_5_clone::decimals());

	EXPECT_FALSE((std::is_same_v<Int64_x, Int64_x_clone>));
	EXPECT_TRUE((std::is_same_v<Int64_x::Underlying_type,
		Int64_x_clone::Underlying_type>));
	EXPECT_EQ(
		Int64_x::decimals_specified(), Int64_x_clone::decimals_specified());
}

TEST(Numeric_type, default_constructor)
{
	// Unsigned 32 bit.
	{
		UInt32_0 num;
		EXPECT_EQ(0u, num.value());
	}
	{
		constexpr UInt32_0 num;
		EXPECT_EQ(0u, num.value());
	}
}

TEST(Numeric_type, value_constructor)
{
	// Unsigned 32 bit.
	{
		UInt32_0 num(10);
		EXPECT_EQ(10u, num.value());
	}
	{
		constexpr UInt32_0 num(20);
		EXPECT_EQ(20u, num.value());
	}
}

TEST(Numeric_type, value)
{
	// Unsigned 32 bit.
	{
		UInt32_0 num;
		auto value = num.value();
		EXPECT_EQ(0u, value);
	}
	{
		UInt32_0 num(10);
		auto value = num.value();
		EXPECT_EQ(10u, value);
	}
	{
		constexpr UInt32_0 num;
		constexpr auto value = num.value();
		EXPECT_EQ(0u, value);
	}
	{
		constexpr UInt32_0 num(20);
		constexpr auto value = num.value();
		EXPECT_EQ(20u, value);
	}
}

TEST(Numeric_type, decimals_specified)
{
	// 32 bit.
	{
		constexpr auto decimals_specified = UInt32_0::decimals_specified();
		EXPECT_TRUE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = Int32_0::decimals_specified();
		EXPECT_TRUE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = UInt32_4::decimals_specified();
		EXPECT_TRUE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = Int32_4::decimals_specified();
		EXPECT_TRUE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = UInt32_x::decimals_specified();
		EXPECT_FALSE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = Int32_x::decimals_specified();
		EXPECT_FALSE(decimals_specified);
	}
	// 64 bit.
	{
		constexpr auto decimals_specified = UInt64_0::decimals_specified();
		EXPECT_TRUE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = Int64_0::decimals_specified();
		EXPECT_TRUE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = UInt64_4::decimals_specified();
		EXPECT_TRUE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = Int64_4::decimals_specified();
		EXPECT_TRUE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = UInt64_x::decimals_specified();
		EXPECT_FALSE(decimals_specified);
	}
	{
		constexpr auto decimals_specified = Int64_x::decimals_specified();
		EXPECT_FALSE(decimals_specified);
	}
}

TEST(Numeric_type, decimals)
{
	// Unsigned 32 bit.
	{
		auto decimals = UInt32_0::decimals();
		EXPECT_EQ(0u, decimals);
	}
	{
		auto decimals = UInt32_4::decimals();
		EXPECT_EQ(4u, decimals);
	}
	{
		constexpr auto decimals = UInt32_0::decimals();
		EXPECT_EQ(0u, decimals);
	}
	{
		constexpr auto decimals = UInt32_4::decimals();
		EXPECT_EQ(4u, decimals);
	}
}

TEST(Numeric_type, min_max)
{
	// Signed 32 bit.
	{
		auto min = Int32_4::min();
		EXPECT_EQ(-2147483648, min.value());
		auto max = Int32_4::max();
		EXPECT_EQ(2147483647, max.value());
	}
	{
		constexpr auto min = Int32_4::min();
		EXPECT_EQ(-2147483648, min.value());
		constexpr auto max = Int32_4::max();
		EXPECT_EQ(2147483647, max.value());
	}
	// Unsigned 64 bit.
	{
		auto min = UInt64_4::min();
		EXPECT_EQ(0u, min.value());
		auto max = UInt64_4::max();
		EXPECT_EQ(18446744073709551615UL, max.value());
	}
	{
		constexpr auto min = UInt64_4::min();
		EXPECT_EQ(0u, min.value());
		constexpr auto max = UInt64_4::max();
		EXPECT_EQ(18446744073709551615UL, max.value());
	}
}

TEST(Numeric_type, numeric_cast_unsigned)
{
	EXPECT_EQ(UInt64_4(12345), misc::numeric_cast<UInt64_4>(UInt64_4(12345)));
	EXPECT_EQ(UInt64_4(12345), misc::numeric_cast<UInt64_4>(UInt32_4(12345)));
	EXPECT_EQ(UInt32_4(12345), misc::numeric_cast<UInt32_4>(UInt64_4(12345)));
	EXPECT_EQ(UInt32_4(12345), misc::numeric_cast<UInt32_4>(UInt32_4(12345)));

	EXPECT_EQ(UInt64_0(1), misc::numeric_cast<UInt64_0>(UInt64_4(12345)));
	EXPECT_EQ(UInt64_0(1), misc::numeric_cast<UInt64_0>(UInt32_4(12345)));
	EXPECT_EQ(UInt32_0(1), misc::numeric_cast<UInt32_0>(UInt64_4(12345)));
	EXPECT_EQ(UInt32_0(1), misc::numeric_cast<UInt32_0>(UInt32_4(12345)));

	EXPECT_EQ(
		UInt64_4(123450000), misc::numeric_cast<UInt64_4>(UInt64_0(12345)));
	EXPECT_EQ(
		UInt64_4(123450000), misc::numeric_cast<UInt64_4>(UInt32_0(12345)));
	EXPECT_EQ(
		UInt32_4(123450000), misc::numeric_cast<UInt32_4>(UInt64_0(12345)));
	EXPECT_EQ(
		UInt32_4(123450000), misc::numeric_cast<UInt32_4>(UInt32_0(12345)));

	EXPECT_EQ(UInt64_0(12345), misc::numeric_cast<UInt64_0>(UInt64_0(12345)));
	EXPECT_EQ(UInt64_0(12345), misc::numeric_cast<UInt64_0>(UInt32_0(12345)));
	EXPECT_EQ(UInt32_0(12345), misc::numeric_cast<UInt32_0>(UInt64_0(12345)));
	EXPECT_EQ(UInt32_0(12345), misc::numeric_cast<UInt32_0>(UInt32_0(12345)));

	// Increasing decimals - via multiplication - no information loss
	EXPECT_EQ(UInt32_0(1234567891),
		misc::numeric_cast<UInt32_0>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt32_1(1234567890),
		misc::numeric_cast<UInt32_1>(UInt32_0(123456789)));
	EXPECT_EQ(
		UInt32_2(1234567800), misc::numeric_cast<UInt32_2>(UInt32_0(12345678)));
	EXPECT_EQ(
		UInt32_3(1234567000), misc::numeric_cast<UInt32_3>(UInt32_0(1234567)));
	EXPECT_EQ(
		UInt32_4(1234560000), misc::numeric_cast<UInt32_4>(UInt32_0(123456)));
	EXPECT_EQ(
		UInt32_5(1234500000), misc::numeric_cast<UInt32_5>(UInt32_0(12345)));
	EXPECT_EQ(
		UInt32_6(1234000000), misc::numeric_cast<UInt32_6>(UInt32_0(1234)));
	EXPECT_EQ(
		UInt32_7(1230000000), misc::numeric_cast<UInt32_7>(UInt32_0(123)));
	EXPECT_EQ(UInt32_8(1200000000), misc::numeric_cast<UInt32_8>(UInt32_0(12)));
	EXPECT_EQ(UInt32_9(1000000000), misc::numeric_cast<UInt32_9>(UInt32_0(1)));

	// Increasing decimals - via multiplication - no information loss since cast
	// to larger type
	EXPECT_EQ(UInt64_0(1234567891UL),
		misc::numeric_cast<UInt64_0>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_1(12345678910UL),
		misc::numeric_cast<UInt64_1>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_2(123456789100UL),
		misc::numeric_cast<UInt64_2>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_3(1234567891000UL),
		misc::numeric_cast<UInt64_3>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_4(12345678910000UL),
		misc::numeric_cast<UInt64_4>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_5(123456789100000UL),
		misc::numeric_cast<UInt64_5>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_6(1234567891000000UL),
		misc::numeric_cast<UInt64_6>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_7(12345678910000000UL),
		misc::numeric_cast<UInt64_7>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_8(123456789100000000UL),
		misc::numeric_cast<UInt64_8>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_9(1234567891000000000UL),
		misc::numeric_cast<UInt64_9>(UInt32_0(1234567891)));
	EXPECT_EQ(UInt64_10(12345678910000000000UL),
		misc::numeric_cast<UInt64_10>(UInt32_0(1234567891)));

	// Decreasing decimals - via division - no information loss
	EXPECT_EQ(UInt32_10(1234567891),
		misc::numeric_cast<UInt32_10>(UInt32_10(1234567891)));
	EXPECT_EQ(UInt32_9(123456789),
		misc::numeric_cast<UInt32_9>(UInt32_10(1234567890)));
	EXPECT_EQ(UInt32_8(12345678),
		misc::numeric_cast<UInt32_8>(UInt32_10(1234567800)));
	EXPECT_EQ(
		UInt32_7(1234567), misc::numeric_cast<UInt32_7>(UInt32_10(1234567000)));
	EXPECT_EQ(
		UInt32_6(123456), misc::numeric_cast<UInt32_6>(UInt32_10(1234560000)));
	EXPECT_EQ(
		UInt32_5(12345), misc::numeric_cast<UInt32_5>(UInt32_10(1234500000)));
	EXPECT_EQ(
		UInt32_4(1234), misc::numeric_cast<UInt32_4>(UInt32_10(1234000000)));
	EXPECT_EQ(
		UInt32_3(123), misc::numeric_cast<UInt32_3>(UInt32_10(1230000000)));
	EXPECT_EQ(
		UInt32_2(12), misc::numeric_cast<UInt32_2>(UInt32_10(1200000000)));
	EXPECT_EQ(UInt32_1(1), misc::numeric_cast<UInt32_1>(UInt32_10(1000000000)));

	// Decreasing decimals - via division - information loss due to truncation
	EXPECT_EQ(UInt32_10(1234567891),
		misc::numeric_cast<UInt32_10>(UInt32_10(1234567891)));
	EXPECT_EQ(UInt32_9(123456789),
		misc::numeric_cast<UInt32_9>(UInt32_10(1234567891)));
	EXPECT_EQ(UInt32_8(12345678),
		misc::numeric_cast<UInt32_8>(UInt32_10(1234567891)));
	EXPECT_EQ(
		UInt32_7(1234567), misc::numeric_cast<UInt32_7>(UInt32_10(1234567891)));
	EXPECT_EQ(
		UInt32_6(123456), misc::numeric_cast<UInt32_6>(UInt32_10(1234567891)));
	EXPECT_EQ(
		UInt32_5(12345), misc::numeric_cast<UInt32_5>(UInt32_10(1234567891)));
	EXPECT_EQ(
		UInt32_4(1234), misc::numeric_cast<UInt32_4>(UInt32_10(1234567891)));
	EXPECT_EQ(
		UInt32_3(123), misc::numeric_cast<UInt32_3>(UInt32_10(1234567891)));
	EXPECT_EQ(
		UInt32_2(12), misc::numeric_cast<UInt32_2>(UInt32_10(1234567891)));
	EXPECT_EQ(UInt32_1(1), misc::numeric_cast<UInt32_1>(UInt32_10(1234567891)));

	constexpr auto cast = misc::numeric_cast<UInt32_4>(UInt32_2(12345));
	EXPECT_EQ(UInt32_4(1234500), cast);
}

TEST(Numeric_type, numeric_cast_unsigned_source_decimals_unspecified)
{
	// Increasing decimals - via multiplication - no information loss
	EXPECT_EQ(UInt32_0(1234567891),
		misc::numeric_cast<UInt32_0>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt32_1(1234567890),
		misc::numeric_cast<UInt32_1>(UInt32_x(123456789), 0));
	EXPECT_EQ(UInt32_2(1234567800),
		misc::numeric_cast<UInt32_2>(UInt32_x(12345678), 0));
	EXPECT_EQ(UInt32_3(1234567000),
		misc::numeric_cast<UInt32_3>(UInt32_x(1234567), 0));
	EXPECT_EQ(UInt32_4(1234560000),
		misc::numeric_cast<UInt32_4>(UInt32_x(123456), 0));
	EXPECT_EQ(
		UInt32_5(1234500000), misc::numeric_cast<UInt32_5>(UInt32_x(12345), 0));
	EXPECT_EQ(
		UInt32_6(1234000000), misc::numeric_cast<UInt32_6>(UInt32_x(1234), 0));
	EXPECT_EQ(
		UInt32_7(1230000000), misc::numeric_cast<UInt32_7>(UInt32_x(123), 0));
	EXPECT_EQ(
		UInt32_8(1200000000), misc::numeric_cast<UInt32_8>(UInt32_x(12), 0));
	EXPECT_EQ(
		UInt32_9(1000000000), misc::numeric_cast<UInt32_9>(UInt32_x(1), 0));

	// Increasing decimals - via multiplication - no information loss since cast
	// to larger type
	EXPECT_EQ(UInt64_0(1234567891UL),
		misc::numeric_cast<UInt64_0>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_1(12345678910UL),
		misc::numeric_cast<UInt64_1>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_2(123456789100UL),
		misc::numeric_cast<UInt64_2>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_3(1234567891000UL),
		misc::numeric_cast<UInt64_3>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_4(12345678910000UL),
		misc::numeric_cast<UInt64_4>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_5(123456789100000UL),
		misc::numeric_cast<UInt64_5>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_6(1234567891000000UL),
		misc::numeric_cast<UInt64_6>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_7(12345678910000000UL),
		misc::numeric_cast<UInt64_7>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_8(123456789100000000UL),
		misc::numeric_cast<UInt64_8>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_9(1234567891000000000UL),
		misc::numeric_cast<UInt64_9>(UInt32_x(1234567891), 0));
	EXPECT_EQ(UInt64_10(12345678910000000000UL),
		misc::numeric_cast<UInt64_10>(UInt32_x(1234567891), 0));

	// Decreasing decimals - via division - no information loss
	EXPECT_EQ(UInt32_10(1234567891),
		misc::numeric_cast<UInt32_10>(UInt32_x(1234567891), 10));
	EXPECT_EQ(UInt32_9(123456789),
		misc::numeric_cast<UInt32_9>(UInt32_x(1234567890), 10));
	EXPECT_EQ(UInt32_8(12345678),
		misc::numeric_cast<UInt32_8>(UInt32_x(1234567800), 10));
	EXPECT_EQ(UInt32_7(1234567),
		misc::numeric_cast<UInt32_7>(UInt32_x(1234567000), 10));
	EXPECT_EQ(UInt32_6(123456),
		misc::numeric_cast<UInt32_6>(UInt32_x(1234560000), 10));
	EXPECT_EQ(UInt32_5(12345),
		misc::numeric_cast<UInt32_5>(UInt32_x(1234500000), 10));
	EXPECT_EQ(
		UInt32_4(1234), misc::numeric_cast<UInt32_4>(UInt32_x(1234000000), 10));
	EXPECT_EQ(
		UInt32_3(123), misc::numeric_cast<UInt32_3>(UInt32_x(1230000000), 10));
	EXPECT_EQ(
		UInt32_2(12), misc::numeric_cast<UInt32_2>(UInt32_x(1200000000), 10));
	EXPECT_EQ(
		UInt32_1(1), misc::numeric_cast<UInt32_1>(UInt32_x(1000000000), 10));

	// Decreasing decimals - via division - information loss due to truncation
	EXPECT_EQ(UInt32_10(1234567891),
		misc::numeric_cast<UInt32_10>(UInt32_x(1234567891), 10));
	EXPECT_EQ(UInt32_9(123456789),
		misc::numeric_cast<UInt32_9>(UInt32_x(1234567891), 10));
	EXPECT_EQ(UInt32_8(12345678),
		misc::numeric_cast<UInt32_8>(UInt32_x(1234567891), 10));
	EXPECT_EQ(UInt32_7(1234567),
		misc::numeric_cast<UInt32_7>(UInt32_x(1234567891), 10));
	EXPECT_EQ(UInt32_6(123456),
		misc::numeric_cast<UInt32_6>(UInt32_x(1234567891), 10));
	EXPECT_EQ(UInt32_5(12345),
		misc::numeric_cast<UInt32_5>(UInt32_x(1234567891), 10));
	EXPECT_EQ(
		UInt32_4(1234), misc::numeric_cast<UInt32_4>(UInt32_x(1234567891), 10));
	EXPECT_EQ(
		UInt32_3(123), misc::numeric_cast<UInt32_3>(UInt32_x(1234567891), 10));
	EXPECT_EQ(
		UInt32_2(12), misc::numeric_cast<UInt32_2>(UInt32_x(1234567891), 10));
	EXPECT_EQ(
		UInt32_1(1), misc::numeric_cast<UInt32_1>(UInt32_x(1234567891), 10));
}

TEST(Numeric_type, numeric_cast_unsigned_destination_decimals_unspecified)
{
	// Increasing decimals - via multiplication - no information loss
	EXPECT_EQ(UInt32_x(1234567891),
		misc::numeric_cast<UInt32_x>(UInt32_0(1234567891), 0));
	EXPECT_EQ(UInt32_x(1234567890),
		misc::numeric_cast<UInt32_x>(UInt32_0(123456789), 1));
	EXPECT_EQ(UInt32_x(1234567800),
		misc::numeric_cast<UInt32_x>(UInt32_0(12345678), 2));
	EXPECT_EQ(UInt32_x(1234567000),
		misc::numeric_cast<UInt32_x>(UInt32_0(1234567), 3));
	EXPECT_EQ(UInt32_x(1234560000),
		misc::numeric_cast<UInt32_x>(UInt32_0(123456), 4));
	EXPECT_EQ(
		UInt32_x(1234500000), misc::numeric_cast<UInt32_x>(UInt32_0(12345), 5));
	EXPECT_EQ(
		UInt32_x(1234000000), misc::numeric_cast<UInt32_x>(UInt32_0(1234), 6));
	EXPECT_EQ(
		UInt32_x(1230000000), misc::numeric_cast<UInt32_x>(UInt32_0(123), 7));
	EXPECT_EQ(
		UInt32_x(1200000000), misc::numeric_cast<UInt32_x>(UInt32_0(12), 8));
	EXPECT_EQ(
		UInt32_x(1000000000), misc::numeric_cast<UInt32_x>(UInt32_0(1), 9));

	// Increasing decimals - via multiplication - no information loss since cast
	// to larger type
	EXPECT_EQ(UInt64_x(1234567891UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 0));
	EXPECT_EQ(UInt64_x(12345678910UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 1));
	EXPECT_EQ(UInt64_x(123456789100UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 2));
	EXPECT_EQ(UInt64_x(1234567891000UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 3));
	EXPECT_EQ(UInt64_x(12345678910000UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 4));
	EXPECT_EQ(UInt64_x(123456789100000UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 5));
	EXPECT_EQ(UInt64_x(1234567891000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 6));
	EXPECT_EQ(UInt64_x(12345678910000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 7));
	EXPECT_EQ(UInt64_x(123456789100000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 8));
	EXPECT_EQ(UInt64_x(1234567891000000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 9));
	EXPECT_EQ(UInt64_x(12345678910000000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_0(1234567891), 10));

	// Decreasing decimals - via division - no information loss
	EXPECT_EQ(UInt32_x(1234567891),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 10));
	EXPECT_EQ(UInt32_x(123456789),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567890), 9));
	EXPECT_EQ(UInt32_x(12345678),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567800), 8));
	EXPECT_EQ(UInt32_x(1234567),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567000), 7));
	EXPECT_EQ(UInt32_x(123456),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234560000), 6));
	EXPECT_EQ(UInt32_x(12345),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234500000), 5));
	EXPECT_EQ(
		UInt32_x(1234), misc::numeric_cast<UInt32_x>(UInt32_10(1234000000), 4));
	EXPECT_EQ(
		UInt32_x(123), misc::numeric_cast<UInt32_x>(UInt32_10(1230000000), 3));
	EXPECT_EQ(
		UInt32_x(12), misc::numeric_cast<UInt32_x>(UInt32_10(1200000000), 2));
	EXPECT_EQ(
		UInt32_x(1), misc::numeric_cast<UInt32_x>(UInt32_10(1000000000), 1));

	// Decreasing decimals - via division - information loss due to truncation
	EXPECT_EQ(UInt32_x(1234567891),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 10));
	EXPECT_EQ(UInt32_x(123456789),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 9));
	EXPECT_EQ(UInt32_x(12345678),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 8));
	EXPECT_EQ(UInt32_x(1234567),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 7));
	EXPECT_EQ(UInt32_x(123456),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 6));
	EXPECT_EQ(UInt32_x(12345),
		misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 5));
	EXPECT_EQ(
		UInt32_x(1234), misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 4));
	EXPECT_EQ(
		UInt32_x(123), misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 3));
	EXPECT_EQ(
		UInt32_x(12), misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 2));
	EXPECT_EQ(
		UInt32_x(1), misc::numeric_cast<UInt32_x>(UInt32_10(1234567891), 1));
}

TEST(Numeric_type,
	numeric_cast_unsigned_source_and_destination_decimals_unspecified)
{
	// Increasing decimals - via multiplication - no information loss
	EXPECT_EQ(UInt32_x(1234567891),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 0, 0));
	EXPECT_EQ(UInt32_x(1234567890),
		misc::numeric_cast<UInt32_x>(UInt32_x(123456789), 0, 1));
	EXPECT_EQ(UInt32_x(1234567800),
		misc::numeric_cast<UInt32_x>(UInt32_x(12345678), 0, 2));
	EXPECT_EQ(UInt32_x(1234567000),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567), 0, 3));
	EXPECT_EQ(UInt32_x(1234560000),
		misc::numeric_cast<UInt32_x>(UInt32_x(123456), 0, 4));
	EXPECT_EQ(UInt32_x(1234500000),
		misc::numeric_cast<UInt32_x>(UInt32_x(12345), 0, 5));
	EXPECT_EQ(UInt32_x(1234000000),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234), 0, 6));
	EXPECT_EQ(UInt32_x(1230000000),
		misc::numeric_cast<UInt32_x>(UInt32_x(123), 0, 7));
	EXPECT_EQ(
		UInt32_x(1200000000), misc::numeric_cast<UInt32_x>(UInt32_x(12), 0, 8));
	EXPECT_EQ(
		UInt32_x(1000000000), misc::numeric_cast<UInt32_x>(UInt32_x(1), 0, 9));

	// Increasing decimals - via multiplication - no information loss since cast
	// to larger type
	EXPECT_EQ(UInt64_x(1234567891UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 0));
	EXPECT_EQ(UInt64_x(12345678910UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 1));
	EXPECT_EQ(UInt64_x(123456789100UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 2));
	EXPECT_EQ(UInt64_x(1234567891000UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 3));
	EXPECT_EQ(UInt64_x(12345678910000UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 4));
	EXPECT_EQ(UInt64_x(123456789100000UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 5));
	EXPECT_EQ(UInt64_x(1234567891000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 6));
	EXPECT_EQ(UInt64_x(12345678910000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 7));
	EXPECT_EQ(UInt64_x(123456789100000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 8));
	EXPECT_EQ(UInt64_x(1234567891000000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 9));
	EXPECT_EQ(UInt64_x(12345678910000000000UL),
		misc::numeric_cast<UInt64_x>(UInt32_x(1234567891), 0, 10));

	// Decreasing decimals - via division - no information loss
	EXPECT_EQ(UInt32_x(1234567891),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 10));
	EXPECT_EQ(UInt32_x(123456789),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567890), 10, 9));
	EXPECT_EQ(UInt32_x(12345678),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567800), 10, 8));
	EXPECT_EQ(UInt32_x(1234567),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567000), 10, 7));
	EXPECT_EQ(UInt32_x(123456),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234560000), 10, 6));
	EXPECT_EQ(UInt32_x(12345),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234500000), 10, 5));
	EXPECT_EQ(UInt32_x(1234),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234000000), 10, 4));
	EXPECT_EQ(UInt32_x(123),
		misc::numeric_cast<UInt32_x>(UInt32_x(1230000000), 10, 3));
	EXPECT_EQ(UInt32_x(12),
		misc::numeric_cast<UInt32_x>(UInt32_x(1200000000), 10, 2));
	EXPECT_EQ(
		UInt32_x(1), misc::numeric_cast<UInt32_x>(UInt32_x(1000000000), 10, 1));

	// Decreasing decimals - via division - information loss due to truncation
	EXPECT_EQ(UInt32_x(1234567891),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 10));
	EXPECT_EQ(UInt32_x(123456789),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 9));
	EXPECT_EQ(UInt32_x(12345678),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 8));
	EXPECT_EQ(UInt32_x(1234567),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 7));
	EXPECT_EQ(UInt32_x(123456),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 6));
	EXPECT_EQ(UInt32_x(12345),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 5));
	EXPECT_EQ(UInt32_x(1234),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 4));
	EXPECT_EQ(UInt32_x(123),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 3));
	EXPECT_EQ(UInt32_x(12),
		misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 2));
	EXPECT_EQ(
		UInt32_x(1), misc::numeric_cast<UInt32_x>(UInt32_x(1234567891), 10, 1));
}

TEST(Numeric_type, numeric_cast_signed_positive)
{
	EXPECT_EQ(Int64_4(12345), misc::numeric_cast<Int64_4>(Int64_4(12345)));
	EXPECT_EQ(Int64_4(12345), misc::numeric_cast<Int64_4>(Int32_4(12345)));
	EXPECT_EQ(Int32_4(12345), misc::numeric_cast<Int32_4>(Int64_4(12345)));
	EXPECT_EQ(Int32_4(12345), misc::numeric_cast<Int32_4>(Int32_4(12345)));

	EXPECT_EQ(Int64_0(1), misc::numeric_cast<Int64_0>(Int64_4(12345)));
	EXPECT_EQ(Int64_0(1), misc::numeric_cast<Int64_0>(Int32_4(12345)));
	EXPECT_EQ(Int32_0(1), misc::numeric_cast<Int32_0>(Int64_4(12345)));
	EXPECT_EQ(Int32_0(1), misc::numeric_cast<Int32_0>(Int32_4(12345)));

	EXPECT_EQ(Int64_4(123450000), misc::numeric_cast<Int64_4>(Int64_0(12345)));
	EXPECT_EQ(Int64_4(123450000), misc::numeric_cast<Int64_4>(Int32_0(12345)));
	EXPECT_EQ(Int32_4(123450000), misc::numeric_cast<Int32_4>(Int64_0(12345)));
	EXPECT_EQ(Int32_4(123450000), misc::numeric_cast<Int32_4>(Int32_0(12345)));

	EXPECT_EQ(Int64_0(12345), misc::numeric_cast<Int64_0>(Int64_0(12345)));
	EXPECT_EQ(Int64_0(12345), misc::numeric_cast<Int64_0>(Int32_0(12345)));
	EXPECT_EQ(Int32_0(12345), misc::numeric_cast<Int32_0>(Int64_0(12345)));
	EXPECT_EQ(Int32_0(12345), misc::numeric_cast<Int32_0>(Int32_0(12345)));

	// Increasing decimals - via multiplication - no information loss
	EXPECT_EQ(
		Int32_0(1234567891), misc::numeric_cast<Int32_0>(Int32_0(1234567891)));
	EXPECT_EQ(
		Int32_1(1234567890), misc::numeric_cast<Int32_1>(Int32_0(123456789)));
	EXPECT_EQ(
		Int32_2(1234567800), misc::numeric_cast<Int32_2>(Int32_0(12345678)));
	EXPECT_EQ(
		Int32_3(1234567000), misc::numeric_cast<Int32_3>(Int32_0(1234567)));
	EXPECT_EQ(
		Int32_4(1234560000), misc::numeric_cast<Int32_4>(Int32_0(123456)));
	EXPECT_EQ(Int32_5(1234500000), misc::numeric_cast<Int32_5>(Int32_0(12345)));
	EXPECT_EQ(Int32_6(1234000000), misc::numeric_cast<Int32_6>(Int32_0(1234)));
	EXPECT_EQ(Int32_7(1230000000), misc::numeric_cast<Int32_7>(Int32_0(123)));
	EXPECT_EQ(Int32_8(1200000000), misc::numeric_cast<Int32_8>(Int32_0(12)));
	EXPECT_EQ(Int32_9(1000000000), misc::numeric_cast<Int32_9>(Int32_0(1)));

	// Increasing decimals - via multiplication - no information loss since cast
	// to larger type
	EXPECT_EQ(
		Int64_0(1234567891L), misc::numeric_cast<Int64_0>(Int32_0(1234567891)));
	EXPECT_EQ(Int64_1(12345678910L),
		misc::numeric_cast<Int64_1>(Int32_0(1234567891)));
	EXPECT_EQ(Int64_2(123456789100L),
		misc::numeric_cast<Int64_2>(Int32_0(1234567891)));
	EXPECT_EQ(Int64_3(1234567891000L),
		misc::numeric_cast<Int64_3>(Int32_0(1234567891)));
	EXPECT_EQ(Int64_4(12345678910000L),
		misc::numeric_cast<Int64_4>(Int32_0(1234567891)));
	EXPECT_EQ(Int64_5(123456789100000L),
		misc::numeric_cast<Int64_5>(Int32_0(1234567891)));
	EXPECT_EQ(Int64_6(1234567891000000L),
		misc::numeric_cast<Int64_6>(Int32_0(1234567891)));
	EXPECT_EQ(Int64_7(12345678910000000L),
		misc::numeric_cast<Int64_7>(Int32_0(1234567891)));
	EXPECT_EQ(Int64_8(123456789100000000L),
		misc::numeric_cast<Int64_8>(Int32_0(1234567891)));
	EXPECT_EQ(Int64_9(1234567891000000000L),
		misc::numeric_cast<Int64_9>(Int32_0(1234567891)));

	// Decreasing decimals - via division - no information loss
	EXPECT_EQ(Int32_10(1234567891),
		misc::numeric_cast<Int32_10>(Int32_10(1234567891)));
	EXPECT_EQ(
		Int32_9(123456789), misc::numeric_cast<Int32_9>(Int32_10(1234567890)));
	EXPECT_EQ(
		Int32_8(12345678), misc::numeric_cast<Int32_8>(Int32_10(1234567800)));
	EXPECT_EQ(
		Int32_7(1234567), misc::numeric_cast<Int32_7>(Int32_10(1234567000)));
	EXPECT_EQ(
		Int32_6(123456), misc::numeric_cast<Int32_6>(Int32_10(1234560000)));
	EXPECT_EQ(
		Int32_5(12345), misc::numeric_cast<Int32_5>(Int32_10(1234500000)));
	EXPECT_EQ(Int32_4(1234), misc::numeric_cast<Int32_4>(Int32_10(1234000000)));
	EXPECT_EQ(Int32_3(123), misc::numeric_cast<Int32_3>(Int32_10(1230000000)));
	EXPECT_EQ(Int32_2(12), misc::numeric_cast<Int32_2>(Int32_10(1200000000)));
	EXPECT_EQ(Int32_1(1), misc::numeric_cast<Int32_1>(Int32_10(1000000000)));

	// Decreasing decimals - via division - information loss due to truncation
	EXPECT_EQ(Int32_10(1234567891),
		misc::numeric_cast<Int32_10>(Int32_10(1234567891)));
	EXPECT_EQ(
		Int32_9(123456789), misc::numeric_cast<Int32_9>(Int32_10(1234567891)));
	EXPECT_EQ(
		Int32_8(12345678), misc::numeric_cast<Int32_8>(Int32_10(1234567891)));
	EXPECT_EQ(
		Int32_7(1234567), misc::numeric_cast<Int32_7>(Int32_10(1234567891)));
	EXPECT_EQ(
		Int32_6(123456), misc::numeric_cast<Int32_6>(Int32_10(1234567891)));
	EXPECT_EQ(
		Int32_5(12345), misc::numeric_cast<Int32_5>(Int32_10(1234567891)));
	EXPECT_EQ(Int32_4(1234), misc::numeric_cast<Int32_4>(Int32_10(1234567891)));
	EXPECT_EQ(Int32_3(123), misc::numeric_cast<Int32_3>(Int32_10(1234567891)));
	EXPECT_EQ(Int32_2(12), misc::numeric_cast<Int32_2>(Int32_10(1234567891)));
	EXPECT_EQ(Int32_1(1), misc::numeric_cast<Int32_1>(Int32_10(1234567891)));

	constexpr auto cast = misc::numeric_cast<Int32_4>(Int32_2(12345));
	EXPECT_EQ(Int32_4(1234500), cast);
}

TEST(Numeric_type, numeric_cast_signed_negative)
{
	EXPECT_EQ(Int64_4(-12345), misc::numeric_cast<Int64_4>(Int64_4(-12345)));
	EXPECT_EQ(Int64_4(-12345), misc::numeric_cast<Int64_4>(Int32_4(-12345)));
	EXPECT_EQ(Int32_4(-12345), misc::numeric_cast<Int32_4>(Int64_4(-12345)));
	EXPECT_EQ(Int32_4(-12345), misc::numeric_cast<Int32_4>(Int32_4(-12345)));

	EXPECT_EQ(Int64_0(-1), misc::numeric_cast<Int64_0>(Int64_4(-12345)));
	EXPECT_EQ(Int64_0(-1), misc::numeric_cast<Int64_0>(Int32_4(-12345)));
	EXPECT_EQ(Int32_0(-1), misc::numeric_cast<Int32_0>(Int64_4(-12345)));
	EXPECT_EQ(Int32_0(-1), misc::numeric_cast<Int32_0>(Int32_4(-12345)));

	EXPECT_EQ(
		Int64_4(-123450000), misc::numeric_cast<Int64_4>(Int64_0(-12345)));
	EXPECT_EQ(
		Int64_4(-123450000), misc::numeric_cast<Int64_4>(Int32_0(-12345)));
	EXPECT_EQ(
		Int32_4(-123450000), misc::numeric_cast<Int32_4>(Int64_0(-12345)));
	EXPECT_EQ(
		Int32_4(-123450000), misc::numeric_cast<Int32_4>(Int32_0(-12345)));

	EXPECT_EQ(Int64_0(-12345), misc::numeric_cast<Int64_0>(Int64_0(-12345)));
	EXPECT_EQ(Int64_0(-12345), misc::numeric_cast<Int64_0>(Int32_0(-12345)));
	EXPECT_EQ(Int32_0(-12345), misc::numeric_cast<Int32_0>(Int64_0(-12345)));
	EXPECT_EQ(Int32_0(-12345), misc::numeric_cast<Int32_0>(Int32_0(-12345)));

	// Increasing decimals - via multiplication - no information loss
	EXPECT_EQ(Int32_0(-1234567891),
		misc::numeric_cast<Int32_0>(Int32_0(-1234567891)));
	EXPECT_EQ(
		Int32_1(-1234567890), misc::numeric_cast<Int32_1>(Int32_0(-123456789)));
	EXPECT_EQ(
		Int32_2(-1234567800), misc::numeric_cast<Int32_2>(Int32_0(-12345678)));
	EXPECT_EQ(
		Int32_3(-1234567000), misc::numeric_cast<Int32_3>(Int32_0(-1234567)));
	EXPECT_EQ(
		Int32_4(-1234560000), misc::numeric_cast<Int32_4>(Int32_0(-123456)));
	EXPECT_EQ(
		Int32_5(-1234500000), misc::numeric_cast<Int32_5>(Int32_0(-12345)));
	EXPECT_EQ(
		Int32_6(-1234000000), misc::numeric_cast<Int32_6>(Int32_0(-1234)));
	EXPECT_EQ(Int32_7(-1230000000), misc::numeric_cast<Int32_7>(Int32_0(-123)));
	EXPECT_EQ(Int32_8(-1200000000), misc::numeric_cast<Int32_8>(Int32_0(-12)));
	EXPECT_EQ(Int32_9(-1000000000), misc::numeric_cast<Int32_9>(Int32_0(-1)));

	// Increasing decimals - via multiplication - no information loss since cast
	// to larger type
	EXPECT_EQ(Int64_0(-1234567891L),
		misc::numeric_cast<Int64_0>(Int32_0(-1234567891)));
	EXPECT_EQ(Int64_1(-12345678910L),
		misc::numeric_cast<Int64_1>(Int32_0(-1234567891)));
	EXPECT_EQ(Int64_2(-123456789100L),
		misc::numeric_cast<Int64_2>(Int32_0(-1234567891)));
	EXPECT_EQ(Int64_3(-1234567891000L),
		misc::numeric_cast<Int64_3>(Int32_0(-1234567891)));
	EXPECT_EQ(Int64_4(-12345678910000L),
		misc::numeric_cast<Int64_4>(Int32_0(-1234567891)));
	EXPECT_EQ(Int64_5(-123456789100000L),
		misc::numeric_cast<Int64_5>(Int32_0(-1234567891)));
	EXPECT_EQ(Int64_6(-1234567891000000L),
		misc::numeric_cast<Int64_6>(Int32_0(-1234567891)));
	EXPECT_EQ(Int64_7(-12345678910000000L),
		misc::numeric_cast<Int64_7>(Int32_0(-1234567891)));
	EXPECT_EQ(Int64_8(-123456789100000000L),
		misc::numeric_cast<Int64_8>(Int32_0(-1234567891)));
	EXPECT_EQ(Int64_9(-1234567891000000000L),
		misc::numeric_cast<Int64_9>(Int32_0(-1234567891)));

	// Decreasing decimals - via division - no information loss
	EXPECT_EQ(Int32_10(-1234567891),
		misc::numeric_cast<Int32_10>(Int32_10(-1234567891)));
	EXPECT_EQ(Int32_9(-123456789),
		misc::numeric_cast<Int32_9>(Int32_10(-1234567890)));
	EXPECT_EQ(
		Int32_8(-12345678), misc::numeric_cast<Int32_8>(Int32_10(-1234567800)));
	EXPECT_EQ(
		Int32_7(-1234567), misc::numeric_cast<Int32_7>(Int32_10(-1234567000)));
	EXPECT_EQ(
		Int32_6(-123456), misc::numeric_cast<Int32_6>(Int32_10(-1234560000)));
	EXPECT_EQ(
		Int32_5(-12345), misc::numeric_cast<Int32_5>(Int32_10(-1234500000)));
	EXPECT_EQ(
		Int32_4(-1234), misc::numeric_cast<Int32_4>(Int32_10(-1234000000)));
	EXPECT_EQ(
		Int32_3(-123), misc::numeric_cast<Int32_3>(Int32_10(-1230000000)));
	EXPECT_EQ(Int32_2(-12), misc::numeric_cast<Int32_2>(Int32_10(-1200000000)));
	EXPECT_EQ(Int32_1(-1), misc::numeric_cast<Int32_1>(Int32_10(-1000000000)));

	// Decreasing decimals - via division - information loss due to truncation
	EXPECT_EQ(Int32_10(-1234567891),
		misc::numeric_cast<Int32_10>(Int32_10(-1234567891)));
	EXPECT_EQ(Int32_9(-123456789),
		misc::numeric_cast<Int32_9>(Int32_10(-1234567891)));
	EXPECT_EQ(
		Int32_8(-12345678), misc::numeric_cast<Int32_8>(Int32_10(-1234567891)));
	EXPECT_EQ(
		Int32_7(-1234567), misc::numeric_cast<Int32_7>(Int32_10(-1234567891)));
	EXPECT_EQ(
		Int32_6(-123456), misc::numeric_cast<Int32_6>(Int32_10(-1234567891)));
	EXPECT_EQ(
		Int32_5(-12345), misc::numeric_cast<Int32_5>(Int32_10(-1234567891)));
	EXPECT_EQ(
		Int32_4(-1234), misc::numeric_cast<Int32_4>(Int32_10(-1234567891)));
	EXPECT_EQ(
		Int32_3(-123), misc::numeric_cast<Int32_3>(Int32_10(-1234567891)));
	EXPECT_EQ(Int32_2(-12), misc::numeric_cast<Int32_2>(Int32_10(-1234567891)));
	EXPECT_EQ(Int32_1(-1), misc::numeric_cast<Int32_1>(Int32_10(-1234567891)));

	constexpr auto cast = misc::numeric_cast<Int32_4>(Int32_2(-12345));
	EXPECT_EQ(Int32_4(-1234500), cast);
}

TEST(Numeric_type, numeric_cast_mixed_sign)
{
	// Unsigned and signed 32 bit.

	EXPECT_EQ(
		Int32_4(1234567891), misc::numeric_cast<Int32_4>(UInt32_4(1234567891)));
	EXPECT_EQ(
		Int32_6(1234567800), misc::numeric_cast<Int32_6>(UInt32_4(12345678)));
	EXPECT_EQ(
		Int32_2(12345678), misc::numeric_cast<Int32_2>(UInt32_4(1234567800)));
	EXPECT_EQ(
		Int32_2(12345678), misc::numeric_cast<Int32_2>(UInt32_4(1234567891)));

	EXPECT_EQ(UInt32_4(1234567891),
		misc::numeric_cast<UInt32_4>(Int32_4(1234567891)));
	EXPECT_EQ(
		UInt32_6(1234567800), misc::numeric_cast<UInt32_6>(Int32_4(12345678)));
	EXPECT_EQ(
		UInt32_2(12345678), misc::numeric_cast<UInt32_2>(Int32_4(1234567800)));
	EXPECT_EQ(
		UInt32_2(12345678), misc::numeric_cast<UInt32_2>(Int32_4(1234567891)));

	// Unsigned and signed 64 bit.

	EXPECT_EQ(Int64_4(12345678912345678912UL),
		misc::numeric_cast<Int64_4>(UInt64_4(12345678912345678912UL)));
	EXPECT_EQ(Int64_6(12345678912345678900UL),
		misc::numeric_cast<Int64_6>(UInt64_4(123456789123456789UL)));
	EXPECT_EQ(Int64_2(123456789123456789UL),
		misc::numeric_cast<Int64_2>(UInt64_4(12345678912345678900UL)));
	EXPECT_EQ(Int64_2(123456789123456789UL),
		misc::numeric_cast<Int64_2>(UInt64_4(12345678912345678912UL)));

	EXPECT_EQ(UInt64_4(1234567891234567891L),
		misc::numeric_cast<UInt64_4>(Int64_4(1234567891234567891L)));
	EXPECT_EQ(UInt64_6(1234567891234567800L),
		misc::numeric_cast<UInt64_6>(Int64_4(12345678912345678L)));
	EXPECT_EQ(UInt64_2(12345678912345678L),
		misc::numeric_cast<UInt64_2>(Int64_4(1234567891234567800L)));
	EXPECT_EQ(UInt64_2(12345678912345678L),
		misc::numeric_cast<UInt64_2>(Int64_4(1234567891234567891L)));

	// Unsigned 64 bit and signed 32 bit.

	EXPECT_EQ(
		Int32_4(1234567891), misc::numeric_cast<Int32_4>(UInt64_4(1234567891)));
	EXPECT_EQ(
		Int32_6(1234567800), misc::numeric_cast<Int32_6>(UInt64_4(12345678)));
	EXPECT_EQ(
		Int32_2(12345678), misc::numeric_cast<Int32_2>(UInt64_4(1234567800)));
	EXPECT_EQ(
		Int32_2(12345678), misc::numeric_cast<Int32_2>(UInt64_4(1234567891)));

	EXPECT_EQ(UInt64_4(1234567891),
		misc::numeric_cast<UInt64_4>(Int32_4(1234567891)));
	EXPECT_EQ(UInt64_6(123456789100),
		misc::numeric_cast<UInt64_6>(Int32_4(1234567891)));
	EXPECT_EQ(
		UInt64_2(12345678), misc::numeric_cast<UInt64_2>(Int32_4(1234567800)));
	EXPECT_EQ(
		UInt64_2(12345678), misc::numeric_cast<UInt64_2>(Int32_4(1234567891)));

	// Unsigned 32 bit and signed 64 bit.

	EXPECT_EQ(
		Int64_4(1234567891), misc::numeric_cast<Int64_4>(UInt32_4(1234567891)));
	EXPECT_EQ(Int64_6(123456789100),
		misc::numeric_cast<Int64_6>(UInt32_4(1234567891)));
	EXPECT_EQ(
		Int64_2(12345678), misc::numeric_cast<Int64_2>(UInt32_4(1234567800)));
	EXPECT_EQ(
		Int64_2(12345678), misc::numeric_cast<Int64_2>(UInt32_4(1234567891)));

	EXPECT_EQ(UInt32_4(1234567891),
		misc::numeric_cast<UInt32_4>(Int64_4(1234567891)));
	EXPECT_EQ(
		UInt32_6(1234567800), misc::numeric_cast<UInt32_6>(Int64_4(12345678)));
	EXPECT_EQ(
		UInt32_2(12345678), misc::numeric_cast<UInt32_2>(Int64_4(1234567800)));
	EXPECT_EQ(
		UInt32_2(12345678), misc::numeric_cast<UInt32_2>(Int64_4(1234567891)));
}

TEST(Numeric_type, arithmetic_operators)
{
	// 32 bit.
	EXPECT_EQ(UInt32_4(4), ++UInt32_4(3));
	auto postfix = UInt32_4(3);
	EXPECT_EQ(UInt32_4(3), postfix++);
	EXPECT_EQ(UInt32_4(4), postfix);
	EXPECT_EQ(UInt32_4(3), UInt32_4(2) + UInt32_4(1));
	EXPECT_EQ(UInt32_4(3), UInt32_4(2) += UInt32_4(1));
	EXPECT_EQ(UInt32_4(2), --UInt32_4(3));
	EXPECT_EQ(UInt32_4(4), postfix--);
	EXPECT_EQ(UInt32_4(3), postfix);
	EXPECT_EQ(UInt32_4(1), UInt32_4(3) - UInt32_4(2));
	EXPECT_EQ(UInt32_4(1), UInt32_4(3) -= UInt32_4(2));
	EXPECT_EQ(UInt32_4(6), UInt32_4(3) * 2);
	EXPECT_EQ(UInt32_4(6), UInt32_4(3) *= 2);
	EXPECT_EQ(UInt32_4(8), 4 * UInt32_4(2));
	EXPECT_EQ(2u, UInt32_4(8) / UInt32_4(4));
	EXPECT_EQ(UInt32_4(3), UInt32_4(9) / 3);
	EXPECT_EQ(UInt32_4(3), UInt32_4(9) /= 3);
	EXPECT_EQ(UInt32_4(1), UInt32_4(7) % UInt32_4(3));
	EXPECT_EQ(UInt32_4(1), UInt32_4(7) %= UInt32_4(3));
	EXPECT_EQ(UInt32_4(2), UInt32_4(5) % 3);
	EXPECT_EQ(UInt32_4(2), UInt32_4(5) %= 3);
	EXPECT_EQ(Int32_4(-1), -Int32_4(1));

	// 64 bit.
	constexpr auto prefix_increment = ++UInt64_4(3);
	EXPECT_EQ(UInt64_4(4), prefix_increment);
	auto postfix_64 = UInt64_4(3);
	EXPECT_EQ(UInt64_4(3), postfix_64++);
	EXPECT_EQ(UInt64_4(4), postfix_64);
	constexpr auto plus = UInt64_4(2) + UInt64_4(1);
	EXPECT_EQ(3u, plus.value());
	constexpr auto assign_plus = UInt64_4(2) += UInt64_4(1);
	EXPECT_EQ(3u, assign_plus.value());
	constexpr auto prefix_decrement = --UInt64_4(3);
	EXPECT_EQ(UInt64_4(2), prefix_decrement);
	EXPECT_EQ(UInt64_4(4), postfix_64--);
	EXPECT_EQ(UInt64_4(3), postfix_64);
	constexpr auto minus = UInt64_4(3) - UInt64_4(2);
	EXPECT_EQ(1u, minus.value());
	constexpr auto assign_minus = UInt64_4(3) -= UInt64_4(2);
	EXPECT_EQ(1u, assign_minus.value());
	constexpr auto multiply_scalar_right = UInt64_4(3) * 2;
	EXPECT_EQ(6u, multiply_scalar_right.value());
	constexpr auto assign_multiply_scalar = UInt64_4(3) *= 2;
	EXPECT_EQ(6u, assign_multiply_scalar.value());
	constexpr auto multiply_scalar_left = 4 * UInt64_4(2);
	EXPECT_EQ(8u, multiply_scalar_left.value());
	constexpr auto divide = UInt64_4(8) / UInt64_4(4);
	EXPECT_EQ(2u, divide);
	constexpr auto divide_scalar = UInt64_4(9) / 3;
	EXPECT_EQ(3u, divide_scalar.value());
	constexpr auto assign_divide_scalar = UInt64_4(9) /= 3;
	EXPECT_EQ(3u, assign_divide_scalar.value());
	constexpr auto modulus = UInt64_4(7) % UInt64_4(3);
	EXPECT_EQ(1u, modulus.value());
	constexpr auto assign_modulus = UInt64_4(7) %= UInt64_4(3);
	EXPECT_EQ(1u, assign_modulus.value());
	constexpr auto modulus_scalar = UInt64_4(5) % 3;
	EXPECT_EQ(2u, modulus_scalar.value());
	constexpr auto assign_modulus_scalar = UInt64_4(5) %= 3;
	EXPECT_EQ(2u, assign_modulus_scalar.value());
	constexpr auto unary_minus = -Int64_4(1);
	EXPECT_EQ(-1, unary_minus.value());

	// Scalar operations without promotions and narrowing.
	{
		using T = int32_t;

		Int32_0 x1 {10};
		x1 *= T {2};
		EXPECT_EQ(20, x1.value());

		Int32_0 x2 {10};
		x2 /= T {2};
		EXPECT_EQ(5, x2.value());

		Int32_0 x3 {10};
		x3 %= T {3};
		EXPECT_EQ(1, x3.value());

		Int32_0 x4 {10};
		x4 = x4 * T {2};
		EXPECT_EQ(20, x4.value());

		Int32_0 x5 {10};
		x5 = T {2} * x5;
		EXPECT_EQ(20, x5.value());

		Int32_0 x6 {10};
		x6 = x6 / T {2};
		EXPECT_EQ(5, x6.value());

		Int32_0 x7 {10};
		x7 = x7 % T {3};
		EXPECT_EQ(1, x7.value());
	}

	// Scalar operations with promotions and narrowing.
	{
		using T = int64_t;

		Int32_0 x1 {10};
		x1 *= T {2};
		EXPECT_EQ(20, x1.value());

		Int32_0 x2 {10};
		x2 /= T {2};
		EXPECT_EQ(5, x2.value());

		Int32_0 x3 {10};
		x3 %= T {3};
		EXPECT_EQ(1, x3.value());

		Int32_0 x4 {10};
		x4 = x4 * T {2};
		EXPECT_EQ(20, x4.value());

		Int32_0 x5 {10};
		x5 = T {2} * x5;
		EXPECT_EQ(20, x5.value());

		Int32_0 x6 {10};
		x6 = x6 / T {2};
		EXPECT_EQ(5, x6.value());

		Int32_0 x7 {10};
		x7 = x7 % T {3};
		EXPECT_EQ(1, x7.value());
	}

	// Unsigned modulus.
	{
		EXPECT_EQ(UInt32_0(0), UInt32_0(6) % UInt32_0(3));
		EXPECT_EQ(UInt32_0(1), UInt32_0(7) % UInt32_0(3));
		EXPECT_EQ(UInt32_0(2), UInt32_0(8) % UInt32_0(3));

		UInt32_0 p6(6);
		UInt32_0 p7(7);
		UInt32_0 p8(8);
		EXPECT_EQ(UInt32_0(0), p6 %= UInt32_0(3));
		EXPECT_EQ(UInt32_0(1), p7 %= UInt32_0(3));
		EXPECT_EQ(UInt32_0(2), p8 %= UInt32_0(3));
		EXPECT_EQ(UInt32_0(0), p6);
		EXPECT_EQ(UInt32_0(1), p7);
		EXPECT_EQ(UInt32_0(2), p8);
	}

	// Signed modulus.
	{
		EXPECT_EQ(Int32_0(0), Int32_0(-6) % Int32_0(3));
		EXPECT_EQ(Int32_0(-1), Int32_0(-7) % Int32_0(3));
		EXPECT_EQ(Int32_0(-2), Int32_0(-8) % Int32_0(3));

		Int32_0 n6(-6);
		Int32_0 n7(-7);
		Int32_0 n8(-8);
		EXPECT_EQ(Int32_0(0), n6 %= Int32_0(3));
		EXPECT_EQ(Int32_0(-1), n7 %= Int32_0(3));
		EXPECT_EQ(Int32_0(-2), n8 %= Int32_0(3));
		EXPECT_EQ(Int32_0(0), n6);
		EXPECT_EQ(Int32_0(-1), n7);
		EXPECT_EQ(Int32_0(-2), n8);
	}

	// Alien grouping.
	{
		using space::Alien;

		// Split 13 aliens into 5 groups.
		EXPECT_EQ(Alien(2), Alien(13) / 5); // 2 aliens per group.
		EXPECT_EQ(Alien(3), Alien(13) % 5); // 3 aliens remaining.
		EXPECT_EQ(Alien(13), Alien(2) * 5 + Alien(3));

		// Split 13 aliens into groups of 5.
		EXPECT_EQ(2, Alien(13) / Alien(5));        // 2 groups of 5.
		EXPECT_EQ(Alien(3), Alien(13) % Alien(5)); // 3 aliens remaining.
		EXPECT_EQ(Alien(13), 2 * Alien(5) + Alien(3));
	}
}

namespace {

template <typename T>
void test_comparisons(const T& low, const T& high)
{
	EXPECT_TRUE(low == low);
	EXPECT_FALSE(low == high);
	EXPECT_FALSE(high == low);
	EXPECT_TRUE(high == high);

	EXPECT_FALSE(low != low);
	EXPECT_TRUE(low != high);
	EXPECT_TRUE(high != low);
	EXPECT_FALSE(high != high);

	EXPECT_FALSE(low < low);
	EXPECT_TRUE(low < high);
	EXPECT_FALSE(high < low);
	EXPECT_FALSE(high < high);

	EXPECT_FALSE(low > low);
	EXPECT_FALSE(low > high);
	EXPECT_TRUE(high > low);
	EXPECT_FALSE(high > high);

	EXPECT_TRUE(low <= low);
	EXPECT_TRUE(low <= high);
	EXPECT_FALSE(high <= low);
	EXPECT_TRUE(high <= high);

	EXPECT_TRUE(low >= low);
	EXPECT_FALSE(low >= high);
	EXPECT_TRUE(high >= low);
	EXPECT_TRUE(high >= high);
}

} // namespace

TEST(Numeric_type, comparison_operators)
{
	test_comparisons(UInt32_4(100), UInt32_4(200));
	test_comparisons(Int32_4(-100), Int32_4(300));

	constexpr bool b1 = Int32_4(400) == Int32_4(400);
	EXPECT_TRUE(b1);
	constexpr bool b2 = Int32_4(400) != Int32_4(500);
	EXPECT_TRUE(b2);
	constexpr bool b3 = Int32_4(400) < Int32_4(500);
	EXPECT_TRUE(b3);
	constexpr bool b4 = Int32_4(500) > Int32_4(400);
	EXPECT_TRUE(b4);
	constexpr bool b5 = Int32_4(400) <= Int32_4(400);
	EXPECT_TRUE(b5);
	constexpr bool b6 = Int32_4(500) >= Int32_4(400);
	EXPECT_TRUE(b6);
}

TEST(Numeric_type, to_string)
{
	EXPECT_EQ("0", to_string(UInt32_0(0)));
	EXPECT_EQ("1", to_string(UInt32_0(1)));
	EXPECT_EQ("10000", to_string(UInt32_0(10000)));
	EXPECT_EQ("10001", to_string(UInt32_0(10001)));
	EXPECT_EQ("11000", to_string(UInt32_0(11000)));
	EXPECT_EQ("2147483646", to_string(UInt32_0(2147483646)));
	EXPECT_EQ("2147483647", to_string(UInt32_0(2147483647)));
	EXPECT_EQ("2147483648", to_string(UInt32_0(2147483648)));
	EXPECT_EQ("4294967294", to_string(UInt32_0(4294967294)));
	EXPECT_EQ("4294967295", to_string(UInt32_0(4294967295)));

	EXPECT_EQ("-2147483648", to_string(Int32_0(-2147483648)));
	EXPECT_EQ("-2147483647", to_string(Int32_0(-2147483647)));
	EXPECT_EQ("-11000", to_string(Int32_0(-11000)));
	EXPECT_EQ("-10001", to_string(Int32_0(-10001)));
	EXPECT_EQ("-10000", to_string(Int32_0(-10000)));
	EXPECT_EQ("-1", to_string(Int32_0(-1)));
	EXPECT_EQ("0", to_string(Int32_0(0)));
	EXPECT_EQ("1", to_string(Int32_0(1)));
	EXPECT_EQ("10000", to_string(Int32_0(10000)));
	EXPECT_EQ("10001", to_string(Int32_0(10001)));
	EXPECT_EQ("11000", to_string(Int32_0(11000)));
	EXPECT_EQ("2147483646", to_string(Int32_0(2147483646)));
	EXPECT_EQ("2147483647", to_string(Int32_0(2147483647)));

	EXPECT_EQ("0.0000", to_string(UInt32_4(0)));
	EXPECT_EQ("0.0001", to_string(UInt32_4(1)));
	EXPECT_EQ("1.0000", to_string(UInt32_4(10000)));
	EXPECT_EQ("1.0001", to_string(UInt32_4(10001)));
	EXPECT_EQ("1.1000", to_string(UInt32_4(11000)));
	EXPECT_EQ("214748.3646", to_string(UInt32_4(2147483646)));
	EXPECT_EQ("214748.3647", to_string(UInt32_4(2147483647)));
	EXPECT_EQ("214748.3648", to_string(UInt32_4(2147483648)));
	EXPECT_EQ("429496.7294", to_string(UInt32_4(4294967294)));
	EXPECT_EQ("429496.7295", to_string(UInt32_4(4294967295)));

	EXPECT_EQ("-214748.3648", to_string(Int32_4(-2147483648)));
	EXPECT_EQ("-214748.3647", to_string(Int32_4(-2147483647)));
	EXPECT_EQ("-1.1000", to_string(Int32_4(-11000)));
	EXPECT_EQ("-1.0001", to_string(Int32_4(-10001)));
	EXPECT_EQ("-1.0000", to_string(Int32_4(-10000)));
	EXPECT_EQ("-0.0001", to_string(Int32_4(-1)));
	EXPECT_EQ("0.0000", to_string(Int32_4(0)));
	EXPECT_EQ("0.0001", to_string(Int32_4(1)));
	EXPECT_EQ("1.0000", to_string(Int32_4(10000)));
	EXPECT_EQ("1.0001", to_string(Int32_4(10001)));
	EXPECT_EQ("1.1000", to_string(Int32_4(11000)));
	EXPECT_EQ("214748.3646", to_string(Int32_4(2147483646)));
	EXPECT_EQ("214748.3647", to_string(Int32_4(2147483647)));

	EXPECT_EQ("0", to_string(UInt64_0(0)));
	EXPECT_EQ("1", to_string(UInt64_0(1)));
	EXPECT_EQ(
		"18446744073709551614", to_string(UInt64_0(18446744073709551614UL)));
	EXPECT_EQ(
		"18446744073709551615", to_string(UInt64_0(18446744073709551615UL)));

	EXPECT_EQ(
		"-9223372036854775808", to_string(Int64_0(-9223372036854775808UL)));
	EXPECT_EQ(
		"-9223372036854775807", to_string(Int64_0(-9223372036854775807L)));
	EXPECT_EQ("-1", to_string(Int64_0(-1)));
	EXPECT_EQ("0", to_string(Int64_0(0)));
	EXPECT_EQ("1", to_string(Int64_0(1)));
	EXPECT_EQ("9223372036854775806", to_string(Int64_0(9223372036854775806L)));
	EXPECT_EQ("9223372036854775807", to_string(Int64_0(9223372036854775807L)));

	EXPECT_EQ("0.0000", to_string(UInt64_4(0)));
	EXPECT_EQ("0.0001", to_string(UInt64_4(1)));
	EXPECT_EQ(
		"1844674407370955.1614", to_string(UInt64_4(18446744073709551614UL)));
	EXPECT_EQ(
		"1844674407370955.1615", to_string(UInt64_4(18446744073709551615UL)));

	EXPECT_EQ(
		"-922337203685477.5808", to_string(Int64_4(-9223372036854775808UL)));
	EXPECT_EQ(
		"-922337203685477.5807", to_string(Int64_4(-9223372036854775807L)));
	EXPECT_EQ("-0.0001", to_string(Int64_4(-1)));
	EXPECT_EQ("0.0000", to_string(Int64_4(0)));
	EXPECT_EQ("0.0001", to_string(Int64_4(1)));
	EXPECT_EQ("922337203685477.5806", to_string(Int64_4(9223372036854775806L)));
	EXPECT_EQ("922337203685477.5807", to_string(Int64_4(9223372036854775807L)));

	EXPECT_EQ("1234567891", to_string(UInt32_0(1234567891)));
	EXPECT_EQ("123456789.1", to_string(UInt32_1(1234567891)));
	EXPECT_EQ("12345678.91", to_string(UInt32_2(1234567891)));
	EXPECT_EQ("1234567.891", to_string(UInt32_3(1234567891)));
	EXPECT_EQ("123456.7891", to_string(UInt32_4(1234567891)));
	EXPECT_EQ("12345.67891", to_string(UInt32_5(1234567891)));
	EXPECT_EQ("1234.567891", to_string(UInt32_6(1234567891)));
	EXPECT_EQ("123.4567891", to_string(UInt32_7(1234567891)));
	EXPECT_EQ("12.34567891", to_string(UInt32_8(1234567891)));
	EXPECT_EQ("1.234567891", to_string(UInt32_9(1234567891)));
	EXPECT_EQ("0.1234567891", to_string(UInt32_10(1234567891)));

	EXPECT_EQ("1234567891", to_string(Int32_0(1234567891)));
	EXPECT_EQ("123456789.1", to_string(Int32_1(1234567891)));
	EXPECT_EQ("12345678.91", to_string(Int32_2(1234567891)));
	EXPECT_EQ("1234567.891", to_string(Int32_3(1234567891)));
	EXPECT_EQ("123456.7891", to_string(Int32_4(1234567891)));
	EXPECT_EQ("12345.67891", to_string(Int32_5(1234567891)));
	EXPECT_EQ("1234.567891", to_string(Int32_6(1234567891)));
	EXPECT_EQ("123.4567891", to_string(Int32_7(1234567891)));
	EXPECT_EQ("12.34567891", to_string(Int32_8(1234567891)));
	EXPECT_EQ("1.234567891", to_string(Int32_9(1234567891)));
	EXPECT_EQ("0.1234567891", to_string(Int32_10(1234567891)));

	EXPECT_EQ("-1234567891", to_string(Int32_0(-1234567891)));
	EXPECT_EQ("-123456789.1", to_string(Int32_1(-1234567891)));
	EXPECT_EQ("-12345678.91", to_string(Int32_2(-1234567891)));
	EXPECT_EQ("-1234567.891", to_string(Int32_3(-1234567891)));
	EXPECT_EQ("-123456.7891", to_string(Int32_4(-1234567891)));
	EXPECT_EQ("-12345.67891", to_string(Int32_5(-1234567891)));
	EXPECT_EQ("-1234.567891", to_string(Int32_6(-1234567891)));
	EXPECT_EQ("-123.4567891", to_string(Int32_7(-1234567891)));
	EXPECT_EQ("-12.34567891", to_string(Int32_8(-1234567891)));
	EXPECT_EQ("-1.234567891", to_string(Int32_9(-1234567891)));
	EXPECT_EQ("-0.1234567891", to_string(Int32_10(-1234567891)));

	EXPECT_EQ(
		"12345678912345678912", to_string(UInt64_0(12345678912345678912UL)));
	EXPECT_EQ(
		"1234567891234567891.2", to_string(UInt64_1(12345678912345678912UL)));
	EXPECT_EQ(
		"123456789123456789.12", to_string(UInt64_2(12345678912345678912UL)));
	EXPECT_EQ(
		"12345678912345678.912", to_string(UInt64_3(12345678912345678912UL)));
	EXPECT_EQ(
		"1234567891234567.8912", to_string(UInt64_4(12345678912345678912UL)));
	EXPECT_EQ(
		"123456789123456.78912", to_string(UInt64_5(12345678912345678912UL)));
	EXPECT_EQ(
		"12345678912345.678912", to_string(UInt64_6(12345678912345678912UL)));
	EXPECT_EQ(
		"1234567891234.5678912", to_string(UInt64_7(12345678912345678912UL)));
	EXPECT_EQ(
		"123456789123.45678912", to_string(UInt64_8(12345678912345678912UL)));
	EXPECT_EQ(
		"12345678912.345678912", to_string(UInt64_9(12345678912345678912UL)));
	EXPECT_EQ(
		"1234567891.2345678912", to_string(UInt64_10(12345678912345678912UL)));

	EXPECT_EQ("1234567891234567891", to_string(Int64_0(1234567891234567891L)));
	EXPECT_EQ("123456789123456789.1", to_string(Int64_1(1234567891234567891L)));
	EXPECT_EQ("12345678912345678.91", to_string(Int64_2(1234567891234567891L)));
	EXPECT_EQ("1234567891234567.891", to_string(Int64_3(1234567891234567891L)));
	EXPECT_EQ("123456789123456.7891", to_string(Int64_4(1234567891234567891L)));
	EXPECT_EQ("12345678912345.67891", to_string(Int64_5(1234567891234567891L)));
	EXPECT_EQ("1234567891234.567891", to_string(Int64_6(1234567891234567891L)));
	EXPECT_EQ("123456789123.4567891", to_string(Int64_7(1234567891234567891L)));
	EXPECT_EQ("12345678912.34567891", to_string(Int64_8(1234567891234567891L)));
	EXPECT_EQ("1234567891.234567891", to_string(Int64_9(1234567891234567891L)));
	EXPECT_EQ(
		"123456789.1234567891", to_string(Int64_10(1234567891234567891L)));

	EXPECT_EQ(
		"-1234567891234567891", to_string(Int64_0(-1234567891234567891L)));
	EXPECT_EQ(
		"-123456789123456789.1", to_string(Int64_1(-1234567891234567891L)));
	EXPECT_EQ(
		"-12345678912345678.91", to_string(Int64_2(-1234567891234567891L)));
	EXPECT_EQ(
		"-1234567891234567.891", to_string(Int64_3(-1234567891234567891L)));
	EXPECT_EQ(
		"-123456789123456.7891", to_string(Int64_4(-1234567891234567891L)));
	EXPECT_EQ(
		"-12345678912345.67891", to_string(Int64_5(-1234567891234567891L)));
	EXPECT_EQ(
		"-1234567891234.567891", to_string(Int64_6(-1234567891234567891L)));
	EXPECT_EQ(
		"-123456789123.4567891", to_string(Int64_7(-1234567891234567891L)));
	EXPECT_EQ(
		"-12345678912.34567891", to_string(Int64_8(-1234567891234567891L)));
	EXPECT_EQ(
		"-1234567891.234567891", to_string(Int64_9(-1234567891234567891L)));
	EXPECT_EQ(
		"-123456789.1234567891", to_string(Int64_10(-1234567891234567891L)));
}

TEST(Numeric_type, to_string_decimals_unspecified)
{
	EXPECT_EQ("0", to_string(UInt32_x(0), 0));
	EXPECT_EQ("1", to_string(UInt32_x(1), 0));
	EXPECT_EQ("10000", to_string(UInt32_x(10000), 0));
	EXPECT_EQ("10001", to_string(UInt32_x(10001), 0));
	EXPECT_EQ("11000", to_string(UInt32_x(11000), 0));
	EXPECT_EQ("2147483646", to_string(UInt32_x(2147483646), 0));
	EXPECT_EQ("2147483647", to_string(UInt32_x(2147483647), 0));
	EXPECT_EQ("2147483648", to_string(UInt32_x(2147483648), 0));
	EXPECT_EQ("4294967294", to_string(UInt32_x(4294967294), 0));
	EXPECT_EQ("4294967295", to_string(UInt32_x(4294967295), 0));

	EXPECT_EQ("-2147483648", to_string(Int32_x(-2147483648), 0));
	EXPECT_EQ("-2147483647", to_string(Int32_x(-2147483647), 0));
	EXPECT_EQ("-11000", to_string(Int32_x(-11000), 0));
	EXPECT_EQ("-10001", to_string(Int32_x(-10001), 0));
	EXPECT_EQ("-10000", to_string(Int32_x(-10000), 0));
	EXPECT_EQ("-1", to_string(Int32_x(-1), 0));
	EXPECT_EQ("0", to_string(Int32_x(0), 0));
	EXPECT_EQ("1", to_string(Int32_x(1), 0));
	EXPECT_EQ("10000", to_string(Int32_x(10000), 0));
	EXPECT_EQ("10001", to_string(Int32_x(10001), 0));
	EXPECT_EQ("11000", to_string(Int32_x(11000), 0));
	EXPECT_EQ("2147483646", to_string(Int32_x(2147483646), 0));
	EXPECT_EQ("2147483647", to_string(Int32_x(2147483647), 0));

	EXPECT_EQ("0.0000", to_string(UInt32_x(0), 4));
	EXPECT_EQ("0.0001", to_string(UInt32_x(1), 4));
	EXPECT_EQ("1.0000", to_string(UInt32_x(10000), 4));
	EXPECT_EQ("1.0001", to_string(UInt32_x(10001), 4));
	EXPECT_EQ("1.1000", to_string(UInt32_x(11000), 4));
	EXPECT_EQ("214748.3646", to_string(UInt32_x(2147483646), 4));
	EXPECT_EQ("214748.3647", to_string(UInt32_x(2147483647), 4));
	EXPECT_EQ("214748.3648", to_string(UInt32_x(2147483648), 4));
	EXPECT_EQ("429496.7294", to_string(UInt32_x(4294967294), 4));
	EXPECT_EQ("429496.7295", to_string(UInt32_x(4294967295), 4));

	EXPECT_EQ("-214748.3648", to_string(Int32_x(-2147483648), 4));
	EXPECT_EQ("-214748.3647", to_string(Int32_x(-2147483647), 4));
	EXPECT_EQ("-1.1000", to_string(Int32_x(-11000), 4));
	EXPECT_EQ("-1.0001", to_string(Int32_x(-10001), 4));
	EXPECT_EQ("-1.0000", to_string(Int32_x(-10000), 4));
	EXPECT_EQ("-0.0001", to_string(Int32_x(-1), 4));
	EXPECT_EQ("0.0000", to_string(Int32_x(0), 4));
	EXPECT_EQ("0.0001", to_string(Int32_x(1), 4));
	EXPECT_EQ("1.0000", to_string(Int32_x(10000), 4));
	EXPECT_EQ("1.0001", to_string(Int32_x(10001), 4));
	EXPECT_EQ("1.1000", to_string(Int32_x(11000), 4));
	EXPECT_EQ("214748.3646", to_string(Int32_x(2147483646), 4));
	EXPECT_EQ("214748.3647", to_string(Int32_x(2147483647), 4));

	EXPECT_EQ("0", to_string(UInt64_x(0), 0));
	EXPECT_EQ("1", to_string(UInt64_x(1), 0));
	EXPECT_EQ(
		"18446744073709551614", to_string(UInt64_x(18446744073709551614UL), 0));
	EXPECT_EQ(
		"18446744073709551615", to_string(UInt64_x(18446744073709551615UL), 0));

	EXPECT_EQ(
		"-9223372036854775808", to_string(Int64_x(-9223372036854775808UL), 0));
	EXPECT_EQ(
		"-9223372036854775807", to_string(Int64_x(-9223372036854775807L), 0));
	EXPECT_EQ("-1", to_string(Int64_x(-1), 0));
	EXPECT_EQ("0", to_string(Int64_x(0), 0));
	EXPECT_EQ("1", to_string(Int64_x(1), 0));
	EXPECT_EQ(
		"9223372036854775806", to_string(Int64_x(9223372036854775806L), 0));
	EXPECT_EQ(
		"9223372036854775807", to_string(Int64_x(9223372036854775807L), 0));

	EXPECT_EQ("0.0000", to_string(UInt64_x(0), 4));
	EXPECT_EQ("0.0001", to_string(UInt64_x(1), 4));
	EXPECT_EQ("1844674407370955.1614",
		to_string(UInt64_x(18446744073709551614UL), 4));
	EXPECT_EQ("1844674407370955.1615",
		to_string(UInt64_x(18446744073709551615UL), 4));

	EXPECT_EQ(
		"-922337203685477.5808", to_string(Int64_x(-9223372036854775808UL), 4));
	EXPECT_EQ(
		"-922337203685477.5807", to_string(Int64_x(-9223372036854775807L), 4));
	EXPECT_EQ("-0.0001", to_string(Int64_x(-1), 4));
	EXPECT_EQ("0.0000", to_string(Int64_x(0), 4));
	EXPECT_EQ("0.0001", to_string(Int64_x(1), 4));
	EXPECT_EQ(
		"922337203685477.5806", to_string(Int64_x(9223372036854775806L), 4));
	EXPECT_EQ(
		"922337203685477.5807", to_string(Int64_x(9223372036854775807L), 4));

	EXPECT_EQ("1234567891", to_string(UInt32_x(1234567891), 0));
	EXPECT_EQ("123456789.1", to_string(UInt32_x(1234567891), 1));
	EXPECT_EQ("12345678.91", to_string(UInt32_x(1234567891), 2));
	EXPECT_EQ("1234567.891", to_string(UInt32_x(1234567891), 3));
	EXPECT_EQ("123456.7891", to_string(UInt32_x(1234567891), 4));
	EXPECT_EQ("12345.67891", to_string(UInt32_x(1234567891), 5));
	EXPECT_EQ("1234.567891", to_string(UInt32_x(1234567891), 6));
	EXPECT_EQ("123.4567891", to_string(UInt32_x(1234567891), 7));
	EXPECT_EQ("12.34567891", to_string(UInt32_x(1234567891), 8));
	EXPECT_EQ("1.234567891", to_string(UInt32_x(1234567891), 9));
	EXPECT_EQ("0.1234567891", to_string(UInt32_x(1234567891), 10));

	EXPECT_EQ("1234567891", to_string(Int32_x(1234567891), 0));
	EXPECT_EQ("123456789.1", to_string(Int32_x(1234567891), 1));
	EXPECT_EQ("12345678.91", to_string(Int32_x(1234567891), 2));
	EXPECT_EQ("1234567.891", to_string(Int32_x(1234567891), 3));
	EXPECT_EQ("123456.7891", to_string(Int32_x(1234567891), 4));
	EXPECT_EQ("12345.67891", to_string(Int32_x(1234567891), 5));
	EXPECT_EQ("1234.567891", to_string(Int32_x(1234567891), 6));
	EXPECT_EQ("123.4567891", to_string(Int32_x(1234567891), 7));
	EXPECT_EQ("12.34567891", to_string(Int32_x(1234567891), 8));
	EXPECT_EQ("1.234567891", to_string(Int32_x(1234567891), 9));
	EXPECT_EQ("0.1234567891", to_string(Int32_x(1234567891), 10));

	EXPECT_EQ("-1234567891", to_string(Int32_x(-1234567891), 0));
	EXPECT_EQ("-123456789.1", to_string(Int32_x(-1234567891), 1));
	EXPECT_EQ("-12345678.91", to_string(Int32_x(-1234567891), 2));
	EXPECT_EQ("-1234567.891", to_string(Int32_x(-1234567891), 3));
	EXPECT_EQ("-123456.7891", to_string(Int32_x(-1234567891), 4));
	EXPECT_EQ("-12345.67891", to_string(Int32_x(-1234567891), 5));
	EXPECT_EQ("-1234.567891", to_string(Int32_x(-1234567891), 6));
	EXPECT_EQ("-123.4567891", to_string(Int32_x(-1234567891), 7));
	EXPECT_EQ("-12.34567891", to_string(Int32_x(-1234567891), 8));
	EXPECT_EQ("-1.234567891", to_string(Int32_x(-1234567891), 9));
	EXPECT_EQ("-0.1234567891", to_string(Int32_x(-1234567891), 10));

	EXPECT_EQ(
		"12345678912345678912", to_string(UInt64_x(12345678912345678912UL), 0));
	EXPECT_EQ("1234567891234567891.2",
		to_string(UInt64_x(12345678912345678912UL), 1));
	EXPECT_EQ("123456789123456789.12",
		to_string(UInt64_x(12345678912345678912UL), 2));
	EXPECT_EQ("12345678912345678.912",
		to_string(UInt64_x(12345678912345678912UL), 3));
	EXPECT_EQ("1234567891234567.8912",
		to_string(UInt64_x(12345678912345678912UL), 4));
	EXPECT_EQ("123456789123456.78912",
		to_string(UInt64_x(12345678912345678912UL), 5));
	EXPECT_EQ("12345678912345.678912",
		to_string(UInt64_x(12345678912345678912UL), 6));
	EXPECT_EQ("1234567891234.5678912",
		to_string(UInt64_x(12345678912345678912UL), 7));
	EXPECT_EQ("123456789123.45678912",
		to_string(UInt64_x(12345678912345678912UL), 8));
	EXPECT_EQ("12345678912.345678912",
		to_string(UInt64_x(12345678912345678912UL), 9));
	EXPECT_EQ("1234567891.2345678912",
		to_string(UInt64_x(12345678912345678912UL), 10));

	EXPECT_EQ(
		"1234567891234567891", to_string(Int64_x(1234567891234567891L), 0));
	EXPECT_EQ(
		"123456789123456789.1", to_string(Int64_x(1234567891234567891L), 1));
	EXPECT_EQ(
		"12345678912345678.91", to_string(Int64_x(1234567891234567891L), 2));
	EXPECT_EQ(
		"1234567891234567.891", to_string(Int64_x(1234567891234567891L), 3));
	EXPECT_EQ(
		"123456789123456.7891", to_string(Int64_x(1234567891234567891L), 4));
	EXPECT_EQ(
		"12345678912345.67891", to_string(Int64_x(1234567891234567891L), 5));
	EXPECT_EQ(
		"1234567891234.567891", to_string(Int64_x(1234567891234567891L), 6));
	EXPECT_EQ(
		"123456789123.4567891", to_string(Int64_x(1234567891234567891L), 7));
	EXPECT_EQ(
		"12345678912.34567891", to_string(Int64_x(1234567891234567891L), 8));
	EXPECT_EQ(
		"1234567891.234567891", to_string(Int64_x(1234567891234567891L), 9));
	EXPECT_EQ(
		"123456789.1234567891", to_string(Int64_x(1234567891234567891L), 10));

	EXPECT_EQ(
		"-1234567891234567891", to_string(Int64_x(-1234567891234567891L), 0));
	EXPECT_EQ(
		"-123456789123456789.1", to_string(Int64_x(-1234567891234567891L), 1));
	EXPECT_EQ(
		"-12345678912345678.91", to_string(Int64_x(-1234567891234567891L), 2));
	EXPECT_EQ(
		"-1234567891234567.891", to_string(Int64_x(-1234567891234567891L), 3));
	EXPECT_EQ(
		"-123456789123456.7891", to_string(Int64_x(-1234567891234567891L), 4));
	EXPECT_EQ(
		"-12345678912345.67891", to_string(Int64_x(-1234567891234567891L), 5));
	EXPECT_EQ(
		"-1234567891234.567891", to_string(Int64_x(-1234567891234567891L), 6));
	EXPECT_EQ(
		"-123456789123.4567891", to_string(Int64_x(-1234567891234567891L), 7));
	EXPECT_EQ(
		"-12345678912.34567891", to_string(Int64_x(-1234567891234567891L), 8));
	EXPECT_EQ(
		"-1234567891.234567891", to_string(Int64_x(-1234567891234567891L), 9));
	EXPECT_EQ(
		"-123456789.1234567891", to_string(Int64_x(-1234567891234567891L), 10));
}

TEST(Numeric_type, to_double)
{
	// 32 bit.
	{
		UInt32_0 num(12345);
		auto value = to_double(num);
		EXPECT_EQ(12345, value);
	}
	{
		constexpr UInt32_0 num(12345);
		constexpr auto value = to_double(num);
		EXPECT_EQ(12345, value);
	}
	{
		UInt32_4 num(12345);
		auto value = to_double(num);
		EXPECT_EQ(1.2345, value);
	}
	{
		constexpr UInt32_4 num(12345);
		constexpr auto value = to_double(num);
		EXPECT_EQ(1.2345, value);
	}
	// 64 bit.
	{
		UInt64_0 num(12345);
		auto value = to_double(num);
		EXPECT_EQ(12345, value);
	}
	{
		constexpr UInt64_0 num(12345);
		constexpr auto value = to_double(num);
		EXPECT_EQ(12345, value);
	}
	{
		UInt64_4 num(12345);
		auto value = to_double(num);
		EXPECT_EQ(1.2345, value);
	}
	{
		constexpr UInt64_4 num(12345);
		constexpr auto value = to_double(num);
		EXPECT_EQ(1.2345, value);
	}
}

TEST(Numeric_type, to_double_decimals_unspecified)
{
	// 32 bit.
	{
		UInt32_x num(12345);
		auto value = to_double(num, 0);
		EXPECT_EQ(12345, value);
	}
	{
		constexpr UInt32_x num(12345);
		constexpr auto value = to_double(num, 0);
		EXPECT_EQ(12345, value);
	}
	{
		UInt32_x num(12345);
		auto value = to_double(num, 4);
		EXPECT_EQ(1.2345, value);
	}
	{
		constexpr UInt32_x num(12345);
		constexpr auto value = to_double(num, 4);
		EXPECT_EQ(1.2345, value);
	}
	// 64 bit.
	{
		UInt64_x num(12345);
		auto value = to_double(num, 0);
		EXPECT_EQ(12345, value);
	}
	{
		constexpr UInt64_x num(12345);
		constexpr auto value = to_double(num, 0);
		EXPECT_EQ(12345, value);
	}
	{
		UInt64_x num(12345);
		auto value = to_double(num, 4);
		EXPECT_EQ(1.2345, value);
	}
	{
		constexpr UInt64_x num(12345);
		constexpr auto value = to_double(num, 4);
		EXPECT_EQ(1.2345, value);
	}
}

TEST(Numeric_type, output_stream_operator)
{
	// 32 bit.
	{
		std::ostringstream os;
		os << UInt32_4(1000012);
		EXPECT_EQ("100.0012", os.str());
	}
	{
		std::ostringstream os;
		os << Int32_4(-1000012);
		EXPECT_EQ("-100.0012", os.str());
	}
	{
		std::ostringstream os;
		os << UInt32_0(100);
		EXPECT_EQ("100", os.str());
	}
	{
		std::ostringstream os;
		os << Int32_0(-100);
		EXPECT_EQ("-100", os.str());
	}
}

TEST(Numeric_type, input_stream_operator)
{
	// 32 bit.
	{
		UInt32_4 num;
		std::istringstream is("100.0012");
		is >> num;
		EXPECT_EQ(1000012u, num.value());
	}
	{
		Int32_4 num;
		std::istringstream is("-100.0012");
		is >> num;
		EXPECT_EQ(-1000012, num.value());
	}
	{
		UInt32_0 num;
		std::istringstream is("100");
		is >> num;
		EXPECT_EQ(100u, num.value());
	}
	{
		Int32_0 num;
		std::istringstream is("-100");
		is >> num;
		EXPECT_EQ(-100, num.value());
	}
}

TEST(Numeric_type, custom_arithmetic_operator)
{
	UInt32_4 uint32_4(12345);
	UInt32_0 uint32_0(67890);
	auto uint64_4 = uint32_4 * uint32_0;
	EXPECT_EQ(838102050u, uint64_4.value());

	auto b = space::Ship() * space::Alien();
	EXPECT_TRUE(b);
}

namespace {

template <typename Map>
void test_map(Map& m)
{
	m.emplace(UInt32_0(1), 10);
	m.emplace(UInt32_0(3), 30);
	m.emplace(UInt32_0(2), 20);

	auto i = m.find(UInt32_0(1));
	ASSERT_NE(m.end(), i);
	EXPECT_EQ(10, i->second);

	i = m.find(UInt32_0(2));
	ASSERT_NE(m.end(), i);
	EXPECT_EQ(20, i->second);

	i = m.find(UInt32_0(3));
	ASSERT_NE(m.end(), i);
	EXPECT_EQ(30, i->second);
}

} // namespace

TEST(Numeric_type, use_in_std_map)
{
	std::map<UInt32_0, int> m;
	test_map(m);
}

TEST(Numeric_type, use_in_std_unordered_map)
{
	std::unordered_map<UInt32_0, int> m;
	test_map(m);
}
