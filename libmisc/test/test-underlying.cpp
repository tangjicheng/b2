#include <istream>
#include <ostream>
#include <sstream>

#include <gtest/gtest.h>

#include <misc/underlying.h>

namespace {

class Price {
public:
	typedef unsigned long UnderlyingType;

	explicit Price(UnderlyingType value)
		: m_value(value)
	{
	}

	UnderlyingType value() const { return m_value; }

private:
	UnderlyingType m_value;
};

struct PriceTraits {
	static std::ostream& out(std::ostream& os, Price rhs)
	{
		return os << rhs.value();
	}

	static std::istream& in(std::istream& is, Price& rhs)
	{
		Price::UnderlyingType value;
		is >> value;
		rhs = Price(value);
		return is;
	}
};

} // namespace

namespace misc {

template <>
struct underlying_traits<Price> : PriceTraits {
};

} // namespace misc

class TestUnderlying : public ::testing::Test {
protected:
	enum E1 { E1_A, E1_B = 12345 };

	enum class E2 { X, Y = 12345 };

	std::ostringstream output;
	std::istringstream input;
};

TEST_F(TestUnderlying, output_stream_price)
{
	Price p(12345);
	output << misc::underlying << p;
	EXPECT_EQ("12345", output.str());
}

TEST_F(TestUnderlying, output_stream_enum)
{
	E1 e = E1_B;
	output << misc::underlying << e;
	EXPECT_EQ("12345", output.str());
}

TEST_F(TestUnderlying, output_stream_enum_class)
{
	E2 e = E2::Y;
	output << misc::underlying << e;
	EXPECT_EQ("12345", output.str());
}

TEST_F(TestUnderlying, input_stream_price)
{
	Price p(0);
	input.str("12345");
	input >> misc::underlying >> p;
	EXPECT_EQ(12345u, p.value());
}

TEST_F(TestUnderlying, input_stream_enum)
{
	E1 e = E1_A;
	input.str("12345");
	input >> misc::underlying >> e;
	EXPECT_EQ(E1_B, e);
}

TEST_F(TestUnderlying, input_stream_enum_class)
{
	E2 e = E2::X;
	input.str("12345");
	input >> misc::underlying >> e;
	EXPECT_EQ(E2::Y, e);
}
