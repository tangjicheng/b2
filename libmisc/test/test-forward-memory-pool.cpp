#include <gtest/gtest.h>

#include <misc/ForwardMemoryPool.h>

class TestForwardMemoryPool : public ::testing::Test {
protected:
	char data[12];
	misc::ForwardMemoryPool pool {data, sizeof(data)};
	char* ptr {nullptr};

	void SetUp() override { ptr = reinterpret_cast<char*>(pool.allocate(6)); }
};

TEST_F(TestForwardMemoryPool, size)
{
	EXPECT_EQ(12u, pool.size());
}

TEST_F(TestForwardMemoryPool, used)
{
	EXPECT_EQ(6u, pool.used());
}

TEST_F(TestForwardMemoryPool, balance)
{
	EXPECT_EQ(6, pool.balance());
}

TEST_F(TestForwardMemoryPool, reset)
{
	pool.reset();
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(0u, pool.used());
	EXPECT_EQ(0, pool.balance());
}

TEST_F(TestForwardMemoryPool, allocate)
{
	EXPECT_EQ(ptr, data);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(6u, pool.used());
	EXPECT_EQ(6, pool.balance());

	EXPECT_EQ(ptr + 6, pool.allocate(4));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(10u, pool.used());
	EXPECT_EQ(10, pool.balance());

	EXPECT_EQ(ptr + 10, pool.allocate(2));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(12, pool.balance());

	EXPECT_THROW(pool.allocate(1), std::bad_alloc);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(12, pool.balance());
}

TEST_F(TestForwardMemoryPool, deallocate)
{
	EXPECT_EQ(ptr + 6, pool.allocate(2));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(8u, pool.used());
	EXPECT_EQ(8, pool.balance());

	pool.deallocate(ptr, 6);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(8u, pool.used());
	EXPECT_EQ(2, pool.balance());

	pool.deallocate(ptr + 6, 2);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(8u, pool.used());
	EXPECT_EQ(0, pool.balance());

	EXPECT_EQ(ptr + 8, pool.allocate(4));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(4, pool.balance());

	pool.deallocate(ptr + 8, 4);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(0, pool.balance());

	EXPECT_THROW(pool.allocate(1), std::bad_alloc);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(0, pool.balance());
}

class TestHeapForwardMemoryPool : public ::testing::Test {
protected:
	misc::HeapForwardMemoryPool pool {12};
	char* ptr {nullptr};

	void SetUp() override { ptr = reinterpret_cast<char*>(pool.allocate(6)); }
};

TEST_F(TestHeapForwardMemoryPool, size)
{
	EXPECT_EQ(12u, pool.size());
}

TEST_F(TestHeapForwardMemoryPool, used)
{
	EXPECT_EQ(6u, pool.used());
}

TEST_F(TestHeapForwardMemoryPool, balance)
{
	EXPECT_EQ(6, pool.balance());
}

TEST_F(TestHeapForwardMemoryPool, reset)
{
	pool.reset();
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(0u, pool.used());
	EXPECT_EQ(0, pool.balance());
}

TEST_F(TestHeapForwardMemoryPool, allocate)
{
	EXPECT_TRUE(ptr != nullptr);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(6u, pool.used());
	EXPECT_EQ(6, pool.balance());

	EXPECT_EQ(ptr + 6, pool.allocate(4));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(10u, pool.used());
	EXPECT_EQ(10, pool.balance());

	EXPECT_EQ(ptr + 10, pool.allocate(2));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(12, pool.balance());

	EXPECT_THROW(pool.allocate(1), std::bad_alloc);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(12, pool.balance());
}

TEST_F(TestHeapForwardMemoryPool, deallocate)
{
	EXPECT_EQ(ptr + 6, pool.allocate(2));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(8u, pool.used());
	EXPECT_EQ(8, pool.balance());

	pool.deallocate(ptr, 6);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(8u, pool.used());
	EXPECT_EQ(2, pool.balance());

	pool.deallocate(ptr + 6, 2);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(8u, pool.used());
	EXPECT_EQ(0, pool.balance());

	EXPECT_EQ(ptr + 8, pool.allocate(4));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(4, pool.balance());

	pool.deallocate(ptr + 8, 4);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(0, pool.balance());

	EXPECT_THROW(pool.allocate(1), std::bad_alloc);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(0, pool.balance());
}

class TestArrayForwardMemoryPool : public ::testing::Test {
protected:
	misc::ArrayForwardMemoryPool<12> pool;
	char* ptr {nullptr};

	void SetUp() override { ptr = reinterpret_cast<char*>(pool.allocate(6)); }
};

TEST_F(TestArrayForwardMemoryPool, size)
{
	EXPECT_EQ(12u, pool.size());
}

TEST_F(TestArrayForwardMemoryPool, used)
{
	EXPECT_EQ(6u, pool.used());
}

TEST_F(TestArrayForwardMemoryPool, balance)
{
	EXPECT_EQ(6, pool.balance());
}

TEST_F(TestArrayForwardMemoryPool, reset)
{
	pool.reset();
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(0u, pool.used());
	EXPECT_EQ(0, pool.balance());
}

TEST_F(TestArrayForwardMemoryPool, allocate)
{
	EXPECT_TRUE(ptr != nullptr);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(6u, pool.used());
	EXPECT_EQ(6, pool.balance());

	EXPECT_EQ(ptr + 6, pool.allocate(4));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(10u, pool.used());
	EXPECT_EQ(10, pool.balance());

	EXPECT_EQ(ptr + 10, pool.allocate(2));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(12, pool.balance());

	EXPECT_THROW(pool.allocate(1), std::bad_alloc);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(12, pool.balance());
}

TEST_F(TestArrayForwardMemoryPool, deallocate)
{
	EXPECT_EQ(ptr + 6, pool.allocate(2));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(8u, pool.used());
	EXPECT_EQ(8, pool.balance());

	pool.deallocate(ptr, 6);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(8u, pool.used());
	EXPECT_EQ(2, pool.balance());

	pool.deallocate(ptr + 6, 2);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(8u, pool.used());
	EXPECT_EQ(0, pool.balance());

	EXPECT_EQ(ptr + 8, pool.allocate(4));
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(4, pool.balance());

	pool.deallocate(ptr + 8, 4);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(0, pool.balance());

	EXPECT_THROW(pool.allocate(1), std::bad_alloc);
	EXPECT_EQ(12u, pool.size());
	EXPECT_EQ(12u, pool.used());
	EXPECT_EQ(0, pool.balance());
}
