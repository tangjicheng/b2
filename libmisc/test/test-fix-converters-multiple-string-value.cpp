#include <stdexcept>
#include <string>
#include <vector>

#include <gtest/gtest.h>

#include <misc/fix/Converters.h>

#include "unittest.h"

TEST(fixMultipleStringValueConvertFromString, valid)
{
	EXPECT_EQ((std::vector<std::string> {"AV"}),
		misc::fix::multipleStringValueConvert(std::string("AV")));
	EXPECT_EQ((std::vector<std::string> {"AV", "AVF"}),
		misc::fix::multipleStringValueConvert(std::string("AV AVF")));
	EXPECT_EQ((std::vector<std::string> {"AV", "AVF", "A"}),
		misc::fix::multipleStringValueConvert(std::string("AV AVF A")));
}

TEST(fixMultipleStringValueConvertFromString, badFormat)
{
	EXPECT_THROW_WHAT(misc::fix::multipleStringValueConvert(std::string("")),
		std::invalid_argument, "Bad format");
	EXPECT_THROW_WHAT(misc::fix::multipleStringValueConvert(std::string("AV ")),
		std::invalid_argument, "Bad format");
}

TEST(fixMultipleStringValueConvertFromString, invalidCharacter)
{
	EXPECT_THROW_WHAT(misc::fix::multipleStringValueConvert(std::string(" ")),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::multipleStringValueConvert(std::string(" AV")),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		misc::fix::multipleStringValueConvert(std::string("AV  AVF")),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(
		misc::fix::multipleStringValueConvert(std::string("AV AVF \x0A")),
		std::invalid_argument, "Invalid character");
}

TEST(fixMultipleStringValueConvertToString, valid)
{
	EXPECT_EQ(std::string("AV"),
		misc::fix::multipleStringValueConvert(std::vector<std::string> {"AV"}));
	EXPECT_EQ(std::string("AV AVF"),
		misc::fix::multipleStringValueConvert(
			std::vector<std::string> {"AV", "AVF"}));
	EXPECT_EQ(std::string("AV AVF A"),
		misc::fix::multipleStringValueConvert(
			std::vector<std::string> {"AV", "AVF", "A"}));
}

TEST(fixMultipleStringValueConvertToString, badFormat)
{
	EXPECT_THROW_WHAT(
		misc::fix::multipleStringValueConvert(std::vector<std::string>()),
		std::invalid_argument, "Bad format");
}

TEST(fixMultipleStringValueConvertToString, invalidCharacter)
{
	EXPECT_THROW_WHAT(misc::fix::multipleStringValueConvert(
						  std::vector<std::string> {"AV", "AVF", " "}),
		std::invalid_argument, "Invalid character");
	EXPECT_THROW_WHAT(misc::fix::multipleStringValueConvert(
						  std::vector<std::string> {"AV", "AVF", "\x0A"}),
		std::invalid_argument, "Invalid character");
}
