#include <thread>

#include <gtest/gtest.h>

#include <misc/atomic_list.h>
#include <misc/pow.h>
namespace {

struct Element {
	uint a;
	double b;
};

constexpr std::uint64_t container_size
	= misc::ceiling_power_of_two<std::uint64_t, 10>();
constexpr std::uint64_t max_size = 1000;

} // namespace

TEST(Test_atomic_list, empty_works)
{
	auto list = misc::Atomic_list<Element, container_size> {};
}

TEST(Test_atomic_list, one_element_works)
{
	auto list = misc::Atomic_list<Element, container_size> {};
	auto e = Element {0, 0.5};
	list.push_back(e);
}

TEST(Test_atomic_list, error_check)
{
	auto list = misc::Atomic_list<Element, container_size> {};
	auto valid = false;
	{
		auto bounded_reader = list.bounded_reader(1);
		ASSERT_EQ(bounded_reader.size(), 0u);
		std::tie(std::ignore, valid) = bounded_reader.read(0);
		ASSERT_FALSE(valid);
	}
	for (auto i = 0u; i < max_size; ++i) {
		auto e = Element {i, i + 0.5};
		list.push_back(e);
	}
	{
		auto bounded_reader = list.bounded_reader(max_size - 1);
		ASSERT_EQ(bounded_reader.size(), max_size - 1);
		bounded_reader.refresh_bound();
		ASSERT_EQ(bounded_reader.size(), max_size);
	}
	{
		auto bounded_reader = list.bounded_reader(max_size + 1);
		ASSERT_EQ(bounded_reader.size(), max_size);
		std::tie(std::ignore, valid) = bounded_reader.read(max_size);
		ASSERT_FALSE(valid);
	}
	auto unbounded_reader = list.unbounded_reader();
	{
		std::tie(std::ignore, valid) = unbounded_reader.read(max_size);
		ASSERT_FALSE(valid);
	}
	list.push_back(Element {max_size, max_size + 0.5});
	{
		auto [e, valid] = unbounded_reader.read(max_size);
		ASSERT_TRUE(valid);
		ASSERT_EQ(e->a, max_size);
		ASSERT_EQ(e->b, max_size + 0.5);
	}
	{
		std::tie(std::ignore, valid) = unbounded_reader.read(max_size + 1);
		ASSERT_FALSE(valid);
	}
	list.push_back(Element {max_size + 1, max_size + 1.5});
	{
		auto [e, valid] = unbounded_reader.read(max_size + 1);
		ASSERT_TRUE(valid);
		ASSERT_EQ(e->a, max_size + 1);
		ASSERT_EQ(e->b, max_size + 1.5);
	}
}

TEST(Test_atomic_list, single_thread)
{
	auto list = misc::Atomic_list<Element, container_size> {};
	const auto half_size = max_size / 2;

	for (auto i = 0u; i < max_size; ++i) {
		auto e = Element {i, i + 0.5};
		list.push_back(e);
	}

	auto bounded_reader = list.bounded_reader(max_size);
	for (auto i = 0u; i < max_size; ++i) {
		auto [e, valid] = bounded_reader.read(i);
		ASSERT_TRUE(valid);
		ASSERT_EQ(e->a, i);
		ASSERT_EQ(e->b, i + 0.5);
	}
	{
		auto [e, valid] = bounded_reader.read(half_size);
		ASSERT_TRUE(valid);
		ASSERT_EQ(e->a, half_size);
		ASSERT_EQ(e->b, half_size + 0.5);
	}
	auto unbounded_reader = list.unbounded_reader();
	ASSERT_EQ(unbounded_reader.size(), max_size);
	auto result = unbounded_reader.read_next();
	auto i = 0u;
	while (std::get<1>(result)) {
		ASSERT_EQ(std::get<0>(result)->a, i);
		ASSERT_EQ(std::get<0>(result)->b, i + 0.5);
		result = unbounded_reader.read_next();
		++i;
	}
	{
		auto [e, valid] = unbounded_reader.read(half_size);
		ASSERT_TRUE(valid);
		ASSERT_EQ(e->a, half_size);
		ASSERT_EQ(e->b, half_size + 0.5);
	}
}

TEST(Test_atomic_list, multi_thread_direct)
{
	auto list = misc::Atomic_list<Element, container_size> {};

	auto worker = [&list]() {
		auto size = list.size();
		while (size != max_size) {
			if (size == 0u) {
				size = list.size();
				continue;
			}
			auto reader = list.bounded_reader(size);
			for (auto i = 0u; i < size; ++i) {
				auto [e, valid] = reader.read(i);
				ASSERT_TRUE(valid);
				ASSERT_EQ(e->a, i);
				ASSERT_EQ(e->b, i + 0.5);
			}
			size = list.size();
		}
	};

	std::thread t1 {worker}, t2 {worker}, t3 {worker}, t4 {worker};
	for (auto i = 0u; i < max_size; ++i) {
		auto e = Element {i, i + 0.5};
		list.push_back(e);
	}

	t1.join();
	t2.join();
	t3.join();
	t4.join();
}

TEST(Test_atomic_list, multi_thread_alternating)
{
	auto list = misc::Atomic_list<Element, container_size> {};

	auto worker = [&list]() {
		auto size = list.size();
		auto reader = list.unbounded_reader();
		auto i = 0u;
		while (i != max_size) {
			if (size == 0u) {
				size = list.size();
				continue;
			}
			auto result = reader.read_next();
			while (std::get<1>(result)) {
				ASSERT_NE(std::get<0>(result), nullptr);
				ASSERT_EQ(std::get<0>(result)->a, i);
				ASSERT_EQ(std::get<0>(result)->b, i + 0.5);
				result = reader.read_next();
				++i;
			}
			size = list.size();
		}
	};

	std::thread t1 {worker}, t2 {worker}, t3 {worker}, t4 {worker};
	for (auto i = 0u; i < max_size; ++i) {
		auto e = Element {i, i + 0.5};
		list.push_back(e);
	}

	t1.join();
	t2.join();
	t3.join();
	t4.join();
}

TEST(Test_atomic_list, unique_ptrs)
{
	auto list = misc::Atomic_list<std::unique_ptr<Element>, container_size> {};
	const auto half_size = max_size / 2;

	for (auto i = 0u; i < max_size; ++i) {
		auto e = Element {i, i + 0.5};
		auto e_ptr = std::make_unique<Element>(e);
		list.push_back(std::move(e_ptr));
	}

	auto bounded_reader = list.bounded_reader(max_size);
	for (auto i = 0u; i < max_size; ++i) {
		auto [e, valid] = bounded_reader.read(i);
		ASSERT_TRUE(valid);
		ASSERT_EQ((*e)->a, i);
		ASSERT_EQ((*e)->b, i + 0.5);
	}
	{
		auto [e, valid] = bounded_reader.read(half_size);
		ASSERT_TRUE(valid);
		ASSERT_EQ((*e)->a, half_size);
		ASSERT_EQ((*e)->b, half_size + 0.5);
	}
	auto unbounded_reader = list.unbounded_reader();
	ASSERT_EQ(unbounded_reader.size(), max_size);
	auto result = unbounded_reader.read_next();
	auto i = 0u;
	while (std::get<1>(result)) {
		ASSERT_EQ((*std::get<0>(result))->a, i);
		ASSERT_EQ((*std::get<0>(result))->b, i + 0.5);
		result = unbounded_reader.read_next();
		++i;
	}
	{
		auto [e, valid] = unbounded_reader.read(half_size);
		ASSERT_TRUE(valid);
		ASSERT_EQ((*e)->a, half_size);
		ASSERT_EQ((*e)->b, half_size + 0.5);
	}
}

TEST(Test_atomic_list, move_next)
{
	auto list = misc::Atomic_list<Element, container_size> {};
	const auto half_size = max_size / 2;
	for (auto i = 0u; i < max_size; ++i) {
		const auto e = Element {i, i + 0.5};
		list.push_back(e);
	}

	auto bounded_reader = list.bounded_reader(half_size);
	ASSERT_EQ(bounded_reader.size(), half_size);
	for (auto i = 0u; i < half_size; ++i) {
		ASSERT_TRUE(bounded_reader.move_next());
	}
	ASSERT_FALSE(bounded_reader.move_next());

	auto unbounded_reader = list.unbounded_reader();
	ASSERT_EQ(unbounded_reader.size(), max_size);
	for (auto i = 0u; i < max_size; ++i) {
		ASSERT_TRUE(unbounded_reader.move_next());
	}
	ASSERT_FALSE(unbounded_reader.move_next());
}

TEST(Test_atomic_list, peek_next)
{
	auto list = misc::Atomic_list<Element, container_size> {};
	const auto half_size = max_size / 2;
	for (auto i = 0u; i < max_size; ++i) {
		const auto e = Element {i, i + 0.5};
		list.push_back(e);
	}

	auto bounded_reader = list.bounded_reader(half_size);
	for (auto i = 0u; i < half_size; ++i, bounded_reader.move_next()) {
		const auto [e, valid] = bounded_reader.peek_next();
		ASSERT_TRUE(valid);
		ASSERT_EQ(e->a, i);
		ASSERT_EQ(e->b, i + 0.5);
	}
	{
		const auto [e, valid] = bounded_reader.peek_next();
		ASSERT_FALSE(valid);
		ASSERT_EQ(e, nullptr);
	}

	auto unbounded_reader = list.unbounded_reader();
	for (auto i = 0u; i < max_size; ++i, unbounded_reader.move_next()) {
		const auto [e, valid] = unbounded_reader.peek_next();
		ASSERT_TRUE(valid);
		ASSERT_EQ(e->a, i);
		ASSERT_EQ(e->b, i + 0.5);
	}
	{
		const auto [e, valid] = bounded_reader.peek_next();
		ASSERT_FALSE(valid);
		ASSERT_EQ(e, nullptr);
	}
}
