#include <unistd.h>

#include <cstdlib>
#include <iostream>
#include <ostream>
#include <thread>
#include <vector>

#include <misc/LockedQueue.h>
#include <misc/TaskQueue.h>

namespace {

const unsigned NUMBER_OF_TASKS = 4;
const unsigned NUMBER_OF_WORKERS = 4;

bool keepGoing = true;

class Number {
public:
	Number(int n, misc::TaskQueue& tq)
		: n(n)
		, tq(&tq)
		, t([this] { g(); })
	{
	}

	~Number()
	{
		std::cout << '(' << n << ") Size of data queue " << q.size() << " (";
		while (!q.empty())
			std::cout << ' ' << *q.pop();
		std::cout << " )" << std::endl;
	}

	void f(int i)
	{
		q.push(i);
		tq->enqueue(t);
		std::cout << '(' << n << ") Pushed to data queue and enqueued task "
				  << i << std::endl;
	}

private:
	int n;
	misc::LockedQueue<int> q;
	misc::TaskQueue* tq;
	misc::Task t;

	void g()
	{
		std::cout << '(' << n << ") Task function called by worker "
				  << std::this_thread::get_id() << std::endl;
		while (true) {
			auto i = q.pop();
			if (!i.has_value())
				break;
			std::cout << '(' << n << ") Popped from data queue " << *i
					  << std::endl;
			sleep(1);
		}
		std::cout << '(' << n << ") Data queue now empty" << std::endl;
	}
};

void f(std::vector<Number*>& numbers)
{
	for (int i = 0; keepGoing; ++i) {
		if (i % NUMBER_OF_TASKS == 0)
			sleep(2);
		numbers[i % NUMBER_OF_TASKS]->f(i);
	}
}

} // namespace

int main()
{
	misc::TaskQueue tq;
	std::cout << "Starting pushing thread" << std::endl;
	std::vector<Number*> numbers;
	for (unsigned i = 0; i != NUMBER_OF_TASKS; ++i)
		numbers.push_back(new Number(i, tq));
	std::thread push(&f, std::ref(numbers));
	std::cout << "Starting task queues" << std::endl;
	std::vector<std::thread> workers;
	for (unsigned i = 0; i != NUMBER_OF_WORKERS; ++i)
		workers.push_back(std::thread(&misc::TaskQueue::run, &tq, true));
	sleep(10);
	std::cout << "Stopping task queues" << std::endl;
	tq.shutdown();
	for (auto i = workers.begin(); i != workers.end(); ++i)
		i->join();
	std::cout << "Stopping pushing thread" << std::endl;
	keepGoing = false;
	push.join();
	for (auto i = numbers.begin(); i != numbers.end(); ++i)
		delete *i;
	return EXIT_SUCCESS;
}
