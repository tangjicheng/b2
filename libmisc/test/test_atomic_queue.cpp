#include <thread>

#include <gtest/gtest.h>

#include <misc/atomic_queue.h>
#include <misc/form.h>

namespace {

struct Element {
	std::size_t a;
	double b;
};

constexpr std::uint64_t container_size = 1000;
constexpr std::uint64_t max_size = 1000;

class Dummy_reactor_command {
public:
	using Command = std::function<void()>;
	using Single_call = std::function<void()>;

	void call_later(const Single_call& call) { add_command(Command {call}); }

	void main_loop()
	{
		execute_commands();
		internal_func();
	}

	void inc() { processed_external_.fetch_add(1); }

	std::uint64_t processed_internal() { return processed_internal_.load(); }
	std::uint64_t processed_external() { return processed_external_.load(); }

private:
	misc::Atomic_queue<Command, 3u> commands_queue_ {1};
	std::atomic<std::uint64_t> processed_internal_ {0u};
	std::atomic<std::uint64_t> processed_external_ {0u};

	void add_command(Command&& cmd) { commands_queue_.push(std::move(cmd)); }

	void execute_commands()
	{
		auto limit = 10u;
		while (limit > 0u) {
			auto command = commands_queue_.peek();
			if (command == nullptr)
				return;
			(*command)();
			--limit;
			commands_queue_.advance();
		}
	}

	void internal_func() { internal_call_later(); }

	void internal_call_later()
	{
		add_command([this]() { ++processed_internal_; });
	}
};

template <size_t bucket_capacity, size_t buckets_num, bool consume_all>
class Dummy_reactor_int {
public:
	void call_later(int value) { add_command(value); }

	void main_loop()
	{
		if (started_) {
			execute_commands();
			internal_func();
		}
	}

	void start() { started_.store(true, std::memory_order_relaxed); }
	void stop() { started_.store(false, std::memory_order_relaxed); }

	std::uint64_t processed_internal() { return processed_internal_; }
	std::uint64_t processed_external() { return processed_external_; }

private:
	misc::Atomic_queue<int, bucket_capacity> commands_queue_ {buckets_num};
	std::atomic<bool> started_ {false};
	std::uint64_t processed_internal_ {0u};
	std::uint64_t processed_external_ {0u};

	void add_command(int value) { commands_queue_.push(value); }

	void execute_commands()
	{
		if constexpr (consume_all) {
			const auto limit = 10u;
			const auto consume_cb = [this](const int* value) {
				if (*value > 0)
					++processed_external_;
			};
			commands_queue_.consume_all(consume_cb, limit);
		}
		else {
			auto limit = 10u;
			while (limit > 0u) {
				auto value = commands_queue_.peek();
				if (value == nullptr)
					return;
				if (*value > 0) {
					++processed_external_;
				}
				--limit;
				commands_queue_.advance();
			}
		}
	}

	void internal_func() { internal_call_later(); }

	void internal_call_later()
	{
		++processed_internal_;
		add_command(0);
	}
};

} // namespace

TEST(Test_atomic_queue, optional_address_is_the_same)
{
	std::optional<std::string> opt {std::nullopt};
	const auto nullopt_address = &*opt;
	opt = std::string {"value"};
	const auto value_address = &*opt;
	ASSERT_EQ(nullopt_address, value_address);
}

TEST(Test_atomic_queue, bucket_empty)
{
	auto queue = misc::Atomic_queue_bucket<Element, container_size> {};
	const auto value = queue.peek();
	ASSERT_EQ(value, nullptr);
}

TEST(Test_atomic_queue, bucket_push_pop)
{
	auto queue = misc::Atomic_queue_bucket<Element, container_size> {};
	auto e = Element {0, 0.5};
	queue.push(std::move(e));
	const auto value = queue.peek();
	queue.advance();
	ASSERT_NE(value, nullptr);
	ASSERT_EQ(0, value->a);
	ASSERT_EQ(0.5, value->b);
}

TEST(Test_atomic_queue, bucket_single_thread)
{
	auto queue = misc::Atomic_queue_bucket<Element, container_size> {};
	const auto half_size = container_size / 2;

	for (auto i = 0u; i < half_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
		const auto value = queue.peek();
		queue.advance();
		ASSERT_NE(value, nullptr);
		ASSERT_EQ(i, value->a);
		ASSERT_EQ(i + 0.5, value->b);
	}
	auto empty_value = queue.peek();
	ASSERT_EQ(empty_value, nullptr);
	for (auto i = half_size; i < container_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
	}
	for (auto i = half_size; i < container_size; ++i) {
		const auto value = queue.peek();
		queue.advance();
		ASSERT_NE(value, nullptr);
		ASSERT_EQ(i, value->a);
		ASSERT_EQ(i + 0.5, value->b);
	}
	empty_value = queue.peek();
	ASSERT_EQ(empty_value, nullptr);
}

TEST(Test_atomic_queue, bucket_multi_thread)
{
	auto queue = misc::Atomic_queue_bucket<Element, container_size> {};
	const auto values_to_write = container_size / 4;

	auto producer = [&queue, values_to_write]() {
		for (auto i = 0u; i < values_to_write; ++i) {
			auto e = Element {i, i + 0.5};
			queue.push(std::move(e));
		}
	};

	auto consumer = [&queue]() {
		auto values_read = 0u;
		while (values_read != container_size) {
			const auto value = queue.peek();
			if (value != nullptr) {
				++values_read;
				queue.advance();
			}
		}
	};

	std::thread t1 {producer}, t2 {producer}, t3 {producer}, t4 {producer},
		t5 {consumer};

	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
}

TEST(Test_atomic_queue, pool_empty)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	const auto value = queue.peek();
	ASSERT_EQ(value, nullptr);
}

TEST(Test_atomic_queue, pool_push_pop)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	auto e = Element {0, 0.5};
	queue.push(std::move(e));
	const auto value = queue.peek();
	queue.advance();
	ASSERT_NE(value, nullptr);
	ASSERT_EQ(0, value->a);
	ASSERT_EQ(0.5, value->b);
}

TEST(Test_atomic_queue, pool_consume)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	auto count = size_t {0u};
	const auto consume_cb = [&count](Element* e) {
		ASSERT_NE(e, nullptr);
		ASSERT_EQ(count, e->a);
		ASSERT_EQ(count + 0.5, e->b);
		++count;
	};
	queue.consume_all(consume_cb, 0);
	ASSERT_EQ(0, count);

	auto i = size_t {0u};
	{
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
	}
	++i;
	queue.consume_all(consume_cb, 0);
	ASSERT_EQ(1, count);

	for (; i < container_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
	}
	queue.consume_all(consume_cb, 0);
	ASSERT_EQ(container_size, count);

	count = 0u;
	for (i = 0; i < 4 * container_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
	}
	queue.consume_all(consume_cb, 10);
	ASSERT_EQ(10, count);
	queue.consume_all(consume_cb, 0);
	ASSERT_EQ(4 * container_size, count);
}

TEST(Test_atomic_queue, pool_single_thread)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	const auto half_size = container_size / 2;

	for (auto i = 0u; i < half_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
		const auto value = queue.peek();
		queue.advance();
		ASSERT_NE(value, nullptr);
		ASSERT_EQ(i, value->a);
		ASSERT_EQ(i + 0.5, value->b);
	}
	auto empty_value = queue.peek();
	ASSERT_EQ(empty_value, nullptr);
	for (auto i = half_size; i < container_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
	}
	for (auto i = half_size; i < container_size; ++i) {
		const auto value = queue.peek();
		queue.advance();
		ASSERT_NE(value, nullptr);
		ASSERT_EQ(i, value->a);
		ASSERT_EQ(i + 0.5, value->b);
	}
	empty_value = queue.peek();
	ASSERT_EQ(empty_value, nullptr);
}

TEST(Test_atomic_queue, pool_multi_thread)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	const auto values_to_write = container_size / 4;

	auto producer = [&queue, values_to_write]() {
		for (auto i = 0u; i < values_to_write; ++i) {
			auto e = Element {i, i + 0.5};
			queue.push(std::move(e));
		}
	};

	auto consumer = [&queue]() {
		auto values_read = 0u;
		while (values_read != container_size) {
			const auto value = queue.peek();
			if (value != nullptr) {
				++values_read;
				queue.advance();
			}
		}
	};

	std::thread t1 {producer}, t2 {producer}, t3 {producer}, t4 {producer},
		t5 {consumer};

	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
}

TEST(Test_atomic_queue, pool_single_thread_double_bucket)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};

	for (auto i = 0u; i < 2 * container_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
		const auto value = queue.peek();
		queue.advance();
		ASSERT_NE(value, nullptr);
		ASSERT_EQ(i, value->a);
		ASSERT_EQ(i + 0.5, value->b);
	}
	auto empty_value = queue.peek();
	ASSERT_EQ(empty_value, nullptr);
	for (auto i = 0u; i < container_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
	}
	for (auto i = 0u; i < container_size; ++i) {
		const auto value = queue.peek();
		queue.advance();
		ASSERT_NE(value, nullptr);
		ASSERT_EQ(i, value->a);
		ASSERT_EQ(i + 0.5, value->b);
	}
	empty_value = queue.peek();
	ASSERT_EQ(empty_value, nullptr);
	for (auto i = 0u; i < container_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
	}
	for (auto i = 0u; i < container_size; ++i) {
		const auto value = queue.peek();
		queue.advance();
		ASSERT_NE(value, nullptr);
		ASSERT_EQ(i, value->a);
		ASSERT_EQ(i + 0.5, value->b);
	}
	empty_value = queue.peek();
	ASSERT_EQ(empty_value, nullptr);
}

TEST(Test_atomic_queue, pool_single_thread_multi_bucket)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};

	for (auto i = 0u; i < 100 * container_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
		const auto value = queue.peek();
		queue.advance();
		ASSERT_NE(value, nullptr);
		ASSERT_EQ(i, value->a);
		ASSERT_EQ(i + 0.5, value->b);
	}
	auto empty_value = queue.peek();
	ASSERT_EQ(empty_value, nullptr);

	for (auto i = 0u; i < 100 * container_size; ++i) {
		auto e = Element {i, i + 0.5};
		queue.push(std::move(e));
	}
	for (auto i = 0u; i < 100 * container_size; ++i) {
		const auto value = queue.peek();
		queue.advance();
		ASSERT_NE(value, nullptr);
		ASSERT_EQ(i, value->a);
		ASSERT_EQ(i + 0.5, value->b);
	}
	empty_value = queue.peek();
	ASSERT_EQ(empty_value, nullptr);
}

TEST(Test_atomic_queue, pool_multi_thread_multi_bucket_push)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	const auto values_to_write = 1024 * container_size;

	auto producer = [&queue, values_to_write]() {
		for (auto i = 0u; i < values_to_write; ++i) {
			auto e = Element {i, i + 0.5};
			queue.push(std::move(e));
		}
	};

	std::thread t1 {producer}, t2 {producer}, t3 {producer}, t4 {producer};

	t1.join();
	t2.join();
	t3.join();
	t4.join();
}

TEST(Test_atomic_queue, pool_multi_thread_multi_bucket_push_wait_pop)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	const auto values_to_write = 4 * container_size;

	auto producer = [&queue, values_to_write]() {
		for (auto i = 0u; i < values_to_write; ++i) {
			auto e = Element {i, i + 0.5};
			queue.push(std::move(e));
		}
	};

	auto consumer = [&queue]() {
		auto values_read = 0u;
		while (values_read != 16 * container_size) {
			const auto value = queue.peek();
			if (value != nullptr) {
				++values_read;
				queue.advance();
			}
		}
	};

	std::thread t1 {producer}, t2 {producer}, t3 {producer}, t4 {producer};

	t1.join();
	t2.join();
	t3.join();
	t4.join();

	std::thread t5 {consumer};
	t5.join();
}

TEST(Test_atomic_queue, pool_multi_thread_multi_bucket_push_pop)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	const auto values_to_write = 4 * container_size;

	auto producer = [&queue, values_to_write]() {
		for (auto i = 0u; i < values_to_write; ++i) {
			auto e = Element {i, i + 0.5};
			queue.push(std::move(e));
		}
	};

	auto consumer = [&queue]() {
		auto values_read = 0u;
		while (values_read != 16 * container_size) {
			const auto value = queue.peek();
			if (value != nullptr) {
				++values_read;
				queue.advance();
			}
		}
	};

	std::thread t1 {producer}, t2 {producer}, t3 {producer}, t4 {producer},
		t5 {consumer};

	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
}

TEST(Test_atomic_queue, pool_multi_thread_multi_bucket_pop_push)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	const auto values_to_write = 4 * container_size;

	auto producer = [&queue, values_to_write]() {
		for (auto i = 0u; i < values_to_write; ++i) {
			auto e = Element {i, i + 0.5};
			queue.push(std::move(e));
		}
	};

	auto consumer = [&queue]() {
		auto values_read = 0u;
		while (values_read != 16 * container_size) {
			const auto value = queue.peek();
			if (value != nullptr) {
				++values_read;
				queue.advance();
			}
		}
	};

	std::thread t1 {consumer};
	std::this_thread::sleep_for(std::chrono::seconds {1});
	std::thread t2 {producer}, t3 {producer}, t4 {producer}, t5 {producer};

	t2.join();
	t3.join();
	t4.join();
	t5.join();
	t1.join();
}

TEST(Test_atomic_queue, pool_multi_thread_multi_bucket_shared_consumer)
{
	auto queue = misc::Atomic_queue<Element, container_size> {3};
	const auto values_to_write = 4 * container_size;

	auto producer = [&queue, values_to_write]() {
		for (auto i = 0u; i < values_to_write; ++i) {
			auto e = Element {i, i + 0.5};
			queue.push(std::move(e));
		}
	};

	auto consumer = [&queue]() {
		auto values_read = 0u;
		auto values_written = 0u;
		while (values_read != 20 * container_size) {
			const auto value = queue.peek();
			if (value != nullptr) {
				++values_read;
				queue.advance();
			}
			if (values_written < values_to_write) {
				auto e = Element {values_written, values_written + 0.5};
				queue.push(std::move(e));
			}
		}
	};

	std::thread t1 {producer}, t2 {producer}, t3 {producer}, t4 {producer},
		t5 {consumer};

	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
}

TEST(Test_atomic_queue, reactor)
{
	auto reactor = Dummy_reactor_command {};
	constexpr auto count = 100u;

	auto reactor_t = [&reactor]() {
		while (reactor.processed_external() < count) {
			reactor.main_loop();
			std::this_thread::yield();
		}
	};

	auto app_t = [&reactor]() {
		for (auto i = 0u; i < count; ++i) {
			reactor.call_later([&reactor]() { reactor.inc(); });
			std::this_thread::yield();
		}
		std::this_thread::yield();
	};

	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	t1.join();
	t2.join();
	ASSERT_EQ(reactor.processed_external(), count);
}

TEST(Test_atomic_queue, reactor_int)
{
	auto reactor = Dummy_reactor_int<3, 1, false> {};
	constexpr auto count = 100u;
	reactor.start();

	auto reactor_t = [&reactor]() {
		while (reactor.processed_external() < count) {
			reactor.main_loop();
			std::this_thread::yield();
		}
	};

	auto app_t = [&reactor]() {
		for (auto i = 1u; i <= count; ++i) {
			reactor.call_later(i);
			std::this_thread::yield();
		}
		std::this_thread::yield();
	};

	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	t1.join();
	t2.join();
	ASSERT_EQ(reactor.processed_external(), count);
}

TEST(Test_atomic_queue, reactor_int_2)
{
	auto reactor = Dummy_reactor_int<1024, 3, false> {};
	constexpr auto count = 100'000u;
	reactor.start();

	auto reactor_t = [&reactor]() {
		while (reactor.processed_external() < count) {
			reactor.main_loop();
			std::this_thread::yield();
		}
	};

	auto app_t = [&reactor]() {
		for (auto i = 1u; i <= count; ++i) {
			reactor.call_later(i);
			std::this_thread::yield();
		}
		std::this_thread::yield();
	};

	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	t1.join();
	t2.join();
	ASSERT_EQ(reactor.processed_external(), count);
}

TEST(Test_atomic_queue, reactor_int_consume_all)
{
	auto reactor = Dummy_reactor_int<1024, 3, true> {};
	constexpr auto count = 100'000u;
	reactor.start();

	auto reactor_t = [&reactor]() {
		while (reactor.processed_external() < count) {
			reactor.main_loop();
			std::this_thread::yield();
		}
	};

	auto app_t = [&reactor]() {
		for (auto i = 1u; i <= count; ++i) {
			reactor.call_later(i);
			std::this_thread::yield();
		}
		std::this_thread::yield();
	};

	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	t1.join();
	t2.join();
	ASSERT_EQ(reactor.processed_external(), count);
}
