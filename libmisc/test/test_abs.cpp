#include <cstdint>
#include <limits>

#include <gtest/gtest.h>

#include <misc/abs.h>

TEST(abs, unsigned_integer)
{
	EXPECT_EQ(uint32_t {0}, misc::abs(uint32_t {0}));
	EXPECT_EQ(uint32_t {1}, misc::abs(uint32_t {1}));
	EXPECT_EQ(std::numeric_limits<uint32_t>::max(),
		misc::abs(std::numeric_limits<uint32_t>::max()));

	EXPECT_EQ(uint64_t {0}, misc::abs(uint64_t {0}));
	EXPECT_EQ(uint64_t {1}, misc::abs(uint64_t {1}));
	EXPECT_EQ(std::numeric_limits<uint64_t>::max(),
		misc::abs(std::numeric_limits<uint64_t>::max()));
}

TEST(abs, signed_integer)
{
	EXPECT_EQ(std::numeric_limits<int32_t>::max(),
		misc::abs(std::numeric_limits<int32_t>::min() + 1));
	EXPECT_EQ(int32_t {1}, misc::abs(int32_t {-1}));
	EXPECT_EQ(int32_t {0}, misc::abs(int32_t {0}));
	EXPECT_EQ(int32_t {1}, misc::abs(int32_t {1}));
	EXPECT_EQ(std::numeric_limits<int32_t>::max(),
		misc::abs(std::numeric_limits<int32_t>::max()));

	EXPECT_EQ(std::numeric_limits<int64_t>::max(),
		misc::abs(std::numeric_limits<int64_t>::min() + 1));
	EXPECT_EQ(int64_t {1}, misc::abs(int64_t {-1}));
	EXPECT_EQ(int64_t {0}, misc::abs(int64_t {0}));
	EXPECT_EQ(int64_t {1}, misc::abs(int64_t {1}));
	EXPECT_EQ(std::numeric_limits<int64_t>::max(),
		misc::abs(std::numeric_limits<int64_t>::max()));
}

TEST(abs, floating_point)
{
	EXPECT_EQ(std::numeric_limits<float>::max(),
		misc::abs(std::numeric_limits<float>::lowest()));
	EXPECT_EQ(1.1f, misc::abs(-1.1f));
	EXPECT_EQ(0.0f, misc::abs(0.0f));
	EXPECT_EQ(1.1f, misc::abs(1.1f));
	EXPECT_EQ(std::numeric_limits<float>::max(),
		misc::abs(std::numeric_limits<float>::max()));

	EXPECT_EQ(std::numeric_limits<double>::max(),
		misc::abs(std::numeric_limits<double>::lowest()));
	EXPECT_EQ(1.1, misc::abs(-1.1));
	EXPECT_EQ(0.0, misc::abs(0.0));
	EXPECT_EQ(1.1, misc::abs(1.1));
	EXPECT_EQ(std::numeric_limits<double>::max(),
		misc::abs(std::numeric_limits<double>::max()));
}
