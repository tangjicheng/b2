#include <memory>
#include <optional>
#include <string>
#include <thread>
#include <utility>

#include <gtest/gtest.h>

#include <misc/LockedQueue.h>

#include "unittest.h"

TEST(TestLockedQueue, constructor)
{
	misc::LockedQueue<std::string> q;

	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

TEST(TestLockedQueue, push)
{
	misc::LockedQueue<std::string> q;

	std::string s1("one");
	ASSERT_NO_THROW(q.push(s1));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.push(std::string("two")));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	std::string s3("three");
	ASSERT_NO_THROW(q.push(std::move(s3)));
	EXPECT_EQ(3u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("one", q.pop());
	EXPECT_EQ("two", q.pop());
	EXPECT_EQ("three", q.pop());
}

TEST(TestLockedQueue, emplace)
{
	misc::LockedQueue<std::string> q;

	ASSERT_NO_THROW(q.emplace("one"));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace(3, '2'));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace("threeTHREE", 5));
	EXPECT_EQ(3u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace(std::string("FOURfourFOUR"), 4, 4));
	EXPECT_EQ(4u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace(std::string("five")));
	EXPECT_EQ(5u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("one", q.pop());
	EXPECT_EQ("222", q.pop());
	EXPECT_EQ("three", q.pop());
	EXPECT_EQ("four", q.pop());
	EXPECT_EQ("five", q.pop());
}

TEST(TestLockedQueue, pop)
{
	misc::LockedQueue<std::string> q;

	EXPECT_EQ(std::nullopt, q.pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	ASSERT_NO_THROW(q.push("one"));
	ASSERT_NO_THROW(q.push("two"));
	ASSERT_NO_THROW(q.push("three"));

	EXPECT_EQ(3u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("one", q.pop());
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("two", q.pop());
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ("three", q.pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	EXPECT_EQ(std::nullopt, q.pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}

namespace {

void push(misc::LockedQueue<std::string>& q, size_t count)
{
	for (size_t i = 0; i != count; ++i) {
		ASSERT_NO_THROW(q.push(std::to_string(i)));
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

void pop(misc::LockedQueue<std::string>& q, size_t count)
{
	for (size_t i = 0; i != count;) {
		auto value = q.pop();
		if (!value.has_value())
			continue;
		EXPECT_EQ(std::to_string(i), value);
		++i;
	}
	EXPECT_EQ(std::nullopt, q.pop());
}

} // namespace

TEST(TestLockedQueue, multiThreaded)
{
	misc::LockedQueue<std::string> q;

	std::thread pushThread([&] { push(q, 1000); });
	std::thread popThread([&] { pop(q, 1000); });

	pushThread.join();
	popThread.join();
}

TEST(TestLockedQueue, move_only)
{
	misc::LockedQueue<std::unique_ptr<int>> q;

	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	ASSERT_NO_THROW(q.push(std::make_unique<int>(1)));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	auto i2 = std::make_unique<int>(2);
	ASSERT_NO_THROW(q.push(std::move(i2)));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ(1, **q.pop());
	EXPECT_EQ(2, **q.pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	ASSERT_NO_THROW(q.emplace(new int(3)));
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	ASSERT_NO_THROW(q.emplace(std::make_unique<int>(4)));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	EXPECT_EQ(3, **q.pop());
	EXPECT_EQ(4, **q.pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	auto p5 = new int(5);
	auto p6 = new int(6);
	ASSERT_NO_THROW(q.emplace(p5));
	ASSERT_NO_THROW(q.emplace(p6));
	EXPECT_EQ(2u, q.size());
	EXPECT_FALSE(q.empty());

	auto i5 = q.pop();
	EXPECT_EQ(5, **i5);
	EXPECT_EQ(p5, i5->get());
	EXPECT_EQ(1u, q.size());
	EXPECT_FALSE(q.empty());

	auto i6 = q.pop();
	EXPECT_EQ(6, **i6);
	EXPECT_EQ(p6, i6->get());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());

	EXPECT_EQ(std::nullopt, q.pop());
	EXPECT_EQ(0u, q.size());
	EXPECT_TRUE(q.empty());
}
