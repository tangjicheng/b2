#include <array>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include <gtest/gtest.h>

#include <misc/FlagSet.h>
#include <misc/Record.h>
#include <misc/dump.h>
#include <misc/external_reference.h>
#include <misc/numeric_type.h>
#include <misc/object_id.h>
#include <misc/pow.h>

#include "unittest.h"

TEST(Record, character)
{
	misc::Record<char, char> r;
	constexpr size_t expectedSize = 2;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	const char a = 'a';
#define b 'b'
	const char expectedData[] = "ab";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	char A = '\0';
	char B = '\0';
	ASSERT_EQ(expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
#undef b
}

TEST(Record, unsignedInteger)
{
	misc::Record<uint8_t, uint16_t, uint32_t, uint64_t> r;
	constexpr size_t expectedSize = 15;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	const uint8_t a = 129U;
	const uint16_t b = 50049U;
	const uint32_t c = 3886400385U;
	const uint64_t d = 17777065148941714305UL;
	const char expectedData[]
		= "\x81\xC3\x81\xE7\xA5\xC3\x81\xF6\xB4\xD2\x90\xE7\xA5\xC3\x81";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	uint8_t A = 0;
	uint16_t B = 0;
	uint32_t C = 0;
	uint64_t D = 0;
	ASSERT_EQ(
		expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B, C, D));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
}

TEST(Record, signedInteger)
{
	misc::Record<int8_t, int16_t, int32_t, int64_t> r;
	constexpr size_t expectedSize = 15;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	const int8_t a = -127;
	const int16_t b = -15487;
	const int32_t c = -408566911;
	const int64_t d = -669678924767837311L;
	const char expectedData[]
		= "\x81\xC3\x81\xE7\xA5\xC3\x81\xF6\xB4\xD2\x90\xE7\xA5\xC3\x81";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	int8_t A = 0;
	int16_t B = 0;
	int32_t C = 0;
	int64_t D = 0;
	ASSERT_EQ(
		expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B, C, D));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
}

TEST(Record, Number)
{
	misc::Record<misc::Number<5>, misc::Number<5>, misc::Number<5>,
		misc::Number<5>, misc::Number<5, '-'>>
		r;
	constexpr size_t expectedSize = 25;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	const uint64_t a = 0;
#define b 1u
#define c 234u
#define d 56789u
	const uint64_t e = 5;
	const char expectedData[] = "    0    1  23456789----5";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d, e));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	uint64_t A = 1;
	uint64_t B = 0;
	uint64_t C = 0;
	uint64_t D = 0;
	uint64_t E = 0;
	ASSERT_EQ(expectedData + expectedSize,
		r.unpackUnsafe(expectedData, A, B, C, D, E));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
	ASSERT_EQ(e, E);
#undef b
#undef c
#undef d
}

TEST(Record, Number_throw)
{
	{
		misc::Record<misc::Number<4>> r;
		constexpr size_t expectedSize = 4;
		const uint64_t a = 10000u;
		char data[expectedSize] = {};
		ASSERT_THROW_WHAT(
			r.packUnsafe(data, a), std::invalid_argument, "Out of range");
	}
	{
		misc::Record<misc::Number<20>> r;
		const char data[] = "18446744073709551616"; // ULONG_MAX + 1
		uint64_t a = 0;
		ASSERT_THROW_WHAT(
			r.unpackUnsafe(data, a), std::invalid_argument, "Out of range");
	}
	{
		misc::Record<misc::Number<5>> r;
		uint64_t a = 0;
		ASSERT_THROW_WHAT(
			r.unpackUnsafe("abcde", a), std::invalid_argument, "Not a digit");
		ASSERT_THROW_WHAT(
			r.unpackUnsafe("+5000", a), std::invalid_argument, "Not a digit");
		ASSERT_THROW_WHAT(
			r.unpackUnsafe("-5000", a), std::invalid_argument, "Not a digit");
	}
}

TEST(Record, FixedString)
{
	misc::Record<misc::FixedString<10>, misc::FixedString<10>,
		misc::FixedString<5>, misc::FixedString<5>, misc::FixedString<5, '-'>>
		r;
	constexpr size_t expectedSize = 35;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	const std::string a = " abc";
#define b std::string("ABCDEVWXYZ")
	const std::string c;
	const std::string d = "a z";
	const std::string e = "A Z";
	const char expectedData[] = " abc      ABCDEVWXYZ     a z  A Z--";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d, e));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	std::string A;
	std::string B;
	std::string C;
	std::string D;
	std::string E;
	ASSERT_EQ(expectedData + expectedSize,
		r.unpackUnsafe(expectedData, A, B, C, D, E));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
	ASSERT_EQ(e, E);
#undef b
}

TEST(Record, FixedString_throw)
{
	{
		misc::Record<misc::FixedString<5>> r;
		constexpr size_t expectedSize = 5;
		const std::string a = "123456";
		char data[expectedSize] = {};
		ASSERT_THROW_WHAT(
			r.packUnsafe(data, a), std::invalid_argument, "Too long");
	}
}

TEST(Record, FixedStringLeftPadded)
{
	misc::Record<misc::FixedStringLeftPadded<10>,
		misc::FixedStringLeftPadded<10>, misc::FixedStringLeftPadded<5>,
		misc::FixedStringLeftPadded<5>, misc::FixedStringLeftPadded<5, '-'>>
		r;
	constexpr size_t expectedSize = 35;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	const std::string a = "abc ";
#define b std::string("ABCDEVWXYZ")
	const std::string c;
	const std::string d = "a z";
	const std::string e = "A Z";
	const char expectedData[] = "      abc ABCDEVWXYZ       a z--A Z";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d, e));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	std::string A;
	std::string B;
	std::string C;
	std::string D;
	std::string E;
	ASSERT_EQ(expectedData + expectedSize,
		r.unpackUnsafe(expectedData, A, B, C, D, E));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
	ASSERT_EQ(e, E);
#undef b
}

TEST(Record, FixedStringLeftPadded_throw)
{
	{
		misc::Record<misc::FixedStringLeftPadded<5>> r;
		constexpr size_t expectedSize = 5;
		const std::string a = "123456";
		char data[expectedSize] = {};
		ASSERT_THROW_WHAT(
			r.packUnsafe(data, a), std::invalid_argument, "Too long");
	}
}

MISC_EXTERNAL_REFERENCE_DECL(ExternalRef, 5)

TEST(Record, FixedExtRef)
{
	misc::Record<misc::FixedExtRef<ExternalRef>, misc::FixedExtRef<ExternalRef>,
		misc::FixedExtRef<ExternalRef>, misc::FixedExtRef<ExternalRef>,
		misc::FixedExtRef<ExternalRef, '-'>>
		r;
	constexpr size_t expectedSize = 25;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	const ExternalRef a(" abc");
#define b ExternalRef("ABCDE")
	const ExternalRef c;
	const ExternalRef d("a z");
	const ExternalRef e("A Z");
	const char expectedData[] = " abc ABCDE     a z  A Z--";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d, e));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	ExternalRef A;
	ExternalRef B;
	ExternalRef C;
	ExternalRef D;
	ExternalRef E;
	ASSERT_EQ(expectedData + expectedSize,
		r.unpackUnsafe(expectedData, A, B, C, D, E));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
	ASSERT_EQ(e, E);
#undef b
}

TEST(Record, arrays)
{
	misc::Record<misc::FixedCArray<3>, misc::FixedArray<3>, misc::FixedArray<3>>
		r;
	constexpr size_t expectedSize = 9;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	const char a[3] = {'a', 'b', 'c'};
	const std::array<uint8_t, 3> b {{'d', 'e', 'f'}};
#define c                  \
	std::array<uint8_t, 3> \
	{                      \
		{                  \
			'X', 'Y', 'Z'  \
		}                  \
	}
	const char expectedData[] = "abcdefXYZ";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	char A[3] = {0};
	std::array<uint8_t, 3> B {{' ', ' ', ' '}};
	std::array<uint8_t, 3> C {{' ', ' ', ' '}};
	ASSERT_EQ(
		expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B, C));
	ASSERT_EQ(0, memcmp(a, A, sizeof(A)));
	ASSERT_EQ(b, B);
	ASSERT_EQ((c), C);
#undef c
}

TEST(Record, blob)
{
	misc::Record<misc::FixedBlob<3>, misc::VarBlob<10>, misc::FixedBlob<3>> r;

	const char buffer[9] = {'A', 'B', 'C', 'D', 'E', 'F', 'x', 'y', 'z'};
	const void* a = buffer + 3;
	const void* b = buffer;
	size_t len = 3;
	const void* c = buffer + 6;
	constexpr size_t expectedSize = 10;
	const char expectedData[] = "DEF\x3xyzABC";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	ASSERT_EQ(expectedSize, r.size(misc::group(b, len)));

	char data[expectedSize] = {};
	ASSERT_EQ(
		data + expectedSize, r.packUnsafe(data, a, misc::group(b, len), c));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	char A[3] = {0};
	char B[10] = {0};
	size_t LEN = 0;
	char C[3] = {0};
	ASSERT_EQ(expectedData + expectedSize,
		r.unpackUnsafe(expectedData, A, misc::group(B, LEN), C));
	ASSERT_EQ(0, memcmp(a, A, 3));
	ASSERT_EQ(0, memcmp(b, B, LEN));
	ASSERT_EQ(0, memcmp(c, C, 3));
}

TEST(Record, blob_throw)
{
	{
		misc::Record<misc::VarBlob<5>> r;
		constexpr size_t expectedSize = 5;
		const char buffer[6] = {'1', '2', '3', '4', '5', '6'};
		const void* a = buffer;
		size_t len = 6;
		char data[expectedSize] = {};
		ASSERT_THROW_WHAT(r.packUnsafe(data, misc::group(a, len)),
			std::invalid_argument, "Too long");
	}
	{
		misc::Record<misc::VarBlob<5>> r;
		const char data[] = "\x6"
							"123456";
		char a[5] = {0};
		size_t len = 0;
		ASSERT_THROW_WHAT(r.unpackUnsafe(data, misc::group(a, len)),
			std::invalid_argument, "Field too long");
	}
}

TEST(Record, std_string)
{
	misc::Record<misc::VarString<16>, misc::VarString<16>, misc::VarString<16>,
		misc::VarString<16>>
		r;

	const std::string a = "hello";
	const std::string b;
#define c std::string("worldHELLOWORLD")
#define d std::string()
	constexpr size_t expectedSize = 24;
	const char expectedData[] = "\x5\x0\xf\x0helloworldHELLOWORLD";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	ASSERT_EQ(expectedSize, r.size(a, b, c, d));

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	std::string A;
	std::string B;
	std::string C;
	std::string D;
	ASSERT_EQ(
		expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B, C, D));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
#undef c
#undef d
}

TEST(Record, std_string_throw)
{
	{
		misc::Record<misc::VarString<5>> r;
		constexpr size_t expectedSize = 5;
		const std::string a = "123456";
		char data[expectedSize] = {};
		ASSERT_THROW_WHAT(
			r.packUnsafe(data, a), std::invalid_argument, "Too long");
	}
	{
		misc::Record<misc::VarString<5>> r;
		char data[] = "\x6"
					  "123456";
		std::string a;
		ASSERT_THROW_WHAT(
			r.unpackUnsafe(data, a), std::invalid_argument, "Field too long");
	}
}

TEST(Record, std_vector)
{
	misc::Record<misc::VarVector<16>, misc::VarVector<16>, misc::VarVector<16>,
		misc::VarVector<16>>
		r;

	std::string h = "HELLO";
	std::string w = "WORLDhelloworld";

	std::vector<uint8_t> a(h.begin(), h.end());
	const std::vector<uint8_t> b;
#define c std::vector<uint8_t>(w.begin(), w.end())
#define d std::vector<uint8_t>()
	constexpr size_t expectedSize = 24;
	const char expectedData[] = "\x5\x0\xf\x0HELLOWORLDhelloworld";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	auto size = r.size(a, b, c, d);
	ASSERT_EQ(expectedSize, size);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	std::vector<uint8_t> A;
	std::vector<uint8_t> B;
	std::vector<uint8_t> C;
	std::vector<uint8_t> D;
	ASSERT_EQ(
		expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B, C, D));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
#undef c
#undef d
}

TEST(Record, std_vector_throw)
{
	{
		misc::Record<misc::VarVector<5>> r;
		constexpr size_t expectedSize = 5;
		const std::string buffer = "123456";
		const std::vector<uint8_t> a(buffer.begin(), buffer.end());
		char data[expectedSize] = {};
		ASSERT_THROW_WHAT(
			r.packUnsafe(data, a), std::invalid_argument, "Too long");
	}
	{
		misc::Record<misc::VarVector<5>> r;
		char data[] = "\x6"
					  "123456";
		std::vector<uint8_t> a;
		ASSERT_THROW_WHAT(
			r.unpackUnsafe(data, a), std::invalid_argument, "Field too long");
	}
}

TEST(Record, multi)
{
	misc::Record<char, misc::VarString<16>, char, misc::VarString<16>, char> r1;
	misc::Record<uint16_t, misc::VarString<16>, uint16_t, misc::VarString<16>,
		uint16_t>
		r2;

	const char a = '<';
	const std::string b = "hello";
	const char c = '_';
	const std::string d = "world";
	const char e = '>';
	const uint16_t f = 1;
	const std::string g = "two";
	const uint16_t h = 3;
	const std::string i = "four";
	const uint16_t j = 5;
	constexpr size_t expectedSize1 = 15;
	constexpr size_t expectedSize2 = 15;
	constexpr size_t expectedSize = expectedSize1 + expectedSize2;
	const char expectedData[]
		= "<\x5_\x5>helloworld\x0\x1\x3\x0\x3\x4\x0\x5twofour";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	ASSERT_EQ(expectedSize1, r1.size(b, d));
	ASSERT_EQ(expectedSize2, r2.size(g, i));

	char data[expectedSize] = {};
	ASSERT_EQ(expectedSize1, r1.pack(data, expectedSize, a, b, c, d, e));
	ASSERT_EQ(expectedSize2,
		r2.pack(data + expectedSize1, expectedSize2, f, g, h, i, j));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	char A = '\0';
	std::string B;
	char C = '\0';
	std::string D;
	char E = '\0';
	uint16_t F = 0;
	std::string G;
	uint16_t H = 0;
	std::string I;
	uint16_t J = 0;
	ASSERT_EQ(
		expectedSize1, r1.unpack(expectedData, expectedSize, A, B, C, D, E));
	ASSERT_EQ(expectedSize2,
		r2.unpack(expectedData + expectedSize1, expectedSize2, F, G, H, I, J));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
	ASSERT_EQ(e, E);
	ASSERT_EQ(f, F);
	ASSERT_EQ(g, G);
	ASSERT_EQ(h, H);
	ASSERT_EQ(i, I);
	ASSERT_EQ(j, J);
}

namespace {

class Convertable {
public:
	friend bool operator==(const Convertable& lhs, const Convertable& rhs)
	{
		return lhs.m_value == rhs.m_value;
	}

	Convertable() = default;

	explicit Convertable(uint8_t value)
		: m_value(value)
	{
	}

	explicit operator uint8_t() const { return m_value; }

private:
	uint8_t m_value {0};
};

class Point {
public:
	friend bool operator==(const Point& lhs, const Point& rhs)
	{
		return misc::group(lhs.m_x, lhs.m_y) == misc::group(rhs.m_x, rhs.m_y);
	}

	Point() = default;

	Point(uint32_t x, uint64_t y)
		: m_x(x)
		, m_y(y)
	{
	}

	uint32_t x() const { return m_x; }
	uint64_t y() const { return m_y; }

private:
	uint8_t m_x {0};
	uint8_t m_y {0};
};

class PointResolver {
public:
	explicit PointResolver(size_t n = 10) { m_points.reserve(n); }

	Point& addPoint(uint8_t x, uint8_t y)
	{
		m_points.emplace_back(x, y);
		return m_points.back();
	}

	Point& pointFromIndex(size_t i) { return m_points.at(i); }

	size_t indexFromPoint(const Point& point) const
	{
		for (size_t i = 0; i < m_points.size(); ++i) {
			if (&point == &m_points[i])
				return i;
		}
		throw std::invalid_argument("Not found");
	}

private:
	std::vector<Point> m_points;
};

class ValuePoint {
public:
	friend bool operator==(const ValuePoint& lhs, const ValuePoint& rhs)
	{
		return misc::group(lhs.m_value, lhs.m_point)
			== misc::group(rhs.m_value, rhs.m_point);
	}

	ValuePoint() = default;
	ValuePoint(uint16_t value, Point& point)
		: m_value(value)
		, m_point(&point)
	{
	}

	uint16_t value() const { return m_value; }
	Point& point() const { return *m_point; }

private:
	uint16_t m_value {0};
	Point* m_point {nullptr};
};

} // namespace

namespace misc {

using PointRecord = Record<uint8_t, uint8_t>;
using PointIndexRecord = Record<uint8_t>;
using ValuePointRecord = Record<uint16_t, Point*>;

template <>
struct PackingTraits<Point> : FixedPackingTraits<PointRecord::size()> {
	class Packer : public FixedPacker {
	public:
		Packer(const Point& point)
			: m_point(point)
		{
		}

		void packFixed(void*& p) const
		{
			p = PointRecord::packUnsafe(p, m_point.x(), m_point.y());
		}

	private:
		const Point& m_point;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(Point& point)
			: m_point(point)
		{
		}

		void unpackFixed(const void*& p) const
		{
			uint8_t x;
			uint8_t y;
			p = PointRecord::unpackUnsafe(p, x, y);
			m_point = Point(x, y);
		}

	private:
		Point& m_point;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <>
struct PackingTraits<Point*> : FixedPackingTraits<PointIndexRecord::size()> {
	using PackParam = std::tuple<const Point&, const PointResolver&>;
	using UnpackParam = std::tuple<Point*&, PointResolver&>;

	class Packer : public FixedPacker {
	public:
		Packer(const PackParam& param)
			: m_point(std::get<0>(param))
			, m_resolver(std::get<1>(param))
		{
		}

		void packFixed(void*& p) const
		{
			p = PointIndexRecord::packUnsafe(
				p, m_resolver.indexFromPoint(m_point));
		}

	private:
		const Point& m_point;
		const PointResolver& m_resolver;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(const UnpackParam& p)
			: m_point(std::get<0>(p))
			, m_resolver(std::get<1>(p))
		{
		}

		void unpackFixed(const void*& p) const
		{
			uint8_t i;
			p = PointIndexRecord::unpackUnsafe(p, i);
			m_point = &m_resolver.pointFromIndex(i);
		}

	private:
		Point*& m_point;
		PointResolver& m_resolver;
	};
};

template <>
struct PackingTraits<ValuePoint>
	: FixedPackingTraits<ValuePointRecord::size()> {
	using PackParam = std::tuple<const ValuePoint&, const PointResolver&>;
	using UnpackParam = std::tuple<ValuePoint&, PointResolver&>;

	class Packer : public FixedPacker {
	public:
		Packer(const PackParam& param)
			: m_valuePoint(std::get<0>(param))
			, m_resolver(std::get<1>(param))
		{
		}

		void packFixed(void*& p) const
		{
			p = ValuePointRecord::packUnsafe(p, m_valuePoint.value(),
				misc::group(m_valuePoint.point(), m_resolver));
		}

	private:
		const ValuePoint& m_valuePoint;
		const PointResolver& m_resolver;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(const UnpackParam& param)
			: m_valuePoint(std::get<0>(param))
			, m_resolver(std::get<1>(param))
		{
		}

		void unpackFixed(const void*& p) const
		{
			uint16_t value;
			Point* point;
			p = ValuePointRecord::unpackUnsafe(
				p, value, misc::group(point, m_resolver));
			m_valuePoint = ValuePoint(value, *point);
		}

	private:
		ValuePoint& m_valuePoint;
		PointResolver& m_resolver;
	};
};

} // namespace misc

TEST(Record, Cast)
{
	misc::Record<misc::Cast<Convertable, uint8_t>,
		misc::Cast<uint16_t, uint8_t>, misc::Cast<uint32_t, uint8_t>,
		misc::Cast<uint64_t, uint8_t>>
		r;
	constexpr size_t expectedSize = 4;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	Convertable a(1);
	const uint16_t b = 2;
	const uint32_t c = 3;
	const uint64_t d = 4;
	const char expectedData[] = "\x1\x2\x3\x4";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	Convertable A;
	uint16_t B = 0;
	uint32_t C = 0;
	uint64_t D = 0;
	ASSERT_EQ(
		expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B, C, D));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
}

namespace {

enum class Flag { ENGLISH, PIRATE, RAINBOW };

MISC_FLAGSET_DECL(Flag, FlagBunch)

} // namespace

MISC_OBJECT_ID_DECL(O32, uint32_t)
MISC_OBJECT_ID_DECL(O64, uint64_t)
MISC_NUMERIC_TYPE_DECL(N320, uint32_t, 0)
MISC_NUMERIC_TYPE_DECL(N321, uint32_t, 1)
MISC_NUMERIC_TYPE_DECL(N640, uint64_t, 0)
MISC_NUMERIC_TYPE_DECL(N641, uint64_t, 1)

TEST(Record, Object_id)
{
	misc::Record<O32, O64> r;
	constexpr size_t expectedSize = 12;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	O32 a(1);
	O64 b(2);
	const char expectedData[] = "\x0\x0\x0\x1\x0\x0\x0\x0\x0\x0\x0\x2";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	O32 A(0);
	O64 B(0);
	ASSERT_EQ(expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
}

TEST(Record, Numeric_type)
{
	misc::Record<N320, N321, N640, N641> r;
	constexpr size_t expectedSize = 24;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	N320 a(1);
	N321 b(2);
	N640 c(3);
	N641 d(4);
	const char expectedData[] = "\x0\x0\x0\x1\x0\x0\x0\x2\x0\x0\x0\x0\x0\x0\x0"
								"\x3\x0\x0\x0\x0\x0\x0\x0\x4";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b, c, d));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	N320 A(0);
	N321 B(0);
	N640 C(0);
	N641 D(0);
	ASSERT_EQ(
		expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B, C, D));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(d, D);
}

TEST(Record, FlagSet)
{
	misc::Record<FlagBunch, FlagBunch> r;
	constexpr size_t expectedSize = 8;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	FlagBunch a(Flag::ENGLISH | Flag::RAINBOW);
	FlagBunch b(Flag::PIRATE);
	const char expectedData[] = "\x0\x0\x0\x5\x0\x0\x0\x2";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	FlagBunch A;
	FlagBunch B;
	ASSERT_EQ(expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
}

TEST(Record, Point)
{
	misc::Record<Point, Point*, ValuePoint> r;
	constexpr size_t expectedSize = 6;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	PointResolver resolver;
	const Point a(1, 2);
	const Point& b = resolver.addPoint(3, 4);
	const ValuePoint c(5, resolver.addPoint(6, 7));
	const char expectedData[] = "\x1\x2\x0\x0\x5\x1";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize,
		r.packUnsafe(
			data, a, misc::group(b, resolver), misc::group(c, resolver)));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	Point A;
	Point* B;
	ValuePoint C;
	ASSERT_EQ(expectedData + expectedSize,
		r.unpackUnsafe(expectedData, A, misc::group(B, resolver),
			misc::group(C, resolver)));
	ASSERT_EQ(a, A);
	ASSERT_EQ(&b, B);
	ASSERT_EQ(c, C);
}

MISC_NUMERIC_TYPE_DECL(Numeric, uint8_t, 1)

struct CustomNumericPacker;

namespace misc {

template <>
struct PackingTraits<CustomNumericPacker>
	: FixedPackingTraits<2 * sizeof(Numeric::Underlying_type)> {
	using Record
		= misc::Record<Numeric::Underlying_type, Numeric::Underlying_type>;

	class Packer : public FixedPacker {
	public:
		Packer(const Numeric& n)
			: m_n(n)
		{
		}

		void packFixed(void*& p) const
		{
			auto integral = m_n.value()
				/ misc::Pow_v<Numeric::Underlying_type, 10, m_n.decimals()>;
			auto fractional = m_n.value()
				% misc::Pow_v<Numeric::Underlying_type, 10, m_n.decimals()>;
			p = Record::packUnsafe(p, integral, fractional);
		}

	private:
		const Numeric& m_n;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(Numeric& n)
			: m_n(n)
		{
		}

		void unpackFixed(const void*& p) const
		{
			Numeric::Underlying_type integral;
			Numeric::Underlying_type fractional;
			p = Record::unpackUnsafe(p, integral, fractional);
			m_n = Numeric(integral
					* misc::Pow_v<Numeric::Underlying_type, 10,
						m_n.decimals()> + fractional);
		}

	private:
		Numeric& m_n;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

} // namespace misc

TEST(Record, CustomNumericPacker)
{
	misc::Record<CustomNumericPacker, Numeric> r;
	constexpr size_t expectedSize = 3;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);

	Numeric a(12);
	Numeric b(12);
	const char expectedData[] = "\x1\x2"
								"\xc";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize, r.packUnsafe(data, a, b));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	Numeric A(0);
	Numeric B(0);
	ASSERT_EQ(expectedData + expectedSize, r.unpackUnsafe(expectedData, A, B));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
}

TEST(Record, sizeFixed)
{
	misc::Record<char, uint16_t> r;
	constexpr size_t expectedSize = 3;

	constexpr auto size = r.size();
	ASSERT_EQ(expectedSize, size);
	constexpr auto sizeAllParams = r.size(0, 0);
	ASSERT_EQ(expectedSize, sizeAllParams);
}

TEST(Record, sizeVar)
{
	misc::Record<char, misc::VarString<16>, uint16_t> r;

	char a = 0;
	std::string b("hello");
	uint16_t c = 0;
	constexpr size_t expectedMinSize = 4;
	constexpr size_t expectedSize = 9;
	const char expectedData[] = "\x0\x5\x0\x0hello";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	ASSERT_EQ(expectedMinSize, r.minSize());
	ASSERT_EQ(expectedSize, r.size(b));
	ASSERT_EQ(expectedSize, r.size(a, b, c));

	char data[expectedSize] = {};
	ASSERT_EQ(expectedSize, r.pack(data, expectedSize, a, b, c));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	ASSERT_EQ(0u, r.pack(data, expectedSize - 1, a, b, c));

	memset(data, 0, expectedSize);
	ASSERT_EQ(expectedSize, r.pack(data, expectedSize + 1, a, b, c));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	char A = 1;
	std::string B;
	uint16_t C = 1;
	ASSERT_EQ(expectedSize, r.unpack(data, expectedSize, A, B, C));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);

	ASSERT_EQ(0u, r.unpack(data, expectedMinSize - 1, A, B, C));
	ASSERT_EQ(0u, r.unpack(data, expectedSize - 1, A, B, C));

	A = 1;
	B = "";
	C = 1;
	ASSERT_EQ(expectedSize, r.unpack(data, expectedSize + 1, A, B, C));
	ASSERT_EQ(a, A);
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
}

TEST(Record, recursive)
{
	using Subrecord1 = misc::Record<uint32_t, misc::Number<5>,
		misc::FixedArray<3>, misc::FixedBlob<3>, misc::FixedString<3>,
		misc::FixedCArray<3>, misc::VarVector<3>, misc::VarString<16>,
		misc::Cast<uint16_t, uint8_t>, O32, N320>;
	using Subrecord2 = misc::Record<uint16_t, misc::VarString<16>>;
	using Subrecord3 = misc::Record<Subrecord2, misc::VarString<16>>;
	misc::Record<Subrecord1, Subrecord2, Subrecord3> r;

	const int a = 1; // check conversion from int to uint32_t
	const uint64_t b = 2;
	const std::array<uint8_t, 3> c {{'a', 'b', 'c'}};
	const char d[] = "def";
	const std::string e("x");
	const char f[3] = {'g', 'h', 'i'};
	const std::vector<uint8_t> g({'j'});
	const std::string h("hello");
	const uint16_t i = 3;
	const O32 j(4);
	const N320 k(5);
	const uint16_t l = 6;
	const std::string m("world");
	const uint16_t n = 6;
	const std::string o("HELLO");
	const std::string p("WORLD");
	constexpr size_t expectedSize = 60;
	const char expectedData[] = "\x0\x0\x0\x1    2abcdefx  "
								"ghi\x1\x5\x3\x0\x0\x0\x4\x0\x0\x0\x5\x0\x6\x5"
								"\x0\x6\x5\x5jhelloworldHELLOWORLD";
	static_assert(expectedSize == sizeof(expectedData) - 1);

	char data[expectedSize] = {};
	ASSERT_EQ(data + expectedSize,
		r.packUnsafe(data, misc::group(a, b, c, d, e, f, g, h, i, j, k),
			misc::group(l, m), misc::group(misc::group(n, o), p)));
	ASSERT_EQ(0, memcmp(expectedData, data, expectedSize))
		<< "data: " << misc::dump(data, expectedSize);

	uint32_t A = 0;
	uint64_t B = 0;
	std::array<uint8_t, 3> C;
	char D[3] = {0};
	std::string E;
	char F[3];
	std::vector<uint8_t> G;
	std::string H;
	uint16_t I = 0;
	O32 J;
	N320 K;
	uint16_t L = 0;
	std::string M;
	uint16_t N = 0;
	std::string O;
	std::string P;
	ASSERT_EQ(expectedData + expectedSize,
		r.unpackUnsafe(expectedData,
			misc::group(A, B, C, D, E, F, G, H, I, J, K), misc::group(L, M),
			misc::group(misc::group(N, O), P)));
	ASSERT_EQ(a, static_cast<int>(A));
	ASSERT_EQ(b, B);
	ASSERT_EQ(c, C);
	ASSERT_EQ(0, memcmp(d, D, sizeof(D)));
	ASSERT_EQ(e, E);
	ASSERT_EQ(0, memcmp(f, F, sizeof(F)));
	ASSERT_EQ(g, G);
	ASSERT_EQ(h, H);
	ASSERT_EQ(i, I);
	ASSERT_EQ(j, J);
	ASSERT_EQ(k, K);
	ASSERT_EQ(l, L);
	ASSERT_EQ(m, M);
	ASSERT_EQ(n, N);
	ASSERT_EQ(o, O);
	ASSERT_EQ(p, P);
}
