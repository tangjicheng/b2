#ifndef MISC_THREADMANAGER_H
#define MISC_THREADMANAGER_H

#include <sched.h>

#include <atomic>
#include <map>
#include <memory>
#include <set>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

#include <yaml-cpp/yaml.h>

namespace misc {

class CPUAffinityMask {
public:
	CPUAffinityMask() { clear(); }
	explicit CPUAffinityMask(int cpu)
	{
		clear();
		set(cpu);
	}
	explicit CPUAffinityMask(const cpu_set_t& mask) { m_mask = mask; }

	void clear() { CPU_ZERO(&m_mask); }
	void clear(int cpu) { CPU_CLR(cpu, &m_mask); }
	void set(int cpu) { CPU_SET(cpu, &m_mask); }
	bool isSet(int cpu) const { return CPU_ISSET(cpu, &m_mask) != 0; }
	int count() const { return CPU_COUNT(&m_mask); }

	const cpu_set_t& cObj() const { return m_mask; }

	CPUAffinityMask& operator&=(const CPUAffinityMask& other)
	{
		CPU_AND(&m_mask, &m_mask, &other.m_mask);
		return *this;
	}
	CPUAffinityMask& operator|=(const CPUAffinityMask& other)
	{
		CPU_OR(&m_mask, &m_mask, &other.m_mask);
		return *this;
	}
	CPUAffinityMask& operator^=(const CPUAffinityMask& other)
	{
		CPU_XOR(&m_mask, &m_mask, &other.m_mask);
		return *this;
	}
	bool operator==(const CPUAffinityMask& other) const
	{
		return CPU_EQUAL(&m_mask, &other.m_mask) != 0;
	}
	bool operator!=(const CPUAffinityMask& other) const
	{
		return CPU_EQUAL(&m_mask, &other.m_mask) == 0;
	}

private:
	cpu_set_t m_mask;
};

class ThreadNameError : public std::invalid_argument {
public:
	explicit ThreadNameError(const char* what)
		: std::invalid_argument(what)
	{
	}

	explicit ThreadNameError(const std::string& what)
		: std::invalid_argument(what)
	{
	}
};

class ThreadAffinitySyntaxError : public std::invalid_argument {
public:
	explicit ThreadAffinitySyntaxError(const char* what)
		: std::invalid_argument(what)
	{
	}

	explicit ThreadAffinitySyntaxError(const std::string& what)
		: std::invalid_argument(what)
	{
	}
};

class ThreadManager {
public:
	~ThreadManager() = default;

	ThreadManager(const ThreadManager&) = delete;
	ThreadManager& operator=(const ThreadManager&) = delete;

	ThreadManager(ThreadManager&&) = delete;
	ThreadManager& operator=(ThreadManager&&) = delete;

	static ThreadManager& instance();
	void defineAffinity(const std::string&, const CPUAffinityMask&);
	void setThreadAffinity(
		std::thread&, const std::string&, bool setThreadName = true);
	void setThreadAffinity(const std::string&, bool setThreadName = true);
	void setThreadName(std::thread&, const std::string&);
	void setThreadName(const std::string&);

	/**
	 *  @brief Process a string specification of thread affinity groups,
	 *  apply them directly.
	 *  @param names An optional list of allowed affinity names.
	 *  @throw ThreadNameError If a thread affinity name is not allowed by
	 *  the supplied set of names.
	 *  @throw std::invalid_argument If the given specification string is
	 *  otherwise invalid.
	 *
	 *  Thread affinity groups:
	 *    aff_list    := aff_spec[;aff_list]
	 *    aff_spec    := <aff_name>=mask_list
	 *    mask_list   := mask_spec[,mask_list]
	 *    mask_spec   := <cpu_num>|<mask_hex>|mask_range
	 *    mask_range  := <cpu_num>-<cpu_num>[:stride]
	 */
	void defineAffinities(const std::string&,
		const std::set<std::string>& names = std::set<std::string>());
	void defineAffinities(const YAML::Node&);

private:
	class ThreadInfo {
	public:
		ThreadInfo();

		void addAffinity(const CPUAffinityMask&);
		const CPUAffinityMask* getNextAffinity();

	private:
		std::vector<CPUAffinityMask> m_cpus;
		std::shared_ptr<std::atomic_size_t> m_cur;
	};

	std::map<std::string, ThreadInfo> m_affinityMask;

	ThreadManager() = default;

	const CPUAffinityMask* findAffinity(const std::string&);
};

CPUAffinityMask cpuAffinityMaskFromString(const std::string&);
void setThreadAffinity(std::thread&, const CPUAffinityMask&);
void setThreadAffinity(const CPUAffinityMask&);
CPUAffinityMask getThreadAffinity(std::thread&);
CPUAffinityMask getThreadAffinity();
void setThreadName(std::thread&, const std::string&);
void setThreadName(const std::string&);

} // namespace misc

#endif // MISC_THREADMANAGER_H
