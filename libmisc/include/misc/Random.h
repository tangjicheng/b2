#ifndef MISC_RANDOM_H
#define MISC_RANDOM_H

#include <chrono>
#include <functional>
#include <random>
#include <stdexcept>

namespace misc {

class UniformRandom {
public:
	/**
	 *  @throw std::invalid_argument If @a min is greater than @a max.
	 */
	UniformRandom(int min, int max, unsigned long value = now())
		: m_distribution(min, min > max ? min : max)
		, m_engine(value)
		, m_generator([this] { return m_distribution(m_engine); })
	{
		if (min > max)
			throw std::invalid_argument("min > max");
	}

	int min() const { return m_distribution.min(); }

	int max() const { return m_distribution.max(); }

	void seed(unsigned long value = now())
	{
		m_engine.seed(value);
		m_generator = [this] {
			return m_distribution(m_engine);
		};
	}

	int operator()() { return m_generator(); }

private:
	std::uniform_int_distribution<> m_distribution;
	std::mt19937 m_engine;
	std::function<int()> m_generator;

	static std::chrono::system_clock::rep now()
	{
		return std::chrono::system_clock::now().time_since_epoch().count();
	}
};

} // namespace misc

#endif // MISC_RANDOM_H
