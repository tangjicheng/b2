#ifndef MISC_PIDFILE_H
#define MISC_PIDFILE_H

#include <sys/types.h>

#include <exception>
#include <string>

namespace misc {

class PidReadTimeout : public std::exception {
public:
	const char* what() const noexcept override { return "Read timeout"; }
};

class PidInvalidFileFormat : public std::exception {
public:
	const char* what() const noexcept override { return "Invalid file format"; }
};

class DaemonAlreadyRunning : public std::exception {
public:
	explicit DaemonAlreadyRunning(pid_t pid) noexcept
		: m_pid(pid)
	{
	}

	const char* what() const noexcept override
	{
		return "Daemon already running";
	}

	pid_t pid() const noexcept { return m_pid; }

private:
	pid_t m_pid;
};

class PidFile {
public:
	explicit PidFile(const std::string&) noexcept;
	~PidFile();

	/**
	 *  @throw std::system_error If the file cannot be opened, locked, read
	 *  or truncated.
	 *  @throw PidReadTimeout If the file already exists but does not
	 *  contain a PID and waiting for it to get one has timed out.
	 *  @throw PidInvalidFileFormat If the file already exists but has
	 *  invalid format, i.e. does not contain a valid id.
	 *  @throw DaemonAlreadyRunning If the file already exists and contains
	 *  a PID.
	 */
	void open(int);
	/**
	 *  @throw std::system_error If the file cannot be opened or read.
	 *  @throw PidInvalidFileFormat If the file already exists but has
	 *  invalid format, i.e. does not contain a valid id.
	 */
	static bool read(const std::string&, pid_t&);
	/**
	 *  @throw std::system_error If the file cannot be truncated or written
	 *  to.
	 */
	void write();
	void close() noexcept;
	void remove() noexcept;

private:
	std::string m_filename;
	int m_fd {-1};
};

} // namespace misc

#endif // MISC_PIDFILE_H
