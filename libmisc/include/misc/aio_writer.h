#ifndef MISC_AIO_WRITER_H
#define MISC_AIO_WRITER_H

#include <aio.h>
#include <signal.h>
#include <sys/types.h>

#include <condition_variable>
#include <cstddef>
#include <deque>
#include <mutex>
#include <vector>

namespace misc {

namespace details {

class Aio_control_block {
public:
	explicit Aio_control_block(size_t);
	~Aio_control_block() = default;

	Aio_control_block(const Aio_control_block&) = delete;
	Aio_control_block& operator=(const Aio_control_block&) = delete;

	Aio_control_block(Aio_control_block&&) = delete;
	Aio_control_block& operator=(Aio_control_block&&) = delete;

	bool empty() const;
	size_t size() const;
	size_t free() const;

	void add(const void*, size_t);
	void write(int, off_t);
	void wait();

private:
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
	// error: invalid use of ‘struct aiocb’ with a zero-size array in ‘class
	// Aio_control_block’
	aiocb cb_;
#pragma GCC diagnostic pop
	size_t buf_size_;
	std::vector<unsigned char> buf_;
	bool completion_received_ {false};
	std::mutex mtx_;
	std::condition_variable cv_;

	static void completion_handler(sigval_t);
	void on_completion();
};

} // namespace details

class Aio_writer {
public:
	/**
	 *  @brief Initializes the asynchronous I/O control blocks and sets the
	 *  file descriptor.
	 *  @param num_cbs The number of control blocks.
	 *  @param cb_buf_size The buffer size of each control block.
	 *  @param fd The file descriptor.
	 *  @pre @a num_cbs must be greater than or equal to 1.
	 *  @pre @a cb_buf_size must be greater than or equal to 1024.
	 */
	Aio_writer(size_t num_cbs, size_t cb_buf_size, int fd = -1);

	/**
	 *  @brief Sets the file descriptor.
	 *  @param fd The file descriptor.
	 */
	void attach(int fd);

	/**
	 *  @brief Adds data to the current control block for asynchronous
	 *  writing.
	 *  @param data The location of the buffer to be written.
	 *  @param size The number of bytes to be written.
	 *  @pre @a size must be less than or equal to a single control block's
	 *  buffer size.
	 *  @throw std::system_error If this function call required a check of
	 *  a previous asynchronous write call and that write failed, or, if
	 *  this function call caused an asynchronous write call which failed
	 *  to be enqueued.
	 *
	 *  If there is insufficient available bytes in the current control
	 *  block's buffer, an asynchronous write call will be made and the
	 *  current control block pointer incremented.
	 */
	void add(const void* data, size_t size);

	/**
	 *  @brief Makes an asynchronous write call for the current control
	 *  block then sets the file offset.
	 *  @param offset The file offset.
	 *  @throw std::system_error If this function call required a check of
	 *  a previous asynchronous write call and that write failed, or, if
	 *  this function call caused an asynchronous write call which failed
	 *  to be enqueued.
	 */
	void seek(off_t offset);

	/**
	 *  @brief Makes an asynchronous write call for the current control
	 *  block then waits for asynchronous notification of write completion
	 *  for all control blocks.
	 *  @throw std::system_error If this function call required a check of
	 *  a previous asynchronous write call and that write failed, or, if
	 *  this function call caused an asynchronous write call which failed
	 *  to be enqueued.
	 */
	void wait();

private:
	int fd_;
	off_t offset_ {0};
	std::deque<details::Aio_control_block> cbs_;
	decltype(cbs_)::iterator cb_;

	void write_cb();
	void next_cb();
};

} // namespace misc

#endif // MISC_AIO_WRITER_H
