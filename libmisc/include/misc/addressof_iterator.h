#ifndef MISC_ADDRESSOF_ITERATOR_H
#define MISC_ADDRESSOF_ITERATOR_H

#include <iterator>
#include <memory>
#include <type_traits>

#include "range.h" // addressof_iterator.h guarantees to provide range.h.

namespace misc {

template <typename Iterator>
class addressof_iterator {
private:
	using traits_type = std::iterator_traits<Iterator>;

public:
	using iterator_type = Iterator;

	using iterator_category = typename traits_type::iterator_category;
	using difference_type = typename traits_type::difference_type;
	using value_type = std::add_pointer_t<typename traits_type::value_type>;
	using pointer = void;
	using reference = std::add_pointer_t<typename traits_type::reference>;

	addressof_iterator()
		: m_current()
	{
	}

	explicit addressof_iterator(const iterator_type& i)
		: m_current(i)
	{
	}

	template <typename OtherIterator>
	addressof_iterator(const addressof_iterator<OtherIterator>& i)
		: m_current(i.base())
	{
	}

	template <typename OtherIterator>
	addressof_iterator& operator=(const addressof_iterator<OtherIterator>& i)
	{
		m_current = i.base();
		return *this;
	}

	const iterator_type& base() const { return m_current; }

	reference operator*() const { return std::addressof(*m_current); }

	reference operator[](difference_type n) const
	{
		return std::addressof(m_current[n]);
	}

	addressof_iterator& operator++()
	{
		++m_current;
		return *this;
	}

	addressof_iterator operator++(int)
	{
		return addressof_iterator(m_current++);
	}

	addressof_iterator& operator--()
	{
		--m_current;
		return *this;
	}

	addressof_iterator operator--(int)
	{
		return addressof_iterator(m_current--);
	}

	addressof_iterator& operator+=(difference_type n)
	{
		m_current += n;
		return *this;
	}

	addressof_iterator& operator-=(difference_type n)
	{
		m_current -= n;
		return *this;
	}

private:
	iterator_type m_current;
};

template <typename Iterator>
addressof_iterator<Iterator> operator+(const addressof_iterator<Iterator>& i,
	typename addressof_iterator<Iterator>::difference_type n)
{
	return addressof_iterator<Iterator>(i.base() + n);
}

template <typename Iterator>
addressof_iterator<Iterator> operator+(
	typename addressof_iterator<Iterator>::difference_type n,
	const addressof_iterator<Iterator>& i)
{
	return addressof_iterator<Iterator>(n + i.base());
}

template <typename Iterator>
addressof_iterator<Iterator> operator-(const addressof_iterator<Iterator>& i,
	typename addressof_iterator<Iterator>::difference_type n)
{
	return addressof_iterator<Iterator>(i.base() - n);
}

template <typename IteratorL, typename IteratorR>
auto operator-(const addressof_iterator<IteratorL>& lhs,
	const addressof_iterator<IteratorR>& rhs)
	-> decltype(lhs.base() - rhs.base())
{
	return lhs.base() - rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator==(const addressof_iterator<IteratorL>& lhs,
	const addressof_iterator<IteratorR>& rhs)
{
	return lhs.base() == rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator!=(const addressof_iterator<IteratorL>& lhs,
	const addressof_iterator<IteratorR>& rhs)
{
	return lhs.base() != rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator<(const addressof_iterator<IteratorL>& lhs,
	const addressof_iterator<IteratorR>& rhs)
{
	return lhs.base() < rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator>(const addressof_iterator<IteratorL>& lhs,
	const addressof_iterator<IteratorR>& rhs)
{
	return lhs.base() > rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator<=(const addressof_iterator<IteratorL>& lhs,
	const addressof_iterator<IteratorR>& rhs)
{
	return lhs.base() <= rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator>=(const addressof_iterator<IteratorL>& lhs,
	const addressof_iterator<IteratorR>& rhs)
{
	return lhs.base() >= rhs.base();
}

template <typename Iterator>
addressof_iterator<Iterator> make_addressof_iterator(const Iterator& i)
{
	return addressof_iterator<Iterator>(i);
}

template <typename Iterator>
range<addressof_iterator<Iterator>> addrof(const Iterator& b, const Iterator& e)
{
	return range<addressof_iterator<Iterator>>(b, e);
}

template <typename Range>
auto addrof(Range& r) -> range<addressof_iterator<decltype(std::begin(r))>>
{
	return decltype(addrof(r))(std::begin(r), std::end(r));
}

template <typename Range>
auto addrof(const Range& r)
	-> range<addressof_iterator<decltype(std::begin(r))>>
{
	return decltype(addrof(r))(std::begin(r), std::end(r));
}

} // namespace misc

#endif // MISC_ADDRESSOF_ITERATOR_H
