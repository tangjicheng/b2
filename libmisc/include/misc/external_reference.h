#ifndef MISC_EXTERNAL_REFERENCE_H
#define MISC_EXTERNAL_REFERENCE_H

#include <cstddef>
#include <cstring>
#include <functional>
#include <ostream>
#include <stdexcept>
#include <string>
#include <string_view>

namespace misc {

template <typename Tag_t, size_t length_v>
class External_reference {
public:
	static_assert(length_v > 0 && length_v <= 32,
		"Unsupported external reference length");

	constexpr External_reference() noexcept
		: data_ {0}
	{
	}

	/**
	 *  @throw std::length_error If @a len is greater than @c max_size().
	 */
	External_reference(const char* str, size_t len) { assign(str, len); }

	/**
	 *  @throw std::length_error If std::distance(@a begin, @a end) is
	 *  greater than @c max_size().
	 */
	External_reference(const char* begin, const char* end)
	{
		assign(begin, end);
	}

	/**
	 *  @throw std::length_error If std::strlen(@a str) is greater than @c
	 *  max_size().
	 */
	explicit External_reference(const char* str) { *this = str; }

	/**
	 *  @throw std::length_error If @a str.size() is greater than @c
	 *  max_size().
	 */
	explicit External_reference(const std::string& str) { *this = str; }

	/**
	 *  @throw std::length_error If @a len is greater than @c max_size().
	 */
	void assign(const char* str, size_t len)
	{
		if (len > length_v)
			throw std::length_error {"Too long"};
		memcpy(data_, str, len);
		memset(data_ + len, 0, length_v - len);
	}

	/**
	 *  @throw std::length_error If std::distance(@a begin, @a end) is
	 *  greater than @c max_size().
	 */
	void assign(const char* begin, const char* end)
	{
		assign(begin, end - begin);
	}

	/**
	 *  @throw std::length_error If std::strlen(@a str) is greater than @c
	 *  max_size().
	 */
	External_reference& operator=(const char* str)
	{
		assign(str, strnlen(str, length_v + 1));
		return *this;
	}

	/**
	 *  @throw std::length_error If @a str.size() is greater than @c
	 *  max_size().
	 */
	External_reference& operator=(const std::string& str)
	{
		assign(str.data(), str.size());
		return *this;
	}

	/**
	 *  @brief Subscript access to the data contained in the
	 *  %External_reference.
	 *  @param pos The index of the character to access.
	 *  @return Read/write reference to the character.
	 *
	 *  This operator allows for easy, array-style, data access. Note that
	 *  data access with this operator is unchecked and out of range
	 *  lookups are not defined.
	 */
	char& operator[](size_t pos) noexcept { return data_[pos]; }

	/**
	 *  @brief Subscript access to the data contained in the
	 *  %External_reference.
	 *  @param pos The index of the character to access.
	 *  @return Read-only (constant) reference to the character.
	 *
	 *  This operator allows for easy, array-style, data access. Note that
	 *  data access with this operator is unchecked and out of range
	 *  lookups are not defined.
	 */
	const char& operator[](size_t pos) const noexcept { return data_[pos]; }

	/**
	 *  @brief Return a pointer to the contents of the %External_reference.
	 *  @return A non-const pointer to the underlying array serving as
	 *  character storage. The pointer is such that the range [@c data(),
	 *  @c data() + @c size()) is valid and the values in it correspond to
	 *  the values stored in the object.
	 *
	 *  Unlike std::basic_string::data() and string literals, @c data() may
	 *  return a pointer to a buffer that is not null-terminated. Therefore
	 *  it is typically a mistake to pass @c data() to a routine that takes
	 *  just a const char* and expects a null-terminated string.
	 */
	char* data() noexcept { return data_; }

	/**
	 *  @brief Return a pointer to the contents of the %External_reference.
	 *  @return A const pointer to the underlying array serving as
	 *  character storage. The pointer is such that the range [@c data(),
	 *  @c data() + @c size()) is valid and the values in it correspond to
	 *  the values stored in the object.
	 *
	 *  Unlike std::basic_string::data() and string literals, @c data() may
	 *  return a pointer to a buffer that is not null-terminated. Therefore
	 *  it is typically a mistake to pass @c data() to a routine that takes
	 *  just a const char* and expects a null-terminated string.
	 */
	const char* data() const noexcept { return data_; }

	size_t size() const noexcept { return strnlen(data_, length_v); }

	static constexpr size_t max_size() noexcept { return length_v; }

	bool empty() const noexcept { return data_[0] == 0; }

	void clear() noexcept { memset(data_, 0, length_v); }

private:
	char data_[length_v];
};

namespace details {

template <typename>
struct External_reference_parameters;

template <typename Tag_t, size_t length_v>
struct External_reference_parameters<External_reference<Tag_t, length_v>> {
	using Tag = Tag_t;
	static constexpr auto length = length_v;
};

} // namespace details

// Comparison operators

template <typename Tag_t, size_t length_v>
bool operator==(
	const External_reference<Tag_t, length_v>& lhs, decltype(lhs) rhs) noexcept
{
	return strncmp(lhs.data(), rhs.data(), length_v) == 0;
}

template <typename Tag_t, size_t length_v>
bool operator!=(
	const External_reference<Tag_t, length_v>& lhs, decltype(lhs) rhs) noexcept
{
	return strncmp(lhs.data(), rhs.data(), length_v) != 0;
}

template <typename Tag_t, size_t length_v>
bool operator<(
	const External_reference<Tag_t, length_v>& lhs, decltype(lhs) rhs) noexcept
{
	return strncmp(lhs.data(), rhs.data(), length_v) < 0;
}

template <typename Tag_t, size_t length_v>
bool operator>(
	const External_reference<Tag_t, length_v>& lhs, decltype(lhs) rhs) noexcept
{
	return strncmp(lhs.data(), rhs.data(), length_v) > 0;
}

template <typename Tag_t, size_t length_v>
bool operator<=(
	const External_reference<Tag_t, length_v>& lhs, decltype(lhs) rhs) noexcept
{
	return strncmp(lhs.data(), rhs.data(), length_v) <= 0;
}

template <typename Tag_t, size_t length_v>
bool operator>=(
	const External_reference<Tag_t, length_v>& lhs, decltype(lhs) rhs) noexcept
{
	return strncmp(lhs.data(), rhs.data(), length_v) >= 0;
}

// Equality operators with null-terminated array of char

template <typename Tag_t, size_t length_v>
bool operator==(
	const External_reference<Tag_t, length_v>& lhs, const char* rhs) noexcept
{
	size_t i = 0;
	for (; i != length_v && lhs[i] != 0 && rhs[i] != 0; ++i) {
		if (lhs[i] != rhs[i])
			return false;
	}
	return (i == length_v || lhs[i] == 0) && rhs[i] == 0;
}

template <typename Tag_t, size_t length_v>
bool operator==(
	const char* lhs, const External_reference<Tag_t, length_v>& rhs) noexcept
{
	return rhs == lhs;
}

template <typename Tag_t, size_t length_v>
bool operator!=(
	const External_reference<Tag_t, length_v>& lhs, const char* rhs) noexcept
{
	return !(lhs == rhs);
}

template <typename Tag_t, size_t length_v>
bool operator!=(
	const char* lhs, const External_reference<Tag_t, length_v>& rhs) noexcept
{
	return !(lhs == rhs);
}

// Equality operators with std::string

template <typename Tag_t, size_t length_v>
bool operator==(const External_reference<Tag_t, length_v>& lhs,
	const std::string& rhs) noexcept
{
	return lhs == rhs.c_str();
}

template <typename Tag_t, size_t length_v>
bool operator==(const std::string& lhs,
	const External_reference<Tag_t, length_v>& rhs) noexcept
{
	return rhs == lhs;
}

template <typename Tag_t, size_t length_v>
bool operator!=(const External_reference<Tag_t, length_v>& lhs,
	const std::string& rhs) noexcept
{
	return !(lhs == rhs);
}

template <typename Tag_t, size_t length_v>
bool operator!=(const std::string& lhs,
	const External_reference<Tag_t, length_v>& rhs) noexcept
{
	return !(lhs == rhs);
}

// std::string conversion

template <typename Tag_t, size_t length_v>
std::string to_string(
	const misc::External_reference<Tag_t, length_v>& r) noexcept
{
	return std::string {r.data(), r.size()};
}

// Output stream operator

template <typename Tag_t, size_t length_v>
std::ostream& operator<<(
	std::ostream& os, const External_reference<Tag_t, length_v>& rhs)
{
	char str[length_v + 1];
	memcpy(str, rhs.data(), length_v);
	str[length_v] = 0;
	os << str;
	return os;
}

// Class declaration macro

#define MISC_EXTERNAL_REFERENCE_DECL(external_reference_name, length) \
	class external_reference_name##_tag;                              \
	using external_reference_name                                     \
		= misc::External_reference<external_reference_name##_tag, length>;

} // namespace misc

namespace std {

// std::hash specialization

template <typename Tag_t, size_t length_v>
struct hash<misc::External_reference<Tag_t, length_v>> {
	using argument_type = misc::External_reference<Tag_t, length_v>;
	using result_type = size_t;

	result_type operator()(const argument_type& r) const noexcept
	{
		return std::hash<std::string_view> {}(
			std::string_view {r.data(), r.size()});
	}
};

} // namespace std

#endif // MISC_EXTERNAL_REFERENCE_H
