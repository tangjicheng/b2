#ifndef MISC_PLIST_H
#define MISC_PLIST_H

#include <sys/types.h>

#include <cstddef>
#include <exception>
#include <istream>
#include <map>
#include <ostream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

#include <expat.h>

#include "dereference_iterator.h" // plist.h guarantees to provide dereference_iterator.h.

namespace plist {

class Error : public std::exception {
public:
	const char* what() const noexcept override { return what_.c_str(); }

protected:
	explicit Error(std::string&& what)
		: what_ {std::move(what)}
	{
	}

private:
	std::string what_;
};

class OpenFailure : public Error {
public:
	OpenFailure()
		: Error("Cannot open file")
	{
	}
};

class ParseError : public Error {
public:
	ParseError(XML_Error error, int line)
		: Error(std::string {XML_ErrorString(error)}
			+ ". Line number: " + std::to_string(line)
			+ ". XML error: " + std::to_string(static_cast<int>(error)))
		, m_error(error)
		, m_lineNumber(line)
	{
	}

	XML_Error error() const { return m_error; }
	int lineNumber() const { return m_lineNumber; }

private:
	XML_Error m_error;
	int m_lineNumber;
};

class FormatError : public Error {
public:
	FormatError(const char* what, int line)
		: Error(std::string {what} + ". Line number: " + std::to_string(line))
		, m_lineNumber(line)
	{
	}

	int lineNumber() const { return m_lineNumber; }

private:
	int m_lineNumber;
};

class KeyError : public Error {
public:
	explicit KeyError(const std::string& key)
		: Error(std::string {"Key not found. Key: "} + key)
		, m_key(key)
	{
	}

	const std::string& key() const { return m_key; }

private:
	std::string m_key;
};

class IndexError : public Error {
public:
	explicit IndexError(size_t index)
		: Error(
			std::string {"Index out of range. Index: "} + std::to_string(index))
		, m_index(index)
	{
	}

	size_t index() const { return m_index; }

private:
	size_t m_index;
};

class TypeError : public Error {
public:
	TypeError()
		: Error("Type error")
	{
	}
};

namespace details {
class Writer;
}
class Dict;
class Array;

class Element {
	friend class details::Writer;

public:
	virtual ~Element() = default;

	virtual Dict& asDict() { throw TypeError(); }
	virtual const Dict& asDict() const { throw TypeError(); }
	virtual Array& asArray() { throw TypeError(); }
	virtual const Array& asArray() const { throw TypeError(); }
	virtual std::string& asString() { throw TypeError(); }
	virtual const std::string& asString() const { throw TypeError(); }
	virtual bool& asBool() { throw TypeError(); }
	virtual bool asBool() const { throw TypeError(); }
	virtual long& asInteger() { throw TypeError(); }
	virtual long asInteger() const { throw TypeError(); }
	virtual double& asReal() { throw TypeError(); }
	virtual double asReal() const { throw TypeError(); }

	virtual Element& operator[](const std::string&) { throw TypeError(); }
	virtual const Element& operator[](const std::string&) const
	{
		throw TypeError();
	}
	virtual Element& operator[](size_t) { throw TypeError(); }
	virtual const Element& operator[](size_t) const { throw TypeError(); }

	virtual std::string getString(const std::string&, const std::string&) const
	{
		throw TypeError();
	}
	virtual std::string getString(size_t, const std::string&) const
	{
		throw TypeError();
	}
	virtual bool getBool(const std::string&, bool) const { throw TypeError(); }
	virtual bool getBool(size_t, bool) const { throw TypeError(); }
	virtual long getInteger(const std::string&, long) const
	{
		throw TypeError();
	}
	virtual long getInteger(size_t, long) const { throw TypeError(); }
	virtual double getReal(const std::string&, double) const
	{
		throw TypeError();
	}
	virtual double getReal(size_t, double) const { throw TypeError(); }

protected:
	Element() = default;

	Element(const Element&) = default;
	Element& operator=(const Element&) = default;

	Element(Element&&) = default;
	Element& operator=(Element&&) = default;

private:
	virtual void write(
		std::ostream&, unsigned int, const details::Writer&) const = 0;
};

namespace details {

class NullElement : public Element {
private:
	void write(
		std::ostream&, unsigned int, const details::Writer&) const override
	{
	}
};

} // namespace details

class Plist : public Element {
public:
	Plist()
		: m_root(new details::NullElement)
	{
	}
	/**
	 *  @pre The element referred to by @a root must not be null.
	 */
	explicit Plist(Element* root)
		: m_root(root)
	{
	}
	~Plist() override { delete m_root; }

	Plist(const Plist&) = delete;
	Plist& operator=(const Plist&) = delete;

	Plist(Plist&& other)
	{
		m_root = other.m_root;
		other.m_root = nullptr;
	}
	Plist& operator=(Plist&& other)
	{
		delete m_root;
		m_root = other.m_root;
		other.m_root = nullptr;
		return *this;
	}

	Dict& asDict() override { return m_root->asDict(); }
	const Dict& asDict() const override { return m_root->asDict(); }
	Array& asArray() override { return m_root->asArray(); }
	const Array& asArray() const override { return m_root->asArray(); }
	std::string& asString() override { return m_root->asString(); }
	const std::string& asString() const override { return m_root->asString(); }
	bool& asBool() override { return m_root->asBool(); }
	bool asBool() const override { return m_root->asBool(); }
	long& asInteger() override { return m_root->asInteger(); }
	long asInteger() const override { return m_root->asInteger(); }
	double& asReal() override { return m_root->asReal(); }
	double asReal() const override { return m_root->asReal(); }

	Element& operator[](const std::string& key) override
	{
		return (*m_root)[key];
	}
	const Element& operator[](const std::string& key) const override
	{
		return (*m_root)[key];
	}
	Element& operator[](size_t i) override { return (*m_root)[i]; }
	const Element& operator[](size_t i) const override { return (*m_root)[i]; }

	std::string getString(
		const std::string& key, const std::string& def) const override
	{
		return m_root->getString(key, def);
	}
	std::string getString(size_t i, const std::string& def) const override
	{
		return m_root->getString(i, def);
	}
	bool getBool(const std::string& key, bool def) const override
	{
		return m_root->getBool(key, def);
	}
	bool getBool(size_t i, bool def) const override
	{
		return m_root->getBool(i, def);
	}
	long getInteger(const std::string& key, long def) const override
	{
		return m_root->getInteger(key, def);
	}
	long getInteger(size_t i, long def) const override
	{
		return m_root->getInteger(i, def);
	}
	double getReal(const std::string& key, double def) const override
	{
		return m_root->getReal(key, def);
	}
	double getReal(size_t i, double def) const override
	{
		return m_root->getReal(i, def);
	}

private:
	Element* m_root;

	void write(
		std::ostream&, unsigned int, const details::Writer&) const override;
};

class Dict : public Element {
public:
	~Dict() override
	{
		for (auto& [key, element] : m_dict) {
			(void)key;
			delete element;
		}
	}

	Dict& asDict() override { return *this; }
	const Dict& asDict() const override { return *this; }

	Element& operator[](const std::string&) override;
	const Element& operator[](const std::string&) const override;

	std::string getString(
		const std::string&, const std::string&) const override;
	bool getBool(const std::string&, bool) const override;
	long getInteger(const std::string&, long) const override;
	double getReal(const std::string&, double) const override;

	size_t size() const { return m_dict.size(); }
	bool empty() const { return m_dict.empty(); }

	void insert(const std::string& key, Element* element)
	{
		m_dict[key] = element;
	}
	void insert(std::string&& key, Element* element)
	{
		m_dict[std::move(key)] = element;
	}

private:
	std::map<std::string, Element*> m_dict;

	void write(
		std::ostream&, unsigned int, const details::Writer&) const override;
};

class Array : public Element {
public:
	~Array() override
	{
		for (auto element : m_array)
			delete element;
	}

	Array& asArray() override { return *this; }
	const Array& asArray() const override { return *this; }

	Element& operator[](size_t) override;
	const Element& operator[](size_t) const override;

	std::string getString(size_t, const std::string&) const override;
	bool getBool(size_t, bool) const override;
	long getInteger(size_t, long) const override;
	double getReal(size_t, double) const override;

	auto begin() { return misc::dereference_iterator(m_array.begin()); }
	auto begin() const { return misc::dereference_iterator(m_array.begin()); }
	auto end() { return misc::dereference_iterator(m_array.end()); }
	auto end() const { return misc::dereference_iterator(m_array.end()); }

	size_t size() const { return m_array.size(); }
	bool empty() const { return m_array.empty(); }

	void append(Element* element) { m_array.push_back(element); }

private:
	std::vector<Element*> m_array;

	void write(
		std::ostream&, unsigned int, const details::Writer&) const override;
};

class StringValue : public Element {
public:
	explicit StringValue(const std::string& value)
		: m_value(value)
	{
	}

	std::string& asString() override { return m_value; }
	const std::string& asString() const override { return m_value; }

private:
	std::string m_value;

	void write(
		std::ostream&, unsigned int, const details::Writer&) const override;
};

class BoolValue : public Element {
public:
	explicit BoolValue(bool value)
		: m_value(value)
	{
	}

	bool& asBool() override { return m_value; }
	bool asBool() const override { return m_value; }

private:
	bool m_value;

	void write(
		std::ostream&, unsigned int, const details::Writer&) const override;
};

class IntegerValue : public Element {
public:
	explicit IntegerValue(long value)
		: m_value(value)
	{
	}
	explicit IntegerValue(const char*);
	explicit IntegerValue(const std::string& value)
		: IntegerValue(value.c_str())
	{
	}

	long& asInteger() override { return m_value; }
	long asInteger() const override { return m_value; }

private:
	long m_value;

	void write(
		std::ostream&, unsigned int, const details::Writer&) const override;
};

class RealValue : public Element {
public:
	explicit RealValue(double value)
		: m_value(value)
	{
	}
	explicit RealValue(const char*);
	explicit RealValue(const std::string& value)
		: RealValue(value.c_str())
	{
	}

	double& asReal() override { return m_value; }
	double asReal() const override { return m_value; }

private:
	double m_value;

	void write(
		std::ostream&, unsigned int, const details::Writer&) const override;
};

class PlistParser {
public:
	PlistParser() { m_parser = XML_ParserCreate(nullptr); }
	~PlistParser() { XML_ParserFree(m_parser); }

	PlistParser(const PlistParser&) = delete;
	PlistParser& operator=(const PlistParser&) = delete;

	PlistParser(PlistParser&&) = delete;
	PlistParser& operator=(PlistParser&&) = delete;

	Plist parseFile(const char*);
	Plist parseFile(const std::string& filename)
	{
		return parseFile(filename.c_str());
	}
	Plist parseStream(std::istream&);

private:
	XML_Parser m_parser;
	Element* m_root;
	std::stack<Element*> m_stack;
	std::string m_cur_key;
	std::string m_cur_data;

	void resetParser();
	void addElement(Element*);

	static void XMLCALL startElement(
		void* userData, const char* name, const char** attr)
	{
		reinterpret_cast<PlistParser*>(userData)->startElement(name, attr);
	}
	void startElement(const char*, const char**);

	static void XMLCALL endElement(void* userData, const char* name)
	{
		reinterpret_cast<PlistParser*>(userData)->endElement(name);
	}
	void endElement(const char*);

	static void XMLCALL characterData(
		void* userData, const XML_Char* content, int len)
	{
		reinterpret_cast<PlistParser*>(userData)->characterData(content, len);
	}
	void characterData(const XML_Char* content, int len)
	{
		m_cur_data.append(content, len);
	}
};

namespace details {

class Writer {
public:
	void operator()(
		const Element* element, std::ostream& os, unsigned int depth) const
	{
		element->write(os, depth, *this);
	}
};

} // namespace details

class PlistWriter {
public:
	void writeFile(const Plist&, const char*) const;
	void writeFile(const Plist& pl, const std::string& filename) const
	{
		return writeFile(pl, filename.c_str());
	}
	void writeStream(const Plist&, std::ostream&) const;
};

} // namespace plist

#endif // MISC_PLIST_H
