#ifndef MISC_SECOND_ITERATOR_H
#define MISC_SECOND_ITERATOR_H

#include "nth_iterator.h"
#include "range.h" // second_iterator.h guarantees to provide range.h.

namespace misc {

template <typename Iterator>
using second_iterator = nth_iterator<1, Iterator>;

template <typename Iterator>
second_iterator<Iterator> make_second_iterator(const Iterator& i)
{
	return second_iterator<Iterator>(i);
}

template <typename Iterator>
range<second_iterator<Iterator>> second(const Iterator& b, const Iterator& e)
{
	return range<second_iterator<Iterator>>(b, e);
}

template <typename Range>
auto second(Range& r) -> range<second_iterator<decltype(std::begin(r))>>
{
	return decltype(second(r))(std::begin(r), std::end(r));
}

template <typename Range>
auto second(const Range& r) -> range<second_iterator<decltype(std::begin(r))>>
{
	return decltype(second(r))(std::begin(r), std::end(r));
}

} // namespace misc

#endif // MISC_SECOND_ITERATOR_H
