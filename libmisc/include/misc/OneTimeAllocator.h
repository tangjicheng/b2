#pragma once

#include <memory>
#include <new>
#include <utility>

namespace misc {

template <typename T, typename Alloc = std::allocator<T>>
class OneTimeAllocator {
	template <typename U, typename OtherAlloc>
	friend class OneTimeAllocator;

	using T_alloc_type =
		typename std::allocator_traits<Alloc>::template rebind_alloc<T>;
	using T_alloc_traits =
		typename std::allocator_traits<Alloc>::template rebind_traits<T>;

public:
	using size_type = typename T_alloc_traits::size_type;
	using difference_type = typename T_alloc_traits::difference_type;
	using value_type = typename T_alloc_traits::value_type;
	using pointer = typename T_alloc_traits::pointer;
	using const_pointer = typename T_alloc_traits::const_pointer;
	using reference = value_type&;
	using const_reference = const value_type&;
	using void_pointer = typename T_alloc_traits::void_pointer;
	using const_void_pointer = typename T_alloc_traits::const_void_pointer;

	template <typename U>
	struct rebind {
		typedef OneTimeAllocator<U, Alloc> other;
	};

	using propagate_on_container_move_assignment =
		typename T_alloc_traits::propagate_on_container_move_assignment;
	using propagate_on_container_copy_assignment =
		typename T_alloc_traits::propagate_on_container_copy_assignment;
	using propagate_on_container_swap =
		typename T_alloc_traits::propagate_on_container_swap;

	OneTimeAllocator() = default;
	~OneTimeAllocator() = default;

	explicit OneTimeAllocator(const Alloc& base)
		: m_base(base)
	{
	}

	OneTimeAllocator(const OneTimeAllocator& other)
		: m_base(other.m_base)
	{
	}

	OneTimeAllocator& operator=(const OneTimeAllocator& other)
	{
		m_base = other.m_base;
		return *this;
	}

	OneTimeAllocator(OneTimeAllocator&&) = default;

	OneTimeAllocator& operator=(OneTimeAllocator&& other)
	{
		m_base = std::move(other.m_base);
		m_used = m_used || other.m_used;
		return *this;
	}

	template <typename U>
	OneTimeAllocator(const OneTimeAllocator<U, Alloc>& other)
		: m_base(other.m_base)
	{
	}

	pointer address(reference x) const { return m_base.address(x); }

	const_pointer address(const_reference x) const { return m_base.address(x); }

	/**
	 *  @throw std::bad_alloc If the %OneTimeAllocator has already been
	 *  used to allocate memory.
	 */
	pointer allocate(size_type n)
	{
		if (m_used)
			throw std::bad_alloc();
		m_used = true;
		return T_alloc_traits::allocate(m_base, n);
	}

	/**
	 *  @throw std::bad_alloc If the %OneTimeAllocator has already been
	 *  used to allocate memory.
	 */
	pointer allocate(size_type n, const_void_pointer hint)
	{
		if (m_used)
			throw std::bad_alloc();
		m_used = true;
		return T_alloc_traits::allocate(m_base, n, hint);
	}

	void deallocate(pointer p, size_type n)
	{
		T_alloc_traits::deallocate(m_base, p, n);
	}

	size_type max_size() const { return T_alloc_traits::max_size(m_base); }

	template <typename U, typename... Args>
	void construct(U* p, Args&&... args)
	{
		::new ((void*)p) U(std::forward<Args>(args)...);
	}

	template <typename U>
	void destroy(U* p)
	{
		p->~U();
	}

	bool operator==(const OneTimeAllocator& rhs)
	{
		return m_base == rhs.m_base;
	}

	bool operator!=(const OneTimeAllocator& rhs)
	{
		return m_base != rhs.m_base;
	}

private:
	T_alloc_type m_base;
	bool m_used {false};
};

} // namespace misc
