#ifndef MISC_LOG_H
#define MISC_LOG_H

#include <cstdarg>

namespace misc {

/**
 *  @brief The priority determines the importance of the message.
 *
 *  The levels are in order of decreasing importance:
 *  APP_LOG_EMERG    system is unusable
 *  APP_LOG_ALERT    action must be taken immediately
 *  APP_LOG_CRIT     critical conditions
 *  APP_LOG_ERR      error conditions
 *  APP_LOG_WARNING  warning conditions
 *  APP_LOG_NOTICE   normal but significant condition
 *  APP_LOG_INFO     informational message
 *  APP_LOG_DEBUG    debug-level message
 */
enum {
	APP_LOG_EMERG,
	APP_LOG_ALERT,
	APP_LOG_CRIT,
	APP_LOG_ERR,
	APP_LOG_WARNING,
	APP_LOG_NOTICE,
	APP_LOG_INFO,
	APP_LOG_DEBUG
};

using app_logger_t = void (*)(int, const char*, va_list) noexcept;

app_logger_t appSetLogger(app_logger_t) noexcept;
void appScreenLogger(int, const char*, va_list) noexcept;
void appScreenLoggerNoTime(int, const char*, va_list) noexcept;

void appLog(int, const char*, ...) noexcept
	__attribute__((__format__(__printf__, 2, 3)));

extern int debug;

} // namespace misc

#define APP_DEBUG(level, ...)                               \
	do {                                                    \
		if (misc::debug >= (level))                         \
			misc::appLog(misc::APP_LOG_DEBUG, __VA_ARGS__); \
	} while (0)
#define APP_DEBUG_BEGIN(level) if (misc::debug >= (level)) {
#define APP_DEBUG_END() }

#endif // MISC_LOG_H
