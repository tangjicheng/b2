#ifndef MISC_ID_GENERATOR_H
#define MISC_ID_GENERATOR_H

#include <cstdint>
#include <limits>

#include "date.h"
#include "pow.h"

namespace misc {

template <typename Id, typename No>
class Id_generator {
	static_assert(sizeof(Id) >= sizeof(No));

public:
	Id_generator() = default;

	void reset(Date date)
	{
		date_ = date.yyyymmdd() * multiplier;
		no_ = 1;
	}

	Id peek() const { return Id {date_ + no_}; }
	Id next() { return Id {date_ + (no_++)}; }

	static Date date(Id id)
	{
		return Date {static_cast<uint32_t>(id.value() / multiplier)};
	}
	static No no(Id id)
	{
		return No {
			static_cast<typename No::Underlying_type>(id.value() % multiplier)};
	}
	static Id id(Date date, No no)
	{
		return Id {date.yyyymmdd() * multiplier + no.value()};
	}

private:
	typename Id::Underlying_type date_ {};
	typename No::Underlying_type no_ {1};

	static constexpr auto multiplier = pow<typename Id::Underlying_type>(
		10, std::numeric_limits<typename No::Underlying_type>::digits10 + 1);
};

} // namespace misc

#endif // MISC_ID_GENERATOR_H
