#ifndef MISC_FORWARDMEMORYPOOL_H
#define MISC_FORWARDMEMORYPOOL_H

#include <array>
#include <cstddef>
#include <cstdint>

namespace misc {

class ForwardMemoryPool {
public:
	using size_type = size_t;
	using difference_type = ptrdiff_t;

	ForwardMemoryPool(void*, size_type);
	~ForwardMemoryPool() = default;

	ForwardMemoryPool(const ForwardMemoryPool&) = delete;
	ForwardMemoryPool& operator=(const ForwardMemoryPool&) = delete;

	ForwardMemoryPool(ForwardMemoryPool&&) = delete;
	ForwardMemoryPool& operator=(ForwardMemoryPool&&) = delete;

	size_type size() const { return m_size; }

	size_type used() const { return m_used; }

	difference_type balance() const { return m_balance; }

	void reset();

	/**
	 *  @throw std::bad_alloc If @a n exceeds @c size() - @c used().
	 *
	 *  @warning This function does not properly align allocations.
	 */
	void* allocate(size_type n);

	void deallocate(void*, size_type n) { m_balance -= n; }

private:
	void* m_start;
	size_type m_size;
	size_type m_used {0};
	difference_type m_balance {0};
};

class HeapForwardMemoryPool {
public:
	using size_type = ForwardMemoryPool::size_type;
	using difference_type = ForwardMemoryPool::difference_type;

	explicit HeapForwardMemoryPool(size_type);
	~HeapForwardMemoryPool();

	HeapForwardMemoryPool(const HeapForwardMemoryPool&) = delete;
	HeapForwardMemoryPool& operator=(const HeapForwardMemoryPool&) = delete;

	HeapForwardMemoryPool(HeapForwardMemoryPool&&) = delete;
	HeapForwardMemoryPool& operator=(HeapForwardMemoryPool&&) = delete;

	size_type size() const { return m_pool.size(); }

	size_type used() const { return m_pool.used(); }

	difference_type balance() const { return m_pool.balance(); }

	void reset() { m_pool.reset(); }

	/**
	 *  @throw std::bad_alloc If @a n exceeds @c size() - @c used().
	 */
	void* allocate(size_type n) { return m_pool.allocate(n); }

	void deallocate(void* p, size_type n) { m_pool.deallocate(p, n); }

private:
	void* m_heap;
	ForwardMemoryPool m_pool;
};

template <size_t N>
class ArrayForwardMemoryPool {
public:
	using size_type = ForwardMemoryPool::size_type;
	using difference_type = ForwardMemoryPool::difference_type;

	ArrayForwardMemoryPool();
	~ArrayForwardMemoryPool() = default;

	ArrayForwardMemoryPool(const ArrayForwardMemoryPool&) = delete;
	ArrayForwardMemoryPool& operator=(const ArrayForwardMemoryPool&) = delete;

	ArrayForwardMemoryPool(ArrayForwardMemoryPool&&) = delete;
	ArrayForwardMemoryPool& operator=(ArrayForwardMemoryPool&&) = delete;

	size_type size() const { return m_pool.size(); }

	size_type used() const { return m_pool.used(); }

	difference_type balance() const { return m_pool.balance(); }

	void reset() { m_pool.reset(); }

	/**
	 *  @throw std::bad_alloc If @a n exceeds @c size() - @c used().
	 */
	void* allocate(size_type n) { return m_pool.allocate(n); }

	void deallocate(void* p, size_type n) { m_pool.deallocate(p, n); }

private:
	std::array<uint8_t, N> m_array;
	ForwardMemoryPool m_pool;
};

template <size_t N>
ArrayForwardMemoryPool<N>::ArrayForwardMemoryPool()
	: m_pool(m_array.data(), N)
{
}

} // namespace misc

#endif // MISC_FORWARDMEMORYPOOL_H
