#ifndef MISC_OBJECT_ID_H
#define MISC_OBJECT_ID_H

#include <cstddef>
#include <functional>
#include <istream>
#include <ostream>
#include <string>

namespace misc {

namespace details {

struct Object_id_no_sentinel {
};
struct Object_id_has_sentinel {
};

} // namespace details

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
class Object_id {
public:
	using Underlying_type = Underlying_type_t;

	Object_id() = default;

	constexpr explicit Object_id(Underlying_type value) noexcept
		: value_ {value}
	{
	}

	constexpr Underlying_type value() const noexcept { return value_; }

	constexpr static bool has_sentinel() noexcept
	{
		return std::is_same_v<Sentinel_policy_t,
			details::Object_id_has_sentinel>;
	}

	constexpr bool is_valid() const noexcept
	{
		static_assert(has_sentinel(), "This Object_id has no sentinel");
		return sentinel_v != value_;
	}

	constexpr static Underlying_type sentinel() noexcept
	{
		static_assert(has_sentinel(), "This Object_id has no sentinel");
		return Underlying_type {sentinel_v};
	}

private:
	Underlying_type value_ {
		std::is_same_v<Sentinel_policy_t, details::Object_id_has_sentinel>
			? sentinel_v
			: 0};
};

namespace details {

template <typename>
struct Object_id_parameters;

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t, Underlying_type_t sentinel_v>
struct Object_id_parameters<
	Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>> {
	using Tag = Tag_t;
	using Underlying_type = Underlying_type_t;
	using Sentinel_policy = Sentinel_policy_t;
	static constexpr Underlying_type_t sentinel = sentinel_v;
};

} // namespace details

// Comparison operators

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
constexpr bool operator==(
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		lhs,
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		rhs) noexcept
{
	return lhs.value() == rhs.value();
}

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
constexpr bool operator!=(
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		lhs,
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		rhs) noexcept
{
	return lhs.value() != rhs.value();
}

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
constexpr bool operator<(
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		lhs,
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		rhs) noexcept
{
	return lhs.value() < rhs.value();
}

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
constexpr bool operator>(
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		lhs,
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		rhs) noexcept
{
	return lhs.value() > rhs.value();
}

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
constexpr bool operator<=(
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		lhs,
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		rhs) noexcept
{
	return lhs.value() <= rhs.value();
}

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
constexpr bool operator>=(
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		lhs,
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		rhs) noexcept
{
	return lhs.value() >= rhs.value();
}

// std::string conversion

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
std::string to_string(
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		o) noexcept
{
	return std::to_string(o.value());
}

// Stream operators

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
std::ostream& operator<<(std::ostream& os,
	const Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>&
		rhs)
{
	return os << rhs.value();
}

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t = details::Object_id_no_sentinel,
	Underlying_type_t sentinel_v = Underlying_type_t()>
std::istream& operator>>(std::istream& is,
	Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>& rhs)
{
	typename Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t,
		sentinel_v>::Underlying_type value;
	if (is >> value)
		rhs = Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t,
			sentinel_v> {value};
	return is;
}

// Class declaration macros

#define MISC_OBJECT_ID_DECL(object_id_name, underlying_type) \
	class object_id_name##_tag;                              \
	using object_id_name                                     \
		= misc::Object_id<object_id_name##_tag, underlying_type>;

#define MISC_OBJECT_ID_HAS_SENTINEL_DECL(                        \
	object_id_name, underlying_type, sentinel)                   \
	class object_id_name##_tag;                                  \
	using object_id_name = misc::Object_id<object_id_name##_tag, \
		underlying_type, misc::details::Object_id_has_sentinel, sentinel>;

} // namespace misc

namespace std {

// std::hash specialization

template <typename Tag_t, typename Underlying_type_t,
	typename Sentinel_policy_t, Underlying_type_t sentinel_v>
struct hash<
	misc::Object_id<Tag_t, Underlying_type_t, Sentinel_policy_t, sentinel_v>> {
	using argument_type = misc::Object_id<Tag_t, Underlying_type_t,
		Sentinel_policy_t, sentinel_v>;
	using result_type = size_t;

	result_type operator()(const argument_type& o) const noexcept
	{
		return std::hash<typename argument_type::Underlying_type> {}(o.value());
	}
};

} // namespace std

#endif // MISC_OBJECT_ID_H
