#ifndef MISC_UNDERLYING_H
#define MISC_UNDERLYING_H

#include <istream>
#include <ostream>
#include <type_traits>

namespace misc {

template <typename T, typename Enable = void>
struct underlying_traits {
	static_assert(sizeof(T) == 0,
		"Type not supported"); // Cannot use static_assert(false, ...).

	static std::ostream& out(std::ostream&, const T&);
	static std::istream& in(std::istream&, T&);
};

template <typename T>
struct underlying_traits<T, std::enable_if_t<std::is_enum_v<T>>> {
	static std::ostream& out(std::ostream& os, T e)
	{
		return os << static_cast<std::underlying_type_t<T>>(e);
	}

	static std::istream& in(std::istream& is, T& e)
	{
		std::underlying_type_t<T> val;
		is >> val;
		e = static_cast<T>(val);
		return is;
	}
};

namespace details {

class underlying_out_proxy {
public:
	explicit underlying_out_proxy(std::ostream& os)
		: m_os(&os)
	{
	}

	template <typename T>
	std::ostream& operator<<(const T& rhs)
	{
		return underlying_traits<T>::out(*m_os, rhs);
	}

private:
	std::ostream* m_os;
};

class underlying_in_proxy {
public:
	explicit underlying_in_proxy(std::istream& is)
		: m_is(&is)
	{
	}

	template <typename T>
	std::istream& operator>>(T& rhs)
	{
		return underlying_traits<T>::in(*m_is, rhs);
	}

private:
	std::istream* m_is;
};

} // namespace details

class underlying_tag {
};

extern underlying_tag underlying;

inline details::underlying_out_proxy operator<<(
	std::ostream& os, const underlying_tag&)
{
	return details::underlying_out_proxy(os);
}

inline details::underlying_in_proxy operator>>(
	std::istream& is, const underlying_tag&)
{
	return details::underlying_in_proxy(is);
}

} // namespace misc

#endif // MISC_UNDERLYING_H
