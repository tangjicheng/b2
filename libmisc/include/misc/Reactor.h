#ifndef MISC_REACTOR_H
#define MISC_REACTOR_H

#include <unistd.h>

#include <atomic>
#include <chrono>
#include <cstdint>
#include <functional>
#include <map>
#include <set>
#include <system_error>
#include <thread>
#include <utility>
#include <vector>

#include <sys/epoll.h>

#include "atomic_queue.h"
#include "hardware_interference_size.h"

namespace misc {

class Reactor {
private:
	class CallIdGenerator;

public:
	using signal_system_error_type
		= std::function<void(const std::system_error&)>;
	using idle_strategy = std::function<void(bool)>;

	signal_system_error_type& signalSystemError()
	{
		return m_signalSystemError;
	}
	const idle_strategy& idleStrategy() const { return m_idleStrategy; }
	void idleStrategy(idle_strategy&& idleStrategy)
	{
		m_idleStrategy = std::move(idleStrategy);
	}

	class CallId {
		friend class Reactor;
		friend class CallIdGenerator;

	public:
		CallId() = default;

		bool operator==(CallId rhs) const { return m_value == rhs.m_value; }
		bool operator!=(CallId rhs) const { return m_value != rhs.m_value; }
		explicit operator bool() const { return m_value != 0; }

		void clear() { m_value = 0; }

	private:
		uint64_t m_value {0};

		explicit CallId(uint64_t);

		uint64_t base() const;
	};

	enum class CallStatus { OK, REMOVE };

	using IOCall = std::function<CallStatus(int)>;
	using RecurringCall = std::function<CallStatus()>;
	using SingleCall = std::function<void()>;

	explicit Reactor(bool supportsIoCalls = true,
		bool supportsTimedCalls = true, bool supportsLoopCalls = false);
	~Reactor();

	Reactor(const Reactor&) = delete;
	Reactor& operator=(const Reactor&) = delete;

	Reactor(Reactor&&) = delete;
	Reactor& operator=(Reactor&&) = delete;

	/**
	 *  @throw std::system_error If the %Reactor could not be initialized.
	 */
	void init();
	void run();
	void shutdown();

	bool supportsIoCalls() const { return m_supportsIoCalls; }
	bool supportsTimedCalls() const { return m_supportsTimedCalls; }
	bool supportsLoopCalls() const { return m_supportsLoopCalls; }

	std::uint64_t commands_per_iteration() const
	{
		return commands_per_iteration_;
	}
	void commands_per_iteration(std::uint64_t value)
	{
		commands_per_iteration_ = value;
	}

	CallId callOnRead(int, const IOCall&);
	CallId callOnWrite(int, const IOCall&);
	CallId callOnException(int, const IOCall&);

	CallId callEvery(std::chrono::microseconds, const RecurringCall&);
	CallId callAfter(std::chrono::microseconds, const SingleCall&);
	CallId callAt(std::chrono::system_clock::time_point, const SingleCall&);

	CallId callInLoop(const RecurringCall&);

	void callLater(const SingleCall&);
	void callNow(const SingleCall&);

	void removeCall(CallId&);

	void idle(bool workDone);

private:
	using Command = std::function<void()>;

	class CallIdGenerator {
	public:
		explicit CallIdGenerator(uint64_t);

		CallId next();

	private:
		uint64_t m_value;
	};

	class IOCallback {
	public:
		IOCallback(CallId, int, const IOCall&, uint32_t flag);

		CallId id() const;
		int fd() const;
		uint32_t flag() const;
		CallStatus operator()(int) const;

		bool removed() const;
		void setRemoved();

	private:
		CallId m_id;
		int m_fd;
		IOCall m_call;
		uint32_t m_flag;
		bool m_removed {false};
	};

	class TimedCallback {
	public:
		TimedCallback(CallId, std::chrono::microseconds, const RecurringCall&);

		CallId id() const;
		std::chrono::microseconds timeout() const;
		CallStatus operator()() const;

	private:
		CallId m_id;
		std::chrono::microseconds m_timeout;
		RecurringCall m_call;
	};

	class LoopCallback {
	public:
		LoopCallback(CallId, const RecurringCall&);

		CallId id() const;
		CallStatus operator()() const;
		bool removed() const;
		void setRemoved();

	private:
		CallId m_id;
		RecurringCall m_call;
		bool m_removed {false};
	};

	const bool m_supportsIoCalls;
	const bool m_supportsTimedCalls;
	const bool m_supportsLoopCalls;

	static constexpr uint64_t CALLID_READ_BASE = 1;
	static constexpr uint64_t CALLID_WRITE_BASE = 2;
	static constexpr uint64_t CALLID_EXCEPTION_BASE = 3;
	static constexpr uint64_t CALLID_TIMED_BASE = 0;
	static constexpr uint64_t CALLID_LOOP_BASE = 4;
	static constexpr uint64_t CALLID_STEP = 5;

	signal_system_error_type m_signalSystemError;
	idle_strategy m_idleStrategy;
	std::thread::id m_threadId;
	Atomic_queue<Command, 1024u> commands_queue_ {3};
	CallIdGenerator m_readCallIdGenerator;
	CallIdGenerator m_writeCallIdGenerator;
	CallIdGenerator m_exceptionCallIdGenerator;
	CallIdGenerator m_timedCallIdGenerator;
	CallIdGenerator m_loopCallIdGenerator;

	struct EpollCallback {
		uint32_t eventsFlag;
		std::vector<IOCallback> callbacks {};
		std::uint32_t inCount {0};
		std::uint32_t outCount {0};
		std::uint32_t exceptCount {0};

		explicit EpollCallback(uint32_t flag)
			: eventsFlag {flag}
		{
			increment(flag);
		};

		EpollCallback(uint32_t flag, int fd, CallId callId, const IOCall* call)
			: eventsFlag {flag}
		{
			increment(flag);
			addCallback(flag, fd, callId, call);
		};

		void addCallback(
			uint32_t flag, int fd, CallId callId, const IOCall* call)
		{
			if (call != nullptr)
				callbacks.emplace_back(callId, fd, *call, flag);
		}

		bool increment(uint32_t flag)
		{
			auto counter = getCounter(flag);
			if (counter == nullptr)
				return false;
			if (*counter == 0) {
				eventsFlag = (eventsFlag | flag);
				++(*counter);
				return true;
			}
			++(*counter);
			return false;
		}

		bool decrement(uint32_t flag)
		{
			auto counter = getCounter(flag);
			if (counter == nullptr || *counter == 0)
				return false;
			--(*counter);
			if (*counter == 0) {
				eventsFlag = (eventsFlag & ~flag);
				return true;
			}
			return false;
		}

		uint32_t* getCounter(uint32_t flag)
		{
			switch (flag) {
			case EPOLLIN:
				return &inCount;
			case EPOLLOUT:
				return &outCount;
			case EPOLLPRI:
				return &exceptCount;
			}
			return nullptr;
		}

		std::uint32_t count() const { return inCount + outCount + exceptCount; }
	};

	std::map<int, EpollCallback> m_fdToEpollCallbacks;
	std::multimap<std::chrono::system_clock::time_point, TimedCallback>
		m_timedCallbacks;
	std::vector<std::pair<std::chrono::system_clock::time_point, TimedCallback>>
		m_timedCallbackReinsertions;
	CallId m_runningCallId;
	std::vector<LoopCallback> m_loopCallbacks;
	int m_epfd;
	std::uint64_t commands_per_iteration_ {0u};
	ALIGN_DESTRUCTIVE std::atomic<bool> consumed_ {false};

	void iterate();
	void addCommand(Command&&);
	void executeCommands();
	uint32_t getEpollFlag(uint32_t epollEvents);
	bool processEpollCallback(EpollCallback&, uint32_t flag, bool& workDone);
	void dispatchIoCalls(bool& workDone);
	void dispatchTimedCalls(bool& workDone);
	void dispatchLoopCalls(bool& workDone);
	void addFlag(int, uint32_t, CallId, const IOCall*);
	void addTimedCall(std::chrono::system_clock::time_point, TimedCallback&&);
	void addLoopCall(LoopCallback&&);
	void removeIOCall(CallId, uint32_t);
	void removeFlag(int, uint32_t);
	void removeTimedCall(CallId);
	void removeLoopCall(CallId);
	void clearIoCallbacks();
};

} // namespace misc

#endif // MISC_REACTOR_H
