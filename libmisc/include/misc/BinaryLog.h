#ifndef MISC_BINARYLOG_H
#define MISC_BINARYLOG_H

#include <sys/types.h>

#include <cstddef>
#include <cstdio>
#include <string>
#include <vector>

#include "Reactor.h"

namespace misc {

class BinaryMessageView {
public:
	BinaryMessageView(void* data, size_t size)
		: m_data(data)
		, m_size(size)
	{
	}

	void* data() { return m_data; }

	const void* data() const { return m_data; }

	size_t size() const { return m_size; }

	bool empty() const { return m_size == 0; }

	void resize(size_t size) { m_size = size; }

private:
	void* m_data;
	size_t m_size;
};

class ConstBinaryMessageView {
public:
	ConstBinaryMessageView(const void* data, size_t size)
		: m_data(data)
		, m_size(size)
	{
	}

	ConstBinaryMessageView(const BinaryMessageView& message)
		: ConstBinaryMessageView(message.data(), message.size())
	{
	}

	const void* data() const { return m_data; }

	size_t size() const { return m_size; }

	bool empty() const { return m_size == 0; }

private:
	const void* m_data;
	size_t m_size;
};

struct BinaryLogPosition {
	size_t messageNo;
	off_t fileOffset;
};

namespace details {

class BinaryLogFile {
public:
	void open(const std::string&, const char*);
	void close();

	void rewind();
	bool seek(size_t, const BinaryLogPosition&, int&);
	bool read(BinaryMessageView&, int&);
	bool write(const ConstBinaryMessageView&, int&);
	void flush();

private:
	FILE* m_file {nullptr};
};

} // namespace details

class BinaryLogIndex {
public:
	BinaryLogIndex(size_t, size_t);

	void add(const ConstBinaryMessageView&);
	const BinaryLogPosition* find(size_t) const;
	bool empty() const;
	void clear();

private:
	size_t m_blockSize;
	BinaryLogPosition m_nextPosition;
	std::vector<BinaryLogPosition> m_index;

	size_t blockOfMessage(size_t) const;
	size_t startOfBlock(size_t) const;
};

class BinaryLogReader {
public:
	explicit BinaryLogReader(Reactor&);

	void open(const std::string&, const BinaryLogIndex&);
	void close();

	template <typename Messages, typename CompletionHandler>
	void read(Messages&& messages, CompletionHandler&& ch)
	{
		m_reactor->callLater(
			[this, messages = std::move(messages), ch = std::move(ch)] {
				int error = 0;
				auto messagesRead = read(messages, error);
				ch(messagesRead, error);
			});
	}

	template <typename Messages, typename CompletionHandler>
	void read(size_t messageNo, Messages&& messages, CompletionHandler&& ch)
	{
		m_reactor->callLater([this, messageNo, messages = std::move(messages),
								 ch = std::move(ch)] {
			auto hint = m_index->find(messageNo);
			if (hint == nullptr) {
				ch(0, EIO);
				return;
			}
			int error;
			if (!m_log.seek(messageNo, *hint, error)) {
				ch(0, error);
				return;
			}
			error = 0;
			auto messagesRead = read(messages, error);
			ch(messagesRead, error);
		});
	}

private:
	Reactor* m_reactor;
	const BinaryLogIndex* m_index {nullptr};
	details::BinaryLogFile m_log;

	template <typename Messages>
	size_t read(const Messages& messages, int& error)
	{
		size_t count = 0;
		for (auto& m : messages) {
			BinaryMessageView message(m.data(), m.size());
			if (!m_log.read(message, error))
				break;
			m.resize(message.size());
			++count;
		}
		return count;
	}
};

class BinaryLogWriter {
public:
	explicit BinaryLogWriter(Reactor&);

	void open(const std::string&, BinaryLogIndex&, bool = false);
	void close();

	template <typename Messages, typename CompletionHandler>
	void write(Messages&& messages, CompletionHandler&& ch)
	{
		m_reactor->callLater(
			[this, messages = std::move(messages), ch = std::move(ch)] {
				int error = 0;
				auto messagesWritten = write(messages, error);
				ch(messagesWritten, error);
			});
	}

	void flush();

private:
	Reactor* m_reactor;
	BinaryLogIndex* m_index {nullptr};
	details::BinaryLogFile m_log;

	template <typename Messages>
	size_t write(const Messages& messages, int& error)
	{
		size_t count = 0;
		for (auto& m : messages) {
			ConstBinaryMessageView message(m.data(), m.size());
			if (!m_log.write(message, error))
				break;
			m_index->add(message);
			++count;
		}
		return count;
	}
};

} // namespace misc

#endif // MISC_BINARYLOG_H
