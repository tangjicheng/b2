#ifndef MISC_JSON_H
#define MISC_JSON_H

#include <cctype>
#include <iterator>
#include <ostream>
#include <string>
#include <type_traits>
#include <utility>

#include "indent.h"

namespace misc { namespace json {

namespace details {

template <typename T>
using ValueType = std::remove_const_t<std::remove_reference_t<T>>;

template <typename T>
using IsCharacter = std::is_same<T, char>;

template <typename T>
using IsSmallInteger = std::bool_constant<
	std::is_same_v<T, unsigned char> || std::is_same_v<T, signed char>>;

template <typename T>
using IsCString = std::bool_constant<
	std::is_same_v<T, char*> || std::is_same_v<T, const char*>>;

std::false_type isSequenceHelper(...);

template <typename U>
auto isSequenceHelper(const U& u)
	-> decltype(std::begin(u), std::end(u), std::true_type {});

template <typename T>
using IsSequence = decltype(isSequenceHelper(std::declval<T>()));

template <typename T>
using SequenceValueType
	= decltype(*std::begin(std::declval<std::add_lvalue_reference_t<T>>()));

std::false_type isCharacterSequenceHelper(...);

template <typename U>
std::enable_if_t<IsSequence<U> {}, IsCharacter<ValueType<SequenceValueType<U>>>>
isCharacterSequenceHelper(const U&);

template <typename T>
using IsCharacterSequence = decltype(isCharacterSequenceHelper(
	std::declval<std::add_lvalue_reference_t<T>>()));

template <typename T>
using IsQuotable = std::bool_constant<IsCharacter<T> {} || IsCString<T> {}
	|| IsCharacterSequence<T> {}>;

std::false_type isPairHelper(...);

template <typename First, typename Second>
std::true_type isPairHelper(const std::pair<First, Second>&);

template <typename T>
using IsPair = decltype(isPairHelper(std::declval<T>()));

class quote_proxy {
public:
	struct tag {
		constexpr tag() {}
	};

	explicit quote_proxy(std::ostream& os)
		: m_os(os)
	{
	}

	template <typename T>
	std::ostream& operator<<(const T& rhs) const
	{
		m_os.put(QUOTE);
		encode(rhs);
		m_os.put(QUOTE);
		return m_os;
	}

private:
	std::ostream& m_os;

	static constexpr char QUOTE = '\"';
	static constexpr char ESCAPE = '\\';

	void putHexDigit(int value) const
	{
		if (value < 10)
			m_os.put(value + '0');
		else
			m_os.put(value - 10 + 'A');
	}

	void putEscapedCharacter(int c) const
	{
		if (iscntrl(c)) {
			m_os.put(ESCAPE);
			switch (c) {
			case '\b':
				m_os.put('b');
				break;
			case '\t':
				m_os.put('t');
				break;
			case '\n':
				m_os.put('n');
				break;
			case '\f':
				m_os.put('f');
				break;
			case '\r':
				m_os.put('r');
				break;
			default:
				m_os.put('u');
				putHexDigit((c & 0xF000) >> 12);
				putHexDigit((c & 0xF00) >> 8);
				putHexDigit((c & 0xF0) >> 4);
				putHexDigit(c & 0xF);
				break;
			}
		}
		else if (c == QUOTE || c == ESCAPE) {
			m_os.put(ESCAPE);
			m_os.put(c);
		}
		else
			m_os.put(c);
	}

	template <typename T>
	std::enable_if_t<IsCharacter<T> {}> encode(T rhs) const
	{
		putEscapedCharacter(rhs);
	}

	void encode(const char* rhs) const
	{
		while (*rhs != '\0')
			putEscapedCharacter(*rhs++);
	}

	template <typename T>
	std::enable_if_t<IsCharacterSequence<T> {}> encode(const T& rhs) const
	{
		for (auto c : rhs)
			putEscapedCharacter(c);
	}
};

inline quote_proxy operator<<(std::ostream& os, const quote_proxy::tag&)
{
	return quote_proxy(os);
}

template <typename T, template <typename...> class ContainerTraits>
struct HasContainerTraits {
	static std::false_type helper(...);

	template <typename U>
	static auto helper(const U&)
		-> decltype(&ContainerTraits<U>::inject, std::true_type {});

	using type = decltype(helper(std::declval<T>()));
};

template <typename T, template <typename...> class ContainerTraits>
using HasContainerTraitsT =
	typename HasContainerTraits<T, ContainerTraits>::type;

template <char OPEN, char CLOSE>
class Container {
public:
	std::ostream& os() const { return m_os; }
	unsigned int indent() const { return m_indent; }

protected:
	Container(std::ostream& os, unsigned int indent)
		: m_os(os)
		, m_indent(indent + 1)
		, m_leader(OPEN)
	{
	}

	~Container()
	{
		if (m_leader == OPEN)
			m_os << OPEN << CLOSE;
		else
			m_os << EOL << misc::indent(m_indent - 1) << CLOSE;
	}

	Container(const Container&) = delete;
	Container& operator=(const Container&) = delete;

	Container(Container&&) = delete;
	Container& operator=(Container&&) = delete;

	void line()
	{
		m_os << m_leader << EOL << misc::indent(m_indent);
		m_leader = SEPARATOR;
	}

private:
	std::ostream& m_os;
	unsigned int m_indent;
	char m_leader;

	static constexpr char EOL = '\n';
	static constexpr char SEPARATOR = ',';
};

struct null_tag {
	constexpr null_tag() {}
};

} // namespace details

constexpr details::quote_proxy::tag quote;

template <typename T, typename Enable = void>
struct ValueTraits;

template <typename T, typename Enable = void>
struct ObjectTraits;

template <typename T, typename Enable = void>
struct ArrayTraits;

class Object : public details::Container<'{', '}'> {
public:
	explicit Object(std::ostream& os, unsigned int indent = 0)
		: Container(os, indent)
	{
	}

	template <typename T>
	void entry(const std::string& key, T&& t)
	{
		line();
		os() << quote << key << ": ";
		ValueTraits<details::ValueType<T>>::encode(*this, std::forward<T>(t));
	}

	template <typename T>
	void inject(T&& t)
	{
		ObjectTraits<details::ValueType<T>>::inject(*this, std::forward<T>(t));
	}
};

class Array : public details::Container<'[', ']'> {
public:
	explicit Array(std::ostream& os, unsigned int indent = 0)
		: Container(os, indent)
	{
	}

	template <typename T>
	void entry(T&& t)
	{
		line();
		ValueTraits<details::ValueType<T>>::encode(*this, std::forward<T>(t));
	}

	template <typename T>
	void inject(T&& t)
	{
		ArrayTraits<details::ValueType<T>>::inject(*this, std::forward<T>(t));
	}
};

namespace details {

template <typename T>
using EncodeAsString = IsQuotable<T>;

template <typename T>
using EncodeAsSmallInteger = IsSmallInteger<T>;

template <typename T>
using EncodeAsInteger = std::bool_constant<
	std::is_integral_v<T> && !std::is_same_v<T, bool> && !IsQuotable<T> {}
	&& !IsSmallInteger<T> {}>;

template <typename T>
using EncodeObjectAsValue = HasContainerTraitsT<T, ObjectTraits>;

template <typename T>
using EncodeArrayAsValue
	= std::bool_constant<HasContainerTraitsT<T, ArrayTraits> {}
		&& !IsQuotable<T> {}>;

template <typename T>
using InjectIntoArray
	= std::bool_constant<IsSequence<T> {} && !IsPair<SequenceValueType<T>> {}>;

template <typename T>
using InjectIntoObject
	= std::bool_constant<IsSequence<T> {} && IsPair<SequenceValueType<T>> {}>;

} // namespace details

template <typename T>
struct ValueTraits<T, std::enable_if_t<details::EncodeAsString<T> {}>> {
	template <typename Container>
	static void encode(const Container& c, const T& t)
	{
		c.os() << quote << t;
	}
};

template <typename T>
struct ValueTraits<T, std::enable_if_t<details::EncodeAsSmallInteger<T> {}>> {
	template <typename Container>
	static void encode(const Container& c, const T& t)
	{
		c.os() << static_cast<int>(t);
	}
};

template <typename T>
struct ValueTraits<T, std::enable_if_t<details::EncodeAsInteger<T> {}>> {
	template <typename Container>
	static void encode(const Container& c, const T& t)
	{
		c.os() << t;
	}
};

// Encode bool as true or false.
template <>
struct ValueTraits<bool> {
	template <typename Container>
	static void encode(const Container& c, bool b)
	{
		c.os() << (b ? "true" : "false");
	}
};

constexpr details::null_tag null;

// Encode null type as null.
template <>
struct ValueTraits<details::null_tag> {
	template <typename Container>
	static void encode(const Container& c, const details::null_tag&)
	{
		c.os() << "null";
	}
};

template <typename T>
struct ValueTraits<T, std::enable_if_t<details::EncodeObjectAsValue<T> {}>> {
	template <typename Container>
	static void encode(const Container& c, const T& t)
	{
		Object(c.os(), c.indent()).inject(t);
	}
};

template <typename T>
struct ValueTraits<T, std::enable_if_t<details::EncodeArrayAsValue<T> {}>> {
	template <typename Container>
	static void encode(const Container& c, const T& t)
	{
		Array(c.os(), c.indent()).inject(t);
	}
};

template <typename T>
struct ArrayTraits<T, std::enable_if_t<details::InjectIntoArray<T> {}>> {
	static void inject(Array& a, const T& t)
	{
		for (auto& entry : t)
			a.entry(entry);
	}
};

template <typename T>
struct ObjectTraits<T, std::enable_if_t<details::InjectIntoObject<T> {}>> {
	static void inject(Object& o, const T& t)
	{
		for (auto& entry : t)
			o.entry(entry.first, entry.second);
	}
};

}} // namespace misc::json

#endif // MISC_JSON_H
