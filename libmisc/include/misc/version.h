#ifndef MISC_VERSION_H
#define MISC_VERSION_H

namespace misc {

struct Version {
	constexpr Version(const char* number, const char* commit_id,
		const char* build_user, const char* build_host, const char* build_path,
		const char* build_date) noexcept
		: number(number)
		, commit_id(commit_id)
		, build_user(build_user)
		, build_host(build_host)
		, build_path(build_path)
		, build_date(build_date)
	{
	}

	const char* number;
	const char* commit_id;
	const char* build_user;
	const char* build_host;
	const char* build_path;
	const char* build_date;
};

} // namespace misc

#endif // MISC_VERSION_H
