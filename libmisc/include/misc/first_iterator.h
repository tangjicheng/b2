#ifndef MISC_FIRST_ITERATOR_H
#define MISC_FIRST_ITERATOR_H

#include "nth_iterator.h"
#include "range.h" // first_iterator.h guarantees to provide range.h.

namespace misc {

template <typename Iterator>
using first_iterator = nth_iterator<0, Iterator>;

template <typename Iterator>
first_iterator<Iterator> make_first_iterator(const Iterator& i)
{
	return first_iterator<Iterator>(i);
}

template <typename Iterator>
range<first_iterator<Iterator>> first(const Iterator& b, const Iterator& e)
{
	return range<first_iterator<Iterator>>(b, e);
}

template <typename Range>
auto first(Range& r) -> range<first_iterator<decltype(std::begin(r))>>
{
	return decltype(first(r))(std::begin(r), std::end(r));
}

template <typename Range>
auto first(const Range& r) -> range<first_iterator<decltype(std::begin(r))>>
{
	return decltype(first(r))(std::begin(r), std::end(r));
}

} // namespace misc

#endif // MISC_FIRST_ITERATOR_H
