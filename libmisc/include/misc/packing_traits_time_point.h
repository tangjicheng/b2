#ifndef MISC_PACKING_TRAITS_TIME_POINT_H
#define MISC_PACKING_TRAITS_TIME_POINT_H

#include <chrono>

#include "Record.h"
#include "packing_traits.h"

namespace misc {

template <typename Clock, typename Duration>
struct PackingTraits<std::chrono::time_point<Clock, Duration>>
	: FixedPackingTraits<sizeof(typename Duration::rep)> {
	static Record<typename Duration::rep> r;

	class Packer : public FixedPacker {
	public:
		Packer(std::chrono::time_point<Clock, Duration> time_point)
			: time_point_ {time_point}
		{
		}

		void packFixed(void*& p) const
		{
			const auto count = time_point_.time_since_epoch().count();
			p = r.packUnsafe(p, count);
		}

	private:
		std::chrono::time_point<Clock, Duration> time_point_;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(std::chrono::time_point<Clock, Duration>& time_point)
			: time_point_ {time_point}
		{
		}

		void unpackFixed(const void*& p) const
		{
			typename Duration::rep count;
			p = r.unpackUnsafe(p, count);
			time_point_
				= std::chrono::time_point<Clock, Duration> {Duration {count}};
		}

	private:
		std::chrono::time_point<Clock, Duration>& time_point_;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

} // namespace misc

#endif // MISC_PACKING_TRAITS_TIME_POINT_H
