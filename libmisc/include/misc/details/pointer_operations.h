#ifndef MISC_DETAILS_POINTER_OPERATIONS_H
#define MISC_DETAILS_POINTER_OPERATIONS_H

#include <cstddef>
#include <cstdint>
#include <stdexcept>

namespace misc { namespace details {

inline const void* pointer_addition(const void* lhs, size_t rhs)
{
	return (const uint8_t*)lhs + rhs;
}

inline void* pointer_addition(void* lhs, size_t rhs)
{
	return (uint8_t*)lhs + rhs;
}

inline ptrdiff_t pointer_subtraction(const void* lhs, const void* rhs)
{
	return (const uint8_t*)lhs - (const uint8_t*)rhs;
}

inline char get_char(const void* p, size_t i = 0)
{
	return ((const char*)p)[i];
}

inline void put_char(void* p, char c, size_t i = 0)
{
	((char*)p)[i] = c;
}

/**
 *  @throw std::invalid_argument If the character pointed to by @a p is not a
 *  digit.
 */
template <typename T>
T get_digit(const void* p)
{
	const char c = get_char(p);
	if (c < '0' || c > '9')
		throw std::invalid_argument("Not a digit");
	return c - '0';
}

template <typename T>
void put_digit(void* p, T value)
{
	put_char(p, value + '0');
}

}} // namespace misc::details

#endif // MISC_DETAILS_POINTER_OPERATIONS_H
