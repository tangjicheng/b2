#ifndef MISC_RECORD_H
#define MISC_RECORD_H

#include <cstddef>
#include <stdexcept>
#include <tuple>
#include <type_traits>
#include <utility>

#include <meta/meta.hpp>

#include "details/pointer_operations.h"
#include "packing_traits.h"

namespace misc {

template <typename... Types>
auto group(Types&&... types)
	-> decltype(std::forward_as_tuple(std::forward<Types>(types)...))
{
	return std::forward_as_tuple(std::forward<Types>(types)...);
}

template <typename... Fields>
class Record {
	static_assert(meta::all_of<meta::list<Fields...>,
					  meta::quote<details::AssertPackingTraits>> {},
		"All Fields must have PackingTraits specialisations");
	static_assert(sizeof...(Fields) != 0, "Empty record");

private:
	using FieldIsVar
		= meta::compose<meta::quote<meta::not_>, meta::quote<details::IsFixed>>;
	using AllFields = meta::list<Fields...>;
	using VarFields = meta::filter<AllFields, FieldIsVar>;

	static constexpr size_t packVarSize(meta::list<>) { return 0; }

	template <typename Field, typename... RemainingFields>
	static constexpr size_t packVarSize(meta::list<Field, RemainingFields...>,
		const details::PackParam<Field>& param,
		const details::PackParam<RemainingFields>&... params)
	{
		return details::Packer<Field>(param).varSize()
			+ packVarSize(meta::list<RemainingFields...> {}, params...);
	}

public:
	static constexpr size_t minSize() { return details::FixedSize<Record> {}; }

	static constexpr size_t maxSize() { return details::MaxSize<Record> {}; }

	template <typename... Args>
	static constexpr size_t size(const Args&... args)
	{
		static_assert(sizeof...(Args) == meta::size<AllFields> {}
				|| sizeof...(Args) == meta::size<VarFields> {},
			"Incorrect number of arguments");
		using CallFields
			= std::conditional_t<sizeof...(Args) == meta::size<AllFields> {},
				AllFields, VarFields>;
		return details::FixedSize<Record>::value
			+ packVarSize(CallFields {}, args...);
	}

	template <typename T>
	static T* packUnsafe(T* tp, const details::PackParam<Fields>&... params)
	{
		void* p = tp;
		details::Packer<Record> packer(params...);
		packer.packFixed(p);
		packer.packVar(p);
		return static_cast<T*>(p);
	}

	static size_t pack(
		void* p, size_t len, const details::PackParam<Fields>&... params)
	{
		auto bytesRequired = details::FixedSize<Record>::value
			+ packVarSize(AllFields {}, params...);
		if (len < bytesRequired)
			return 0;
		packUnsafe(p, params...);
		return bytesRequired;
	}

	template <typename T>
	static const T* unpackUnsafe(
		const T* tp, const details::UnpackParam<Fields>&... params)
	{
		const void* p = tp;
		details::Unpacker<Record> unpacker(params...);
		unpacker.unpackFixed(p);
		unpacker.unpackVar(p);
		return static_cast<const T*>(p);
	}

	static size_t unpack(const void* p, size_t len,
		const details::UnpackParam<Fields>&... params)
	{
		if (len < details::FixedSize<Record>::value)
			return 0;
		auto bytesUsed = details::FixedSize<Record>::value
			+ details::Unpacker<Record>::varSize(p);
		if (len < bytesUsed)
			return 0;
		unpackUnsafe(p, params...);
		return bytesUsed;
	}
};

} // namespace misc

#endif // MISC_RECORD_H
