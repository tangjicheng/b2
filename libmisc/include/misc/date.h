#ifndef MISC_DATE_H
#define MISC_DATE_H

#include <cstdint>
#include <istream>
#include <ostream>

#include "pow.h"

namespace misc {

class Date {
public:
	using Underlying_type = uint32_t;

	Date() = default;

	explicit Date(Underlying_type yyyymmdd)
		: date_ {yyyymmdd}
	{
	}

	Date(Underlying_type year, Underlying_type month, Underlying_type day)
		: date_ {year * 10000 + month * 100 + day}
	{
	}

	Underlying_type yyyymmdd() const { return date_; }

	Underlying_type year() const { return date_ / 10000; }
	Underlying_type month() const { return date_ / 100 % 100; }
	Underlying_type day() const { return date_ % 100; }

private:
	Underlying_type date_ {19700101};
};

inline bool operator==(Date lhs, Date rhs)
{
	return lhs.yyyymmdd() == rhs.yyyymmdd();
}

inline bool operator!=(Date lhs, Date rhs)
{
	return lhs.yyyymmdd() != rhs.yyyymmdd();
}

inline bool operator<(Date lhs, Date rhs)
{
	return lhs.yyyymmdd() < rhs.yyyymmdd();
}

inline bool operator>(Date lhs, Date rhs)
{
	return lhs.yyyymmdd() > rhs.yyyymmdd();
}

inline bool operator<=(Date lhs, Date rhs)
{
	return lhs.yyyymmdd() <= rhs.yyyymmdd();
}

inline bool operator>=(Date lhs, Date rhs)
{
	return lhs.yyyymmdd() >= rhs.yyyymmdd();
}

std::ostream& operator<<(std::ostream&, const Date&);
std::istream& operator>>(std::istream&, Date&);

Date get_date(const std::string& date);

} // namespace misc

#endif // MISC_DATE_H
