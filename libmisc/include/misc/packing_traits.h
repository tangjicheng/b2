#ifndef MISC_PACKING_TRAITS_H
#define MISC_PACKING_TRAITS_H

#include <endian.h>

#include <array>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>

#include <meta/meta.hpp>

#include "FlagSet.h"
#include "external_reference.h"
#include "inject.h"
#include "numeric_type.h"
#include "object_id.h"

namespace misc {

template <typename Field, typename Enable = void>
struct PackingTraits;

namespace details {

template <typename Field>
using IsFixed = typename PackingTraits<Field>::IsFixed;

template <typename Field>
using FixedSize
	= std::integral_constant<size_t, PackingTraits<Field>::fixedSize>;

template <typename Field>
using MaxSize = std::integral_constant<size_t, PackingTraits<Field>::maxSize>;

template <typename Field>
using PackParam = typename PackingTraits<Field>::PackParam;

template <typename Field>
using UnpackParam = typename PackingTraits<Field>::UnpackParam;

template <typename Field>
using Packer = typename PackingTraits<Field>::Packer;

template <typename Field>
using Unpacker = typename PackingTraits<Field>::Unpacker;

inline void pack_one(void*& p, char c)
{
	memcpy(p, &c, sizeof(c));
	p = details::pointer_addition(p, sizeof(c));
}

inline void pack_one(void*& p, uint8_t i)
{
	memcpy(p, &i, sizeof(i));
	p = details::pointer_addition(p, sizeof(i));
}

inline void pack_one(void*& p, int8_t i)
{
	pack_one(p, static_cast<uint8_t>(i));
}

inline void pack_one(void*& p, uint16_t i)
{
	i = htobe16(i);
	memcpy(p, &i, sizeof(i));
	p = details::pointer_addition(p, sizeof(i));
}

inline void pack_one(void*& p, int16_t i)
{
	pack_one(p, static_cast<uint16_t>(i));
}

inline void pack_one(void*& p, uint32_t i)
{
	i = htobe32(i);
	memcpy(p, &i, sizeof(i));
	p = details::pointer_addition(p, sizeof(i));
}

inline void pack_one(void*& p, int32_t i)
{
	pack_one(p, static_cast<uint32_t>(i));
}

inline void pack_one(void*& p, uint64_t i)
{
	i = htobe64(i);
	memcpy(p, &i, sizeof(i));
	p = details::pointer_addition(p, sizeof(i));
}

inline void pack_one(void*& p, int64_t i)
{
	pack_one(p, static_cast<uint64_t>(i));
}

inline void read_one(const void* p, char& c)
{
	memcpy(&c, p, sizeof(c));
}

inline void read_one(const void* p, uint8_t& i)
{
	memcpy(&i, p, sizeof(i));
}

inline void read_one(const void* p, int8_t& i)
{
	read_one(p, reinterpret_cast<uint8_t&>(i));
}

inline void read_one(const void* p, uint16_t& i)
{
	memcpy(&i, p, sizeof(i));
	i = be16toh(i);
}

inline void read_one(const void* p, int16_t& i)
{
	read_one(p, reinterpret_cast<uint16_t&>(i));
}

inline void read_one(const void* p, uint32_t& i)
{
	memcpy(&i, p, sizeof(i));
	i = be32toh(i);
}

inline void read_one(const void* p, int32_t& i)
{
	read_one(p, reinterpret_cast<uint32_t&>(i));
}

inline void read_one(const void* p, uint64_t& i)
{
	memcpy(&i, p, sizeof(i));
	i = be64toh(i);
}

inline void read_one(const void*& p, int64_t& i)
{
	read_one(p, reinterpret_cast<uint64_t&>(i));
}

inline void unpack_one(const void*& p, char& c)
{
	read_one(p, c);
	p = details::pointer_addition(p, sizeof(c));
}

inline void unpack_one(const void*& p, uint8_t& i)
{
	read_one(p, i);
	p = details::pointer_addition(p, sizeof(i));
}

inline void unpack_one(const void*& p, int8_t& i)
{
	unpack_one(p, reinterpret_cast<uint8_t&>(i));
}

inline void unpack_one(const void*& p, uint16_t& i)
{
	read_one(p, i);
	p = details::pointer_addition(p, sizeof(i));
}

inline void unpack_one(const void*& p, int16_t& i)
{
	unpack_one(p, reinterpret_cast<uint16_t&>(i));
}

inline void unpack_one(const void*& p, uint32_t& i)
{
	read_one(p, i);
	p = details::pointer_addition(p, sizeof(i));
}

inline void unpack_one(const void*& p, int32_t& i)
{
	unpack_one(p, reinterpret_cast<uint32_t&>(i));
}

inline void unpack_one(const void*& p, uint64_t& i)
{
	read_one(p, i);
	p = details::pointer_addition(p, sizeof(i));
}

inline void unpack_one(const void*& p, int64_t& i)
{
	unpack_one(p, reinterpret_cast<uint64_t&>(i));
}

std::false_type hasPackOneHelper(...);

template <typename Field>
auto hasPackOneHelper(const Field& field)
	-> decltype(pack_one(std::declval<void*&>(), field), std::true_type {});

template <typename Field>
using HasPackOne = decltype(hasPackOneHelper(std::declval<Field>()));

std::false_type hasPackingTraitsHelper(...);

template <typename Field>
auto hasPackingTraitsHelper(const Field& field)
	-> decltype(typename PackingTraits<Field>::IsFixed {}, std::true_type {});

template <typename Field>
using HasPackingTraits
	= decltype(hasPackingTraitsHelper(std::declval<Field>()));

template <typename Field>
struct AssertPackingTraits : HasPackingTraits<Field> {
	static_assert(HasPackingTraits<Field> {},
		"Field must have PackingTraits specialisation");
};

} // namespace details

template <size_t Size>
struct FixedPackingTraits {
	using IsFixed = std::true_type;
	static constexpr size_t fixedSize = Size;
	static constexpr size_t maxSize = fixedSize;
};

class FixedPacker {
public:
	static void packVar(void*&) {}

	static constexpr size_t varSize() { return 0; }
};

class FixedUnpacker {
public:
	static void unpackVar(const void*&) {}

	static constexpr size_t varSize(const void*) { return 0; }
};

template <size_t Size>
class FixedMemoryPacker : public FixedPacker {
public:
	constexpr FixedMemoryPacker(const void* data)
		: m_data(data)
	{
	}

	void packFixed(void*& p) const
	{
		memcpy(p, m_data, Size);
		p = details::pointer_addition(p, Size);
	}

private:
	const void* m_data;
};

template <size_t Size>
class FixedMemoryUnpacker : public FixedUnpacker {
public:
	FixedMemoryUnpacker(void* data)
		: m_data(data)
	{
	}

	void unpackFixed(const void*& p) const
	{
		memcpy(m_data, p, Size);
		p = details::pointer_addition(p, Size);
	}

private:
	void* m_data;
};

template <size_t MaxVarSize, typename SizeField>
struct VarPackingTraits {
	static_assert(std::numeric_limits<SizeField>::max() >= MaxVarSize,
		"VarPackingTraits' SizeField type must be wide enough for MaxVarSize");

	using IsFixed = std::false_type;
	static constexpr size_t fixedSize = sizeof(SizeField);
	static constexpr size_t maxSize = fixedSize + MaxVarSize;
};

template <size_t MaxVarSize, typename SizeField>
class VarPacker {
	static_assert(std::numeric_limits<SizeField>::max() >= MaxVarSize,
		"VarPacker's SizeField type must be wide enough for MaxVarSize");

public:
	VarPacker(size_t varSize)
	{
		if (varSize > MaxVarSize)
			throw std::invalid_argument("Too long");
		m_varSize = varSize;
	}

	void packFixed(void*& p) const
	{
		auto varSize = static_cast<SizeField>(m_varSize);
		details::pack_one(p, varSize);
	}

	size_t varSize() const { return m_varSize; }

private:
	size_t m_varSize;
};

template <size_t MaxVarSize, typename SizeField>
class VarUnpacker {
	static_assert(std::numeric_limits<SizeField>::max() >= MaxVarSize,
		"VarUnpacker's SizeField type must be wide enough for MaxVarSize");

public:
	void unpackFixed(const void*& p)
	{
		SizeField varSize;
		details::unpack_one(p, varSize);
		if (varSize > MaxVarSize)
			throw std::invalid_argument("Field too long");
		m_varSize = static_cast<size_t>(varSize);
	}

	static size_t varSize(const void* p)
	{
		SizeField varSize;
		details::read_one(p, varSize);
		if (varSize > MaxVarSize)
			throw std::invalid_argument("Field too long");
		return static_cast<size_t>(varSize);
	}

protected:
	size_t varSize() const { return m_varSize; }

private:
	size_t m_varSize;
};

template <typename Field>
struct PackingTraits<Field, std::enable_if_t<details::HasPackOne<Field> {}>>
	: FixedPackingTraits<sizeof(Field)> {
	class Packer : public FixedPacker {
	public:
		constexpr Packer(Field field)
			: m_field(field)
		{
		}

		void packFixed(void*& p) const { details::pack_one(p, m_field); }

	private:
		Field m_field;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(Field& field)
			: m_field(field)
		{
		}

		void unpackFixed(const void*& p) const
		{
			details::unpack_one(p, m_field);
		}

	private:
		Field& m_field;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <unsigned int Length, char Pad = ' '>
struct Number;

template <unsigned int Length, char Pad>
struct PackingTraits<Number<Length, Pad>> : FixedPackingTraits<Length> {
	static_assert(Length != 0, "Zero length");

	class Packer : public FixedPacker {
	public:
		constexpr Packer(uint64_t val)
			: m_val(val)
		{
		}

		void packFixed(void*& p) const
		{
			inject<Length, uint64_t, Pad, ErrorPolicy::THROW>(p, m_val);
			p = details::pointer_addition(p, Length);
		}

	private:
		uint64_t m_val;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(uint64_t& val)
			: m_val(val)
		{
		}

		void unpackFixed(const void*& p) const
		{
			extract<Length, uint64_t, Pad, ErrorPolicy::THROW>(p, m_val);
			p = details::pointer_addition(p, Length);
		}

	private:
		uint64_t& m_val;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <unsigned int Length, char Pad, typename String>
struct FixedStringType;

template <unsigned int Length, char Pad = ' '>
using FixedString = FixedStringType<Length, Pad, std::string>;

template <typename ExtRefType, char Pad = ' '>
using FixedExtRef = FixedStringType<ExtRefType::max_size(), Pad,
	External_reference<
		typename details::External_reference_parameters<ExtRefType>::Tag,
		ExtRefType::max_size()>>;

template <unsigned int Length, char Pad, typename String>
struct PackingTraits<FixedStringType<Length, Pad, String>>
	: FixedPackingTraits<Length> {
	static_assert(Length != 0, "Zero length");

	class Packer : public FixedPacker {
	public:
		Packer(const String& str)
			: m_str(str)
		{
			if (m_str.size() > Length)
				throw std::invalid_argument("Too long");
		}

		void packFixed(void*& p) const
		{
			size_t len = m_str.size();
			memcpy(p, m_str.data(), len);
			memset(details::pointer_addition(p, len), Pad, Length - len);
			p = details::pointer_addition(p, Length);
		}

	private:
		const String& m_str;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(String& str)
			: m_str(str)
		{
		}

		void unpackFixed(const void*& p) const
		{
			size_t len = Length;
			while (len != 0 && details::get_char(p, len - 1) == Pad)
				--len;
			m_str.assign(static_cast<const char*>(p), len);
			p = details::pointer_addition(p, Length);
		}

	private:
		String& m_str;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <unsigned int Length, char Pad, typename String>
struct FixedStringLeftPaddedType;

template <unsigned int Length, char Pad = ' '>
using FixedStringLeftPadded
	= FixedStringLeftPaddedType<Length, Pad, std::string>;

template <unsigned int Length, char Pad, typename String>
struct PackingTraits<FixedStringLeftPaddedType<Length, Pad, String>>
	: FixedPackingTraits<Length> {
	static_assert(Length != 0, "Zero length");

	class Packer : public FixedPacker {
	public:
		Packer(const String& str)
			: m_str(str)
		{
			if (m_str.size() > Length)
				throw std::invalid_argument("Too long");
		}

		void packFixed(void*& p) const
		{
			size_t len = m_str.size();
			memset(p, Pad, Length - len);
			memcpy(
				details::pointer_addition(p, Length - len), m_str.data(), len);
			p = details::pointer_addition(p, Length);
		}

	private:
		const String& m_str;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(String& str)
			: m_str(str)
		{
		}

		void unpackFixed(const void*& p) const
		{
			size_t i = 0;
			while (i != Length && details::get_char(p, i) == Pad)
				++i;
			m_str.assign(
				static_cast<const char*>(details::pointer_addition(p, i)),
				Length - i);
			p = details::pointer_addition(p, Length);
		}

	private:
		String& m_str;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <size_t Size, typename Container>
struct FixedContainer;

template <size_t Size>
using FixedArray = FixedContainer<Size, std::array<uint8_t, Size>>;

template <size_t Size, typename Container>
struct PackingTraits<FixedContainer<Size, Container>>
	: FixedPackingTraits<Size> {
	static_assert(Size != 0, "Zero size");

	class Packer : public FixedMemoryPacker<Size> {
	public:
		constexpr Packer(const Container& c)
			: FixedMemoryPacker<Size>(c.data())
		{
		}
	};

	class Unpacker : public FixedMemoryUnpacker<Size> {
	public:
		Unpacker(Container& c)
			: FixedMemoryUnpacker<Size>(c.data())
		{
		}
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <size_t Size>
struct FixedBlob;

template <size_t Size>
struct PackingTraits<FixedBlob<Size>> : FixedPackingTraits<Size> {
	static_assert(Size != 0, "Zero size");

	using Packer = FixedMemoryPacker<Size>;
	using Unpacker = FixedMemoryUnpacker<Size>;

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <size_t>
struct FixedCArray;

template <size_t Size>
struct PackingTraits<FixedCArray<Size>> : FixedPackingTraits<Size> {
	class Packer : public FixedMemoryPacker<Size> {
	public:
		Packer(const char (&a)[Size])
			: FixedMemoryPacker<Size>(a)
		{
		}
	};

	class Unpacker : public FixedMemoryUnpacker<Size> {
	public:
		Unpacker(char (&a)[Size])
			: FixedMemoryUnpacker<Size>(a)
		{
		}
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <size_t MaxVarSize, typename SizeField, typename Container>
struct VarContainer;

template <size_t MaxVarSize, typename SizeField = uint8_t>
using VarString = VarContainer<MaxVarSize, SizeField, std::string>;

template <size_t MaxVarSize, typename SizeField = uint8_t>
using VarVector = VarContainer<MaxVarSize, SizeField, std::vector<uint8_t>>;

template <size_t MaxVarSize, typename SizeField, typename Container>
struct PackingTraits<VarContainer<MaxVarSize, SizeField, Container>>
	: VarPackingTraits<MaxVarSize, SizeField> {
	static_assert(sizeof(decltype(*(std::declval<Container>().data()))) == 1,
		"Container elements must be byte size");

	class Packer : public VarPacker<MaxVarSize, SizeField> {
	public:
		Packer(const Container& c)
			: VarPacker<MaxVarSize, SizeField>(c.size())
			, m_c(c)
		{
		}

		void packVar(void*& p) const
		{
			memcpy(p, m_c.data(), this->varSize());
			p = details::pointer_addition(p, this->varSize());
		}

	private:
		const Container& m_c;
	};

	class Unpacker : public VarUnpacker<MaxVarSize, SizeField> {
	public:
		Unpacker(Container& c)
			: m_c(c)
		{
		}

		void unpackVar(const void*& p) const
		{
			m_c.assign(static_cast<const char*>(p),
				static_cast<const char*>(p) + this->varSize());
			p = details::pointer_addition(p, this->varSize());
		}

	private:
		Container& m_c;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <size_t, typename SizeField = uint8_t>
struct VarBlob;

template <size_t MaxVarSize, typename SizeField>
struct PackingTraits<VarBlob<MaxVarSize, SizeField>>
	: VarPackingTraits<MaxVarSize, SizeField> {
	using PackParam = std::tuple<const void*, size_t>;
	using UnpackParam = std::tuple<void*, size_t&>;

	class Packer : public VarPacker<MaxVarSize, SizeField> {
	public:
		Packer(const PackParam& param)
			: VarPacker<MaxVarSize, SizeField>(std::get<1>(param))
			, m_data(std::get<0>(param))
		{
		}

		void packVar(void*& p) const
		{
			memcpy(p, m_data, this->varSize());
			p = details::pointer_addition(p, this->varSize());
		}

	private:
		const void* m_data;
	};

	class Unpacker : public VarUnpacker<MaxVarSize, SizeField> {
	public:
		Unpacker(const UnpackParam& param)
			: m_data(std::get<0>(param))
			, m_size(std::get<1>(param))
		{
		}

		void unpackVar(const void*& p) const
		{
			m_size = this->varSize();
			memcpy(m_data, p, this->varSize());
			p = details::pointer_addition(p, this->varSize());
		}

	private:
		void* m_data;
		size_t& m_size;
	};
};

template <typename Unpacked, typename Packed>
struct Cast;

template <typename Unpacked, typename Packed>
struct PackingTraits<Cast<Unpacked, Packed>,
	std::enable_if_t<details::HasPackOne<Packed> {}>>
	: FixedPackingTraits<sizeof(Packed)> {
	class Packer : public FixedPacker {
	public:
		constexpr Packer(const Unpacked& unpacked)
			: m_unpacked(unpacked)
		{
		}

		void packFixed(void*& p) const
		{
			auto packed = static_cast<Packed>(m_unpacked);
			details::pack_one(p, packed);
		}

	private:
		const Unpacked& m_unpacked;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(Unpacked& unpacked)
			: m_unpacked(unpacked)
		{
		}

		void unpackFixed(const void*& p) const
		{
			Packed packed;
			details::unpack_one(p, packed);
			m_unpacked = static_cast<Unpacked>(packed);
		}

	private:
		Unpacked& m_unpacked;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <typename T, typename ValueT, typename Enable = void>
struct ValuePackingTraits;

template <typename T, typename ValueT>
struct ValuePackingTraits<T, ValueT,
	std::enable_if_t<details::HasPackOne<ValueT> {}>>
	: FixedPackingTraits<sizeof(ValueT)> {
	class Packer : public FixedPacker {
	public:
		constexpr Packer(const T& t)
			: m_t(t)
		{
		}

		void packFixed(void*& p) const
		{
			auto value = m_t.value();
			details::pack_one(p, value);
		}

	private:
		const T& m_t;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(T& t)
			: m_t(t)
		{
		}

		void unpackFixed(const void*& p) const
		{
			ValueT value;
			details::unpack_one(p, value);
			m_t = T(value);
		}

	private:
		T& m_t;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

template <typename TagT, typename UnderlyingTypeT, typename SentinelPolicyT,
	UnderlyingTypeT sentinelV>
struct PackingTraits<
	Object_id<TagT, UnderlyingTypeT, SentinelPolicyT, sentinelV>>
	: ValuePackingTraits<
		  Object_id<TagT, UnderlyingTypeT, SentinelPolicyT, sentinelV>,
		  UnderlyingTypeT> {
};

template <typename TagT, typename UnderlyingTypeT, unsigned int DECIMALS>
struct PackingTraits<Numeric_type<TagT, UnderlyingTypeT, DECIMALS>>
	: ValuePackingTraits<Numeric_type<TagT, UnderlyingTypeT, DECIMALS>,
		  UnderlyingTypeT> {
};

template <typename FlagEnum, typename UnderlyingTypeT>
struct PackingTraits<FlagSet<FlagEnum, UnderlyingTypeT>>
	: ValuePackingTraits<FlagSet<FlagEnum, UnderlyingTypeT>, UnderlyingTypeT> {
};

template <typename... Fields>
class Record;

template <typename... Fields>
struct PackingTraits<Record<Fields...>> {
	using AllFields = meta::list<Fields...>;
	template <typename Field>
	using FieldIsFixed = details::IsFixed<Field>;
	template <typename List>
	using Sum = meta::fold<List, std::integral_constant<size_t, 0>,
		meta::quote<meta::plus>>;

	using IsFixed = meta::all_of<AllFields, meta::quote<FieldIsFixed>>;
	static constexpr size_t fixedSize = Sum<
		meta::transform<AllFields, meta::quote<details::FixedSize>>>::value;
	static constexpr size_t maxSize
		= Sum<meta::transform<AllFields, meta::quote<details::MaxSize>>>::value;

	using PackParam = meta::apply<meta::quote<std::tuple>,
		meta::transform<AllFields, meta::quote<details::PackParam>>>;
	using UnpackParam = meta::apply<meta::quote<std::tuple>,
		meta::transform<AllFields, meta::quote<details::UnpackParam>>>;

	class Packer {
		using Packers = meta::apply<meta::quote<std::tuple>,
			meta::transform<AllFields, meta::quote<details::Packer>>>;

		void packFixedForEach(std::index_sequence<>, void*&) const {}

		template <size_t Index, size_t... Indexes>
		void packFixedForEach(
			std::index_sequence<Index, Indexes...>, void*& p) const
		{
			std::get<Index>(m_packers).packFixed(p);
			packFixedForEach(std::index_sequence<Indexes...> {}, p);
		}

		void packVarForEach(std::index_sequence<>, void*&) const {}

		template <size_t Index, size_t... Indexes>
		void packVarForEach(
			std::index_sequence<Index, Indexes...>, void*& p) const
		{
			std::get<Index>(m_packers).packVar(p);
			packVarForEach(std::index_sequence<Indexes...> {}, p);
		}

		size_t varSizeForEach(std::index_sequence<>) const { return 0; }

		template <size_t Index, size_t... Indexes>
		size_t varSizeForEach(std::index_sequence<Index, Indexes...>) const
		{
			return std::get<Index>(m_packers).varSize()
				+ varSizeForEach(std::index_sequence<Indexes...> {});
		}

	public:
		Packer(const PackParam& packParam)
			: m_packers(packParam)
		{
		}

		Packer(const details::PackParam<Fields>&... packParams)
			: m_packers(PackParam(packParams...))
		{
		}

		void packFixed(void*& p) const
		{
			packFixedForEach(std::make_index_sequence<sizeof...(Fields)>(), p);
		}

		void packVar(void*& p) const
		{
			packVarForEach(std::make_index_sequence<sizeof...(Fields)>(), p);
		}

		size_t varSize() const
		{
			return varSizeForEach(
				std::make_index_sequence<sizeof...(Fields)>());
		}

	private:
		Packers m_packers;
	};

	class Unpacker {
		using Unpackers = meta::apply<meta::quote<std::tuple>,
			meta::transform<meta::list<Fields...>,
				meta::quote<details::Unpacker>>>;

		void unpackFixedForEach(std::index_sequence<>, const void*&) {}

		template <size_t Index, size_t... Indexes>
		void unpackFixedForEach(
			std::index_sequence<Index, Indexes...>, const void*& p)
		{
			std::get<Index>(m_unpackers).unpackFixed(p);
			unpackFixedForEach(std::index_sequence<Indexes...> {}, p);
		}

		void unpackVarForEach(std::index_sequence<>, const void*&) const {}

		template <size_t Index, size_t... Indexes>
		void unpackVarForEach(
			std::index_sequence<Index, Indexes...>, const void*& p) const
		{
			std::get<Index>(m_unpackers).unpackVar(p);
			unpackVarForEach(std::index_sequence<Indexes...> {}, p);
		}

		static size_t varSizeForEach(meta::list<>, const void*) { return 0; }

		template <typename Field, typename... RemainingFields>
		static size_t varSizeForEach(
			meta::list<Field, RemainingFields...>, const void* p)
		{
			auto nextP
				= details::pointer_addition(p, details::FixedSize<Field> {});
			return details::Unpacker<Field>::varSize(p)
				+ varSizeForEach(meta::list<RemainingFields...> {}, nextP);
		}

	public:
		Unpacker(const UnpackParam& unpackParam)
			: m_unpackers(unpackParam)
		{
		}

		Unpacker(const details::UnpackParam<Fields>&... unpackParams)
			: m_unpackers(UnpackParam(unpackParams...))
		{
		}

		void unpackFixed(const void*& p)
		{
			unpackFixedForEach(
				std::make_index_sequence<sizeof...(Fields)>(), p);
		}

		void unpackVar(const void*& p) const
		{
			unpackVarForEach(std::make_index_sequence<sizeof...(Fields)>(), p);
		}

		static size_t varSize(const void* p)
		{
			return varSizeForEach(meta::list<Fields...> {}, p);
		}

	private:
		Unpackers m_unpackers;
	};
};

} // namespace misc

#endif // MISC_PACKING_TRAITS_H
