#ifndef MISC_STRING_TO_H
#define MISC_STRING_TO_H

#include <cstddef>
#include <string>
#include <type_traits>

namespace misc {

namespace details {

short string_to_short(const char*, size_t*, int = 10);
unsigned short string_to_unsigned_short(const char*, size_t*, int = 10);
int string_to_int(const char*, size_t*, int = 10);
unsigned int string_to_unsigned_int(const char*, size_t*, int = 10);
long string_to_long(const char*, size_t*, int = 10);
unsigned long string_to_unsigned_long(const char*, size_t*, int = 10);
float string_to_float(const char*, size_t*);
double string_to_double(const char*, size_t*);

short string_to_short(const char*, int = 10);
unsigned short string_to_unsigned_short(const char*, int = 10);
int string_to_int(const char*, int = 10);
unsigned int string_to_unsigned_int(const char*, int = 10);
long string_to_long(const char*, int = 10);
unsigned long string_to_unsigned_long(const char*, int = 10);
float string_to_float(const char*);
double string_to_double(const char*);

template <typename T>
constexpr bool is_supported_integral_type()
{
	typedef std::remove_cv_t<T> U;
	return std::is_same_v<U,
			   short> || std::is_same_v<U, unsigned short> || std::is_same_v<U, int> || std::is_same_v<U, unsigned int> || std::is_same_v<U, long> || std::is_same_v<U, unsigned long>;
}

template <typename T>
constexpr bool is_supported_floating_point_type()
{
	typedef std::remove_cv_t<T> U;
	return std::is_same_v<U, float> || std::is_same_v<U, double>;
}

} // namespace details

/**
 *  @throw std::invalid_argument If no conversion could be performed.
 *  @throw std::out_of_range If the converted value would fall out of the range
 *  of the result type.
 */
template <typename T>
std::enable_if_t<details::is_supported_integral_type<T>(), T> string_to(
	const char*, size_t*, int = 10);

template <>
inline short string_to<short>(const char* s, size_t* idx, int base)
{
	return details::string_to_short(s, idx, base);
}

template <>
inline unsigned short string_to<unsigned short>(
	const char* s, size_t* idx, int base)
{
	return details::string_to_unsigned_short(s, idx, base);
}

template <>
inline int string_to<int>(const char* s, size_t* idx, int base)
{
	return details::string_to_int(s, idx, base);
}

template <>
inline unsigned int string_to<unsigned int>(
	const char* s, size_t* idx, int base)
{
	return details::string_to_unsigned_int(s, idx, base);
}

template <>
inline long string_to<long>(const char* s, size_t* idx, int base)
{
	return details::string_to_long(s, idx, base);
}

template <>
inline unsigned long string_to<unsigned long>(
	const char* s, size_t* idx, int base)
{
	return details::string_to_unsigned_long(s, idx, base);
}

/**
 *  @throw std::invalid_argument If no conversion could be performed.
 *  @throw std::out_of_range If the converted value would fall out of the range
 *  of the result type.
 */
template <typename T>
std::enable_if_t<details::is_supported_floating_point_type<T>(), T> string_to(
	const char*, size_t*);

template <>
inline float string_to<float>(const char* s, size_t* idx)
{
	return details::string_to_float(s, idx);
}

template <>
inline double string_to<double>(const char* s, size_t* idx)
{
	return details::string_to_double(s, idx);
}

/**
 *  @throw std::invalid_argument If no conversion could be performed.
 *  @throw std::out_of_range If the converted value would fall out of the range
 *  of the result type.
 */
template <typename T>
std::enable_if_t<details::is_supported_integral_type<T>(), T> string_to(
	const char*, int = 10);

template <>
inline short string_to<short>(const char* s, int base)
{
	return details::string_to_short(s, base);
}

template <>
inline unsigned short string_to<unsigned short>(const char* s, int base)
{
	return details::string_to_unsigned_short(s, base);
}

template <>
inline int string_to<int>(const char* s, int base)
{
	return details::string_to_int(s, base);
}

template <>
inline unsigned int string_to<unsigned int>(const char* s, int base)
{
	return details::string_to_unsigned_int(s, base);
}

template <>
inline long string_to<long>(const char* s, int base)
{
	return details::string_to_long(s, base);
}

template <>
inline unsigned long string_to<unsigned long>(const char* s, int base)
{
	return details::string_to_unsigned_long(s, base);
}

/**
 *  @throw std::invalid_argument If no conversion could be performed.
 *  @throw std::out_of_range If the converted value would fall out of the range
 *  of the result type.
 */
template <typename T>
std::enable_if_t<details::is_supported_floating_point_type<T>(), T> string_to(
	const char*);

template <>
inline float string_to<float>(const char* s)
{
	return details::string_to_float(s);
}

template <>
inline double string_to<double>(const char* s)
{
	return details::string_to_double(s);
}

/**
 *  @throw std::invalid_argument If no conversion could be performed.
 *  @throw std::out_of_range If the converted value would fall out of the range
 *  of the result type.
 */
template <typename T>
std::enable_if_t<details::is_supported_integral_type<T>(), T> string_to(
	const std::string&, size_t*, int = 10);

template <>
inline short string_to<short>(const std::string& s, size_t* idx, int base)
{
	return details::string_to_short(s.c_str(), idx, base);
}

template <>
inline unsigned short string_to<unsigned short>(
	const std::string& s, size_t* idx, int base)
{
	return details::string_to_unsigned_short(s.c_str(), idx, base);
}

template <>
inline int string_to<int>(const std::string& s, size_t* idx, int base)
{
	return details::string_to_int(s.c_str(), idx, base);
}

template <>
inline unsigned int string_to<unsigned int>(
	const std::string& s, size_t* idx, int base)
{
	return details::string_to_unsigned_int(s.c_str(), idx, base);
}

template <>
inline long string_to<long>(const std::string& s, size_t* idx, int base)
{
	return details::string_to_long(s.c_str(), idx, base);
}

template <>
inline unsigned long string_to<unsigned long>(
	const std::string& s, size_t* idx, int base)
{
	return details::string_to_unsigned_long(s.c_str(), idx, base);
}

/**
 *  @throw std::invalid_argument If no conversion could be performed.
 *  @throw std::out_of_range If the converted value would fall out of the range
 *  of the result type.
 */
template <typename T>
std::enable_if_t<details::is_supported_floating_point_type<T>(), T> string_to(
	const std::string&, size_t*);

template <>
inline float string_to<float>(const std::string& s, size_t* idx)
{
	return details::string_to_float(s.c_str(), idx);
}

template <>
inline double string_to<double>(const std::string& s, size_t* idx)
{
	return details::string_to_double(s.c_str(), idx);
}

/**
 *  @throw std::invalid_argument If no conversion could be performed.
 *  @throw std::out_of_range If the converted value would fall out of the range
 *  of the result type.
 */
template <typename T>
std::enable_if_t<details::is_supported_integral_type<T>(), T> string_to(
	const std::string&, int = 10);

template <>
inline short string_to<short>(const std::string& s, int base)
{
	return details::string_to_short(s.c_str(), base);
}

template <>
inline unsigned short string_to<unsigned short>(const std::string& s, int base)
{
	return details::string_to_unsigned_short(s.c_str(), base);
}

template <>
inline int string_to<int>(const std::string& s, int base)
{
	return details::string_to_int(s.c_str(), base);
}

template <>
inline unsigned int string_to<unsigned int>(const std::string& s, int base)
{
	return details::string_to_unsigned_int(s.c_str(), base);
}

template <>
inline long string_to<long>(const std::string& s, int base)
{
	return details::string_to_long(s.c_str(), base);
}

template <>
inline unsigned long string_to<unsigned long>(const std::string& s, int base)
{
	return details::string_to_unsigned_long(s.c_str(), base);
}

/**
 *  @throw std::invalid_argument If no conversion could be performed.
 *  @throw std::out_of_range If the converted value would fall out of the range
 *  of the result type.
 */
template <typename T>
std::enable_if_t<details::is_supported_floating_point_type<T>(), T> string_to(
	const std::string&);

template <>
inline float string_to<float>(const std::string& s)
{
	return details::string_to_float(s.c_str());
}

template <>
inline double string_to<double>(const std::string& s)
{
	return details::string_to_double(s.c_str());
}

} // namespace misc

#endif // MISC_STRING_TO_H
