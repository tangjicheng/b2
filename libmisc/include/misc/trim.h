#ifndef MISC_TRIM_H
#define MISC_TRIM_H

#include <cctype>
#include <cstddef>
#include <cstring>
#include <string>

namespace misc {

namespace details {

inline size_t ltrim_begin(const char* str, size_t len)
{
	size_t beg = 0;
	while (beg != len && isspace(str[beg]))
		++beg;
	return beg;
}

inline size_t rtrim_end(const char* str, size_t len)
{
	while (len != 0 && isspace(str[len - 1]))
		--len;
	return len;
}

} // namespace details

inline std::string ltrim(const char* str, size_t len)
{
	auto b = details::ltrim_begin(str, len);
	return std::string(str + b, len - b);
}

inline std::string ltrim(const char* str)
{
	auto len = strlen(str);
	auto b = details::ltrim_begin(str, len);
	return std::string(str + b, len - b);
}

inline std::string& ltrim(std::string& str)
{
	str.erase(0, details::ltrim_begin(str.data(), str.size()));
	return str;
}

inline std::string rtrim(const char* str, size_t len)
{
	return std::string(str, details::rtrim_end(str, len));
}

inline std::string rtrim(const char* str)
{
	return std::string(str, details::rtrim_end(str, strlen(str)));
}

inline std::string& rtrim(std::string& str)
{
	str.resize(details::rtrim_end(str.data(), str.size()));
	return str;
}

inline std::string trim(const char* str, size_t len)
{
	auto b = details::ltrim_begin(str, len);
	return std::string(str + b, details::rtrim_end(str + b, len - b));
}

inline std::string trim(const char* str)
{
	auto len = strlen(str);
	auto b = details::ltrim_begin(str, len);
	return std::string(str + b, details::rtrim_end(str + b, len - b));
}

inline std::string& trim(std::string& str)
{
	str.erase(0, details::ltrim_begin(str.data(), str.size()));
	str.resize(details::rtrim_end(str.data(), str.size()));
	return str;
}

} // namespace misc

#endif // MISC_TRIM_H
