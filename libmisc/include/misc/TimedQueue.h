#ifndef MISC_TIMEDQUEUE_H
#define MISC_TIMEDQUEUE_H

#include <chrono>
#include <condition_variable>
#include <deque>
#include <mutex>
#include <optional>
#include <utility>

namespace misc {

template <typename T, typename Container = std::deque<T>>
class TimedQueue {
public:
	using value_type = typename Container::value_type;
	using pointer = typename Container::pointer;
	using const_pointer = typename Container::const_pointer;
	using reference = typename Container::reference;
	using const_reference = typename Container::const_reference;
	using size_type = typename Container::size_type;
	using allocator_type = typename Container::allocator_type;
	using container_type = Container;

	TimedQueue() = default;
	~TimedQueue() = default;

	TimedQueue(const TimedQueue&) = delete;
	TimedQueue& operator=(const TimedQueue&) = delete;

	TimedQueue(TimedQueue&&) = delete;
	TimedQueue& operator=(TimedQueue&&) = delete;

	void push(const value_type&);

	void push(value_type&&);

	template <class... Args>
	void emplace(Args&&...);

	std::optional<value_type> pop();

	std::optional<value_type> try_pop();

	template <typename Duration>
	std::optional<value_type> try_pop_until(
		const std::chrono::time_point<std::chrono::system_clock, Duration>&);

	template <typename Rep, typename Period>
	std::optional<value_type> try_pop_for(
		const std::chrono::duration<Rep, Period>&);

	void unblock() const;

	size_type size() const;

	bool empty() const;

private:
	Container m_cont;
	mutable std::mutex m_mtx;
	mutable std::condition_variable m_cv;
};

template <typename T, typename Container>
void TimedQueue<T, Container>::push(const value_type& val)
{
	std::lock_guard<std::mutex> lk(m_mtx);
	m_cont.push_back(val);
	m_cv.notify_one();
}

template <typename T, typename Container>
void TimedQueue<T, Container>::push(value_type&& val)
{
	std::lock_guard<std::mutex> lk(m_mtx);
	m_cont.push_back(std::move(val));
	m_cv.notify_one();
}

template <typename T, typename Container>
template <class... Args>
void TimedQueue<T, Container>::emplace(Args&&... args)
{
	std::lock_guard<std::mutex> lk(m_mtx);
	m_cont.emplace_back(std::forward<Args>(args)...);
	m_cv.notify_one();
}

template <typename T, typename Container>
std::optional<typename TimedQueue<T, Container>::value_type>
TimedQueue<T, Container>::pop()
{
	std::unique_lock<std::mutex> lk(m_mtx);
	if (m_cont.empty()) {
		m_cv.wait(lk);
		if (m_cont.empty())
			return std::nullopt;
	}
	auto ret = std::move(m_cont.front());
	m_cont.pop_front();
	return {std::move(ret)};
}

template <typename T, typename Container>
std::optional<typename TimedQueue<T, Container>::value_type>
TimedQueue<T, Container>::try_pop()
{
	std::lock_guard<std::mutex> lk(m_mtx);
	if (m_cont.empty())
		return std::nullopt;
	auto ret = std::move(m_cont.front());
	m_cont.pop_front();
	return {std::move(ret)};
}

template <typename T, typename Container>
template <typename Duration>
std::optional<typename TimedQueue<T, Container>::value_type>
TimedQueue<T, Container>::try_pop_until(
	const std::chrono::time_point<std::chrono::system_clock, Duration>& absTime)
{
	std::unique_lock<std::mutex> lk(m_mtx);
	if (m_cont.empty()) {
		m_cv.wait_until(lk, absTime);
		if (m_cont.empty())
			return std::nullopt;
	}
	auto ret = std::move(m_cont.front());
	m_cont.pop_front();
	return {std::move(ret)};
}

template <typename T, typename Container>
template <typename Rep, typename Period>
std::optional<typename TimedQueue<T, Container>::value_type>
TimedQueue<T, Container>::try_pop_for(
	const std::chrono::duration<Rep, Period>& relTime)
{
	return try_pop_until(std::chrono::system_clock::now() + relTime);
}

template <typename T, typename Container>
void TimedQueue<T, Container>::unblock() const
{
	m_cv.notify_one();
}

template <typename T, typename Container>
typename TimedQueue<T, Container>::size_type TimedQueue<T, Container>::size()
	const
{
	std::lock_guard<std::mutex> lk(m_mtx);
	return m_cont.size();
}

template <typename T, typename Container>
bool TimedQueue<T, Container>::empty() const
{
	std::lock_guard<std::mutex> lk(m_mtx);
	return m_cont.empty();
}

} // namespace misc

#endif // MISC_TIMEDQUEUE_H
