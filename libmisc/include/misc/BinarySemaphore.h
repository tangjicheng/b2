#ifndef MISC_BINARYSEMAPHORE_H
#define MISC_BINARYSEMAPHORE_H

#include <condition_variable>
#include <mutex>

namespace misc {

class BinarySemaphore {
public:
	void wait();
	void signal();

private:
	bool m_signalled {false};
	std::mutex m_mtx;
	std::condition_variable m_cv;
};

} // namespace misc

#endif // MISC_BINARYSEMAPHORE_H
