#ifndef MISC_DAEMON_APPLICATION_H
#define MISC_DAEMON_APPLICATION_H

#include <atomic>
#include <cctype>
#include <set>
#include <string>
#include <vector>

#include "ThreadedReactor.h"
#include "base_application.h"
#include "option.h"
#include "version.h"

namespace misc {

class DaemonApplication : public Base_application {
protected:
	DaemonApplication(const std::string&, const std::string&, const Version&);
	DaemonApplication(const std::string&, const std::string&,
		const std::vector<Option>&, const Version&);

	~DaemonApplication() = default;

	DaemonApplication(const DaemonApplication&) = delete;
	DaemonApplication& operator=(const DaemonApplication&) = delete;

	DaemonApplication(DaemonApplication&&) = delete;
	DaemonApplication& operator=(DaemonApplication&&) = delete;

	void init(const YAML::Node&) override;
	int do_work() override;

	Reactor& reactor() { return m_reactor.get(); }

private:
	ThreadedReactor m_reactor;

	void start();
	void stop();
};

} // namespace misc

#endif // MISC_DAEMON_APPLICATION_H
