#ifndef MISC_APPLICATION_EXCEPTION_H
#define MISC_APPLICATION_EXCEPTION_H

#include <exception>
#include <string>
#include <utility>

namespace misc {

class ConfigurationError : public std::exception {
public:
	explicit ConfigurationError(const std::string& what)
		: what_ {what}
	{
	}

	explicit ConfigurationError(std::string&& what)
		: what_ {std::move(what)}
	{
	}

	const char* what() const noexcept override { return what_.c_str(); }

private:
	std::string what_;
};

class CriticalError : public std::exception {
public:
	explicit CriticalError(const std::string& what)
		: what_ {what}
	{
	}

	explicit CriticalError(std::string&& what)
		: what_ {std::move(what)}
	{
	}

	const char* what() const noexcept override { return what_.c_str(); }

private:
	std::string what_;
};

} // namespace misc

#endif // MISC_APPLICATION_EXCEPTION_H
