#ifndef MISC_INJECT_H
#define MISC_INJECT_H

#include <cstring>
#include <limits>
#include <stdexcept>
#include <tuple>
#include <type_traits>

#include "details/pointer_operations.h"
#include "pow.h"

namespace misc {

// policy

enum class ErrorPolicy { NONE, FAIL, CAP, THROW };

namespace details {

template <typename T, bool = std::numeric_limits<T>::is_signed>
struct is_negative_impl;

template <typename T>
struct is_negative_impl<T, true> {
	static bool test(T value) { return value < 0; }
};

template <typename T>
struct is_negative_impl<T, false> {
	static bool test(T) { return false; }
};

template <typename T>
bool is_negative(T value)
{
	return is_negative_impl<T>::test(value);
}

template <unsigned int Length, typename T,
	bool = Length<(unsigned int)(std::numeric_limits<T>::digits10
		+ 1)> struct is_overflow_impl;

template <unsigned int Length, typename T>
struct is_overflow_impl<Length, T, true> {
	static constexpr T cap = Pow_v<T, 10, Length> - (T)1;

	static bool test(T value) { return value > cap; }
};

template <unsigned int Length, typename T>
struct is_overflow_impl<Length, T, false> {
	// This constant is never actually used.
	static constexpr T cap = 0;

	static bool test(T) { return false; }
};

template <unsigned int Length, typename T>
bool is_overflow(T value)
{
	return is_overflow_impl<Length, T>::test(value);
}

template <unsigned int RightLength, typename T, bool = std::is_signed_v<T>>
class range;

template <unsigned int RightLength, typename T>
class range<RightLength, T, false> {
public:
	static bool is_out_of_range(const T& left, const T& right)
	{
		auto tmp = std::tie(left, right);
		return tmp > max;
	}

	static void cap_if_out_of_range(T& left, T& right)
	{
		auto tmp = std::tie(left, right);
		if (tmp > max)
			tmp = max;
	}

private:
	static constexpr std::tuple<T, T> max = std::make_tuple(
		std::numeric_limits<T>::max() / Pow_v<T, 10, RightLength>,
		std::numeric_limits<T>::max() % Pow_v<T, 10, RightLength>);
};

template <unsigned int RightLength, typename T>
constexpr std::tuple<T, T> range<RightLength, T, false>::max;

template <unsigned int RightLength, typename T>
class range<RightLength, T, true> {
public:
	static bool is_out_of_range(const T& left, const T& right)
	{
		auto tmp = std::tie(left, right);
		return tmp > max || tmp < min;
	}

	static void cap_if_out_of_range(T& left, T& right)
	{
		auto tmp = std::tie(left, right);
		if (tmp > max)
			tmp = max;
		else if (tmp < min)
			tmp = min;
	}

private:
	static constexpr std::tuple<T, T> max = std::make_tuple(
		std::numeric_limits<T>::max() / Pow_v<T, 10, RightLength>,
		std::numeric_limits<T>::max() % Pow_v<T, 10, RightLength>);
	static constexpr std::tuple<T, T> min = std::make_tuple(
		std::numeric_limits<T>::min() / Pow_v<T, 10, RightLength>,
		std::numeric_limits<T>::min() % Pow_v<T, 10, RightLength>);
};

template <unsigned int RightLength, typename T>
constexpr std::tuple<T, T> range<RightLength, T, true>::max;

template <unsigned int RightLength, typename T>
constexpr std::tuple<T, T> range<RightLength, T, true>::min;

template <unsigned int RightLength, ErrorPolicy Policy>
struct ExtractValidation;

template <unsigned int RightLength>
struct ExtractValidation<RightLength, ErrorPolicy::NONE> {
	template <typename T>
	bool operator()(T&, T&) const
	{
		return true;
	}
};

template <unsigned int RightLength>
struct ExtractValidation<RightLength, ErrorPolicy::FAIL> {
	template <typename T>
	bool operator()(T& left, T& right) const
	{
		if (range<RightLength, T>::is_out_of_range(left, right))
			return false;
		return true;
	}
};

template <unsigned int RightLength>
struct ExtractValidation<RightLength, ErrorPolicy::CAP> {
	template <typename T>
	bool operator()(T& left, T& right) const
	{
		range<RightLength, T>::cap_if_out_of_range(left, right);
		return true;
	}
};

template <unsigned int RightLength>
struct ExtractValidation<RightLength, ErrorPolicy::THROW> {
	template <typename T>
	bool operator()(T& left, T& right) const
	{
		if (range<RightLength, T>::is_out_of_range(left, right))
			throw std::invalid_argument("Out of range");
		return true;
	}
};

template <unsigned int Length, ErrorPolicy Policy>
struct InjectValidation;

template <unsigned int Length>
struct InjectValidation<Length, ErrorPolicy::NONE> {
	template <typename T>
	bool operator()(T&) const
	{
		return true;
	}
};

template <unsigned int Length>
struct InjectValidation<Length, ErrorPolicy::FAIL> {
	template <typename T>
	bool operator()(T& value) const
	{
		if (is_negative(value))
			return false;
		if (is_overflow<Length>(value))
			return false;
		return true;
	}
};

template <unsigned int Length>
struct InjectValidation<Length, ErrorPolicy::CAP> {
	template <typename T>
	bool operator()(T& value) const
	{
		if (is_negative(value))
			value = 0;
		if (is_overflow<Length>(value))
			value = is_overflow_impl<Length, T>::cap;
		return true;
	}
};

template <unsigned int Length>
struct InjectValidation<Length, ErrorPolicy::THROW> {
	template <typename T>
	bool operator()(T& value) const
	{
		if (is_negative(value))
			throw std::invalid_argument("Out of range");
		if (is_overflow<Length>(value))
			throw std::invalid_argument("Out of range");
		return true;
	}
};

template <typename T>
struct sunpack_final_digit {
	static void unpack(const char* p, T& value) { value = get_digit<T>(p); }
};

template <typename T, char Pad>
struct sunpack_final_pad {
	template <typename U = T>
	static std::enable_if_t<std::is_unsigned_v<U>> unpack(
		const char* p, T& value)
	{
		if (*p == Pad)
			value = 0;
		else
			value = get_digit<T>(p);
	}

	template <typename U = T>
	static std::enable_if_t<std::is_signed_v<U>> unpack(const char* p, T& value)
	{
		if (*p == Pad || *p == '-' || *p == '+')
			value = 0;
		else
			value = get_digit<T>(p);
	}
};

template <unsigned int Length, typename T, char Pad>
void unpackFromSign(const char*, T&);

template <unsigned int Length, typename T, char Pad,
	typename Final = sunpack_final_digit<T>>
struct sunpack {
	template <typename U = T>
	static std::enable_if_t<std::is_unsigned_v<U>> unpack(
		const char* p, T& value)
	{
		if (*p == Pad)
			sunpack<Length - 1, T, Pad, Final>::unpack(p + 1, value);
		else {
			sunpack<Length - 1, T, '0'>::unpack(p + 1, value);
			value += get_digit<T>(p) * Pow_v<T, 10, Length - 1>;
		}
	}

	template <typename U = T>
	static std::enable_if_t<std::is_signed_v<U>> unpack(const char* p, T& value)
	{
		if (*p == Pad)
			sunpack<Length - 1, T, Pad, Final>::unpack(p + 1, value);
		else
			unpackFromSign<Length, T, '0'>(p, value);
	}
};

template <unsigned int Length, typename T, typename Final>
struct sunpack<Length, T, '0', Final> {
	static void unpack(const char* p, T& value)
	{
		sunpack<Length - 1, T, '0'>::unpack(p + 1, value);
		value += get_digit<T>(p) * Pow_v<T, 10, Length - 1>;
	}
};

template <typename T, char Pad, typename Final>
struct sunpack<1, T, Pad, Final> {
	static void unpack(const char* p, T& value) { Final::unpack(p, value); }
};

template <typename T, typename Final>
struct sunpack<1, T, '0', Final> {
	static void unpack(const char* p, T& value)
	{
		sunpack_final_digit<T>::unpack(p, value);
	}
};

template <unsigned int Length, typename T, char Pad>
void unpackFromSign(const char* p, T& value)
{
	if (Pad == '0' && (*p == '-' || *p == '+')) {
		sunpack<Length - 1, T, '0'>::unpack(p + 1, value);
		if (*p == '-')
			value *= -1;
	}
	else
		sunpack<Length, T, Pad>::unpack(p, value);
}

} // namespace details

template <unsigned int Length, typename T, char Pad = '0',
	ErrorPolicy Policy = ErrorPolicy::CAP>
void inject(void* vp, T value)
{
	static_assert(
		(unsigned int)(std::numeric_limits<unsigned long>::digits10 + 1)
			>= Length,
		"Maximum field length exceeded");
	details::InjectValidation<Length, Policy> validation;
	if (!validation(value))
		return;
	auto first = static_cast<char*>(vp);
	auto last = first + Length;
	do {
		details::put_digit(--last, value % 10);
		value /= 10;
	} while (value > 0);
	if (first < last)
		memset(first, Pad, last - first);
}

template <unsigned int Length>
void inject(void* p, const void* value)
{
	memcpy(p, value, Length);
}

template <unsigned int Length, typename T, char Pad = '0',
	ErrorPolicy Policy = ErrorPolicy::CAP>
std::enable_if_t<std::is_unsigned_v<T>> extract(const void* vp, T& value)
{
	static_assert(
		(unsigned int)(std::numeric_limits<T>::digits10 + 1) >= Length,
		"Wider integral type required");
	static_assert(Length > 1, "Length not supported");
	auto p = static_cast<const char*>(vp);
	if (Policy == ErrorPolicy::NONE
		|| std::numeric_limits<T>::digits10 + 1 > Length)
		details::sunpack<Length, T, Pad>::unpack(p, value);
	else {
		constexpr unsigned int LeftLength = (Length + 1) / 2;
		constexpr unsigned int RightLength = Length - LeftLength;
		T left, right;
		if (Pad == '0') {
			details::sunpack<LeftLength, T, '0'>::unpack(p, left);
			details::sunpack<RightLength, T, '0'>::unpack(
				p + LeftLength, right);
		}
		else {
			details::sunpack<LeftLength, T, Pad,
				details::sunpack_final_pad<T, Pad>>::unpack(p, left);
			if (p[LeftLength - 1] == Pad)
				details::sunpack<RightLength, T, Pad>::unpack(
					p + LeftLength, right);
			else
				details::sunpack<RightLength, T, '0'>::unpack(
					p + LeftLength, right);
		}
		details::ExtractValidation<RightLength, Policy> validation;
		if (validation(left, right))
			value = left * Pow_v<T, 10, RightLength> + right;
	}
}

template <unsigned int Length, typename T, char Pad = '0',
	ErrorPolicy Policy = ErrorPolicy::CAP>
std::enable_if_t<std::is_signed_v<T>> extract(const void* vp, T& value)
{
	static_assert(
		(unsigned int)(std::numeric_limits<T>::digits10 + 2) >= Length,
		"Wider integral type required");
	static_assert(Length > 2, "Length not supported");
	auto p = static_cast<const char*>(vp);
	if (Policy == ErrorPolicy::NONE
		|| std::numeric_limits<T>::digits10 + 1 > Length)
		details::unpackFromSign<Length, T, Pad>(p, value);
	else {
		constexpr unsigned int LeftLength = (Length + 1) / 2;
		constexpr unsigned int RightLength = Length - LeftLength;
		T left, right;
		if (Pad == '0') {
			details::unpackFromSign<LeftLength, T, '0'>(p, left);
			details::sunpack<RightLength, T, '0'>::unpack(
				p + LeftLength, right);
			if (*p == '-')
				right *= -1;
		}
		else {
			details::sunpack<LeftLength, T, Pad,
				details::sunpack_final_pad<T, Pad>>::unpack(p, left);
			if (p[LeftLength - 1] == Pad)
				details::sunpack<RightLength, T, Pad>::unpack(
					p + LeftLength, right);
			else
				details::sunpack<RightLength, T, '0'>::unpack(
					p + LeftLength, right);
			if (left < 0
				|| (left == 0 && memchr(p, '-', LeftLength) != nullptr))
				right *= -1;
		}
		details::ExtractValidation<RightLength, Policy> validation;
		if (validation(left, right))
			value = left * Pow_v<T, 10, RightLength> + right;
	}
}

template <unsigned int Length>
void extract(const void* p, void* value)
{
	memcpy(value, p, Length);
}

} // namespace misc

#endif // MISC_INJECT_H
