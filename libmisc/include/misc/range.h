#ifndef MISC_RANGE_H
#define MISC_RANGE_H

#include <iterator>

namespace misc {

template <typename Iterator>
class range {
private:
	using traits_type = std::iterator_traits<Iterator>;

public:
	using iterator_type = Iterator;

	using iterator_category = typename traits_type::iterator_category;
	using difference_type = typename traits_type::difference_type;
	using value_type = typename traits_type::value_type;
	using pointer = typename traits_type::pointer;
	using reference = typename traits_type::reference;

	range()
		: m_begin()
		, m_end(m_begin)
	{
	}

	range(iterator_type begin, iterator_type end)
		: m_begin(begin)
		, m_end(end)
	{
	}

	template <typename OtherIterator>
	range(OtherIterator begin, OtherIterator end)
		: m_begin(begin)
		, m_end(end)
	{
	}

	iterator_type begin() const { return m_begin; }

	iterator_type end() const { return m_end; }

	bool empty() const { return m_begin == m_end; }

private:
	iterator_type m_begin;
	iterator_type m_end;
};

template <typename Iterator>
range<Iterator> make_range(Iterator begin, Iterator end)
{
	return range<Iterator>(begin, end);
}

template <typename Container>
auto make_range(Container& container) -> range<decltype(std::begin(container))>
{
	return decltype(make_range(container))(
		std::begin(container), std::end(container));
}

template <typename Container>
auto make_range(const Container& container)
	-> range<decltype(std::begin(container))>
{
	return decltype(make_range(container))(
		std::begin(container), std::end(container));
}

} // namespace misc

#endif // MISC_RANGE_H
