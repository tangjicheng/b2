#ifndef MISC_TRANSACTION_H
#define MISC_TRANSACTION_H

#include <cstddef>
#include <cstdint>
#include <vector>

namespace misc {

struct TransactionChunk {
	int8_t type;
	uint16_t size;
	const void* data;
};

class TransactionIterator {
public:
	TransactionIterator() = default;

	TransactionIterator(const void* data, size_t size)
	{
		auto ptr = reinterpret_cast<const uint8_t*>(data);
		m_cur = ptr;
		m_end = ptr + size;
	}

	TransactionChunk next()
	{
		TransactionChunk chunk;
		chunk.type = *m_cur++;
		union {
			uint16_t val;
			uint8_t x[2];
		} size;
		size.x[0] = *m_cur++;
		size.x[1] = *m_cur++;
		chunk.size = size.val;
		chunk.data = m_cur;
		m_cur += size.val;
		return chunk;
	}

	explicit operator bool() const { return m_cur < m_end; }

private:
	const uint8_t* m_cur {nullptr};
	const uint8_t* m_end {nullptr};
};

class Transaction {
public:
	const void* data() const { return m_buffer.data(); }

	size_t size() const { return m_buffer.size(); }

	void add(int8_t type, const void* data, uint16_t size)
	{
		addField(type);
		addField(size);
		addField(data, size);
	}

private:
	std::vector<uint8_t> m_buffer;

	template <typename T>
	void addField(T t)
	{
		auto ptr = reinterpret_cast<const uint8_t*>(&t);
		m_buffer.insert(m_buffer.end(), ptr, ptr + sizeof(t));
	}

	void addField(const void* data, uint16_t size)
	{
		auto ptr = reinterpret_cast<const uint8_t*>(data);
		m_buffer.insert(m_buffer.end(), ptr, ptr + size);
	}
};

} // namespace misc

#endif // MISC_TRANSACTION_H
