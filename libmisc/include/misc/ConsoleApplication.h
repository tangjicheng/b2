#ifndef MISC_CONSOLE_APPLICATION_H
#define MISC_CONSOLE_APPLICATION_H

#include <atomic>
#include <cctype>
#include <set>
#include <string>
#include <vector>

#include <yaml-cpp/yaml.h>

#include "ThreadedReactor.h"
#include "option.h"
#include "version.h"

namespace misc {

class ConsoleApplication {
public:
	int run(int, char**);

protected:
	ConsoleApplication(const std::string&, const std::string&, const Version&);
	ConsoleApplication(const std::string&, const std::string&,
		const std::vector<Option>&, const Version&);

	~ConsoleApplication() = default;

	ConsoleApplication(const ConsoleApplication&) = delete;
	ConsoleApplication& operator=(const ConsoleApplication&) = delete;

	ConsoleApplication(ConsoleApplication&&) = delete;
	ConsoleApplication& operator=(ConsoleApplication&&) = delete;

	void shutdown();

	Reactor& reactor() { return m_reactor.get(); }

private:
	struct OptionCompare {
		bool operator()(const Option& lhs, const Option& rhs) const
		{
			char lhsUpper = toupper(lhs.character);
			char rhsUpper = toupper(rhs.character);
			if (lhsUpper != rhsUpper)
				return lhsUpper < rhsUpper;
			return lhs.character > rhs.character;
		}
	};

	std::string m_applicationName;
	std::string m_executableName;
	std::string m_defaultConfigFilename;
	std::vector<Option> m_extraOptions;
	std::set<Option, OptionCompare> m_options;
	Version m_version;
	ThreadedReactor m_reactor;
	std::atomic_bool m_keepGoing {true};

	std::string optionString() const;
	void printVersion() const;
	void printUsage() const;

	void init(const YAML::Node&);
	void start();
	void stop();

	virtual void onOption(int, const char*) = 0;
	virtual void onEndOfOptions() = 0;
	virtual void onInit(const YAML::Node&) = 0;
	virtual void onStart() = 0;
	virtual void onStop() = 0;

	virtual void onHangupSignal();
	virtual void onUserSignal1();
	virtual void onUserSignal2();
};

} // namespace misc

#endif // MISC_CONSOLE_APPLICATION_H
