#ifndef MISC_HARDWARE_INTERFERENCE_SIZE_H
#define MISC_HARDWARE_INTERFERENCE_SIZE_H

#include <new>

namespace misc {

// #if __cpp_lib_hardware_interference_size >= 201603

// using std::hardware_constructive_interference_size;
// using std::hardware_destructive_interference_size;
// #else
constexpr std::size_t hardware_constructive_interference_size = 64;
constexpr std::size_t hardware_destructive_interference_size = 64;
// #endif

#define ALIGN_CONSTRUCTIVE \
	alignas(misc::hardware_constructive_interference_size)
#define ALIGN_DESTRUCTIVE alignas(misc::hardware_destructive_interference_size)

} // namespace misc

#endif // MISC_HARDWARE_INTERFERENCE_SIZE_H
