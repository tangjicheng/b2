#ifndef MISC_THREADEDTASKQUEUE_H
#define MISC_THREADEDTASKQUEUE_H

#include <thread>

#include "TaskQueue.h"

namespace misc {

class ThreadedTaskQueue {
public:
	ThreadedTaskQueue() = default;
	~ThreadedTaskQueue();

	ThreadedTaskQueue(const ThreadedTaskQueue&) = delete;
	ThreadedTaskQueue& operator=(const ThreadedTaskQueue&) = delete;

	ThreadedTaskQueue(ThreadedTaskQueue&&) = delete;
	ThreadedTaskQueue& operator=(ThreadedTaskQueue&&) = delete;

	/**
	 *  @throw std::system_error If the std::thread used to run the
	 *  TaskQueue could not be started.
	 */
	void start();
	void stop();

	TaskQueue& get() { return m_taskQueue; }

	void enqueue(Task& task) { m_taskQueue.enqueue(task); }
	void drain(Task& task) { m_taskQueue.drain(task); }

private:
	TaskQueue m_taskQueue;
	std::thread m_thread;
	bool m_running {false};
};

} // namespace misc

#endif // MISC_THREADEDTASKQUEUE_H
