#ifndef MISC_THREADEDREACTOR_H
#define MISC_THREADEDREACTOR_H

#include <chrono>
#include <thread>

#include "Reactor.h"

namespace misc {

class ThreadedReactor {
public:
	using signal_system_error_type = Reactor::signal_system_error_type;
	using idle_strategy = Reactor::idle_strategy;

	signal_system_error_type& signalSystemError()
	{
		return m_reactor.signalSystemError();
	}
	const idle_strategy& idleStrategy() const
	{
		return m_reactor.idleStrategy();
	}
	void idleStrategy(idle_strategy&& idleStrategy)
	{
		m_reactor.idleStrategy(std::move(idleStrategy));
	}

	using CallId = Reactor::CallId;

	using CallStatus = Reactor::CallStatus;

	using IOCall = Reactor::IOCall;
	using RecurringCall = Reactor::RecurringCall;
	using SingleCall = Reactor::SingleCall;

	explicit ThreadedReactor(bool supportsIoCalls = true,
		bool supportsTimedCalls = true, bool supportsLoopCalls = false);
	~ThreadedReactor();

	ThreadedReactor(const ThreadedReactor&) = delete;
	ThreadedReactor& operator=(const ThreadedReactor&) = delete;

	ThreadedReactor(ThreadedReactor&&) = delete;
	ThreadedReactor& operator=(ThreadedReactor&&) = delete;

	/**
	 *  @throw std::system_error If the Reactor could not be initialized,
	 *  or, if the std::thread used to run the Reactor could not be
	 *  started.
	 */
	void start(const std::string& threadAffinity = "");
	void stop();

	Reactor& get() { return m_reactor; }

	bool supportsIoCalls() const { return m_reactor.supportsIoCalls(); }
	bool supportsTimedCalls() const { return m_reactor.supportsTimedCalls(); }
	bool supportsLoopCalls() const { return m_reactor.supportsLoopCalls(); }

	CallId callOnRead(int fd, const IOCall& call)
	{
		return m_reactor.callOnRead(fd, call);
	}
	CallId callOnWrite(int fd, const IOCall& call)
	{
		return m_reactor.callOnWrite(fd, call);
	}
	CallId callOnException(int fd, const IOCall& call)
	{
		return m_reactor.callOnException(fd, call);
	}

	CallId callEvery(
		std::chrono::microseconds timeout, const RecurringCall& call)
	{
		return m_reactor.callEvery(timeout, call);
	}
	CallId callAfter(std::chrono::microseconds timeout, const SingleCall& call)
	{
		return m_reactor.callAfter(timeout, call);
	}
	CallId callAt(
		std::chrono::system_clock::time_point expiry, const SingleCall& call)
	{
		return m_reactor.callAt(expiry, call);
	}

	CallId callInLoop(const RecurringCall& call)
	{
		return m_reactor.callInLoop(call);
	}

	void callLater(const SingleCall& call) { m_reactor.callLater(call); }
	void callNow(const SingleCall& call) { m_reactor.callNow(call); }

	void removeCall(CallId& callId) { m_reactor.removeCall(callId); }

	void idle(bool workDone) { m_reactor.idle(workDone); }
	/**
	 * Only valid after start has been called and before stop has been called.
	 */
	int get_tid() const { return m_tid; }

private:
	Reactor m_reactor;
	std::thread m_thread;
	int m_tid {-1};
	bool m_running {false};
};

} // namespace misc

#endif // MISC_THREADEDREACTOR_H
