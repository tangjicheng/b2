#ifndef MISC_LOCKEDQUEUE_H
#define MISC_LOCKEDQUEUE_H

#include <deque>
#include <mutex>
#include <optional>
#include <utility>

namespace misc {

template <typename T, typename Container = std::deque<T>>
class LockedQueue {
public:
	using value_type = typename Container::value_type;
	using pointer = typename Container::pointer;
	using const_pointer = typename Container::const_pointer;
	using reference = typename Container::reference;
	using const_reference = typename Container::const_reference;
	using size_type = typename Container::size_type;
	using allocator_type = typename Container::allocator_type;
	using container_type = Container;

	LockedQueue() = default;
	~LockedQueue() = default;

	LockedQueue(const LockedQueue&) = delete;
	LockedQueue& operator=(const LockedQueue&) = delete;

	LockedQueue(LockedQueue&&) = delete;
	LockedQueue& operator=(LockedQueue&&) = delete;

	void push(const value_type&);

	void push(value_type&&);

	template <class... Args>
	void emplace(Args&&...);

	std::optional<value_type> pop();

	size_type size() const;

	bool empty() const;

private:
	Container m_cont;
	mutable std::mutex m_mtx;
};

template <typename T, typename Container>
void LockedQueue<T, Container>::push(const value_type& val)
{
	std::lock_guard<std::mutex> lk(m_mtx);
	m_cont.push_back(val);
}

template <typename T, typename Container>
void LockedQueue<T, Container>::push(value_type&& val)
{
	std::lock_guard<std::mutex> lk(m_mtx);
	m_cont.push_back(std::move(val));
}

template <typename T, typename Container>
template <class... Args>
void LockedQueue<T, Container>::emplace(Args&&... args)
{
	std::lock_guard<std::mutex> lk(m_mtx);
	m_cont.emplace_back(std::forward<Args>(args)...);
}

template <typename T, typename Container>
std::optional<typename LockedQueue<T, Container>::value_type>
LockedQueue<T, Container>::pop()
{
	std::lock_guard<std::mutex> lk(m_mtx);
	if (m_cont.empty())
		return std::nullopt;
	auto ret = std::move(m_cont.front());
	m_cont.pop_front();
	return {std::move(ret)};
}

template <typename T, typename Container>
typename LockedQueue<T, Container>::size_type LockedQueue<T, Container>::size()
	const
{
	std::lock_guard<std::mutex> lk(m_mtx);
	return m_cont.size();
}

template <typename T, typename Container>
bool LockedQueue<T, Container>::empty() const
{
	std::lock_guard<std::mutex> lk(m_mtx);
	return m_cont.empty();
}

} // namespace misc

#endif // MISC_LOCKEDQUEUE_H
