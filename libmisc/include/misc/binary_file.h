#ifndef MISC_BINARY_FILE_H
#define MISC_BINARY_FILE_H

#include <array>
#include <cstdint>
#include <fstream>
#include <limits>
#include <string>

namespace misc {

class Binary_file_handler;
class Reactor;

class Binary_file_reader {
public:
	Binary_file_reader(Reactor&, Binary_file_handler&) noexcept;
	~Binary_file_reader() = default;

	Binary_file_reader(const Binary_file_reader&) = delete;
	Binary_file_reader& operator=(const Binary_file_reader&) = delete;

	Binary_file_reader(Binary_file_reader&&) = delete;
	Binary_file_reader& operator=(Binary_file_reader&&) = delete;

	void init(const std::string&) noexcept;

	void start() noexcept;
	void stop() noexcept;

private:
	using Length = uint16_t;
	static constexpr auto max_length = std::numeric_limits<Length>::max();

	Reactor* reactor_;
	Binary_file_handler* handler_;
	std::string filename_;
	std::ifstream file_;
	bool keep_going_ {true};
	uint64_t sequence_number_ {0};
	std::array<char, max_length> buffer_;

	void read_message_block() noexcept;
};

} // namespace misc

#endif // MISC_BINARY_FILE_H
