#ifndef ADDRINFO_ERROR_H
#define ADDRINFO_ERROR_H

#include <netdb.h>

#include <system_error>
#include <type_traits>

namespace misc {

enum class ai_errc {
	invalid_flags = EAI_BADFLAGS,
	name_or_service_not_known = EAI_NONAME,
	temporary_failure_in_name_resolution = EAI_AGAIN,
	non_recoverable_failure_in_name_resolution = EAI_FAIL,
	no_address_associated_with_name = EAI_NODATA,
	address_family_not_supported = EAI_FAMILY,
	socket_type_not_supported = EAI_SOCKTYPE,
	service_not_supported_for_socket_type = EAI_SERVICE,
	address_family_for_name_not_supported = EAI_ADDRFAMILY,
	memory_allocation_failure = EAI_MEMORY,
	system_error = EAI_SYSTEM,
	argument_buffer_overflow = EAI_OVERFLOW,
	request_in_progress = EAI_INPROGRESS,
	request_canceled = EAI_CANCELED,
	request_not_canceled = EAI_NOTCANCELED,
	all_requests_done = EAI_ALLDONE,
	interrupted = EAI_INTR,
	encoding_failure = EAI_IDN_ENCODE
};

const std::error_category& addrinfo_category();
std::error_code make_error_code(ai_errc);
std::error_condition make_error_condition(ai_errc);

} // namespace misc

namespace std {

template <>
struct is_error_code_enum<misc::ai_errc> : public true_type {
};

} // namespace std

#endif // ADDRINFO_ERROR_H
