#ifndef MISC_ATOMIC_LIST_H
#define MISC_ATOMIC_LIST_H

#include <array>
#include <atomic>
#include <cmath>
#include <cstdint>
#include <exception>
#include <tuple>

#include "hardware_interference_size.h"
#include "pow.h"

namespace misc {

template <typename T, std::size_t container_size>
class Atomic_list_element;

template <typename T, std::size_t container_size, bool bounded>
class Atomic_list_reader;

template <std::size_t container_size>
constexpr std::size_t index_mask {container_size - 1};
template <std::size_t container_size>
constexpr std::size_t index_base {
	static_cast<std::size_t>(std::log2(container_size))};

template <typename T, std::size_t container_size>
class Atomic_list {
	static_assert(
		is_power_of_two(container_size), "Container size must be power of two");

public:
	Atomic_list() = default;
	~Atomic_list()
	{
		auto ptr = front_;
		while (ptr) {
			auto next_ptr = ptr->next();
			delete ptr;
			ptr = next_ptr;
		}
	}

	template <typename Arg>
	void push_back(Arg&& data)
	{
		const auto size = size_.load(std::memory_order_acquire);
		const auto index = size & index_mask<container_size>;
		if (index == 0) {
			if (current_ != nullptr) {
				auto element = new Atomic_list_element<T, container_size> {};
				current_->set_next(element);
				current_ = element;
			}
			else
				current_ = front_;
		}

		(*current_)[index] = std::forward<Arg>(data);
		size_.store(size + 1, std::memory_order_release);
	}

	std::size_t size() const { return size_.load(std::memory_order_acquire); }

	Atomic_list_reader<T, container_size, true> bounded_reader(
		std::size_t end, std::size_t begin = 0) const
	{
		return Atomic_list_reader<T, container_size, true> {
			*this, front(), std::min(end, size()), begin};
	}

	Atomic_list_reader<T, container_size, false> unbounded_reader(
		std::size_t begin = 0) const
	{
		return Atomic_list_reader<T, container_size, false> {
			*this, front(), size(), begin};
	}

private:
	ALIGN_DESTRUCTIVE std::atomic<std::size_t> size_ {0};
	Atomic_list_element<T, container_size>* front_ {
		new Atomic_list_element<T, container_size> {}};
	Atomic_list_element<T, container_size>* current_ {nullptr};

	const Atomic_list_element<T, container_size>* front() const
	{
		return front_;
	}
};

template <typename T, std::size_t container_size, bool bounded>
class Atomic_list_reader {
public:
	std::tuple<const T*, bool> read(std::size_t position)
	{
		if (!validate_position(position))
			return {nullptr, false};

		if (position != current_position_)
			set_position(position);
		return {read_current_and_advance(), true};
	}

	std::tuple<const T*, bool> read_next()
	{
		if (!validate_position(current_position_))
			return {nullptr, false};
		return {read_current_and_advance(), true};
	}

	std::tuple<const T*, bool> peek_next()
	{
		if (!validate_position(current_position_))
			return {nullptr, false};
		return {&((*current_element_)[current_index_]), true};
	}

	bool move_next()
	{
		if (!validate_position(current_position_))
			return false;
		advance();
		return true;
	}

	void refresh_bound()
	{
		size_ = list_.size();
		if (!current_element_) {
			auto num = (current_position_ >> index_base<container_size>);
			current_element_ = front_;
			while (num-- > 0)
				current_element_ = current_element_->next();
		}
	}

	std::size_t size() const { return size_; }

private:
	friend Atomic_list<T, container_size>;
	const Atomic_list<T, container_size>& list_;
	const Atomic_list_element<T, container_size>* front_;
	std::size_t size_;
	std::size_t current_position_;
	std::size_t current_index_;
	const Atomic_list_element<T, container_size>* current_element_;

	Atomic_list_reader(const Atomic_list<T, container_size>& list,
		const Atomic_list_element<T, container_size>* front, std::size_t size,
		std::size_t position)
		: list_ {list}
		, front_ {front}
		, size_ {size}
		, current_position_ {0}
		, current_index_ {0}
	{
		set_position(position);
	}

	bool validate_position(std::size_t position)
	{
		if (position < size_)
			return true;
		if (bounded)
			return false;
		refresh_bound();
		return position < size_;
	}

	void set_position(std::size_t position)
	{
		current_position_ = position;
		auto num = (position >> index_base<container_size>);
		current_index_ = (position & index_mask<container_size>);
		current_element_ = front_;
		while (num-- > 0)
			current_element_ = current_element_->next();
	}

	const T* read_current_and_advance()
	{
		const auto output = &((*current_element_)[current_index_]);
		advance();
		return output;
	}

	inline void advance()
	{
		++current_index_;
		++current_position_;
		if (current_index_ == container_size) {
			current_index_ = 0;
			current_element_ = current_element_->next();
		}
	}
};

// for internal use
template <typename T, std::size_t container_size>
class Atomic_list_element {
public:
	Atomic_list_element() = default;

	T& operator[](std::size_t index) { return data_[index]; }
	const T& operator[](std::size_t index) const { return data_[index]; }

	void set_next(Atomic_list_element* next) { next_ = next; }
	Atomic_list_element* next() { return next_; }
	const Atomic_list_element* next() const { return next_; }

private:
	std::array<T, container_size> data_;
	Atomic_list_element* next_ {nullptr};
};

} // namespace misc

#endif // MISC_ATOMIC_LIST_H
