#ifndef MISC_INTERNAL_EXCEPTION_H
#define MISC_INTERNAL_EXCEPTION_H

#include <exception>
#include <string>
#include <utility>

namespace misc {

class InternalError : public std::exception {
public:
	InternalError(const std::string& what, const char* file, int line)
		: what_ {what}
		, file_ {file}
		, line_ {line}
	{
	}

	InternalError(std::string&& what, const char* file, int line)
		: what_ {std::move(what)}
		, file_ {file}
		, line_ {line}
	{
	}

	const char* what() const noexcept override { return what_.c_str(); }

	const char* file() const { return file_.c_str(); }

	int line() const { return line_; }

private:
	std::string what_;
	std::string file_;
	int line_;
};

} // namespace misc

#endif // MISC_INTERNAL_EXCEPTION_H
