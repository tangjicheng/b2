#ifndef MISC_BINARYMESSAGE_H
#define MISC_BINARYMESSAGE_H

#include <cstddef>
#include <cstdint>

namespace misc {

template <size_t max_size_v>
class BinaryMessage {
public:
	static_assert(max_size_v > 0, "Unsupported binary message max size");

	BinaryMessage();
	~BinaryMessage();

	BinaryMessage(const BinaryMessage&) = delete;
	BinaryMessage& operator=(const BinaryMessage&) = delete;

	BinaryMessage(BinaryMessage&&);
	BinaryMessage& operator=(BinaryMessage&&);

	explicit BinaryMessage(size_t);

	uint8_t* data() { return m_data; }

	const uint8_t* data() const { return m_data; }

	size_t size() const { return m_size; }

	bool empty() const { return m_size == 0; }

	void resize(size_t size) { m_size = size; }

	static constexpr size_t maxSize() { return max_size_v; }

private:
	uint8_t* m_data;
	size_t m_size;
};

template <size_t max_size_v>
BinaryMessage<max_size_v>::BinaryMessage()
	: BinaryMessage(max_size_v)
{
}

template <size_t max_size_v>
BinaryMessage<max_size_v>::~BinaryMessage()
{
	delete[] m_data;
}

template <size_t max_size_v>
BinaryMessage<max_size_v>::BinaryMessage(BinaryMessage&& other)
	: m_data(other.m_data)
	, m_size(other.m_size)
{
	other.m_data = nullptr;
	other.m_size = 0;
}

template <size_t max_size_v>
BinaryMessage<max_size_v>& BinaryMessage<max_size_v>::operator=(
	BinaryMessage&& other)
{
	delete[] m_data;
	m_data = other.m_data;
	m_size = other.m_size;
	other.m_data = nullptr;
	other.m_size = 0;
	return *this;
}

template <size_t max_size_v>
BinaryMessage<max_size_v>::BinaryMessage(size_t size)
	: m_data(new uint8_t[max_size_v])
	, m_size(size)
{
}

} // namespace misc

#endif // MISC_BINARYMESSAGE_H
