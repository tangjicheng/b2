#ifndef MISC_SYSLOGLOGGER_H
#define MISC_SYSLOGLOGGER_H

#include <cstdarg>
#include <exception>

namespace misc {

class SyslogLoggerError : public std::exception {
public:
	const char* what() const noexcept override { return "Syslog logger error"; }
};

class SyslogLogger {
public:
	SyslogLogger(const char*, int, int) noexcept;
	~SyslogLogger() noexcept;

private:
	static void logger(int, const char*, va_list) noexcept;
};

/**
 *  @throw SyslogLoggerError If @a facility is invalid.
 */
int toSyslogFacility(const char* facility);

} // namespace misc

#endif // MISC_SYSLOGLOGGER_H
