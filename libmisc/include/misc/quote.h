#ifndef MISC_QUOTE_H
#define MISC_QUOTE_H

#include <ostream>
#include <string>

namespace misc {

namespace details {

class quote_proxy {
public:
	explicit quote_proxy(std::ostream& os)
		: m_os(&os)
	{
	}

	template <typename T>
	std::ostream& operator<<(const T& rhs) const
	{
		return *m_os << rhs;
	}

	std::ostream& operator<<(char* rhs) const
	{
		return *m_os << '\"' << rhs << '\"';
	}

	std::ostream& operator<<(const char* rhs) const
	{
		return *m_os << '\"' << rhs << '\"';
	}

	std::ostream& operator<<(const std::string& rhs) const
	{
		return *m_os << '\"' << rhs << '\"';
	}

private:
	std::ostream* m_os;
};

} // namespace details

class quote_tag {
};

extern quote_tag quote;

inline details::quote_proxy operator<<(std::ostream& os, const quote_tag&)
{
	return details::quote_proxy(os);
}

} // namespace misc

#endif // MISC_QUOTE_H
