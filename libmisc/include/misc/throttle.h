#ifndef MISC_THROTTLE_H
#define MISC_THROTTLE_H

#include <algorithm>
#include <limits>
#include <vector>

namespace misc {

template <typename T>
class Throttle {
public:
	Throttle(size_t limit, T duration)
		: buffer_(limit, std::numeric_limits<T>::min())
		, begin_ {buffer_.begin()}
		, current_ {buffer_.begin()}
		, limit_ {limit}
		, duration_ {duration}
	{
	}

	Throttle(const Throttle&) = delete;
	Throttle& operator=(const Throttle&) = delete;

	Throttle(Throttle&&) = default;
	Throttle& operator=(Throttle&&) = default;

	T last_value() const { return last_value_; };
	bool insert(T value, bool force_insert = false);
	size_t size() const { return size_; };
	size_t rate(T value);
	size_t limit() const { return limit_; }

private:
	std::vector<T> buffer_;
	typename std::vector<T>::iterator begin_;
	typename std::vector<T>::iterator current_;
	size_t limit_;
	T duration_;
	bool first_ {true};
	T last_value_ {std::numeric_limits<T>::min()};
	size_t size_ {0};

	void update_begin(T value);
	void wrap_around(typename std::vector<T>::iterator&);
};

template <typename T>
bool Throttle<T>::insert(T value, bool force_insert)
{
	if (limit_ == 0)
		return true;
	if (value < last_value_)
		return false;
	update_begin(value);
	if (!first_) {
		auto limit = size_t {0};
		if (value > last_value_ + duration_)
			limit = 0;
		else {
			limit = (current_ >= begin_)
				? (current_ - begin_) + 1
				: (buffer_.end() - begin_) + (current_ - buffer_.begin()) + 1;
		}
		const auto limit_reached = (limit == limit_);
		if (limit_reached && !force_insert)
			return false;
		++current_;
		wrap_around(current_);
		*current_ = value;
		if (limit_reached) {
			++begin_;
			wrap_around(begin_);
		}
		else
			size_ = limit + 1;
	}
	else {
		*current_ = value;
		size_ = 1;
		first_ = false;
	}
	last_value_ = value;
	return true;
}

template <typename T>
size_t Throttle<T>::rate(T value)
{
	if (limit_ == 0 || value < last_value_)
		return size();
	update_begin(value);
	if (!first_) {
		if (value > last_value_ + duration_)
			return 0;
		return (current_ >= begin_)
			? (current_ - begin_) + 1
			: (buffer_.end() - begin_) + (current_ - buffer_.begin()) + 1;
	}
	return 0;
}

template <typename T>
void Throttle<T>::update_begin(T value)
{
	const auto to_search = (duration_ > value) ? std::numeric_limits<T>::min()
											   : value - duration_;
	if (begin_ <= current_) {
		begin_ = std::lower_bound(begin_, current_ + 1, to_search);
		wrap_around(begin_);
		return;
	}
	auto it = std::lower_bound(begin_, buffer_.end(), to_search);
	if (it != buffer_.end()) {
		begin_ = it;
		return;
	}
	begin_ = std::lower_bound(buffer_.begin(), current_ + 1, to_search);
	wrap_around(begin_);
}

template <typename T>
void Throttle<T>::wrap_around(typename std::vector<T>::iterator& it)
{
	if (it == buffer_.end())
		it = buffer_.begin();
}

} // namespace misc

#endif // MISC_THROTTLE_H
