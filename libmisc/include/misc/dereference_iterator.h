#ifndef MISC_DEREFERENCE_ITERATOR_H
#define MISC_DEREFERENCE_ITERATOR_H

#include <iterator>
#include <memory>
#include <type_traits>

#include "range.h" // dereference_iterator.h guarantees to provide range.h.

namespace misc {

template <typename Iterator>
class dereference_iterator {
private:
	using traits_type = std::iterator_traits<Iterator>;

	template <typename T>
	using pointer_element_type =
		typename std::pointer_traits<std::remove_const_t<T>>::element_type;

public:
	using iterator_type = Iterator;

	using iterator_category = typename traits_type::iterator_category;
	using difference_type = typename traits_type::difference_type;
	using value_type = pointer_element_type<typename traits_type::value_type>;
	using pointer = std::add_pointer_t<pointer_element_type<
		std::remove_pointer_t<typename traits_type::pointer>>>;
	using reference = std::add_lvalue_reference_t<pointer_element_type<
		std::remove_reference_t<typename traits_type::reference>>>;

	dereference_iterator()
		: m_current()
	{
	}

	explicit dereference_iterator(const iterator_type& i)
		: m_current(i)
	{
	}

	template <typename OtherIterator>
	dereference_iterator(const dereference_iterator<OtherIterator>& i)
		: m_current(i.base())
	{
	}

	template <typename OtherIterator>
	dereference_iterator& operator=(
		const dereference_iterator<OtherIterator>& i)
	{
		m_current = i.base();
		return *this;
	}

	const iterator_type& base() const { return m_current; }

	reference operator*() const { return **m_current; }

	pointer operator->() const { return std::addressof(**m_current); }

	reference operator[](difference_type n) const { return *m_current[n]; }

	dereference_iterator& operator++()
	{
		++m_current;
		return *this;
	}

	dereference_iterator operator++(int)
	{
		return dereference_iterator(m_current++);
	}

	dereference_iterator& operator--()
	{
		--m_current;
		return *this;
	}

	dereference_iterator operator--(int)
	{
		return dereference_iterator(m_current--);
	}

	dereference_iterator& operator+=(difference_type n)
	{
		m_current += n;
		return *this;
	}

	dereference_iterator& operator-=(difference_type n)
	{
		m_current -= n;
		return *this;
	}

private:
	iterator_type m_current;
};

template <typename Iterator>
dereference_iterator<Iterator> operator+(
	const dereference_iterator<Iterator>& i,
	typename dereference_iterator<Iterator>::difference_type n)
{
	return dereference_iterator<Iterator>(i.base() + n);
}

template <typename Iterator>
dereference_iterator<Iterator> operator+(
	typename dereference_iterator<Iterator>::difference_type n,
	const dereference_iterator<Iterator>& i)
{
	return dereference_iterator<Iterator>(n + i.base());
}

template <typename Iterator>
dereference_iterator<Iterator> operator-(
	const dereference_iterator<Iterator>& i,
	typename dereference_iterator<Iterator>::difference_type n)
{
	return dereference_iterator<Iterator>(i.base() - n);
}

template <typename IteratorL, typename IteratorR>
auto operator-(const dereference_iterator<IteratorL>& lhs,
	const dereference_iterator<IteratorR>& rhs)
	-> decltype(lhs.base() - rhs.base())
{
	return lhs.base() - rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator==(const dereference_iterator<IteratorL>& lhs,
	const dereference_iterator<IteratorR>& rhs)
{
	return lhs.base() == rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator!=(const dereference_iterator<IteratorL>& lhs,
	const dereference_iterator<IteratorR>& rhs)
{
	return lhs.base() != rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator<(const dereference_iterator<IteratorL>& lhs,
	const dereference_iterator<IteratorR>& rhs)
{
	return lhs.base() < rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator>(const dereference_iterator<IteratorL>& lhs,
	const dereference_iterator<IteratorR>& rhs)
{
	return lhs.base() > rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator<=(const dereference_iterator<IteratorL>& lhs,
	const dereference_iterator<IteratorR>& rhs)
{
	return lhs.base() <= rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator>=(const dereference_iterator<IteratorL>& lhs,
	const dereference_iterator<IteratorR>& rhs)
{
	return lhs.base() >= rhs.base();
}

template <typename Iterator>
dereference_iterator<Iterator> make_dereference_iterator(const Iterator& i)
{
	return dereference_iterator<Iterator>(i);
}

template <typename Iterator>
range<dereference_iterator<Iterator>> deref(
	const Iterator& b, const Iterator& e)
{
	return range<dereference_iterator<Iterator>>(b, e);
}

template <typename Range>
auto deref(Range& r) -> range<dereference_iterator<decltype(std::begin(r))>>
{
	return decltype(deref(r))(std::begin(r), std::end(r));
}

template <typename Range>
auto deref(const Range& r)
	-> range<dereference_iterator<decltype(std::begin(r))>>
{
	return decltype(deref(r))(std::begin(r), std::end(r));
}

} // namespace misc

#endif // MISC_DEREFERENCE_ITERATOR_H
