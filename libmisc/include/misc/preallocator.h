#ifndef MISC_PREALLOCATOR_H
#define MISC_PREALLOCATOR_H

#include <cstdint>
#include <cstdlib>
#include <stdexcept>
#include <tuple>
#include <type_traits>

#include <boost/core/demangle.hpp>
#include <yaml-cpp/yaml.h>

#include "Log.h"
#include "form.h"
#include "hardware_interference_size.h"
#include "preallocated_vector.h"

namespace misc {

class Preallocator_memory_exception : public std::runtime_error {
public:
	using std::runtime_error::runtime_error;
};

struct Preallocator_settings {
	std::int64_t capacity {0};

	Preallocator_settings() = default;
	Preallocator_settings(const Preallocator_settings&) = default;
	Preallocator_settings& operator=(const Preallocator_settings&) = default;
	Preallocator_settings(Preallocator_settings&&) = default;
	Preallocator_settings& operator=(Preallocator_settings&&) = default;

	explicit Preallocator_settings(std::int64_t capacity)
		: capacity {capacity}
	{
		if (capacity < 0) {
			throw Preallocator_memory_exception {
				form("Invalid capacity: %ld", capacity).c_str()};
		}
	}
};

inline Preallocator_settings preallocator_settings_from_yaml(
	const YAML::Node& conf)
{
	return Preallocator_settings {conf["capacity"].as<std::int64_t>()};
}

template <typename T>
class Preallocator {
public:
	static_assert(std::is_default_constructible_v<T>);

	static constexpr std::int64_t cache_line_size
		= hardware_constructive_interference_size;

	static constexpr std::int64_t aligned_element_size
		= sizeof(std::aligned_storage_t<sizeof(T), alignof(T)>);

	static constexpr std::int64_t cache_aligned_element_size()
	{
		// TODO: For now, when multiple elements can fit in one cache
		// line, only allow a power of two per cache line.
		auto align_to = cache_line_size;
		auto result = 0LL;
		while (true) {
			result = align_to * ((aligned_element_size - 1) / align_to + 1);
			if (result > align_to || aligned_element_size > (align_to >> 1))
				break;
			align_to >>= 1;
		}
		return result;
	}

	Preallocator() = default;
	Preallocator(const Preallocator&) = delete;
	Preallocator& operator=(const Preallocator&) = delete;
	Preallocator(Preallocator<T>&&) = default;
	Preallocator& operator=(Preallocator&&) = default;

	explicit Preallocator(Preallocator_settings&& settings)
		: settings_ {std::move(settings)}
		, elements_(settings_.capacity)
	{
		if (settings_.capacity > 1)
			verify_alignment();

		free_list_.reserve(settings_.capacity);
		for (auto& aligner : elements_)
			free_list_.push_back(&aligner.element());
	}

	explicit Preallocator(const YAML::Node& conf)
		: Preallocator {preallocator_settings_from_yaml(conf)}
	{
	}

	T& acquire()
	{
		if (!can_reuse())
			throw Preallocator_memory_exception {
				form("Preallocator reached capacity: %ld", capacity())};
		return reuse();
	}

	void release(T& element)
	{
		free_list_.push_back(&element);
		--acquired_;
	}

	std::int64_t capacity() const { return settings_.capacity; }
	std::int64_t size() const { return acquired_; }

	std::tuple<std::size_t, std::size_t> get_memory_status() const
	{
		return {size(), capacity()};
	}
	bool is_full() const { return size() >= capacity(); }
	bool can_fit(std::int64_t count) const
	{
		return (size() + count) <= capacity();
	}

	void log_info() const
	{
		appLog(APP_LOG_INFO,
			"Preallocator:\n"
			"	acquired: %ld\n"
			"	capacity: %ld\n"
			"	%s size (aligned): %ld\n"
			"	%s size (cache line aligned): %ld",
			size(), capacity(), boost::core::demangle(typeid(T).name()).c_str(),
			aligned_element_size,
			boost::core::demangle(typeid(T).name()).c_str(),
			cache_aligned_element_size());
	}

private:
	// Using boost::align::aligned_allocator with Preallocated_vector does
	// not work for sizeof(T) == 24 so wrap T in class Aligner instead.
	// TODO: class Preallocator does not perform page alignment so T must
	// fit into a number of cache lines that evenly divides into the page
	// size. For Linux x64, where page size is 4096, the alignas keyword
	// below asserts this condition since alignas only accepts powers of
	// two.
	class alignas(cache_aligned_element_size()) Aligner {
	public:
		T& element() { return element_; }

	private:
		T element_;
	};

	Preallocator_settings settings_;
	Preallocated_vector<Aligner> elements_;
	Preallocated_vector<T*> free_list_;
	std::int64_t acquired_ {0};

	bool can_reuse() const { return !free_list_.empty(); }

	T& reuse()
	{
		auto& element = *free_list_.back();
		++acquired_;
		free_list_.pop_back();
		return element;
	}

	void verify_alignment() const
	{
		const auto base_addr = reinterpret_cast<std::intptr_t>(&elements_[0]);
		if (base_addr % cache_aligned_element_size() != 0) {
			throw Preallocator_memory_exception {
				form("Over-alignment to cache line not respected. Target: %ld "
					 "Base address: %ld",
					cache_aligned_element_size(), base_addr)
					.c_str()};
		}

		const auto alignment
			= reinterpret_cast<std::intptr_t>(&elements_[1]) - base_addr;
		if (alignment != cache_aligned_element_size()) {
			throw Preallocator_memory_exception {
				form("Over-alignment to cache line not respected. Target: %ld "
					 "Actual: %ld",
					cache_aligned_element_size(), alignment)
					.c_str()};
		}
	}
};

} // namespace misc

#endif // MISC_PREALLOCATOR_H
