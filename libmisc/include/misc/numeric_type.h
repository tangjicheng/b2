#ifndef MISC_NUMERIC_TYPE_H
#define MISC_NUMERIC_TYPE_H

#include <cassert>
#include <cstddef>
#include <functional>
#include <iomanip>
#include <istream>
#include <limits>
#include <ostream>
#include <sstream>
#include <string>
#include <type_traits>

#include "abs.h"
#include "pow.h"

namespace misc {

namespace details {

constexpr unsigned int Numeric_type_decimals_unspecified
	= std::numeric_limits<unsigned int>::max();

} // namespace details

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
class Numeric_type {
public:
	static_assert(std::is_integral_v<Underlying_type_t>,
		"Template argument Underlying_type_t not integral type");

	static_assert(decimals_v == details::Numeric_type_decimals_unspecified
			|| decimals_v
				<= std::numeric_limits<Underlying_type_t>::digits10 + 1,
		"Template argument decimals_v too large for Underlying_type_t");

	using Underlying_type = Underlying_type_t;

	Numeric_type() = default;

	constexpr explicit Numeric_type(Underlying_type value) noexcept
		: value_ {value}
	{
	}

	constexpr Underlying_type value() const noexcept { return value_; }

	static constexpr bool decimals_specified() noexcept
	{
		return decimals_v != details::Numeric_type_decimals_unspecified;
	}

	static constexpr unsigned int decimals() noexcept
	{
		static_assert(decimals_specified(),
			"The decimals() function is not available when decimals is "
			"unspecified");
		return decimals_v;
	}

	static constexpr Numeric_type min() noexcept
	{
		return Numeric_type {std::numeric_limits<Underlying_type>::min()};
	}

	static constexpr Numeric_type max() noexcept
	{
		return Numeric_type {std::numeric_limits<Underlying_type>::max()};
	}

	constexpr Numeric_type& operator++() noexcept
	{
		++value_;
		return *this;
	}

	constexpr Numeric_type& operator--() noexcept
	{
		--value_;
		return *this;
	}

	constexpr Numeric_type operator++(int) noexcept
	{
		const auto old = *this;
		operator++();
		return old;
	}

	constexpr Numeric_type operator--(int) noexcept
	{
		const auto old = *this;
		operator--();
		return old;
	}

	constexpr Numeric_type& operator+=(Numeric_type rhs) noexcept
	{
		value_ += rhs.value_;
		return *this;
	}

	constexpr Numeric_type& operator-=(Numeric_type rhs) noexcept
	{
		value_ -= rhs.value_;
		return *this;
	}

	template <typename T>
	constexpr std::enable_if_t<std::is_arithmetic_v<T>, Numeric_type&>
	operator*=(T rhs) noexcept
	{
		value_ *= rhs;
		return *this;
	}

	template <typename T>
	constexpr std::enable_if_t<std::is_arithmetic_v<T>, Numeric_type&>
	operator/=(T rhs) noexcept
	{
		value_ /= rhs;
		return *this;
	}

	constexpr Numeric_type& operator%=(Numeric_type rhs) noexcept
	{
		value_ %= rhs.value_;
		return *this;
	}

	template <typename T>
	constexpr std::enable_if_t<std::is_arithmetic_v<T>, Numeric_type&>
	operator%=(T rhs) noexcept
	{
		value_ %= rhs;
		return *this;
	}

private:
	Underlying_type value_ {0};
};

namespace details {

template <typename>
struct Numeric_type_parameters;

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
struct Numeric_type_parameters<
	Numeric_type<Tag_t, Underlying_type_t, decimals_v>> {
	using Tag = Tag_t;
	using Underlying_type = Underlying_type_t;
	static constexpr auto decimals = decimals_v;
};

} // namespace details

// Numeric cast

template <typename To_numeric_type_t, typename Tag_t,
	typename Underlying_type_t, unsigned int decimals_v>
constexpr auto numeric_cast(
	Numeric_type<Tag_t, Underlying_type_t, decimals_v> from) noexcept
	-> std::enable_if_t<decltype(from)::decimals_specified()
			&& To_numeric_type_t::decimals_specified(),
		To_numeric_type_t>
{
	using Common_underlying_type
		= std::common_type_t<typename decltype(from)::Underlying_type,
			typename To_numeric_type_t::Underlying_type>;
	constexpr auto digits10
		= std::numeric_limits<Common_underlying_type>::digits10;
	if constexpr (To_numeric_type_t::decimals() < from.decimals()) {
		constexpr auto decimals_decrease
			= from.decimals() - To_numeric_type_t::decimals();
		static_assert(decimals_decrease <= digits10,
			"Decimals decrease too large for common underlying type");
		return static_cast<To_numeric_type_t>(from.value()
			/ misc::Pow_v<Common_underlying_type, 10, decimals_decrease>);
	}
	else {
		constexpr auto decimals_increase
			= To_numeric_type_t::decimals() - from.decimals();
		static_assert(decimals_increase <= digits10,
			"Decimals increase too large for common underlying type");
		return static_cast<To_numeric_type_t>(from.value()
			* misc::Pow_v<Common_underlying_type, 10, decimals_increase>);
	}
}

namespace details {

template <typename To_numeric_type_t, typename From_numeric_type_t>
auto numeric_cast_impl(From_numeric_type_t from, unsigned int from_decimals,
	unsigned int to_decimals) noexcept
{
	using Common_underlying_type
		= std::common_type_t<typename From_numeric_type_t::Underlying_type,
			typename To_numeric_type_t::Underlying_type>;
	[[maybe_unused]] constexpr auto digits10
		= std::numeric_limits<Common_underlying_type>::digits10;
	if (to_decimals < from_decimals) {
		const auto decimals_decrease = from_decimals - to_decimals;
		assert(decimals_decrease <= digits10); // Decimals decrease too large
											   // for common underlying type.
		return static_cast<To_numeric_type_t>(from.value()
			/ misc::pow<Common_underlying_type>(10, decimals_decrease));
	}
	else {
		const auto decimals_increase = to_decimals - from_decimals;
		assert(decimals_increase <= digits10); // Decimals increase too large
											   // for common underlying type.
		return static_cast<To_numeric_type_t>(from.value()
			* misc::pow<Common_underlying_type>(10, decimals_increase));
	}
}

} // namespace details

template <typename To_numeric_type_t, typename Tag_t,
	typename Underlying_type_t, unsigned int decimals_v>
auto numeric_cast(Numeric_type<Tag_t, Underlying_type_t, decimals_v> from,
	unsigned int from_decimals) noexcept
	-> std::enable_if_t<!decltype(from)::decimals_specified()
			&& To_numeric_type_t::decimals_specified(),
		To_numeric_type_t>
{
	return details::numeric_cast_impl<To_numeric_type_t>(
		from, from_decimals, To_numeric_type_t::decimals());
}

template <typename To_numeric_type_t, typename Tag_t,
	typename Underlying_type_t, unsigned int decimals_v>
auto numeric_cast(Numeric_type<Tag_t, Underlying_type_t, decimals_v> from,
	unsigned int to_decimals) noexcept
	-> std::enable_if_t<decltype(from)::decimals_specified()
			&& !To_numeric_type_t::decimals_specified(),
		To_numeric_type_t>
{
	return details::numeric_cast_impl<To_numeric_type_t>(
		from, from.decimals(), to_decimals);
}

template <typename To_numeric_type_t, typename Tag_t,
	typename Underlying_type_t, unsigned int decimals_v>
auto numeric_cast(Numeric_type<Tag_t, Underlying_type_t, decimals_v> from,
	unsigned int from_decimals, unsigned int to_decimals) noexcept
	-> std::enable_if_t<!decltype(from)::decimals_specified()
			&& !To_numeric_type_t::decimals_specified(),
		To_numeric_type_t>
{
	return details::numeric_cast_impl<To_numeric_type_t>(
		from, from_decimals, to_decimals);
}

// Arithmetic operators

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr auto operator+(Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept -> decltype(lhs)
{
	return decltype(lhs) {lhs.value() + rhs.value()};
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr auto operator-(Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept -> decltype(lhs)
{
	return decltype(lhs) {lhs.value() - rhs.value()};
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr auto operator-(
	Numeric_type<Tag_t, Underlying_type_t, decimals_v> rhs) noexcept
	-> std::enable_if_t<std::is_signed_v<Underlying_type_t>, decltype(rhs)>
{
	return decltype(rhs) {-rhs.value()};
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v,
	typename T>
constexpr auto operator*(Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	T rhs) noexcept -> std::enable_if_t<std::is_arithmetic_v<T>, decltype(lhs)>
{
	return static_cast<decltype(lhs)>(lhs.value() * rhs);
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v,
	typename T>
constexpr auto operator*(
	T lhs, Numeric_type<Tag_t, Underlying_type_t, decimals_v> rhs) noexcept
	-> std::enable_if_t<std::is_arithmetic_v<T>, decltype(rhs)>
{
	return static_cast<decltype(rhs)>(lhs * rhs.value());
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr auto operator/(Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept -> typename decltype(lhs)::Underlying_type
{
	return lhs.value() / rhs.value();
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v,
	typename T>
constexpr auto operator/(Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	T rhs) noexcept -> std::enable_if_t<std::is_arithmetic_v<T>, decltype(lhs)>
{
	return static_cast<decltype(lhs)>(lhs.value() / rhs);
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr auto operator%(Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept -> decltype(lhs)
{
	return decltype(lhs) {lhs.value() % rhs.value()};
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v,
	typename T>
constexpr auto operator%(Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	T rhs) noexcept -> std::enable_if_t<std::is_arithmetic_v<T>, decltype(lhs)>
{
	return static_cast<decltype(lhs)>(lhs.value() % rhs);
}

// Comparison operators

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr bool operator==(
	Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept
{
	return lhs.value() == rhs.value();
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr bool operator!=(
	Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept
{
	return lhs.value() != rhs.value();
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr bool operator<(Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept
{
	return lhs.value() < rhs.value();
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr bool operator>(Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept
{
	return lhs.value() > rhs.value();
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr bool operator<=(
	Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept
{
	return lhs.value() <= rhs.value();
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr bool operator>=(
	Numeric_type<Tag_t, Underlying_type_t, decimals_v> lhs,
	decltype(lhs) rhs) noexcept
{
	return lhs.value() >= rhs.value();
}

// std::string conversion

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
auto to_string(Numeric_type<Tag_t, Underlying_type_t, decimals_v> n) noexcept
	-> std::enable_if_t<decltype(n)::decimals_specified() && decimals_v == 0,
		std::string>
{
	return std::to_string(n.value());
}

namespace details {

template <typename Underlying_type>
std::string numeric_type_unsigned_to_string(
	Underlying_type value, unsigned int decimals) noexcept
{
	std::ostringstream oss;
	oss << value / misc::pow<Underlying_type>(10, decimals) << '.'
		<< std::setfill('0') << std::setw(decimals)
		<< value % misc::pow<Underlying_type>(10, decimals);
	return oss.str();
}

} // namespace details

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
auto to_string(Numeric_type<Tag_t, Underlying_type_t, decimals_v> n) noexcept
	-> std::enable_if_t<decltype(n)::decimals_specified() && decimals_v != 0
			&& std::is_unsigned_v<Underlying_type_t>,
		std::string>
{
	return details::numeric_type_unsigned_to_string(n.value(), n.decimals());
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
auto to_string(Numeric_type<Tag_t, Underlying_type_t, decimals_v> n,
	unsigned int decimals) noexcept
	-> std::enable_if_t<!decltype(n)::decimals_specified()
			&& std::is_unsigned_v<Underlying_type_t>,
		std::string>
{
	if (decimals == 0)
		return std::to_string(n.value());
	else
		return details::numeric_type_unsigned_to_string(n.value(), decimals);
}

namespace details {

template <typename Underlying_type>
std::string numeric_type_signed_to_string(
	Underlying_type value, unsigned int decimals) noexcept
{
	std::ostringstream oss;
	if (value < 0)
		oss << '-';
	using Unsigned_underlying_type = std::make_unsigned_t<Underlying_type>;
	const auto abs_value = value == std::numeric_limits<Underlying_type>::min()
		? static_cast<Unsigned_underlying_type>(
			  std::numeric_limits<Underlying_type>::max())
			+ static_cast<Unsigned_underlying_type>(1)
		: static_cast<Unsigned_underlying_type>(misc::abs(value));
	oss << abs_value / misc::pow<Unsigned_underlying_type>(10, decimals) << '.'
		<< std::setfill('0') << std::setw(decimals)
		<< abs_value % misc::pow<Unsigned_underlying_type>(10, decimals);
	return oss.str();
}

} // namespace details

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
auto to_string(Numeric_type<Tag_t, Underlying_type_t, decimals_v> n) noexcept
	-> std::enable_if_t<decltype(n)::decimals_specified() && decimals_v != 0
			&& std::is_signed_v<Underlying_type_t>,
		std::string>
{
	return details::numeric_type_signed_to_string(n.value(), n.decimals());
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
auto to_string(Numeric_type<Tag_t, Underlying_type_t, decimals_v> n,
	unsigned int decimals) noexcept
	-> std::enable_if_t<!decltype(n)::decimals_specified()
			&& std::is_signed_v<Underlying_type_t>,
		std::string>
{
	if (decimals == 0)
		return std::to_string(n.value());
	else
		return details::numeric_type_signed_to_string(n.value(), decimals);
}

// Double conversion

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr auto to_double(
	Numeric_type<Tag_t, Underlying_type_t, decimals_v> n) noexcept
	-> std::enable_if_t<decltype(n)::decimals_specified(), double>
{
	return n.value()
		/ static_cast<double>(
			Pow_v<typename decltype(n)::Underlying_type, 10, n.decimals()>);
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
constexpr auto to_double(Numeric_type<Tag_t, Underlying_type_t, decimals_v> n,
	unsigned int decimals) noexcept
	-> std::enable_if_t<!decltype(n)::decimals_specified(), double>
{
	return n.value()
		/ static_cast<double>(
			misc::pow<typename decltype(n)::Underlying_type>(10, decimals));
}

// Stream operators

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
auto operator<<(
	std::ostream& os, Numeric_type<Tag_t, Underlying_type_t, decimals_v> rhs)
	-> std::enable_if_t<decltype(rhs)::decimals_specified(), std::ostream&>
{
	return os << to_string(rhs);
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
auto operator>>(
	std::istream& is, Numeric_type<Tag_t, Underlying_type_t, decimals_v>& rhs)
	-> std::enable_if_t<
		std::remove_reference_t<decltype(rhs)>::decimals_specified()
			&& decimals_v == 0,
		std::istream&>
{
	using T = std::remove_reference_t<decltype(rhs)>;
	typename T::Underlying_type value;
	if (is >> value)
		rhs = T {value};
	return is;
}

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
auto operator>>(
	std::istream& is, Numeric_type<Tag_t, Underlying_type_t, decimals_v>& rhs)
	-> std::enable_if_t<
		std::remove_reference_t<decltype(rhs)>::decimals_specified()
			&& decimals_v != 0,
		std::istream&>
{
	using T = std::remove_reference_t<decltype(rhs)>;
	double value;
	if (is >> value)
		rhs = static_cast<T>(
			value * Pow_v<typename T::Underlying_type, 10, T::decimals()>);
	return is;
}

// Class declaration macro

#define MISC_NUMERIC_TYPE_DECL(numeric_type_name, underlying_type, decimals) \
	class numeric_type_name##_tag;                                           \
	using numeric_type_name = misc::Numeric_type<numeric_type_name##_tag,    \
		underlying_type, decimals>;

#define MISC_NUMERIC_TYPE_DECIMALS_UNSPECIFIED_DECL(           \
	numeric_type_name, underlying_type)                        \
	MISC_NUMERIC_TYPE_DECL(numeric_type_name, underlying_type, \
		misc::details::Numeric_type_decimals_unspecified)

#define MISC_NUMERIC_TYPE_BASED_ON_DECL(numeric_type_name, numeric_type_base) \
	MISC_NUMERIC_TYPE_DECL(numeric_type_name,                                 \
		numeric_type_base::Underlying_type,                                   \
		misc::details::Numeric_type_parameters<numeric_type_base>::decimals)

} // namespace misc

namespace std {

// std::hash specialization

template <typename Tag_t, typename Underlying_type_t, unsigned int decimals_v>
struct hash<misc::Numeric_type<Tag_t, Underlying_type_t, decimals_v>> {
	using argument_type
		= misc::Numeric_type<Tag_t, Underlying_type_t, decimals_v>;
	using result_type = size_t;

	result_type operator()(const argument_type& n) const noexcept
	{
		return std::hash<typename argument_type::Underlying_type> {}(n.value());
	}
};

} // namespace std

#endif // MISC_NUMERIC_TYPE_H
