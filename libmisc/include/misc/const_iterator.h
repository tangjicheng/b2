#ifndef MISC_CONST_ITERATOR_H
#define MISC_CONST_ITERATOR_H

#include <iterator>
#include <memory>
#include <type_traits>

#include "range.h" // const_iterator.h guarantees to provide range.h.

namespace misc {

template <typename Iterator>
class const_iterator {
private:
	using traits_type = std::iterator_traits<Iterator>;

public:
	using iterator_type = Iterator;

	using iterator_category = typename traits_type::iterator_category;
	using difference_type = typename traits_type::difference_type;
	using value_type = typename traits_type::value_type;
	using pointer = std::add_pointer_t<
		std::add_const_t<std::remove_pointer_t<typename traits_type::pointer>>>;
	using reference = std::add_lvalue_reference_t<std::add_const_t<
		std::remove_reference_t<typename traits_type::reference>>>;

	const_iterator()
		: m_current()
	{
	}

	explicit const_iterator(const iterator_type& i)
		: m_current(i)
	{
	}

	const_iterator& operator=(const iterator_type& i)
	{
		m_current = i;
		return *this;
	}

	template <typename OtherIterator>
	const_iterator(const const_iterator<OtherIterator>& i)
		: m_current(i.base())
	{
	}

	template <typename OtherIterator>
	const_iterator& operator=(const const_iterator<OtherIterator>& i)
	{
		m_current = i.base();
		return *this;
	}

	const iterator_type& base() const { return m_current; }

	reference operator*() const { return *m_current; }

	pointer operator->() const { return std::addressof(*m_current); }

	reference operator[](difference_type n) const { return m_current[n]; }

	const_iterator& operator++()
	{
		++m_current;
		return *this;
	}

	const_iterator operator++(int) { return const_iterator(m_current++); }

	const_iterator& operator--()
	{
		--m_current;
		return *this;
	}

	const_iterator operator--(int) { return const_iterator(m_current--); }

	const_iterator& operator+=(difference_type n)
	{
		m_current += n;
		return *this;
	}

	const_iterator& operator-=(difference_type n)
	{
		m_current -= n;
		return *this;
	}

private:
	iterator_type m_current;
};

template <typename Iterator>
const_iterator<Iterator> operator+(const const_iterator<Iterator>& i,
	typename const_iterator<Iterator>::difference_type n)
{
	return const_iterator<Iterator>(i.base() + n);
}

template <typename Iterator>
const_iterator<Iterator> operator+(
	typename const_iterator<Iterator>::difference_type n,
	const const_iterator<Iterator>& i)
{
	return const_iterator<Iterator>(n + i.base());
}

template <typename Iterator>
const_iterator<Iterator> operator-(const const_iterator<Iterator>& i,
	typename const_iterator<Iterator>::difference_type n)
{
	return const_iterator<Iterator>(i.base() - n);
}

template <typename IteratorL, typename IteratorR>
auto operator-(const const_iterator<IteratorL>& lhs,
	const const_iterator<IteratorR>& rhs) -> decltype(lhs.base() - rhs.base())
{
	return lhs.base() - rhs.base();
}

template <typename IteratorL, typename IteratorR>
auto operator-(const const_iterator<IteratorL>& lhs, const IteratorR& rhs)
	-> decltype(lhs.base() - rhs)
{
	return lhs.base() - rhs;
}

template <typename IteratorL, typename IteratorR>
auto operator-(const IteratorL& lhs, const const_iterator<IteratorR>& rhs)
	-> decltype(lhs - rhs.base())
{
	return lhs - rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator==(
	const const_iterator<IteratorL>& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs.base() == rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator==(const const_iterator<IteratorL>& lhs, const IteratorR& rhs)
{
	return lhs.base() == rhs;
}

template <typename IteratorL, typename IteratorR>
bool operator==(const IteratorL& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs == rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator!=(
	const const_iterator<IteratorL>& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs.base() != rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator!=(const const_iterator<IteratorL>& lhs, const IteratorR& rhs)
{
	return lhs.base() != rhs;
}

template <typename IteratorL, typename IteratorR>
bool operator!=(const IteratorL& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs != rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator<(
	const const_iterator<IteratorL>& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs.base() < rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator<(const const_iterator<IteratorL>& lhs, const IteratorR& rhs)
{
	return lhs.base() < rhs;
}

template <typename IteratorL, typename IteratorR>
bool operator<(const IteratorL& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs < rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator>(
	const const_iterator<IteratorL>& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs.base() > rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator>(const const_iterator<IteratorL>& lhs, const IteratorR& rhs)
{
	return lhs.base() > rhs;
}

template <typename IteratorL, typename IteratorR>
bool operator>(const IteratorL& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs > rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator<=(
	const const_iterator<IteratorL>& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs.base() <= rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator<=(const const_iterator<IteratorL>& lhs, const IteratorR& rhs)
{
	return lhs.base() <= rhs;
}

template <typename IteratorL, typename IteratorR>
bool operator<=(const IteratorL& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs <= rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator>=(
	const const_iterator<IteratorL>& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs.base() >= rhs.base();
}

template <typename IteratorL, typename IteratorR>
bool operator>=(const const_iterator<IteratorL>& lhs, const IteratorR& rhs)
{
	return lhs.base() >= rhs;
}

template <typename IteratorL, typename IteratorR>
bool operator>=(const IteratorL& lhs, const const_iterator<IteratorR>& rhs)
{
	return lhs >= rhs.base();
}

template <typename Iterator>
const_iterator<Iterator> make_const_iterator(const Iterator& i)
{
	return const_iterator<Iterator>(i);
}

template <typename Iterator>
range<const_iterator<Iterator>> constify(const Iterator& b, const Iterator& e)
{
	return range<const_iterator<Iterator>>(b, e);
}

template <typename Range>
auto constify(Range& r) -> range<const_iterator<decltype(std::begin(r))>>
{
	return decltype(constify(r))(std::begin(r), std::end(r));
}

template <typename Range>
auto constify(const Range& r) -> range<const_iterator<decltype(std::begin(r))>>
{
	return decltype(constify(r))(std::begin(r), std::end(r));
}

} // namespace misc

#endif // MISC_CONST_ITERATOR_H
