#ifndef MISC_MAP_ITERATOR_H
#define MISC_MAP_ITERATOR_H

#include <memory>
#include <type_traits>

#include "range.h" // map_iterator.h guarantees to provide range.h.

namespace misc {

namespace details {

struct ref_traits {
	template <typename T>
	struct value_type {
		typedef T type;
	};

	template <typename T>
	static typename value_type<T>::type& dereference(T& t)
	{
		return t;
	}

	template <typename T>
	static typename value_type<T>::type* address_of(T& t)
	{
		return &t;
	}
};

struct ptr_traits {
	template <typename T>
	struct value_type {
		typedef typename std::pointer_traits<
			typename std::remove_const<T>::type>::element_type type;
	};

	template <typename T>
	static typename value_type<T>::type& dereference(T& t)
	{
		return *t;
	}

	template <typename T>
	static typename value_type<T>::type* address_of(T& t)
	{
		return &*t;
	}
};

template <typename map_type, typename traits>
class map_data_const_iterator;

template <typename map_type, typename traits>
class map_data_iterator {
	friend class map_data_const_iterator<map_type, traits>;

	typedef map_data_iterator<map_type, traits> self;
	typedef typename map_type::iterator iterator;

public:
	typedef typename iterator::iterator_category iterator_category;
	typedef typename iterator::difference_type difference_type;
	typedef typename traits::template value_type<
		typename map_type::mapped_type>::type value_type;
	typedef value_type* pointer;
	typedef value_type& reference;

	map_data_iterator() = default;
	explicit map_data_iterator(const iterator& iter)
		: m_iter(iter)
	{
	}

	const iterator& base() const { return m_iter; }

	reference operator*() const { return traits::dereference(m_iter->second); }
	pointer operator->() const { return traits::address_of(m_iter->second); }

	self& operator++()
	{
		++m_iter;
		return *this;
	}
	self operator++(int)
	{
		auto tmp = *this;
		++m_iter;
		return tmp;
	}

	self& operator--()
	{
		--m_iter;
		return *this;
	}
	self operator--(int)
	{
		auto tmp = *this;
		--m_iter;
		return tmp;
	}

	bool operator==(const self& rhs) const { return m_iter == rhs.m_iter; }
	bool operator!=(const self& rhs) const { return m_iter != rhs.m_iter; }

private:
	iterator m_iter;
};

template <typename map_type, typename traits>
class map_data_const_iterator {
	typedef map_data_const_iterator<map_type, traits> self;
	typedef typename map_type::const_iterator const_iterator;

public:
	typedef typename const_iterator::iterator_category iterator_category;
	typedef typename const_iterator::difference_type difference_type;
	typedef typename traits::template value_type<
		typename map_type::mapped_type>::type value_type;
	typedef const value_type* pointer;
	typedef const value_type& reference;

	map_data_const_iterator() = default;
	explicit map_data_const_iterator(const const_iterator& iter)
		: m_iter(iter)
	{
	}
	map_data_const_iterator(const map_data_iterator<map_type, traits>& i)
		: m_iter(i.m_iter)
	{
	}

	const const_iterator& base() const { return m_iter; }

	reference operator*() const { return traits::dereference(m_iter->second); }
	pointer operator->() const { return traits::address_of(m_iter->second); }

	self& operator++()
	{
		++m_iter;
		return *this;
	}
	self operator++(int)
	{
		auto tmp = *this;
		++m_iter;
		return tmp;
	}

	self& operator--()
	{
		--m_iter;
		return *this;
	}
	self operator--(int)
	{
		auto tmp = *this;
		--m_iter;
		return tmp;
	}

	bool operator==(const self& rhs) const { return m_iter == rhs.m_iter; }
	bool operator!=(const self& rhs) const { return m_iter != rhs.m_iter; }

private:
	const_iterator m_iter;
};

template <typename map_type, typename traits>
class map_key_const_iterator {
	typedef map_key_const_iterator<map_type, traits> self;
	typedef typename map_type::const_iterator const_iterator;

public:
	typedef typename const_iterator::iterator_category iterator_category;
	typedef typename const_iterator::difference_type difference_type;
	typedef
		typename traits::template value_type<typename map_type::key_type>::type
			value_type;
	typedef const value_type* pointer;
	typedef const value_type& reference;

	map_key_const_iterator() = default;
	explicit map_key_const_iterator(const const_iterator& iter)
		: m_iter(iter)
	{
	}

	const const_iterator& base() const { return m_iter; }

	reference operator*() const { return traits::dereference(m_iter->first); }
	pointer operator->() const { return traits::address_of(m_iter->first); }

	self& operator++()
	{
		++m_iter;
		return *this;
	}
	self operator++(int)
	{
		auto tmp = *this;
		++m_iter;
		return tmp;
	}

	self& operator--()
	{
		--m_iter;
		return *this;
	}
	self operator--(int)
	{
		auto tmp = *this;
		--m_iter;
		return tmp;
	}

	bool operator==(const self& rhs) const { return m_iter == rhs.m_iter; }
	bool operator!=(const self& rhs) const { return m_iter != rhs.m_iter; }

private:
	const_iterator m_iter;
};

template <typename map_type, typename traits>
bool operator==(const map_data_iterator<map_type, traits>& lhs,
	const map_data_const_iterator<map_type, traits>& rhs)
{
	return map_data_const_iterator<map_type, traits>(lhs) == rhs;
}

template <typename map_type, typename traits>
bool operator!=(const map_data_iterator<map_type, traits>& lhs,
	const map_data_const_iterator<map_type, traits>& rhs)
{
	return map_data_const_iterator<map_type, traits>(lhs) != rhs;
}

} // namespace details

template <typename map_type>
struct map_data_ref_iterator {
	typedef details::map_data_iterator<map_type, details::ref_traits> type;
};

template <typename map_type>
struct map_data_ptr_iterator {
	typedef details::map_data_iterator<map_type, details::ptr_traits> type;
};

template <typename map_type>
struct map_data_ref_const_iterator {
	typedef details::map_data_const_iterator<map_type, details::ref_traits>
		type;
};

template <typename map_type>
struct map_data_ptr_const_iterator {
	typedef details::map_data_const_iterator<map_type, details::ptr_traits>
		type;
};

template <typename map_type>
struct map_key_ref_const_iterator {
	typedef details::map_key_const_iterator<map_type, details::ref_traits> type;
};

template <typename map_type>
struct map_key_ptr_const_iterator {
	typedef details::map_key_const_iterator<map_type, details::ptr_traits> type;
};

template <typename map_type>
range<typename map_data_ref_iterator<map_type>::type> data(map_type& m)
{
	return range<typename map_data_ref_iterator<map_type>::type>(
		m.begin(), m.end());
}

template <typename map_type>
range<typename map_data_ref_const_iterator<map_type>::type> data(
	const map_type& m)
{
	return range<typename map_data_ref_const_iterator<map_type>::type>(
		m.begin(), m.end());
}

template <typename map_type>
range<typename map_key_ref_const_iterator<map_type>::type> keys(
	const map_type& m)
{
	return range<typename map_key_ref_const_iterator<map_type>::type>(
		m.begin(), m.end());
}

template <typename map_type>
range<typename map_type::iterator> values(map_type& m)
{
	return range<typename map_type::iterator>(m.begin(), m.end());
}

template <typename map_type>
range<typename map_type::const_iterator> values(const map_type& m)
{
	return range<typename map_type::const_iterator>(m.begin(), m.end());
}

} // namespace misc

#endif // MISC_MAP_ITERATOR_H
