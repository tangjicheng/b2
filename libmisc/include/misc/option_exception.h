#ifndef MISC_OPTION_EXCEPTION_H
#define MISC_OPTION_EXCEPTION_H

#include <exception>
#include <string>
#include <utility>

#include "form.h"

namespace misc {

class OptionError : public std::exception {
public:
	const char* what() const noexcept override { return what_.c_str(); }

protected:
	explicit OptionError(std::string&& what)
		: what_ {std::move(what)}
	{
	}

private:
	std::string what_;
};

class InvalidOption : public OptionError {
public:
	explicit InvalidOption(int option)
		: OptionError {form("Invalid option -%c", option)}
	{
	}
};

class MissingOptionArgument : public OptionError {
public:
	explicit MissingOptionArgument(int option)
		: OptionError {form("Option -%c requires an argument", option)}
	{
	}
};

class InvalidOptionArgument : public OptionError {
public:
	InvalidOptionArgument(int option, const char* what)
		: OptionError {
			form("Option -%c has an invalid argument (%s)", option, what)}
	{
	}

	InvalidOptionArgument(int option, const std::string& what)
		: InvalidOptionArgument {option, what.c_str()}
	{
	}
};

class RequiredOptionMissing : public OptionError {
public:
	explicit RequiredOptionMissing(int option)
		: OptionError {form("Option -%c is required", option)}
	{
	}
};

class IncompatibleOptions : public OptionError {
public:
	IncompatibleOptions(int firstOption, int secondOption)
		: OptionError {
			form("Incompatible options -%c and -%c", firstOption, secondOption)}
	{
	}
};

class NoOptionSpecified : public OptionError {
public:
	NoOptionSpecified()
		: OptionError {"No option specified"}
	{
	}
};

} // namespace misc

#endif // MISC_OPTION_EXCEPTION_H
