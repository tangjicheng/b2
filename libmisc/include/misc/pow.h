#ifndef MISC_POW_H
#define MISC_POW_H

#include <climits>
#include <stdexcept>
#include <type_traits>

namespace misc {

template <typename T, T value>
constexpr T ceiling_power_of_two()
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");
	constexpr auto highest_bit_pos = (std::is_signed<T>::value)
		? (sizeof(value) * CHAR_BIT - 1)
		: (sizeof(value) * CHAR_BIT);
	constexpr auto highest_bit_value
		= (value & (T {1} << (highest_bit_pos - 1))) >> (highest_bit_pos - 1);
	static_assert(
		highest_bit_value == T {0}, "Template argument value is too large");

	auto output = value;
	--output;
	constexpr auto bit_count = sizeof(output) * CHAR_BIT;
	if constexpr (1 < bit_count)
		output |= (output >> 1);
	if constexpr (2 < bit_count)
		output |= (output >> 2);
	if constexpr (4 < bit_count)
		output |= (output >> 4);
	if constexpr (8 < bit_count)
		output |= (output >> 8);
	if constexpr (16 < bit_count)
		output |= (output >> 16);
	if constexpr (32 < bit_count)
		output |= (output >> 32);
	if constexpr (64 < bit_count)
		output |= (output >> 64);
	++output;

	return output + static_cast<T>(output == 0);
}

template <typename T>
T ceiling_power_of_two(T value)
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");

	const auto highest_bit_pos = (std::is_signed<T>::value)
		? (sizeof(value) * CHAR_BIT - 1)
		: (sizeof(value) * CHAR_BIT);
	const auto highest_bit_value
		= ((value & (T {1} << (highest_bit_pos - 1))) >> (highest_bit_pos - 1));
	if (highest_bit_value == T {1})
		throw std::overflow_error {"Argument value is too large"};

	--value;
	for (auto i = 1u; i < sizeof(value) * CHAR_BIT; i <<= 1) {
		value |= (value >> i);
	}
	++value;

	return value + static_cast<T>(value == 0);
}

template <typename T>
constexpr bool is_power_of_two(T x)
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");
	return x > 0 && (x & (x - 1)) == 0;
}

template <typename T>
constexpr T pow(T base, unsigned int n) noexcept
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");
	return n == 0 ? T {1} : misc::pow(base, n - 1) * base;
}

template <typename T, T Base, unsigned int N>
struct Pow : std::integral_constant<T, misc::pow(Base, N)> {
};

template <typename T, T Base, unsigned int N>
inline constexpr T Pow_v = Pow<T, Base, N>::value;

} // namespace misc

#endif // MISC_POW_H
