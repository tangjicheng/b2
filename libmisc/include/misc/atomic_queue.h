#ifndef MISC_ATOMIC_QUEUE_H
#define MISC_ATOMIC_QUEUE_H

#include <atomic>
#include <cmath>
#include <cstdint>
#include <deque>
#include <functional>
#include <limits>
#include <optional>
#include <vector>

#include "hardware_interference_size.h"
#include "pow.h"

namespace misc {

constexpr std::uint64_t BLOCKED_BUCKET_NUM
	= std::numeric_limits<std::uint64_t>::max();

template <typename T>
class Atomic_item {
public:
	Atomic_item()
		: committed_ {false}
		, data_ {std::nullopt}
	{
	}

	template <typename Arg>
	void set(Arg&& data)
	{
		data_ = std::forward<Arg>(data);
		committed_.store(true, std::memory_order_release);
	}

	T& get()
	{
		while (!committed_.load(std::memory_order_acquire)) {
		}
		return *data_;
	}

	void consume() { committed_.store(false, std::memory_order_relaxed); }

	// It works based on assertion that std::nullopt address and value address
	// are the same. This function should be reviewed if std::optional
	// internal implementation will change from union to something else.
	T* loc() { return &*data_; }

private:
	ALIGN_DESTRUCTIVE std::atomic<bool> committed_;
	std::optional<T> data_;
};

/**
 * @file
 * @brief Single consumer multi producer concurrent queue with limited capacity.
 * Used as a bucket in a pool for Atomic_queue.
 */
template <typename T, size_t capacity>
class Atomic_queue_bucket {
public:
	Atomic_queue_bucket()
		: data_ {capacity}
		, end_ {data_.data() + capacity}
		, head_ {data_.data()}
		, tail_cache_ {data_.data()}
		, tail_ {data_.data()}
		, in_use_ {false}
		, next_ {nullptr}
		, last_peek_ {false}
	{
	}

	template <typename Arg>
	bool push(Arg&& data)
	{
		Atomic_item<T>* current_tail = nullptr;
		Atomic_item<T>* next_tail = nullptr;
		do {
			current_tail = tail_.load(std::memory_order_relaxed);
			next_tail = current_tail + 1;
			if (current_tail == end_)
				return false;
		} while (!std::atomic_compare_exchange_weak_explicit(&tail_,
			&current_tail, next_tail, std::memory_order_release,
			std::memory_order_relaxed));

		current_tail->set(std::forward<Arg>(data));
		return true;
	}

	T* peek()
	{
		Atomic_item<T>* current_tail = nullptr;
		if (head_ < tail_cache_) {
			current_tail = tail_cache_;
		}
		else {
			current_tail = tail_.load(std::memory_order_acquire);
			tail_cache_ = current_tail;
		}
		if (head_ == current_tail) {
			last_peek_ = false;
			return nullptr;
		}
		last_peek_ = true;
		auto& value = head_->get();
		return &value;
	}

	void advance()
	{
		if (last_peek_) {
			head_->consume();
			++head_;
			last_peek_ = false;
		}
	}

	void clear()
	{
		last_peek_ = false;
		in_use_.store(false, std::memory_order_relaxed);
		next_.store(nullptr, std::memory_order_relaxed);
	}

	bool can_reuse() const
	{
		return (!in_use_.load(std::memory_order_relaxed));
	}

	void reuse()
	{
		head_ = data_.data();
		tail_cache_ = data_.data();
		tail_ = data_.data();
		last_peek_ = false;
		in_use_.store(true, std::memory_order_relaxed);
		next_.store(nullptr, std::memory_order_relaxed);
	}

	void set_next(Atomic_queue_bucket<T, capacity>& queue)
	{
		next_.store(&queue, std::memory_order_relaxed);
	}

	Atomic_queue_bucket<T, capacity>* next()
	{
		return next_.load(std::memory_order_relaxed);
	}

	bool done() { return head_ == end_; }

	T* tail_loc() { return tail_.load(std::memory_order_relaxed)->loc(); }

private:
	std::vector<Atomic_item<T>> data_;
	Atomic_item<T>* end_;
	Atomic_item<T>* head_;
	Atomic_item<T>* tail_cache_;
	ALIGN_DESTRUCTIVE std::atomic<Atomic_item<T>*> tail_;
	ALIGN_DESTRUCTIVE std::atomic<bool> in_use_;
	ALIGN_DESTRUCTIVE std::atomic<Atomic_queue_bucket<T, capacity>*> next_;
	bool last_peek_;
};

/**
 * @file
 * @brief Pool of reusable preallocated atomic queue buckets.
 * It allocates new buckets if consumer is slower than producing.
 * Not thread-safe on its own.
 */
template <typename T, size_t capacity>
class Atomic_queue_pool {
public:
	explicit Atomic_queue_pool(std::uint64_t start_capacity)
	{
		for (auto i = 0u; i < start_capacity; ++i)
			queues_.emplace_back();
	}

	Atomic_queue_bucket<T, capacity>& acquire()
	{
		for (auto it = queues_.begin(); it != queues_.end(); ++it) {
			if (it->can_reuse()) {
				it->reuse();
				return *it;
			}
		}
		queues_.emplace_back();
		auto& queue = queues_.back();
		queue.reuse();
		return queue;
	}

	void release(Atomic_queue_bucket<T, capacity>& queue) { queue.clear(); }

private:
	std::deque<Atomic_queue_bucket<T, capacity>> queues_;
};

/**
 * @file
 * @brief Single consumer multi producer concurrent queue.
 */
template <typename T, size_t capacity>
class Atomic_queue {
public:
	explicit Atomic_queue(std::uint64_t start_capacity)
		: pool_ {start_capacity}
	{
		auto& queue = pool_.acquire();
		head_ = &queue;
		tail_.store(&queue, std::memory_order_relaxed);
		bucket_num_.store(0u, std::memory_order_seq_cst);
	}

	template <typename Arg>
	void push(Arg&& data)
	{
		while (true) {
			auto bucket_num = bucket_num_.load(std::memory_order_acquire);
			if (bucket_num == BLOCKED_BUCKET_NUM)
				continue;
			auto& queue = *(tail_.load(std::memory_order_relaxed));
			if (queue.push(std::forward<Arg>(data)))
				break;
			if (acquire(bucket_num, std::forward<Arg>(data)))
				break;
		}
	}

	T* peek()
	{
		if (head_->done()) {
			release();
		}
		auto value = head_->peek();
		return value;
	}

	void advance() { head_->advance(); }

	template <typename Callback>
	requires(std::invocable<Callback, T*>) bool consume_all(
		Callback callback, std::uint64_t max)
	{
		auto& last_bucket = *(tail_.load(std::memory_order_relaxed));
		const auto last_item = last_bucket.tail_loc();
		auto num = std::uint64_t {0u};
		T* current_item = nullptr;
		while (max == 0 || num < max) {
			const auto is_last_item = (current_item == last_item);

			current_item = peek();
			if (current_item == nullptr)
				return true;
			callback(current_item);
			++num;
			advance();
			if (is_last_item)
				break;
		}
		return false;
	}

private:
	Atomic_queue_pool<T, capacity> pool_;
	Atomic_queue_bucket<T, capacity>* head_;
	ALIGN_DESTRUCTIVE std::atomic<Atomic_queue_bucket<T, capacity>*> tail_;
	ALIGN_DESTRUCTIVE std::atomic<std::uint64_t> bucket_num_;

	template <typename Arg>
	bool acquire(std::uint64_t bucket_num, Arg&& data)
	{
		auto expected = bucket_num;
		if (std::atomic_compare_exchange_strong_explicit(&bucket_num_,
				&expected, BLOCKED_BUCKET_NUM, std::memory_order_release,
				std::memory_order_relaxed)) {
			auto& queue = pool_.acquire();
			queue.push(std::forward<Arg>(data));
			auto& tail = *(tail_.load(std::memory_order_relaxed));
			tail.set_next(queue);
			tail_.store(&queue, std::memory_order_relaxed);
			bucket_num_.store(bucket_num + 1, std::memory_order_release);
			return true;
		}
		return false;
	}

	void release()
	{
		auto queue = head_;
		if (!queue->next())
			return;
		head_ = queue->next();
		pool_.release(*queue);
	}
};

} // namespace misc

#endif // MISC_ATOMIC_QUEUE_H
