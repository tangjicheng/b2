#ifndef MISC_FLAGSET_H
#define MISC_FLAGSET_H

#include <cstdint>

namespace misc {

template <typename FlagEnum, typename UnderlyingTypeT = uint32_t>
class FlagSet {
public:
	using UnderlyingType = UnderlyingTypeT;

	FlagSet()
		: m_set(0)
	{
	}
	explicit FlagSet(UnderlyingType set)
		: m_set(set)
	{
	}
	FlagSet(FlagEnum flag)
		: m_set(static_cast<UnderlyingType>(0x1)
			<< static_cast<UnderlyingType>(flag))
	{
	}

	bool empty() const { return m_set == 0; }
	bool includes(FlagSet other) const
	{
		return (m_set & other.m_set) == other.m_set;
	}
	bool intersects(FlagSet other) const { return (m_set & other.m_set) != 0; }

	UnderlyingType value() const { return m_set; }

	FlagSet& operator&=(FlagSet rhs)
	{
		m_set &= rhs.m_set;
		return *this;
	}
	FlagSet& operator|=(FlagSet rhs)
	{
		m_set |= rhs.m_set;
		return *this;
	}
	FlagSet& operator^=(FlagSet rhs)
	{
		m_set ^= rhs.m_set;
		return *this;
	}

private:
	UnderlyingType m_set;
};

#define MISC_FLAGSET_DECL(FlagEnum, FlagSetName)                         \
	using FlagSetName = misc::FlagSet<FlagEnum>;                         \
	inline misc::FlagSet<FlagEnum> operator|(FlagEnum lhs, FlagEnum rhs) \
	{                                                                    \
		return misc::FlagSet<FlagEnum>(lhs) |= rhs;                      \
	}

#define MISC_FLAGSET_TYPE_DECL(Flag_enum, Flag_set_name, Underlying_type) \
	using Flag_set_name = misc::FlagSet<Flag_enum, Underlying_type>;      \
	inline misc::FlagSet<Flag_enum, Underlying_type> operator|(           \
		Flag_enum lhs, Flag_enum rhs)                                     \
	{                                                                     \
		return misc::FlagSet<Flag_enum, Underlying_type> {lhs} |= rhs;    \
	}

template <typename FlagEnum, typename Underlying_type>
bool operator==(FlagSet<FlagEnum, Underlying_type> lhs,
	FlagSet<FlagEnum, Underlying_type> rhs)
{
	return lhs.value() == rhs.value();
}

template <typename FlagEnum, typename Underlying_type>
bool operator!=(FlagSet<FlagEnum, Underlying_type> lhs,
	FlagSet<FlagEnum, Underlying_type> rhs)
{
	return lhs.value() != rhs.value();
}

template <typename FlagEnum, typename Underlying_type>
FlagSet<FlagEnum, Underlying_type> operator&(
	FlagSet<FlagEnum, Underlying_type> lhs,
	FlagSet<FlagEnum, Underlying_type> rhs)
{
	return lhs &= rhs;
}

template <typename FlagEnum, typename Underlying_type>
FlagSet<FlagEnum, Underlying_type> operator|(
	FlagSet<FlagEnum, Underlying_type> lhs, FlagEnum rhs)
{
	return lhs |= rhs;
}

template <typename FlagEnum, typename Underlying_type>
FlagSet<FlagEnum, Underlying_type> operator|(
	FlagSet<FlagEnum, Underlying_type> lhs,
	FlagSet<FlagEnum, Underlying_type> rhs)
{
	return lhs |= rhs;
}

template <typename FlagEnum, typename Underlying_type>
FlagSet<FlagEnum, Underlying_type> operator^(
	FlagSet<FlagEnum, Underlying_type> lhs,
	FlagSet<FlagEnum, Underlying_type> rhs)
{
	return lhs ^= rhs;
}

} // namespace misc

#endif // MISC_FLAGSET_H
