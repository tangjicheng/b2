#ifndef MISC_BINARY_FILE_HANDLER_H
#define MISC_BINARY_FILE_HANDLER_H

#include <cstddef>
#include <cstdint>

namespace misc {

enum class Binary_file_event { opened, end_of_file, closed };

enum class Binary_file_error { open_failure, unexpected_end_of_file };

class Binary_file_handler {
public:
	virtual void on_message(const void*, size_t, uint64_t) = 0;
	virtual void on_event(Binary_file_event) = 0;
	virtual void on_error(Binary_file_error) = 0;

protected:
	Binary_file_handler() = default;
	~Binary_file_handler() = default;

	Binary_file_handler(const Binary_file_handler&) = default;
	Binary_file_handler& operator=(const Binary_file_handler&) = default;

	Binary_file_handler(Binary_file_handler&&) = default;
	Binary_file_handler& operator=(Binary_file_handler&&) = default;
};

} // namespace misc

#endif // MISC_BINARY_FILE_HANDLER_H
