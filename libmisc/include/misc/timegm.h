#ifndef MISC_TIMEGM_H
#define MISC_TIMEGM_H

#include <ctime>

namespace misc {

time_t timegm(tm*);

} // namespace misc

#endif // MISC_TIMEGM_H
