#ifndef MISC_BASE64_H
#define MISC_BASE64_H

#include <cstddef>
#include <exception>
#include <string>

namespace misc { namespace base64 {

class DecodeError : public std::exception {
public:
	const char* what() const noexcept override { return m_what.c_str(); }

protected:
	explicit DecodeError(const char* what)
		: m_what(what)
	{
	}

private:
	std::string m_what;
};

class DecodeLengthError : public DecodeError {
public:
	DecodeLengthError(const char* what, size_t length)
		: DecodeError(what)
		, m_length(length)
	{
	}

	size_t length() const { return m_length; }

private:
	size_t m_length;
};

class DecodeCharacterError : public DecodeError {
public:
	DecodeCharacterError(const char* what, size_t offset, char character)
		: DecodeError(what)
		, m_offset(offset)
		, m_character(character)
	{
	}

	size_t offset() const { return m_offset; }
	char character() const { return m_character; }

private:
	size_t m_offset;
	char m_character;
};

/**
 *  @brief Calculates the encoded buffer length from the binary buffer length.
 *  @param len The binary buffer length.
 *  @return The number of bytes required for the encoded buffer.
 */
constexpr size_t encodedBufferLength(size_t len)
{
	return len / 3 * 4 + ((len % 3 > 0) ? 4 : 0);
}

/**
 *  @brief Calculates the upper bound of the decoded buffer length from the
 *  base64 buffer length.
 *  @param len The base64 buffer length.
 *  @return The upper bound of the number of bytes required for the decoded
 *  buffer.
 */
constexpr size_t decodedBufferLengthUpperBound(size_t len)
{
	return len / 4 * 3;
}

/**
 *  @brief Calculates the decoded buffer length from the base64 buffer.
 *  @param src The base64 buffer address.
 *  @param len The base64 buffer length.
 *  @return The number of bytes required for the decoded buffer.
 *  @throw DecodeLengthError If the given base64 buffer length is wrong.
 *  @throw DecodeCharacterError If there are illegal characters in the given
 *  base64 buffer.
 */
size_t decodedBufferLength(const char* src, size_t len);

/**
 *  @brief Encode binary to base64.
 *  @param dest The encoded buffer address.
 *  @param src The binary buffer address.
 *  @param len The binary buffer length.
 *  @return The number of bytes used in @a dest.
 */
size_t encode(char* dest, const void* src, size_t len);

/**
 *  @brief Decode base64 to binary.
 *  @param dest The decoded buffer address.
 *  @param src The base64 buffer address.
 *  @param len The base64 buffer length.
 *  @return The number of bytes used in @a dest.
 *  @throw DecodeLengthError If the given base64 buffer length is wrong.
 *  @throw DecodeCharacterError If there are illegal characters in the given
 *  base64 buffer.
 */
size_t decode(void* dest, const char* src, size_t len);

}} // namespace misc::base64

#endif // MISC_BASE64_H
