#ifndef MISC_TASKQUEUE_H
#define MISC_TASKQUEUE_H

#include <condition_variable>
#include <cstddef>
#include <functional>
#include <mutex>
#include <utility>

namespace misc {

class Task {
	friend class TaskQueue;

public:
	~Task() = default;

	Task(const Task&) = delete;
	Task& operator=(const Task&) = delete;

	Task(Task&&) = delete;
	Task& operator=(Task&&) = delete;

	template <typename Callable>
	explicit Task(Callable&& task)
		: m_task(std::forward<Callable>(task))
	{
	}

private:
	std::function<void()> m_task;
	size_t m_pending {0};
	bool m_draining {false};
	Task* m_next {nullptr};

	bool operator()(std::unique_lock<std::mutex>&);

	size_t incrementPending() { return ++m_pending; }

	bool isPending() const { return m_pending > 0; }

	void setDraining(bool flag = true) { m_draining = flag; }

	bool isDraining() const { return m_draining; }
};

class TaskQueue {
public:
	TaskQueue() = default;
	~TaskQueue();

	TaskQueue(const TaskQueue&) = delete;
	TaskQueue& operator=(const TaskQueue&) = delete;

	TaskQueue(TaskQueue&&) = delete;
	TaskQueue& operator=(TaskQueue&&) = delete;

	void enqueue(Task&);
	void drain(Task&);

	bool run(bool);
	void shutdown();

private:
	Task* m_head {nullptr};
	Task* m_tail {nullptr};
	std::mutex m_mtx;
	std::condition_variable m_cv;
	bool m_shutdown {false};

	void pushTask(Task* task)
	{
		if (m_tail != nullptr)
			m_tail->m_next = task;
		else
			m_head = task;
		m_tail = task;
	}

	Task* popTask()
	{
		auto task = m_head;
		if (task != nullptr) {
			m_head = task->m_next;
			if (m_head != nullptr)
				task->m_next = nullptr;
			else
				m_tail = nullptr;
		}
		return task;
	}
};

} // namespace misc

#endif // MISC_TASKQUEUE_H
