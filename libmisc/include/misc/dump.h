#ifndef MISC_DUMP_H
#define MISC_DUMP_H

#include <cctype>
#include <cstddef>
#include <functional>
#include <iomanip>
#include <ios>
#include <ostream>
#include <sstream>
#include <string>

#include "Finally.h"

namespace misc {

namespace details {

inline void dump(const char* str, size_t len, std::ostream& os)
{
	for (auto end = str + len; str != end; ++str) {
		if (isprint(*str))
			os << *str;
		else
			os << "\\x" << std::setw(2)
			   << static_cast<int>(static_cast<unsigned char>(*str));
	}
}

} // namespace details

inline void dump(const void* str, size_t len, std::ostream& os)
{
	auto flags = os.setf(std::ios::hex, std::ios::basefield);
	auto ch = os.fill('0');
	auto restore = misc::finally([flags, ch, &os] {
		os.setf(flags, std::ios::basefield);
		os.fill(ch);
	});
	details::dump(static_cast<const char*>(str), len, os);
}

inline void dump(const std::string& str, std::ostream& os)
{
	dump(str.c_str(), str.size(), os);
}

inline std::string dump(const void* str, size_t len)
{
	std::ostringstream oss;
	oss.setf(std::ios::hex, std::ios::basefield);
	oss.fill('0');
	details::dump(static_cast<const char*>(str), len, oss);
	return oss.str();
}

inline std::string dump(const std::string& str)
{
	return dump(str.c_str(), str.size());
}

class Dump {
	friend std::ostream& operator<<(std::ostream& os, const Dump& rhs)
	{
		dump(rhs.m_str, rhs.m_len, os);
		return os;
	}

public:
	Dump(const void* str, size_t len)
		: m_str(static_cast<const char*>(str))
		, m_len(len)
	{
	}

	explicit Dump(const std::string& str)
		: Dump(str.c_str(), str.size())
	{
	}

private:
	const char* m_str;
	size_t m_len;
};

} // namespace misc

#endif // MISC_DUMP_H
