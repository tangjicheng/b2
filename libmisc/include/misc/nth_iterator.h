#ifndef MISC_NTH_ITERATOR_H
#define MISC_NTH_ITERATOR_H

#include <cstddef>
#include <iterator>
#include <memory>
#include <tuple>
#include <type_traits>

#include "range.h" // nth_iterator.h guarantees to provide range.h.

namespace misc {

template <size_t N, typename Iterator>
class nth_iterator {
private:
	using traits_type = std::iterator_traits<Iterator>;

public:
	using iterator_type = Iterator;

	using iterator_category = typename traits_type::iterator_category;
	using difference_type = typename traits_type::difference_type;
	using value_type
		= std::tuple_element_t<N, typename traits_type::value_type>;
	using pointer = std::add_pointer_t<std::tuple_element_t<N,
		std::remove_pointer_t<typename traits_type::pointer>>>;
	using reference = std::add_lvalue_reference_t<std::tuple_element_t<N,
		std::remove_reference_t<typename traits_type::reference>>>;

	nth_iterator()
		: m_current()
	{
	}

	explicit nth_iterator(const iterator_type& i)
		: m_current(i)
	{
	}

	template <typename OtherIterator>
	nth_iterator(const nth_iterator<N, OtherIterator>& i)
		: m_current(i.base())
	{
	}

	template <typename OtherIterator>
	nth_iterator& operator=(const nth_iterator<N, OtherIterator>& i)
	{
		m_current = i.base();
		return *this;
	}

	const iterator_type& base() const { return m_current; }

	reference operator*() const { return std::get<N>(*m_current); }

	pointer operator->() const
	{
		return std::addressof(std::get<N>(*m_current));
	}

	reference operator[](difference_type n) const
	{
		return std::get<N>(m_current[n]);
	}

	nth_iterator& operator++()
	{
		++m_current;
		return *this;
	}

	nth_iterator operator++(int) { return nth_iterator(m_current++); }

	nth_iterator& operator--()
	{
		--m_current;
		return *this;
	}

	nth_iterator operator--(int) { return nth_iterator(m_current--); }

	nth_iterator& operator+=(difference_type n)
	{
		m_current += n;
		return *this;
	}

	nth_iterator& operator-=(difference_type n)
	{
		m_current -= n;
		return *this;
	}

private:
	iterator_type m_current;
};

template <size_t N, typename Iterator>
nth_iterator<N, Iterator> operator+(const nth_iterator<N, Iterator>& i,
	typename nth_iterator<N, Iterator>::difference_type n)
{
	return nth_iterator<N, Iterator>(i.base() + n);
}

template <size_t N, typename Iterator>
nth_iterator<N, Iterator> operator+(
	typename nth_iterator<N, Iterator>::difference_type n,
	const nth_iterator<N, Iterator>& i)
{
	return nth_iterator<N, Iterator>(n + i.base());
}

template <size_t N, typename Iterator>
nth_iterator<N, Iterator> operator-(const nth_iterator<N, Iterator>& i,
	typename nth_iterator<N, Iterator>::difference_type n)
{
	return nth_iterator<N, Iterator>(i.base() - n);
}

template <size_t N, typename IteratorL, typename IteratorR>
auto operator-(const nth_iterator<N, IteratorL>& lhs,
	const nth_iterator<N, IteratorR>& rhs) -> decltype(lhs.base() - rhs.base())
{
	return lhs.base() - rhs.base();
}

template <size_t N, typename IteratorL, typename IteratorR>
bool operator==(const nth_iterator<N, IteratorL>& lhs,
	const nth_iterator<N, IteratorR>& rhs)
{
	return lhs.base() == rhs.base();
}

template <size_t N, typename IteratorL, typename IteratorR>
bool operator!=(const nth_iterator<N, IteratorL>& lhs,
	const nth_iterator<N, IteratorR>& rhs)
{
	return lhs.base() != rhs.base();
}

template <size_t N, typename IteratorL, typename IteratorR>
bool operator<(const nth_iterator<N, IteratorL>& lhs,
	const nth_iterator<N, IteratorR>& rhs)
{
	return lhs.base() < rhs.base();
}

template <size_t N, typename IteratorL, typename IteratorR>
bool operator>(const nth_iterator<N, IteratorL>& lhs,
	const nth_iterator<N, IteratorR>& rhs)
{
	return lhs.base() > rhs.base();
}

template <size_t N, typename IteratorL, typename IteratorR>
bool operator<=(const nth_iterator<N, IteratorL>& lhs,
	const nth_iterator<N, IteratorR>& rhs)
{
	return lhs.base() <= rhs.base();
}

template <size_t N, typename IteratorL, typename IteratorR>
bool operator>=(const nth_iterator<N, IteratorL>& lhs,
	const nth_iterator<N, IteratorR>& rhs)
{
	return lhs.base() >= rhs.base();
}

template <size_t N, typename Iterator>
nth_iterator<N, Iterator> make_nth_iterator(const Iterator& i)
{
	return nth_iterator<N, Iterator>(i);
}

template <size_t N, typename Iterator>
range<nth_iterator<N, Iterator>> nth(const Iterator& b, const Iterator& e)
{
	return range<nth_iterator<N, Iterator>>(b, e);
}

template <size_t N, typename Range>
auto nth(Range& r) -> range<nth_iterator<N, decltype(std::begin(r))>>
{
	return decltype(nth<N>(r))(std::begin(r), std::end(r));
}

template <size_t N, typename Range>
auto nth(const Range& r) -> range<nth_iterator<N, decltype(std::begin(r))>>
{
	return decltype(nth<N>(r))(std::begin(r), std::end(r));
}

} // namespace misc

#endif // MISC_NTH_ITERATOR_H
