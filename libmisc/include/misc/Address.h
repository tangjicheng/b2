#ifndef MISC_ADDRESS_H
#define MISC_ADDRESS_H

#include <optional>
#include <string>
#include <tuple>

#include <cstdint>

namespace misc { namespace io {

constexpr char address_any_ipv4[] = "0.0.0.0";
constexpr char address_loopback_ipv4[] = "127.0.0.1";

std::tuple<std::uint32_t, bool> ip_address_to_int(const std::string& address);
std::tuple<std::uint16_t, bool> ip_port_to_int(const std::string& port);

std::string int_to_ip_address(std::uint32_t address);
std::string int_to_ip_port(std::uint16_t port);

class Address {
public:
	Address() = default;
	Address(const std::string&, const std::string&);
	Address(std::string&&, std::string&&);

	const std::string& host() const { return m_host; }
	const std::string& service() const { return m_service; }

	bool matches(const Address&) const;
	void complete(const Address&);

	std::string to_string() const;

private:
	std::string m_host;
	std::string m_service;
};

// service is not verified to be a port number
std::optional<misc::io::Address> to_address(const std::string&);

inline bool operator==(const Address& lhs, const Address& rhs)
{
	return std::tie(lhs.host(), lhs.service())
		== std::tie(rhs.host(), rhs.service());
}

inline bool operator!=(const Address& lhs, const Address& rhs)
{
	return std::tie(lhs.host(), lhs.service())
		!= std::tie(rhs.host(), rhs.service());
}

inline bool operator<(const Address& lhs, const Address& rhs)
{
	return std::tie(lhs.host(), lhs.service())
		< std::tie(rhs.host(), rhs.service());
}

inline bool operator>(const Address& lhs, const Address& rhs)
{
	return std::tie(lhs.host(), lhs.service())
		> std::tie(rhs.host(), rhs.service());
}

inline bool operator<=(const Address& lhs, const Address& rhs)
{
	return std::tie(lhs.host(), lhs.service())
		<= std::tie(rhs.host(), rhs.service());
}

inline bool operator>=(const Address& lhs, const Address& rhs)
{
	return std::tie(lhs.host(), lhs.service())
		>= std::tie(rhs.host(), rhs.service());
}

}} // namespace misc::io

#endif // MISC_ADDRESS_H
