#ifndef MISC_INDENT_H
#define MISC_INDENT_H

#include <ostream>

namespace misc {

class indent {
public:
	explicit indent(unsigned int depth, char ch = '\t')
		: m_depth(depth)
		, m_char(ch)
	{
	}

	std::ostream& operator()(std::ostream& os) const
	{
		for (auto i = 0u; i != m_depth; ++i)
			os.put(m_char);
		return os;
	}

private:
	unsigned int m_depth;
	char m_char;
};

inline std::ostream& operator<<(std::ostream& os, const indent& rhs)
{
	return rhs(os);
}

} // namespace misc

#endif // MISC_INDENT_H
