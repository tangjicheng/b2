#ifndef MISC_FIND_INSERT_HINT_H
#define MISC_FIND_INSERT_HINT_H

#include <algorithm>
#include <utility>

namespace misc {

enum class fih_result { found, collision };

// Find in map via map::lower_bound and map::key_comp

template <typename Map>
std::pair<typename Map::iterator, fih_result> find_insert_hint(
	Map& map, const typename Map::key_type& key)
{
	auto i = map.lower_bound(key);
	typedef std::pair<typename Map::iterator, fih_result> return_type;
	return return_type(i,
		i == map.end() || map.key_comp()(key, i->first)
			? fih_result::found
			: fih_result::collision);
}

template <typename Map>
std::pair<typename Map::const_iterator, fih_result> find_insert_hint(
	const Map& map, const typename Map::key_type& key)
{
	auto i = map.lower_bound(key);
	typedef std::pair<typename Map::const_iterator, fih_result> return_type;
	return return_type(i,
		i == map.end() || map.key_comp()(key, i->first)
			? fih_result::found
			: fih_result::collision);
}

// Find in range via std::lower_bound and less

template <typename ForwardIterator, typename T>
std::pair<ForwardIterator, fih_result> find_insert_hint(
	ForwardIterator first, ForwardIterator last, const T& value)
{
	auto i = std::lower_bound(first, last, value);
	typedef std::pair<ForwardIterator, fih_result> return_type;
	return return_type(i,
		i == last || (value < *i) ? fih_result::found : fih_result::collision);
}

// Find in range via std::lower_bound and compare

template <typename ForwardIterator, typename T, typename Compare>
std::pair<ForwardIterator, fih_result> find_insert_hint(
	ForwardIterator first, ForwardIterator last, const T& value, Compare comp)
{
	auto i = std::lower_bound(first, last, value, comp);
	typedef std::pair<ForwardIterator, fih_result> return_type;
	return return_type(i,
		i == last || comp(value, *i) ? fih_result::found
									 : fih_result::collision);
}

} // namespace misc

#endif // MISC_FIND_INSERT_HINT_H
