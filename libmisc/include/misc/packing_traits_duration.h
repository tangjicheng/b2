#ifndef MISC_PACKING_TRAITS_DURATION_H
#define MISC_PACKING_TRAITS_DURATION_H

#include <chrono>

#include "Record.h"
#include "packing_traits.h"

namespace misc {

template <typename Rep, typename Period>
struct PackingTraits<std::chrono::duration<Rep, Period>>
	: FixedPackingTraits<sizeof(Rep)> {
	static Record<Rep> r;

	class Packer : public FixedPacker {
	public:
		Packer(std::chrono::duration<Rep, Period> duration)
			: duration_ {duration}
		{
		}

		void packFixed(void*& p) const
		{
			const auto count = duration_.count();
			p = r.packUnsafe(p, count);
		}

	private:
		std::chrono::duration<Rep, Period> duration_;
	};

	class Unpacker : public FixedUnpacker {
	public:
		Unpacker(std::chrono::duration<Rep, Period>& duration)
			: duration_ {duration}
		{
		}

		void unpackFixed(const void*& p) const
		{
			Rep count;
			p = r.unpackUnsafe(p, count);
			duration_ = std::chrono::duration<Rep, Period> {count};
		}

	private:
		std::chrono::duration<Rep, Period>& duration_;
	};

	using PackParam = Packer;
	using UnpackParam = Unpacker;
};

} // namespace misc

#endif // MISC_PACKING_TRAITS_DURATION_H
