#ifndef MISC_OPTION_H
#define MISC_OPTION_H

#include <optional>
#include <string>

namespace misc {

struct Option {
	Option(char character, bool is_optional,
		const std::optional<std::string>& argument, const std::string& details)
		: character(character)
		, is_optional(is_optional)
		, argument(argument)
		, details(details)
	{
	}

	char character;
	bool is_optional;
	std::optional<std::string> argument;
	std::string details;
};

} // namespace misc

#endif // MISC_OPTION_H
