#ifndef MISC_PREALLOCATED_VECTOR_H
#define MISC_PREALLOCATED_VECTOR_H

#include <vector>

#include <boost/core/demangle.hpp>

#include "Log.h"
#include "OneTimeAllocator.h"

namespace misc {

template <typename T>
class Preallocated_vector : public std::vector<T, OneTimeAllocator<T>> {
public:
	using std::vector<T, OneTimeAllocator<T>>::vector;

	void log_info() const
	{
		appLog(APP_LOG_INFO,
			"OneTimeAllocator:\n"
			"	acquired: %zu\n"
			"	capacity: %zu\n"
			"	%s size: %zu",
			this->size(), this->capacity(),
			boost::core::demangle(typeid(T).name()).c_str(), sizeof(T));
	}
};

} // namespace misc

#endif // MISC_PREALLOCATED_VECTOR_H
