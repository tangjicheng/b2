#ifndef MISC_BASE_APPLICATION_H
#define MISC_BASE_APPLICATION_H

#include <atomic>
#include <cctype>
#include <set>
#include <string>
#include <vector>

#include <yaml-cpp/yaml.h>

#include "Reactor.h"
#include "option.h"
#include "version.h"

namespace misc {

class Base_application {
public:
	int run(int, char**);
	const std::string& config_filename() const;

protected:
	Base_application(const std::string&, const std::string&, const Version&);
	Base_application(const std::string&, const std::string&,
		const std::vector<Option>&, const Version&);

	~Base_application() = default;

	Base_application(const Base_application&) = delete;
	Base_application& operator=(const Base_application&) = delete;

	Base_application(Base_application&&) = delete;
	Base_application& operator=(Base_application&&) = delete;

	virtual void init(const YAML::Node&);
	virtual void shutdown();
	virtual int do_work();

	virtual void onOption(int, const char*) = 0;
	virtual void onEndOfOptions() = 0;
	virtual void onInit(const YAML::Node&) = 0;
	virtual misc::Reactor::idle_strategy
	get_application_reactor_idle_strategy();
	virtual void onStart() = 0;
	virtual void onStop() = 0;

	virtual void print_version() const;
	virtual void print_start_log() const;

	virtual void onHangupSignal();
	virtual void onUserSignal1();
	virtual void onUserSignal2();

	virtual void register_health_jobs() {}

	misc::Reactor& application_reactor() { return application_reactor_; }

	sigset_t signals_;
	std::atomic_bool keep_going_ {true};

private:
	struct OptionCompare {
		bool operator()(const Option& lhs, const Option& rhs) const
		{
			char lhsUpper = toupper(lhs.character);
			char rhsUpper = toupper(rhs.character);
			if (lhsUpper != rhsUpper)
				return lhsUpper < rhsUpper;
			return lhs.character > rhs.character;
		}
	};

	std::string application_name_;
	std::string executable_name_;
	std::string config_filename_;
	std::string default_pid_filename_;
	std::vector<Option> extra_options_;
	std::set<Option, OptionCompare> options_;
	Version version_;
	misc::Reactor application_reactor_;

	std::string optionString() const;
	void printUsage() const;
};

inline const std::string& Base_application::config_filename() const
{
	return config_filename_;
}

} // namespace misc

#endif // MISC_BASE_APPLICATION_H
