#ifndef MISC_DELAY_SEQUENCE_H
#define MISC_DELAY_SEQUENCE_H

#include <array>

namespace misc {

template <typename T, unsigned int... delays>
class Delay_sequence {
public:
	static_assert(sizeof...(delays) != 0, "Empty delays");

	T next()
	{
		const auto delay = T {*next_delay_};
		if (next_delay_ + 1 != delays_.end())
			++next_delay_;
		return delay;
	}

	void reset() { next_delay_ = delays_.begin(); }

private:
	static constexpr std::array<unsigned int, sizeof...(delays)> delays_ {
		delays...};
	typename decltype(delays_)::const_iterator next_delay_ {delays_.begin()};
};

template <typename T, unsigned int... delays>
constexpr std::array<unsigned int, sizeof...(delays)>
	Delay_sequence<T, delays...>::delays_;

} // namespace misc

#endif // MISC_DELAY_SEQUENCE_H
