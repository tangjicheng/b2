#ifndef MISC_IO_SOCKET_H
#define MISC_IO_SOCKET_H

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#include <cerrno>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <system_error>
#include <type_traits>
#include <utility>

#include "Address.h" // Socket.h guarantees to provide Address.h.

namespace misc { namespace io {

namespace details {

// Helper for handling functions which can fail with EINTR at any time.
template <typename Callable, typename... Args>
auto skip_EINTR(Callable&& call, Args&&... args)
	-> std::result_of_t<Callable(Args...)>
{
	std::result_of_t<Callable(Args...)> res;
	while ((res = call(std::forward<Args>(args)...)) == -1 && errno == EINTR) {
	}
	return res;
}

} // namespace details

enum class Address_family : int {
	unspecified = AF_UNSPEC,
	ipv4 = AF_INET,
	ipv6 = AF_INET6
};

enum class Socket_type : int {
	unspecified = 0,
	stream = SOCK_STREAM,
	datagram = SOCK_DGRAM
};

enum class Address_info_flag : int {
	none = 0,
	passive = AI_PASSIVE,
	numeric_host = AI_NUMERICHOST,
	numeric_service = AI_NUMERICSERV
};

class Address_info_flags {
	friend Address_info_flags operator|(
		Address_info_flags lhs, Address_info_flags rhs)
	{
		lhs.m_flags |= rhs.m_flags;
		return lhs;
	}

public:
	Address_info_flags(Address_info_flag flag)
		: m_flags(static_cast<int>(flag))
	{
	}

	int value() const { return m_flags; }

private:
	int m_flags;
};

inline Address_info_flags operator|(
	Address_info_flag lhs, Address_info_flag rhs)
{
	return Address_info_flags(lhs) | rhs;
}

enum class Name_info_flag : int {
	none = 0,
	numeric_host = NI_NUMERICHOST,
	numeric_service = NI_NUMERICSERV
};

class Name_info_flags {
	friend Name_info_flags operator|(Name_info_flags lhs, Name_info_flags rhs)
	{
		lhs.m_flags |= rhs.m_flags;
		return lhs;
	}

public:
	Name_info_flags(Name_info_flag flag)
		: m_flags(static_cast<int>(flag))
	{
	}

	int value() const { return m_flags; }

private:
	int m_flags;
};

inline Name_info_flags operator|(Name_info_flag lhs, Name_info_flag rhs)
{
	return Name_info_flags(lhs) | rhs;
}

enum class Blocking_mode { blocking, non_blocking };

enum class Nagle_algorithm { unspecified, enabled, disabled };

struct Socket_stream_options {
	explicit Socket_stream_options(Blocking_mode mode)
		: blocking_mode {mode}
	{
	}

	Socket_stream_options(Blocking_mode mode, Nagle_algorithm nagle_algorithm,
		int receive_buffer_size, int send_buffer_size)
		: blocking_mode {mode}
		, nagle_algorithm {nagle_algorithm}
		, receive_buffer_size {receive_buffer_size}
		, send_buffer_size {send_buffer_size}
	{
	}

	Blocking_mode blocking_mode {Blocking_mode::non_blocking};
	Nagle_algorithm nagle_algorithm {Nagle_algorithm::unspecified};
	int receive_buffer_size {0};
	int send_buffer_size {0};
};

class Address_info {
public:
	Address_info() = default;
	~Address_info();

	Address_info(const Address_info&) = delete;
	Address_info& operator=(const Address_info&) = delete;

	Address_info(Address_info&&);
	Address_info& operator=(Address_info&&);

	Address_info(const Address&, Address_family, Socket_type,
		Address_info_flags = Address_info_flag::none);
	Address_info(const Address&, Socket_type,
		Address_info_flags = Address_info_flag::none);

	static Address_info from_host(const std::string&, Address_family,
		Socket_type, Address_info_flags = Address_info_flag::none);
	static Address_info from_host(const std::string&, Socket_type,
		Address_info_flags = Address_info_flag::none);

	static Address_info from_service(const std::string&, Address_family,
		Socket_type, Address_info_flags = Address_info_flag::none);
	static Address_info from_service(const std::string&, Socket_type,
		Address_info_flags = Address_info_flag::none);

	int family() const { return m_addrinfo->ai_family; }
	int type() const { return m_addrinfo->ai_socktype; }
	int protocol() const { return m_addrinfo->ai_protocol; }

	const sockaddr* address() const { return m_addrinfo->ai_addr; }
	socklen_t length() const { return m_addrinfo->ai_addrlen; }

private:
	addrinfo* m_addrinfo {nullptr};

	Address_info(const char*, const char*, Address_family, Socket_type,
		Address_info_flags);

	void free();
	void move(Address_info&);

	void getaddrinfo(const char*, const char*, const addrinfo*);
};

class Socket_address {
	friend class Socket;

public:
	Socket_address() = default;
	explicit Socket_address(const Address_info&);

	Address to_address(Name_info_flags
		= Name_info_flag::numeric_host | Name_info_flag::numeric_service) const;
	std::string to_host(Name_info_flags = Name_info_flag::numeric_host) const;
	std::string to_service(
		Name_info_flags = Name_info_flag::numeric_service) const;

	const sockaddr* address() const
	{
		return reinterpret_cast<const sockaddr*>(&m_address);
	}
	socklen_t length() const { return m_length; }

private:
	sockaddr_storage m_address;
	socklen_t m_length {0};

	void copy(const void*, socklen_t);

	void getnameinfo(char*, size_t, char*, size_t, int) const;
};

class Socket {
public:
	Socket() = default;
	~Socket();

	Socket(const Socket&) = delete;
	Socket& operator=(const Socket&) = delete;

	Socket(Socket&&);
	Socket& operator=(Socket&&);

	static Socket listen(const Address&, Socket_stream_options);
	static Socket accept(const Socket&, Socket_stream_options);
	static Socket connect(const Address&, Socket_stream_options);
	static Socket connect(
		const Address&, const Address&, Socket_stream_options);

	static Socket datagram(const Address&, Blocking_mode);
	static Socket multicast_listen(
		const std::string&, const std::string&, Blocking_mode);
	static Socket multicast_listen(const std::string&, const std::string&,
		const std::string&, Blocking_mode);
	static Socket multicast_listen(const std::string&, const std::string&,
		const std::string&, const std::string&, Blocking_mode);

	int fd() const { return m_fd; }

	bool is_open() const { return m_fd != -1; }

	void set_connected() { m_connected = true; }

	std::error_code error_status() const;

	Socket_address local_socket_address() const;
	Address local_address(Name_info_flags
		= Name_info_flag::numeric_host | Name_info_flag::numeric_service) const;
	std::string local_host(
		Name_info_flags = Name_info_flag::numeric_host) const;
	std::string local_service(
		Name_info_flags = Name_info_flag::numeric_service) const;

	Socket_address remote_socket_address() const;
	Address remote_address(Name_info_flags
		= Name_info_flag::numeric_host | Name_info_flag::numeric_service) const;
	std::string remote_host(
		Name_info_flags = Name_info_flag::numeric_host) const;
	std::string remote_service(
		Name_info_flags = Name_info_flag::numeric_service) const;

	int get_receive_buffer_size() const;
	int get_send_buffer_size() const;
	std::int32_t get_send_queue_size() const;

	size_t blocking_read(void*, size_t) const;
	void blocking_write_all(const void*, size_t) const;

	ssize_t read(void*, size_t) const;
	ssize_t write(const void*, size_t, bool throw_on_again = false) const;
	ssize_t vector_read(const iovec*, int) const;
	ssize_t vector_write(const iovec*, int) const;
	ssize_t recv(void*, size_t, int) const;
	ssize_t send(const void*, size_t, int) const;
	ssize_t recv_from(void*, size_t, int, Socket_address&) const;
	ssize_t send_to(const void*, size_t, int, const Socket_address&) const;

	/**
	 *  @brief Shut down socket send and receive operations.
	 *
	 *  Causes all of a full-duplex connection on the socket to be shut
	 *  down. Further receptions and transmissions will be disallowed.
	 */
	std::error_code shutdown();
	/**
	 *  @brief Close the associated file descriptor.
	 *
	 *  First calls @c shutdown() if the socket is connected. Then closes
	 *  the socket so that it no longer refers to any socket and may be
	 *  reused.
	 */
	std::error_code close();

private:
	int m_fd {-1};
	bool m_connected {false};

	Socket(const Address_info&);

	void move(Socket&);

	void bind(const sockaddr*, socklen_t);
	void listen(int);
	void accept(int, sockaddr*, socklen_t*, Blocking_mode);
	void connect(const sockaddr*, socklen_t, Blocking_mode);

	void getsockopt(int, int, void*, socklen_t*) const;
	void setsockopt(int, int, const void*, socklen_t);
	int get_status_flags() const;
	Blocking_mode get_blocking_mode() const;
	void set_status_flags(long);
	void set_blocking_mode(Blocking_mode);
	Nagle_algorithm get_nagle_algorithm() const;
	void set_nagle_algorithm(Nagle_algorithm);
	void set_receive_buffer_size(int);
	void set_send_buffer_size(int);

	std::error_code do_shutdown(int);
	std::error_code do_close();
};

/**
 *  @return The number of bytes read. 0 indicates end of file.
 *  @throw std::system_error Upon read failure.
 */
inline size_t Socket::blocking_read(void* buff, size_t size) const
{
	auto n = details::skip_EINTR(::read, m_fd, buff, size);
	if (n == -1)
		throw std::system_error(errno, std::system_category(), "read() failed");
	return n;
}

/**
 *  @throw std::system_error Upon write failure.
 */
inline void Socket::blocking_write_all(const void* buff, size_t size) const
{
	size_t i = 0;
	while (i < size) {
		auto n = details::skip_EINTR(::write, m_fd,
			static_cast<const unsigned char*>(buff) + i, size - i);
		if (n == -1)
			throw std::system_error(
				errno, std::system_category(), "write() failed");
		i += n;
	}
}

/**
 *  @return The number of bytes read. 0 indicates end of file, -1 indicates the
 *  call would block.
 *  @throw std::system_error Upon read failure.
 */
inline ssize_t Socket::read(void* buff, size_t size) const
{
	auto n = details::skip_EINTR(::read, m_fd, buff, size);
	if (n == -1 && errno != EAGAIN && errno != EWOULDBLOCK)
		throw std::system_error(errno, std::system_category(), "read() failed");
	return n;
}

/**
 *  @return The number of bytes written. 0 indicates nothing was written, -1
 *  indicates the call would block.
 *  @throw std::system_error Upon write failure.
 */
inline ssize_t Socket::write(
	const void* buff, size_t size, bool throw_on_again) const
{
	auto n = details::skip_EINTR(::write, m_fd, buff, size);
	if (n == -1 && errno != EAGAIN && errno != EWOULDBLOCK)
		throw std::system_error {
			errno, std::system_category(), "write() failed"};
	if (throw_on_again && (n == -1 || n == 0)) {
		throw std::system_error {
			errno, std::system_category(), "write() exceeded retry attempts"};
	}
	return n;
}

/**
 *  @return The number of bytes read. 0 indicates end of file, -1 indicates the
 *  call would block.
 *  @throw std::system_error Upon read failure.
 */
inline ssize_t Socket::vector_read(const iovec* iov, int count) const
{
	auto n = details::skip_EINTR(::readv, m_fd, iov, count);
	if (n == -1 && errno != EAGAIN && errno != EWOULDBLOCK)
		throw std::system_error(
			errno, std::system_category(), "readv() failed");
	return n;
}

/**
 *  @return The number of bytes written. 0 indicates nothing was written, -1
 *  indicates the call would block.
 *  @throw std::system_error Upon write failure.
 */
inline ssize_t Socket::vector_write(const iovec* iov, int count) const
{
	auto n = details::skip_EINTR(::writev, m_fd, iov, count);
	if (n == -1 && errno != EAGAIN && errno != EWOULDBLOCK)
		throw std::system_error(
			errno, std::system_category(), "writev() failed");
	return n;
}

/**
 *  @return The number of bytes received. 0 indicates end of file, -1 indicates
 *  the call would block.
 *  @throw std::system_error Upon receive failure.
 */
inline ssize_t Socket::recv(void* buff, size_t size, int flags) const
{
	auto n = details::skip_EINTR(::recv, m_fd, buff, size, flags);
	if (n == -1 && errno != EAGAIN && errno != EWOULDBLOCK)
		throw std::system_error(errno, std::system_category(), "recv() failed");
	return n;
}

/**
 *  @return The number of bytes sent. 0 indicates nothing was sent, -1
 *  indicates the call would block.
 *  @throw std::system_error Upon send failure.
 */
inline ssize_t Socket::send(const void* buff, size_t size, int flags) const
{
	auto n = details::skip_EINTR(::send, m_fd, buff, size, flags);
	if (n == -1 && errno != EAGAIN && errno != EWOULDBLOCK)
		throw std::system_error(errno, std::system_category(), "send() failed");
	return n;
}

/**
 *  @return The number of bytes received. 0 indicates end of file, -1 indicates
 *  the call would block.
 *  @throw std::system_error Upon receive failure.
 */
inline ssize_t Socket::recv_from(
	void* buff, size_t size, int flags, Socket_address& address) const
{
	address.m_length = sizeof(address.m_address);
	auto n = details::skip_EINTR(::recvfrom, m_fd, buff, size, flags,
		reinterpret_cast<sockaddr*>(&address.m_address), &address.m_length);
	if (n == -1 && errno != EAGAIN && errno != EWOULDBLOCK)
		throw std::system_error(
			errno, std::system_category(), "recvfrom() failed");
	return n;
}

/**
 *  @return The number of bytes sent. 0 indicates nothing was sent, -1
 *  indicates the call would block.
 *  @throw std::system_error Upon send failure.
 */
inline ssize_t Socket::send_to(const void* buff, size_t size, int flags,
	const Socket_address& address) const
{
	auto n = details::skip_EINTR(
		::sendto, m_fd, buff, size, flags, address.address(), address.length());
	if (n == -1 && errno != EAGAIN && errno != EWOULDBLOCK)
		throw std::system_error(
			errno, std::system_category(), "sendto() failed");
	return n;
}

}} // namespace misc::io

#endif // MISC_IO_SOCKET_H
