#ifndef MISC_FIX_UTCTIMEDATE_H
#define MISC_FIX_UTCTIMEDATE_H

#include <cstdint>
#include <cstring>
#include <ctime>
#include <tuple>

#include "../timegm.h"

namespace misc { namespace fix {

class UTCDateOnly {
public:
	UTCDateOnly() = default;

	explicit UTCDateOnly(uint32_t yyyymmdd)
		: m_date(yyyymmdd)
	{
	}

	UTCDateOnly(uint32_t year, uint32_t month, uint32_t day)
		: m_date(year * 10000 + month * 100 + day)
	{
	}

	uint32_t yyyymmdd() const { return m_date; }

	uint32_t year() const { return m_date / 10000; }
	uint32_t month() const { return m_date / 100 % 100; }
	uint32_t day() const { return m_date % 100; }

private:
	uint32_t m_date {19700101};
};

inline bool operator==(UTCDateOnly lhs, UTCDateOnly rhs)
{
	return lhs.yyyymmdd() == rhs.yyyymmdd();
}

inline bool operator!=(UTCDateOnly lhs, UTCDateOnly rhs)
{
	return lhs.yyyymmdd() != rhs.yyyymmdd();
}

inline bool operator<(UTCDateOnly lhs, UTCDateOnly rhs)
{
	return lhs.yyyymmdd() < rhs.yyyymmdd();
}

inline bool operator>(UTCDateOnly lhs, UTCDateOnly rhs)
{
	return lhs.yyyymmdd() > rhs.yyyymmdd();
}

inline bool operator<=(UTCDateOnly lhs, UTCDateOnly rhs)
{
	return lhs.yyyymmdd() <= rhs.yyyymmdd();
}

inline bool operator>=(UTCDateOnly lhs, UTCDateOnly rhs)
{
	return lhs.yyyymmdd() >= rhs.yyyymmdd();
}

class UTCTimeOnly {
public:
	UTCTimeOnly() = default;

	explicit UTCTimeOnly(uint32_t hhmmss)
		: m_time(hhmmss * 1000)
		, m_hasMilliseconds(false)
	{
	}

	UTCTimeOnly(uint32_t hhmmss, uint32_t msec)
		: m_time(hhmmss * 1000 + msec)
	{
	}

	UTCTimeOnly(uint32_t hour, uint32_t minute, uint32_t second)
		: m_time(hour * 10000000 + minute * 100000 + second * 1000)
		, m_hasMilliseconds(false)
	{
	}

	UTCTimeOnly(uint32_t hour, uint32_t minute, uint32_t second, uint32_t msec)
		: m_time(hour * 10000000 + minute * 100000 + second * 1000 + msec)
	{
	}

	uint32_t hhmmssSSS() const { return m_time; }
	uint32_t hhmmss() const { return m_time / 1000; }

	uint32_t hour() const { return m_time / 10000000; }
	uint32_t minute() const { return m_time / 100000 % 100; }
	uint32_t second() const { return m_time / 1000 % 100; }
	uint32_t msec() const { return m_time % 1000; }

	bool hasMilliseconds() const { return m_hasMilliseconds; }

private:
	uint32_t m_time {0};
	bool m_hasMilliseconds {true};
};

inline bool operator==(UTCTimeOnly lhs, UTCTimeOnly rhs)
{
	return lhs.hhmmssSSS() == rhs.hhmmssSSS();
}

inline bool operator!=(UTCTimeOnly lhs, UTCTimeOnly rhs)
{
	return lhs.hhmmssSSS() != rhs.hhmmssSSS();
}

inline bool operator<(UTCTimeOnly lhs, UTCTimeOnly rhs)
{
	return lhs.hhmmssSSS() < rhs.hhmmssSSS();
}

inline bool operator>(UTCTimeOnly lhs, UTCTimeOnly rhs)
{
	return lhs.hhmmssSSS() > rhs.hhmmssSSS();
}

inline bool operator<=(UTCTimeOnly lhs, UTCTimeOnly rhs)
{
	return lhs.hhmmssSSS() <= rhs.hhmmssSSS();
}

inline bool operator>=(UTCTimeOnly lhs, UTCTimeOnly rhs)
{
	return lhs.hhmmssSSS() >= rhs.hhmmssSSS();
}

class UTCTimestamp {
public:
	UTCTimestamp() = default;

	UTCTimestamp(UTCDateOnly date, UTCTimeOnly time)
		: m_date(date)
		, m_time(time)
	{
	}

	UTCTimestamp(uint32_t yyyymmdd, uint32_t hhmmss)
		: m_date(yyyymmdd)
		, m_time(hhmmss)
	{
	}

	UTCTimestamp(uint32_t yyyymmdd, uint32_t hhmmss, uint32_t msec)
		: m_date(yyyymmdd)
		, m_time(hhmmss, msec)
	{
	}

	UTCTimestamp(uint32_t year, uint32_t month, uint32_t day, uint32_t hour,
		uint32_t minute, uint32_t second)
		: m_date(year, month, day)
		, m_time(hour, minute, second)
	{
	}

	UTCTimestamp(uint32_t year, uint32_t month, uint32_t day, uint32_t hour,
		uint32_t minute, uint32_t second, uint32_t msec)
		: m_date(year, month, day)
		, m_time(hour, minute, second, msec)
	{
	}

	UTCDateOnly date() const { return m_date; }
	UTCTimeOnly time() const { return m_time; }

	uint32_t yyyymmdd() const { return m_date.yyyymmdd(); }
	uint32_t hhmmssSSS() const { return m_time.hhmmssSSS(); }
	uint32_t hhmmss() const { return m_time.hhmmss(); }

	uint32_t year() const { return m_date.year(); }
	uint32_t month() const { return m_date.month(); }
	uint32_t day() const { return m_date.day(); }
	uint32_t hour() const { return m_time.hour(); }
	uint32_t minute() const { return m_time.minute(); }
	uint32_t second() const { return m_time.second(); }
	uint32_t msec() const { return m_time.msec(); }

	bool hasMilliseconds() const { return m_time.hasMilliseconds(); }

	time_t toTimeT() const
	{
		tm tm;
		memset(&tm, 0, sizeof(tm));
		tm.tm_sec = second();
		tm.tm_min = minute();
		tm.tm_hour = hour();
		tm.tm_mday = day();
		tm.tm_mon = month() - 1;
		tm.tm_year = year() - 1900;
		return misc::timegm(&tm);
	}

	static UTCTimestamp fromTimeT(time_t t)
	{
		tm tm;
		memset(&tm, 0, sizeof(tm));
		gmtime_r(&t, &tm);
		return UTCTimestamp(tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
			tm.tm_hour, tm.tm_min, tm.tm_sec);
	}

	static UTCTimestamp fromTimeT(time_t t, uint32_t msec)
	{
		tm tm;
		memset(&tm, 0, sizeof(tm));
		gmtime_r(&t, &tm);
		return UTCTimestamp(tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
			tm.tm_hour, tm.tm_min, tm.tm_sec, msec);
	}

private:
	UTCDateOnly m_date;
	UTCTimeOnly m_time;
};

inline bool operator==(const UTCTimestamp& lhs, const UTCTimestamp& rhs)
{
	return std::forward_as_tuple(lhs.date(), lhs.time())
		== std::forward_as_tuple(rhs.date(), rhs.time());
}

inline bool operator!=(const UTCTimestamp& lhs, const UTCTimestamp& rhs)
{
	return std::forward_as_tuple(lhs.date(), lhs.time())
		!= std::forward_as_tuple(rhs.date(), rhs.time());
}

inline bool operator<(const UTCTimestamp& lhs, const UTCTimestamp& rhs)
{
	return std::forward_as_tuple(lhs.date(), lhs.time())
		< std::forward_as_tuple(rhs.date(), rhs.time());
}

inline bool operator>(const UTCTimestamp& lhs, const UTCTimestamp& rhs)
{
	return std::forward_as_tuple(lhs.date(), lhs.time())
		> std::forward_as_tuple(rhs.date(), rhs.time());
}

inline bool operator<=(const UTCTimestamp& lhs, const UTCTimestamp& rhs)
{
	return std::forward_as_tuple(lhs.date(), lhs.time())
		<= std::forward_as_tuple(rhs.date(), rhs.time());
}

inline bool operator>=(const UTCTimestamp& lhs, const UTCTimestamp& rhs)
{
	return std::forward_as_tuple(lhs.date(), lhs.time())
		>= std::forward_as_tuple(rhs.date(), rhs.time());
}

}} // namespace misc::fix

#endif // MISC_FIX_UTCTIMEDATE_H
