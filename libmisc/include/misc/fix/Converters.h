#ifndef MISC_FIX_CONVERTERS_H
#define MISC_FIX_CONVERTERS_H

#include <algorithm>
#include <cstdint>
#include <limits>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>

#include "../abs.h"
#include "../pow.h"

namespace misc { namespace fix {

class UTCDateOnly;
class UTCTimeOnly;
class UTCTimestamp;

/**
 *  @throw std::invalid_argument If @a str has bad format or contains invalid
 *  characters.
 */
char charConvert(const std::string& str);

/**
 *  @throw std::invalid_argument If @a c is an invalid character.
 */
std::string charConvert(char c);

/**
 *  @throw std::invalid_argument If @a str has bad format or contains invalid
 *  characters.
 */
bool booleanConvert(const std::string& str);

std::string booleanConvert(bool) noexcept;

/**
 *  @throw std::invalid_argument If @a str has bad format or contains invalid
 *  characters.
 */
const std::string& stringConvert(const std::string& str);

/**
 *  @throw std::invalid_argument If @a str has bad format or contains invalid
 *  characters.
 */
std::vector<char> multipleCharValueConvert(const std::string& str);

/**
 *  @throw std::invalid_argument If @a vec is empty or contains invalid
 *  characters.
 */
std::string multipleCharValueConvert(const std::vector<char>& vec);

/**
 *  @throw std::invalid_argument If @a str has bad format or contains invalid
 *  characters.
 */
std::vector<std::string> multipleStringValueConvert(const std::string& str);

/**
 *  @throw std::invalid_argument If @a vec is empty or contains invalid
 *  characters.
 */
std::string multipleStringValueConvert(const std::vector<std::string>& vec);

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
uint32_t checksumConvert(const std::string& str);

/**
 *  @throw std::invalid_argument If @a i is out of range.
 */
std::string checksumConvert(uint32_t i);

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
UTCDateOnly utcDateOnlyConvert(const std::string& str);

std::string utcDateOnlyConvert(UTCDateOnly) noexcept;

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
UTCTimeOnly utcTimeOnlyConvert(const std::string& str);

std::string utcTimeOnlyConvert(UTCTimeOnly) noexcept;

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
UTCTimestamp utcTimestampConvert(const std::string& str);

std::string utcTimestampConvert(const UTCTimestamp&) noexcept;

namespace details {

/**
 *  @throw std::invalid_argument If the string defined by the range [@a i, @a
 *  end) has bad format, contains invalid characters or is out of range.
 */
template <typename T>
T stringToInt(std::string::const_iterator i, std::string::const_iterator end)
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");
	if (i == end) // There are no characters.
		throw std::invalid_argument("Bad format");
	// Skip leading zeros.
	while (i != end && *i == '0')
		++i;
	auto b = i;
	// Process digits.
	T value = 0;
	for (; i != end && isdigit(*i); ++i)
		value = value * 10 + (*i - '0');
	if (i - b
		> std::numeric_limits<T>::digits10) // The value cannot be represented.
		throw std::invalid_argument("Out of range");
	if (i == end) // There are no more characters.
		return value;
	// There are trailing non-digit characters.
	throw std::invalid_argument("Invalid character");
}

/**
 *  @throw std::invalid_argument If the string defined by the range [@a p, @a p
 *  + @a LENGTH) contains invalid characters.
 */
template <unsigned int LENGTH, typename T>
T stringToIntPad(const char* p)
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");
	static_assert(LENGTH != 0, "Pad length equal to zero");
	static_assert(LENGTH <= std::numeric_limits<T>::digits10,
		"Pad length not less than or equal to number of base-10 digits");
	T value = 0;
	for (unsigned int i = 0; i != LENGTH; ++i) {
		if (!isdigit(p[i]))
			throw std::invalid_argument("Invalid character");
		value = value * 10 + (p[i] - '0');
	}
	return value;
}

template <typename T>
char* intToString(T value, char* p) noexcept
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");
	do {
		*--p = value % 10 + '0';
		value /= 10;
	} while (value > 0);
	return p;
}

template <unsigned int LENGTH, typename T>
char* intToStringPad(T value, char* p) noexcept
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");
	static_assert(LENGTH != 0, "Pad length equal to zero");
	for (unsigned int i = 0; i != LENGTH; ++i) {
		*--p = value % 10 + '0';
		value /= 10;
	}
	return p;
}

template <unsigned int LENGTH, typename T>
T intToStringPadEx(T value, char* p) noexcept
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");
	static_assert(LENGTH != 0, "Pad length equal to zero");
	for (unsigned int i = 0; i != LENGTH; ++i) {
		*--p = value % 10 + '0';
		value /= 10;
	}
	return value;
}

} // namespace details

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
template <typename T>
T positiveIntConvert(const std::string& str)
{
	return details::stringToInt<T>(str.begin(), str.end());
}

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
template <typename T>
T intConvert(const std::string& str)
{
	static_assert(std::is_signed_v<T>, "Template argument T not signed type");
	auto begin = str.begin();
	auto end = str.end();
	if (begin != end && *begin == '-')
		return -details::stringToInt<T>(++begin, end);
	return details::stringToInt<T>(begin, end);
}

template <typename T>
std::enable_if_t<std::is_unsigned_v<T>, std::string> intConvert(
	T value) noexcept
{
	// Add one for the half digit.
	char buf[std::numeric_limits<T>::digits10 + 1];
	auto end = buf + sizeof(buf);
	auto begin = details::intToString(value, end);
	return std::string(begin, end);
}

template <typename T>
std::enable_if_t<std::is_signed_v<T>, std::string> intConvert(T value) noexcept
{
	// Add one for the sign character and one for the half digit.
	char buf[std::numeric_limits<T>::digits10 + 2];
	auto end = buf + sizeof(buf);
	char* begin;
	if (value < 0) {
		auto p = end;
		if (value == std::numeric_limits<T>::min()) {
			*--p = misc::abs(value % 10) + '0';
			value /= 10;
		}
		value *= -1;
		begin = details::intToString(value, p);
		*--begin = '-';
	}
	else
		begin = details::intToString(value, end);
	return std::string(begin, end);
}

template <unsigned int LENGTH, typename T>
std::string paddedIntConvert(T value) noexcept
{
	static_assert(
		std::is_unsigned_v<T>, "Template argument T not unsigned type");
	char buf[LENGTH];
	auto end = buf + sizeof(buf);
	details::intToStringPad<sizeof(buf)>(value, end);
	return std::string(buf, end);
}

namespace details {

/**
 *  @throw std::invalid_argument If the string defined by the range [@a begin,
 *  @a end) has bad format, contains invalid characters or is out of range.
 */
template <unsigned int DECIMALS, typename T>
T stringToFloat(
	std::string::const_iterator begin, std::string::const_iterator end)
{
	static_assert(
		std::is_integral_v<T>, "Template argument T must be an integral type");
	static_assert(DECIMALS <= std::numeric_limits<T>::digits10,
		"Decimals not less than or equal to number of base-10 digits");
	auto i = begin;
	if (i == end) // There are no characters.
		throw std::invalid_argument("Bad format");
	// Skip leading zeros.
	while (i != end && *i == '0')
		++i;
	auto b = i;
	// Process whole digits.
	T value = 0;
	for (; i != end && isdigit(*i); ++i)
		value = value * 10 + (*i - '0');
	if (i - b > std::numeric_limits<T>::digits10
			- DECIMALS) // The whole part cannot be represented.
		throw std::invalid_argument("Out of range");
	if (i == end) // There are no more characters.
		return value * Pow_v<T, 10, DECIMALS>;
	if (*i != '.') // The character after the optional whole part is not a
				   // decimal point.
		throw std::invalid_argument("Invalid character");
	if (end - begin == 1) // The input contains only a decimal point.
		throw std::invalid_argument("Bad format");
	// Process decimal digits.
	auto limit = ++i + DECIMALS;
	auto e = std::min(limit, end);
	for (; i != e && isdigit(*i); ++i)
		value = value * 10 + (*i - '0');
	for (auto j = i; j != limit; ++j)
		value *= 10;
	// Skip trailing zeros.
	while (i != end && *i == '0')
		++i;
	if (i == end) // There are no more characters.
		return value;
	if (isdigit(*i)) // There are more nonzero digits than number of decimals.
		throw std::invalid_argument("Out of range");
	// There are trailing non-digit characters.
	throw std::invalid_argument("Invalid character");
}

template <unsigned int DECIMALS, typename T>
std::enable_if_t<DECIMALS == 0, char*> floatToString(T value, char* p) noexcept
{
	return intToString(value, p);
}

template <unsigned int DECIMALS, typename T>
std::enable_if_t<DECIMALS != 0, char*> floatToString(T value, char* p) noexcept
{
	static_assert(DECIMALS <= std::numeric_limits<T>::digits10,
		"Decimals not less than or equal to number of base-10 digits");
	value = intToStringPadEx<DECIMALS>(value, p);
	p -= DECIMALS;
	*--p = '.';
	return intToString(value, p);
}

} // namespace details

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
template <unsigned int DECIMALS, typename T>
T positiveFloatConvert(const std::string& str)
{
	return details::stringToFloat<DECIMALS, T>(str.begin(), str.end());
}

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
template <unsigned int DECIMALS, typename T>
T floatConvert(const std::string& str)
{
	static_assert(std::is_signed_v<T>, "Template argument T not signed type");
	auto begin = str.begin();
	auto end = str.end();
	if (begin != end && *begin == '-')
		return -details::stringToFloat<DECIMALS, T>(++begin, end);
	return details::stringToFloat<DECIMALS, T>(begin, end);
}

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
template <unsigned int DECIMALS, typename T, typename F>
F positiveFloatConvert(const std::string& str)
{
	static_assert(std::is_floating_point_v<F>,
		"Template argument F not floating point type");
	return positiveFloatConvert<DECIMALS, T>(str)
		/ static_cast<F>(Pow_v<T, 10, DECIMALS>);
}

/**
 *  @throw std::invalid_argument If @a str has bad format, contains invalid
 *  characters or is out of range.
 */
template <unsigned int DECIMALS, typename T, typename F>
F floatConvert(const std::string& str)
{
	static_assert(std::is_floating_point_v<F>,
		"Template argument F not floating point type");
	return floatConvert<DECIMALS, T>(str)
		/ static_cast<F>(Pow_v<T, 10, DECIMALS>);
}

template <unsigned int DECIMALS, typename T>
std::enable_if_t<std::is_integral_v<T> && std::is_unsigned_v<T>, std::string>
floatConvert(T value) noexcept
{
	// Add one for the half digit and one for the decimal point.
	char buf[std::numeric_limits<T>::digits10 + 1 + 1];
	auto end = buf + sizeof(buf);
	auto begin = details::floatToString<DECIMALS>(value, end);
	return std::string(begin, end);
}

template <unsigned int DECIMALS, typename T>
std::enable_if_t<std::is_integral_v<T> && std::is_signed_v<T>, std::string>
floatConvert(T value) noexcept
{
	// Add one for the sign character, one for the half digit and one for the
	// decimal point.
	char buf[std::numeric_limits<T>::digits10 + 1 + 1 + 1];
	auto end = buf + sizeof(buf);
	char* begin;
	if (value < 0) {
		auto p = end;
		if (value == std::numeric_limits<T>::min()) {
			*--p = misc::abs(value % 10) + '0';
			value /= 10;
			value *= -1;
			if (DECIMALS == 1)
				*--p = '.';
			begin = details::floatToString < DECIMALS == 0
				? 0
				: DECIMALS - 1 > (value, p);
		}
		else {
			value *= -1;
			begin = details::floatToString<DECIMALS>(value, p);
		}
		*--begin = '-';
	}
	else
		begin = details::floatToString<DECIMALS>(value, end);
	return std::string(begin, end);
}

template <unsigned int DECIMALS, typename T, typename F>
std::enable_if_t<std::is_floating_point_v<F>, std::string> floatConvert(
	F value) noexcept
{
	static_assert(std::is_signed_v<T>, "Template argument T not signed type");
	return floatConvert<DECIMALS>(static_cast<T>((value
		+ static_cast<F>(value < 0 ? -0.5 : 0.5)
			/ Pow_v<T, 10, DECIMALS>)*Pow_v<T, 10, DECIMALS>));
}

}} // namespace misc::fix

#endif // MISC_FIX_CONVERTERS_H
