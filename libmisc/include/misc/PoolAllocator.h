#ifndef MISC_POOLALLOCATOR_H
#define MISC_POOLALLOCATOR_H

#include <cstddef>
#include <limits>
#include <memory>
#include <new>
#include <type_traits>
#include <utility>

namespace misc {

template <typename T, typename MemoryPool>
class PoolAllocator {
	template <typename U, typename OtherMemoryPool>
	friend class PoolAllocator;

public:
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	typedef T value_type;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef T& reference;
	typedef const T& const_reference;

	template <typename U>
	struct rebind {
		typedef PoolAllocator<U, MemoryPool> other;
	};

	typedef std::false_type propagate_on_container_copy_assignment;
	typedef std::false_type propagate_on_container_move_assignment;
	typedef std::true_type
		propagate_on_container_swap; // Not propagating on std::vector swap is
									 // broken in GCC 4.8.1.

	explicit PoolAllocator(MemoryPool& pool)
		: m_pool(&pool)
	{
	}

	template <typename U>
	PoolAllocator(const PoolAllocator<U, MemoryPool>& other)
		: m_pool(other.m_pool)
	{
	}

	pointer address(reference x) const { return std::addressof(x); }

	const_pointer address(const_reference x) const { return std::addressof(x); }

	pointer allocate(size_type n)
	{
		return (pointer)m_pool->allocate(n * sizeof(T));
	}

	void deallocate(pointer p, size_type n)
	{
		m_pool->deallocate(p, n * sizeof(T));
	}

	size_type max_size() const { return std::numeric_limits<size_type>::max(); }

	template <typename U, typename... Args>
	void construct(U* p, Args&&... args)
	{
		::new ((void*)p) U(std::forward<Args>(args)...);
	}

	template <typename U>
	void destroy(U* p)
	{
		p->~U();
	}

	bool operator==(const PoolAllocator& rhs) { return m_pool == rhs.m_pool; }

	bool operator!=(const PoolAllocator& rhs) { return m_pool != rhs.m_pool; }

private:
	MemoryPool* m_pool;
};

} // namespace misc

#endif // MISC_POOLALLOCATOR_H
