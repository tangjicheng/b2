#ifndef MISC_THREAD_H
#define MISC_THREAD_H

namespace misc {

int get_tid();

} // namespace misc

#endif // MISC_THREAD_H
