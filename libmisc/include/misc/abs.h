#ifndef MISC_ABS_H
#define MISC_ABS_H

#include <type_traits>

namespace misc {

template <typename T>
constexpr std::enable_if_t<std::is_unsigned_v<T>, T> abs(T t) noexcept
{
	return t;
}

template <typename T>
constexpr std::enable_if_t<std::is_signed_v<T>, T> abs(T t) noexcept
{
	return t < T {0} ? -t : t;
}

} // namespace misc

#endif // MISC_ABS_H
