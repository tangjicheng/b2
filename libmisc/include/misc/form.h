#ifndef MISC_FORM_H
#define MISC_FORM_H

#include <string>

namespace misc {

std::string form(const char*, ...)
	__attribute__((__format__(__printf__, 1, 2)));

} // namespace misc

#endif // MISC_FORM_H
