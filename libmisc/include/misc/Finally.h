#ifndef MISC_FINALLY_H
#define MISC_FINALLY_H

#include <functional>
#include <utility>

namespace misc {

template <typename Callable>
class FinalAction {
public:
	FinalAction(const FinalAction&) = delete;
	FinalAction& operator=(const FinalAction&) = delete;

	FinalAction(FinalAction&& other)
		: m_set(other.m_set)
		, m_action(std::move(other.m_action))
	{
		other.clear();
	}

	FinalAction& operator=(FinalAction&&) = delete;

	template <typename OtherCallable>
	explicit FinalAction(OtherCallable&& action)
		: m_set(true)
		, m_action(std::forward<OtherCallable>(action))
	{
	}

	~FinalAction()
	{
		if (m_set)
			m_action();
	}

	void clear() { m_set = false; }

private:
	bool m_set;
	Callable m_action;
};

using Finally = FinalAction<std::function<void()>>;

template <typename Callable>
FinalAction<Callable> finally(Callable&& action)
{
	return FinalAction<Callable>(std::forward<Callable>(action));
}

} // namespace misc

#endif // MISC_FINALLY_H
