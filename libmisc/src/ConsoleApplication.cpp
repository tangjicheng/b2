#include "misc/ConsoleApplication.h"

#include <signal.h>
#include <unistd.h>

#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <system_error>
#include <thread>

#include "misc/Log.h"
#include "misc/application_exception.h"
#include "misc/form.h"
#include "misc/option_exception.h"

namespace misc {

namespace {

void processDebugLevel(const char* argument)
{
	std::istringstream iss(argument);
	if (!(iss >> debug))
		throw InvalidOptionArgument('d', "invalid debug level");
}

std::string processConfigFilename(const char* argument)
{
	std::string filename(argument);
	if (filename.empty())
		throw InvalidOptionArgument('f', "invalid configuration filename");
	return filename;
}

void onUnhandledSignal(int signo)
{
	appLog(APP_LOG_NOTICE, "Unhandled signal %d", signo);
}

} // namespace

ConsoleApplication::ConsoleApplication(const std::string& applicationName,
	const std::string& executableName, const Version& version)
	: ConsoleApplication(
		applicationName, executableName, std::vector<Option>(), version)
{
}

ConsoleApplication::ConsoleApplication(const std::string& applicationName,
	const std::string& executableName, const std::vector<Option>& extraOptions,
	const Version& version)
	: m_applicationName(applicationName)
	, m_executableName(executableName)
	, m_defaultConfigFilename(m_applicationName + ".yml")
	, m_extraOptions(extraOptions)
	, m_version(version)
{
	m_options.emplace('d', true, "debug", "debug level [0]");
	m_options.emplace('f', true, "config-file",
		misc::form(
			"configuration filename [%s]", m_defaultConfigFilename.c_str()));
	m_options.emplace('h', false, std::nullopt, "display this help and exit");
	m_options.emplace(
		'v', false, std::nullopt, "output version information and exit");
}

int ConsoleApplication::run(int argc, char** argv)
{
	for (auto& option : m_extraOptions) {
		if (!m_options.insert(option).second) {
			fprintf(stderr, "%s: Duplicate option -%c\n",
				m_executableName.c_str(), option.character);
			return EXIT_FAILURE;
		}
	}

	auto configFilename = m_defaultConfigFilename;

	try {
		auto optionString = this->optionString();
		int option;
		while ((option = getopt(argc, argv, optionString.c_str())) != -1) {
			switch (option) {
			case 'd':
				processDebugLevel(optarg);
				break;
			case 'f':
				configFilename = processConfigFilename(optarg);
				break;
			case 'h':
				printUsage();
				return EXIT_SUCCESS;
			case 'v':
				printVersion();
				return EXIT_SUCCESS;
			case ':':
				throw MissingOptionArgument(optopt);
			case '?':
				throw InvalidOption(optopt);
			default:
				onOption(option, optarg);
				break;
			}
		}
		onEndOfOptions();
	}
	catch (const OptionError& e) {
		fprintf(stderr, "%s: %s\n", m_executableName.c_str(), e.what());
		fprintf(stderr, "%s: Try '%s -h' for more information.\n",
			m_executableName.c_str(), m_executableName.c_str());
		return EXIT_FAILURE;
	}

	appSetLogger(&appScreenLoggerNoTime);

	appLog(APP_LOG_INFO, "Starting up %s version %s", m_applicationName.c_str(),
		m_version.number);

	sigset_t signals;
	sigemptyset(&signals);
	sigaddset(&signals, SIGINT);
	sigaddset(&signals, SIGQUIT);
	sigaddset(&signals, SIGTERM);
	sigaddset(&signals, SIGHUP);
	sigaddset(&signals, SIGUSR1);
	sigaddset(&signals, SIGUSR2);
	sigaddset(&signals, SIGPIPE);
	pthread_sigmask(SIG_BLOCK, &signals, nullptr);

	try {
		misc::appLog(misc::APP_LOG_INFO, "Configuration file (%s). Loading..",
			configFilename.c_str());
		const auto& config = YAML::LoadFile(configFilename);
		appLog(APP_LOG_INFO, "Initializing...");
		init(config);
		misc::appLog(misc::APP_LOG_INFO,
			"Configuration file (%s). Parsing completed.",
			configFilename.c_str());
	}
	catch (const std::exception& e) {
		appLog(APP_LOG_EMERG, "Initialization error: %s", e.what());
		return EXIT_FAILURE;
	}

	try {
		appLog(APP_LOG_INFO, "Starting...");
		start();
		appLog(APP_LOG_INFO, "Ready");
	}
	catch (const CriticalError& e) {
		appLog(APP_LOG_EMERG, "Start error: %s", e.what());
		return EXIT_FAILURE;
	}

	while (m_keepGoing) {
		siginfo_t info;
		timespec timeout {1, 0};
		if (sigtimedwait(&signals, &info, &timeout) == -1)
			continue;
		switch (info.si_signo) {
		case SIGINT:
		case SIGQUIT:
		case SIGTERM:
			m_keepGoing = false;
			break;
		case SIGHUP:
			m_reactor.callNow([this] { onHangupSignal(); });
			break;
		case SIGUSR1:
			m_reactor.callNow([this] { onUserSignal1(); });
			break;
		case SIGUSR2:
			m_reactor.callNow([this] { onUserSignal2(); });
			break;
		default:
			onUnhandledSignal(info.si_signo);
			break;
		}
	}

	appLog(APP_LOG_INFO, "Shutting down...");
	stop();
	appLog(APP_LOG_INFO, "Finished");
	return EXIT_SUCCESS;
}

void ConsoleApplication::shutdown()
{
	m_keepGoing = false;
}

std::string ConsoleApplication::optionString() const
{
	std::string str(":");
	for (auto& option : m_options) {
		str += option.character;
		if (option.argument.has_value())
			str += ':';
	}
	return str;
}

void ConsoleApplication::printVersion() const
{
	printf("%s version %s (commitid %s) %s\n", m_executableName.c_str(),
		m_version.number, m_version.commit_id, m_version.build_date);
	printf("\t%s@%s:%s\n", m_version.build_user, m_version.build_host,
		m_version.build_path);
}

void ConsoleApplication::printUsage() const
{
	printf("Usage: %s", m_executableName.c_str());
	for (auto& option : m_options) {
		if (option.character == 'h' || option.character == 'v')
			continue;
		printf(" ");
		if (option.is_optional)
			printf("[");
		printf("-%c", option.character);
		if (option.argument.has_value())
			printf(" <%s>", option.argument->c_str());
		if (option.is_optional)
			printf("]");
	}
	printf("\n");
	printf("       %s -h\n", m_executableName.c_str());
	printf("       %s -v\n", m_executableName.c_str());
	printf("\n");
	for (auto& option : m_options)
		printf("  -%c    %s\n", option.character, option.details.c_str());
}

void ConsoleApplication::init(const YAML::Node& config)
{
	m_reactor.signalSystemError() = [](const std::system_error& e) {
		appLog(APP_LOG_EMERG, "Reactor system error: %s (error %s:%d)",
			e.what(), e.code().category().name(), e.code().value());
		auto eptr = std::current_exception();
		if (eptr)
			std::rethrow_exception(eptr);
	};
	onInit(config);
}

void ConsoleApplication::start()
{
	try {
		m_reactor.start();
	}
	catch (const std::system_error& e) {
		throw CriticalError(
			form("Cannot start threaded reactor: %s (error %s:%d)", e.what(),
				e.code().category().name(), e.code().value()));
	}
	try {
		m_reactor.callNow([this] { onStart(); });
	}
	catch (const CriticalError&) {
		m_reactor.stop();
		throw;
	}
}

void ConsoleApplication::stop()
{
	m_reactor.callNow([this] { onStop(); });
	m_reactor.stop();
}

void ConsoleApplication::onHangupSignal()
{
	onUnhandledSignal(SIGHUP);
}

void ConsoleApplication::onUserSignal1()
{
	onUnhandledSignal(SIGUSR1);
}

void ConsoleApplication::onUserSignal2()
{
	onUnhandledSignal(SIGUSR2);
}

} // namespace misc
