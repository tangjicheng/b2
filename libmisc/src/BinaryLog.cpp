#include "misc/BinaryLog.h"

#include <array>
#include <cerrno>
#include <cstdint>
#include <limits>
#include <stdexcept>
#include <system_error>

namespace misc {

namespace {

using RecordLength = uint16_t;

constexpr size_t MESSAGE_MAX_SIZE = std::numeric_limits<RecordLength>::max();

using MaxBinaryMessage = std::array<uint8_t, MESSAGE_MAX_SIZE>;

constexpr BinaryLogPosition FIRST_LOG_POSITION {0, 0};

size_t recordSize(const ConstBinaryMessageView& message)
{
	return sizeof(RecordLength) + message.size();
}

void rebuildIndex(details::BinaryLogFile&, BinaryLogIndex&);

} // namespace

namespace details {

void BinaryLogFile::open(const std::string& filename, const char* mode)
{
	if ((m_file = fopen(filename.c_str(), mode)) == nullptr)
		throw std::system_error(errno, std::system_category());
}

void BinaryLogFile::close()
{
	if (fclose(m_file) != 0)
		throw std::system_error(errno, std::system_category());
	m_file = nullptr;
}

void BinaryLogFile::rewind()
{
	fseek(m_file, FIRST_LOG_POSITION.fileOffset, SEEK_SET);
}

bool BinaryLogFile::seek(
	size_t messageNo, const BinaryLogPosition& hint, int& error)
{
	fseek(m_file, hint.fileOffset, SEEK_SET);
	size_t nMessages = messageNo - hint.messageNo;
	for (; nMessages != 0; --nMessages) {
		MaxBinaryMessage m;
		BinaryMessageView message(m.data(), m.size());
		if (!read(message, error)) {
			if (error == 0)
				error = EIO;
			return false;
		}
	}
	return true;
}

bool BinaryLogFile::read(BinaryMessageView& message, int& error)
{
	RecordLength length;
	auto bytesRead = fread(&length, 1, sizeof(length), m_file);
	if (bytesRead != sizeof(length)) {
		if (feof(m_file)) {
			if (bytesRead == 0) {
				clearerr(m_file);
				error = 0;
			}
			else
				error = EIO;
		}
		else
			error = errno;
		return false;
	}
	if (length > message.size()) {
		error = EIO;
		return false;
	}
	message.resize(length);
	if (!message.empty()) {
		if (fread(message.data(), message.size(), 1, m_file) != 1) {
			error = feof(m_file) ? EIO : errno;
			return false;
		}
	}
	return true;
}

bool BinaryLogFile::write(const ConstBinaryMessageView& message, int& error)
{
	if (message.size() > MESSAGE_MAX_SIZE) {
		error = EIO;
		return false;
	}
	RecordLength length = message.size();
	if (fwrite(&length, sizeof(length), 1, m_file) != 1) {
		error = errno;
		return false;
	}
	if (!message.empty()) {
		if (fwrite(message.data(), message.size(), 1, m_file) != 1) {
			error = errno;
			return false;
		}
	}
	return true;
}

void BinaryLogFile::flush()
{
	if (fflush(m_file) != 0)
		throw std::system_error(errno, std::system_category());
}

} // namespace details

BinaryLogIndex::BinaryLogIndex(size_t blockSize, size_t indexSize)
	: m_blockSize(blockSize)
	, m_index(indexSize)
{
	clear();
}

void BinaryLogIndex::add(const ConstBinaryMessageView& message)
{
	auto nextBlockNo = blockOfMessage(m_nextPosition.messageNo);
	if (m_nextPosition.messageNo == startOfBlock(nextBlockNo))
		m_index[nextBlockNo % m_index.size()] = m_nextPosition;
	++m_nextPosition.messageNo;
	m_nextPosition.fileOffset += recordSize(message);
}

const BinaryLogPosition* BinaryLogIndex::find(size_t messageNo) const
{
	if (messageNo >= m_nextPosition.messageNo)
		return nullptr;
	auto& hint = m_index[blockOfMessage(messageNo) % m_index.size()];
	if (hint.messageNo > messageNo)
		return &FIRST_LOG_POSITION;
	return &hint;
}

bool BinaryLogIndex::empty() const
{
	return m_nextPosition.messageNo == FIRST_LOG_POSITION.messageNo;
}

void BinaryLogIndex::clear()
{
	m_nextPosition = FIRST_LOG_POSITION;
}

size_t BinaryLogIndex::blockOfMessage(size_t messageNo) const
{
	return messageNo / m_blockSize;
}

size_t BinaryLogIndex::startOfBlock(size_t blockNo) const
{
	return blockNo * m_blockSize;
}

BinaryLogReader::BinaryLogReader(Reactor& reactor)
	: m_reactor(&reactor)
{
}

void BinaryLogReader::open(
	const std::string& filename, const BinaryLogIndex& index)
{
	m_reactor->callNow([this, &index, filename] {
		m_index = &index;
		m_log.open(filename, "r");
	});
}

void BinaryLogReader::close()
{
	m_reactor->callNow([this] { m_log.close(); });
}

BinaryLogWriter::BinaryLogWriter(Reactor& reactor)
	: m_reactor(&reactor)
{
}

void BinaryLogWriter::open(
	const std::string& filename, BinaryLogIndex& index, bool reuse)
{
	m_reactor->callNow([this, &index, reuse, filename] {
		m_index = &index;
		if (reuse) {
			try {
				m_log.open(filename, "r+");
				rebuildIndex(m_log, *m_index);
				return;
			}
			catch (const std::system_error& e) {
				if (e.code() != std::error_code(ENOENT, std::system_category()))
					throw;
			}
		}
		m_log.open(filename, "w");
		m_index->clear();
	});
}

void BinaryLogWriter::close()
{
	m_reactor->callNow([this] { m_log.close(); });
}

void BinaryLogWriter::flush()
{
	m_reactor->callNow([this] { m_log.flush(); });
}

namespace {

void rebuildIndex(details::BinaryLogFile& log, BinaryLogIndex& index)
{
	index.clear();
	log.rewind();
	while (true) {
		MaxBinaryMessage m;
		BinaryMessageView message(m.data(), m.size());
		int error;
		if (!log.read(message, error)) {
			if (error != 0)
				throw std::system_error(error, std::system_category());
			break;
		}
		index.add(message);
	}
}

} // namespace

} // namespace misc
