#include "misc/addrinfo_error.h"

#include <string>

namespace misc {

class addrinfo_category_impl : public std::error_category {
public:
	const char* name() const noexcept override;
	std::string message(int) const override;
	std::error_condition default_error_condition(int) const noexcept override;
};

const char* addrinfo_category_impl::name() const noexcept
{
	return "getaddrinfo";
}

std::string addrinfo_category_impl::message(int value) const
{
	// Plug incorrect behaviour of gai_strerror() for EAI_OVERFLOW.
	if (static_cast<ai_errc>(value) == ai_errc::argument_buffer_overflow)
		return "Argument buffer overflow";
	return gai_strerror(value);
}

std::error_condition addrinfo_category_impl::default_error_condition(
	int value) const noexcept
{
	switch (static_cast<ai_errc>(value)) {
	case ai_errc::invalid_flags:
		return std::errc::invalid_argument; // EAI_BADFLAGS -> EINVAL
	case ai_errc::name_or_service_not_known:
		return std::errc::no_such_file_or_directory; // EAI_NONAME -> ENOENT
	case ai_errc::temporary_failure_in_name_resolution:
		return std::errc::resource_unavailable_try_again; // EAI_AGAIN -> EAGAIN
	case ai_errc::non_recoverable_failure_in_name_resolution:
		return std::errc::io_error; // EAI_FAIL -> EIO
	case ai_errc::no_address_associated_with_name:
		return std::errc::address_not_available; // EAI_NODATA -> EADDRNOTAVAIL
	case ai_errc::address_family_not_supported:
		return std::errc::address_family_not_supported; // EAI_FAMILY ->
														// EAFNOSUPPORT
	case ai_errc::socket_type_not_supported:
		return std::errc::not_supported; // EAI_SOCKTYPE -> ENOTSUP
										 // (ESOCKTNOSUPPORT)
	case ai_errc::service_not_supported_for_socket_type:
		return std::errc::not_supported; // EAI_SERVICE -> ENOTSUP
	case ai_errc::address_family_for_name_not_supported:
		return std::errc::not_supported; // EAI_ADDRFAMILY -> ENOTSUP
	case ai_errc::memory_allocation_failure:
		return std::errc::not_enough_memory; // EAI_MEMORY -> ENOMEM
	case ai_errc::system_error:
		break; // Check errno for details.
	case ai_errc::argument_buffer_overflow:
		return std::errc::no_space_on_device; // EAI_OVERFLOW -> ENOSPC
	case ai_errc::request_in_progress:
		return std::errc::operation_in_progress; // EAI_INPROGRESS ->
												 // EINPROGRESS
	case ai_errc::request_canceled:
		return std::errc::operation_canceled; // EAI_CANCELED -> ECANCELED
	case ai_errc::request_not_canceled:
		break;
	case ai_errc::all_requests_done:
		break;
	case ai_errc::interrupted:
		return std::errc::interrupted; // EAI_INTR -> EINTR
	case ai_errc::encoding_failure:
		break;
	}
	return std::error_condition(value, *this);
}

const std::error_category& addrinfo_category()
{
	static addrinfo_category_impl instance;
	return instance;
}

std::error_code make_error_code(ai_errc err)
{
	return std::error_code(static_cast<int>(err), addrinfo_category());
}

std::error_condition make_error_condition(ai_errc err)
{
	return std::error_condition(static_cast<int>(err), addrinfo_category());
}

} // namespace misc
