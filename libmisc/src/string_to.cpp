#include "misc/string_to.h"

#include <cctype>
#include <cerrno>
#include <cstdlib>
#include <limits>
#include <stdexcept>

#include "misc/abs.h"

namespace misc { namespace details {

namespace {

template <typename T, typename Enable = void>
struct Traits;

template <typename T>
struct Traits<T,
	std::enable_if_t<
		std::is_same_v<T,
			short> || std::is_same_v<T, unsigned short> || std::is_same_v<T, int> || std::is_same_v<T, unsigned int>>> {
	static constexpr long min = std::numeric_limits<T>::min();
	static constexpr long max = std::numeric_limits<T>::max();

	static bool is_out_of_range(long x) { return x < min || x > max; }

	static bool is_underflow(long x) { return x < min; }
};

template <typename T>
struct Traits<T,
	std::enable_if_t<
		std::is_same_v<T, long> || std::is_same_v<T, unsigned long>>> {
	static constexpr auto max = std::numeric_limits<T>::max();

	static bool is_out_of_range(T) { return false; }

	static bool is_underflow(T x) { return x != max; }
};

template <typename T>
struct Traits<T,
	std::enable_if_t<std::is_same_v<T, float> || std::is_same_v<T, double>>> {
	static bool is_out_of_range(T) { return false; }

	static bool is_underflow(T x) { return misc::abs(x) < static_cast<T>(1.0); }
};

template <typename T, typename Ret, typename... Base>
Ret str_to_x(T (*str_to)(const char*, char**, Base...), const char* s,
	char*& end_ptr, Base... base)
{
	if (std::is_unsigned_v<Ret>) {
		while (*s != '\0' && isspace(*s))
			++s;
		if (*s == '+' || *s == '-')
			throw std::invalid_argument("Invalid sign");
	}
	errno = 0;
	const auto x = str_to(s, &end_ptr, base...);
	if (end_ptr == s)
		throw std::invalid_argument("No conversion");
	if (errno == EINVAL)
		throw std::invalid_argument("Unsupported base");
	if (errno == ERANGE || Traits<Ret>::is_out_of_range(x))
		throw std::out_of_range(
			Traits<Ret>::is_underflow(x) ? "Underflow" : "Overflow");
	return x;
}

template <typename T, typename Ret = T, typename... Base>
Ret string_to_x(T (*str_to)(const char*, char**, Base...), const char* s,
	size_t* idx, Base... base)
{
	char* end_ptr;
	const auto x = str_to_x<T, Ret>(str_to, s, end_ptr, base...);
	if (idx != nullptr)
		*idx = end_ptr - s;
	return x;
}

template <typename T, typename Ret = T, typename... Base>
Ret string_to_x(
	T (*str_to)(const char*, char**, Base...), const char* s, Base... base)
{
	char* end_ptr;
	const auto x = str_to_x<T, Ret>(str_to, s, end_ptr, base...);
	if (*end_ptr != '\0')
		throw std::invalid_argument("Invalid character");
	return x;
}

} // namespace

short string_to_short(const char* s, size_t* idx, int base)
{
	return string_to_x<long, short>(&strtol, s, idx, base);
}

unsigned short string_to_unsigned_short(const char* s, size_t* idx, int base)
{
	return string_to_x<long, unsigned short>(&strtol, s, idx, base);
}

int string_to_int(const char* s, size_t* idx, int base)
{
	return string_to_x<long, int>(&strtol, s, idx, base);
}

unsigned int string_to_unsigned_int(const char* s, size_t* idx, int base)
{
	return string_to_x<long, unsigned int>(&strtol, s, idx, base);
}

long string_to_long(const char* s, size_t* idx, int base)
{
	return string_to_x(&strtol, s, idx, base);
}

unsigned long string_to_unsigned_long(const char* s, size_t* idx, int base)
{
	return string_to_x(&strtoul, s, idx, base);
}

float string_to_float(const char* s, size_t* idx)
{
	return string_to_x(&strtof, s, idx);
}

double string_to_double(const char* s, size_t* idx)
{
	return string_to_x(&strtod, s, idx);
}

short string_to_short(const char* s, int base)
{
	return string_to_x<long, short>(&strtol, s, base);
}

unsigned short string_to_unsigned_short(const char* s, int base)
{
	return string_to_x<long, unsigned short>(&strtol, s, base);
}

int string_to_int(const char* s, int base)
{
	return string_to_x<long, int>(&strtol, s, base);
}

unsigned int string_to_unsigned_int(const char* s, int base)
{
	return string_to_x<long, unsigned int>(&strtol, s, base);
}

long string_to_long(const char* s, int base)
{
	return string_to_x(&strtol, s, base);
}

unsigned long string_to_unsigned_long(const char* s, int base)
{
	return string_to_x(&strtoul, s, base);
}

float string_to_float(const char* s)
{
	return string_to_x(&strtof, s);
}

double string_to_double(const char* s)
{
	return string_to_x(&strtod, s);
}

}} // namespace misc::details
