#include "misc/date.h"

#include <cstring>

namespace misc {

std::ostream& operator<<(std::ostream& os, const Date& rhs)
{
	tm tm;
	memset(&tm, 0, sizeof(tm));
	tm.tm_year = rhs.year() - 1900;
	tm.tm_mon = rhs.month() - 1;
	tm.tm_mday = rhs.day();
	char buf[sizeof("YYYY-MM-DD")];
	strftime(buf, sizeof(buf), "%Y-%m-%d", &tm);
	os << buf;
	return os;
}

std::istream& operator>>(std::istream& is, Date& rhs)
{
	std::string date;
	std::getline(is, date,
		'\t'); // XXX Should not extract delimiter character (tab) from input.
	rhs = get_date(date);
	return is;
}

Date get_date(const std::string& date)
{
	tm tm;
	strptime(date.c_str(), "%Y-%m-%d", &tm);
	return Date(tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
}

} // namespace misc
