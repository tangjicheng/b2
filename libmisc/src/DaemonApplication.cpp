#include "misc/DaemonApplication.h"

#include <signal.h>
#include <syslog.h>
#include <unistd.h>

#include <cerrno>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <exception>
#include <memory>
#include <sstream>
#include <system_error>
#include <thread>

#include "misc/Log.h"
#include "misc/PidFile.h"
#include "misc/SyslogLogger.h"
#include "misc/application_exception.h"
#include "misc/form.h"
#include "misc/option_exception.h"

namespace misc {
namespace {

void onUnhandledSignal(int signo)
{
	appLog(APP_LOG_NOTICE, "Unhandled signal %d", signo);
}

} // namespace

DaemonApplication::DaemonApplication(const std::string& applicationName,
	const std::string& executableName, const Version& version)
	: DaemonApplication(
		applicationName, executableName, std::vector<Option>(), version)
{
}

DaemonApplication::DaemonApplication(const std::string& applicationName,
	const std::string& executableName, const std::vector<Option>& extraOptions,
	const Version& version)
	: Base_application {applicationName, executableName, extraOptions, version}
{
}

void DaemonApplication::init(const YAML::Node& config)
{
	m_reactor.signalSystemError() = [](const std::system_error& e) {
		appLog(APP_LOG_EMERG, "Reactor system error: %s (error %s:%d)",
			e.what(), e.code().category().name(), e.code().value());
		auto eptr = std::current_exception();
		if (eptr)
			std::rethrow_exception(eptr);
	};
	Base_application::init(config);
}

void DaemonApplication::start()
{
	try {
		m_reactor.start();
	}
	catch (const std::system_error& e) {
		throw CriticalError(form("Cannot start threaded reactor: %s "
								 "(error %s:%d)",
			e.what(), e.code().category().name(), e.code().value()));
	}
	try {
		m_reactor.callNow([this] { onStart(); });
	}
	catch (const CriticalError&) {
		m_reactor.stop();
		throw;
	}
}

int DaemonApplication::do_work()
{
	try {
		appLog(APP_LOG_INFO, "Starting...");
		start();
		appLog(APP_LOG_INFO, "Ready");
	}
	catch (const CriticalError& e) {
		appLog(APP_LOG_EMERG, "Start error: %s", e.what());
		return EXIT_FAILURE;
	}

	application_reactor().init();
	application_reactor().callEvery(std::chrono::seconds {1}, [this]() {
		siginfo_t info;
		timespec timeout {0, 0};
		if (sigtimedwait(&signals_, &info, &timeout) == -1)
			return misc::Reactor::CallStatus::OK;
		switch (info.si_signo) {
		case SIGINT:
		case SIGQUIT:
		case SIGTERM:
			application_reactor().shutdown();
			break;
		case SIGHUP:
			m_reactor.callNow([this] { onHangupSignal(); });
			break;
		case SIGUSR1:
			m_reactor.callNow([this] { onUserSignal1(); });
			break;
		case SIGUSR2:
			m_reactor.callNow([this] { onUserSignal2(); });
			break;
		default:
			onUnhandledSignal(info.si_signo);
			break;
		}
		return misc::Reactor::CallStatus::OK;
	});
	register_health_jobs();

	application_reactor().run();

	appLog(APP_LOG_INFO, "Shutting down...");
	stop();
	appLog(APP_LOG_INFO, "Finished");

	return EXIT_SUCCESS;
}

void DaemonApplication::stop()
{
	m_reactor.callNow([this] { onStop(); });
	m_reactor.stop();
}

} // namespace misc
