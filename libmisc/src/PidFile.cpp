#include "misc/PidFile.h"

#include <unistd.h>

#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <system_error>

#include <sys/file.h>

namespace misc {

namespace {

constexpr int READ_ATTEMPTS_MAX = 3;

} // namespace

PidFile::PidFile(const std::string& filename) noexcept
	: m_filename(filename)
{
}

PidFile::~PidFile()
{
	remove();
}

void PidFile::open(int mode)
{
	auto fd = ::open(m_filename.c_str(), O_WRONLY | O_CREAT, mode);
	if (fd == -1)
		throw std::system_error(errno, std::system_category(), "open() failed");
	try {
		if (flock(fd, LOCK_EX | LOCK_NB) == -1) {
			if (errno != EWOULDBLOCK)
				throw std::system_error(
					errno, std::system_category(), "flock() failed");
			for (int nattempts = 0; nattempts != READ_ATTEMPTS_MAX;
				 ++nattempts) {
				pid_t other;
				if (read(m_filename, other))
					throw DaemonAlreadyRunning(other);
				usleep(5000);
			}
			throw PidReadTimeout();
		}
		if (ftruncate(fd, 0) == -1)
			throw std::system_error(
				errno, std::system_category(), "ftruncate() failed");
	}
	catch (const std::exception&) {
		::close(fd);
		throw;
	}
	m_fd = fd;
}

bool PidFile::read(const std::string& filename, pid_t& pid)
{
	auto fd = ::open(filename.c_str(), O_RDONLY);
	if (fd == -1)
		throw std::system_error(errno, std::system_category(), "open() failed");
	char buf[32];
	auto n = ::read(fd, buf, sizeof(buf) - 1);
	auto error = errno;
	::close(fd);
	if (n == -1)
		throw std::system_error(error, std::system_category(), "read() failed");
	if (n == 0)
		return false;
	buf[n] = 0;
	char* endp;
	pid = strtol(buf, &endp, 10);
	if (endp != buf + n)
		throw PidInvalidFileFormat();
	return true;
}

void PidFile::write()
{
	if (ftruncate(m_fd, 0) == -1)
		throw std::system_error(
			errno, std::system_category(), "ftruncate() failed");
	char buf[32];
	auto n = snprintf(buf, sizeof(buf), "%d", getpid());
	if (pwrite(m_fd, buf, n, 0) == -1)
		throw std::system_error(
			errno, std::system_category(), "pwrite() failed");
}

void PidFile::close() noexcept
{
	if (m_fd == -1)
		return;
	::close(m_fd);
	m_fd = -1;
}

void PidFile::remove() noexcept
{
	if (m_fd == -1)
		return;
	::close(m_fd);
	unlink(m_filename.c_str());
	m_fd = -1;
}

} // namespace misc
