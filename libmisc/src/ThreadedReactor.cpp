#include "misc/ThreadedReactor.h"

#include <chrono>
#include <future>

#include "misc/ThreadManager.h"
#include "misc/application_exception.h"
#include "misc/form.h"
#include "misc/thread.h"

namespace misc {

namespace {

auto& threadManager = ThreadManager::instance();

} // namespace

ThreadedReactor::ThreadedReactor(
	bool supportsIoCalls, bool supportsTimedCalls, bool supportsLoopCalls)
	: m_reactor(supportsIoCalls, supportsTimedCalls, supportsLoopCalls)
{
}

ThreadedReactor::~ThreadedReactor()
{
	if (m_running)
		stop();
}

void ThreadedReactor::start(const std::string& threadAffinity)
{
	m_reactor.init();

	auto tid_set = std::promise<void> {};
	m_thread = std::thread([this, threadAffinity, &tid_set] {
		if (!threadAffinity.empty())
			threadManager.setThreadAffinity(threadAffinity);
		m_tid = misc::get_tid();
		tid_set.set_value();
		m_reactor.run();
	});
	using namespace std::chrono_literals;
	const auto status = tid_set.get_future().wait_for(10s);
	if (status != std::future_status::ready) {
		throw ConfigurationError {form("Thread for threaded reactor cannot be "
									   "created. Thread affinity ('%s')",
			threadAffinity.c_str())};
	}
	m_running = true;
}

void ThreadedReactor::stop()
{
	m_reactor.shutdown();
	m_thread.join();
	m_tid = -1;
	m_running = false;
}

} // namespace misc
