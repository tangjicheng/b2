#include "misc/ThreadManager.h"

#include <pthread.h>
#include <strings.h>

#include <cstddef>
#include <sstream>
#include <system_error>

#include "misc/Log.h"

namespace misc {

namespace {

std::unique_ptr<misc::ThreadManager> threadManager;

int parseNumber(std::istringstream& iss)
{
	const int CPUMAX = 4096;

	int num;
	iss >> num;
	if (!iss)
		throw misc::ThreadAffinitySyntaxError("Not a number");
	if (num < 0)
		throw misc::ThreadAffinitySyntaxError("Negative number");
	if (num > CPUMAX)
		throw misc::ThreadAffinitySyntaxError("Number too big");
	return num;
}

} // namespace

CPUAffinityMask cpuAffinityMaskFromString(const std::string& spec)
{
	misc::CPUAffinityMask mask;
	std::istringstream iss(spec);

	// Try parsing as hex first:
	if (spec.size() > 2 && !strncasecmp(spec.c_str(), "0x", 2)) {
		uint32_t num;
		iss >> std::hex >> num;
		if (!iss || !iss.eof())
			throw misc::ThreadAffinitySyntaxError("Invalid hex number");

		// Convert hex mask to CPUAffinityMask:
		for (auto i = 0; num != 0; ++i, num >>= 1) {
			if (num & 0x1)
				mask.set(i);
		}
	}

	// Next try considering it as a single cpu or range (with or without a
	// stride):
	else {
		auto lower = parseNumber(iss);
		auto upper = lower;
		auto stride = 1;

		auto c = iss.get();
		if (!iss.eof() && c == '-') { // looks like a range
			upper = parseNumber(iss);
			if (upper < lower)
				throw misc::ThreadAffinitySyntaxError("Range reversed");

			// optionally read a stride
			c = iss.get();
			if (!iss.eof() && c == ':') { // looks like a stride
				stride = parseNumber(iss);
				if (stride == 0)
					throw misc::ThreadAffinitySyntaxError("Zero stride");
			}
		}

		// stream should be consumed at this point
		if (!iss.eof())
			throw misc::ThreadAffinitySyntaxError("Trailing characters");
		for (auto i = lower; i <= upper; i += stride)
			mask.set(i);
	}

	return mask;
}

ThreadManager& ThreadManager::instance()
{
	if (threadManager == nullptr)
		threadManager.reset(new ThreadManager);
	return *threadManager;
}

void ThreadManager::defineAffinity(
	const std::string& name, const CPUAffinityMask& mask)
{
	m_affinityMask[name].addAffinity(mask);
}

void ThreadManager::setThreadAffinity(
	std::thread& t, const std::string& name, bool setThreadName)
{
	if (auto mask = findAffinity(name)) {
		misc::setThreadAffinity(t, *mask);
	}
	if (setThreadName)
		misc::setThreadName(t, name);
}

void ThreadManager::setThreadAffinity(
	const std::string& name, bool setThreadName)
{
	if (auto mask = findAffinity(name)) {
		misc::setThreadAffinity(*mask);
	}
	if (setThreadName)
		misc::setThreadName(name);
}

void ThreadManager::setThreadName(std::thread& t, const std::string& name)
{
	misc::setThreadName(t, name);
}

void ThreadManager::setThreadName(const std::string& name)
{
	misc::setThreadName(name);
}

const CPUAffinityMask* ThreadManager::findAffinity(const std::string& name)
{
	auto i = m_affinityMask.find(name);
	if (i == m_affinityMask.end())
		return nullptr;
	return i->second.getNextAffinity();
}

void ThreadManager::defineAffinities(
	const std::string& arg, const std::set<std::string>& names)
{
	std::istringstream affStream(arg);
	do {
		std::string affSpec;
		std::getline(affStream, affSpec, ' ');
		std::istringstream affStream(affSpec);

		std::string affName;
		std::getline(affStream, affName, '=');

		if (affStream.eof())
			throw ThreadAffinitySyntaxError("Missing '='");
		if (!names.empty() && names.find(affName) == names.end())
			throw ThreadNameError(
				std::string("unknown name '") + affName + "'");

		CPUAffinityMask mask;
		std::string maskToken;
		do {
			std::getline(affStream, maskToken, ',');
			mask |= cpuAffinityMaskFromString(maskToken);
		} while (!affStream.eof());
		defineAffinity(affName, mask);
	} while (!affStream.eof());
}

void ThreadManager::defineAffinities(const YAML::Node& pl)
{
	for (auto& affinity : pl) {
		const auto& affinityName = affinity["Name"].as<std::string>();
		const auto& affinityMask = affinity["Mask"].as<std::string>();
		if (affinityMask.empty())
			continue;
		APP_DEBUG(1, "Using CPU affinity: %s = %s", affinityName.c_str(),
			affinityMask.c_str());
		defineAffinity(affinityName, cpuAffinityMaskFromString(affinityMask));
	}
}

ThreadManager::ThreadInfo::ThreadInfo()
	: m_cur(new std::atomic_size_t(0))
{
}

void ThreadManager::ThreadInfo::addAffinity(const CPUAffinityMask& mask)
{
	m_cpus.push_back(mask);
}

const CPUAffinityMask* ThreadManager::ThreadInfo::getNextAffinity()
{
	if (m_cpus.empty())
		return nullptr;
	size_t cur = *m_cur, next;
	do {
		next = (cur + 1) % m_cpus.size();
	} while (!m_cur->compare_exchange_strong(cur, next));
	return &m_cpus[cur];
}

void setThreadAffinity(std::thread& t, const CPUAffinityMask& mask)
{
	auto& cObj = mask.cObj();
	auto err = pthread_setaffinity_np(t.native_handle(), sizeof(cObj), &cObj);
	if (err != 0)
		throw std::system_error(
			err, std::system_category(), "pthread_setaffinity_np() failed");
}

void setThreadAffinity(const CPUAffinityMask& mask)
{
	auto& cObj = mask.cObj();
	auto err = pthread_setaffinity_np(pthread_self(), sizeof(cObj), &cObj);
	if (err != 0)
		throw std::system_error(
			err, std::system_category(), "pthread_setaffinity_np() failed");
}

CPUAffinityMask getThreadAffinity(std::thread& t)
{
	cpu_set_t mask;
	auto err = pthread_getaffinity_np(t.native_handle(), sizeof(mask), &mask);
	if (err != 0)
		throw std::system_error(
			err, std::system_category(), "pthread_getaffinity_np() failed");
	return CPUAffinityMask(mask);
}

CPUAffinityMask getThreadAffinity()
{
	cpu_set_t mask;
	auto err = pthread_getaffinity_np(pthread_self(), sizeof(mask), &mask);
	if (err != 0)
		throw std::system_error(
			err, std::system_category(), "pthread_getaffinity_np() failed");
	return CPUAffinityMask(mask);
}

void setThreadName(std::thread& t, const std::string& name)
{
	const auto sub_str = name.substr(0, 15);
	auto err = pthread_setname_np(t.native_handle(), sub_str.c_str());
	if (err != 0)
		throw std::system_error(
			err, std::system_category(), "pthread_setname_np() failed");
}

void setThreadName(const std::string& name)
{
	const auto sub_str = name.substr(0, 15);
	auto err = pthread_setname_np(pthread_self(), sub_str.c_str());
	if (err != 0)
		throw std::system_error(
			err, std::system_category(), "pthread_setname_np() failed");
}

} // namespace misc
