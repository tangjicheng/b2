#include "misc/Address.h"

#include <arpa/inet.h>

#include <limits>

namespace misc { namespace io {

std::tuple<std::uint32_t, bool> ip_address_to_int(const std::string& address)
{
	if (address.empty())
		return {0u, false};

	struct in_addr client_addr;
	if (inet_pton(AF_INET, address.c_str(), &(client_addr)) <= 0)
		return {0u, false};
	return {ntohl(client_addr.s_addr), true};
}

std::tuple<std::uint16_t, bool> ip_port_to_int(const std::string& port)
{
	if (port.empty())
		return {0u, false};

	auto output = std::int32_t {0u};
	try {
		output = std::stoi(port);
	}
	catch (const std::exception& e) {
		return {0u, false};
	}
	if (output < 0 || output > std::numeric_limits<std::uint16_t>::max())
		return {0u, false};
	return {output, true};
}

std::string int_to_ip_address(std::uint32_t address)
{
	struct in_addr ip_addr;
	ip_addr.s_addr = htonl(address);
	return std::string {inet_ntoa(ip_addr)};
}

std::string int_to_ip_port(std::uint16_t port)
{
	return std::to_string(port);
}

Address::Address(const std::string& host, const std::string& service)
	: m_host(host)
	, m_service(service)
{
}

Address::Address(std::string&& host, std::string&& service)
	: m_host {std::move(host)}
	, m_service {std::move(service)}
{
}

bool Address::matches(const Address& other) const
{
	if (m_host == other.host() || m_host == address_any_ipv4
		|| other.host() == address_any_ipv4)
		return m_service == other.service();
	return false;
}

void Address::complete(const Address& defaultAddress)
{
	if (m_host.empty())
		m_host = defaultAddress.host();
	if (m_service.empty())
		m_service = defaultAddress.service();
}

std::string Address::to_string() const
{
	return m_host + ':' + m_service;
}

std::optional<misc::io::Address> to_address(const std::string& address_port)
{
	auto n = address_port.find(':');
	if (n == std::string::npos)
		return std::nullopt;

	auto host = address_port.substr(0, n);
	auto service = address_port.substr(n + 1);
	if (host.empty() || service.empty())
		return std::nullopt;

	return misc::io::Address {host, service};
}

}} // namespace misc::io
