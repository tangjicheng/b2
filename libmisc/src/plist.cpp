#include "misc/plist.h"

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <functional>
#include <ios>

#include "misc/Finally.h"
#include "misc/indent.h"

namespace plist {

void Plist::write(
	std::ostream& os, unsigned int depth, const details::Writer& writer) const
{
	os << misc::indent(depth) << "<plist version=\"1.0\">\n";
	writer(m_root, os, depth + 1);
	os << misc::indent(depth) << "</plist>\n";
}

Element& Dict::operator[](const std::string& key)
{
	auto i = m_dict.find(key);
	if (i == m_dict.end())
		throw KeyError(key);
	return *i->second;
}

const Element& Dict::operator[](const std::string& key) const
{
	auto i = m_dict.find(key);
	if (i == m_dict.end())
		throw KeyError(key);
	return *i->second;
}

std::string Dict::getString(
	const std::string& key, const std::string& def) const
{
	auto i = m_dict.find(key);
	if (i == m_dict.end())
		return def;
	return i->second->asString();
}

bool Dict::getBool(const std::string& key, bool def) const
{
	auto i = m_dict.find(key);
	if (i == m_dict.end())
		return def;
	return i->second->asBool();
}

long Dict::getInteger(const std::string& key, long def) const
{
	auto i = m_dict.find(key);
	if (i == m_dict.end())
		return def;
	return i->second->asInteger();
}

double Dict::getReal(const std::string& key, double def) const
{
	auto i = m_dict.find(key);
	if (i == m_dict.end())
		return def;
	return i->second->asReal();
}

void Dict::write(
	std::ostream& os, unsigned int depth, const details::Writer& writer) const
{
	os << misc::indent(depth) << "<dict>\n";
	for (auto& [key, element] : m_dict) {
		os << misc::indent(depth + 1) << "<key>" << key << "</key>\n";
		writer(element, os, depth + 1);
	}
	os << misc::indent(depth) << "</dict>\n";
}

Element& Array::operator[](size_t i)
{
	if (i >= m_array.size())
		throw IndexError(i);
	return *m_array[i];
}

const Element& Array::operator[](size_t i) const
{
	if (i >= m_array.size())
		throw IndexError(i);
	return *m_array[i];
}

std::string Array::getString(size_t i, const std::string& def) const
{
	if (i >= m_array.size())
		return def;
	return m_array[i]->asString();
}

bool Array::getBool(size_t i, bool def) const
{
	if (i >= m_array.size())
		return def;
	return m_array[i]->asBool();
}

long Array::getInteger(size_t i, long def) const
{
	if (i >= m_array.size())
		return def;
	return m_array[i]->asInteger();
}

double Array::getReal(size_t i, double def) const
{
	if (i >= m_array.size())
		return def;
	return m_array[i]->asReal();
}

void Array::write(
	std::ostream& os, unsigned int depth, const details::Writer& writer) const
{
	os << misc::indent(depth) << "<array>\n";
	for (auto element : m_array)
		writer(element, os, depth + 1);
	os << misc::indent(depth) << "</array>\n";
}

void StringValue::write(
	std::ostream& os, unsigned int depth, const details::Writer&) const
{
	os << misc::indent(depth) << "<string>" << m_value << "</string>\n";
}

void BoolValue::write(
	std::ostream& os, unsigned int depth, const details::Writer&) const
{
	os << misc::indent(depth) << (m_value ? "<true/>\n" : "<false/>\n");
}

IntegerValue::IntegerValue(const char* value)
{
	errno = 0;
	char* endptr;
	m_value = strtol(value, &endptr, 10);
	if (endptr == value || *endptr != '\0' || errno == ERANGE)
		throw TypeError();
}

void IntegerValue::write(
	std::ostream& os, unsigned int depth, const details::Writer&) const
{
	os << misc::indent(depth) << "<integer>" << m_value << "</integer>\n";
}

RealValue::RealValue(const char* value)
{
	errno = 0;
	char* endptr;
	m_value = strtod(value, &endptr);
	if (endptr == value || *endptr != '\0' || errno == ERANGE)
		throw TypeError();
}

void RealValue::write(
	std::ostream& os, unsigned int depth, const details::Writer&) const
{
	os << misc::indent(depth) << "<real>" << m_value << "</real>\n";
}

Plist PlistParser::parseFile(const char* filename)
{
	std::ifstream ifs(filename);
	if (!ifs)
		throw OpenFailure();
	return parseStream(ifs);
}

Plist PlistParser::parseStream(std::istream& is)
{
	auto except = is.exceptions();
	is.exceptions(std::istream::failbit | std::istream::badbit);
	auto restore = misc::finally([except, &is] { is.exceptions(except); });
	resetParser();
	for (bool done = false; !done;) {
		char buf[BUFSIZ];
		size_t len = is.readsome(buf, sizeof(buf));
		done = len < sizeof(buf);
		if (XML_Parse(m_parser, buf, len, done) == XML_STATUS_ERROR) {
			delete m_root;
			m_root = nullptr;
			throw ParseError(
				XML_GetErrorCode(m_parser), XML_GetCurrentLineNumber(m_parser));
		}
	}
	if (m_root == nullptr)
		throw FormatError(
			"Null root element", XML_GetCurrentLineNumber(m_parser));
	Plist pl(m_root);
	m_root = nullptr;
	return pl;
}

void PlistParser::resetParser()
{
	XML_ParserReset(m_parser, nullptr);
	XML_SetUserData(m_parser, this);
	XML_SetElementHandler(
		m_parser, &PlistParser::startElement, &PlistParser::endElement);
	XML_SetCharacterDataHandler(m_parser, &PlistParser::characterData);
	m_root = nullptr;
	m_stack = decltype(m_stack)();
	m_cur_key.clear();
	m_cur_data.clear();
}

void PlistParser::addElement(Element* element)
{
	if (m_stack.empty()) {
		if (m_root != nullptr)
			throw FormatError(
				"Multiple root elements", XML_GetCurrentLineNumber(m_parser));
		m_root = element;
		return;
	}
	if (!m_cur_key.empty()) {
		m_stack.top()->asDict().insert(m_cur_key, element);
		m_cur_key.clear();
	}
	else
		m_stack.top()->asArray().append(element);
}

void PlistParser::startElement(const char* name, const char**)
{
	m_cur_data.clear();
	if (strcmp(name, "dict") == 0) {
		auto dict = new Dict();
		addElement(dict);
		m_stack.push(dict);
	}
	else if (strcmp(name, "array") == 0) {
		auto array = new Array();
		addElement(array);
		m_stack.push(array);
	}
}

void PlistParser::endElement(const char* name)
{
	if (strcmp(name, "dict") == 0 || strcmp(name, "array") == 0)
		m_stack.pop();
	else if (strcmp(name, "key") == 0)
		m_cur_key = m_cur_data;
	else if (strcmp(name, "string") == 0)
		addElement(new StringValue(m_cur_data));
	else if (strcmp(name, "true") == 0)
		addElement(new BoolValue(true));
	else if (strcmp(name, "false") == 0)
		addElement(new BoolValue(false));
	else if (strcmp(name, "integer") == 0)
		addElement(new IntegerValue(m_cur_data));
	else if (strcmp(name, "real") == 0)
		addElement(new RealValue(m_cur_data));
}

void PlistWriter::writeFile(const Plist& pl, const char* filename) const
{
	std::ofstream ofs(filename);
	if (!ofs)
		throw OpenFailure();
	writeStream(pl, ofs);
}

void PlistWriter::writeStream(const Plist& pl, std::ostream& os) const
{
	os << "<\?xml version=\"1.0\" encoding=\"UTF-8\"\?>\n"
	   << "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" "
		  "\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n";
	details::Writer()(&pl, os, 0);
	os << std::flush;
}

} // namespace plist
