#include "misc/timegm.h"

#include <cstdlib>

namespace misc {

time_t timegm(tm* tm)
{
	char* tz = getenv("TZ");
	setenv("TZ", "", 1);
	tzset();
	time_t t = mktime(tm);
	if (tz)
		setenv("TZ", tz, 1);
	else
		unsetenv("TZ");
	tzset();
	return t;
}

} // namespace misc
