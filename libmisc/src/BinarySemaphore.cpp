#include "misc/BinarySemaphore.h"

namespace misc {

void BinarySemaphore::wait()
{
	std::unique_lock<std::mutex> lk(m_mtx);
	while (!m_signalled)
		m_cv.wait(lk);
	m_signalled = false;
}

void BinarySemaphore::signal()
{
	std::lock_guard<std::mutex> lk(m_mtx);
	m_signalled = true;
	m_cv.notify_one();
}

} // namespace misc
