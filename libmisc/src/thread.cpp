#include "misc/thread.h"

#include <sys/types.h>
#include <unistd.h>

#include <sys/syscall.h>

#ifndef SYS_gettid
#error "SYS_gettid unavailable on this system"
#endif

namespace misc {

int get_tid()
{
	return static_cast<pid_t>(syscall(SYS_gettid));
}

} // namespace misc
