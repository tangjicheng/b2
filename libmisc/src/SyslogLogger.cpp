#include "misc/SyslogLogger.h"

#define SYSLOG_NAMES
#include <stddef.h> // NULL is used but not declared in syslog.h.
#include <syslog.h>

#include <cstring>

#include "misc/Log.h"

namespace misc {

namespace {

int toSyslogPriority(int priority) noexcept
{
	switch (priority) {
	case APP_LOG_EMERG:
		return LOG_EMERG;
	case APP_LOG_ALERT:
		return LOG_ALERT;
	case APP_LOG_CRIT:
		return LOG_CRIT;
	case APP_LOG_ERR:
		return LOG_ERR;
	case APP_LOG_WARNING:
		return LOG_WARNING;
	case APP_LOG_NOTICE:
		return LOG_NOTICE;
	case APP_LOG_INFO:
		return LOG_INFO;
	case APP_LOG_DEBUG:
		return LOG_DEBUG;
	default:
		// Highest priority if precondition is violated.
		return LOG_EMERG;
	}
}

} // namespace

SyslogLogger::SyslogLogger(const char* ident, int option, int facility) noexcept
{
	appSetLogger(&SyslogLogger::logger);
	openlog(ident, option, facility);
}

SyslogLogger::~SyslogLogger() noexcept
{
	closelog();
}

void SyslogLogger::logger(int priority, const char* format, va_list ap) noexcept
{
	auto syslogPriority = toSyslogPriority(priority);
	vsyslog(syslogPriority, format, ap);
}

int toSyslogFacility(const char* facility)
{
	for (CODE* p = facilitynames; p->c_name != nullptr; ++p) {
		if (strcmp(facility, p->c_name) == 0)
			return p->c_val;
	}
	throw SyslogLoggerError();
}

} // namespace misc
