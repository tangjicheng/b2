#include "misc/fix/Converters.h"

#include <cstdint>
#include <utility>

#include "misc/fix/UTCTimeDate.h"

namespace misc { namespace fix {

namespace details {

/**
 *  @throw std::invalid_argument If @a c is an invalid character.
 */
void validateChar(char c)
{
	// The FIX specification states "any alphanumeric character or
	// punctuation except the delimiter". The delimiter is SOH (0x01).
	if (!isgraph(c))
		throw std::invalid_argument("Invalid character");
}

/**
 * @throw std::invalid_argument If @a str contains invalid characters.
 */
void validateString(const std::string& str)
{
	for (char c : str)
		details::validateChar(c);
}

} // namespace details

char charConvert(const std::string& str)
{
	if (str.size() != 1)
		throw std::invalid_argument("Bad format");
	auto c = str[0];
	details::validateChar(c);
	return c;
}

std::string charConvert(char c)
{
	details::validateChar(c);
	return std::string(1, c);
}

bool booleanConvert(const std::string& str)
{
	if (str.size() != 1)
		throw std::invalid_argument("Bad format");
	auto c = str[0];
	if (c == 'Y')
		return true;
	if (c == 'N')
		return false;
	throw std::invalid_argument("Invalid character");
}

std::string booleanConvert(bool b) noexcept
{
	return std::string(1, b ? 'Y' : 'N');
}

const std::string& stringConvert(const std::string& str)
{
	if (str.empty())
		throw std::invalid_argument("Bad format");
	details::validateString(str);
	return str;
}

std::vector<char> multipleCharValueConvert(const std::string& str)
{
	if (str.empty())
		throw std::invalid_argument("Bad format");
	std::vector<char> vec;
	for (auto i = str.begin();;) {
		details::validateChar(*i);
		vec.push_back(*i);
		if (++i == str.end())
			break;
		if (*i != ' ')
			throw std::invalid_argument("Bad format");
		if (++i == str.end())
			throw std::invalid_argument("Bad format");
	}
	return vec;
}

std::string multipleCharValueConvert(const std::vector<char>& vec)
{
	if (vec.empty())
		throw std::invalid_argument("Bad format");
	std::string str;
	for (auto c : vec) {
		details::validateChar(c);
		str.push_back(c);
		str.push_back(' ');
	}
	str.pop_back();
	return str;
}

std::vector<std::string> multipleStringValueConvert(const std::string& str)
{
	if (str.empty())
		throw std::invalid_argument("Bad format");
	std::vector<std::string> vec;
	for (auto i = str.begin();;) {
		std::string s;
		do {
			details::validateChar(*i);
			s.push_back(*i);
			++i;
		} while (i != str.end() && *i != ' ');
		vec.push_back(std::move(s));
		if (i == str.end())
			break;
		if (++i == str.end())
			throw std::invalid_argument("Bad format");
	}
	return vec;
}

std::string multipleStringValueConvert(const std::vector<std::string>& vec)
{
	if (vec.empty())
		throw std::invalid_argument("Bad format");
	std::string str;
	for (auto& s : vec) {
		details::validateString(s);
		str.append(s);
		str.push_back(' ');
	}
	str.pop_back();
	return str;
}

uint32_t checksumConvert(const std::string& str)
{
	if (str.size() != 3)
		throw std::invalid_argument("Bad format");
	auto value = details::stringToIntPad<3, uint32_t>(str.data());
	if (value > 255)
		throw std::invalid_argument("Out of range");
	return value;
}

std::string checksumConvert(uint32_t value)
{
	if (value > 255)
		throw std::invalid_argument("Out of range");
	char buf[3];
	auto end = buf + sizeof(buf);
	details::intToStringPad<sizeof(buf)>(value, end);
	return std::string(buf, end);
}

namespace details {

/**
 *  @throw std::invalid_argument If the string defined by the range [@a p, @a p
 *  + 8) contains invalid characters or is out of range.
 */
UTCDateOnly stringToDate(const char* p)
{
	auto year = details::stringToIntPad<4, uint32_t>(p);
	auto month = details::stringToIntPad<2, uint32_t>(p + 4);
	auto day = details::stringToIntPad<2, uint32_t>(p + 6);
	if (month < 1 || month > 12 || day < 1 || day > 31)
		throw std::invalid_argument("Out of range");
	return UTCDateOnly(year, month, day);
}

char* dateToString(UTCDateOnly date, char* p) noexcept
{
	p = details::intToStringPad<2>(date.day(), p);
	p = details::intToStringPad<2>(date.month(), p);
	p = details::intToStringPad<4>(date.year(), p);
	return p;
}

/**
 *  @throw std::invalid_argument If the string defined by the range [@a p, @a p
 *  + 8) or [@a p, @a p + 12) has bad format, contains invalid characters or is
 *  out of range.
 */
UTCTimeOnly stringToTime(const char* p)
{
	if (p[2] != ':' || p[5] != ':')
		throw std::invalid_argument("Bad format");
	auto hour = details::stringToIntPad<2, uint32_t>(p);
	auto minute = details::stringToIntPad<2, uint32_t>(p + 3);
	auto second = details::stringToIntPad<2, uint32_t>(p + 6);
	if (hour > 23 || minute > 59 || second > 60)
		throw std::invalid_argument("Out of range");
	if (p[8] == '\0')
		return UTCTimeOnly(hour, minute, second);
	if (p[8] != '.')
		throw std::invalid_argument("Bad format");
	auto msec = details::stringToIntPad<3, uint32_t>(p + 9);
	return UTCTimeOnly(hour, minute, second, msec);
}

char* timeToString(UTCTimeOnly time, char* p) noexcept
{
	if (time.hasMilliseconds()) {
		p = details::intToStringPad<3>(time.msec(), p);
		*--p = '.';
	}
	p = details::intToStringPad<2>(time.second(), p);
	*--p = ':';
	p = details::intToStringPad<2>(time.minute(), p);
	*--p = ':';
	p = details::intToStringPad<2>(time.hour(), p);
	return p;
}

} // namespace details

UTCDateOnly utcDateOnlyConvert(const std::string& str)
{
	// YYYYMMDD
	if (str.size() != 8)
		throw std::invalid_argument("Bad format");
	return details::stringToDate(str.data());
}

std::string utcDateOnlyConvert(UTCDateOnly date) noexcept
{
	// YYYYMMDD
	char buf[8];
	auto end = buf + sizeof(buf);
	details::dateToString(date, end);
	return std::string(buf, end);
}

UTCTimeOnly utcTimeOnlyConvert(const std::string& str)
{
	// HH:MM:SS.sss
	if (str.size() != 8 && str.size() != 12)
		throw std::invalid_argument("Bad format");
	return details::stringToTime(str.data());
}

std::string utcTimeOnlyConvert(UTCTimeOnly time) noexcept
{
	// HH:MM:SS.sss
	char buf[12];
	auto end = buf + sizeof(buf);
	auto begin = details::timeToString(time, end);
	return std::string(begin, end);
}

UTCTimestamp utcTimestampConvert(const std::string& str)
{
	// YYYYMMDD-HH:MM:SS.sss
	if (str.size() != 17 && str.size() != 21)
		throw std::invalid_argument("Bad format");
	if (str[8] != '-' || str[11] != ':' || str[14] != ':')
		throw std::invalid_argument("Bad format");
	auto date = details::stringToDate(str.data());
	auto time = details::stringToTime(str.data() + 9);
	return UTCTimestamp(date, time);
}

std::string utcTimestampConvert(const UTCTimestamp& timestamp) noexcept
{
	// YYYYMMDD-HH:MM:SS.sss
	char buf[21];
	auto end = buf + sizeof(buf);
	auto begin = details::timeToString(timestamp.time(), end);
	*--begin = '-';
	begin = details::dateToString(timestamp.date(), begin);
	return std::string(begin, end);
}

}} // namespace misc::fix
