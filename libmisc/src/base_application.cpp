#include "misc/base_application.h"

#include <signal.h>
#include <syslog.h>
#include <unistd.h>

#include <cerrno>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <exception>
#include <memory>
#include <sstream>
#include <system_error>
#include <thread>

#include "misc/Log.h"
#include "misc/PidFile.h"
#include "misc/SyslogLogger.h"
#include "misc/application_exception.h"
#include "misc/form.h"
#include "misc/option_exception.h"

namespace misc {

namespace {

void processDebugLevel(const char* argument)
{
	std::istringstream iss(argument);
	if (!(iss >> debug))
		throw InvalidOptionArgument('d', "invalid debug level");
}

std::string processConfigFilename(const char* argument)
{
	std::string filename(argument);
	if (filename.empty())
		throw InvalidOptionArgument('f', "invalid configuration filename");
	return filename;
}

int processSyslogFacility(const char* argument)
{
	try {
		return toSyslogFacility(argument);
	}
	catch (const SyslogLoggerError&) {
		throw InvalidOptionArgument('l', "invalid syslog facility");
	}
}

std::string processPidFilename(const char* argument)
{
	std::string filename(argument);
	if (filename.empty())
		throw InvalidOptionArgument('p', "invalid PID filename");
	return filename;
}

void onUnhandledSignal(int signo)
{
	appLog(APP_LOG_NOTICE, "Unhandled signal %d", signo);
}

} // namespace

Base_application::Base_application(const std::string& applicationName,
	const std::string& executableName, const Version& version)
	: Base_application(
		applicationName, executableName, std::vector<Option>(), version)
{
}

Base_application::Base_application(const std::string& applicationName,
	const std::string& executableName, const std::vector<Option>& extraOptions,
	const Version& version)
	: application_name_(applicationName)
	, executable_name_(executableName)
	, config_filename_(application_name_ + ".yml")
	, default_pid_filename_(executable_name_ + ".pid")
	, extra_options_(extraOptions)
	, version_(version)
	, application_reactor_ {false, true, true}
{
	options_.emplace('d', true, "debug", "debug level [0]");
	options_.emplace('D', true, std::nullopt, "do not run as a daemon");
	options_.emplace('f', true, "config-file",
		misc::form("configuration filename [%s]", config_filename_.c_str()));
	options_.emplace('h', false, std::nullopt, "display this help and exit");
	options_.emplace(
		'l', true, "syslog-facility", "syslog facility name [LOG_LOCAL0]");
	options_.emplace('p', true, "PID-file",
		misc::form("PID filename [%s]", default_pid_filename_.c_str()));
	options_.emplace(
		'v', false, std::nullopt, "output version information and exit");

	application_reactor_.signalSystemError() = [](const std::system_error& e) {
		appLog(APP_LOG_EMERG, "Reactor system error: %s (error %s:%d)",
			e.what(), e.code().category().name(), e.code().value());
		auto eptr = std::current_exception();
		if (eptr)
			std::rethrow_exception(eptr);
	};
}

int Base_application::run(int argc, char** argv)
{
	for (auto& option : extra_options_) {
		if (!options_.insert(option).second) {
			fprintf(stderr, "%s: Duplicate option -%c\n",
				executable_name_.c_str(), option.character);
			return EXIT_FAILURE;
		}
	}

	bool daemonize = true;
	bool useSyslog = false;
	int syslogFacility = LOG_LOCAL0;
	auto pidFilename = default_pid_filename_;

	try {
		auto optionString = this->optionString();
		int option;
		while ((option = getopt(argc, argv, optionString.c_str())) != -1) {
			switch (option) {
			case 'd':
				processDebugLevel(optarg);
				break;
			case 'D':
				daemonize = false;
				break;
			case 'f':
				config_filename_ = processConfigFilename(optarg);
				break;
			case 'h':
				printUsage();
				return EXIT_SUCCESS;
			case 'l':
				syslogFacility = processSyslogFacility(optarg);
				useSyslog = true;
				break;
			case 'p':
				pidFilename = processPidFilename(optarg);
				break;
			case 'v':
				print_version();
				return EXIT_SUCCESS;
			case ':':
				throw MissingOptionArgument(optopt);
			case '?':
				throw InvalidOption(optopt);
			default:
				onOption(option, optarg);
				break;
			}
		}
		onEndOfOptions();
	}
	catch (const OptionError& e) {
		fprintf(stderr, "%s: %s\n", executable_name_.c_str(), e.what());
		fprintf(stderr, "%s: Try '%s -h' for more information.\n",
			executable_name_.c_str(), executable_name_.c_str());
		return EXIT_FAILURE;
	}

	std::unique_ptr<SyslogLogger> slog;
	if (daemonize || useSyslog)
		slog.reset(new SyslogLogger(
			executable_name_.c_str(), LOG_NDELAY | LOG_PID, syslogFacility));

	print_start_log();

	PidFile pidFile(pidFilename);
	try {
		pidFile.open(0644);
	}
	catch (const DaemonAlreadyRunning& e) {
		appLog(APP_LOG_EMERG, "Daemon already running (PID %d)",
			static_cast<int>(e.pid()));
		return EXIT_FAILURE;
	}
	catch (const std::system_error& e) {
		appLog(APP_LOG_EMERG, "Cannot open PID file: %s (error %s:%d)",
			e.what(), e.code().category().name(), e.code().value());
		return EXIT_FAILURE;
	}
	catch (const std::exception& e) {
		appLog(APP_LOG_EMERG, "Cannot open PID file: %s", e.what());
		return EXIT_FAILURE;
	}
	if (daemonize) {
		auto pid = getpid();
		if (daemon(1, 0) == -1) {
			appLog(APP_LOG_EMERG, "Cannot daemonize: %s (errno %d)",
				strerror(errno), errno);
			return EXIT_FAILURE;
		}
		appLog(APP_LOG_INFO, "Running as daemon (parent PID %d)",
			static_cast<int>(pid));
	}
	try {
		pidFile.write();
	}
	catch (const std::system_error& e) {
		appLog(APP_LOG_EMERG, "Cannot write PID file: %s (error %s:%d)",
			e.what(), e.code().category().name(), e.code().value());
		return EXIT_FAILURE;
	}

	sigemptyset(&signals_);
	sigaddset(&signals_, SIGINT);
	sigaddset(&signals_, SIGQUIT);
	sigaddset(&signals_, SIGTERM);
	sigaddset(&signals_, SIGHUP);
	sigaddset(&signals_, SIGUSR1);
	sigaddset(&signals_, SIGUSR2);
	sigaddset(&signals_, SIGPIPE);
	pthread_sigmask(SIG_BLOCK, &signals_, nullptr);

	try {
		misc::appLog(misc::APP_LOG_INFO, "Configuration file (%s). Loading..",
			config_filename_.c_str());
		const auto& config = YAML::LoadFile(config_filename_);
		appLog(APP_LOG_INFO, "Initializing...");
		init(config);
		misc::appLog(misc::APP_LOG_INFO,
			"Configuration file (%s). Parsing completed.",
			config_filename_.c_str());
	}
	catch (const ConfigurationError& e) {
		appLog(APP_LOG_EMERG, "Configuration error (file %s): %s",
			config_filename_.c_str(), e.what());
		return EXIT_FAILURE;
	}
	catch (const CriticalError& e) {
		appLog(APP_LOG_EMERG, "Critical error: %s", e.what());
		return EXIT_FAILURE;
	}
	catch (const std::exception& e) {
		appLog(APP_LOG_EMERG, "Initialization error: %s", e.what());
		return EXIT_FAILURE;
	}

	return do_work();
}

void Base_application::shutdown()
{
	keep_going_ = false;
}

int Base_application::do_work()
{
	appLog(APP_LOG_INFO, "Starting...");
	onStart();
	appLog(APP_LOG_INFO, "Ready");

	application_reactor_.init();
	application_reactor_.callEvery(std::chrono::seconds {1}, [this]() {
		siginfo_t info;
		timespec timeout {0, 0};
		if (sigtimedwait(&signals_, &info, &timeout) == -1)
			return misc::Reactor::CallStatus::OK;
		switch (info.si_signo) {
		case SIGINT:
		case SIGQUIT:
		case SIGTERM:
			application_reactor_.shutdown();
			break;
		case SIGHUP:
			onHangupSignal();
			break;
		case SIGUSR1:
			onUserSignal1();
			break;
		case SIGUSR2:
			onUserSignal2();
			break;
		default:
			onUnhandledSignal(info.si_signo);
			break;
		}
		return misc::Reactor::CallStatus::OK;
	});
	register_health_jobs();

	application_reactor_.run();

	appLog(APP_LOG_INFO, "Shutting down...");
	appLog(APP_LOG_INFO, "Finished");

	return EXIT_SUCCESS;
}

std::string Base_application::optionString() const
{
	std::string str(":");
	for (auto& option : options_) {
		str += option.character;
		if (option.argument.has_value())
			str += ':';
	}
	return str;
}

void Base_application::print_version() const
{
	printf("%s %s\ncommit_id=%s\nbuild_date=%s\n", executable_name_.c_str(),
		version_.number, version_.commit_id, version_.build_date);
	printf("%s@%s:%s\n", version_.build_user, version_.build_host,
		version_.build_path);
}

void Base_application::print_start_log() const
{
	appLog(APP_LOG_INFO, "Starting up %s (version=%s, commit_id=%s)",
		application_name_.c_str(), version_.number, version_.commit_id);
}

void Base_application::printUsage() const
{
	printf("Usage: %s", executable_name_.c_str());
	for (auto& option : options_) {
		if (option.character == 'h' || option.character == 'v')
			continue;
		printf(" ");
		if (option.is_optional)
			printf("[");
		printf("-%c", option.character);
		if (option.argument.has_value())
			printf(" <%s>", option.argument->c_str());
		if (option.is_optional)
			printf("]");
	}
	printf("\n");
	printf("       %s -h\n", executable_name_.c_str());
	printf("       %s -v\n", executable_name_.c_str());
	printf("\n");
	for (auto& option : options_)
		printf("  -%c    %s\n", option.character, option.details.c_str());
}

void Base_application::init(const YAML::Node& config)
{
	onInit(config);
	application_reactor_.idleStrategy(get_application_reactor_idle_strategy());
}

misc::Reactor::idle_strategy
Base_application::get_application_reactor_idle_strategy()
{
	return ([](bool) { std::this_thread::yield(); });
}

void Base_application::onHangupSignal()
{
	onUnhandledSignal(SIGHUP);
}

void Base_application::onUserSignal1()
{
	onUnhandledSignal(SIGUSR1);
}

void Base_application::onUserSignal2()
{
	onUnhandledSignal(SIGUSR2);
}

} // namespace misc
