#include "misc/Socket.h"

#include <netinet/tcp.h>

#include <cstring>

#include <sys/ioctl.h>

#include "misc/addrinfo_error.h"

namespace misc { namespace io {

Address_info::~Address_info()
{
	free();
}

Address_info::Address_info(Address_info&& other)
{
	move(other);
}

Address_info& Address_info::operator=(Address_info&& other)
{
	free();
	move(other);
	return *this;
}

Address_info::Address_info(const Address& address, Address_family family,
	Socket_type type, Address_info_flags flags)
	: Address_info(
		address.host().c_str(), address.service().c_str(), family, type, flags)
{
}

Address_info::Address_info(
	const Address& address, Socket_type type, Address_info_flags flags)
	: Address_info(address, Address_family::unspecified, type, flags)
{
}

Address_info Address_info::from_host(const std::string& host,
	Address_family family, Socket_type type, Address_info_flags flags)
{
	return {host.c_str(), nullptr, family, type, flags};
}

Address_info Address_info::from_host(
	const std::string& host, Socket_type type, Address_info_flags flags)
{
	return from_host(host, Address_family::unspecified, type, flags);
}

Address_info Address_info::from_service(const std::string& service,
	Address_family family, Socket_type type, Address_info_flags flags)
{
	return {nullptr, service.c_str(), family, type, flags};
}

Address_info Address_info::from_service(
	const std::string& service, Socket_type type, Address_info_flags flags)
{
	return from_service(service, Address_family::unspecified, type, flags);
}

Address_info::Address_info(const char* host, const char* service,
	Address_family family, Socket_type type, Address_info_flags flags)
{
	addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = static_cast<int>(family);
	hints.ai_socktype = static_cast<int>(type);
	hints.ai_flags = flags.value();
	getaddrinfo(host, service, &hints);
}

void Address_info::free()
{
	if (m_addrinfo == nullptr)
		return;
	freeaddrinfo(m_addrinfo);
	m_addrinfo = nullptr;
}

void Address_info::move(Address_info& other)
{
	m_addrinfo = other.m_addrinfo;
	other.m_addrinfo = nullptr;
}

void Address_info::getaddrinfo(
	const char* host, const char* service, const addrinfo* hints)
{
	auto error = ::getaddrinfo(host, service, hints, &m_addrinfo);
	if (error != 0) {
		if (error == EAI_SYSTEM)
			throw std::system_error(
				errno, std::system_category(), "getaddrinfo() failed");
		throw std::system_error(
			error, misc::addrinfo_category(), "getaddrinfo() failed");
	}
}

Socket_address::Socket_address(const Address_info& info)
{
	copy(info.address(), info.length());
}

Address Socket_address::to_address(Name_info_flags flags) const
{
	char host[NI_MAXHOST];
	char service[NI_MAXSERV];
	getnameinfo(host, sizeof(host), service, sizeof(service), flags.value());
	return {host, service};
}

std::string Socket_address::to_host(Name_info_flags flags) const
{
	char host[NI_MAXHOST];
	getnameinfo(host, sizeof(host), nullptr, 0, flags.value());
	return host;
}

std::string Socket_address::to_service(Name_info_flags flags) const
{
	char service[NI_MAXSERV];
	getnameinfo(nullptr, 0, service, sizeof(service), flags.value());
	return service;
}

void Socket_address::copy(const void* address, socklen_t length)
{
	memcpy(&m_address, address, length);
	m_length = length;
}

void Socket_address::getnameinfo(char* host, size_t host_length, char* service,
	size_t service_length, int flags) const
{
	auto error = ::getnameinfo(
		address(), m_length, host, host_length, service, service_length, flags);
	if (error != 0) {
		if (error == EAI_SYSTEM)
			throw std::system_error(
				errno, std::system_category(), "getnameinfo() failed");
		throw std::system_error(
			error, misc::addrinfo_category(), "getnameinfo() failed");
	}
}

Socket::~Socket()
{
	close();
}

Socket::Socket(Socket&& other)
{
	move(other);
}

Socket& Socket::operator=(Socket&& other)
{
	close();
	move(other);
	return *this;
}

Socket Socket::listen(const Address& address, Socket_stream_options options)
{
	Address_info info(address, Socket_type::stream, Address_info_flag::passive);
	Socket socket(info);

	socket.bind(info.address(), info.length());
	socket.listen(SOMAXCONN);
	socket.set_blocking_mode(options.blocking_mode);
	socket.set_nagle_algorithm(options.nagle_algorithm);
	socket.set_receive_buffer_size(options.receive_buffer_size);
	socket.set_send_buffer_size(options.send_buffer_size);

	return socket;
}

Socket Socket::accept(
	const Socket& listening_socket, Socket_stream_options options)
{
	Socket socket;
	socket.accept(listening_socket.fd(), nullptr, nullptr,
		listening_socket.get_blocking_mode());
	if (!socket.is_open())
		return {};

	socket.set_blocking_mode(options.blocking_mode);
	socket.set_nagle_algorithm(options.nagle_algorithm);
	socket.set_receive_buffer_size(options.receive_buffer_size);
	socket.set_send_buffer_size(options.send_buffer_size);

	return socket;
}

Socket Socket::connect(const Address& address, Socket_stream_options options)
{
	Address_info info(address, Socket_type::stream);
	Socket socket(info);

	// Set nonblocking mode before the connect call so that connect is also
	// blocking. If nonblocking mode is set then EINPROGRESS is expected from
	// the connect call.
	socket.set_blocking_mode(options.blocking_mode);
	socket.set_nagle_algorithm(options.nagle_algorithm);
	socket.set_receive_buffer_size(options.receive_buffer_size);
	socket.set_send_buffer_size(options.send_buffer_size);
	socket.connect(info.address(), info.length(), options.blocking_mode);

	return socket;
}

Socket Socket::connect(const Address& source_address, const Address& address,
	Socket_stream_options options)
{
	Address_info info(address, Socket_type::stream);
	Socket socket(info);

	Address_info source_info(source_address, Socket_type::stream);
	socket.bind(source_info.address(), source_info.length());

	// Set nonblocking mode before the connect call so that connect is also
	// blocking. If nonblocking mode is set then EINPROGRESS is expected from
	// the connect call.
	socket.set_blocking_mode(options.blocking_mode);
	socket.set_nagle_algorithm(options.nagle_algorithm);
	socket.set_receive_buffer_size(options.receive_buffer_size);
	socket.set_send_buffer_size(options.send_buffer_size);
	socket.connect(info.address(), info.length(), options.blocking_mode);

	return socket;
}

Socket Socket::datagram(const Address& address, Blocking_mode mode)
{
	Address_info info(address, Socket_type::datagram);
	Socket socket(info);

	socket.bind(info.address(), info.length());
	socket.set_blocking_mode(mode);

	return socket;
}

Socket Socket::multicast_listen(
	const std::string& group, const std::string& service, Blocking_mode mode)
{
	return multicast_listen(group, service, address_any_ipv4, mode);
}

Socket Socket::multicast_listen(const std::string& group,
	const std::string& service, const std::string& interface,
	Blocking_mode mode)
{
	auto socket = datagram(Address(group, service), mode);

	ip_mreqn mreq;

	auto group_info = Address_info::from_host(
		group, Address_family::ipv4, Socket_type::datagram);
	mreq.imr_multiaddr
		= reinterpret_cast<const sockaddr_in*>(group_info.address())->sin_addr;

	if (interface == address_any_ipv4)
		mreq.imr_address.s_addr = htonl(INADDR_ANY);
	else {
		auto interface_info = Address_info::from_host(
			interface, Address_family::ipv4, Socket_type::unspecified);
		mreq.imr_address
			= reinterpret_cast<const sockaddr_in*>(interface_info.address())
				  ->sin_addr;
	}

	mreq.imr_ifindex = 0;

	socket.setsockopt(IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));

	return socket;
}

Socket Socket::multicast_listen(const std::string& group,
	const std::string& service, const std::string& interface,
	const std::string& source, Blocking_mode mode)
{
	auto socket = datagram(Address(group, service), mode);

	ip_mreq_source mreq;

	auto group_info = Address_info::from_host(
		group, Address_family::ipv4, Socket_type::datagram);
	mreq.imr_multiaddr
		= reinterpret_cast<const sockaddr_in*>(group_info.address())->sin_addr;

	if (interface == address_any_ipv4)
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	else {
		auto interface_info = Address_info::from_host(
			interface, Address_family::ipv4, Socket_type::unspecified);
		mreq.imr_interface
			= reinterpret_cast<const sockaddr_in*>(interface_info.address())
				  ->sin_addr;
	}

	auto source_info = Address_info::from_host(
		source, Address_family::ipv4, Socket_type::unspecified);
	mreq.imr_sourceaddr
		= reinterpret_cast<const sockaddr_in*>(source_info.address())->sin_addr;

	socket.setsockopt(
		IPPROTO_IP, IP_ADD_SOURCE_MEMBERSHIP, &mreq, sizeof(mreq));

	return socket;
}

std::error_code Socket::error_status() const
{
	int error;
	socklen_t length = sizeof(error);
	getsockopt(SOL_SOCKET, SO_ERROR, &error, &length);
	return {error, std::system_category()};
}

Socket_address Socket::local_socket_address() const
{
	Socket_address address;
	address.m_length = sizeof(address.m_address);
	if (::getsockname(m_fd, reinterpret_cast<sockaddr*>(&address.m_address),
			&address.m_length)
		== -1)
		throw std::system_error(
			errno, std::system_category(), "getsockname() failed");
	return address;
}

Address Socket::local_address(Name_info_flags flags) const
{
	auto address = local_socket_address();
	return address.to_address(flags);
}

std::string Socket::local_host(Name_info_flags flags) const
{
	auto address = local_socket_address();
	return address.to_host(flags);
}

std::string Socket::local_service(Name_info_flags flags) const
{
	auto address = local_socket_address();
	return address.to_service(flags);
}

Socket_address Socket::remote_socket_address() const
{
	Socket_address address;
	address.m_length = sizeof(address.m_address);
	if (::getpeername(m_fd, reinterpret_cast<sockaddr*>(&address.m_address),
			&address.m_length)
		== -1)
		throw std::system_error(
			errno, std::system_category(), "getpeername() failed");
	return address;
}

Address Socket::remote_address(Name_info_flags flags) const
{
	auto address = remote_socket_address();
	return address.to_address(flags);
}

std::string Socket::remote_host(Name_info_flags flags) const
{
	auto address = remote_socket_address();
	return address.to_host(flags);
}

std::string Socket::remote_service(Name_info_flags flags) const
{
	auto address = remote_socket_address();
	return address.to_service(flags);
}

std::error_code Socket::shutdown()
{
	if (!m_connected)
		return {ENOTCONN, std::system_category()};
	return do_shutdown(SHUT_RDWR);
}

std::error_code Socket::close()
{
	if (m_connected)
		do_shutdown(SHUT_RDWR);
	if (m_fd == -1)
		return {EBADF, std::system_category()};
	return do_close();
}

Socket::Socket(const Address_info& info)
	: m_fd(::socket(info.family(), info.type(), info.protocol()))
{
	if (m_fd == -1)
		throw std::system_error(
			errno, std::system_category(), "socket() failed");
}

void Socket::move(Socket& other)
{
	m_fd = other.m_fd;
	other.m_fd = -1;
}

void Socket::bind(const sockaddr* address, socklen_t length)
{
	int reuse_addr = 1;
	setsockopt(SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr));

	if (::bind(m_fd, address, length) == -1)
		throw std::system_error(errno, std::system_category(), "bind() failed");
}

void Socket::listen(int backlog)
{
	if (::listen(m_fd, backlog) == -1)
		throw std::system_error(
			errno, std::system_category(), "listen() failed");
}

void Socket::accept(
	int listening_fd, sockaddr* address, socklen_t* length, Blocking_mode mode)
{
	auto fd = details::skip_EINTR(::accept, listening_fd, address, length);
	if (fd == -1) {
		if (mode == Blocking_mode::blocking
			|| (errno != EAGAIN && errno != EWOULDBLOCK))
			throw std::system_error(
				errno, std::system_category(), "accept() failed");
		return;
	}
	m_fd = fd;
	m_connected = true;
}

void Socket::connect(
	const sockaddr* address, socklen_t length, Blocking_mode mode)
{
	auto error = details::skip_EINTR(::connect, m_fd, address, length);
	if (error == -1) {
		if (mode == Blocking_mode::blocking || errno != EINPROGRESS)
			throw std::system_error(
				errno, std::system_category(), "connect() failed");
		return;
	}
	m_connected = true;
}

void Socket::getsockopt(
	int level, int name, void* value, socklen_t* length) const
{
	if (::getsockopt(m_fd, level, name, value, length) == -1)
		throw std::system_error(
			errno, std::system_category(), "getsockopt() failed");
}

void Socket::setsockopt(
	int level, int name, const void* value, socklen_t length)
{
	if (::setsockopt(m_fd, level, name, value, length) == -1)
		throw std::system_error(
			errno, std::system_category(), "setsockopt() failed");
}

int Socket::get_status_flags() const
{
	auto flags = ::fcntl(m_fd, F_GETFL);
	if (flags == -1)
		throw std::system_error(
			errno, std::system_category(), "fcntl() failed");
	return flags;
}

Blocking_mode Socket::get_blocking_mode() const
{
	return (get_status_flags() & O_NONBLOCK) != 0 ? Blocking_mode::non_blocking
												  : Blocking_mode::blocking;
}

void Socket::set_status_flags(long flags)
{
	if (::fcntl(m_fd, F_SETFL, flags) == -1)
		throw std::system_error(
			errno, std::system_category(), "fcntl() failed");
}

void Socket::set_blocking_mode(Blocking_mode mode)
{
	auto flags = get_status_flags();
	set_status_flags(mode == Blocking_mode::non_blocking
			? (flags | O_NONBLOCK)
			: (flags & ~O_NONBLOCK));
}

Nagle_algorithm Socket::get_nagle_algorithm() const
{
	auto flag = int {0};
	auto length = static_cast<socklen_t>(sizeof(flag));
	getsockopt(IPPROTO_TCP, TCP_NODELAY, &flag, &length);
	switch (flag) {
	case 0:
		return Nagle_algorithm::enabled;
	case 1:
		return Nagle_algorithm::disabled;
	}
	return Nagle_algorithm::unspecified;
}

void Socket::set_nagle_algorithm(Nagle_algorithm nagle_algorithm)
{
	auto flag = int {0};
	switch (nagle_algorithm) {
	case Nagle_algorithm::enabled:
		flag = 0;
		break;
	case Nagle_algorithm::disabled:
		flag = 1;
		break;
	case Nagle_algorithm::unspecified:
		return;
	}
	setsockopt(IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(flag));
}

int Socket::get_receive_buffer_size() const
{
	auto size = int {0};
	auto length = static_cast<socklen_t>(sizeof(size));
	getsockopt(SOL_SOCKET, SO_RCVBUF, &size, &length);
	return size;
}

void Socket::set_receive_buffer_size(int size)
{
	if (size != 0)
		setsockopt(SOL_SOCKET, SO_RCVBUF, &size, sizeof(size));
}

int Socket::get_send_buffer_size() const
{
	auto size = int {0};
	auto length = static_cast<socklen_t>(sizeof(size));
	getsockopt(SOL_SOCKET, SO_SNDBUF, &size, &length);
	return size;
}

std::int32_t Socket::get_send_queue_size() const
{
	const auto size = std::int32_t {};
	const auto err = ioctl(m_fd, TIOCOUTQ, &size);
	if (err == -1 && errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
		throw std::system_error {
			errno, std::system_category(), "get_send_queue_size() failed"};
	if (err == -1)
		return -1;
	return size;
}

void Socket::set_send_buffer_size(int size)
{
	if (size != 0)
		setsockopt(SOL_SOCKET, SO_SNDBUF, &size, sizeof(size));
}

std::error_code Socket::do_shutdown(int how)
{
	auto error = ::shutdown(m_fd, how) == -1
		? std::error_code(errno, std::system_category())
		: std::error_code();
	m_connected = false;
	return error;
}

std::error_code Socket::do_close()
{
	auto error = details::skip_EINTR(::close, m_fd) == -1
		? std::error_code(errno, std::system_category())
		: std::error_code();
	m_fd = -1;
	return error;
}

}} // namespace misc::io
