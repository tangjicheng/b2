#include "misc/aio_writer.h"

#include <cerrno>
#include <cstring>
#include <system_error>

namespace misc {

namespace details {

Aio_control_block::Aio_control_block(size_t buf_size)
	: buf_size_(buf_size)
{
	buf_.reserve(buf_size);
}

bool Aio_control_block::empty() const
{
	return buf_.empty();
}

size_t Aio_control_block::size() const
{
	return buf_.size();
}

size_t Aio_control_block::free() const
{
	return buf_size_ - buf_.size();
}

void Aio_control_block::add(const void* data, size_t size)
{
	const auto begin = static_cast<const unsigned char*>(data);
	const auto end = begin + size;
	buf_.insert(buf_.end(), begin, end);
}

void Aio_control_block::write(int fd, off_t offset)
{
	memset(&cb_, 0, sizeof(aiocb));
	cb_.aio_fildes = fd;
	cb_.aio_offset = offset;
	cb_.aio_buf = buf_.data();
	cb_.aio_nbytes = buf_.size();
	cb_.aio_reqprio = 0;
	cb_.aio_sigevent.sigev_notify = SIGEV_THREAD;
	cb_.aio_sigevent.sigev_signo = 0;
	cb_.aio_sigevent.sigev_value.sival_ptr = this;
	cb_.aio_sigevent.sigev_notify_function
		= &Aio_control_block::completion_handler;
	cb_.aio_sigevent.sigev_notify_attributes = nullptr;
	cb_.aio_lio_opcode = 0;
	int status;
	while ((status = aio_write(&cb_)) == -1 && errno == EAGAIN) {
	}
	if (status == -1) {
		const auto err = errno;
		buf_.clear();
		throw std::system_error(err, std::system_category());
	}
}

void Aio_control_block::wait()
{
	std::unique_lock lk(mtx_);
	cv_.wait(lk, [this] { return completion_received_; });
	completion_received_ = false;
	lk.unlock();
	buf_.clear();
	const auto err = aio_error(&cb_);
	if (err != 0)
		throw std::system_error(err, std::system_category());
	else if (aio_return(&cb_) != static_cast<ssize_t>(cb_.aio_nbytes))
		throw std::system_error(ENOSPC, std::system_category());
}

void Aio_control_block::completion_handler(sigval_t sigval)
{
	const auto this_ptr
		= reinterpret_cast<Aio_control_block*>(sigval.sival_ptr);
	this_ptr->on_completion();
}

void Aio_control_block::on_completion()
{
	std::lock_guard lk(mtx_);
	completion_received_ = true;
	cv_.notify_one();
}

} // namespace details

Aio_writer::Aio_writer(size_t num_cbs, size_t cb_buf_size, int fd)
	: fd_(fd)
{
	if (num_cbs == 0)
		num_cbs = 1;
	if (cb_buf_size < 1024)
		cb_buf_size = 1024;
	for (size_t i = 0; i != num_cbs; ++i)
		cbs_.emplace_back(cb_buf_size);
	cb_ = cbs_.begin();
}

void Aio_writer::attach(int fd)
{
	fd_ = fd;
}

void Aio_writer::add(const void* data, size_t size)
{
	if (size > cb_->free()) {
		write_cb();
		next_cb();
		if (size > cb_->free()) // Precondition violated.
			return;
	}
	cb_->add(data, size);
}

void Aio_writer::seek(off_t offset)
{
	write_cb();
	next_cb();
	offset_ = offset;
}

void Aio_writer::wait()
{
	write_cb();
	for (auto& cb : cbs_) {
		if (!cb.empty())
			cb.wait();
	}
}

void Aio_writer::write_cb()
{
	if (cb_->empty())
		return;
	cb_->write(fd_, offset_);
	offset_ += cb_->size();
}

void Aio_writer::next_cb()
{
	if (++cb_ == cbs_.end())
		cb_ = cbs_.begin();
	if (!cb_->empty())
		cb_->wait();
}

} // namespace misc
