#include "misc/ThreadedTaskQueue.h"

namespace misc {

ThreadedTaskQueue::~ThreadedTaskQueue()
{
	if (m_running)
		stop();
}

void ThreadedTaskQueue::start()
{
	m_thread = std::thread([this] { m_taskQueue.run(true); });
	m_running = true;
}

void ThreadedTaskQueue::stop()
{
	m_taskQueue.shutdown();
	m_thread.join();
	m_running = false;
}

} // namespace misc
