#include "misc/binary_file.h"

#include <arpa/inet.h>

#include <ios>

#include "misc/Reactor.h"
#include "misc/binary_file_handler.h"

namespace misc {

Binary_file_reader::Binary_file_reader(
	Reactor& reactor, Binary_file_handler& handler) noexcept
	: reactor_ {&reactor}
	, handler_ {&handler}
{
}

void Binary_file_reader::init(const std::string& filename) noexcept
{
	filename_ = filename;
}

void Binary_file_reader::start() noexcept
{
	reactor_->callNow([this] {
		file_.open(filename_, std::ios::binary);
		if (!file_.is_open()) {
			handler_->on_error(Binary_file_error::open_failure);
			return;
		}
		handler_->on_event(Binary_file_event::opened);
		reactor_->callLater([this] { read_message_block(); });
	});
}

void Binary_file_reader::stop() noexcept
{
	reactor_->callNow([this] {
		keep_going_ = false;
		if (!file_.is_open())
			return;
		file_.close();
		handler_->on_event(Binary_file_event::closed);
	});
}

void Binary_file_reader::read_message_block() noexcept
{
	if (!keep_going_)
		return;
	for (auto i = 0u; i != 1000; ++i) {
		Length length;
		file_.read(reinterpret_cast<char*>(&length), sizeof(length));
		if (!file_) {
			handler_->on_event(Binary_file_event::end_of_file);
			return;
		}
		length = ntohs(length);
		if (length == 0) {
			handler_->on_event(Binary_file_event::end_of_file);
			return;
		}
		++sequence_number_;
		file_.read(buffer_.data(), length);
		if (!file_) {
			handler_->on_error(Binary_file_error::unexpected_end_of_file);
			return;
		}
		handler_->on_message(buffer_.data(), length, sequence_number_);
	}
	reactor_->callLater([this] { read_message_block(); });
}

} // namespace misc
