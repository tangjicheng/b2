#include "misc/base64.h"

namespace misc { namespace base64 {

namespace {

/**
 *  @return 0, 1 or 2.
 *  @throw DecodeLengthError If the given base64 buffer length is wrong.
 *  @throw DecodeCharacterError If there are illegal characters in the given
 *  base64 buffer.
 */
size_t paddingLength(const char* src, size_t len)
{
	if (len == 0)
		return 0;
	if (len % 4 != 0)
		throw DecodeLengthError("length not a multiple of 4", len);
	// At this point we are guaranteed at least 4 bytes in the buffer.
	size_t count = 0;
	while (src[--len] == '=') {
		if (++count > 2)
			throw DecodeCharacterError("too many trailing '='", len, '=');
	}
	return count;
}

constexpr char encodeMap[]
	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

constexpr unsigned char decodeMap[256] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62, 0, 0, 0,
	63,                             //  [43]:+, [47]:/
	52, 53, 54, 55, 56, 57, 58, 59, //  [48]:0..7
	60, 61, 0, 0, 0, 0, 0, 0,       //  [56]:8,9
	0, 0, 1, 2, 3, 4, 5, 6,         //  [65]:A...
	7, 8, 9, 10, 11, 12, 13, 14,    //       ....
	15, 16, 17, 18, 19, 20, 21, 22, //       ....
	23, 24, 25, 0, 0, 0, 0, 0,      //  [88]:X,Y,Z
	0, 26, 27, 28, 29, 30, 31, 32,  //  [97]:a...
	33, 34, 35, 36, 37, 38, 39, 40, //       ....
	41, 42, 43, 44, 45, 46, 47, 48, //       ....
	49, 50, 51, 0, 0, 0, 0, 0,      // [120]:x,y,z
};                                  // 128+ stay zeroed

constexpr bool decodeWhitelist[256] = {
	false, false, false, false, false, false, false, false, false, false, false,
	false, false, false, false, false, false, false, false, false, false, false,
	false, false, false, false, false, false, false, false, false, false, false,
	false, false, false, false, false, false, false, false, false, false, true,
	false, false, false, true,                            //  [43]:+, [47]:/
	true, true, true, true, true, true, true, true,       //  [48]:0..7
	true, true, false, false, false, false, false, false, //  [56]:8,9
	false, true, true, true, true, true, true, true,      //  [65]:A...
	true, true, true, true, true, true, true, true,       //       ....
	true, true, true, true, true, true, true, true,       //       ....
	true, true, true, false, false, false, false, false,  //  [88]:X,Y,Z
	false, true, true, true, true, true, true, true,      //  [97]:a...
	true, true, true, true, true, true, true, true,       //       ....
	true, true, true, true, true, true, true, true,       //       ....
	true, true, true, false, false, false, false, false,  // [120]:x,y,z
};                                                        // 128+ stay false

} // namespace

size_t decodedBufferLength(const char* src, size_t len)
{
	auto padLen = paddingLength(src, len);
	auto destLen = (len - padLen) / 4 * 3;
	if (padLen == 1)
		destLen += 2;
	else if (padLen == 2)
		destLen += 1;
	return destLen;
}

size_t encode(char* dest, const void* src, size_t len)
{
	auto srcBytes = reinterpret_cast<const unsigned char*>(src);
	size_t destLen = 0;
	auto getByte = [&] {
		return *(srcBytes++);
	};
	auto putChar = [&](char c) {
		*(dest++) = c;
		++destLen;
	};
	auto putByte = [&](unsigned char byte) {
		putChar(encodeMap[byte]);
	};
	for (; len >= 3; len -= 3) {
		auto first = getByte();
		auto second = getByte();
		auto third = getByte();
		putByte(first >> 2);
		putByte((first & 0x03) << 4 | second >> 4);
		putByte((second & 0x0f) << 2 | third >> 6);
		putByte(third & 0x3f);
	}
	if (len == 2) {
		auto first = getByte();
		auto second = getByte();
		putByte(first >> 2);
		putByte((first & 0x03) << 4 | second >> 4);
		putByte((second & 0x0f) << 2);
		putChar('=');
	}
	else if (len == 1) {
		auto first = getByte();
		putByte(first >> 2);
		putByte((first & 0x03) << 4);
		putChar('=');
		putChar('=');
	}
	return destLen;
}

size_t decode(void* dest, const char* src, size_t len)
{
	auto origSrc = src;
	auto destBytes = reinterpret_cast<unsigned char*>(dest);
	size_t destLen = 0;
	auto getByte = [&] {
		unsigned char index = *src;
		if (!decodeWhitelist[index])
			throw DecodeCharacterError(
				"illegal character", src - origSrc, *src);
		++src;
		return decodeMap[index];
	};
	auto putByte = [&](unsigned char byte) {
		*(destBytes++) = byte;
		++destLen;
	};
	len -= paddingLength(src, len);
	for (; len >= 4; len -= 4) {
		auto first = getByte();
		auto second = getByte();
		auto third = getByte();
		auto fourth = getByte();
		putByte(first << 2 | second >> 4);
		putByte(second << 4 | third >> 2);
		putByte(third << 6 | fourth);
	}
	if (len == 3) {
		auto first = getByte();
		auto second = getByte();
		auto third = getByte();
		putByte(first << 2 | second >> 4);
		putByte(second << 4 | third >> 2);
	}
	else if (len == 2) {
		auto first = getByte();
		auto second = getByte();
		putByte(first << 2 | second >> 4);
	}
	// len cannot be 1 since trailingEquals cannot return 3
	return destLen;
}

}} // namespace misc::base64
