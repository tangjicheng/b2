#include "misc/Reactor.h"

#include <algorithm>
#include <cerrno>
#include <future>
#include <iterator>

#include "misc/internal_exception.h"

namespace misc {

namespace {

class Shutdown {
};

} // namespace

Reactor::CallId::CallId(uint64_t value)
	: m_value(value)
{
}

uint64_t Reactor::CallId::base() const
{
	return m_value % CALLID_STEP;
}

Reactor::CallIdGenerator::CallIdGenerator(uint64_t value)
	: m_value(value)
{
}

Reactor::CallId Reactor::CallIdGenerator::next()
{
	return CallId(m_value += CALLID_STEP);
}

Reactor::IOCallback::IOCallback(
	CallId id, int fd, const IOCall& call, uint32_t flag)
	: m_id(id)
	, m_fd(fd)
	, m_call(call)
	, m_flag(flag)
{
}

Reactor::CallId Reactor::IOCallback::id() const
{
	return m_id;
}

int Reactor::IOCallback::fd() const
{
	return m_fd;
}

uint32_t Reactor::IOCallback::flag() const
{
	return m_flag;
}

Reactor::CallStatus Reactor::IOCallback::operator()(int fd) const
{
	return m_call(fd);
}

bool Reactor::IOCallback::removed() const
{
	return m_removed;
}

void Reactor::IOCallback::setRemoved()
{
	m_removed = true;
}

Reactor::TimedCallback::TimedCallback(
	CallId id, std::chrono::microseconds timeout, const RecurringCall& call)
	: m_id(id)
	, m_timeout(timeout)
	, m_call(call)
{
}

Reactor::CallId Reactor::TimedCallback::id() const
{
	return m_id;
}

std::chrono::microseconds Reactor::TimedCallback::timeout() const
{
	return m_timeout;
}

Reactor::CallStatus Reactor::TimedCallback::operator()() const
{
	return m_call();
}

Reactor::LoopCallback::LoopCallback(CallId id, const RecurringCall& call)
	: m_id(id)
	, m_call(call)
{
}

Reactor::CallId Reactor::LoopCallback::id() const
{
	return m_id;
}

Reactor::CallStatus Reactor::LoopCallback::operator()() const
{
	return m_call();
}

bool Reactor::LoopCallback::removed() const
{
	return m_removed;
}

void Reactor::LoopCallback::setRemoved()
{
	m_removed = true;
}

Reactor::Reactor(
	bool supportsIoCalls, bool supportsTimedCalls, bool supportsLoopCalls)
	: m_supportsIoCalls(supportsIoCalls)
	, m_supportsTimedCalls(supportsTimedCalls)
	, m_supportsLoopCalls(supportsLoopCalls)
	, m_idleStrategy {[](bool) {
		std::this_thread::yield();
	}}
	, m_threadId {std::thread::id {}}
	, m_readCallIdGenerator(CALLID_READ_BASE)
	, m_writeCallIdGenerator(CALLID_WRITE_BASE)
	, m_exceptionCallIdGenerator(CALLID_EXCEPTION_BASE)
	, m_timedCallIdGenerator(CALLID_TIMED_BASE)
	, m_loopCallIdGenerator(CALLID_LOOP_BASE)
{
	if (m_supportsIoCalls) {
		m_epfd = epoll_create1(0);
		if (m_epfd == -1) {
			return;
		}
	}
}

Reactor::~Reactor()
{
	if (m_supportsIoCalls) {
		close(m_epfd);
	}
}

void Reactor::init()
{
	if (m_supportsIoCalls) {
		if (m_epfd == -1)
			throw std::system_error(
				errno, std::system_category(), "epoll_create1() failed");
	}
}

void Reactor::run()
{
	m_threadId = std::this_thread::get_id();
	try {
		while (true)
			iterate();
	}
	catch (const Shutdown&) {
	}
	catch (...) {
		if (m_signalSystemError)
			m_signalSystemError(std::system_error(
				errno, std::system_category(), "callback failed."));
		else
			throw;
	}
	m_threadId = std::thread::id();
}

void Reactor::shutdown()
{
	callLater([this] { throw Shutdown(); });
}

void Reactor::iterate()
{
	executeCommands();
	auto workDone = false;
	if (m_supportsIoCalls)
		dispatchIoCalls(workDone);
	if (m_supportsTimedCalls)
		dispatchTimedCalls(workDone);
	if (m_supportsLoopCalls)
		dispatchLoopCalls(workDone);
	idle(workDone);
}

void Reactor::addCommand(Command&& cmd)
{
	commands_queue_.push(std::move(cmd));
	consumed_.store(false, std::memory_order_relaxed);
}

void Reactor::executeCommands()
{
	if (consumed_.load(std::memory_order_relaxed))
		return;
	consumed_.store(true, std::memory_order_relaxed);
	constexpr auto consume_cb = [](Command* command) {
		(*command)();
	};
	const auto limit = commands_per_iteration();
	if (!commands_queue_.consume_all(consume_cb, limit))
		consumed_.store(false, std::memory_order_relaxed);
}

void Reactor::dispatchIoCalls(bool& workDone)
{
	const auto maxEvents = 10;
	epoll_event events[maxEvents];
	auto eventCount = 0;
	do {
		eventCount = epoll_wait(m_epfd, events, maxEvents, 0);
	} while (eventCount < 0 && errno == EINTR);

	if (eventCount < 0) {
		if (m_signalSystemError)
			m_signalSystemError(std::system_error(
				errno, std::system_category(), "epoll_wait() failed"));
	}
	else if (eventCount > 0) {
		auto doClear = false;
		for (auto i = 0; i < eventCount; ++i) {
			auto& epollCallback
				= *(reinterpret_cast<EpollCallback*>(events[i].data.ptr));
			if (epollCallback.callbacks.empty())
				continue;

			const auto epollFlag = getEpollFlag(events[i].events);
			if (processEpollCallback(epollCallback, epollFlag, workDone))
				doClear = true;
		}
		if (doClear) {
			clearIoCallbacks();
		}
	}
}

uint32_t Reactor::getEpollFlag(uint32_t epollEvents)
{
	if ((epollEvents & EPOLLIN) == EPOLLIN)
		return EPOLLIN;
	if ((epollEvents & EPOLLOUT) == EPOLLOUT)
		return EPOLLOUT;
	return EPOLLPRI;
}

bool Reactor::processEpollCallback(
	EpollCallback& epollCallback, uint32_t flag, bool& workDone)
{
	auto& callbacks = epollCallback.callbacks;
	const auto size = callbacks.size();
	auto end = size;
	for (size_t i = 0; i != end;) {
		const auto fd = callbacks[i].fd();
		if (callbacks[i].removed())
			std::swap(callbacks[i], callbacks[--end]);
		else if (callbacks[i].flag() == flag
			&& callbacks[i](fd) == CallStatus::REMOVE) {
			removeFlag(fd, flag);
			callbacks[i].setRemoved();
			std::swap(callbacks[i], callbacks[--end]);
			workDone = true;
		}
		else {
			++i;
			workDone = true;
		}
	}
	if (end == size)
		return false;
	callbacks.erase(callbacks.begin() + end, callbacks.begin() + size);
	return callbacks.empty();
}

void Reactor::dispatchTimedCalls(bool& workDone)
{
	if (m_timedCallbacks.empty() && m_timedCallbackReinsertions.empty())
		return;
	auto now = std::chrono::system_clock::now();
	while (
		!m_timedCallbacks.empty() && m_timedCallbacks.begin()->first <= now) {
		auto callback = std::move(m_timedCallbacks.begin()->second);
		m_timedCallbacks.erase(m_timedCallbacks.begin());
		m_runningCallId = callback.id();
		if (callback() == CallStatus::OK && m_runningCallId) {
			auto expiry = now + callback.timeout();
			m_timedCallbackReinsertions.emplace_back(
				expiry, std::move(callback));
		}
		m_runningCallId.clear();
		workDone = true;
	}
	if (m_timedCallbackReinsertions.empty())
		return;
	m_timedCallbacks.insert(
		std::make_move_iterator(m_timedCallbackReinsertions.begin()),
		std::make_move_iterator(m_timedCallbackReinsertions.end()));
	m_timedCallbackReinsertions.clear();
}

void Reactor::dispatchLoopCalls(bool& workDone)
{
	if (m_loopCallbacks.empty())
		return;
	const auto size = m_loopCallbacks.size();
	auto end = size;
	for (size_t i = 0; i != end;) {
		if (m_loopCallbacks[i].removed())
			std::swap(m_loopCallbacks[i], m_loopCallbacks[--end]);
		else if (m_loopCallbacks[i]() == CallStatus::REMOVE) {
			std::swap(m_loopCallbacks[i], m_loopCallbacks[--end]);
			workDone = true;
		}
		else {
			++i;
			workDone = true;
		}
	}
	if (end == size)
		return;
	m_loopCallbacks.erase(
		m_loopCallbacks.begin() + end, m_loopCallbacks.begin() + size);
}

void Reactor::idle(bool workDone)
{
	if (m_idleStrategy) {
		m_idleStrategy(workDone);
		return;
	}
	if (!workDone)
		std::this_thread::yield();
}

Reactor::CallId Reactor::callOnRead(int fd, const IOCall& call)
{
	if (!m_supportsIoCalls)
		throw InternalError(
			"Reactor doesn't support IO calls", __FILE__, __LINE__);
	CallId callId;
	callNow([this, &callId, &call, fd] {
		callId = m_readCallIdGenerator.next();
		addFlag(fd, EPOLLIN, callId, &call);
	});
	return callId;
}

Reactor::CallId Reactor::callOnWrite(int fd, const IOCall& call)
{
	if (!m_supportsIoCalls)
		throw InternalError(
			"Reactor doesn't support IO calls", __FILE__, __LINE__);
	CallId callId;
	callNow([this, &callId, &call, fd] {
		callId = m_writeCallIdGenerator.next();
		addFlag(fd, EPOLLOUT, callId, &call);
	});
	return callId;
}

Reactor::CallId Reactor::callOnException(int fd, const IOCall& call)
{
	if (!m_supportsIoCalls)
		throw InternalError(
			"Reactor doesn't support IO calls", __FILE__, __LINE__);
	CallId callId;
	callNow([this, &callId, &call, fd] {
		callId = m_exceptionCallIdGenerator.next();
		addFlag(fd, EPOLLPRI, callId, &call);
	});
	return callId;
}

Reactor::CallId Reactor::callEvery(
	std::chrono::microseconds timeout, const RecurringCall& call)
{
	if (!m_supportsTimedCalls)
		throw InternalError(
			"Reactor doesn't support timed calls", __FILE__, __LINE__);
	auto expiry = std::chrono::system_clock::now() + timeout;
	CallId callId;
	callNow([this, &callId, &call, expiry, timeout] {
		callId = m_timedCallIdGenerator.next();
		addTimedCall(expiry, TimedCallback(callId, timeout, call));
	});
	return callId;
}

Reactor::CallId Reactor::callAfter(
	std::chrono::microseconds timeout, const SingleCall& call)
{
	if (!m_supportsTimedCalls)
		throw InternalError(
			"Reactor doesn't support timed calls", __FILE__, __LINE__);
	auto expiry = std::chrono::system_clock::now() + timeout;
	return callAt(expiry, call);
}

Reactor::CallId Reactor::callAt(
	std::chrono::system_clock::time_point expiry, const SingleCall& call)
{
	if (!m_supportsTimedCalls)
		throw InternalError(
			"Reactor doesn't support timed calls", __FILE__, __LINE__);
	CallId callId;
	callNow([this, &callId, &call, expiry] {
		callId = m_timedCallIdGenerator.next();
		addTimedCall(expiry,
			TimedCallback(
				callId, std::chrono::microseconds::max(), [this, call] {
					call();
					return CallStatus::REMOVE;
				}));
	});
	return callId;
}

Reactor::CallId Reactor::callInLoop(const RecurringCall& call)
{
	if (!m_supportsLoopCalls)
		throw InternalError(
			"Reactor doesn't support loop calls", __FILE__, __LINE__);
	CallId callId;
	callNow([this, &callId, &call] {
		callId = m_loopCallIdGenerator.next();
		addLoopCall(LoopCallback(callId, call));
	});
	return callId;
}

void Reactor::callLater(const SingleCall& call)
{
	addCommand(Command(call));
}

void Reactor::callNow(const SingleCall& call)
{
	if (std::this_thread::get_id() == m_threadId
		|| std::thread::id {} == m_threadId)
		call();
	else {
		std::packaged_task<void()> task(call);
		auto future = task.get_future();
		addCommand([&] { task(); });
		future.get();
	}
}

void Reactor::addTimedCall(
	std::chrono::system_clock::time_point expiry, TimedCallback&& callback)
{
	m_timedCallbacks.emplace(expiry, std::move(callback));
}

void Reactor::addLoopCall(LoopCallback&& callback)
{
	m_loopCallbacks.emplace_back(std::move(callback));
}

void Reactor::removeCall(CallId& callId)
{
	if (!callId)
		return;
	switch (callId.base()) {
	case CALLID_READ_BASE:
		callNow([this, callId] { removeIOCall(callId, EPOLLIN); });
		break;
	case CALLID_WRITE_BASE:
		callNow([this, callId] { removeIOCall(callId, EPOLLOUT); });
		break;
	case CALLID_EXCEPTION_BASE:
		callNow([this, callId] { removeIOCall(callId, EPOLLPRI); });
		break;
	case CALLID_TIMED_BASE:
		callNow([this, callId] { removeTimedCall(callId); });
		break;
	case CALLID_LOOP_BASE:
		callNow([this, callId] { removeLoopCall(callId); });
		break;
	default:
		break;
	}
	callId.clear();
}

void Reactor::removeIOCall(CallId callId, uint32_t flag)
{
	for (auto& [fd, epollCallback] : m_fdToEpollCallbacks) {
		for (auto& callback : epollCallback.callbacks) {
			if (callback.id() == callId) {
				removeFlag(callback.fd(), flag);
				callback.setRemoved();
				return;
			}
		}
	}
}

void Reactor::removeTimedCall(CallId callId)
{
	if (m_runningCallId == callId) {
		m_runningCallId.clear();
		return;
	}
	auto i = std::find_if(m_timedCallbacks.begin(), m_timedCallbacks.end(),
		[this, callId](const decltype(m_timedCallbacks)::value_type& callback) {
			return callback.second.id() == callId;
		});
	if (i != m_timedCallbacks.end()) {
		m_timedCallbacks.erase(i);
		return;
	}
	auto j = std::find_if(m_timedCallbackReinsertions.begin(),
		m_timedCallbackReinsertions.end(),
		[this, callId](
			const decltype(m_timedCallbackReinsertions)::value_type& callback) {
			return callback.second.id() == callId;
		});
	if (j != m_timedCallbackReinsertions.end())
		m_timedCallbackReinsertions.erase(j);
}

void Reactor::removeLoopCall(CallId callId)
{
	auto i = std::find_if(m_loopCallbacks.begin(), m_loopCallbacks.end(),
		[this, callId](
			const LoopCallback& callback) { return callback.id() == callId; });
	if (i == m_loopCallbacks.end())
		return;

	i->setRemoved();
}

void Reactor::addFlag(int fd, uint32_t flag, CallId callId, const IOCall* call)
{
	auto callCtl = false;
	auto mode = EPOLL_CTL_ADD;
	epoll_event options;

	const auto it = m_fdToEpollCallbacks.lower_bound(fd);
	if (it == m_fdToEpollCallbacks.end() || it->first != fd) {
		auto epollCallback = EpollCallback {flag, fd, callId, call};
		const auto itCallback = m_fdToEpollCallbacks.emplace_hint(
			it, fd, std::move(epollCallback));
		options.events = flag;
		options.data.ptr = reinterpret_cast<void*>(&(itCallback->second));
		callCtl = true;
	}
	else {
		auto& epollCallback = it->second;
		epollCallback.addCallback(flag, fd, callId, call);
		if (epollCallback.increment(flag)) {
			options.events = epollCallback.eventsFlag;
			options.data.ptr = reinterpret_cast<void*>(&epollCallback);
			if (epollCallback.count() > 1u)
				mode = EPOLL_CTL_MOD;
			callCtl = true;
		}
	}
	if (!callCtl)
		return;
	const auto err = epoll_ctl(m_epfd, mode, fd, &options);
	if (err < 0) {
		if (m_signalSystemError)
			m_signalSystemError(std::system_error(
				errno, std::system_category(), "epoll_ctl() failed"));
	}
}

void Reactor::removeFlag(int fd, uint32_t flag)
{
	const auto it = m_fdToEpollCallbacks.find(fd);
	if (it == m_fdToEpollCallbacks.end())
		return;

	auto& epollCallback = it->second;
	if (!epollCallback.decrement(flag))
		return;

	epoll_event options;
	options.events = epollCallback.eventsFlag;
	options.data.ptr = reinterpret_cast<void*>(&epollCallback);
	const auto mode = options.events == 0 ? EPOLL_CTL_DEL : EPOLL_CTL_MOD;
	const auto err = epoll_ctl(m_epfd, mode, fd, &options);
	if (err < 0) {
		if (m_signalSystemError)
			m_signalSystemError(std::system_error(
				errno, std::system_category(), "epoll_ctl() failed"));
	}
}

void Reactor::clearIoCallbacks()
{
	for (auto it = m_fdToEpollCallbacks.begin();
		 it != m_fdToEpollCallbacks.end();) {
		if (it->second.callbacks.empty())
			it = m_fdToEpollCallbacks.erase(it);
		else
			++it;
	}
}

} // namespace misc
