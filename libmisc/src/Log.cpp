#include "misc/Log.h"

#include <chrono>
#include <cstdio>
#include <mutex>

namespace misc {

namespace {

const char* toString(int priority) noexcept
{
	switch (priority) {
	case APP_LOG_EMERG:
		return "EMERG";
	case APP_LOG_ALERT:
		return "ALERT";
	case APP_LOG_CRIT:
		return "CRIT";
	case APP_LOG_ERR:
		return "ERR";
	case APP_LOG_WARNING:
		return "WARNING";
	case APP_LOG_NOTICE:
		return "NOTICE";
	case APP_LOG_INFO:
		return "INFO";
	case APP_LOG_DEBUG:
		return "DEBUG";
	default:
		// Highest priority if precondition is violated.
		return "EMERG";
	}
}

app_logger_t logger = &appScreenLogger;
std::mutex mtx;

} // namespace

app_logger_t appSetLogger(app_logger_t newLogger) noexcept
{
	auto oldLogger = logger;
	logger = newLogger;
	return oldLogger;
}

void appScreenLogger(int priority, const char* format, va_list ap) noexcept
{
	auto now = std::chrono::system_clock::now();
	auto time = std::chrono::duration_cast<std::chrono::duration<double>>(
		now.time_since_epoch())
					.count();
	std::lock_guard<std::mutex> lk(mtx);
	fprintf(stderr, "%.06f %s: ", time, toString(priority));
	vfprintf(stderr, format, ap);
	fputc('\n', stderr);
}

void appScreenLoggerNoTime(
	int priority, const char* format, va_list ap) noexcept
{
	std::lock_guard<std::mutex> lk(mtx);
	fprintf(stderr, "%s: ", toString(priority));
	vfprintf(stderr, format, ap);
	fputc('\n', stderr);
}

void appLog(int priority, const char* format, ...) noexcept
{
	if (logger == nullptr)
		return;
	va_list ap;
	va_start(ap, format);
	logger(priority, format, ap);
	va_end(ap);
}

int debug = 0;

} // namespace misc
