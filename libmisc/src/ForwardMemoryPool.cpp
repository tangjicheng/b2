#include "misc/ForwardMemoryPool.h"

#include <new>

namespace misc {

ForwardMemoryPool::ForwardMemoryPool(void* start, size_type size)
	: m_start(start)
	, m_size(size)
{
}

void ForwardMemoryPool::reset()
{
	m_used = 0;
	m_balance = 0;
}

void* ForwardMemoryPool::allocate(size_type n)
{
	if (n > m_size - m_used)
		throw std::bad_alloc();
	void* p = static_cast<uint8_t*>(m_start) + m_used;
	m_used += n;
	m_balance += n;
	return p;
}

HeapForwardMemoryPool::HeapForwardMemoryPool(size_type size)
	: m_heap(::operator new(size))
	, m_pool(m_heap, size)
{
}

HeapForwardMemoryPool::~HeapForwardMemoryPool()
{
	::operator delete(m_heap);
}

} // namespace misc
