PROJECT_NAME = libmisc

AR = ar
ARFLAGS = crv

CC = gcc
CXX = g++
LANGSTD = -std=c++20
OPTFLAGS = -O3 -flto
DEBUG = -gdwarf-4
WARNINGS = -Wall -Wextra -Werror -pedantic-errors

GIT = git
SED = sed

TOPDIR := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

EXTLIBDIR = $(realpath $(TOPDIR)/..)
RANGEV3DIR = $(EXTLIBDIR)/range-v3
LIBMISCDIR = $(TOPDIR)
GOOGLETESTDIR = $(EXTLIBDIR)/googletest/build

BOOST_CPPFLAGS = -I/usr/include/boost

RANGEV3_CPPFLAGS = -I$(RANGEV3DIR)/include

LIBMISC_CPPFLAGS = -I$(LIBMISCDIR)/include
LIBMISC_LDLIBS = -L$(LIBMISCDIR)/lib -lmisc -lexpat

GOOGLETEST_CPPFLAGS = -I$(GOOGLETESTDIR)/include
GOOGLETEST_LDLIBS = -L$(GOOGLETESTDIR)/lib64 -lgtest_main -lgtest
