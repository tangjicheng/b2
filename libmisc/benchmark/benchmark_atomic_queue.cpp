#include <sys/time.h>

#include <atomic>
#include <functional>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

#include <misc/atomic_queue.h>

namespace {

constexpr auto count = 10'000'000u;
constexpr auto test_time = std::chrono::seconds {10};

} // namespace

void test_basic_1();
void test_basic_2();
void test_basic_3();
void test_basic_4();
void test_basic_original();
void test_no_consume_atomic();
void test_no_consume_original();
void test_single_thread_atomic();
void test_single_thread_original();
void test_high_load_atomic();
void test_high_load_original();

std::int64_t get_tick_count()
{
	timeval tv;
	gettimeofday(&tv, 0);
	auto millsec = static_cast<std::int64_t>(tv.tv_sec * 1000);
	millsec += static_cast<std::int64_t>(tv.tv_usec) / 1000;
	return millsec;
}

void report(std::int64_t ticks, std::uint64_t external_count,
	std::uint64_t internal_count)
{
	const auto total_count = external_count + internal_count;
	const auto seconds = static_cast<double>(ticks) / 1000;
	const auto num_per_second = total_count / seconds;
	std::cout << "    total_count: " << total_count
			  << ", external_count: " << external_count
			  << ", internal_count: " << internal_count
			  << ", seconds: " << seconds
			  << ", num_per_second: " << std::setprecision(12) << num_per_second
			  << std::endl;
}

void report(std::int64_t ticks, std::uint64_t count)
{
	const auto seconds = static_cast<double>(ticks) / 1000;
	const auto num_per_second = count / seconds;
	std::cout << "    count: " << count << ", seconds: " << seconds
			  << ", num_per_second: " << std::setprecision(12) << num_per_second
			  << std::endl;
}

int main(int, char**)
{
	std::cout << "Atomic queue, 1024x3, no consume" << std::endl;
	test_no_consume_atomic();

	std::cout << "Lock guard pair of vectors, no consume" << std::endl;
	test_no_consume_original();

	std::cout << "Atomic queue, 1024x3, single thread consumer-producer"
			  << std::endl;
	test_single_thread_atomic();

	std::cout << "Lock guard pair of vectors, single thread consumer-producer"
			  << std::endl;
	test_single_thread_original();

	std::cout
		<< "Atomic queue, 1024x3, peek-advance, two producers, single consumer"
		<< std::endl;
	test_basic_1();

	std::cout
		<< "Atomic queue, 8192x3, peek-advance, two producers, single consumer"
		<< std::endl;
	test_basic_2();

	std::cout
		<< "Atomic queue, 1024x24, peek-advance, two producers, single consumer"
		<< std::endl;
	test_basic_3();

	std::cout
		<< "Atomic queue, 8192x3, consume-all, two producers, single consumer"
		<< std::endl;
	test_basic_4();

	std::cout << "Lock guard pair of vectors, two producers, single consumer"
			  << std::endl;
	test_basic_original();

	std::cout
		<< "Atomic queue, 1024x3, consume-all, five producers, single consumer"
		<< std::endl;
	test_high_load_atomic();

	std::cout << "Lock guard pair of vectors, five producers, single consumer"
			  << std::endl;
	test_high_load_original();

	return 0;
}

template <size_t bucket_capacity, size_t buckets_num, bool consume_all,
	bool do_internal_calls>
class Dummy_reactor_int {
public:
	void call_later(int value) { add_command(value); }

	void main_loop()
	{
		if (started_) {
			execute_commands();
			if constexpr (do_internal_calls)
				internal_func();
		}
	}

	void start() { started_.store(true, std::memory_order_relaxed); }
	void stop() { started_.store(false, std::memory_order_relaxed); }

	std::uint64_t processed_internal() { return processed_internal_; }
	std::uint64_t processed_external() { return processed_external_; }

private:
	misc::Atomic_queue<int, bucket_capacity> commands_queue_ {buckets_num};
	std::atomic<bool> started_ {false};
	std::uint64_t processed_internal_ {0u};
	std::uint64_t processed_external_ {0u};
	ALIGN_DESTRUCTIVE std::atomic<bool> consumed_ {false};

	void add_command(int value)
	{
		commands_queue_.push(value);
		consumed_.store(false, std::memory_order_relaxed);
	}

	void execute_commands()
	{
		if (consumed_.load(std::memory_order_relaxed))
			return;
		consumed_.store(true, std::memory_order_relaxed);
		if constexpr (consume_all) {
			const auto limit = 10u;
			const auto consume_cb = [this](const int* value) {
				if (*value > 0)
					++processed_external_;
			};
			if (!commands_queue_.consume_all(consume_cb, limit))
				consumed_.store(false, std::memory_order_relaxed);
		}
		else {
			auto limit = 10u;
			while (limit > 0u) {
				auto value = commands_queue_.peek();
				if (value == nullptr)
					return;
				if (*value > 0) {
					++processed_external_;
				}
				--limit;
				commands_queue_.advance();
			}
			consumed_.store(false, std::memory_order_relaxed);
		}
	}

	void internal_func() { internal_call_later(); }

	void internal_call_later()
	{
		++processed_internal_;
		add_command(0);
	}
};

template <bool do_internal_calls>
class Original_reactor {
public:
	void call_later(int value) { add_command(value); }

	void main_loop()
	{
		if (started_) {
			execute_commands();
			if constexpr (do_internal_calls)
				internal_func();
		}
	}

	void start() { started_.store(true, std::memory_order_relaxed); }
	void stop() { started_.store(false, std::memory_order_relaxed); }

	std::uint64_t processed_internal() { return processed_internal_; }
	std::uint64_t processed_external() { return processed_external_; }

private:
	std::vector<int> m_commands;
	std::vector<int> commands_queue_;
	ALIGN_DESTRUCTIVE std::mutex m_mtx;
	ALIGN_DESTRUCTIVE std::atomic_bool m_commandAdded {false};

	std::atomic<bool> started_ {false};
	std::uint64_t processed_internal_ {0u};
	std::uint64_t processed_external_ {0u};

	void add_command(int value)
	{
		std::lock_guard<std::mutex> lk(m_mtx);
		m_commands.push_back(value);
		m_commandAdded.store(true, std::memory_order_release);
	}

	void execute_commands()
	{
		if (m_commandAdded.load(std::memory_order_acquire) == false)
			return;
		std::unique_lock<std::mutex> lk(m_mtx);
		if (m_commands.empty())
			return;

		const auto limit = 10u;
		if (limit == 0u || m_commands.size() <= limit) {
			commands_queue_ = std::move(m_commands);
			m_commands.clear();
			m_commandAdded.store(false, std::memory_order_release);
		}
		else {
			commands_queue_.resize(limit);
			std::move(m_commands.begin(), m_commands.begin() + limit,
				commands_queue_.begin());
			std::move(m_commands.begin() + limit, m_commands.end(),
				m_commands.begin());
			m_commands.resize(m_commands.size() - limit);
			m_commandAdded.store(true, std::memory_order_release);
		}
		lk.unlock();
		for (auto& value : commands_queue_) {
			if (value > 0) {
				++processed_external_;
			}
		}
	}

	void internal_func() { internal_call_later(); }

	void internal_call_later()
	{
		++processed_internal_;
		add_command(0);
	}
};

void test_basic_1()
{
	auto running_ = std::atomic<bool> {true};
	auto reactor = Dummy_reactor_int<1024, 3, false, true> {};
	reactor.start();

	auto reactor_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.main_loop();
		}
	};
	auto app_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.call_later(1);
		}
	};

	const auto start = get_tick_count();
	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	std::this_thread::sleep_for(test_time);
	running_.store(false, std::memory_order_relaxed);
	t1.join();
	t2.join();
	const auto ticks = get_tick_count() - start;

	const auto external_count = reactor.processed_external();
	const auto internal_count = reactor.processed_internal();
	report(ticks, external_count, internal_count);
}

void test_basic_2()
{
	auto running_ = std::atomic<bool> {true};
	auto reactor = Dummy_reactor_int<8192, 3, false, true> {};
	reactor.start();

	auto reactor_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.main_loop();
		}
	};
	auto app_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.call_later(1);
		}
	};

	const auto start = get_tick_count();
	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	std::this_thread::sleep_for(test_time);
	running_.store(false, std::memory_order_relaxed);
	t1.join();
	t2.join();
	const auto ticks = get_tick_count() - start;

	const auto external_count = reactor.processed_external();
	const auto internal_count = reactor.processed_internal();
	report(ticks, external_count, internal_count);
}

void test_basic_3()
{
	auto running_ = std::atomic<bool> {true};
	auto reactor = Dummy_reactor_int<1024, 24, false, true> {};
	reactor.start();

	auto reactor_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.main_loop();
		}
	};
	auto app_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.call_later(1);
		}
	};

	const auto start = get_tick_count();
	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	std::this_thread::sleep_for(test_time);
	running_.store(false, std::memory_order_relaxed);
	t1.join();
	t2.join();
	const auto ticks = get_tick_count() - start;

	const auto external_count = reactor.processed_external();
	const auto internal_count = reactor.processed_internal();
	report(ticks, external_count, internal_count);
}

void test_basic_4()
{
	auto running_ = std::atomic<bool> {true};
	auto reactor = Dummy_reactor_int<8192, 3, true, true> {};
	reactor.start();

	auto reactor_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.main_loop();
		}
	};
	auto app_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.call_later(1);
		}
	};

	const auto start = get_tick_count();
	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	std::this_thread::sleep_for(test_time);
	running_.store(false, std::memory_order_relaxed);
	t1.join();
	t2.join();
	const auto ticks = get_tick_count() - start;

	const auto external_count = reactor.processed_external();
	const auto internal_count = reactor.processed_internal();
	report(ticks, external_count, internal_count);
}

void test_basic_original()
{
	auto running_ = std::atomic<bool> {true};
	auto reactor = Original_reactor<true> {};
	reactor.start();

	auto reactor_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.main_loop();
		}
	};
	auto app_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.call_later(1);
		}
	};

	const auto start = get_tick_count();
	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	std::this_thread::sleep_for(test_time);
	running_.store(false, std::memory_order_relaxed);
	t1.join();
	t2.join();
	const auto ticks = get_tick_count() - start;

	const auto external_count = reactor.processed_external();
	const auto internal_count = reactor.processed_internal();
	report(ticks, external_count, internal_count);
}

void test_no_consume_atomic()
{
	auto reactor = Dummy_reactor_int<8192, 3, true, false> {};
	reactor.start();

	const auto start = get_tick_count();
	for (auto i = 0u; i < count; ++i) {
		reactor.main_loop();
	}
	const auto ticks = get_tick_count() - start;

	report(ticks, count);
}

void test_no_consume_original()
{
	auto reactor = Original_reactor<false> {};
	reactor.start();

	const auto start = get_tick_count();
	for (auto i = 0u; i < count; ++i) {
		reactor.main_loop();
	}
	const auto ticks = get_tick_count() - start;

	report(ticks, count);
}

void test_single_thread_atomic()
{
	auto reactor = Dummy_reactor_int<8192, 3, true, true> {};
	reactor.start();

	const auto start = get_tick_count();
	for (auto i = 0u; i < count; ++i) {
		reactor.main_loop();
	}
	const auto ticks = get_tick_count() - start;

	report(ticks, count);
}

void test_single_thread_original()
{
	auto reactor = Original_reactor<true> {};
	reactor.start();

	const auto start = get_tick_count();
	for (auto i = 0u; i < count; ++i) {
		reactor.main_loop();
	}
	const auto ticks = get_tick_count() - start;

	report(ticks, count);
}

void test_high_load_atomic()
{
	auto running_ = std::atomic<bool> {true};
	auto reactor = Dummy_reactor_int<1024, 3, true, true> {};
	reactor.start();

	auto reactor_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.main_loop();
		}
	};
	auto app_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.call_later(1);
		}
	};

	const auto start = get_tick_count();
	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	std::thread t3 {app_t};
	std::thread t4 {app_t};
	std::thread t5 {app_t};
	std::this_thread::sleep_for(test_time);
	running_.store(false, std::memory_order_relaxed);
	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	const auto ticks = get_tick_count() - start;

	const auto external_count = reactor.processed_external();
	const auto internal_count = reactor.processed_internal();
	report(ticks, external_count, internal_count);
}

void test_high_load_original()
{
	auto running_ = std::atomic<bool> {true};
	auto reactor = Original_reactor<true> {};
	reactor.start();

	auto reactor_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.main_loop();
		}
	};
	auto app_t = [&reactor, &running_]() {
		while (running_.load(std::memory_order_relaxed)) {
			reactor.call_later(1);
		}
	};

	const auto start = get_tick_count();
	std::thread t1 {reactor_t};
	std::thread t2 {app_t};
	std::thread t3 {app_t};
	std::thread t4 {app_t};
	std::thread t5 {app_t};
	std::this_thread::sleep_for(test_time);
	running_.store(false, std::memory_order_relaxed);
	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	const auto ticks = get_tick_count() - start;

	const auto external_count = reactor.processed_external();
	const auto internal_count = reactor.processed_internal();
	report(ticks, external_count, internal_count);
}
