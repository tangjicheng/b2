#include <arpa/inet.h>  // for sockaddr_in and inet_ntoa
#include <fcntl.h>
#include <sys/epoll.h>
#include <sys/socket.h>  // for socket, bind, listen, accept
#include <unistd.h>
#include <cstring>
#include <functional>
#include <iostream>
#include <unordered_map>
#include <vector>

class EventHandler {
   public:
    virtual void handleEvent(uint32_t events) = 0;
    virtual int getFd() const = 0;
};

class Reactor {
   public:
    Reactor() {
        epoll_fd = epoll_create1(0);
        if (epoll_fd == -1) {
            perror("epoll_create1");
            exit(EXIT_FAILURE);
        }
    }

    ~Reactor() {
        close(epoll_fd);
    }

    void registerHandler(EventHandler* handler, uint32_t events) {
        struct epoll_event event;
        event.events = events;
        event.data.ptr = handler;
        if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, handler->getFd(), &event) == -1) {
            perror("epoll_ctl: add");
            exit(EXIT_FAILURE);
        }
        handlers[handler->getFd()] = handler;
    }

    void unregisterHandler(EventHandler* handler) {
        if (epoll_ctl(epoll_fd, EPOLL_CTL_DEL, handler->getFd(), nullptr) == -1) {
            perror("epoll_ctl: del");
            exit(EXIT_FAILURE);
        }
        handlers.erase(handler->getFd());
    }

    void eventLoop() {
        const int MAX_EVENTS = 10;
        struct epoll_event events[MAX_EVENTS];

        while (true) {
            int n = epoll_wait(epoll_fd, events, MAX_EVENTS, -1);
            if (n == -1) {
                perror("epoll_wait");
                exit(EXIT_FAILURE);
            }

            for (int i = 0; i < n; ++i) {
                auto handler = static_cast<EventHandler*>(events[i].data.ptr);
                handler->handleEvent(events[i].events);
            }
        }
    }

   private:
    int epoll_fd;
    std::unordered_map<int, EventHandler*> handlers;
};

class SimpleHandler : public EventHandler {
   public:
    SimpleHandler(int fd) : fd(fd) {
        setNonBlocking(fd);
    }

    void handleEvent(uint32_t events) override {
        if (events & EPOLLIN) {
            char buffer[512];
            int bytes = read(fd, buffer, sizeof(buffer) - 1);
            if (bytes > 0) {
                buffer[bytes] = '\0';
                std::cout << "Read: " << buffer << std::endl;
            } else if (bytes == 0) {
                std::cout << "Connection closed" << std::endl;
                close(fd);
            } else {
                perror("read");
            }
        }
    }

    int getFd() const override {
        return fd;
    }

   private:
    int fd;

    void setNonBlocking(int fd) {
        int flags = fcntl(fd, F_GETFL, 0);
        if (flags == -1) {
            perror("fcntl F_GETFL");
            exit(EXIT_FAILURE);
        }
        if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
            perror("fcntl F_SETFL");
            exit(EXIT_FAILURE);
        }
    }
};

int main() {
    int listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_fd == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in addr;
    std::memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(8081);

    if (bind(listen_fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    if (listen(listen_fd, SOMAXCONN) == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    Reactor reactor;

    class ListenHandler : public EventHandler {
       public:
        ListenHandler(int fd, Reactor& reactor) : fd(fd), reactor(reactor) {
        }

        void handleEvent(uint32_t events) override {
            if (events & EPOLLIN) {
                int client_fd = accept(fd, nullptr, nullptr);
                if (client_fd == -1) {
                    perror("accept");
                } else {
                    std::cout << "Accepted connection" << std::endl;
                    auto handler = new SimpleHandler(client_fd);
                    reactor.registerHandler(handler, EPOLLIN | EPOLLET);
                }
            }
        }

        int getFd() const override {
            return fd;
        }

       private:
        int fd;
        Reactor& reactor;
    };

    ListenHandler listenHandler(listen_fd, reactor);
    reactor.registerHandler(&listenHandler, EPOLLIN);

    reactor.eventLoop();

    close(listen_fd);
    return 0;
}
