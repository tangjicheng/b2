#pragma once

#include <concepts>

template <typename T>
concept StringTrait = requires(T t) {
    { t.begin() } -> std::same_as<typename T::iterator>;
    { t.end() } -> std::same_as<typename T::iterator>;
    { t.front() } -> std::same_as<char&>;
    { t.back() } -> std::same_as<char&>;
};

enum class Direction { ToPipe, ToSOH };

template <Direction d = Direction::ToPipe, StringTrait T>
T replace(const T& input) {
    T output = input;

    if constexpr (d == Direction::ToPipe) {
        for (char& ch : output) {
            if (ch == '\x01') {
                ch = '|';
            }
        }
        return output;
    }

    if constexpr (d == Direction::ToSOH) {
        for (char& ch : output) {
            if (ch == '|') {
                ch = '\x01';
            }
        }
        return output;
    }

    return output;
}
