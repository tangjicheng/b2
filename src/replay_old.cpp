#include <quickfix/Application.h>
#include <quickfix/Exceptions.h>
#include <quickfix/FileStore.h>
#include <quickfix/Log.h>
#include <quickfix/Message.h>
#include <quickfix/MessageCracker.h>
#include <quickfix/Session.h>
#include <quickfix/SocketInitiator.h>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <optional>
#include <regex>
#include <string>
#include <thread>
#include <unordered_set>
#include <vector>
#include <spdlog/spdlog.h>
#include "utils.hpp"

[[nodiscard]]
inline std::string extractFIXPart(const std::string& input) {
    static const std::regex pattern(R"(8=FIX.*?\x0110=\d+\x01)");

    std::smatch match;
    if (std::regex_search(input, match, pattern)) {
        return match.str();
    } else {
        return "";
    }
}

[[nodiscard]]
inline std::vector<std::string> parseFixStringFromFile(const std::string& file_path) {
    std::vector<std::string> orders_vec;
    std::ifstream file(file_path);
    std::string line;

    if (!file) {
        std::cerr << "Unable to open file: " << file_path << std::endl;
        return orders_vec;
    }

    while (getline(file, line)) {
        if (line.find("35=D") != std::string::npos) {
            auto new_order_str = extractFIXPart(line);
            if (new_order_str != "") {
                orders_vec.push_back(new_order_str);
            }
        } else if (line.find("35=F") != std::string::npos) {
            auto cancel_order_str = extractFIXPart(line);
            if (cancel_order_str != "") {
                orders_vec.push_back(cancel_order_str);
            }
        }
    }

    file.close();
    return orders_vec;
}

[[nodiscard]]
inline std::optional<FIX::Message> parseOneLine(const std::string& line) {
    std::string raw_fix_message = extractFIXPart(line);

    FIX::Message message;
    try {
        message.setString(raw_fix_message, false);
    } catch (const FIX::InvalidMessage& ex) {
        return std::nullopt;
    }

    return message;
}

[[nodiscard]]
inline std::vector<FIX::Message> parseOrdersFromFile(const std::string& file_path) {
    std::vector<FIX::Message> orders_vec;
    std::ifstream file(file_path);
    std::string line;

    if (!file) {
        std::cerr << "Unable to open file: " << file_path << std::endl;
        return orders_vec;
    }

    while (getline(file, line)) {
        if (line.find("35=D") != std::string::npos) {
            auto new_order_opt = parseOneLine(line);
            if (new_order_opt) {
                orders_vec.push_back(*new_order_opt);
            }
        } else if (line.find("35=F") != std::string::npos) {
            auto cancel_order_opt = parseOneLine(line);
            if (cancel_order_opt) {
                orders_vec.push_back(*cancel_order_opt);
            }
        }
    }

    file.close();
    return orders_vec;
}

static FIX::SessionID session_id;

class FixApp : public FIX::Application {
   public:
    void onCreate(const FIX::SessionID& sessionID) override {
        std::cout << "onCreate: " << sessionID.toString() << std::endl;
    }

    void onLogon(const FIX::SessionID& sessionID) override {
        std::cout << "onLogon: " << sessionID.toString() << std::endl;
        session_id = sessionID;
    }

    void onLogout(const FIX::SessionID& sessionID) override {
        std::cout << "onLogout: " << sessionID.toString() << std::endl;
    }

    void toAdmin(FIX::Message& message, const FIX::SessionID& sessionID) override {
        std::cout << "toAdmin: " << sessionID.toString() << std::endl;
    }

    void toApp(FIX::Message& message, const FIX::SessionID& sessionID) noexcept override {
        // std::cout << "toApp: " << sessionID.toString() << "  :  " << replace(message.toString())
        //           << std::endl;
    }

    void fromAdmin(const FIX::Message& message, const FIX::SessionID& sessionID) noexcept override {
        std::cout << "fromAdmin: " << sessionID.toString() << "  :  " << replace(message.toString())
                  << std::endl;
    }

    void fromApp(const FIX::Message& message, const FIX::SessionID& sessionID) noexcept override {
        // std::cout << "fromApp: " << sessionID.toString() << "  :  " << replace(message.toString())
        //           << std::endl;
    }
};

int main(int argc, char** argv) {
    spdlog::info("Hello, spdlog!");

    if (argc != 3) {
        std::cout << "Usage: " << std::string(argv[0]) << "<fix config file>  <replay filename>"
                  << std::endl;
        return 1;
    }
    std::string config_filename(argv[1]);
    std::string replay_filename(argv[2]);

    auto fix_settings = FIX::SessionSettings(config_filename);
    auto application = FixApp();
    auto store_factory = FIX::FileStoreFactory(fix_settings);
    auto log_factory = FIX::ScreenLogFactory(fix_settings);
    auto initiator = FIX::SocketInitiator(application, store_factory, fix_settings, log_factory);

    try {
        initiator.start();
        std::this_thread::sleep_for(std::chrono::seconds(1));
        auto fix_str_vec = parseFixStringFromFile(replay_filename);
        std::cout << "Finish parsing, fix_str_vec.size: " << fix_str_vec.size() << std::endl;
        FIX::Message msg;

        auto start = std::chrono::high_resolution_clock::now();
        for (auto&& fix_str : fix_str_vec) {
            msg.setString(fix_str, false);
        }
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
        double unit_duration = static_cast<double>(duration) / fix_str_vec.size();
        std::cout << "Total cost: " << duration << " nanoseconds to execute." << std::endl;
        std::cout << "Unit cost: " << unit_duration << " nanoseconds per message encode"
                  << std::endl;

        for (auto&& fix_str : fix_str_vec) {
            msg.setString(fix_str, false);
            FIX::Session::sendToTarget(msg, session_id);
        }

        while (true) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    } catch (const FIX::Exception& e) {
        std::cout << "FIX::Exception  " << e.what() << std::endl;
    } catch (const std::exception& e) {
        std::cout << "std::exception  " << e.what() << std::endl;
    }

    initiator.stop();

    return 0;
}
