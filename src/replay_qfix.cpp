#include <qfix/Application.h>
#include <qfix/Exceptions.h>
#include <qfix/FileStore.h>
#include <qfix/Log.h>
#include <qfix/Message.h>
#include <qfix/Session.h>
#include <qfix/SocketInitiator.h>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <optional>
#include <regex>
#include <string>
#include <thread>
#include <unordered_set>
#include <vector>
#include "utils.hpp"

[[nodiscard]]
inline std::string extractFIXPart(const std::string& input) {
    static const std::regex pattern(R"(8=FIX.*?\x0110=\d+\x01)");

    std::smatch match;
    if (std::regex_search(input, match, pattern)) {
        return match.str();
    } else {
        return "";
    }
}

[[nodiscard]]
inline std::vector<std::string> parseFixStringFromFile(const std::string& file_path) {
    std::vector<std::string> orders_vec;
    std::ifstream file(file_path);
    std::string line;

    if (!file) {
        std::cerr << "Unable to open file: " << file_path << std::endl;
        return orders_vec;
    }

    while (getline(file, line)) {
        if (line.find("35=D") != std::string::npos) {
            auto new_order_str = extractFIXPart(line);
            if (new_order_str != "") {
                orders_vec.push_back(new_order_str);
            }
        } else if (line.find("35=F") != std::string::npos) {
            auto cancel_order_str = extractFIXPart(line);
            if (cancel_order_str != "") {
                orders_vec.push_back(cancel_order_str);
            }
        }
    }

    file.close();
    return orders_vec;
}

[[nodiscard]]
inline std::optional<FIX::Message> parseOneLine(const std::string& line) {
    std::string raw_fix_message = extractFIXPart(line);

    FIX::Message message;
    try {
        message.setString(raw_fix_message, false);
    } catch (const FIX::InvalidMessage& ex) {
        return std::nullopt;
    }

    return message;
}

[[nodiscard]]
inline std::vector<FIX::Message> parseOrdersFromFile(const std::string& file_path) {
    std::vector<FIX::Message> orders_vec;
    std::ifstream file(file_path);
    std::string line;

    if (!file) {
        std::cerr << "Unable to open file: " << file_path << std::endl;
        return orders_vec;
    }

    while (getline(file, line)) {
        if (line.find("35=D") != std::string::npos) {
            auto new_order_opt = parseOneLine(line);
            if (new_order_opt) {
                orders_vec.push_back(*new_order_opt);
            }
        } else if (line.find("35=F") != std::string::npos) {
            auto cancel_order_opt = parseOneLine(line);
            if (cancel_order_opt) {
                orders_vec.push_back(*cancel_order_opt);
            }
        }
    }

    file.close();
    return orders_vec;
}

static FIX::SessionID session_id;

class TestApplication : public FIX::Application {
   public:
    TestApplication() {
    }

    void onCreate(const FIX::SessionID& sessionId) {
        auto session = FIX::Session::lookupSession(sessionId);
        session->set_handler(handler_);
        session_id = sessionId;
    }

    int getCount() {
        return handler_.m_count;
    }

   private:
    class TestHandler : public FIX::Handler {
       public:
        void onLogon() override {
        }
        void onLogout() override {
        }
        void toAdmin(FIX::Message&) override {
        }
        void toApp(FIX::Message&) override {
        }
        void fromAdmin(const FIX::Message&) override {
        }
        void fromApp(const FIX::Message&) override {
            m_count++;
        }
        void onRun() {
        }

        void on_disconnect() override {
        }
        void on_incoming_message() override {
        }
        void on_incoming_reset(std::int32_t) override {
        }
        void on_outgoing_message(const FIX::Message&) override {
        }
        void on_outgoing_reset() override {
        }

        int m_count{0};
    } handler_;
};

int main(int argc, char** argv) {
    if (argc != 3) {
        std::cout << "Usage: " << std::string(argv[0]) << "<fix config file>  <replay filename>"
                  << std::endl;
        return 1;
    }
    std::string config_filename(argv[1]);
    std::string replay_filename(argv[2]);

    auto fix_settings = FIX::SessionSettings(config_filename);
    auto application = TestApplication();
    auto store_factory = FIX::FileStoreFactory(fix_settings);
    auto log_factory = FIX::ScreenLogFactory(fix_settings);
    auto initiator = FIX::SocketInitiator(application, store_factory, fix_settings, log_factory);

    try {
        initiator.start();
        std::this_thread::sleep_for(std::chrono::seconds(1));
        auto fix_str_vec = parseFixStringFromFile(replay_filename);
        std::cout << "Finish parsing, fix_str_vec.size: " << fix_str_vec.size() << std::endl;
        FIX::Message msg;

        auto start = std::chrono::high_resolution_clock::now();
        for (auto&& fix_str : fix_str_vec) {
            msg.setString(fix_str, false);
        }
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
        double unit_duration = static_cast<double>(duration) / fix_str_vec.size();
        std::cout << "Total cost: " << duration << " nanoseconds to execute." << std::endl;
        std::cout << "Unit cost: " << unit_duration << " nanoseconds per message encode"
                  << std::endl;

        for (auto&& fix_str : fix_str_vec) {
            msg.setString(fix_str, false);
            FIX::Session::sendToTarget(msg, session_id);
        }

        while (true) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    } catch (const FIX::Exception& e) {
        std::cout << "FIX::Exception  " << e.what() << std::endl;
    } catch (const std::exception& e) {
        std::cout << "std::exception  " << e.what() << std::endl;
    }

    initiator.stop();

    return 0;
}
